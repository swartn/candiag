#!/bin/sh
#
#  DESCRIPTION
#
#  Purge gp and ge files.
#
#  PREREQUISITES
#
#  PARMSUB PARAMETERS
#
#    -- days, flabel, memory1, model1, run, dtime.
#
#  CHANGELOG
#
#    2023-11-03: Changed to be run after standard cleanall deck and
#                 only target additional chemical files
#    2023-02-01: Converted to shell script and borrowed code from
#                CanESM5 cleanall (D. Plummer)
#    2015-04-20: Added several gp files of chemical fields
#    2015-02-26: Added delz and delm files created by dynamics deck
#                 but held on to for use in chemistry (D. Plummer)
#    2008-03-25: Added gpu, gpv and gpvr files (A.Jonsson)
#    2007-11-19: Added proper description header (A.Jonsson)
#    2007-11-18: Added purging of gpz, gpt and gpq files (A.Jonsson)
#    2007-11-15: Added subversion date ('Date') and revision ('Rev')
#                keyword substitutions to all decks (A.Jonsson)
#    2007-11-12: Added delete statement for gerho file in
#                gxpurg_chem_diag.dk (A.Jonsson)
#    2007-11-12: Moved gppurg_chem_diag.dk to gxpurg_chem_diag.dk
#                (A.Jonsson)
#

    if [ "$SITE_ID" = 'Dorval' -o "$SITE_ID" = 'DrvlSC' ] ; then dpalist=${dpalist:='on'} ; export dpalist ; fi
#
    if [ "$nocleanup" != "on" ] ; then
#
      for v in gedpde gend gep gerho geshum get gew gewe gez ; do
        access $v ${flabel}_$v nocp na
        delete $v na
      done

      for v in gpday gpepfd gpepfy gpepfz gppv ; do
        access $v ${flabel}_$v nocp na
        delete $v na
      done

      for v in gpvres gpwe gpwres ; do
        access $v ${flabel}_$v nocp na
        delete $v na
      done

      for v in gthpv gthz ; do
        access $v ${flabel}_$v nocp na
        delete $v na
      done

      access x ${flabel}_delm nocp na
      delete x na
      access x ${flabel}_delz nocp na
      delete x na

    fi

