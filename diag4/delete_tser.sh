#!/bin/bash
# Delete time series files
# RSK, 05 2019

# create file lists to be deleted
data_file_lists $runid $RUNPATH --start=$run_start_year:$run_start_month --stop=$run_stop_year:$run_stop_month
[ -s ${tser_uxxx}_time_series_files ] || { echo "ERROR in deling: could not make ${tser_uxx}_time_series_files" ; exit 1 ; }

# delete archived time series
fdb mdelete -f ${tser_uxxx}_time_series_files
