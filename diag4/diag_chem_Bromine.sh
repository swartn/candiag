#!/bin/sh
#
# This script is only included in the merged_diag_script grouping when
# gas-phase chemistry is run in MAM mode. BW, June 2023
#
# Amendments to original: source the file of switches
#
#  DESCRIPTION (original name: diag_chem_Bromine_960d.sh)
# 
#  Create monthly averages and monthly zonal averages for bromine
#  species: Br, BrO, HBr, HOBr, BrONO2, BrCl, CH3Br and the advected
#  family BX (Br + BrO + HOBr + BrONO2 + BrCl); and diagnosed
#  families: BrOx (Br + BrO + HOBr + BrCl), Bry (Br + BrO + HOBr +
#  BrONO2 + BrCl + HBr) and Brz (Bry + CH3Br).
#
#  Note that BrOx = BX - BRONO2; and Bry = BX + HBr.
#
#  PARMSUB PARAMETERS
#
#    -- chem, coord, days, flabel, lat, lon, memory1, model1, npg,
#       plid, plv, plv2, pmax, pmaxc, pmin, run, stime, t1, t2, t3, t4
#    -- p01-p100 (pressure level set)
#
#  PREREQUISITES
#
#    -- GS and SS history files
#
#  CHANGELOG
#
#    2023-10-27: Calculation of variables required by selstep and daily save 
#                 from available CanESM5 variables
#    2023-01-23: Converted to shell script, removed ztsave and the logic
#                 to support running chemistry on a subset of model levels
#                 controlled by the trchem switch (D. Plummer)
#    2014-06-04: Monthly-average output on model surfaces for CCMI and 
#                 harmonization of save options iesave, ipsave, gesave
#                 (D. Plummer)
#    2013-07-19: Modified to include extra halocarbons CHBr3 and CH2Br2 
#                 (D. Plummer)
#    2013-06-10: Add iesave option (Y. Jiao)
#    2011-11-29: Modified xsave labels to be friendlier when converted to
#                timeseries filenames by spltdiag (D. Plummer)
#    2007-07-01: Added saving of instantaneous (un-averaged) Bry to ie
#                file (A.Jonsson)
#    2008-06-30: Changed output names for diagnostic families: NY to
#                NOY; CY to CLY; BY to BRY; to avoid confusion with
#                new simulated advected families (NY, CY, BY). For
#                consistency CZ was changed to CLZ and BZ to BRZ
#                (A.Jonsson)
#    2008-06-27: Modified super label for Brz to spell out components
#                (A.Jonsson)
#    2008-06-25: Added diagnostics for BrOx family; Changed order of
#                species to output files to better reflect grouping
#                within families and to separate between chemistry and
#                full domain species; Changed super labels for
#                families (BX, BrOx, Bry) to show all components
#                (A.Jonsson)
#    2007-11-23: Capitalized all super labels (affecting H20(g),
#                H2O(s), HNO3(g), HNO3(s) and labels containing Br,
#                Cl, x, y or z) (A.Jonsson)
#    2007-11-21: Correction to BX super label: added BrCl (A.Jonsson)
#    2007-11-15: Added subversion date ('Date') and revision ('Rev')
#                keyword substitutions to all decks (A.Jonsson)
#    2007-11-13: Revised $ztsave if-statement to only encompass xp
#                file data (A.Jonsson)
#    2007-11-13: Added 'cat Input_cards' at the end of most decks
#                (A.Jonsson)
#    2007-11-05: Added Bromine deck (A.Jonsson)
#    2007-11-??: Based on the Brz deck (A.Jonsson)
#
# ------------------------------------------------------------------------------

# Source the list of on/off switches
source ${CANESM_SRC_ROOT}/CanDIAG/diag4/gaschem_diag_switches.sh

#  ---- variables for selstep
yearoff=`echo "$year_offset"  | $AWK '{printf "%4d", $0+1}'`
deltmin=`echo "$delt"         | $AWK '{printf "%5d", $0/60.}'`
#
#  ---- number of timesteps in one day used for selection of once daily output
dyfrq=`echo $delt | awk '{printf "%04d",86400/$1}'`

#
#  ---- access model history files
#
.   spfiles.cdk
.   ggfiles.cdk
#
#  ----  get the surface pressure field
#
    access gslnsp ${flabel}_gslnsp na
    if [ ! -s gslnsp ] ; then
      if [ "$datatype" = "gridsig" ] ; then
        echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME LNSP" |
          ccc select npakgg gslnsp
      else
        echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME LNSP" |
          ccc select npaksp sslnsp
        echo "COFAGG.   $lon$lat" | ccc cofagg sslnsp gslnsp
        rm sslnsp
      fi
    fi
#
#  ----  select out required fields from model history
#
    echo "SELECT    STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME   BR   BO   BN   BC
SELECT       OB" | ccc select npakgg Br_ge BrO_ge BrONO2_ge BrCl_ge HOBr_ge
    echo "SELECT    STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME   BX   HB" |
                   ccc select npaksp BX_se HBr_se
    echo "SELECT    STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME   CB   BF   DB" |
                   ccc select npaksp CH3Br_se CHBr3_se CH2Br2_se
#
    echo "COFAGGxxx $lon$lat    0$npg" > .cofagg_input_card    
    cofagg BX_se    BX_ge input=.cofagg_input_card
    cofagg HBr_se   HBr_ge input=.cofagg_input_card
    cofagg CH3Br_se CH3Br_ge input=.cofagg_input_card
    cofagg CHBr3_se CHBr3_ge input=.cofagg_input_card
    cofagg CH2Br2_se CH2Br2_ge input=.cofagg_input_card
#
    rm *_se npakgg npaksp
#
#  ----  create new variables
#
# -- create total inorganic bromine family (Bry = BX + HBr)
    add BX_ge HBr_ge Bry_ge
#
# -- create BrOx family (Br + BrO + HOBr + BrCl)
    add    Br_ge    BrO_ge    aw
    add    aw       HOBr_ge   bw
    add    bw       BrCl_ge   cw
    echo "NEWNAM.    BROX" | ccc newnam cw  BrOx_ge
#
    rm aw bw cw
#
# --- create total bromine family (Brz)
    echo "XLIN......       3.0       0.0" | ccc xlin CHBr3_ge  3xCHBr3_ge
    echo "XLIN......       2.0       0.0" | ccc xlin CH2Br2_ge 2xCH2Br2_ge
    add Bry_ge CH3Br_ge aw
    add aw 3xCHBr3_ge   bw
    add bw 2xCH2Br2_ge  Brz_ge
    rm aw bw 3xCHBr3_ge 2xCH2Br2_ge
#
#  ----  interpolate onto constant pressure surfaces
#
    echo "GSAPL.    $plv       0.0       0.0$coord$plid
$p01$p02$p03$p04$p05$p06$p07$p08$p09$p10$p11$p12$p13$p14$p15$p16
$p17$p18$p19$p20$p21$p22$p23$p24$p25$p26$p27$p28$p29$p30$p31$p32
$p33$p34$p35$p36$p37$p38$p39$p40$p41$p42$p43$p44$p45$p46$p47$p48
$p49$p50$p51$p52$p53$p54$p55$p56$p57$p58$p59$p60$p61$p62$p63$p64
$p65$p66$p67$p68$p69$p70$p71$p72$p73$p74$p75$p76$p77$p78$p79$p80
$p81$p82$p83$p84$p85$p86$p87$p88$p89$p90$p91$p92$p93$p94$p95$p96
$p97$p98$p99$p100" > .gsapl_input_card

    gsapl Br_ge     gslnsp Br_gp input=.gsapl_input_card
    gsapl BrO_ge    gslnsp BrO_gp input=.gsapl_input_card
    gsapl HOBr_ge   gslnsp HOBr_gp input=.gsapl_input_card
    gsapl BrCl_ge   gslnsp BrCl_gp input=.gsapl_input_card
    gsapl BrOx_ge   gslnsp BrOx_gp input=.gsapl_input_card
    gsapl BrONO2_ge gslnsp BrONO2_gp input=.gsapl_input_card
    gsapl BX_ge     gslnsp BX_gp input=.gsapl_input_card
    gsapl HBr_ge    gslnsp HBr_gp input=.gsapl_input_card
    gsapl Bry_ge    gslnsp Bry_gp input=.gsapl_input_card
    gsapl CH3Br_ge  gslnsp CH3Br_gp input=.gsapl_input_card
    gsapl CHBr3_ge  gslnsp CHBr3_gp input=.gsapl_input_card
    gsapl CH2Br2_ge gslnsp CH2Br2_gp input=.gsapl_input_card
    gsapl Brz_ge    gslnsp Brz_gp input=.gsapl_input_card
#
#  ----  create monthly and zonal averages
#
# -- monthly averages
    timavg Br_gp     Br_tav
    timavg BrO_gp    BrO_tav
    timavg HOBr_gp   HOBr_tav
    timavg BrCl_gp   BrCl_tav
    timavg BrOx_gp   BrOx_tav
    timavg BrONO2_gp BrONO2_tav
    timavg BX_gp     BX_tav
    timavg HBr_gp    HBr_tav
    timavg Bry_gp    Bry_tav
    timavg CH3Br_gp  CH3Br_tav
    timavg CHBr3_gp  CHBr3_tav
    timavg CH2Br2_gp CH2Br2_tav
    timavg Brz_gp    Brz_tav
#
    if [ "$gesave" = on ] ; then
      timavg Br_ge     Br_etav
      timavg BrO_ge    BrO_etav
      timavg HOBr_ge   HOBr_etav
      timavg BrCl_ge   BrCl_etav
      timavg BrOx_ge   BrOx_etav
      timavg BrONO2_ge BrONO2_etav
      timavg BX_ge     BX_etav
      timavg HBr_ge    HBr_etav
      timavg Bry_ge    Bry_etav
      timavg CH3Br_ge  CH3Br_etav
      timavg CHBr3_ge  CHBr3_etav
      timavg CH2Br2_ge CH2Br2_etav
      timavg Brz_ge    Brz_etav
    fi
#
# -- monthly zonal averages
    zonavg Br_tav     Br_ztav
    zonavg BrO_tav    BrO_ztav
    zonavg HOBr_tav   HOBr_ztav
    zonavg BrCl_tav   BrCl_ztav
    zonavg BrOx_tav   BrOx_ztav
    zonavg BrONO2_tav BrONO2_ztav
    zonavg BX_tav     BX_ztav
    zonavg HBr_tav    HBr_ztav
    zonavg Bry_tav    Bry_ztav
    zonavg CH3Br_tav  CH3Br_ztav
    zonavg CHBr3_tav  CHBr3_ztav
    zonavg CH2Br2_tav CH2Br2_ztav
    zonavg Brz_tav    Brz_ztav
#
#  ----  save instantaneous fields
#
#     ie - instantaneous data on model levels
#     ip - instantaneous data on constant pressure surfaces
#
    if [ "$iesave" = "on" ] ; then
      echo "XSAVE.        BR
NEWNAM.
XSAVE.        BRO
NEWNAM.
XSAVE.        HOBR
NEWNAM.
XSAVE.        BRCL
NEWNAM.
XSAVE.        BRONO2
NEWNAM.
XSAVE.        HBR
NEWNAM.
XSAVE.        BRY - BR BRO HOBR BRCL BRONO2 HBR
NEWNAM.     BRY
XSAVE.        CH3BR
NEWNAM.
XSAVE.        CHBR3
NEWNAM.
XSAVE.        CH2BR2
NEWNAM." > .iesave_input_card

      if [ "$ieslct" = "on" ] ; then
        echo "C*TSTEP   $deltmin     ${yearoff}010100          MODEL     YYYYMMDDHH" > ic.tstep_model
        selstep     Br_ge     Br_ie input=ic.tstep_model
        selstep    BrO_ge    BrO_ie input=ic.tstep_model
        selstep   HOBr_ge   HOBr_ie input=ic.tstep_model
        selstep   BrCl_ge   BrCl_ie input=ic.tstep_model
        selstep BrONO2_ge BrONO2_ie input=ic.tstep_model
        selstep    HBr_ge    HBr_ie input=ic.tstep_model
        selstep    Bry_ge    Bry_ie input=ic.tstep_model
        selstep  CH3Br_ge  CH3Br_ie input=ic.tstep_model
        selstep  CHBr3_ge  CHBr3_ie input=ic.tstep_model
        selstep CH2Br2_ge CH2Br2_ie input=ic.tstep_model
#
	release oldie
        access oldie ${flabel}ie      na
        xsave oldie Br_ie BrO_ie HOBr_ie BrCl_ie BrONO2_ie HBr_ie Bry_ie \
                      CH3Br_ie CHBr3_ie CH2Br2_ie newie input=.iesave_input_card
        save newie ${flabel}ie
        delete oldie                  na
        rm Br_ie BrO_ie HOBr_ie BrCl_ie BrONO2_ie HBr_ie Bry_ie
        rm CH3Br_ie CHBr3_ie CH2Br2_ie
      fi
#
      if [ "$ieslct" = "off" ] ; then
        release oldie
        access oldie ${flabel}ie      na
        xsave oldie Br_ge BrO_ge HOBr_ge BrCl_ge BrONO2_ge HBr_ge Bry_ge \
                      CH3Br_ge CHBr3_ge CH2Br2_ge newie input=.iesave_input_card
        save   newie ${flabel}ie
        delete oldie                  na
      fi
    fi
#
    if [ "$ipsave" = on ] ; then
      release oldip
      access oldip ${flabel}ip      na
      echo "XSAVE.        BR
NEWNAM.
XSAVE.        BRO
NEWNAM.
XSAVE.        HOBR
NEWNAM.
XSAVE.        BRCL
NEWNAM.
XSAVE.        BRX - BR BRO HOBR BRCL
NEWNAM.
XSAVE.        BRONO2
NEWNAM.
XSAVE.        BRX_2 - BR BRO HOBR BRCL BRONO2
NEWNAM.
XSAVE.        HBR
NEWNAM.
XSAVE.        BRY - BR BRO HOBR BRCL BRONO2 HBR
NEWNAM.     BRY
XSAVE.        CH3BR
NEWNAM.
XSAVE.        CHBR3
NEWNAM.
XSAVE.        CH2BR2
NEWNAM.
XSAVE.        BR_TOT - BRY CH3BR 3XCHBR3 2XCH2BR2
NEWNAM.     BRZ" | ccc xsave oldip Br_gp BrO_gp HOBr_gp BrCl_gp BrOx_gp BrONO2_gp \
                     BX_gp HBr_gp Bry_gp CH3Br_gp CHBr3_gp CH2Br2_gp Brz_gp newip
      save newip ${flabel}ip
      delete oldip                  na
    fi
#
    if [ "$dailysv" = on ] ; then
      echo "SELECT.   STEPS 000000000 999999999 $dyfrq LEVS-9001 1000 NAME   BO" | 
               ccc select BrO_gp BrO_dly_gp
      zonavg BrO_dly_gp BrO_ixp
#
      release oldix
      access oldix ${flabel}ix      na
      echo "XSAVE.        INSTANTANEOUS BRO
NEWNAM." | ccc xsave oldix BrO_ixp newix
      save   newix ${flabel}ix
      delete oldix                  na
    fi
#
    rm *_gp
    rm *_ge
#
#  ----  save time averaged fields
#
#     xp - time and zonal averaged data on pressure levels
#     gp - time averaged data on pressure levels
#     ge - time averaged data on model levels
#
    release oldxp
    access oldxp ${flabel}xp       na
    echo "XSAVE.        BR
NEWNAM.
XSAVE.        BRO
NEWNAM.
XSAVE.        HOBR
NEWNAM.
XSAVE.        BRCL
NEWNAM.
XSAVE.        BRX - BR BRO HOBR BRCL
NEWNAM.
XSAVE.        BRONO2
NEWNAM.
XSAVE.        BRX_2 - BR BRO HOBR BRCL BRONO2
NEWNAM.
XSAVE.        HBR
NEWNAM.
XSAVE.        BRY - BR BRO HOBR BRCL BRONO2 HBR
NEWNAM.     BRY
XSAVE.        CH3BR
NEWNAM.
XSAVE.        CHBR3
NEWNAM.
XSAVE.        CH2BR2
NEWNAM.
XSAVE.        BR_TOT - BRY CH3BR 3XCHBR3 2XCH2BR2
NEWNAM.     BRZ" | ccc xsave oldxp Br_ztav BrO_ztav HOBr_ztav BrCl_ztav BrOx_ztav \
                     BrONO2_ztav BX_ztav HBr_ztav Bry_ztav CH3Br_ztav CHBr3_ztav \
                     CH2Br2_ztav Brz_ztav newxp
    save newxp ${flabel}xp
    delete oldxp                   na
#
    release oldgp
    access oldgp ${flabel}gp       na
    echo "XSAVE.        BR
NEWNAM.
XSAVE.        BRO
NEWNAM.
XSAVE.        HOBR
NEWNAM.
XSAVE.        BRCL
NEWNAM.
XSAVE.        BRX - BR BRO HOBR BRCL
NEWNAM.
XSAVE.        BRONO2
NEWNAM.
XSAVE.        BRX_2 - BR BRO HOBR BRCL BRONO2
NEWNAM.
XSAVE.        HBR
NEWNAM.
XSAVE.        BRY - BR BRO HOBR BRCL BRONO2 HBR
NEWNAM.     BRY
XSAVE.        CH3BR
NEWNAM.
XSAVE.        CHBR3
NEWNAM.
XSAVE.        CH2BR2
NEWNAM.
XSAVE.        BR_TOT - BRY CH3BR 3XCHBR3 2XCH2BR2
NEWNAM.     BRZ" | ccc xsave oldgp Br_tav BrO_tav HOBr_tav BrCl_tav BrOx_tav \
                         BrONO2_tav BX_tav HBr_tav Bry_tav CH3Br_tav CHBr3_tav \
                         CH2Br2_tav Brz_tav newgp
    save newgp ${flabel}gp
    delete oldgp                   na
    rm newgp *_tav
#
    if [ "$gesave" = on ] ; then
      release oldge
      access oldge ${flabel}ge     na
      echo "XSAVE.        BR
NEWNAM.
XSAVE.        BRO
NEWNAM.
XSAVE.        HOBR
NEWNAM.
XSAVE.        BRCL
NEWNAM.
XSAVE.        BRX - BR BRO HOBR BRCL
NEWNAM.
XSAVE.        BRONO2
NEWNAM.
XSAVE.        BRX_2 - BR BRO HOBR BRCL BRONO2
NEWNAM.
XSAVE.        HBR
NEWNAM.
XSAVE.        BRY - BR BRO HOBR BRCL BRONO2 HBR
NEWNAM.     BRY
XSAVE.        CH3BR
NEWNAM.
XSAVE.        CHBR3
NEWNAM.
XSAVE.        CH2BR2
NEWNAM.
XSAVE.        BR_TOT - BRY CH3BR 3XCHBR3 2XCH2BR2
NEWNAM.     BRZ" | ccc xsave oldge Br_etav BrO_etav HOBr_etav BrCl_etav BrOx_etav \
                     BrONO2_etav BX_etav HBr_etav Bry_etav CH3Br_etav CHBr3_etav \
                     CH2Br2_etav Brz_etav newge
      save newge ${flabel}ge
      delete oldge                 na
      rm newge *_etav
    fi
