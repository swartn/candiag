#!/bin/sh
#
# This script is only included in the merged_diag_script grouping when
# gas-phase chemistry is run in MAM mode. BW, June 2023
#
# Amendments to the original:  define the station list
#
#  DESCRIPTION
#
#  Pulls out vertical profiles of instantaneous values of a field
#  at a pre-defined set of station locations from the 3-D model
#  concentration fields
#
#  PARMSUB PARAMETERS
#
#    -- stime, memory1, model1, flabel, lat, lon, pmax, pmin,
#       t1, t2, t3, t4, sonde_stlist, coord, plid, delt
#
#  PREREQUISITES
#
#    -- SS, GS history files
#
#  CHANGELOG
#
#    2023-11-23: Save output as individual months to avoid problems when
#                 diagnostics are done in parallel (D. Plummer)
#    2023-01-30: Converted to shell script and removed the logic to support
#                 running chemistry on a subset of model levels (D. Plummer)
#    2008-05-30: Version one modified from earlier stand-alone scripts
# -----------------------------------------------------------------------------
#  ---- access the list of station locations
sonde_stlist="/home/rdp001/cmam/diags/stations/o3sonde_stations_v2b"
ln -s ${sonde_stlist} stations1
#
#  ---- access model history files
#
.   spfiles.cdk
.   ggfiles.cdk
#
#  ---- get the surface pressure field
#
access gslnsp ${flabel}_gslnsp    na
if [ ! -s gslnsp ] ; then
  if [ "$datatype" = "gridsig" ] ; then
    echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME LNSP" | ccc select npakgg gslnsp
  else
    echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME LNSP" | ccc select npaksp sslnsp
    echo "COFAGG    $lon$lat    0    0" | ccc cofagg sslnsp gslnsp
    rm sslnsp
  fi
fi
expone gslnsp sfprx
echo "XLIN.          100.0       0.0 SFPR" | ccc xlin sfprx sfprx1
rm gslnsp sfprx
#
#  ------- retrieve grid-point temperature field
#
access gstemp ${flabel}_get
if [ ! -s gstemp ] ; then
  if [ "$gcmtsav" = on ] ; then
    echo "SELECT    STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME TEMP" | ccc select npaksp sstemp
  else
    echo "SELECT    STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME PHIS" | ccc select npaksp ssphis
    echo "SELECT    STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME  PHI" | ccc select npaksp ssphi
    ctemps ssphis ssphi sstemp
    rm ssphis ssphi
  fi
  echo "COFAGG.   $lon$lat    0    0" | ccc cofagg sstemp gstemp
  rm sstemp
fi
#
#  ------- retrieve the grid-point ozone field
#
echo "SELECT    STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME   O3" | ccc   select npakgg O3_ge
#
#    --------- pull data at selected stations
#
echo "PULLSONDE  1$coord$plid $delt" | ccc pull_sondes_v2 O3_ge gstemp sfprx1 stations1 txtout1
#
# -- there should not be a pre-existing output file for this month
access oldgp o3sondes_st_${runid}_${year}_${mon} na
delete oldgp   na
save txtout1 o3sondes_st_${runid}_${year}_${mon}
rm -f O3_ge gstemp stations1 txtout1
