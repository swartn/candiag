#!/bin/sh
#
# Adjust for "SDCB" change to "CSCB".
#                  xtrconv             KS - Jan 15/2007 - ML. ------xtrconv
#   ---------------------------------- Compute diagnostic results for
#                                      convection.
#
# -------------------------------------------------------------------------
#
.   ggfiles.cdk
#
#  ---------------------------------- select input fields.
#
echo "SELECT          $r1 $r2 $r3         1    1       BCD  BCS  TCD  TCS
SELECT     PCPS SCMT DCMT ACMT CAPE CINH CDCB CSCB" | ccc select npakgg BCD BCS TCD TCS  \
                  PCPS SCMT DCMT ACMT \
                  CAPE CINH CDCB CSCB
echo "SELECT          $r1 $r2 $r3     -9001 1000      SHRD SHRS LHRD LHRS
SELECT      DMC  SMC" | ccc select npakgg SHRD SHRS LHRD LHRS \
                  DMC SMC
#
#  ----------------------------------- compute and save statistics.
#
    statsav BCD  BCS  TCD  TCS  PCPS SCMT DCMT ACMT CAPE CINH CDCB \
            CSCB SHRD SHRS LHRD LHRS DMC  SMC \
            new_gp new_xp $stat2nd
#
    rm      BCD  BCS  TCD  TCS  PCPS SCMT DCMT ACMT CAPE CINH CDCB \
            CSCB SHRD SHRS LHRD LHRS DMC  SMC

#  ----------------------------------- save results.
      release oldgp
      access  oldgp ${flabel}gp
      xjoin   oldgp new_gp newgp
      save    newgp ${flabel}gp
      delete  oldgp

      if [ "$rcm" != "on" ] ; then
      release oldxp
      access  oldxp ${flabel}xp
      xjoin   oldxp new_xp newxp
      save    newxp ${flabel}xp
      delete  oldxp
      fi
