#!/bin/bash
# Convert canesm timeseries data into 'cmorized' netcdf data
#
#   TODO: assess the parallelization across tables.. this implementation is a hack at best
#           - one thing to consider is having parallelization WITHIN pycmor itself, instead of
#               Clint's gobbly implementation below
#==================================================
# setup working directory, and setup conversion env
#==================================================
  CONV_DIR=$(pwd)
  source activate ${pycmor_env}
  export NCCONV_DIR=${CANESM_SRC_ROOT}/CCCma_tools/data_conv/ncconv
  export PATH=${NCCONV_DIR}/bin:${NCCONV_DIR}/pycmor:${PATH}

  # source additional pycmor settings (version strings/table lists)
  . ${NCCONV_DIR}/pycmor/pycmor.cfg
  output_dir=${RUNPATH}/nc_output
  tmp_nc_dir=output/CMIP6
  log_tar_file="${output_dir}/conv_logs/${chunk}.tar.gz"
  log_storage_dir=$(dirname $log_tar_file)

  # get run tables
  assumed_experiment_table_dir="cmip6_experiments"
  run_experiment_table_dir=$(dirname $ncconv_exptab)
  cp -r ${WRK_DIR}/config/netcdf_conv projects
  cp -r ${run_experiment_table_dir} $assumed_experiment_table_dir

#=================
#   run pycmor
#=================
  # check if user provided a list of tables to convert
  if [[ -n $ncconv_tabs2conv ]]; then
    # use user defined list
    tabs2conv="$ncconv_tabs2conv"
  else
    # use default cmorlist from pycmor.cfg
    tabs2conv="$cmorlist"
  fi

  # build argument list
  if (( PRODUCTION == 1 )) ; then
    pycmor_opts="--prod"
  fi
  pycmor_opts="${pycmor_opts} -L -r $runid"
  pycmor_opts="${pycmor_opts} -p ${ncconv_conv_project} -k ${chunk}"
  pycmor_opts="${pycmor_opts} -v ${version} -n ${newversion}"

  # check if we want to run the conversion in parallel groups
  if [[ "$tabs2conv" == *"|"* ]]; then
    # create an array for each group
    readarray -d "|" -t table_groups <<< "$tabs2conv"

    # we need to run pycmor in temporary work spaces so the parallel processes
    #   don't interfere with each other. Here we define the basename for these dirs
    work_space_dirs="tmp_pycmor_work"

    # loop over each group, and launch pycmor
    for (( n=0; n < ${#table_groups[*]}; n++)); do
        # trim leading/trailing whitespace from table group list with 'xargs'
        table_group=$(echo ${table_groups[n]} | xargs)

        # prior to launching, create a workable subdirectory
        work_space=${CONV_DIR}/${work_space_dirs}_${n}
        mkdir $work_space
        (   cd $work_space;                  \
            cp -r ${CONV_DIR}/projects . ;   \
            cp -r ${CONV_DIR}/${assumed_experiment_table_dir} $assumed_experiment_table_dir ;   \
            time pycmor.py $pycmor_opts -t "${table_group}" >> conv.log${n} 2>&1 ) &
    done

    # wait for completion
    wait

    # link/move log files and output into parent directory
    #   - we use hard links for the directories to avoid copying a copious amount of files
    dirs2sync="pycmor_logs cmor_logs output"
    for d in $dirs2sync; do
        for work_space_dir in $(ls -d ${work_space_dirs}*/${d}); do
            rsync -ax --link-dest=$(pwd)/${work_space_dir}/ $(pwd)/${work_space_dir}/ $(pwd)/${d}/
        done
    done
    mv ${work_space_dirs}*/*.log* .
  else
    # run all groups together
    time pycmor.py $pycmor_opts -t "$tabs2conv" >> conv.log 2>&1
  fi

  # check how comprehensive the conversion was 
  log_parse -c >> comprehensive_conv.log 2>&1
#====================
#   handle output
#====================

  # make sure the storage location exists
  [[ -d $log_storage_dir ]] || mkdir -p $log_storage_dir

  # bundle up log files and store them
  dir_to_be_tarrd=$(basename $log_tar_file)
  dir_to_be_tarrd=${dir_to_be_tarrd%%.*}
  mkdir $dir_to_be_tarrd

  #-- Driver logs (one for each variable table)
  if compgen -G "pycmor-driver-log_*" > /dev/null; then
    #-- only store if any were produced
    mv pycmor-driver-log_* $dir_to_be_tarrd
  else
    echo "No pycmor driver logs produced"
  fi

  #-- other single log files/direcotires
  tmp_file_list="comprehensive_conv.log conv.log* cmor_logs pycmor_logs"
  for f in $tmp_file_list; do 
    if [[ -f $f ]] || [[ -d $f ]]; then
      mv $f $dir_to_be_tarrd
    else
      echo "No $f produced"
    fi
  done

  #-- clean old tar file
  [[ -f $log_tar_file ]] && rm -f $log_tar_file

  #-- create tar file
  tar -czvf $log_tar_file $dir_to_be_tarrd

  # store netcdf files if any were produced, bail if not
  if [[ -d "$tmp_nc_dir" ]] ; then
    # loop over rsync calls until the files are successfully transfered
    max_retries=10
    rsync_status=1
    count=0
    while [[ $rsync_status -ne 0 ]] ; do
        if (( count == max_retries )); then
            bail_msg="the ncconv job failed to transfer all output .nc files to $output_dir after $max_retries"
            bail_msg+=" retries of the rsync command! Why is it failing??"
            bail $bail_msg
        fi
        rsync -av $(dirname $tmp_nc_dir)/ $output_dir
        rsync_status=$?
        count=$((count+1))
        sleep 30
    done
  else
    bail_msg="pycmor failed to produce ANY output! Are you sure the timeseries files are on disk?"
    bail_msg="${bail_msg} See the chunk's ($chunk) conversion logs at ${log_storage_dir} for additional information."
    bail $bail_msg
  fi
