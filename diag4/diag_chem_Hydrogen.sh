#!/bin/sh
#
# This script is only included in the merged_diag_script grouping when
# gas-phase chemistry is run in MAM mode. BW, June 2023
#
# Amendments to the original: source the file of switches
#
#  DESCRIPTION
# 
#  Create monthly averages and monthly zonal averages for hydrogen
#  species: H, OH, HO2, H2O2, H2, H2O and the advected family HX (H +
#  OH + HO2 + 2*H2O2); and diagnosed families: HOx (OH + HO2), total
#  water (2*CH4 + H2O) and total hydrogen (2*CH4 + H2O + H2 +
#  (1/2)*HOx).
#
#  H2O is diagnosed in its total amount and in its gas phase and
#  condensed phase parts, H2O(g) and H2O(s) respectively.
#
#  Zonal averages include daytime and nighttime masked values for H,
#  OH and HO2.
#
#  PARMSUB PARAMETERS
#
#    -- chem, coord, days, dnsave, flabel, lat, lon, memory1, model1,
#       npg, plid, plv, plv2, pmax, pmaxc, pmin, run, stime, t1, t2,
#       t3, sref, spow.
#    -- p01-p100 (pressure level set)
#
#  PREREQUISITES
#
#    -- GS and SS history files
#    -- DAYMASK deck (GPDAY data file, unaveraged)
#    -- NUMBERDENSITY deck (GEND data file, unaveraged)
#    -- PRESSURE_DENSITY deck for unaverage specific humidity (_gsshum)
#
#  CHANGELOG
#
#    2023-10-27: Calculation of variables required by selstep and daily save 
#                 from available CanESM5 variables
#    2023-01-16: Converted to shell script, removed ztsave and the logic
#                 to support running chemistry on a subset of model levels
#                 controlled by the trchem switch (D. Plummer)
#    2015-04-20: Use standard model water in place of T6 tracer for
#                 total water - requires sref, spow for chemistry decks
#    2014-06-06: Monthly-average output on model surfaces for CCMI and 
#                 harmonization of save options iesave, ipsave, gesave
#                 (D. Plummer)
#    2013-06-10: Add iesave option (Y. Jiao)
#    2011-11-29: Modified xsave labels to be friendlier when converted to
#                timeseries filenames by spltdiag (D. Plummer)
#    2009-06-22: Save instantaneous H2O -M. Neish
#    2008-06-25: Added diagnostics for HOx (OH + HO2); Added option to
#                account for having chemical species on all model
#                levels (A.Jonsson)
#    2008-04-24: Added option to account for having chemical species on
#                all model levels (D. Plummer)
#    2007-12-17: Moved call to access gpday to within $dnsave if-statement
#                (A.Jonsson)
#    2007-11-23: Capitalized all super labels (affecting H20(g),
#                H2O(s), HNO3(g), HNO3(s) and labels containing Br,
#                Cl, x, y or z) (A.Jonsson)
#    2007-11-23: Changed super label unit from "[MOL/M3]" to "[CM-3]"
#                (A.Jonsson)
#    2007-11-18: Bug fix: Added $dnsave if-statement for daymask
#                select input card; Bug fix: Added CH4 extraction for
#                total water derivation (A.Jonsson)
#    2007-11-15: Added subversion date ('Date') and revision ('Rev')
#                keyword substitutions to all decks (A.Jonsson)
#    2007-11-15: Added parmsub parameter $dnsave to control day/night
#                time zonal averaging; Bugfix: added input cards for
#                HY and WY in Hydrogen deck (A.Jonsson)
#    2007-11-15: Renamed VMR and ND output of H2O(s) and HNO3(s) to
#                AH2O and AHNO (A.Jonsson)
#    2007-11-13: Revised $ztsave if-statement to only encompass xp
#                file data (A.Jonsson)
#    2007-11-13: Added 'cat Input_cards' at the end of most decks
#                (A.Jonsson)
#    2007-11-13: Added total water diagnostics to Hydrogen deck
#                (A.Jonsson)
#    2007-11-13: Replaced hard coded level specification for upper
#                limit of "tropospheric" domain by $pmint (A.Jonsson)
#    2007-11-12: Bug fix: Switched upper limit from $pmin to $p01 in
#                input card for daymask selection; Extended H2O(g)
#                diagnostic to surface (A.Jonsson)
#    2007-11-05: Added Hydrogen deck (A.Jonsson)
#    2007-11-??: Merged the H2O, H2, HOx and H2Otot decks (A.Jonsson)
# -----------------------------------------------------------------------------

# Source the list of on/off switches
source ${CANESM_SRC_ROOT}/CanDIAG/diag4/gaschem_diag_switches.sh

#
#  ---- variables for selstep
    yearoff=`echo "$year_offset"  | $AWK '{printf "%4d", $0+1}'`
    deltmin=`echo "$delt"         | $AWK '{printf "%5d", $0/60.}'`
#
#  ---- number of timesteps in one day used for selection of once daily output
    dyfrq=`echo $delt | awk '{printf "%04d",86400/$1}'`
#
#  ----  access model history and pre-generated files
#
.   ggfiles.cdk
.   spfiles.cdk
    access nd_ge  ${flabel}_gend
#
#  ----  get the surface pressure field
#
    access gslnsp ${flabel}_gslnsp na
    if [ ! -s gslnsp ] ; then
      if [ "$datatype" = "gridsig" ] ; then
        echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME LNSP" |
           ccc select npakgg gslnsp
      else
        echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME LNSP" |
           ccc select npaksp sslnsp
        echo "COFAGG.   $lon$lat" | ccc cofagg sslnsp gslnsp
        rm sslnsp
      fi
    fi
#
#  ----  calculate water from the standard GCM water variable
#
    if [ "$gcmtsav" = on ] ; then
      echo "SELECT    STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME TEMP" |
        ccc select npaksp sstemp
    else
      echo "SELECT.   STEPS $t1 $t2 $t3 LEVS    1    1 NAME PHIS" |
        ccc select npaksp ssphis
      echo "SELECT          $t1 $t2 $t3 LEVS-9001 1000       PHI" |
        ccc select npaksp ssphi
      ctemps ssphis ssphi sstemp
    fi
    echo "COFAGG.   $lon$lat    0$npg" | ccc cofagg sstemp gstemp
#
    if [ "$moist" = " SL3D" -o "$moist" = " SLQB" ] ; then
      echo "SELECT.   STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME   ES" |
        ccc select npakgg gses
    else
      echo "SELECT.   STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME   ES" |
        ccc select npaksp sses
      echo "COFAGG.   $lon$lat    0$npg" | ccc cofagg sses gses
      rm sses
    fi
    echo "GSHUMH.   $coord$moist$plid$sref$spow" |
       ccc gshumh gses gstemp gslnsp gsshum gsrhum
    rm -f ssphis ssphi sstemp gses gstemp gsrhum
#
# --- convert from specific humidty to volume mixing ratio
#   ---- mwrt holds the ratio of the molecular weight of dry air
#          to that of water
    echo "XLIN ONE       0.00       1.00" | ccc xlin gsshum one
    echo "XLIN MWRT      0.00   1.609111" | ccc xlin gsshum mwrt
    sub one gsshum wrk1
    div gsshum wrk1 wrk2
    mlt wrk2 mwrt wrk3
    echo "NEWNAM.      T6" | ccc newnam wrk3 H2O_ge
    rm gsshum one mwrt wrk1 wrk2 wrk3
#
#  ----  select out chemical fields
#
    echo "SELECT    STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME   H0   H1   H2   HH
SELECT     SH2O" | ccc   select npakgg H_ge OH_ge HO2_ge H2O2_ge fr_sH2O_ge
    echo "SELECT    STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME   HX   HD   MM" |
      ccc select npaksp HX_se H2_se CH4_se
    echo "COFAGGxxx $lon$lat    0$npg" | ccc cofagg HX_se  HX_ge
    echo "COFAGGxxx $lon$lat    0$npg" | ccc cofagg H2_se  H2_ge
    echo "COFAGGxxx $lon$lat    0$npg" | ccc cofagg CH4_se CH4_ge
#
    rm *_se npaksp
#
#  ----  create new variables
#
# --- separate total water (H2O(g)+H2O(s)) into its components
    mlt fr_sH2O_ge  H2O_ge sH2O_ge
    sub     H2O_ge sH2O_ge new1
    echo "NEWNAM.    GH2O" | ccc newnam new1 gH2O_ge
    rm new1
#
# --- create HOx (OH+HO2)
    add OH_ge HO2_ge HOx_ge
#
# --- create total water (WY) and total hydrogen (HY)
    echo "XLIN......         2        0." | ccc xlin CH4_ge aCH4_ge
    echo "XLIN......       0.5        0." | ccc xlin HX_ge   bHX_ge
    add  aCH4_ge H2O_ge  WY_ge
    add  WY_ge   H2_ge   bwrk
    add  bwrk    bHX_ge  HY_ge
    rm aCH4_ge bHX_ge bwrk
#
#  ----  convert units of vmr to number density
#
#     nd_air    = rho * Na/Ma
#     nd_tracer = vmr_tracer * nd_air
#   
#     nd  - number density [molecules/m3]
#     rho - mass density [kg/m3]
#     Na  - Avogadro constant [molecules/mol]
#     Ma  - molecular mass [kg/mol]
#
    echo "SELECT    STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME   ND" |
      ccc select nd_ge nd_c_ge
    mlt sH2O_ge nd_c_ge sH2O_nd_ge
    rm nd_* npakgg
#
#  ----  interpolate onto constant pressure surfaces
#
    echo "GSAPL.    $plv       0.0       0.0$coord$plid
$p01$p02$p03$p04$p05$p06$p07$p08$p09$p10$p11$p12$p13$p14$p15$p16
$p17$p18$p19$p20$p21$p22$p23$p24$p25$p26$p27$p28$p29$p30$p31$p32
$p33$p34$p35$p36$p37$p38$p39$p40$p41$p42$p43$p44$p45$p46$p47$p48
$p49$p50$p51$p52$p53$p54$p55$p56$p57$p58$p59$p60$p61$p62$p63$p64
$p65$p66$p67$p68$p69$p70$p71$p72$p73$p74$p75$p76$p77$p78$p79$p80
$p81$p82$p83$p84$p85$p86$p87$p88$p89$p90$p91$p92$p93$p94$p95$p96
$p97$p98$p99$p100" > .gsapl_input_card
    
    gsapl H_ge       gslnsp H_gp input=.gsapl_input_card
    gsapl OH_ge      gslnsp OH_gp input=.gsapl_input_card
    gsapl HO2_ge     gslnsp HO2_gp input=.gsapl_input_card
    gsapl HOx_ge     gslnsp HOx_gp input=.gsapl_input_card
    gsapl H2O2_ge    gslnsp H2O2_gp input=.gsapl_input_card
    gsapl HX_ge      gslnsp HX_gp input=.gsapl_input_card
    gsapl H2_ge      gslnsp H2_gp input=.gsapl_input_card
    gsapl H2O_ge     gslnsp H2O_gp input=.gsapl_input_card
    gsapl gH2O_ge    gslnsp gH2O_gp input=.gsapl_input_card
    gsapl sH2O_ge    gslnsp sH2O_gp input=.gsapl_input_card
    gsapl sH2O_nd_ge gslnsp sH2O_nd_gp input=.gsapl_input_card
    gsapl WY_ge      gslnsp WY_gp input=.gsapl_input_card
    gsapl HY_ge      gslnsp HY_gp input=.gsapl_input_card
#
#  ----  create monthly and zonal averages
#
# -- monthly averages
    timavg H_gp       H_tav
    timavg OH_gp      OH_tav
    timavg HO2_gp     HO2_tav
    timavg HOx_gp     HOx_tav
    timavg H2O2_gp    H2O2_tav
    timavg HX_gp      HX_tav
    timavg H2_gp      H2_tav
    timavg H2O_gp     H2O_tav
    timavg gH2O_gp    gH2O_tav
    timavg sH2O_gp    sH2O_tav
    timavg sH2O_nd_gp sH2O_nd_tav
    timavg WY_gp      WY_tav
    timavg HY_gp      HY_tav
#
# -- monthly averages on model levels
    if [ "$gesave" = on ] ; then
      timavg H_ge       H_etav
      timavg OH_ge      OH_etav
      timavg HO2_ge     HO2_etav
      timavg HOx_ge     HOx_etav
      timavg H2O2_ge    H2O2_etav
      timavg H2_ge      H2_etav
      timavg H2O_ge     H2O_etav
      timavg gH2O_ge    gH2O_etav
      timavg sH2O_ge    sH2O_etav
      timavg sH2O_nd_ge sH2O_nd_etav
    fi
#
# -- monthly zonal averages
    zonavg H_tav       H_ztav
    zonavg OH_tav      OH_ztav
    zonavg HO2_tav     HO2_ztav
    zonavg HOx_tav     HOx_ztav
    zonavg H2O2_tav    H2O2_ztav
    zonavg HX_tav      HX_ztav
    zonavg H2_tav      H2_ztav
    zonavg H2O_tav     H2O_ztav
    zonavg gH2O_tav    gH2O_ztav
    zonavg sH2O_tav    sH2O_ztav
    zonavg sH2O_nd_tav sH2O_nd_ztav
    zonavg WY_tav      WY_ztav
    zonavg HY_tav      HY_ztav
#
# -- calculate daytime and nighttime averages
    if [ "$dnsave" = on ] ; then
      access day_gp ${flabel}_gpday 
      echo "SELECT    STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME DMSK" |
        ccc select day_gp day
      rzonavg3 H_gp   day H_z   H_d   H_n
      rzonavg3 OH_gp  day OH_z  OH_d  OH_n
      rzonavg3 HO2_gp day HO2_z HO2_d HO2_n
#
#       Monthly zonal/day/night averages 
#
      timavgmsk H_d H_dztav
      timavgmsk H_n H_nztav
#
      timavgmsk OH_d OH_dztav
      timavgmsk OH_n OH_nztav
#
      timavgmsk HO2_d HO2_dztav
      timavgmsk HO2_n HO2_nztav
#
      rm day* *_z *_d *_n
    fi
#
#  ----  save instantaneous fields
#
#     ie - instantaneous data on model levels
#     ip - instantaneous data on constant pressure surfaces
#     ix - instanteneous zonal average cross-sections
#
    if [ "$iesave" = "on" ] ; then
      if [ "$ieslct" = "on" ] ; then
        echo "C*TSTEP   $deltmin     ${yearoff}010100          MODEL     YYYYMMDDHH" > ic.tstep_model
        selstep   OH_ge   OH_ie input=ic.tstep_model
        selstep  HO2_ge  HO2_ie input=ic.tstep_model
        selstep H2O2_ge H2O2_ie input=ic.tstep_model
        selstep   H2_ge   H2_ie input=ic.tstep_model
        selstep  H2O_ge  H2O_ie input=ic.tstep_model
        selstep gH2O_ge gH2O_ie input=ic.tstep_model
        selstep sH2O_ge sH2O_ie input=ic.tstep_model
#
        release oldie
        access oldie ${flabel}ie      na
        echo "XSAVE.        OH
NEWNAM.
XSAVE.        HO2
NEWNAM.
XSAVE.        H2O2
NEWNAM.
XSAVE.        H2
NEWNAM.
XSAVE.        H2O
NEWNAM.
XSAVE.        H2O_G
NEWNAM.    GH2O
XSAVE.        H2O_S
NEWNAM.    AH2O" | ccc xsave oldie OH_ie HO2_ie H2O2_ie H2_ie H2O_ie \
                         gH2O_ie sH2O_ie newie
        save   newie  ${flabel}ie
        delete oldie                  na
        rm *_ie
      fi
#
      if [ "$ieslct" = "off" ] ; then
        release oldie
        access oldie ${flabel}ie      na
        echo "XSAVE.        OH
NEWNAM.
XSAVE.        HO2
NEWNAM.
XSAVE.        H2O2
NEWNAM.
XSAVE.        H2
NEWNAM.
XSAVE.        H2O
NEWNAM.
XSAVE.        H2O_G
NEWNAM.    GH2O
XSAVE.        H2O_S
NEWNAM.    AH2O" | ccc xsave oldie OH_ge HO2_ge H2O2_ge H2_ge H2O_ge \
                         gH2O_ge sH2O_ge newie
        save   newie  ${flabel}ie
        delete oldie                  na
      fi
    fi
#
    if [ "$ipsave" = "on" ] ; then
      release oldip
      access oldip ${flabel}ip      na
      echo "XSAVE.        ATOMIC H
NEWNAM.
XSAVE.        OH
NEWNAM.
XSAVE.        HO2
NEWNAM.
XSAVE.        HOX - OH HO2
NEWNAM.     HOX
XSAVE.        H2O2
NEWNAM.
XSAVE.        H2
NEWNAM.
XSAVE.        H2O
NEWNAM.
XSAVE.        H2O_G
NEWNAM.    GH2O
XSAVE.        H2O_S
NEWNAM.    AH2O
XSAVE.        H2O_S NUMDEN - 1 OVER CM3
NEWNAM.    AH2O" | ccc xsave oldip H_gp OH_gp HO2_gp HOx_gp H2O2_gp H2_gp \
                        H2O_gp gH2O_gp sH2O_gp sH2O_nd_gp newip
      save   newip ${flabel}ip
      delete oldip                  na
    fi
#
    if [ "$dailysv" = on ] ; then
      echo "SELECT.   STEPS 000000000 999999999 $dyfrq LEVS-9001 1000 NAME GH2O" |
         ccc select gH2O_gp gH2O_dly_gp
      zonavg gH2O_dly_gp gH2O_ixp
#
      release oldix
      access oldix ${flabel}ix       na
      echo "XSAVE.        INSTANTANEOUS H2O_G
NEWNAM." | ccc xsave oldix gH2O_ixp newix
      save   newix ${flabel}ix
      delete oldix                   na
    fi
#
    rm *_gp
    rm *_ge  
#
#  ----  save time averaged fields
#
#     xp - time and zonal averaged data on pressure levels
#     gp - time averaged data on pressure levels
#     ge - time averaged data on model eta levels
#
    echo "XSAVE.        ATOMIC H
NEWNAM.
XSAVE.        OH
NEWNAM.
XSAVE.        HO2
NEWNAM.
XSAVE.        HOX - OH HO2
NEWNAM.     HOX
XSAVE.        H2O2
NEWNAM.
XSAVE.        HOX_2 - H OH HO2 2XH2O2
NEWNAM.
XSAVE.        H2
NEWNAM.
XSAVE.        H2O
NEWNAM.
XSAVE.        H2O_G
NEWNAM.    GH2O
XSAVE.        H2O_S
NEWNAM.    AH2O
XSAVE.        H2O_S NUMDEN - 1 OVER CM3
NEWNAM.    AH2O
XSAVE.        H2O_TOT - 2XCH4 H2O
NEWNAM.      WY
XSAVE.        H2O_TOT_2 - 2XCH4 H2O H2 0.5HOX
NEWNAM.      HY" | ccc xsave oldxp H_ztav OH_ztav HO2_ztav HOx_ztav H2O2_ztav \
                      HX_ztav H2_ztav H2O_ztav gH2O_ztav sH2O_ztav sH2O_nd_ztav \
                      WY_ztav HY_ztav newxp
#
    if [ "$dnsave" = on ] ; then
      echo "XSAVE.        H DAY
NEWNAM.
XSAVE.        OH DAY
NEWNAM.
XSAVE.        HO2 DAY
NEWNAM.
XSAVE.        H NIGHT
NEWNAM.
XSAVE.        OH NIGHT
NEWNAM.
XSAVE.        HO2 NIGHT
NEWNAM." | ccc xsave newxp H_dztav OH_dztav HO2_dztav H_nztav OH_nztav HO2_nztav newxp1
      mv newxp1 newxp
    fi
#
    release old_xp
    access old_xp ${flabel}xp      na
    xjoin  old_xp newxp new_xp
    save   new_xp ${flabel}xp
    delete old_xp                  na
#
    release oldgp
    access oldgp ${flabel}gp       na
    echo "XSAVE.        ATOMIC H
NEWNAM.
XSAVE.        OH
NEWNAM.
XSAVE.        HO2
NEWNAM.
XSAVE.        HOX - OH HO2
NEWNAM.     HOX
XSAVE.        H2O2
NEWNAM.
XSAVE.        HOX_2 - H OH HO2 2XH2O2
NEWNAM.
XSAVE.        H2
NEWNAM.
XSAVE.        H2O
NEWNAM.
XSAVE.        H2O_G
NEWNAM.    GH2O
XSAVE.        H2O_S
NEWNAM.    AH2O
XSAVE.        H2O_S NUMDEN - 1 OVER CM3
NEWNAM.    AH2O
XSAVE.        H2O_TOT - 2XCH4 H2O
NEWNAM.      WY
XSAVE.        H2O_TOT_2 - 2XCH4 H2O H2 0.5HOX
NEWNAM.      HY" | ccc xsave oldgp H_tav OH_tav HO2_tav HOx_tav H2O2_tav HX_tav \
                     H2_tav H2O_tav gH2O_tav sH2O_tav sH2O_nd_tav WY_tav HY_tav newgp
    save newgp ${flabel}gp
    delete oldgp                    na
#
    if [ "$gesave" = on ] ; then
      release oldge
      access oldge ${flabel}ge     na
      echo "XSAVE.        ATOMIC H
NEWNAM.
XSAVE.        OH
NEWNAM.
XSAVE.        HO2
NEWNAM.
XSAVE.        HOX - OH HO2
NEWNAM.     HOX
XSAVE.        H2O2
NEWNAM.
XSAVE.        H2
NEWNAM.
XSAVE.        H2O
NEWNAM.
XSAVE.        H2O_G
NEWNAM.    GH2O
XSAVE.        H2O_S
NEWNAM.    AH2O
XSAVE.        H2O_S NUMDEN - 1 OVER CM3
NEWNAM.    AH2O" | ccc xsave oldge H_etav OH_etav HO2_etav HOx_etav H2O2_etav \
                     H2_etav H2O_etav gH2O_etav sH2O_etav sH2O_nd_etav newge
      save newge ${flabel}ge
      delete oldge                 na
      rm newge *_etav
    fi
