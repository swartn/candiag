#!/bin/sh
#
#                  cldmic2             KvS - Oct 3/2006
#   ---------------------------------- Microphysical sources and sinks.
#
#                                      jcl - Mar. 26, 2009
#                                      Added ice sedimentation
#
#                                      jcl - Feb. 16, 2012
#                                      Added cloud microphysical diagnostics
#                                      Consistent with "aerosol" table in
#                                      CMIP5 data request.
# -------------------------------------------------------------------------
#
.   ggfiles.cdk
#
#  ---------------------------------- select input fields.
#
echo "SELECT          $r1 $r2 $r3     -9001 1000       AGG  AUT  CND  DEP
SELECT      EVP  FRH  FRK  FRS MLTI MLTS RACL SACI SACL  SUB SEDI RAIN SNOW VAGG
           VAUT VCND VDEP VEVP VFRH VFRK VFRS VMLI VMLS VRCL VSCI VSCL VSUB VSED
           RLIQ RICE CLIQ CICE  LNC RELL RELI CLDL CLDI CTLN CLLN" | ccc select npakgg AGG AUT CND DEP  \
                  EVP FRH FRK FRS  \
                  MLTI MLTS RACL SACI \
                  SACL SUB SEDI RAIN SNOW \
                  VAGG VAUT VCND VDEP VEVP \
                  VFRH VFRK VFRS VMLI VMLS \
                  VRCL VSCI VSCL VSUB VSED \
                  RLIQ RICE CLIQ CICE LNC \
                  RELL RELI CLDL CLDI CTLN CLLN
#
#  ----------------------------------- compute and save statistics.
#
    statsav  AGG  AUT  CND  DEP  EVP  FRH  FRK  FRS MLTI MLTS RACL \
            SACI SACL  SUB SEDI RAIN SNOW \
            VAGG VAUT VCND VDEP VEVP VFRH VFRK VFRS \
            VMLI VMLS VRCL VSCI VSCL VSUB VSED \
            RLIQ RICE CLIQ CICE LNC \
            RELL RELI CLDL CLDI CTLN CLLN \
            new_gp new_xp $stat2nd
#
    rm       AGG  AUT  CND  DEP  EVP  FRH  FRK  FRS  MLTI MLTS RACL \
            SACI SACL  SUB SEDI VAGG VAUT VCND VDEP VEVP VFRH VFRK \
            VFRS VMLI VMLS VRCL VSCI VSCL VSUB VSED RAIN SNOW \
            RLIQ RICE CLIQ CICE LNC RELL RELI CLDL CLDI CTLN CLLN

#  ----------------------------------- save results.
    release oldgp
    access  oldgp ${flabel}gp
    xjoin   oldgp new_gp newgp
    save    newgp ${flabel}gp
    delete  oldgp

    if [ "$rcm" != "on" ] ; then
    release oldxp
    access  oldxp ${flabel}xp
    xjoin   oldxp new_xp newxp
    save    newxp ${flabel}xp
    delete  oldxp
    fi
