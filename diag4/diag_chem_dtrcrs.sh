#!/bin/sh
#
# This script is only included in the merged_diag_script grouping when
# gas-phase chemistry is run in MAM mode. BW, June 2023
#
# Amendments to original: source the file of switches
#
#  DESCRIPTION (original name: diag_chem_dtrcrs_961u.sh)
# 
#  Create monthly averages and monthly zonal averages for the
#    standard set of diagnostic tracers
#      --- NOT including the long-standing stratospheric age of 
#          air (X3) that is treated in the standard way
#
#    X1 - stratospheric ozone with photochemical decay in troposphere
#    X2 - e90 tracer with constant emissions globally, 90-day lifetime
#
#  PARMSUB PARAMETERS
#
#    -- crate, delt, chem, coord, flabel, lat, lon, memory1, model1,
#       npg, plid, plv, pmax, pmin, run, stime, t1, t2, t3, t4.
#    -- p01-p100 (pressure level set)
#
#  PREREQUISITES
#
#    -- SS history files
#
#  CHANGELOG
#
#    2023-10-27: Calculation of variables required by selstep and daily save 
#                 from available CanESM5 variables
#    2023-01-25: Converted to shell script, removed ztsave and the logic
#                 to support running chemistry on a subset of model levels
#                 controlled by the trchem switch (D. Plummer)
#    2015-03-19: Created from standard strat age-of-air 
#                diagnostic (D. Plummer)
# -----------------------------------------------------------------------------

# Source the list of on/off switches
source ${CANESM_SRC_ROOT}/CanDIAG/diag4/gaschem_diag_switches.sh

#  ---- variables for selstep
    yearoff=`echo "$year_offset"  | $AWK '{printf "%4d", $0+1}'`
    deltmin=`echo "$delt"         | $AWK '{printf "%5d", $0/60.}'`
#
#  ----  access model history files
.   spfiles.cdk
#
#  ----  get the surface pressure field
    access gslnsp ${flabel}_gslnsp na
    if [ ! -s gslnsp ] ; then
      if [ "$datatype" = "gridsig" ] ; then
        echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME LNSP" |
           ccc select npakgg gslnsp
      else
        echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME LNSP" |
           ccc select npaksp sslnsp
        echo "COFAGG.   $lon$lat" | ccc cofagg sslnsp gslnsp
        rm sslnsp
      fi
    fi
#
#  ----  select out required fields from model history
#
    echo "SELECT    STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME   X1   X2" |
           ccc select npaksp x1_se x2_se
    echo "COFAGGxxx $lon$lat    0$npg" | ccc cofagg x1_se x1_ge
    echo "COFAGGxxx $lon$lat    0$npg" | ccc cofagg x2_se x2_ge
    rm *_se
#
#  ----  interpolate onto constant pressure surfaces
#
echo "GSAPL.    $plv       0.0       0.0$coord$plid
$p01$p02$p03$p04$p05$p06$p07$p08$p09$p10$p11$p12$p13$p14$p15$p16
$p17$p18$p19$p20$p21$p22$p23$p24$p25$p26$p27$p28$p29$p30$p31$p32
$p33$p34$p35$p36$p37$p38$p39$p40$p41$p42$p43$p44$p45$p46$p47$p48
$p49$p50$p51$p52$p53$p54$p55$p56$p57$p58$p59$p60$p61$p62$p63$p64
$p65$p66$p67$p68$p69$p70$p71$p72$p73$p74$p75$p76$p77$p78$p79$p80
$p81$p82$p83$p84$p85$p86$p87$p88$p89$p90$p91$p92$p93$p94$p95$p96
$p97$p98$p99$p100" > .gsapl_input_card
    gsapl x1_ge gslnsp x1_gp input=.gsapl_input_card
    gsapl x2_ge gslnsp x2_gp input=.gsapl_input_card
#
#  ----  create monthly and zonal averages
#
    timavg x1_gp x1_tav
    timavg x2_gp x2_tav
#
    if [ "$gesave" = on ] ; then
      timavg x1_ge x1_etav
      timavg x2_ge x2_etav
    fi
#
    zonavg x1_tav x1_ztav
    zonavg x2_tav x2_ztav
#
# -- xsave card is the same for all calls
    echo "XSAVE.        STRATOSPHERIC OZONE
NEWNAM.
XSAVE.        CONSTANT EMISSION - 90-DAY LIFETIME
NEWNAM." > .xsave_input_card
#
#  ----  save instantaneous fields
#
#     ie - instantaneous data on model levels
#     ip - instantaneous data on constant pressure surfaces
#
    if [ "$iesave" = on ] ; then
      if [ "$ieslct" = "on" ] ; then
        echo "C*TSTEP   $deltmin     ${yearoff}010100          MODEL     YYYYMMDDHH" > ic.tstep_model
        selstep    x1_ge x1_ie input=ic.tstep_model
        selstep    x2_ge x2_ie input=ic.tstep_model
#
        release oldie
        access oldie ${flabel}ie      na
        xsave  oldie x1_ie x2_ie newie input=.xsave_input_card
        save   newie ${flabel}ie
        delete oldie                  na
        rm *_ie newie
      fi
#
      if [ "$ieslct" = "off" ] ; then
        release oldie
        access oldie ${flabel}ie      na
        xsave  oldie x1_ge x2_ge newie input=.xsave_input_card
        save   newie ${flabel}ie
        delete oldie                  na
      fi
    fi
#
    if [ "$ipsave" = "on" ] ; then
      release oldip
      access oldip ${flabel}ip    na
      xsave  oldip x1_gp x2_gp newip input=.xsave_input_card
      save   newip  ${flabel}ip
      delete oldip                 na
      rm newip
    fi
    rm *_gp
    rm *_ge
#
#  ----  save time averaged fields
#
#     xp - time and zonal averaged data on pressure levels
#     gp - time averaged data on pressure levels
#     ge - time averaged data on model eta surfaces
#
    release oldxp
    access oldxp ${flabel}xp      na
    xsave  oldxp x1_ztav x2_ztav newxp input=.xsave_input_card
    save   newxp ${flabel}xp
    delete oldxp                  na
    rm *_ztav newxp
#
    release oldgp
    access oldgp ${flabel}gp      na
    xsave  oldgp x1_tav x2_tav newgp input=.xsave_input_card
    save   newgp ${flabel}gp
    delete oldgp                  na
    rm newgp *_tav
#
    if [ "$gesave" = "on" ] ; then
      release oldge
      access oldge ${flabel}ge      na
      xsave  oldge x1_etav x2_etav newge input=.xsave_input_card
      save   newge ${flabel}ge
      delete oldge                  na
      rm *_etav newge
    fi
#
