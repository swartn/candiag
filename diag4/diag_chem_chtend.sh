#!/bin/sh
#
# This script is only included in the merged_diag_script grouping when
# gas-phase chemistry is run in MAM mode. BW, June 2023
#
#  DESCRIPTION (original name: diag_chem_chtend_971p.sh)
# 
#    Create monthly averages for a set of chemical tendencies output 
#    from the model and calculate the atmospheriic burden for this set
#    of species as well
#
#  PARMSUB PARAMETERS
#
#    -- stime, memory1, chem, model1, r1, r2, r3, t1, t2, t3, t4,
#       flabel, datatype, coord, lon, lat, topsig, plid
#
#  PREREQUISITES
#
#    -- GS history file containing chemical tendency fields
#    -- SS history file
#
#  CHANGELOG
#
#    2023-01-27: Converted to shell script and removed the logic to support
#                 running chemistry on a subset of model levels (D. Plummer)
#    2020-12-22: Included LT21, the chemical loss of stratospheric
#                 ozone tracer for CCMI-2022
#    2015-01-08: Included CCMI-requested diagnostics
#    2008-07-09: Created original version (D. Plummer)
#

#
#  ----  access model history files
#
.     spfiles.cdk
.     ggfiles.cdk
#
#  ----  get the surface pressure field
#
    access gslnsp ${flabel}_gslnsp na
    if [ ! -s gslnsp ] ; then
      if [ "$datatype" = "gridsig" ] ; then
        echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME LNSP" |
           ccc select npakgg gslnsp
      else
        echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME LNSP" |
           ccc select npaksp sslnsp
        echo "COFAGG.   $lon$lat" | ccc cofagg sslnsp gslnsp
        rm sslnsp
      fi
    fi
#
# ---- get surface pressure in Pa
    access sfpr1 ${flabel}_ps na
    if [ ! -s sfpr1 ] ; then    
      expone gslnsp sfpr1
    fi
    echo "XLIN.....      100.0       0.0" | ccc xlin sfpr1 sfpr_pa
    rm sfpr1
#
#  ----  select out required fields from model history
#
# -- chemical tendencies integrated up to the diagnosed tropopause
    echo "SELECT    STEPS $r1 $r2 $r3 LEVS    1    1 NAME LT01 LT02 LT03" |
       ccc select npakgg O3_tr_prod  O3_tr_dest  O3_tr_tend
    echo "SELECT    STEPS $r1 $r2 $r3 LEVS    1    1 NAME LT08 LT11 LT12" |
       ccc select npakgg N2O_tr_tend CH4_tr_tend CO_tr_tend
    echo "SELECT    STEPS $r1 $r2 $r3 LEVS    1    1 NAME LT13 LT14 LT15" |
       ccc select npakgg H2_tr_tend  F1_tr_tend  F2_tr_tend
    echo "SELECT    STEPS $r1 $r2 $r3 LEVS    1    1 NAME LT16 LT18 LT19" |
       ccc select npakgg F3_tr_tend  F5_tr_tend  F6_tr_tend
    echo "SELECT    STEPS $r1 $r2 $r3 LEVS    1    1 NAME LT20 LT21" |
       ccc select npakgg CH3Br_tr_tend O3st_tr_dest
#
# -- chemical tendencies integrated through the lower-most
#    stratosphere: from 100 hPa to tropopause
    echo "SELECT    STEPS $r1 $r2 $r3 LEVS    1    1 NAME LS03" |
       ccc select npakgg O3_ls_tend
#
# -- chemical tendencies integrated on model levels above 
#    the diagnosed tropopause
    echo "SELECT    STEPS $r1 $r2 $r3 LEVS    1    1 NAME LS08 LS11 LS12" |
       ccc select npakgg N2O_st_tend CH4_st_tend CO_st_tend
    echo "SELECT    STEPS $r1 $r2 $r3 LEVS    1    1 NAME LS13 LS14 LS15" |
       ccc select npakgg H2_st_tend  F1_st_tend  F2_st_tend
    echo "SELECT    STEPS $r1 $r2 $r3 LEVS    1    1 NAME LS16 LS18 LS19 LS20" |
       ccc select npakgg F3_st_tend  F5_st_tend  F6_st_tend CH3Br_st_tend
#
#  ----  calculated monthly average fields
#
    timavg O3_ls_tend    O3_ls_tav
    timavg N2O_st_tend   N2O_st_tav
    timavg CH4_st_tend   CH4_st_tav
    timavg CO_st_tend    CO_st_tav
    timavg H2_st_tend    H2_st_tav
    timavg F1_st_tend    F1_st_tav
    timavg F2_st_tend    F2_st_tav
    timavg F3_st_tend    F3_st_tav
    timavg F5_st_tend    F5_st_tav
    timavg F6_st_tend    F6_st_tav
    timavg CH3Br_st_tend CH3Br_st_tav
#
    timavg O3_tr_prod    O3tr_prod_tav
    timavg O3_tr_dest    O3tr_dest_tav
    timavg O3_tr_tend    O3tr_tend_tav
    timavg N2O_tr_tend   N2O_tr_tav
    timavg CH4_tr_tend   CH4_tr_tav
    timavg CO_tr_tend    CO_tr_tav
    timavg H2_tr_tend    H2_tr_tav
    timavg F1_tr_tend    F1_tr_tav
    timavg F2_tr_tend    F2_tr_tav
    timavg F3_tr_tend    F3_tr_tav
    timavg F5_tr_tend    F5_tr_tav
    timavg F6_tr_tend    F6_tr_tav
    timavg CH3Br_tr_tend CH3Br_tr_tav
    timavg O3st_tr_dest  O3st_dest_tav
#
    rm *_tend
    rm *_prod
    rm *_dest
#
#  ----  save time averaged chemical terms
#
#     gp - time averaged data
#
    echo "XSAVE.        O3 LOWERSTRAT CHEM TENDENCY - MOLEC OVER CM2 OVER SEC
NEWNAM.    O3CR
XSAVE.        N2O STRAT CHEM TENDENCY - MOLEC OVER CM2 OVER SEC
NEWNAM.    T5CS
XSAVE.        CH4 STRAT CHEM TENDENCY - MOLEC OVER CM2 OVER SEC
NEWNAM.    MMCS
XSAVE.        CO STRAT CHEM TENDENCY - MOLEC OVER CM2 OVER SEC
NEWNAM.    COCS
XSAVE.        H2 STRAT CHEM TENDENCY - MOLEC OVER CM2 OVER SEC
NEWNAM.    HDCS
XSAVE.        CFC-11 STRAT CHEM TENDENCY - MOLEC OVER CM2 OVER SEC
NEWNAM.    F1CS
XSAVE.        CFC-12 STRAT CHEM TENDENCY - MOLEC OVER CM2 OVER SEC
NEWNAM.    F2CS
XSAVE.        CCL4 STRAT CHEM TENDENCY - MOLEC OVER CM2 OVER SEC
NEWNAM.    F3CS
XSAVE.        HCFC-22 STRAT CHEM TENDENCY - MOLEC OVER CM2 OVER SEC
NEWNAM.    F5CS
XSAVE.        CH3CL STRAT CHEM TENDENCY - MOLEC OVER CM2 OVER SEC
NEWNAM.    F6CS
XSAVE.        CH3BR STRAT CHEM TENDENCY - MOLEC OVER CM2 OVER SEC
NEWNAM.    CBCS" | ccc xsave oldgp O3_ls_tav N2O_st_tav CH4_st_tav CO_st_tav \
                         H2_st_tav F1_st_tav F2_st_tav F3_st_tav F5_st_tav \
                         F6_st_tav CH3Br_st_tav newgp
#
    echo "XSAVE.        O3 TROP CHEM PRODUCTION - MOLEC OVER CM2 OVER SEC
NEWNAM.    O3PT
XSAVE.        O3 TROP CHEM DESTRUCTION - MOLEC OVER CM2 OVER SEC
NEWNAM.    O3DT
XSAVE.        O3 TROP CHEM TENDENCY - MOLEC OVER CM2 OVER SEC
NEWNAM.    O3CT
XSAVE.        N2O TROP CHEM TENDENCY - MOLEC OVER CM2 OVER SEC
NEWNAM.    T5CT
XSAVE.        CH4 TROP CHEM TENDENCY - MOLEC OVER CM2 OVER SEC
NEWNAM.    MMCT
XSAVE.        CO TROP CHEM TENDENCY - MOLEC OVER CM2 OVER SEC
NEWNAM.    COCT
XSAVE.        H2 TROP CHEM TENDENCY - MOLEC OVER CM2 OVER SEC
NEWNAM.    HDCT
XSAVE.        CFC-11 TROP CHEM TENDENCY - MOLEC OVER CM2 OVER SEC
NEWNAM.    F1CT
XSAVE.        CFC-12 TROP CHEM TENDENCY - MOLEC OVER CM2 OVER SEC
NEWNAM.    F2CT
XSAVE.        CCL4 TROP CHEM TENDENCY - MOLEC OVER CM2 OVER SEC
NEWNAM.    F3CT
XSAVE.        HCFC-22 TROP CHEM TENDENCY - MOLEC OVER CM2 OVER SEC
NEWNAM.    F5CT
XSAVE.        CH3CL TROP CHEM TENDENCY - MOLEC OVER CM2 OVER SEC
NEWNAM.    F6CT
XSAVE.        CH3BR TROP CHEM TENDENCY - MOLEC OVER CM2 OVER SEC
NEWNAM.    CBCT
XSAVE.        O3STRAT CHEM TENDENCY - MOLEC OVER CM2 OVER SEC
NEWNAM.    O3SL" | ccc xsave newgp O3tr_prod_tav O3tr_dest_tav O3tr_tend_tav \
                        N2O_tr_tav CH4_tr_tav CO_tr_tav H2_tr_tav F1_tr_tav \
                        F2_tr_tav F3_tr_tav F5_tr_tav F6_tr_tav CH3Br_tr_tav \
                        O3st_dest_tav newgp1
    mv newgp1 newgp
#
    access old_gp ${flabel}gp     na
    xjoin  old_gp newgp new_gp
    save   new_gp ${flabel}gp
    delete old_gp                 na
    rm new* *_tav
#
#  ---- compute atmospheric burdens
#
    echo "SELECT    STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME   T5   MM   CO   HD" |
       ccc select npaksp N2O_se CH4_se CO_se H2_se
    echo "SELECT    STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME   F1   F2   F3   F5" |
       ccc select npaksp F1_se F2_se F3_se F5_se
    echo "SELECT    STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME   F6   CB" |
       ccc   select npaksp F6_se CH3Br_se
#
    echo "COFAGG.   $lon$lat    0    0" > .cofagg_input_card
    cofagg N2O_se   N2O_ge input=.cofagg_input_card
    cofagg CH4_se   CH4_ge input=.cofagg_input_card
    cofagg CO_se    CO_ge input=.cofagg_input_card
    cofagg H2_se    H2_ge input=.cofagg_input_card
    cofagg F1_se    F1_ge input=.cofagg_input_card
    cofagg F2_se    F2_ge input=.cofagg_input_card
    cofagg F3_se    F3_ge input=.cofagg_input_card
    cofagg F5_se    F5_ge input=.cofagg_input_card
    cofagg F6_se    F6_ge input=.cofagg_input_card
    cofagg CH3Br_se CH3Br_ge input=.cofagg_input_card
#
    rm *_se
#
#   Constant on vsinth parmsub line converts integral of
#   mixing ratio in pressure coordinates (Pa) to molecules/cm^2
#     constant = 1.0E-04*(1/grav)*(1/MWair) *Navog
#       grav = N/kg ; MWair = kg/mole ; Navog = molecules/mole
#     constant = 1.0E-04*(1/9.80616)*(1/0.02897)*6.0221418E23
    echo "VSINTH..   HALF2.1198E+20$lay$coord$topsig$plid" > .vsinth_input_card
    vsinth N2O_ge   gslnsp xcol1 input=.vsinth_input_card
    vsinth CH4_ge   gslnsp xcol2 input=.vsinth_input_card
    vsinth CO_ge    gslnsp xcol3 input=.vsinth_input_card
    vsinth H2_ge    gslnsp xcol4 input=.vsinth_input_card
    vsinth F1_ge    gslnsp xcol5 input=.vsinth_input_card
    vsinth F2_ge    gslnsp xcol6 input=.vsinth_input_card
    vsinth F3_ge    gslnsp xcol7 input=.vsinth_input_card
    vsinth F5_ge    gslnsp xcol8 input=.vsinth_input_card
    vsinth F6_ge    gslnsp xcol9 input=.vsinth_input_card
    vsinth CH3Br_ge gslnsp xcol10 input=.vsinth_input_card
#
    mlt xcol1  sfpr_pa xcol1p
    mlt xcol2  sfpr_pa xcol2p
    mlt xcol3  sfpr_pa xcol3p
    mlt xcol4  sfpr_pa xcol4p
    mlt xcol5  sfpr_pa xcol5p
    mlt xcol6  sfpr_pa xcol6p
    mlt xcol7  sfpr_pa xcol7p
    mlt xcol8  sfpr_pa xcol8p
    mlt xcol9  sfpr_pa xcol9p
    mlt xcol10 sfpr_pa xcol10p
#
#  ----  create monthly averages
#
    timavg xcol1p  N2O_col
    timavg xcol2p  CH4_col
    timavg xcol3p  CO_col
    timavg xcol4p  H2_col
    timavg xcol5p  F1_col
    timavg xcol6p  F2_col
    timavg xcol7p  F3_col
    timavg xcol8p  F5_col
    timavg xcol9p  F6_col
    timavg xcol10p CH3Br_col
#
    rm xcol*
#
#  ----  save monthly average burdens
#
    access oldgp2 ${flabel}gp      na
    echo "XSAVE.        N2O BURDEN - MOLEC OVER CM2
NEWNAM.    T5CM
XSAVE.        CH4 BURDEN - MOLEC OVER CM2
NEWNAM.    MMCM
XSAVE.        CO BURDEN - MOLEC OVER CM2
NEWNAM.    COCM
XSAVE.        H2 BURDEN - MOLEC OVER CM2
NEWNAM.    HDCM
XSAVE.        CFC-11 BURDEN - MOLEC OVER CM2
NEWNAM.    F1CM
XSAVE.        CFC-12 BURDEN - MOLEC OVER CM2
NEWNAM.    F2CM
XSAVE.        CCL4 BURDEN - MOLEC OVER CM2
NEWNAM.    F3CM
XSAVE.        HCFC-22 BURDEN - MOLEC OVER CM2
NEWNAM.    F5CM
XSAVE.        CH3CL BURDEN - MOLEC OVER CM2
NEWNAM.    F6CM
XSAVE.        CH3BR BURDEN - MOLEC OVER CM2
NEWNAM.    CBCM" | ccc xsave oldgp2 N2O_col CH4_col CO_col H2_col \
                    F1_col F2_col F3_col F5_col F6_col CH3Br_col newgp2
    save   newgp2 ${flabel}gp
    delete oldgp2                  na
    rm *_col
