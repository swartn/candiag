#!/bin/sh
#   sk: re-write for new timavg/xsave, phase out xtrans.
#
#                  zstats              gjb,ec,dl. jan 24/01 - sk.
#   ---------------------------------- calculate phi statistics
#                                      and covariances with u, v and w.
#

      access z ${flabel}_gpz
      access u ${flabel}_gpu
      access v ${flabel}_gpv

      if [ "$datatype" = "specsig" ]; then
	echo "DIFF        PHI $t1 $t2 $delt" | ccc dif z dzdt
      else
	echo "DIFF        PHI $t1 $t2 $delt       1.0" | ccc dif z dzdt
      fi
      timavg z  tz zp  tzp2
      timavg u  tu up
      timavg v  tv vp
      timcov zp up tzpup
      timcov zp vp tzpvp

      echo 'XSAVE.        GZ
NEWNAM.
XSAVE.        DGZ/DT
NEWNAM.     GZ.
XSAVE.        GZ"GZ"
NEWNAM.    Z"Z"
XSAVE.        GZ"U"
NEWNAM.    Z"U"
XSAVE.        GZ"V"
NEWNAM.    Z"V"' > ic.xsave
      xsave.files="tz dzdt tzp2 tzpup tzpvp"
      if [ "$wxstats" = on ] ; then
         access w  ${flabel}_gpw
         timavg w  tw wp
         timcov zp wp tzpwp
	 xsave.files="$xsave.files tzpwp"
	 echo 'XSAVE.        GZ"W"
NEWNAM.    Z"W"'>> ic.xsave
      fi
#   ---------------------------------- xsave
      xsave new_gp $files new.
      mv new. new_gp

#   ---------------------------------- save results.
      release oldgp
      access  oldgp ${flabel}gp
      xjoin   oldgp new_gp newgp
      save    newgp ${flabel}gp
      delete  oldgp
