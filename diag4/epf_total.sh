#!/bin/sh
#
# This script is only included in the merged_diag_script grouping when
# gas-phase chemistry is run in MAM mode. BW, June 2023
#
# Amendments to the original: source the file of switches
#
# ---------------------------------------------------------------------------
# EP flux diagnostics ("total", i.e., stationary + transient): 
#   Outputs EP flux divergence, meridional and vertical components of the EP 
#   flux, residual circulation, and meridional heat flux. 
#   Log pressure coordinates are used. 
#   Options to: 
#   1) decompose all fluxes into different zonal wavenumber bands (used only
#      if nbands > 0)
#   2) plot monthly mean results.
# ----------------------------------------------- C.McLandress (Jan 9/2007)
#                                         adapted by M.Sigmond (Jun 6/2007)
#
# -- 2022-10-17: converted to shell script and removed plotting (D. Plummer)
# ------------------------------------------------------------------------------

# Source the list of on/off switches
source ${CANESM_SRC_ROOT}/CanDIAG/diag4/gaschem_diag_switches.sh

access u ${flabel}_gpu
access v ${flabel}_gpv
access w ${flabel}_gpw
access t ${flabel}_gpt

# Calculate zonal means and deviations about the zonal mean
zonavg u uz udev
zonavg v vz vdev
zonavg w wz wdev
zonavg t tz tdev
rm u v w t

# ****************************************************************
# EP flux diags for all zonal wavenumbers 
# ****************************************************************

cp udev up
cp vdev vp
cp wdev wp
cp tdev tp
mlt    up vp upvp
mlt    up wp upwp
mlt    vp tp vptp
zonavg upvp upvpz
zonavg upwp upwpz
zonavg vptp vptpz
rm upvp upwp vptp
ccc epflux4gaschem upvpz upwpz vptpz uz tz vz wz epfy epfz epfd vres wres
echo "RELABL.    ZONL
                          VPTP" | ccc relabl vptpz htflux

timavg epfy epfyt
timavg epfz epfzt
timavg epfd epfdt
timavg vres vrest
timavg wres wrest
timavg htflux htfluxt

echo "XSAVE.        MERIDIONAL COMPONENT EP FLUX
NEWNAM.
              VERTICAL COMPONENT EP FLUX
NEWNAM.
              EP FLUX DIVERGENCE
NEWNAM.
              RESIDUAL MERIDIONAL VELOCITY
NEWNAM.
              RESIDUAL VERTICAL VELOCITY
NEWNAM.
              MERIDIONAL HEAT FLUX
NEWNAM." | ccc xsave epfile epfyt epfzt epfdt vrest wrest htfluxt new
mv new epfile

# Calculate the vertical and meridional EP flux including a factor of density,
# as requested by CMIP6/CCMI data request
# In an isothermal atmosphere, the density varies as pressure

pgen epfyt apres
echo "XLIN......       0.0 1.01325E3" | ccc xlin apres p0
echo "XLIN......       0.0      1.47" | ccc xlin apres rho0
div apres p0 pop0
mlt pop0 rho0 rho
#
div epfyt rho epfydrhot
div epfzt rho epfzdrhot

echo "XSAVE.        MERIDIONAL COMPONENT EP FLUX OVER DENSITY
NEWNAM.    EPFY
              VERTICAL COMPONENT EP FLUX OVER DENSITY
NEWNAM.    EPFZ" | ccc xsave epfile epfydrhot epfzdrhot new
mv new epfile

# ****************************************************************
# EP flux diags for zonal wavenumber bands 
# ****************************************************************

if [ "$nbands" -ge 1 ] ; then

  cp udev up
  cp vdev vp
  cp wdev wp
  cp tdev tp

  echo "LLAFRB.    $maxf $mfirst_band1 $mlast_band1" | ccc llafrb up um
  echo "FRALL.    $lon" | ccc frall um up     
  echo "LLAFRB.    $maxf $mfirst_band1 $mlast_band1" | ccc llafrb vp vm
  echo "FRALL.    $lon" | ccc frall vm vp
  echo "LLAFRB.    $maxf $mfirst_band1 $mlast_band1" | ccc llafrb wp wm
  echo "FRALL.    $lon" | ccc frall wm wp
  echo "LLAFRB.    $maxf $mfirst_band1 $mlast_band1" | ccc llafrb tp tm
  echo "FRALL.    $lon" | ccc frall tm tp
  rm um vm wm tm

  mlt up vp upvp
  mlt up wp upwp
  mlt vp tp vptp
  zonavg upvp upvpz
  zonavg upwp upwpz
  zonavg vptp vptpz
  rm upvp upwp vptp

  ccc epflux4gaschem upvpz upwpz vptpz uz tz vz wz epfy1 epfz1 epfd1 vdum wdum
  echo "RELABL.    ZONL
                          VPTP" | ccc relabl vptpz htflux1
  timavg epfy1 epfy1t
  timavg epfz1 epfz1t
  timavg epfd1 epfd1t
  timavg htflux1 htflux1t

  echo "XSAVE.        MERIDIONAL COMPONENT EP FLUX FOR M=$mfirst_band1 TO$mlast_band1
NEWNAM.
              VERTICAL COMPONENT EP FLUX FOR M=$mfirst_band1 TO$mlast_band1
NEWNAM.
              EP FLUX DIVERGENCE FOR M=$mfirst_band1 TO$mlast_band1
NEWNAM.
              MERIDIONAL HEAT FLUX FOR M=$mfirst_band1 TO$mlast_band1
NEWNAM." | ccc xsave epfile epfy1t epfz1t epfd1t htflux1t new
  mv new epfile

  if [ "$nbands" -ge 2 ] ; then

    cp udev up
    cp vdev vp
    cp wdev wp
    cp tdev tp
    echo "LLAFRB.    $maxf $mfirst_band2 $mlast_band2" | ccc llafrb up um
    echo "FRALL.    $lon" | ccc frall  um up     
    echo "LLAFRB.    $maxf $mfirst_band2 $mlast_band2" | ccc llafrb vp vm
    echo "FRALL.    $lon" | ccc frall  vm vp
    echo "LLAFRB.    $maxf $mfirst_band2 $mlast_band2" | ccc llafrb wp wm
    echo "FRALL.    $lon" | ccc frall  wm wp
    echo "LLAFRB.    $maxf $mfirst_band2 $mlast_band2" | ccc llafrb tp tm
    echo "FRALL.    $lon" | ccc frall  tm tp

    rm um vm wm tm
    mlt up vp upvp
    mlt up wp upwp
    mlt vp tp vptp
    zonavg upvp upvpz
    zonavg upwp upwpz
    zonavg vptp vptpz
    rm upvp upwp vptp
    ccc epflux4gaschem upvpz upwpz vptpz uz tz vz wz epfy2 epfz2 epfd2 vdum wdum
    echo "RELABL.    ZONL
                          VPTP" | ccc relabl vptpz htflux2

    timavg epfy2 epfy2t
    timavg epfz2 epfz2t
    timavg epfd2 epfd2t
    timavg htflux2 htflux2t

    echo "XSAVE.        MERIDIONAL COMPONENT EP FLUX FOR M=$mfirst_band2 TO$mlast_band2
NEWNAM.
              VERTICAL COMPONENT EP FLUX FOR M=$mfirst_band2 TO$mlast_band2
NEWNAM.
              EP FLUX DIVERGENCE FOR M=$mfirst_band2 TO$mlast_band2
NEWNAM.
              MERIDIONAL HEAT FLUX FOR M=$mfirst_band2 TO$mlast_band2
NEWNAM." | ccc xsave epfile epfy2t epfz2t epfd2t htflux2t new
    mv new epfile

    if [ "$nbands" -ge 3 ] ; then
      cp udev up
      cp vdev vp
      cp wdev wp
      cp tdev tp
      echo "LLAFRB.    $maxf $mfirst_band3 $mlast_band3" | ccc llafrb up um
      echo "FRALL.    $lon" | ccc frall um up     
      echo "LLAFRB.    $maxf $mfirst_band3 $mlast_band3" | ccc llafrb vp vm
      echo "FRALL.    $lon" | ccc frall  vm vp
      echo "LLAFRB.    $maxf $mfirst_band3 $mlast_band3" | ccc llafrb wp wm
      echo "FRALL.    $lon" | ccc frall  wm wp
      echo "LLAFRB.    $maxf $mfirst_band3 $mlast_band3" | ccc llafrb tp tm
      echo "FRALL.    $lon" | ccc frall  tm tp
      rm um vm wm tm

      mlt up vp upvp
      mlt up wp upwp
      mlt vp tp vptp
      zonavg upvp upvpz
      zonavg upwp upwpz
      zonavg vptp vptpz
      rm upvp upwp vptp
      ccc epflux4gaschem upvpz upwpz vptpz uz tz vz wz epfy3 epfz3 epfd3 vdum wdum
      echo "RELABL.    ZONL
                          VPTP" | ccc relabl vptpz htflux3
      timavg epfy3 epfy3t
      timavg epfz3 epfz3t
      timavg epfd3 epfd3t
      timavg htflux3 htflux3t

      echo "XSAVE.        MERIDIONAL COMPONENT EP FLUX FOR M=$mfirst_band3 TO$mlast_band3
NEWNAM.
              VERTICAL COMPONENT EP FLUX FOR M=$mfirst_band3 TO$mlast_band3
NEWNAM.
              EP FLUX DIVERGENCE FOR M=$mfirst_band3 TO$mlast_band3
NEWNAM.
              MERIDIONAL HEAT FLUX FOR M=$mfirst_band3 TO$mlast_band3
NEWNAM." | ccc xsave epfile epfy3t epfz3t epfd3t htflux3t new
      mv new epfile

      if [ "$nbands" -ge 4 ] ; then
        cp udev up
        cp vdev vp
        cp wdev wp
        cp tdev tp
        echo "LLAFRB.    $maxf $mfirst_band4 $mlast_band4" | ccc llafrb up um
        echo "FRALL.    $lon" | ccc frall um up     
        echo "LLAFRB.    $maxf $mfirst_band4 $mlast_band4" | ccc llafrb vp vm
        echo "FRALL.    $lon" | ccc frall vm vp
        echo "LLAFRB.    $maxf $mfirst_band4 $mlast_band4" | ccc llafrb wp wm
        echo "FRALL.    $lon" | ccc frall wm wp
        echo "LLAFRB.    $maxf $mfirst_band4 $mlast_band4" | ccc llafrb tp tm
        echo "FRALL.    $lon" | ccc frall tm tp
        rm um vm wm tm

        mlt up vp upvp
        mlt up wp upwp
        mlt vp tp vptp
        zonavg upvp upvpz
        zonavg upwp upwpz
        zonavg vptp vptpz
        rm upvp upwp vptp
        ccc epflux4gaschem upvpz upwpz vptpz uz tz vz wz epfy4 epfz4 epfd4 vdum wdum
        echo "RELABL.    ZONL
                          VPTP" | relabl vptpz htflux4

        timavg epfy4 epfy4t
        timavg epfz4 epfz4t
        timavg epfd4 epfd4t
        timavg htflux4 htflux4t

        echo "XSAVE.        MERIDIONAL COMPONENT EP FLUX FOR M=$mfirst_band4 TO$mlast_band4
NEWNAM.
              VERTICAL COMPONENT EP FLUX FOR M=$mfirst_band4 TO$mlast_band4
NEWNAM.
              EP FLUX DIVERGENCE FOR M=$mfirst_band4 TO$mlast_band4
NEWNAM.
              MERIDIONAL HEAT FLUX FOR M=$mfirst_band4 TO$mlast_band4
NEWNAM." | ccc xsave epfile epfy4t epfz4t epfd4t htflux4t new
        mv new epfile
	     
        if [ "$nbands" -ge 5 ] ; then
          cp udev up
          cp vdev vp
          cp wdev wp
          cp tdev tp
          echo "LLAFRB.    $maxf $mfirst_band5 $mlast_band5" | ccc llafrb up um
          echo "FRALL.    $lon" | ccc frall um up
          echo "LLAFRB.    $maxf $mfirst_band5 $mlast_band5" | ccc llafrb vp vm
          echo "FRALL.    $lon" | ccc frall vm vp
          echo "LLAFRB.    $maxf $mfirst_band5 $mlast_band5" | ccc llafrb wp wm
          echo "FRALL.    $lon" | ccc frall wm wp
          echo "LLAFRB.    $maxf $mfirst_band5 $mlast_band5" | ccc llafrb tp tm
          echo "FRALL.    $lon" | ccc frall tm tp
          rm um vm wm tm

          mlt up vp upvp
          mlt up wp upwp
          mlt vp tp vptp
          zonavg upvp upvpz
          zonavg upwp upwpz
          zonavg vptp vptpz
          rm upvp upwp vptp
          ccc epflux4gaschem upvpz upwpz vptpz uz tz vz wz epfy5 epfz5 epfd5 vdum wdum
          echo "RELABL.    ZONL
                          VPTP" | relabl vptpz htflux5

          timavg epfy5 epfy5t
          timavg epfz5 epfz5t
          timavg epfd5 epfd5t
          timavg htflux5 htflux5t

          echo "XSAVE.        MERIDIONAL COMPONENT EP FLUX FOR M=$mfirst_band5 TO$mlast_band5
NEWNAM.
              VERTICAL COMPONENT EP FLUX FOR M=$mfirst_band5 TO$mlast_band5
NEWNAM.
              EP FLUX DIVERGENCE FOR M=$mfirst_band5 TO$mlast_band5
NEWNAM.
              MERIDIONAL HEAT FLUX FOR M=$mfirst_band5 TO$mlast_band5
NEWNAM." | ccc xsave epfile epfy5t epfz5t epfd5t htflux5t new
          mv new epfile

        fi
      fi
    fi
  fi
fi

# Save in XP file
release oldxp
access oldxp ${flabel}xp
xjoin oldxp epfile newxp 
save newxp ${flabel}xp
delete oldxp

# Save instantaneous fields
if [ "$iesave" = "on" -o "$ipsave" = "on" ] ; then
  release oldip
  access oldip ${flabel}ip
  echo "XSAVE         INSTANTANEOUS RESIDUAL MERIDIONAL VELOCITY
NEWNAM.
              INSTANTANEOUS RESIDUAL VERTICAL VELOCITY
NEWNAM.
              INSTANTANEOUS MERIDIONAL COMPONENT EP FLUX
NEWNAM.
              INSTANTANEOUS VERTICAL COMPONENT EP FLUX
NEWNAM.
              INSTANTANEOUS EP FLUX DIVERGENCE
NEWNAM." | ccc xsave oldip vres wres epfy epfz epfd newip
  save newip ${flabel}ip
  delete oldip
  rm newip
fi

# Check that the sum of EP flux & div for all wavenumber bands equals total
# (works only for 4 or 5 bands and only if the sum of the wavenumber bands 
# equals the total)

if [ "$epchecksum" = on ] ; then

  add epfy1 epfy2 sum1
  add epfy3 epfy4 sum2
  add sum1  sum2  epfy_sum
  if [ "$nbands" = 5 ] ; then
    mv epfy_sum sum3
    add epfy5 sum3  epfy_sum 
  fi
  timavg epfy epfyt
  timavg epfy_sum epfyt_sum
  sub epfyt epfyt_sum epfyt_diff 
  ggstat epfyt_diff  
  ggstat epfyt
  ggstat epfyt_sum 

  add epfz1 epfz2 sum1
  add epfz3 epfz4 sum2
  add sum1  sum2  epfz_sum
  if [ "$nbands" = 5 ] ; then
    mv epfz_sum sum3
    add epfz5 sum3  epfz_sum 
  fi
  timavg epfz epfzt
  timavg epfz_sum epfzt_sum
  sub epfzt epfzt_sum epfzt_diff 
  ggstat epfzt_diff  
  ggstat epfzt
  ggstat epfzt_sum

  add epfd1 epfd2 sum1
  add epfd3 epfd4 sum2
  add sum1  sum2  epfd_sum
  if [ "$nbands" = 5 ] ; then
    mv epfd_sum sum3
    add epfd5 sum3  epfd_sum 
  fi
  timavg epfd epfdt
  timavg epfd_sum epfdt_sum
  sub epfdt epfdt_sum epfdt_diff 
  ggstat epfdt_diff
  ggstat epfdt
  ggstat epfdt_sum

  add htflux1 htflux2 sum1
  add htflux3 htflux4 sum2
  add sum1  sum2  htflux_sum
  if [ "$nbands" = 5 ] ; then
    mv htflux_sum sum3
    add htflux5 sum3  htflux_sum 
  fi
  timavg htflux htfluxt
  timavg htflux_sum htfluxt_sum 
  sub htfluxt htfluxt_sum htfluxt_diff 
  ggstat htfluxt_diff  
  ggstat htfluxt
  ggstat htfluxt_sum
fi
     
# Temporary save of instantaneous fields for later use
if [ "$dailysv" = on ] ; then
  save vres ${flabel}_gpvres
  save wres ${flabel}_gpwres
  save epfy ${flabel}_gpepfy
  save epfz ${flabel}_gpepfz
  save epfd ${flabel}_gpepfd
fi
