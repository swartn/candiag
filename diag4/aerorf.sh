#!/bin/sh
#
#
#                  aerorf              Y. PENG - Apr 30/12
#   ---------------------------------- EXTRACT RADIATIVE FLUXES OF TOTAL
#                                      AND EACH AEROSOL SPECIES
#                                      available if RF_SCENARIO="AERORF"
#                                      (bulk and pla)
#
.     ggfiles.cdk
#
#  ----------------------------------  RADIATIVE FLUXES
#
#                                      NET ALL SKY LW
echo "SELECT.   STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME ALNF AL01 AL02 AL03" | ccc select npakgg ALNF AL01 AL02 AL03
echo "SELECT    STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME AL04 AL05 AL06"      | ccc select npakgg AL04 AL05 AL06
#                                      NET ALL SKY SW
echo "SELECT    STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME ASNF AS01 AS02 AS03" | ccc select npakgg ASNF AS01 AS02 AS03
echo "SELECT    STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME AS04 AS05 AS06"      | ccc select npakgg AS04 AS05 AS06
#                                      NET CLEAR SKY LW
echo "SELECT    STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME CLNF CL01 CL02 CL03" | ccc select npakgg CLNF CL01 CL02 CL03
echo "SELECT    STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME CL04 CL05 CL06"      | ccc select npakgg CL04 CL05 CL06
#                                      NET CLEAR SKY SW
echo "SELECT    STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME CSNF CS01 CS02 CS03" | ccc select npakgg CSNF CS01 CS02 CS03
echo "SELECT    STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME CS04 CS05 CS06"      | ccc select npakgg CS04 CS05 CS06
#                                      ALL SKY TOTAL (SW+LW)
echo "SELECT    STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME RF01 RF02 RF03 RF04" | ccc select npakgg RF01 RF02 RF03 RF04
echo "SELECT    STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME RF05 RF06"           | ccc select npakgg RF05 RF06
#                                      HEATING RATE & CLD COVER
echo "SELECT    STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME HRL  HRS  CLD"       | ccc select npakgg HRL  HRS  CLD
#
      rm -f npakgg .npakgg_Link
#                                      CALCULATIONS OF TOTAL
#                                      RADIATIVE FORCING
      add ALNF ASNF RFNF
#
#  ----------------------------------  SAVE SELECTED FIELDS
#
      statsav ALNF AL01 AL02 AL03 \
              AL04 AL05 AL06 \
              ASNF AS01 AS02 AS03 \
              AS04 AS05 AS06 \
              CLNF CL01 CL02 CL03 \
              CL04 CL05 CL06 \
              CSNF CS01 CS02 CS03 \
              CS04 CS05 CS06 \
              RFNF RF01 RF02 RF03 \
              RF04 RF05 RF06 \
              HRL  HRS  CLD  \
              new_gp new_xp $stat2nd

#   ---------------------------------- SAVE RESULTS
      release oldgp
      access  oldgp ${flabel}gp
      xjoin   oldgp new_gp newgp
      save    newgp ${flabel}gp
      delete  oldgp

      if [ "$rcm" != "on" ] ; then
      release oldxp
      access  oldxp ${flabel}xp
      xjoin   oldxp new_xp newxp
      save    newxp ${flabel}xp
      delete  oldxp
      fi
