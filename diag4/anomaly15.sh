#!/bin/sh
#
#                  anomaly15           ml - Mar 27/13 - ml.
#                                      - superlabels adjusted for
#                                        new Ceres_2.6r dataset.
#                                      Based on anomaly12, but
#                                      processes 1-9 mbs also.
#                                      Previous version anomal12:
#                                      - based on anomaly11 but
#                                        revise superlabels in
#                                        xfind's to work with
#                                        new CERES dataset.
#                                      - handle both cases with/
#                                        without CLDO.
#                                      - uses new UWISC liquid water path
#                                        dataset.
#                                      Previous version anomal10:
#                                      - CLDT -> CLDO.
#                                      Previous version anomaly9:
#                                      - GPCP/CMAP (Xie&Arkin)
#                                      - SSM/I Liquid water path added.
#                                      Previous version anomaly8:
#                                      - use new Ceres GEO (including PWAT).
#
#                                       computes differences of mean fields
#                                       for basic data. adjusts the layering
#                                       and resolution so that model and
#                                       observed data may be used or models
#                                       with different resolution.
#
#                                       model1 and letter x refer to control
#                                       or observations, while model2 and
#                                       letter y refer to experiment.
#
#                                       obsfile refers to monthly, seasonally,
#                                       or annually averaged surface
#                                       observations.
#

# --- Determine dimensions of the target grid
    nlon=`echo "$resol" | cut -f1 -d'_' | $AWK '{printf "%5i", $1}'`
    nlat=`echo "$resol" | cut -f2 -d'_' | $AWK '{printf "%5i", $1}'`
#
# --- Define ccg function to add global means to ggplot plots
#
ccg(){
  if [ "$datatype" = "gridsig" ] ; then
     avg=$(echo "C* GLOBAVL${shiftdy}         1"|ccc globavl $2 |grep "SURFACE MEAN IS"|head -1|awk '{printf "%10.3f\n",$11}')
  else
     globavg $2 _ gavg_$2
     avg=$(ggstat gavg_$2 | grep GRID | head -1 | cut -c73-85 | awk '{printf "%g", $1}' | tr -d ' ')
     rm gavg_$2
  fi
  echo "GGPLOT$name        $idx NEXT  $ILVL$ICH1$ICH2$FLO$HI$FINC${b}8" >ic.gplotf
  echo "$run $days $title $avg                                                   " >>ic.gplotf
  if [ "$colourplots" = on ] ; then
     cat ic.gplotf0 >>ic.gplotf
  fi
  ggplot $2 input=ic.gplotf
}

#
# --- Define ccz function for making zxplot plots
#
ccz(){
  if [ "$colourplots" != on ] ; then
     icosh=$(printf "%02d" 0)
  else
     icosh=$(printf "%02d" 2)
  fi
  echo "ZXPLOT.    NEXT    $icosh   0$lxp$SCAL$FLO$HI$FINC$ikax$kin   " >ic.zplotf
  echo "$run $days $title                                                   " >>ic.zplotf
  if [ "$colourplots" = on ] ; then
     echo "              7  187  184  180  140  150  154  157                              " >>ic.zplotf
     cat ic.zplotf0 >>ic.zplotf
  fi
  zxplot $2 input=ic.zplotf
}

# --- Define a function to print out global average
printglb(){
  if [ "$datatype" = "gridsig" ] ; then
     glbline1=$(echo "C* GLOBAVL${shiftdy}         1"|ccc globavl $1 |head -1 |sed 's/,input=.ccc_cards//'| awk '{printf "%-30s",$2}')
     glbline2=$(echo "C* GLOBAVL${shiftdy}         1"|ccc globavl $1 |grep "SURFACE MEAN IS"|head -1| cut -c45-73)
  else
     glbline1=$(globavg $1 | head -1 | awk '{printf "%-30s",$2}')
     glbline2=$(globavg $1 | grep SURFACE | head -1 | cut -c45-73)
  fi
  echo "$glbline1$glbline2"
  echo "$glbline1$glbline2" >> $2
}

#   ---------------------------------- standard plots.
#
headframe=$(cat $(which headfram.cdk) |sed '1,2d')
echo "$headframe" > Input_Cards
echo "         DAYS      = $days     ">> Input_Cards
echo "         RUN       = $runid    ">> Input_Cards
echo "         MODEL1    = $model1   ">> Input_Cards
echo "         MODEL2    = $model2   ">> Input_Cards
echo "         OBSFILE   = $obsfile  ">> Input_Cards

#    libncar plunit=$plunit
.   plotid.cdk
rm Input_Cards

    release filex filey gpcp cmap ceres ceresm cerescld uwisc uwiscm snow
#
#   ----------------------------------- access the data.
#
    access filex ${model1}gp
    access filey ${model2}gp
    if [ "$obsdat" = "on" ] ; then
      access gpcp      $obspcp_gpcp      # GPCP (precipitation) data
      access cmap      $obspcp_cmap      # CMAP (precipitation) data
      access ceres     $obsceres         # Ceres climatology
      access ceresm    $obsceres_msk     # Ceres climatology mask file
      access cerescld  $ceres_cld        # Ceres climatology cloud
      access uwisc     $obsuwisc         # UWISC cloud liquid water path
      access uwiscm    $obsuwisc_msk     # UWISC mask
      access snow      $obssnow          # SNOW mass data

# select subset of ERA fields to accelerate interpolation
ccc   xfind filex subset << eor
XFIND.      1 PMSL
XFIND.      1 ST
XFIND.      1 GT
XFIND.      1 ICE
XFIND.      1 SRAD
XFIND.      1 LRAD
XFIND.      1 SH
XFIND.      1 LH
XFIND.      1 PW
XFIND.      1 $d
XFIND.      1 U
XFIND.      1 V
XFIND.      1 T
XFIND.      1 Q
XFIND.      1 T"T"
XFIND.      1 T"V"
XFIND.      1 U"U"
XFIND.      1 U"V"
XFIND.      1 V"V"
eor
      mv subset filex

      if [ "$datatype" = "gridsig" ] ; then
        #ryj: for interpolating obs to GU grid
        shiftdy=$(echo $nlat |awk '{printf "%10.5f", 90.0/$1}')
        mlat=`echo $resol | cut -f2 -d'_' | $AWK '{printf "%5i", -$1}'`
# interpolate ERA on unshifted LAT/LON grid to the target resolution (area-weighted)
        printf "LLAGG     $nlon$mlat    5                                       1\nC* LLAGG  ${shiftdy}        0.    0" | ccc llagg filex filex.1 ; mv filex.1 filex

# interpolate 5x5 degree datasets on shifted LAT/LON grid to the target resolution (area-weighted)
        for v in gpcp cmap; do
        printf "LLAGG     $nlon$mlat   -5    0     1.E38      1.25      1.25    1\nC* LLAGG  ${shiftdy}        0.    0" | ccc llagg $v $v.1 ; mv $v.1 $v
        done

# interpolate 1x1 degree datasets on shifted LAT/LON grid to the target resolution (area-weighted)
        for v in ceres ceresm cerescld uwisc uwiscm snow ; do
        printf "LLAGG     $nlon$mlat   -5    0     1.E38       0.5       0.5    1\nC* LLAGG  ${shiftdy}        0.    0" | ccc llagg $v $v.1 ; mv $v.1 $v
        done

      else
# interpolate ERA on unshifted LAT/LON grid to the target resolution (area-weighted)
        echo "LLAGG     $nlon$nlat    5                                       1" | ccc llagg filex filex.1 ; mv filex.1 filex

# interpolate 5x5 degree datasets on shifted LAT/LON grid to the target resolution
        for v in gpcp cmap ; do
        echo "LLAGG     $nlon$nlat   -5                     1.25      1.25    1" | ccc llagg $v $v.1 ; mv $v.1 $v
        done

# interpolate 1x1 degree datasets on shifted LAT/LON grid to the target resolution
        for v in ceres ceresm cerescld uwisc uwiscm snow ; do
        echo "LLAGG     $nlon$nlat   -5    0     1.E38       0.5       0.5    1" | ccc llagg $v $v.1 ; mv $v.1 $v
        done
      fi

    fi

#   ---------------------------------- print out datasets.
    rm -f dummy
    echo "DATASETS"               >> dummy
    echo "MODEL1=$model1"         >> dummy
    echo "MODEL2=$model2"         >> dummy
    if [ "$obsdat" = "on" ] ; then
      echo "GPCP=$obspcp_gpcp"    >> dummy
      echo "CMAP=$obspcp_cmap"    >> dummy
      echo "CERES=$obsceres"      >> dummy
      echo "CERES_CLD=$ceres_cld" >> dummy
      echo "UWISC=$obsuwisc"      >> dummy
    fi
    txtplot input=dummy
    rm dummy

#
#   ----------------------------------- surface anomalies.
#
#                                       pmsl.
    echo "XFIND.        PMSL" |ccc xfind filey y
    echo "XFIND.        PMSL" |ccc xfind filex x
    sub y x dif
    idx=$(printf "%2d" 0)
    if [ "$colourplots" != "on" ] ; then
       ILVL=$(printf "%4d" 1) ; ICH1=$(printf "%4d" $map) ; ICH2=$(printf "%10.1f" 1.)
    else
       ILVL=$(printf "%4d" 13) ; ICH1=$(printf "%4d" 0) ; ICH2=$(printf "%10.1f" 1.)
    fi
    FLO=$(printf "%10.1f" -100.) ; HI=$(printf "%10.1f" 100.)
    FINC=$(printf "%11.1f" 2.0)
    echo "              7  187  184  180  140  150  154  157                              " >ic.gplotf0
    echo "                -10.       -6.       -2.        2.        6.       10.      100." >>ic.gplotf0
    title="PMSL ANOMALY [MB]" ; name=".   "
ccg ggplot dif

    zonavg dif zdif
    echo "XMPLOTPMSL         0 NEXT    1      -10.       10.    1    1                   " >ic.plts
    echo "$run $days PMSL ANOMALY [MB]                                                   " >>ic.plts
    xmplot zdif input=ic.plts

    rm y x dif
#
#                                       precipitation (convert to mm/day).
    echo "XFIND.        PCP" |ccc xfind filey yy
    echo "XLIN.       86400.        0." |ccc xlin yy y
    rm yy
    if [ "$obsdat" = "on" ] ; then
      if [ "$gpcp" = "on" ] ; then
        echo "XFIND.        PCP" |ccc xfind gpcp x
      else
        echo "XFIND.        PCP1" |ccc xfind cmap x
      fi
    else
      echo "XFIND.        PCP" |ccc xfind filex xx
      echo "XLIN.       86400.        0." |ccc xlin xx x
      rm xx
    fi
#
    sub y x dif
    div dif x rdif
    if [ "$colourplots" != "on" ] ; then
       ILVL=$(printf "%4d" 1) ; ICH1="1  0" ; ICH2=$(printf "%10.1f" 1.) ; name=" PCP"
    else
       ILVL=$(printf "%4d" 13) ; ICH1="1  0" ; ICH2=$(printf "%10.1f" 1.) ; name=".   "
    fi
    FLO=$(printf "%10.1f" -50.) ; HI=$(printf "%10.1f" 50.)
    FINC=$(printf "%11.1f" 2.0)
    echo "              7  187  184  180  140  150  154  157                              " >ic.gplotf0
    echo "                -10.       -6.       -2.        2.        6.       10.      100." >>ic.gplotf0
    title="PRECIP ANOMALY [MM/DAY]"
ccg ggplot  dif
    ICH2=$(printf "%10.1f" 100.)
    FLO=$(printf "%10.1f" -225.) ; HI=$(printf "%10.1f" 225.)
    FINC=$(printf "%11.1f" 50.0)
    echo "              7  187  184  180  140  150  154  157                              " >ic.gplotf0
    echo "               -125.      -75.      -25.       25.       75.      125.    99999." >>ic.gplotf0
    title="PCP REL. ANOM. [%]"
ccg ggplot rdif

    zonavg dif zdif
    zonavg   y zy
    div zdif zy rzdif
    echo "XMPLOT PCP         0 NEXT    1       -5.        5.    1    1                   " >ic.plts
    echo "$run $days PRECIP ANOMALY [MM/DAY]                                             " >>ic.plts
    xmplot  zdif input=ic.plts
    echo "XMPLOT PCP         0 NEXT    1       -1.        1.    1    1                   " >ic.plts
    echo "$run $days PCP REL. ANOM. [FRAC]                                               " >>ic.plts
    xmplot rzdif input=ic.plts

    rm y x dif zdif rzdif
#
#                                       total cloudiness.
    if [ "$nocldo" = "off" ] ; then
       echo "XFIND.        CLDO" |ccc xfind filey y
    else
       echo "XFIND.        CLDT" |ccc xfind filey y
    fi
#
    if [ "$obsdat" = "on" ] ; then
      echo "XFIND.        Cloud Area Fraction, Day and Night (unitless)" |ccc xfind cerescld c3
      echo "XLIN.         0.01        0." |ccc xlin c3 x
      rm c3
    else
      if [ "$nocldo" = "off" ] ; then
         echo "XFIND.        CLDO" |ccc xfind filey y
      else
         echo "XFIND.        CLDT" |ccc xfind filey y
      fi
    fi
#
    sub y x dif
    echo "XLIN.          100." |ccc xlin dif dif100 ; mv dif100 dif
    if [ "$colourplots" != "on" ] ; then
       ILVL=$(printf "%4d" 1) ; ICH1=$(printf "%4d" 0) ; ICH2=$(printf "%10.1f" 1.)
    else
       ILVL=$(printf "%4d" 13) ; ICH1=$(printf "%4d" 0) ; ICH2=$(printf "%10.1f" 1.)
    fi
    FLO=$(printf "%10.1f" -105.) ; HI=$(printf "%10.1f" 105.)
    FINC=$(printf "%11.1f" 10.0)
    title="(MOD-CERES) TOT.OVERLAP.CLOUD ANOM.[%]" ; name="CLDO"
    echo "              7  187  184  181  140  151  154  157                              " >ic.gplotf0
    echo "                -25.      -15.       -5.        5.       15.       25.          " >>ic.gplotf0
ccg ggplot dif
    rm y x dif
#
#                                       outgoing longwave radiation - positive
#                                       upwards.
    echo "XFIND.        OLR" |ccc xfind filey y
    if [ "$obsdat" = "on" ] ; then
      echo "XFIND.        Total-sky TOA Outgoing LW Flux  (W/m2)" |ccc xfind ceres x
      echo "XFIND.        Total-sky TOA Outgoing LW Flux  (W/m2)" |ccc xfind ceresm xm
      echo "FMASK.               NEXT   GE       0.5" |ccc fmask xm mask
      mlt y mask my
      mlt x mask mx
      mv my y
      mv mx x
      rm xm mask
    else
      echo "XFIND.        OLR" |ccc xfind filex x
    fi
    sub y x dif
    globavg dif
    FLO=$(printf "%10.1f" -205.) ; HI=$(printf "%10.1f" 205.)
    title="OLR ANOMALY [W/M2]" ; name=" OLR"
ccg ggplot dif
    rm y x dif
#
#                                       clear-sky net longwave flux at toa.
    echo "XFIND.        OLRC" |ccc xfind filey y
    if [ "$obsdat" = "on" ] ; then
      echo "XFIND.        Clear-sky TOA Outgoing LW Flux  (W/m2)" |ccc xfind ceres x
      echo "XFIND.        Clear-sky TOA Outgoing LW Flux  (W/m2)" |ccc xfind ceresm xm
      echo "FMASK.               NEXT   GE       0.5" |ccc fmask xm mask
      mlt y mask my
      mlt x mask mx
      mv my y
      mv mx x
      rm xm mask
    else
      echo "XFIND.        OLRC" |ccc xfind filex x
    fi
    sub y x dif
    globavg dif
    title="CLEAR-SKY OLR ANOMALY [W/M2]" ; name=" OLR"
ccg ggplot dif
    rm y x dif
#
#                                       all-sky planetary albedo.
    echo "XFIND.        LPA" |ccc xfind filey y
    if [ "$obsdat" = "on" ] ; then
      echo "XFIND.        Total-sky TOA Albedo SSTF/SOLF (dimensionless)" |ccc xfind ceres x
      echo "XFIND.        Total-sky TOA Albedo SSTF/SOLF (dimensionless)" |ccc xfind ceresm xm
      echo "FMASK.               NEXT   GE       0.5" |ccc fmask xm mask
      mlt y mask my
      mlt x mask mx
      mv my y
      mv mx x
      cp mask mask0
      rm xm
    else
      echo "XFIND.        LPA" |ccc xfind filex x
    fi
    sub y x dif
    echo "XLIN.          100." |ccc xlin dif dif100 ; mv dif100 dif
    if [ "$obsdat" = "on" ] ; then
      echo "C* FMSKPLT        -1 NEXT   GT      0.E0" | ccc fmskplt dif difm mask ; mv difm dif
      rm mask
    fi
    FLO=$(printf "%10.1f" -102.) ; HI=$(printf "%10.1f" 102.)
    FINC=$(printf "%11.1f" 4.0)
    title="LPA ANOMALY [%]" ; name=" LPA"
    echo "              7  187  184  181  140  151  154  157                              " >ic.gplotf0
    echo "                -10.       -6.       -2.        2.        6.       10.          " >>ic.gplotf0
ccg ggplot dif
    rm y x dif
#
#                                       clear-sky planetary albedo.
    echo "XFIND.        LPAC" |ccc xfind filey y
    if [ "$obsdat" = "on" ] ; then
      echo "XFIND.        Clear-sky TOA Albedo SATF/SOLF (dimensionless)" |ccc xfind ceres x
      echo "XFIND.        Clear-sky TOA Albedo SATF/SOLF (dimensionless)" |ccc xfind ceresm xm
      echo "FMASK.               NEXT   GE       0.5" |ccc fmask xm mask1
      mlt mask0 mask1 mask
      mlt y mask my
      mlt x mask mx
      mv my y
      mv mx x
      rm xm mask0 mask1
    else
      echo "XFIND.        LPAC" |ccc xfind filex x
    fi
    sub y x dif
    echo "XLIN.          100." |ccc xlin dif dif100 ; mv dif100 dif
    if [ "$obsdat" = "on" ] ; then
      echo "C* FMSKPLT        -1 NEXT   GT      0.E0" | ccc fmskplt dif difm mask ; mv difm dif
      rm mask
    fi
    title="CLEAR-SKY ALBEDO ANOMALY [%]" ; name=" LPA"
ccg ggplot dif
    rm y x dif
#
#                                       reflected all-sky solar flux at toa.
    echo "XFIND.        FSR" |ccc xfind filey y
    if [ "$obsdat" = "on" ] ; then
      echo "XFIND.        Total-sky TOA Outgoing SW Flux  (W/m2)" |ccc xfind ceres x
      echo "XFIND.        Total-sky TOA Outgoing SW Flux  (W/m2)" |ccc xfind ceresm xm
      echo "FMASK.               NEXT   GE       0.5" |ccc fmask xm mask
      mlt y mask my
      mlt x mask mx
      mv my y
      mv mx x
      rm xm mask
    else
      echo "XFIND.        FSR" |ccc xfind filex x
    fi
    sub y x dif
    globavg dif
    FLO=$(printf "%10.1f" -205.) ; HI=$(printf "%10.1f" 205.)
    FINC=$(printf "%11.1f" 10.0)
    title="FSR ANOMALY [W/M2]" ; name=" FSR"
    echo "              7  187  184  181  140  151  154  157                              " >ic.gplotf0
    echo "                -25.      -15.       -5.        5.       15.       25.          " >>ic.gplotf0
ccg ggplot dif
    rm y x dif
#
#                                       reflected clear-sky solar flux at toa.
    echo "XFIND.        FSRC" |ccc xfind filey y
    if [ "$obsdat" = "on" ] ; then
      echo "XFIND.        Clear-sky TOA Outgoing SW Flux  (W/m2)" |ccc xfind ceres x
      echo "XFIND.        Clear-sky TOA Outgoing SW Flux  (W/m2)" |ccc xfind ceresm xm
      echo "FMASK.               NEXT   GE       0.5" |ccc fmask xm mask
      mlt y mask my
      mlt x mask mx
      mv my y
      mv mx x
      rm xm mask
    else
      echo "XFIND.        FSRC" |ccc xfind filex x
    fi
    sub y x dif
    globavg dif
ccc lpprint dif << CARD
  LPPRINT.    0        1.
        FSRC DIFFERENCE [W/M2]
CARD
    title="FSRC ANOMALY [W/M2]" ; name="FSRC"
ccg ggplot dif
    rm y x dif
#
#                                       short-wave cloud forcing at toa.
    echo "XFIND.        CFST" |ccc xfind filey y
    if [ "$obsdat" = "on" ] ; then
      echo "XFIND.        SW cloud-radiative effect at TOA  (W/m2)" |ccc xfind ceres x
      echo "XFIND.        SW cloud-radiative effect at TOA  (W/m2)" |ccc xfind ceresm xm
      echo "FMASK.               NEXT   GE       0.5" |ccc fmask xm mask
      mlt y mask my
      mlt x mask mx
      mv my y
      mv mx x
      rm mask
    else
      echo "XFIND.        CFST" |ccc xfind filex x
    fi
    sub y x dif
    title="CFST ANOMALY [W/M2]" ; name="CFST"
ccg ggplot dif
    rm y x dif
#
#                                       long-wave cloud forcing at toa.
    echo "XFIND.        CFLT" |ccc xfind filey y
#
    if [ "$obsdat" = "on" ] ; then
      echo "XFIND.        LW cloud-radiative effect at TOA  (W/m2)" |ccc xfind ceres x
      echo "XFIND.        LW cloud-radiative effect at TOA  (W/m2)" |ccc xfind ceresm xm
      echo "FMASK.               NEXT   GE       0.5" |ccc fmask xm mask
      mlt y mask my
      mlt x mask mx
      mv my y
      mv mx x
      rm mask
    else
      echo "XFIND.        CFLT" |ccc xfind filex x
    fi
    sub y x dif
    title="CFLT ANOMALY [W/M2]" ; name="CFLT"
ccg ggplot dif
    rm y x dif
#
#                                       balance at top of atmosphere.
    echo "XFIND.        BALT" |ccc xfind filey y
    if [ "$obsdat" = "on" ] ; then
      echo "XFIND.        Total-sky TOA Net Flux  (W/m2)" |ccc xfind ceres x
      echo "XFIND.        Total-sky TOA Net Flux  (W/m2)" |ccc xfind ceresm xm
      echo "FMASK.               NEXT   GE       0.5" |ccc fmask xm mask
      mlt y mask my
      mlt x mask mx
      mv my y
      mv mx x
      rm xm mask
    else
      echo "XFIND.        BALT" |ccc xfind filex x
    fi
    sub y x dif
    title="BALANCE AT TOA ANOMALY [W/M2]" ; name="BALT"
ccg ggplot dif
    rm y x dif
#                                       surface energy balance
#
    echo "XFIND.        BEG" |ccc xfind filey y
    if [ "$obsdat" = "on" ] ; then
#                                       compute BEG from individual fluxes.
      echo "XFIND.        SRAD
XFIND.        LRAD
XFIND.        SH
XFIND.        LH" |ccc xfind filex fsg flg hfs hfl
      add fsg flg balg
      add hfs hfl hfsl
      add balg hfsl x
    else
      echo "XFIND.        BEG" |ccc xfind filex x
    fi
    sub y x dif
    FLO=$(printf "%10.1f" -905.) ; HI=$(printf "%10.1f" 905.)
    title="BEG ANOMALY [W/M2]" ; name="    "
ccg ggplot dif
    rm y x dif
#
#                                       screen temperature.
    echo "XFIND.        ST" |ccc xfind filey y
    echo "XFIND.        ST" |ccc xfind filex x
    if [ "$obsdat" = "on" ] ; then
      echo "XLIN.            1.   -273.16" |ccc xlin x z
      mv z x
    fi
    sub y x dif
    FLO=$(printf "%10.1f" -50.) ;  HI=$(printf "%10.1f" 50.)
    FINC=$(printf "%11.1f" 1.0)
    title="SCREEN TEMP. ANOMALY [K]" ; name="  ST"
    echo "              9  187  184  181  180  140  150  151                              " >ic.gplotf0
    echo "                 154  157                                                       " >>ic.gplotf0
    echo "                 -5.       -3.       -1.      -0.5       0.5        1.        3." >>ic.gplotf0
    echo "                  5.                                                            " >>ic.gplotf0
ccg ggplot dif
    rm y x dif
#                                       skin temperature.
    echo "XFIND.        GT" |ccc xfind filey y
    echo "XFIND.        GT" |ccc xfind filex x
    if [ "$obsdat" = "on" ] ; then
      echo "XLIN.            1.   -273.16" |ccc xlin x z
      mv z x
    fi
    sub y x dif
    title="SKIN TEMP. ANOMALY [K]" ; name="  GT"
ccg ggplot dif
    rm y x dif
##                                       sea ice concentration SICN
#    echo "XFIND.        SICN" |ccc xfind filey y
#    echo "XFIND.        ICE " |ccc xfind filex x
#    if [ "$obsdat" = "on" ] ; then
#      echo "C* FMSKPLT        -1 NEXT   LT0.9999E+38    1        0.    1" |ccc fmskplt x x0
#      sub y x0 dif0
#      echo "C* FMSKPLT        -1 NEXT   LT0.9999E+38                   1" |ccc fmskplt dif0 dif x
#    else
#      sub y x dif
#    fi
#    idx=$(printf "%2d" 0)
#    if [ "$colourplots" != "on" ] ; then
#       ILVL=$(printf "%4d" -1) ; ICH1=$(printf "%4d" 0) ; ICH2=$(printf "%10.1f" 1.)
#    else
#       ILVL=$(printf "%4d" -13) ; ICH1=$(printf "%4d" 0) ; ICH2=$(printf "%10.1f" 1.)
#    fi
#    FLO=$(printf "%10.1f" -1.0) ;  HI=$(printf "%10.1f" 1.0)
#    FINC=$(printf "%11.1f" 0.1)
#    title="SICN ANOMALY" ; name=" ICE"
#    echo "              9  187  184  181  180  140  150  151                              " >ic.gplotf0
#    echo "                 154  157                                                       " #>>ic.gplotf0
#    echo "                -0.5      -0.3      -0.2      -0.1       0.1       0.2       0.3" #>>ic.gplotf0
#    echo "                 0.5                                                            " #>>ic.gplotf0
#ccg ggplot dif
#    rm y x dif
#                                       precipitable water
#
    echo "XFIND         PWAT" |ccc xfind filey y
    echo "XFIND         PW" |ccc xfind filex x
    sub y x dif
    if [ "$colourplots" != "on" ] ; then
       ILVL=$(printf "%4d" 1) ; ICH1=$(printf "%4d" 0) ; ICH2=$(printf "%10.1f" 1.)
    else
       ILVL=$(printf "%4d" 13) ; ICH1=$(printf "%4d" 0) ; ICH2=$(printf "%10.1f" 1.)
    fi
    FLO=$(printf "%10.1f" -90.0) ;  HI=$(printf "%10.1f" 90.0)
    FINC=$(printf "%11.1f" 2.0)
    title="PRECIP. WATER ANOMALY [MM]" ; name=" PCP"
    echo "              7  187  184  181  140  151  154  157                              " >ic.gplotf0
    echo "                -10.       -6.       -2.        2.        6.       10.          " >>ic.gplotf0
ccg ggplot dif
    zonavg dif zdif
    echo "XMPLOT             0 NEXT    1       -6.        5.    1    1                   " >ic.plts
    echo "$run $days PRECIP WAT                                                          " >>ic.plts
    xmplot zdif input=ic.plts
    rm y x dif zdif
#                                       liquid water path
#
    echo "XFIND.        CLWT" |ccc xfind filey yy
    echo "XLIN.        1000.        0." |ccc xlin yy y
    rm yy
    if [ "$obsdat" = "on" ] ; then
      echo "XFIND.        Total Cloud Liquid Water Path (g/m2)" |ccc xfind uwisc x
      echo "XFIND.        Total Cloud Liquid Water Path (g/m2)" |ccc xfind uwiscm maskx
      echo "FMASK.               NEXT   GE       0.5" |ccc fmask maskx mask
      mlt y mask my
      mlt x mask mx
      mv my y
      mv mx x
      rm uwisc uwiscm maskx
    else
      echo "XFIND.        CLWT" |ccc xfind filex xx
      echo "XLIN.        1000.        0." |ccc xlin xx x
      rm xx
    fi
    sub y x dif
    if [ "$obsdat" = "on" ] ; then
      echo "C* FMSKPLT        -1 NEXT   GT      0.E0" | ccc fmskplt dif difm mask ; mv difm dif
      rm mask
    fi
    if [ "$colourplots" != "on" ] ; then
       ILVL=$(printf "%4d" 1) ; ICH1=$(printf "%4d" 0) ; ICH2=$(printf "%10.1f" 1.)
    else
       ILVL=$(printf "%4d" 16) ; ICH1=$(printf "%4d" 0) ; ICH2=$(printf "%10.1f" 1.)
    fi
    FLO=$(printf "%10.1f" -130.0) ; HI=$(printf "%10.1f" 130.0)
    FINC=$(printf "%11.1f" 20.0)
    title="LIQUID WATER PATH ANOMALY [G/M2]" ; name="    "
    echo "          0  13  124  120  117  113  109  107  141                              " >ic.gplotf0
    echo "                 106  105  104  102  101  100                                   " >>ic.gplotf0
    echo "               -110.      -90.      -70.      -50.      -30.      -10.       10." >>ic.gplotf0
    echo "                 30.       50.       70.       90.      110.      130.          " >>ic.gplotf0
ccg ggplot dif
    rm y x dif
#
#   ----------------------------------- atmospheric anomalies.
#
#                                       get beta.
    echo "XFIND.        $d" |ccc xfind filex betax
    echo "XFIND.        $d" |ccc xfind filey beta

#                                       select common set of levels for beta
    comlev betax beta .betax .beta ; mv .betax betax ; mv .beta beta

#   ----------------------------------- get zonal and meridional velocities and
#                                       calculate simple (no beta) version of
#                                       v* to be used by comdeck testx. also
#                                       calculate various u,v standing eddy
#                                       statistics to be plotted later.
    echo "XFIND.        U
XFIND.        V" |ccc xfind filex ux vx
    echo "XFIND.        U
XFIND.        V" |ccc xfind filey uy vy
#                                       select common set of levels
    comlev ux uy ux_lev uy_lev ; mv ux_lev ux ; mv uy_lev uy
    comlev vx vy vx_lev vy_lev ; mv vx_lev vx ; mv vy_lev vy
#
#   ----------------------------------- basic mean and standing eddy stats.
    zonavg ux zux
    zonavg uy zuy
    zondev ux zux uxs
    zondev uy zuy uys
#
    zonavg vx zvx
    zonavg vy zvy
    zondev vx zvx vxs
    zondev vy zvy vys
#
    rm zux zuy zvx zvy
#
    square uxs uxs2
    square uys uys2
    zonavg uxs2 zux2
    zonavg uys2 zuy2
    sqroot zux2 sduxs
    sqroot zuy2 sduys
    sub sduys sduxs seusd
    rm uxs2 uys2 zux2 zuy2 sduxs sduys
#
    square vxs vxs2
    square vys vys2
    zonavg vxs2 zvx2
    zonavg vys2 zvy2
    sqroot zvx2 sdvxs
    sqroot zvy2 sdvys
    sub sdvys sdvxs sevsd
    rm vxs2 vys2 zvx2 zvy2 sdvxs sdvys
#
    mlt vxs uxs vsuxs
    mlt vys uys vsuys
    sub vsuys vsuxs dvs

    rzonavg dvs beta seutran
    rm uxs uys vsuxs vsuys dvs
#
    sub uy ux udif
    rzonavg udif beta zudif
    rm udif

# ------------------------------------- temperature.

    echo "XFIND.        T" |ccc xfind filex x
    echo "XFIND.        T" |ccc xfind filey y
#                                       select common set of levels
    comlev x y x_lev y_lev ; mv x_lev x ; mv y_lev y
#                                       basic mean and standing eddy stats.
    zonavg x zx
    zonavg y zy
    zondev x zx xs
    zondev y zy ys
#
    square xs xs2
    square ys ys2
    mlt vxs xs vsxs
    mlt vys ys vsys
#
    zonavg xs2 zx2
    zonavg ys2 zy2
    sqroot zx2 sdxs
    sqroot zy2 sdys
    sub sdys sdxs sesd
    rm zx zy xs ys xs2 ys2 zx2 zy2
#
    sub vsys vsxs dvs
    rzonavg dvs beta setran
    rm vsys vsxs vys vxs sdys
#
    sub y x dif
    rzonavg dif beta zdif
#                                       850 and 200 mb.
    sub y x tdif
    echo "SELECT.                 0 999999999    1 LEVS  850  850 NAME  ALL " |ccc select tdif tdiff
    echo "SELECT.                 0 999999999    1 LEVS  200  200 NAME  ALL " |ccc select tdif tdif200
    rm x y tdif
#
    echo "SELECT.                 0 999999999    1 LEVS  850  850 NAME  ALL " |ccc select beta beta850
    mlt tdiff beta850 tdif850
    ggstat tdiff
    ggstat beta850
    ggstat tdif850

    idx=$(printf "%2d" 0)
    if [ "$colourplots" != "on" ] ; then
       ILVL=$(printf "%4d" -1) ; ICH1=$(printf "%4d" 0) ; ICH2=$(printf "%10.1f" 1.)
    else
       ILVL=$(printf "%4d" -12) ; ICH1=$(printf "%4d" 0) ; ICH2=$(printf "%10.1f" 1.)
    fi
    FLO=$(printf "%10.1f" -50.) ; HI=$(printf "%10.1f" 50.)
    FINC=$(printf "%11.1f" 2.0)
    title="T850 ANOMALY [K]" ; name="   T"
    echo "              7  187  184  180  140  150  154  157                              " >ic.gplotf0
    echo "                -10.       -6.       -2.        2.        6.       10.      100." >>ic.gplotf0
ccg ggplot tdif850
    title="T200 ANOMALY [K]" ; name="   T"
ccg ggplot tdif200
    rm beta850 tdiff tdif850 tdif200
#                                       t, t*t*, v*t*
    SCAL=$(printf "%10.2f" 1.E0) ; FLO=$(printf "%10.2f" -40.E0) ; HI=$(printf "%10.2f" 40.E0)
    FINC=$(printf "%10.2f" 1.E0) ; ikax=$(printf "%5d" $kax)
    title="(T)R ANOMALY [K]"
    echo "                -10.       -6.       -2.        2.        6.       10.      100." >ic.zplotf0
    ccz zxplot zdif
#
    if [ "$xtradif" = "on" ] ; then
      SCAL=$(printf "%10.2f" 1.E0) ; FLO=$(printf "%10.2f" -1.E2) ; HI=$(printf "%10.2f" 1.E2)
      FINC=$(printf "%10.2f" 0.25) ; ikax=$(printf "%5d" $kax)
      if [ "$colourplots" != "on" ] ; then FINC=$(printf "%10.2f" 5.E-1) ; fi
      title="ANOMALY ((T*T*)R)**0.5 [K]"
      echo "                -1.5       -1.     -0.50      0.50       1.0       1.5      100." >ic.zplotf0
      ccz zxplot sesd
      SCAL=$(printf "%10.2f" 1.E0) ; FLO=$(printf "%10.2f" -2.E2) ; HI=$(printf "%10.2f" 2.E2)
      FINC=$(printf "%10.2f" 1.E0) ; ikax=$(printf "%5d" $kax)
      if [ "$colourplots" != "on" ] ; then FINC=$(printf "%10.2f" 2.E0) ; fi
      title="ANOMALY (V*T*)R [M*K/S]"
      echo "                 -6.       -4.       -2.        2.        4.        6.      100." >ic.zplotf0
      ccz zxplot setran
      rm zdif sesd setran
      if [ "$stat2nd" != "off" ] ; then
# ------------------------------------- t't'

      echo "XFIND.        T'T'" |ccc xfind filex x
      echo "XFIND.        T'T'" |ccc xfind filey y
#                                       select common set of levels
      comlev x y x_lev y_lev ; mv x_lev x ; mv y_lev y

      sqroot x sdx
      sqroot y sdy
#
      sub sdy sdx dif
      rzonavg dif beta zdif
      SCAL=$(printf "%10.2f" 1.E0) ; FLO=$(printf "%10.2f" -1.E2) ; HI=$(printf "%10.2f" 1.E2)
      FINC=$(printf "%10.2f" 0.25) ; ikax=$(printf "%5d" $kax)
      if [ "$colourplots" != "on" ] ; then FINC=$(printf "%10.2f" 5.E-1) ; fi
      title="ANOMALY ((T+T+)R)**0.5 [K]"
      echo "                -1.5       -1.     -0.50      0.50       1.0       1.5      100." >ic.zplotf0
      ccz zxplot zdif
      rm x y zdif

# ------------------------------------- v't'

      echo "XFIND.        T'V'" |ccc xfind filex x
      echo "XFIND.        T'V'" |ccc xfind filey y
#                                       select common set of levels
      comlev x y x_lev y_lev ; mv x_lev x ; mv y_lev y
#
      sub y x dif
      rzonavg dif beta zdif
      SCAL=$(printf "%10.2f" 1.E0) ; FLO=$(printf "%10.2f" -2.E2) ; HI=$(printf "%10.2f" 2.E2)
      FINC=$(printf "%10.2f" 1.E0) ; ikax=$(printf "%5d" $kax)
      if [ "$colourplots" != "on" ] ; then FINC=$(printf "%10.2f" 2.E0) ; fi
      title="ANOMALY (V+T+)R [M*K/S]"
      echo "                 -6.       -4.       -2.        2.        4.        6.      100." >ic.zplotf0
      ccz zxplot zdif
      rm y x dif zdif
      fi
    fi
#
#                                       zonal velocity.
#
#                                       u, u*u*, u*v*
    SCAL=$(printf "%10.2f" 1.E0) ; FLO=$(printf "%10.2f" -40.E0) ; HI=$(printf "%10.2f" 40.E0)
    FINC=$(printf "%10.2f" 2.5) ; ikax=$(printf "%5d" $kax)
    title="ANOMALY (U)R [M/S]"
    echo "                -10.       -5.      -2.5       2.5        5.       10.      100." >ic.zplotf0
    ccz zxplot zudif
    rm zudif
#
    if [ "$xtradif" = "on" ] ; then
      SCAL=$(printf "%10.2f" 1.E0) ; FLO=$(printf "%10.2f" -1.E2) ; HI=$(printf "%10.2f" 1.E2)
      FINC=$(printf "%10.2f" 0.25) ; ikax=$(printf "%5d" $kax)
      if [ "$colourplots" != "on" ] ; then FINC=$(printf "%10.2f" 5.E-1) ; fi
      title="ANOMALY ((U*U*)R)**0.5 [M/S]"
      echo "                -1.5       -1.     -0.50      0.50       1.0       1.5      100." >ic.zplotf0
      ccz zxplot seusd
      SCAL=$(printf "%10.2f" 1.E0) ; FLO=$(printf "%10.2f" -2.E2) ; HI=$(printf "%10.2f" 2.E2)
      FINC=$(printf "%10.2f" 1.E0) ; ikax=$(printf "%5d" $kax)
      if [ "$colourplots" != "on" ] ; then FINC=$(printf "%10.2f" 2.E0) ; fi
      title="ANOMALY (U*V*)R [M2/S2]"
      echo "                 -6.       -4.       -2.        2.        4.        6.      100." >ic.zplotf0
      ccz zxplot seutran
      if [ "$stat2nd" != "off" ] ; then
# ------------------------------------- u'u'
      echo "XFIND.        U'U'" |ccc xfind filex x
      echo "XFIND.        U'U'" |ccc xfind filey y
#                                       select common set of levels
      comlev x y x_lev y_lev ; mv x_lev x ; mv y_lev y

      sqroot x sdx
      sqroot y sdy
#
      sub sdy sdx dif
      rzonavg dif beta zdif
      rm sdx sdy seusd seutran

      SCAL=$(printf "%10.2f" 1.E0) ; FLO=$(printf "%10.2f" -1.E2) ; HI=$(printf "%10.2f" 1.E2)
      FINC=$(printf "%10.2f" 0.25) ; ikax=$(printf "%5d" $kax)
      if [ "$colourplots" != "on" ] ; then FINC=$(printf "%10.2f" 5.E-1) ; fi
      title="ANOMALY ((U+U+)R)**0.5 [M/S]"
      echo "                -1.5       -1.     -0.50      0.50       1.0       1.5      100." >ic.zplotf0
      ccz zxplot zdif
      rm x y zdif

# ------------------------------------- u'v'
      echo "XFIND.        U'V'" |ccc xfind filex x
      echo "XFIND.        U'V'" |ccc xfind filey y
#                                       select common set of levels
      comlev x y x_lev y_lev ; mv x_lev x ; mv y_lev y
#
      sub y x dif
      rzonavg dif beta zdif

      SCAL=$(printf "%10.2f" 1.E0) ; FLO=$(printf "%10.2f" -2.E2) ; HI=$(printf "%10.2f" 2.E2)
      FINC=$(printf "%10.2f" 1.E0) ; ikax=$(printf "%5d" $kax)
      if [ "$colourplots" != "on" ] ; then FINC=$(printf "%10.2f" 2.E0) ; fi
      title="ANOMALY ((U+V+)R [M2/S2]"
      echo "                 -6.       -4.       -2.        2.        4.        6.      100." >ic.zplotf0
      ccz zxplot zdif
      rm x y dif zdif
      fi
#
#                                         meridional velocity.
#
#                                         v*v*
      SCAL=$(printf "%10.2f" 1.E0) ; FLO=$(printf "%10.2f" -1.E2) ; HI=$(printf "%10.2f" 1.E2)
      FINC=$(printf "%10.2f" 0.25) ; ikax=$(printf "%5d" $kax)
      if [ "$colourplots" != "on" ] ; then FINC=$(printf "%10.2f" 5.E-1) ; fi
      title="ANOMALY ((V+V+)R)**0.5 [M/S]"
      echo "                -1.5       -1.     -0.50      0.50       1.0       1.5      100." >ic.zplotf0
      ccz zxplot sevsd
      rm sevsd
      if [ "$stat2nd" != "off" ] ; then
      echo "XFIND.        V'V'" |ccc xfind filex x
      echo "XFIND.        V'V'" |ccc xfind filey y
#                                         select common set of levels
      comlev x y x_lev y_lev ; mv x_lev x ; mv y_lev y
#
      sub sdy sdx dif
      rzonavg dif beta zdif

      SCAL=$(printf "%10.2f" 1.E0) ; FLO=$(printf "%10.2f" -1.E2) ; HI=$(printf "%10.2f" 1.E2)
      FINC=$(printf "%10.2f" 0.25) ; ikax=$(printf "%5d" $kax)
      if [ "$colourplots" != "on" ] ; then FINC=$(printf "%10.2f" 5.E-1) ; fi
      title="ANOMALY ((V+V+)R)**0.5 [M/S]"
      echo "                -1.5       -1.     -0.50      0.50       1.0       1.5      100." >ic.zplotf0
      ccz zxplot zdif
      rm x y dif zdif
      fi
    fi
#
#                                       vector plots of (u,v).
#                                       850 and 200 mb.
    joinup x ux vx
    joinup y uy vy
    echo "SELECT.                 0        99    1 LEVS  850  850 NAME    U    V " |ccc select x u850x v850x
    echo "SELECT.                 0        99    1 LEVS  200  200 NAME    U    V " |ccc select x u200x v200x
    echo "SELECT.                 0        99    1 LEVS  850  850 NAME    U    V " |ccc select y u850y v850y
    echo "SELECT.                 0        99    1 LEVS  200  200 NAME    U    V " |ccc select y u200y v200y
    rm uy vy ux vx x y
#
    sub u850y u850x udif850
    sub v850y v850x vdif850
    square udif850 u2
    square vdif850 v2
    add u2 v2 amp2
    sqroot amp2 amp
    if [ "$datatype" = "gridsig" ] ; then
       avg=$(echo "C* GLOBAVL${shiftdy}         1"|ccc globavl amp |grep "SURFACE MEAN IS"|head -1|awk '{printf "%10.3f\n",$11}')
    else
       globavg amp _ gavg_amp
       avg=$(ggstat gavg_amp | grep GRID | head -1 | cut -c73-85 | awk '{printf "%g", $1}' | tr -d ' ')
       rm gavg_amp
    fi
    echo "GGPLOT.           -1   -1  850$map        1.        0.       60.        2.2${b}8" >ic.gplotf
    echo "$run $days MEAN 850MB WIND ANOMALY [M/S] $avg                                   " >>ic.gplotf
    echo "                  1.        0.        5.$ncx$ncy                                " >>ic.gplotf
    echo "                  VECPLOT OF (U,V) [M/S]                                        " >>ic.gplotf
    ggplot amp udif850 vdif850 input=ic.gplotf
    rm udif850 vdif850 u2 v2 amp2 amp
#
    sub u200y u200x udif200
    sub v200y v200x vdif200
    square udif200 u2
    square vdif200 v2
    add u2 v2 amp2
    sqroot amp2 amp
    if [ "$datatype" = "gridsig" ] ; then
       avg=$(echo "C* GLOBAVL${shiftdy}         1"|ccc globavl amp |grep "SURFACE MEAN IS"|head -1|awk '{printf "%10.3f\n",$11}')
    else
       globavg amp _ gavg_amp
       avg=$(ggstat gavg_amp | grep GRID | head -1 | cut -c73-85 | awk '{printf "%g", $1}' | tr -d ' ')
       rm gavg_amp
    fi
    echo "GGPLOT.           -1   -1  200$map        1.        0.       60.        2.2${b}8" >ic.gplotf
    echo "$run $days MEAN 200MB WIND ANOMALY [M/S] $avg                                   " >>ic.gplotf
    echo "                  1.        0.       10.$ncx$ncy                                " >>ic.gplotf
    echo "                  VECPLOT OF (U,V) [M/S]                                        " >>ic.gplotf
    ggplot amp udif200 vdif200 input=ic.gplotf
#
    rm udif200 vdif200 u2 v2 amp2 amp
#
#                                       velocity potential at 850 and 200 mbs
#                                       (mean fields).
#
#                                       model run.
    echo "GWTQD  $lrt$lmt$typ    1" |ccc gwtqd u200y v200y vor200 div200
    echo "GWTQD  $lrt$lmt$typ    1" |ccc gwtqd u850y v850y vor850 div850

    splinv div200 nchi200
    splinv div850 nchi850
    rm u200y v200y u850y v850y vor200 div200 vor850 div850
    echo "XLIN.          -1.        0." |ccc xlin nchi200 schi200
    echo "XLIN.          -1.        0." |ccc xlin nchi850 schi850
    echo "COFAGG    $lon$lat" |ccc cofagg schi200 c200y
    echo "COFAGG    $lon$lat" |ccc cofagg schi850 c850y
    rm nchi200 nchi850 schi200 schi850
#
#                                       control run.
    echo "GWTQD  $lrt$lmt$typ    1" |ccc gwtqd u200x v200x vor200 div200
    echo "GWTQD  $lrt$lmt$typ    1" |ccc gwtqd u850x v850x vor850 div850
    splinv div200 nchi200
    splinv div850 nchi850
    rm u200x v200x u850x v850x vor200 div200 vor850 div850
    echo "XLIN.          -1.        0." |ccc xlin nchi200 schi200
    echo "XLIN.          -1.        0." |ccc xlin nchi850 schi850
    echo "COFAGG    $lon$lat" |ccc cofagg schi200 c200x
    echo "COFAGG    $lon$lat" |ccc cofagg schi850 c850x
    rm nchi200 nchi850 schi200 schi850
#
    joinup chi c850y c200y c850x c200x
    if [ "$datatype" = "gridsig" ] ; then
       avg=$(echo "C* GLOBAVL${shiftdy}         1"|ccc globavl chi |grep "SURFACE MEAN IS"|head -1|awk '{printf "%10.3f\n",$11}')
    else
       globavg chi _ gavg_chi
       avg=$(ggstat gavg_chi | grep GRID | head -1 | cut -c73-85 | awk '{printf "%g", $1}' | tr -d ' ')
       rm gavg_chi
    fi
    echo "GGPLOT.           -1 NEXT  850$map     1.E-6     -100.      100.        1.0${b}8" >ic.gplotf
    echo "$run $days EXP 850MB VELOCITY POTENTIAL [1E6*M2/S] $avg                         " >>ic.gplotf
    echo "GGPLOT.           -1 NEXT  200$map     1.E-6     -100.      100.        1.0${b}8" >>ic.gplotf
    echo "$run $days EXP 200MB VELOCITY POTENTIAL [1E6*M2/S] $avg                         " >>ic.gplotf
    echo "GGPLOT.           -1 NEXT  850$map     1.E-6     -100.      100.        1.0${b}8" >>ic.gplotf
    echo "$run $days CNTL 850MB VELOCITY POTENTIAL [1E6*M2/S] $avg                        " >>ic.gplotf
    echo "GGPLOT.           -1 NEXT  200$map     1.E-6     -100.      100.        1.0${b}8" >>ic.gplotf
    echo "$run $days CNTL 200MB VELOCITY POTENTIAL [1E6*M2/S] $avg                        " >>ic.gplotf
    ggplot chi input=ic.gplotf
    rm c200y c850y c200x c850x chi

#   ----------------------------------- clouds

    if [ "$obsdat" != "on" ] ; then
      echo "XFIND.        SCLD" |ccc xfind filex cldx
      echo "XFIND.        SCLD" |ccc xfind filey cldy
      sub cldy cldx clddif
      zonavg clddif zclddif
      SCAL=$(printf "%10.2f" 1.E02) ; FLO=$(printf "%10.2f" -1.E2) ; HI=$(printf "%10.2f" 1.E2)
      FINC=$(printf "%10.2f" 5.) ; ikax=$(printf "%5d" 0)
      title="CLOUDINESS ANOMALY (SCLD)R."
      echo "                -15.      -10.       -5.        5.      10.       15.      100." >ic.zplotf0
      ccz zxplot zclddif
      rm cldx cldy clddif zclddif
    fi

#   ----------------------------------- vertical velocity

    if [ "$obsdat" != "on" ] ; then
      echo "XFIND.        W" |ccc xfind filex omega_x
      echo "XFIND.        W" |ccc xfind filey omega_y
      sub omega_y omega_x omega_diff
      zonavg omega_diff zomega_diff
      SCAL=$(printf "%10.2f" 1.E3) ; FLO=$(printf "%10.2f" -1.E2) ; HI=$(printf "%10.2f" 1.E2)
      FINC=$(printf "%10.2f" 1.) ; ikax=$(printf "%5d" 0)
      title="VERT.MOT. ANOMALY (W)R [1E-3*PA/S]"
      echo "                 -3.       -2.       -1.        1.       2.        3.      100." >ic.zplotf0
      ccz zxplot zomega_diff
      rm omega_x omega_y omega_diff zomega_diff
    fi

#   ----------------------------------- specific humidity.

    echo "XFIND.        Q" |ccc xfind filex x
    echo "XFIND.        Q" |ccc xfind filey y
#                                       select common set of levels
    comlev x y x_lev y_lev ; mv x_lev x ; mv y_lev y

    sub y x dif
    rzonavg dif beta zdif
    SCAL=$(printf "%10.2f" 1.E3) ; FLO=$(printf "%10.2f" -40.00) ; HI=$(printf "%10.2f" 40.00)
    FINC=$(printf "%10.2f" 0.125) ; ikax=$(printf "%5d" 0)
       if [ "$colourplots" != "on" ] ; then FINC=$(printf "%10.2f" 0.25) ; fi
   title="ANOMALY (Q)R [G/KG]"
    echo "                -1.0      -0.5     -0.25      0.25      0.5       1.0      100." >ic.zplotf0
    ccz zxplot zdif
    rm x y dif zdif
#
tailfram=$(cat $(which tailfram.cdk) |sed '1,2d')
echo "$tailfram" > Input_Cards

.   plotid.cdk
.   plot.cdk

rm Input_Cards
