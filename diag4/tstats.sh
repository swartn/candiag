#!/bin/sh
#                  tstats              gjb,ec,dl. jan 24/00 - sk.
#   ---------------------------------- calculate t statistics
#                                      and covariances with u, v and w.
#   sk: re-write for new timavg/xsave, phase out xtrans.
#
      access t ${flabel}_gpt
      access u ${flabel}_gpu
      access v ${flabel}_gpv

      if [ "$datatype" = "specsig" ]; then
	echo "DIFF       TEMP $t1 $t2 $delt" | ccc dif t dtdt
      else
	echo "DIFF       TEMP $t1 $t2 $delt       1.0" | ccc dif t dtdt
      fi
      timavg t  tt tp  ttp2
      timavg u  tu up
      timavg v  tv vp
      timcov tp up ttpup
      timcov tp vp ttpvp

echo "XSAVE.        T
NEWNAM.
XSAVE.        DT/DT
NEWNAM.      T.
XSAVE.        T\"T\"
NEWNAM.    T\"T\"
XSAVE.        T\"U\"
NEWNAM.    T\"U\"
XSAVE.        T\"V\"
NEWNAM.    T\"V\"" > ic.xsave
      vars="tt dtdt ttp2 ttpup ttpvp"

      if [ "$wxstats" != on ]; then
#   ---------------------------------- do calculations for w
         access w  ${flabel}_gpw
         timavg w  tw wp
         timcov tp wp ttpwp
echo "XSAVE.        T\"W\"
NEWNAM.    T\"W\"" >> ic.xsave
         vars="$vars ttpwp"
      fi

#   ---------------------------------- xsave
      xsave new_gp $vars new.
      mv new. new_gp

#   ---------------------------------- save results.
      release oldgp
      access  oldgp ${flabel}gp
      xjoin   oldgp new_gp newgp
      save    newgp ${flabel}gp
      delete  oldgp
