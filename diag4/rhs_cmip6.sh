#!/bin/sh
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Summary of changes.
# Initial version for CMIP6: Jason Cole, 18 January 2019.
# Updated so it will work regardless of data available: Jason Cole, 23 January 2019
# Interpolated some fields onto pressure levels for EmonZ: Jason Cole, 18 February 2019

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Summary of function
#
# Compute the "right-hand side" terms that have been requested for
# CMIP6.
#
# These variables will be generated when using the following cpp directives
# in CanAM.
#
# cpp1 - Save the total tendency
# #define tprhs
# #define qprhs
# #define uprhs
# #define vprhs
# #define xprhs
#
# cpp1 - Save the component tendencies
# #define tprhsc
# #define qprhsc
# #define uprhsc
# #define vprhsc
# #define xprhsc

# cpp1 - Save the dynamic tendencies (t,q,u and v)
# #define dynrhs

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Output variables

# TTP  -> Tendency due to all model physics (temperature)
# TTPV -> Tendency due to vertical diffusion (temperature)
# TTPM -> Tendency due to shallow mixing (temperature)
# TTPC -> Tendency due to deep convection (temperature)
# TTPP -> Tendency due to large-scale condensation (temperature)
# TTPK -> Tendency due to frictional heating (temperature)
# TTPN -> Tendency due to dry convective adjustment (temperature)
# TTPS -> Tendency due to all-sky solar radiation heating (temperature)
# TTPL -> Tendency due to all-sky thermal radiation heating (temperature)
# TTSC -> Tendency due to clear-sky solar radiation heating (temperature)
# TTLC -> Tendency due to clear-sky thermal radiation heating (temperature)
# TTD  -> Tendency due to advection (temperature)
# TTOT -> Total tendency (temperature)

# QTP  -> Tendency due to all model physics (moisture)
# QTPV -> Tendency due to vertical diffusion (moisture)
# QTPM -> Tendency due to shallow mixing (moisture)
# QTPC -> Tendency due to deep convection (moisture)
# QTPP -> Tendency due to large-scale condensation (moisture)
# ESTD -> Tendency due to advection (moisture)
# QTOT -> Total tendency (moisture)

# UTP  -> Tendency due to all model physics (zonal wind)
# UTPV -> Tendency due to vertical diffusion (zonal wind)
# UTPG -> Tendency due to orographic gravity wave drag (zonal wind)
# UTPC -> Tendency due to deep convection (zonal wind)
# UTPS -> Tendency due to roof-drag and/or sponge (zonal wind)
# UTPN -> Tendency due to non-orographic gravity wave drag (zonal wind)
# UTD  -> Tendency due to advection (zonal wind)
# UTOT -> Total tendency (zonal wind)

# VTP  -> Tendency due to all model physics (meridional wind)
# VTPV -> Tendency due to vertical diffusion (meridional wind)
# VTPG -> Tendency due to orographic gravity wave drag (meridional wind)
# VTPC -> Tendency due to deep convection (meridional wind)
# VTPS -> Tendency due to roof-drag and/or sponge (meridional wind)
# VTPN -> Tendency due to non-orographic gravity wave drag (meridional wind)
# VTD  -> Tendency due to advection (meridional wind)
# VTOT -> Total tendency (meridional wind)

# PT*  -> Total physics tracer tendency
# PM*  -> Physics tracer tendency due to shallow mixing
# PC*  -> Physics tracer tendency due to deep convection
# PP*  -> Physics tracer tendency due to large-scale condensation
# PV*  -> Physics tracer tendency due to vertical diffusion

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Input variables

# TTP  -> Tendency due to all model physics (temperature)
# TTPV -> Tendency due to vertical diffusion (temperature)
# TTPM -> Tendency due to shallow mixing (temperature)
# TTPC -> Tendency due to deep convection (temperature)
# TTPP -> Tendency due to large-scale condensation (temperature)
# TTPK -> Tendency due to frictional heating (temperature)
# TTPN -> Tendency due to dry convective adjustment (temperature)
# TTPS -> Tendency due to all-sky solar radiation heating (temperature)
# TTPL -> Tendency due to all-sky thermal radiation heating (temperature)
# TTSC -> Tendency due to clear-sky solar radiation heating (temperature)
# TTLC -> Tendency due to clear-sky thermal radiation heating (temperature)
# TTD  -> Tendency due to advection (temperature)

# QTP  -> Tendency due to all model physics (moisture)
# QTPV -> Tendency due to vertical diffusion (moisture)
# QTPM -> Tendency due to shallow mixing (moisture)
# QTPC -> Tendency due to deep convection (moisture)
# QTPP -> Tendency due to large-scale condensation (moisture)
# ESTD -> Tendency due to advection (moisture)

# UTP  -> Tendency due to all model physics (zonal wind)
# UTPV -> Tendency due to vertical diffusion (zonal wind)
# UTPG -> Tendency due to orographic gravity wave drag (zonal wind)
# UTPC -> Tendency due to deep convection (zonal wind)
# UTPS -> Tendency due to roof-drag and/or sponge (zonal wind)
# UTPN -> Tendency due to non-orographic gravity wave drag (zonal wind)
# UTD  -> Tendency due to advection (zonal wind)

# VTP  -> Tendency due to all model physics (meridional wind)
# VTPV -> Tendency due to vertical diffusion (meridional wind)
# VTPG -> Tendency due to orographic gravity wave drag (meridional wind)
# VTPC -> Tendency due to deep convection (meridional wind)
# VTPS -> Tendency due to roof-drag and/or sponge (meridional wind)
# VTPN -> Tendency due to non-orographic gravity wave drag (meridional wind)
# VTD  -> Tendency due to advection (meridional wind)

# PT*  -> Total physics tracer tendency
# PM*  -> Physics tracer tendency due to shallow mixing
# PC*  -> Physics tracer tendency due to deep convection
# PP*  -> Physics tracer tendency due to large-scale condensation
# PV*  -> Physics tracer tendency due to vertical diffusion

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Internal variables

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Being code/script

# ===============================================================
# Access ss and td files
# To try and make the script robust only try to split the "td"
# files if they are present.
# ===============================================================

.   spfiles.cdk
.   ggfiles.cdk

if [ "${tdfiles}" == "on" ] ; then
.   tdfiles.cdk
else
  echo "tdfiles not set to on."
  echo "If you generated them and want them processed set tdfiles=on in canesm.cfg"
fi

# ===============================================================
# Read in the variables from the td file.
# ===============================================================

# Variable to hold all variables that are present
outvars=""

# Temperature, specific humidity, U and V
tdvars1="ttp qtp utp vtp \
         ttpv ttpm ttpc ttpp ttpk ttpn ttps ttpl ttsc ttlc \
         qtpv qtpm qtpc qtpp \
         utpv utpg utpc utps utpn \
         vtpv vtpg vtpc vtps vtpn"

TDVARS1=`fmtselname $tdvars1`
echo "C*SELECT  STEPS $t1 $t2 $t3 LEVS-9100 1000 NAME$TDVARS1" | ccc select npaktd $tdvars1 || true

for ivar in ${tdvars1} ; do
   if [ -s $ivar ] ; then
     outvars="$outvars $ivar"
   fi
done

# Tracers (Can be larger or smaller)
tdvars2="pt01 pm01 pc01 pp01 pv01 pt02 pm02 pc02 pp02 pv02 \
         pt03 pm03 pc03 pp03 pv03 pt04 pm04 pc04 pp04 pv04 \
         pt05 pm05 pc05 pp05 pv05 pt06 pm06 pc06 pp06 pv06 \
         pt07 pm07 pc07 pp07 pv07 pt08 pm08 pc08 pp08 pv08 \
         pt09 pm09 pc09 pp09 pv09 pt10 pm10 pc10 pp10 pv10 \
         pt11 pm11 pc11 pp11 pv11 pt12 pm12 pc12 pp12 pv12 \
         pt13 pm13 pc13 pp13 pv13 pt14 pm14 pc14 pp14 pv14 \
         pt15 pm15 pc15 pp15 pv15 pt16 pm16 pc16 pp16 pv16"

TDVARS2=`fmtselname $tdvars2`

echo "C*SELECT  STEPS $t1 $t2 $t3 LEVS-9100 1000 NAME$TDVARS2" | ccc select npaktd $tdvars2 || true

for ivar in ${tdvars2} ; do
   if [ -s $ivar ] ; then
     outvars="$outvars $ivar"
   fi
done

# ===============================================================
# Read in the variables from the ss file.
# ===============================================================

# Process total and advective tendencies

# Select spectral variables needed for tendencies
ssvars1="llns lnsp ltmp les lvor ldiv \
         ttd estd ptd ctd"
ssvars2="ssllns sslnsp ssltmp ssles sslvor ssldiv \
         ssttd ssestd ssptd ssctd"

SSVARS1=`fmtselname $ssvars1`

echo "C*SELECT  STEPS $t1 $t2 $t3 LEVS-9100 1000 NAME$SSVARS1" | ccc select npaksp $ssvars2 || true

# ===============================================================
# Read in the variables from the gs file.
# ===============================================================

gsvars1="rh"
gsvars2="rhml"

GSVARS1=`fmtselname $gsvars1`

echo "C*SELECT  STEPS $t1 $t2 $t3 LEVS-9100 1000 NAME$GSVARS1" | ccc select npakgg $gsvars2 || true

outvars="${outvars} rhml"

# To save some time we must use "gssave=on" so that some of the data used below
# does not need to be recomputed, it can just be read from file.

if [ "$gssave" = "on" ] ; then
   access gsshum ${flabel}_gsq
   access gstemp ${flabel}_gst
   access gsu    ${flabel}_gsu
   access gsv    ${flabel}_gsv

# Make a copy of the temperature and specific humidity files so they can be used in
# statsav with the correct names.
   ln gsshum q_ml
   ln gstemp t_ml
   outvars="${outvars} q_ml t_ml"

else
   echo "It is necessary to have gssave=on for rhs_cmip6."
   exit 1
fi

# Compute total tendencies

# The computation of total tendencies is "all or nothing" so we check if
# ssltmp exists.  If it doesn't, don't do the diagnostic and write a message
# stating this.

if [ -s ssltmp ] ; then

 # Compute values needed to compute the total tendency
  intstp=$issp ;  # model output interval in time steps (24 ~ 6 hours)
  intout=`echo $intstp $delt | awk '{printf "%10.0f",$1 * $2}'` ;
  ilevp1=`echo $ilev | awk '{printf "%10d",$1+1}'`

 # Pressure
     echo "C*  NEWNAM LNSP" | ccc newnam ssllns ssllns1 ; mv ssllns1 ssllns
     echo "COFAGG.   $lon$lat    0    1" > .card_cofagg
     cofagg ssllns gsllns input=.card_cofagg
     cofagg sslnsp gslnsp input=.card_cofagg
     rm .card_cofagg

 # compute lnsp in the middle of saving interval as geometric mean
     echo "C* RCOPY           2 999999999" | ccc rcopy gslnsp gslnsp1
     cat gsllns >> gslnsp1
     mlt gslnsp1 gslnsp gslnsp2
     sqroot gslnsp2 gslnsph
     rm gslnsp1 gslnsp2

 # Temperature
     echo "C*  NEWNAM TEMP" | ccc newnam ssltmp ssltmp1 ; mv ssltmp1 ssltmp
     echo "COFAGG.   $lon$lat    0$npg" > .card_cofagg
     cofagg ssltmp gsltmp input=.card_cofagg
     echo "          $ilevp1 999999999" | ccc rcopy gstemp gstemp1
     cat gsltmp >> gstemp1
     sub gstemp1 gstemp gsttot1
     echo "          $intout      0.E0 TTOT    1" | ccc xlin gsttot1 gsttot
     mv gsttot ttot
     rm gstemp1 gsttot1 .card_cofagg

 # Specific Humidity
     echo "C*  NEWNAM   ES" | ccc newnam ssles ssles1 ; mv ssles1 ssles
     echo "COFAGG.   $lon$lat    0$npg" > .card_cofagg
     cofagg ssles gsles input=.card_cofagg
     echo "GSHUMH.   $coord$moist$plid$sref$spow" > .card_gshumh
     gshumh gsles gsltmp gsllns gslshm gslrhm input=.card_gshumh
     echo "          $ilevp1 999999999" | ccc rcopy gsshum gsshum1
     cat gslshm >> gsshum1
     sub gsshum1 gsshum gsqtot1
     echo "          $intout      0.E0 QTOT    1" | ccc xlin gsqtot1 gsqtot
     mv gsqtot qtot
     rm gsshum1 gsqtot1 .card_cofagg .card_gshumh

 # Winds
     echo "C*  NEWNAM VORT" | ccc newnam sslvor sslvor1 ; mv sslvor1 sslvor
     echo "C*  NEWNAM  DIV" | ccc newnam ssldiv ssldiv1 ; mv ssldiv1 ssldiv
     cwinds sslvor ssldiv sslbigu sslbigv
     echo "COFAGG.   $lon$lat    1$npg" > .card_cofagg
     cofagg sslbigu gslu input=.card_cofagg
     cofagg sslbigv gslv input=.card_cofagg

     echo "          $ilevp1 999999999" | ccc rcopy gsu gsu1
     echo "          $ilevp1 999999999" | ccc rcopy gsv gsv1
     cat gslu >> gsu1
     cat gslv >> gsv1
     sub gsu1 gsu gsutot1
     sub gsv1 gsv gsvtot1
     echo "          $intout      0.E0 UTOT    1" | ccc xlin gsutot1 gsutot
     echo "          $intout      0.E0 VTOT    1" | ccc xlin gsvtot1 gsvtot
     mv gsutot utot
     mv gsvtot vtot
     rm gsu1 gsutot1 gsv1 gsvtot1 .card_cofagg

     outvars="${outvars} ttot qtot utot vtot"
else

  echo "We did not compute total tendencies in rhs_cmip6.dk due to lack of required inputs from the model."

fi

# Compute tendencies due to advection

# The computation of advective tendencies is "all or nothing" so we check if
# ssttd exists.  If it doesn't, don't do the diagnostic and write a message
# stating this.

if [ -s ssttd ] ; then

 # Temperature and specific humidity
     echo "COFAGG.   $lon$lat    0$npg" > .card_cofagg
     cofagg ssttd  gsttd input=.card_cofagg
     cofagg ssestd gsestd input=.card_cofagg

     echo "C*  NEWNAM  TTD" | ccc newnam gsttd gsttd1 ; mv gsttd1 ttd
     echo "C*  NEWNAM ESTD" | ccc newnam gsestd gsestd1 ; mv gsestd1 estd

rm .card_cofagg

 # Winds
     echo "C*  NEWNAM VORT" | ccc newnam ssptd ssptd1 ; mv ssptd1 ssptd
     echo "C*  NEWNAM  DIV" | ccc newnam ssctd ssctd1 ; mv ssctd1 ssctd
     cwinds ssptd ssctd ssbutd ssbvtd

     echo "C*  NEWNAM  UTD" | ccc newnam ssbutd ssbutd1
     echo "C*  NEWNAM  VTD" | ccc newnam ssbvtd ssbvtd1
     echo "COFAGG.   $lon$lat    1$npg" > .card_cofagg
     cofagg ssbutd1 gsutd input=.card_cofagg
     cofagg ssbvtd1 gsvtd input=.card_cofagg

     echo "C*  NEWNAM  UTD" | ccc newnam gsutd gsutd1 ; mv gsutd1 utd
     echo "C*  NEWNAM  VTD" | ccc newnam gsvtd gsvtd1 ; mv gsvtd1 vtd

     outvars="${outvars} ttd estd utd vtd"
else

  echo "We did not compute advective tendencies in rhs_cmip6.dk due to lack of required inputs from the model."

fi

# ===============================================================
# Interpolate a subset of the variables to pressure levels.
# This is to support the EmonZ data request.
# ===============================================================

            echo "GSAPL.    $plv       0.0       0.0$coord$plid
$p01$p02$p03$p04$p05$p06$p07$p08$p09$p10$p11$p12$p13$p14$p15$p16
$p17$p18$p19$p20$p21$p22$p23$p24$p25$p26$p27$p28$p29$p30$p31$p32
$p33$p34$p35$p36$p37$p38$p39$p40$p41$p42$p43$p44$p45$p46$p47$p48
$p49$p50$p51$p52$p53$p54$p55$p56$p57$p58$p59$p60$p61$p62$p63$p64
$p65$p66$p67$p68$p69$p70$p71$p72$p73$p74$p75$p76$p77$p78$p79$p80
$p81$p82$p83$p84$p85$p86$p87$p88$p89$p90$p91$p92$p93$p94$p95$p96
$p97$p98$p99$p100" > .gsapl_input_card

intpvars="ttp ttps ttpl ttsc ttlc ttpc ttpm"
for ivar in ${intpvars} ; do
   gsapl ${ivar}  gslnsp gp${ivar} input=.gsapl_input_card || true

   if [ -s gp$ivar ] ; then
     outvars="$outvars gp$ivar"
   fi
done

# ===============================================================
# Save the results.
#
# Only if output variables were generated in this deck.
# ===============================================================

echo "outvars " ${outvars}

if [ ! -z "${outvars}" ] ; then

  statsav ${outvars} new_gp new_zp $stat2nd

  access oldgp ${flabel}gp
  xjoin oldgp new_gp newgp
  save newgp ${flabel}gp
  delete oldgp na

  if [ "$rcm" != "on" ] ; then
    access oldzp ${flabel}zp
    xjoin oldzp new_zp newzp
    save newzp ${flabel}zp
    delete oldzp
  fi

fi
