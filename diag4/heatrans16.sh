#!/bin/sh
#
#
#                  heatrans16          ml.
#                             Aug 12/18- Remove OBEG for CMIP6.
#                             Nov 14/17- Remove RES for CMIP6.
#                             Jun 24/12- Include additional calculation
#                                        of BEGA (xfind: FSG,FLG,HFS,PCP,PCPN).
#                             Jun 01/12- Previous version heatrans15:
#                                        superlabels adjusted for
#                                        new Ceres_2.6r dataset.
#  ----------------------------------- compute annual implied heat transport
#                                      from agcm surface net energy balance
#                                      (beg). it needs beg from each season.
#                                      this computation is done for the global
#                                      ocean as well as for each regional ocean
#

# --- Determine target grid dimensions
    nlon=`echo "$resol" | cut -f1 -d'_' | $AWK '{printf "%5i", $1}'`
    nlat=`echo "$resol" | cut -f2 -d'_' | $AWK '{printf "%5i", $1}'`

#
# --- Define ccg function to add global means to ggplot plots
#
ccg(){
  if [ "$datatype" = "gridsig" ] ; then
     avg=$(echo "C* GLOBAVL${shiftdy}         1"|ccc globavl $2 |grep "SURFACE MEAN IS"|head -1|awk '{printf "%10.3f\n",$11}')
  else
     globavg $2 _ gavg_$2
     avg=$(ggstat gavg_$2 | grep GRID | head -1 | cut -c73-85 | awk '{printf "%g", $1}' | tr -d ' ')
     rm gavg_$2
  fi
  echo "GGPLOT$name        $idx NEXT  $ILVL$ICH1$ICH2$FLO$HI$FINC${b}8" >ic.gplotf
  echo "$run $days $title $avg                                                   " >>ic.gplotf
  cat ic.gplotf0 >>ic.gplotf
  ggplot $2 input=ic.gplotf
}

# --- Define a function to print out global average
printglb(){
  if [ "$datatype" = "gridsig" ] ; then
     glbline1=$(echo "C* GLOBAVL${shiftdy}         1"|ccc globavl $1 |head -1 |sed 's/,input=.ccc_cards//'| awk '{printf "%-30s",$2}')
     glbline2=$(echo "C* GLOBAVL${shiftdy}         1"|ccc globavl $1 |grep "SURFACE MEAN IS"|head -1| cut -c45-73)
  else
     glbline1=$(globavg $1 | head -1 | awk '{printf "%-30s",$2}')
     glbline2=$(globavg $1 | grep SURFACE | head -1 | cut -c45-73)
  fi
  echo "$glbline1$glbline2"
  echo "$glbline1$glbline2" >> $2
}

#   ---------------------------------- standard plots.
#
headframe=$(cat $(which headfram.cdk) |sed '1,2d')
echo "$headframe" > Input_Cards
echo "         DAYS      = $days     ">> Input_Cards
echo "         RUN       = $runid    ">> Input_Cards
echo "         MODEL     = $model    ">> Input_Cards
echo "         OBS_CERES = $obs_ceres">> Input_Cards
echo "         OBS_ERA   = $obs_era  ">> Input_Cards

#    libncar plunit=$plunit
.   plotid.cdk
rm Input_Cards

#
    access ggw ${model}djf_gp
    access ggp ${model}mam_gp
    access ggs ${model}jja_gp
    access ggf ${model}son_gp
#
    access obw ${obs_ceres}djf
    access obp ${obs_ceres}mam
    access obs ${obs_ceres}jja
    access obf ${obs_ceres}son
#
    access obsw ${obs_era}djf_gp
    access obsp ${obs_era}mam_gp
    access obss ${obs_era}jja_gp
    access obsf ${obs_era}son_gp

#   ---------------------------------- print out datasets.
    rm -f dummy
    echo "DATASETS"             >> dummy
    echo "MODEL=$model"         >> dummy
    echo "REANAL=$obs_era"      >> dummy
    echo "CERES=$obs_ceres"     >> dummy
    txtplot input=dummy
    rm dummy
#
    echo "XFIND.        BEG" | ccc xfind ggw begw
    echo "XFIND.        BEG" | ccc xfind ggp begp
    echo "XFIND.        BEG" | ccc xfind ggs begs
    echo "XFIND.        BEG" | ccc xfind ggf begf
#
    echo "XFIND.        BALT" | ccc xfind ggw baltw
    echo "XFIND.        BALT" | ccc xfind ggp baltp
    echo "XFIND.        BALT" | ccc xfind ggs balts
    echo "XFIND.        BALT" | ccc xfind ggf baltf
#
    echo "XFIND.        FSAM" | ccc xfind ggw fsamw
    echo "XFIND.        FSAM" | ccc xfind ggp fsamp
    echo "XFIND.        FSAM" | ccc xfind ggs fsams
    echo "XFIND.        FSAM" | ccc xfind ggf fsamf
#
    echo "XFIND.        FLAM" | ccc xfind ggw flamw
    echo "XFIND.        FLAM" | ccc xfind ggp flamp
    echo "XFIND.        FLAM" | ccc xfind ggs flams
    echo "XFIND.        FLAM" | ccc xfind ggf flamf
#                         "BALTX" is net radiation budget at top of
#                         model vertical domain, not TOA!
    add fsamw flamw famw
    add fsamp flamp famp
    add fsams flams fams
    add fsamf flamf famf
    sub baltw famw baltxw
    sub baltp famp baltxp
    sub balts fams baltxs
    sub baltf famf baltxf
    rm fsamw fsamp fsams fsamf flamw flamp flams flamf famw famp fams famf
#
    echo "XFIND.        FSG
XFIND.        FLG
XFIND.        HFS
XFIND.        PCP
XFIND.        PCPN" | ccc xfind ggw fsgw flgw hfsw pcpw pcpnw
    echo "XFIND.        FSG
XFIND.        FLG
XFIND.        HFS
XFIND.        PCP
XFIND.        PCPN" | ccc xfind ggp fsgp flgp hfsp pcpp pcpnp
    echo "XFIND.        FSG
XFIND.        FLG
XFIND.        HFS
XFIND.        PCP
XFIND.        PCPN" | ccc xfind ggs fsgs flgs hfss pcps pcpns
    echo "XFIND.        FSG
XFIND.        FLG
XFIND.        HFS
XFIND.        PCP
XFIND.        PCPN" | ccc xfind ggf fsgf flgf hfsf pcpf pcpnf
    rm ggw ggp ggs ggf
#
    echo "XFIND.        Total-sky TOA Net Flux  (W/m2)" | ccc xfind obw obs_baltw
    echo "XFIND.        Total-sky TOA Net Flux  (W/m2)" | ccc xfind obp obs_baltp
    echo "XFIND.        Total-sky TOA Net Flux  (W/m2)" | ccc xfind obs obs_balts
    echo "XFIND.        Total-sky TOA Net Flux  (W/m2)" | ccc xfind obf obs_baltf
    rm obw obp obs obf
#                                       interpolate to target resolution
    if [ "$datatype" = "gridsig" ] ; then
      #ryj: for interpolating obs to GU grid
      shiftdy=$(echo $nlat |awk '{printf "%10.5f", 90.0/$1}')
      mlat=`echo $resol | cut -f2 -d'_' | $AWK '{printf "%5i", -$1}'`
      for s in w p s f ; do
        printf "LLAGG     $nlon$mlat   -5    0     1.E38      0.5       0.5     1\nC* LLAGG  ${shiftdy}        0.    0" | ccc llagg obs_balt$s obs_balt$s.1 ; mv obs_balt$s.1 obs_balt$s
      done
    else
      for s in w p s f ; do
        echo "LLAGG     $nlon$nlat   -5    0     1.E38       0.5       0.5    1" | ccc llagg obs_balt$s obs_balt$s.1 ; mv obs_balt$s.1 obs_balt$s
      done
    fi

#                                       select surface fluxes from ERA
    echo "XFIND.        SRAD
XFIND.        LRAD
XFIND.        SH
XFIND.        LH" | ccc xfind obsw obs_fsgw obs_flgw obs_hfsw obs_hflw
    echo "XFIND.        SRAD
XFIND.        LRAD
XFIND.        SH
XFIND.        LH" | ccc xfind obsp obs_fsgp obs_flgp obs_hfsp obs_hflp
    echo "XFIND.        SRAD
XFIND.        LRAD
XFIND.        SH
XFIND.        LH" | ccc xfind obss obs_fsgs obs_flgs obs_hfss obs_hfls
    echo "XFIND.        SRAD
XFIND.        LRAD
XFIND.        SH
XFIND.        LH" | ccc xfind obsf obs_fsgf obs_flgf obs_hfsf obs_hflf
    rm obsw obsp obss obsf

#                                       compute BEG in ERA from individual fluxes.
    for s in w p s f ; do
      add obs_fsg$s  obs_flg$s  obs_balg$s
      add obs_hfs$s  obs_hfl$s  obs_hfsl$s
      add obs_balg$s obs_hfsl$s obs_beg$s
    done

#                                       interpolate to target resolution
    if [ "$datatype" = "gridsig" ] ; then
      for s in w p s f ; do
        printf "LLAGG     $nlon$mlat    5                                       1\nC* LLAGG  ${shiftdy}        0.    0" | ccc llagg obs_beg$s obs_beg$s.1 ; mv obs_beg$s.1 obs_beg$s
      done
    else
      for s in w p s f ; do
        echo "LLAGG     $nlon$nlat    5                                       1" | ccc llagg obs_beg$s obs_beg$s.1 ; mv obs_beg$s.1 obs_beg$s
      done
    fi
#                                       print seasonal global means
    echo "SEASONAL MEANS" >> dummy
    for v in balt obs_balt beg obs_beg ; do
      for s in w p s f ; do
        printglb $v$s dummy
      done
    done
#
#  ----------------------------- compute annual means
#
    echo "ANNUAL MEANS" >> dummy
    for v in baltx beg balt obs_beg obs_balt ; do
      joinup ${v} ${v}w ${v}p ${v}s ${v}f
      echo "C* BINSML     4              4
C* BINSML    90   92   92   91" | ccc binsml ${v} ${v}a
#                                       print annual global means
      printglb ${v}a dummy
      rm ${v}w ${v}p ${v}s ${v}f
    done
    rm baltx beg balt obs_beg obs_balt
#
#  ----------------------------- compute annual means for bega terms.
#
    for v in fsg flg hfs pcp pcpn ; do
      joinup ${v} ${v}w ${v}p ${v}s ${v}f
      echo "C* BINSML     4              4
C* BINSML    90   92   92   91" | ccc binsml ${v} ${v}a
      rm ${v}w ${v}p ${v}s ${v}f
    done
    add fsga flga x1
    sub pcpa pcpna pcpra
    echo "XLIN        2.501E6" | ccc xlin pcpra x2
    echo "XLIN        2.501E6" | ccc xlin pcpna x3
    add x2 x3 lata
    sub x1 hfsa x4
    sub x4 lata x5
    echo "NEWNAM.   ABEG" | ccc newnam x5 abeg
    rm fsga flga hfsa pcpa pcpna pcpra lata x1 x2 x3 x4 x5
    printglb abeg dummy

#
#  ------------------------------------ make sea-land masks
#                                       land and ocean separately
#
#  ------------------------------------ table used to extract masks.
#                                       must be consistent with content of the
#                                       file contaning the masks.
#
#       land ------------> 0.0
#       atlantic north --> 1.0
#                south --> 2.0
#       pacific  north --> 3.0
#                south --> 4.0
#       indian   north --> 5.0
#                south --> 6.0
#       mediterranean ---> 8.0
#       hudson bay ------> 9.0
#
    access all.masks $ocean_mask_file
#  ---------------------------------- make masks for global ocean
#                                     1- grid mask (to be used for computation)
    echo "FMASK.               NEXT   GT       0.5" | ccc fmask all.masks glob.oceans
    cp glob.oceans tempo.mask
#                                     2- zonal mask (to be used for display)
#                                       (-1e6 is put at latitude where there
#                                       is no occurence of ocean. xplot uses
#                                       this as a flag to do not plot at
#                                       those latitudes)
#
    rzonavg glob.oceans tempo.mask rzon.mask
    echo "FMASK.               NEXT   LT       0.5" | ccc fmask rzon.mask rzon.mask.tempo
    echo "XLIN         -1.E6" | ccc xlin rzon.mask.tempo glob.oceans.zonm
    rm rzon.mask rzon.mask.tempo
#  ---------------------------------- make mask for atlantic ocean
    echo "FMASK.               NEXT   LT       2.5" | ccc fmask all.masks lt_2.5
    mlt glob.oceans lt_2.5 atl.ocean
    cp atl.ocean tempo.mask
#
    rzonavg atl.ocean tempo.mask rzon.mask
    echo "FMASK.               NEXT   LT       0.5" | ccc fmask rzon.mask rzon.mask.tempo
    echo "XLIN         -1.E6" | ccc xlin rzon.mask.tempo atl.ocean.zonm
    rm lt_2.5 tempo.mask rzon.mask.tempo
#  ---------------------------------- make mask for pacific ocean
    echo "FMASK.               NEXT   LT       2.5" | ccc fmask all.masks gt_2.5
    echo "FMASK.               NEXT   LT       4.5" | ccc fmask all.masks lt_4.5
    mlt gt_2.5 lt_4.5 pac.ocean
    cp pac.ocean tempo.mask
#
    rzonavg pac.ocean tempo.mask rzon.mask
    echo "FMASK.               NEXT   LT       0.5" | ccc fmask rzon.mask rzon.mask.tempo
    echo "XLIN         -1.E6" | ccc xlin rzon.mask.tempo pac.ocean.zonm
    rm gt_2.5 lt_4.5 tempo.mask rzon.mask
#  ---------------------------------- make mask for indian ocean
    echo "FMASK.               NEXT   LT       4.5" | ccc fmask all.masks gt_4.5
    echo "FMASK.               NEXT   LT       6.5" | ccc fmask all.masks lt_6.5
    mlt gt_4.5 lt_6.5 ind.ocean
    cp ind.ocean tempo.mask
#
    rzonavg ind.ocean tempo.mask rzon.mask
    echo "FMASK.               NEXT   LT       0.5" | ccc fmask rzon.mask rzon.mask.tempo
    echo "XLIN         -1.E6" | ccc xlin rzon.mask.tempo ind.ocean.zonm
    rm gt_4.5 lt_6.5 tempo.mask rzon.mask
#  ---------------------------------- make mask for other basins
    echo "FMASK.               NEXT   GT       6.5" | ccc fmask all.masks other.basins
    cp other.basins tempo.mask
#
    rzonavg other.basins tempo.mask rzon.mask
    echo "FMASK.               NEXT   GT       0.5" | ccc fmask rzon.mask rzon.mask.tempo
    echo "XLIN         -1.E6" | ccc xlin rzon.mask.tempo other.basins.zonm
    rm tempo.mask rzon.mask
#
# #########################################################################
#  ---------------------------- compute implied transport over ocean(s)   #
#                                                                         #
#                                t=int{2pi.a**2.[beta.h].cos(phi).dphi}   #
#                               where h=bega. the latitudinal integration #
#                               is done southward from the north pole.    #
# #########################################################################
#
#  ---------------------------------- 1- computation for global oceans
#
    echo "FMASK.               NEXT   LE     1.E30" | ccc fmask obs_bega obs_mask
    mlt bega glob.oceans begao
    mlt obs_bega obs_mask obs_begao
    printglb begao dummy
    printglb obs_begao dummy
    zonavg begao zbego
    rzonavg begao glob.oceans zbego_plot
    rzonavg obs_begao obs_mask z_obs_begao_plot
    zonavg balta zbalta
    echo "ZLATINT     GAU   NP" | ccc zlatint zbego zlbego
    echo "XLIN    2.5505E14" | ccc xlin zlbego iheatrans
# -----------------------------------    plot the obeg on a full grid
    idx=$(printf "%2d" 0)
    ILVL=$(printf "%4d" 13) ; ICH1=$(printf "%4d" 0) ; ICH2=$(printf "%10.1f" 1.)
    FLO=$(printf "%10.1f" -400.) ; HI=$(printf "%10.1f" 400.)
    FINC=$(printf "%11.1f" 25.0)
    echo "              7  186  183  180  140  150  153  156                              " >ic.gplotf0
    echo "               -125.      -75.      -25.       25.       75.      125.          " >>ic.gplotf0
    title="MODEL OBEG [W/M2]" ; name=".   "
ccg ggplot begao
    title="ERA BEG [W/M2]" ; name=".   "
ccg ggplot obs_begao
    joinup plts_beg zbego_plot z_obs_begao_plot
    echo "XMPLOTBEG          0 NEXT    1     -150.      150.    1    2                   8" >ic.plts
    echo "$run $days MOD,OBS(R) OCEAN OBEG[W/M2]                                         " >>ic.plts
    echo "              2   99  100                                                      " >>ic.plts
    xmplot plts_beg input=ic.plts
    rm -f zbego_plot z_obs_begao_plot

    echo " XPLOT.            0 NEXT    1     -150.      150.                              " >ic.plts
    echo "$run $days BALT [W/M2]                                                          " >>ic.plts
    xplot zbalta input=ic.plts
# -----------------------------------    plot the implied transport
    echo "XLIN      1.0E-13" | ccc xlin iheatrans iheatrans.glob
    add iheatrans.glob glob.oceans.zonm trans.plot.glob
    echo " XPLOT.            0 NEXT    1     -400.      400.    1     -1.E5               " >ic.plts
    echo "$run $days IMPLIED HEAT TRANSP.(ALL BASINS)[1E13W]                              " >>ic.plts
    xplot trans.plot.glob input=ic.plts
    rm -f zbalta trans.plot.glob
#
# -----------------------------------    recomputed implied transport
#                                        with unbiased beg.
#                                        (unbalanced energy removed)
    globavg zbego unbalanced.beg
    echo "XYLIN        1.       -1.        0." | ccc xylin zbego unbalanced.beg corec.zbego
    echo "ZLATINT     GAU   NP" | ccc zlatint corec.zbego corec.zlbego
    echo "XLIN    2.5505E14" | ccc xlin corec.zlbego corec.iheatrans
    printglb corec.zbego dummy

    txtplot input=dummy
#
    echo "XLIN      1.0E-13" | ccc xlin corec.iheatrans corec.iheatrans.glob
    add corec.iheatrans.glob glob.oceans.zonm corec.trans.plot.glob
    echo " XPLOT.            0 NEXT    1     -400.      400.    1     -1.E5               " >ic.plts
    echo "$run $days CORRECTED IMPLIED HEAT TRANSP.[1E13W]                                " >>ic.plts
    xplot corec.trans.plot.glob input=ic.plts
#
#
#  put a release here!
    rm corec.iheatrans corec.iheatrans.glob corec.trans.plot.glob corec.zbego corec.zlbego
    rm glob.oceans glob.oceans.zonm begao zbego zlbego iheatrans
#
#
#  ---------------------------------- 2- computation for the atlantic ocean
#                                                           ==============
    mlt bega atl.ocean begao
    zonavg begao zbego
    echo "ZLATINT     GAU   NP" | ccc zlatint zbego zlbego
    echo "XLIN    2.5505E14" | ccc xlin zlbego iheatrans
# -----------------------------------   plot the implied transport
    echo "XLIN       1.0E-13" | ccc xlin iheatrans iheatrans.atl
    add iheatrans.atl atl.ocean.zonm trans.plot.atl
    echo " XPLOT.            0 NEXT    1     -400.      400.    1     -1.E5               " >ic.plts
    echo "$run $days IMPLIED HEAT TRANSP.(ATLANTIC)[1E13W]                                " >>ic.plts
    xplot trans.plot.atl input=ic.plts
    rm atl.ocean atl.ocean.zonm begao zbego zlbego iheatrans
#
#
#  ---------------------------------- 3- computation for the pacific ocean
#                                                           =============
    mlt bega pac.ocean begao
    zonavg begao zbego
    echo "ZLATINT     GAU   NP" | ccc zlatint zbego zlbego
    echo "XLIN    2.5505E14" | ccc xlin zlbego iheatrans
# #---------------------------- plot the implied transport
    echo "XLIN      1.0E-13" | ccc xlin iheatrans iheatrans.pac
    add iheatrans.pac pac.ocean.zonm trans.plot.pac
    echo " XPLOT.            0 NEXT    1     -400.      400.    1     -1.E5               " >ic.plts
    echo "$run $days IMPLIED HEAT TRANSP.(PACIFIC)[1E13W]                                 " >>ic.plts
    xplot trans.plot.pac input=ic.plts
    rm pac.ocean pac.ocean.zonm begao zbego zlbego iheatrans
#
#
#  ---------------------------------- 4- computation for the indian ocean
#                                                           ============
    mlt bega ind.ocean begao
    zonavg begao zbego
    echo "ZLATINT     GAU   NP" | ccc zlatint zbego zlbego
    echo "XLIN    2.5505E14" | ccc xlin zlbego iheatrans
# #----------------------------------   plot the implied transport
    echo "XLIN      1.0E-13" | ccc xlin iheatrans iheatrans.ind
    add iheatrans.ind ind.ocean.zonm trans.plot.ind
    echo " XPLOT.            0 NEXT    1     -400.      400.    1     -1.E5               " >ic.plts
    echo "$run $days IMPLIED HEAT TRANSP.(INDIAN)[1E13W]                                  " >>ic.plts
    xplot trans.plot.ind input=ic.plts
    rm ind.ocean ind.ocean.zonm begao zbego zlbego iheatrans
#
#
#  ---------------------------------- 5- computation for the other basins
#                                                           ============
    mlt bega other.basins begao
    zonavg begao zbego
    echo "ZLATINT     GAU   NP" | ccc zlatint zbego zlbego
    echo "XLIN    2.5505E14" | ccc xlin zlbego iheatrans
# -----------------------------------   plot the implied transport
    echo "XLIN      1.0E-13" | ccc xlin iheatrans iheatrans.other
    add iheatrans.other other.basins.zonm trans.plot.other
    echo " XPLOT.            0 NEXT    1     -400.      400.    1     -1.E5               " >ic.plts
    echo "$run $days IMPLIED HEAT TRANSP.(OTHER)[1E13W]                                   " >>ic.plts
    xplot trans.plot.other input=ic.plts
    rm other.basins other.basins.zonm begao zbego zlbego
    rm iheatrans iheatrans.other
#
#
# -----------------------------------  put the 4 first curves on one plot
#                                      (without the -1e6 mask)
#
    joinup curves iheatrans.glob iheatrans.atl iheatrans.pac iheatrans.ind
    echo "XMPLOT.            0 NEXT    1     -400.      400.         4                   8" >ic.plts
    echo "$run IMPLIED HEAT TRANSP.(GLB,ATL-R,PAC-B,IND-G)[1E13W]                        " >>ic.plts
    echo "              4   99  100  120  112                                            " >>ic.plts
    xmplot curves input=ic.plts
    rm iheatrans.glob iheatrans.atl iheatrans.pac iheatrans.ind
    rm curves
#
tailfram=$(cat $(which tailfram.cdk) |sed '1,2d')
echo "$tailfram" > Input_Cards

.   plotid.cdk
.   plot.cdk

rm Input_Cards
