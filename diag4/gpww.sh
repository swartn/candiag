#!/bin/sh
#
# This script is only included in the merged_diag_script grouping when
# gas-phase chemistry is run in MAM mode. BW, June 2023
#
# 
#                   gpWW                 MAY 96/SRB.
#   ----------------------------------  compute and save w on pressure grids.
#                                        2) dz/dt in m/s.
#     useable only for spectral sigma/eta input fields.. 
#
#   D. Plummer - April 2015
#      - integrated SK's modifications for CMAM20 version
#   D. Plummer - Nov. 4, 2022
#      - converted to shell script
#
#   PREREQUISITES
#      - requires _gep from pressure_density deck
#      - requires _geshum and _get from pqtphirho deck
#
# Parmsub variables used:
#      lon        longitudes in grid
#      lat        latitudes  in grid
#      p01-p50    pressure levels to interpolate to.
#      t1 t2 t3   start, finish and step of data timesteps.
#      npg        packing density 
#      coord      coordinate code ETA or ET15 etc...
#      plid       lid pressure.
#      lay        vertical interpolation method code number
#      days        data period label (e.g. JAN)
#      run         Run identifier
#      flabel      diagnostic file name


. spfiles.cdk
access gep    ${flabel}_gep
access geshum ${flabel}_geshum
access getemp ${flabel}_get

# Get time-averaged beta for zonal averaging
access beta ${flabel}_gptbeta na
if [ ! -s beta ] ; then

# if not available, get it from gp file
  access oldgp ${flabel}gp
  echo "XFIND.        $d" | ccc xfind oldgp beta
fi

echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME PHIS LNSP" |
       ccc select npaksp ssphis sslnsp
echo "COFAGG.   $lon$lat    0    1"| ccc cofagg sslnsp gslnsp

# A) Calculate omega if not saved from gpintstat
if [ "$gssave"= "on" -a "$wxstats" = "on" ] ; then
  access gsomeg ${flabel}_gsw
else
  echo "SELECT.   STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME VORT  DIV" |
         ccc select npaksp ssvort ssdiv 
  if [ "$gcmtsav" = on ] ; then
    echo "SELECT.   STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME TEMP" |
           ccc select npaksp sstemp
  else
    echo "SELECT.   STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME  PHI" |
           ccc select npaksp ssphi
    ctemps ssphis ssphi sstemp
    rm ssphi
  fi

# B) Compute omega from T and vorticity and divergence.
  if [ "$gcm2plus" = on ] ; then
    echo "GSOMEGA/H $lon$lat$npg$coord$plid$lay" |
         ccc gsomgah sslnsp sstemp ssvort ssdiv gsomeg 
  fi
  if [ "$gcm1" = on ] ; then
    echo "GSOMEGA/H $lon$lat$npg$coord$plid$lay" |
         ccc gsomega sslnsp ssvort ssdiv gsomeg 
  fi
  rm ssvort ssdiv sstemp
fi

# C) Compute RT/pg
echo "XLINRGASM      176.       287." | ccc xlin geshum ge_rgasm
mlt ge_rgasm getemp a
echo "XLIN GRAV      0.00       9.81" | ccc xlin a gravity
div a gravity b
div b gep c
mlt gsomeg c gew1
echo "XLIN -1.0     -1.00       0.00" | ccc xlin gew1 gew2
echo "NEWNAM....   WE" | ccc newnam gew2 gew
echo "GSAPL.    $plv       0.0       0.0$coord$plid
$p01$p02$p03$p04$p05$p06$p07$p08$p09$p10$p11$p12$p13$p14$p15$p16
$p17$p18$p19$p20$p21$p22$p23$p24$p25$p26$p27$p28$p29$p30$p31$p32
$p33$p34$p35$p36$p37$p38$p39$p40$p41$p42$p43$p44$p45$p46$p47$p48
$p49$p50$p51$p52$p53$p54$p55$p56$p57$p58$p59$p60$p61$p62$p63$p64
$p65$p66$p67$p68$p69$p70$p71$p72$p73$p74$p75$p76$p77$p78$p79$p80
$p81$p82$p83$p84$p85$p86$p87$p88$p89$p90$p91$p92$p93$p94$p95$p96
$p97$p98$p99$p100" | ccc gsapl gew gslnsp gpw

rm gep geshum ge_rgasm getemp a gravity b c
rm gew1 gew2 gslnsp

timavg gpw gpw_tav
rzonavg gpw_tav beta gpw_ztav

# Save fields

release oldgp
access oldgp ${flabel}gp
echo "XSAVE.        WE EULERIAN VERTICAL VELOCITY - M OVER SEC
NEWNAM." | ccc xsave oldgp gpw_tav newgp
save newgp ${flabel}gp
delete oldgp
rm newgp

release oldxp
access oldxp ${flabel}xp
echo "XSAVE.        WE EULERIAN VERTICAL VELOCITY - M OVER SEC
NEWNAM." | ccc xsave oldxp gpw_ztav newxp
save newxp ${flabel}xp      
delete oldxp
rm newxp

save gpw ${flabel}_gpwe
save gew ${flabel}_gewe
save gsomeg ${flabel}_gew
