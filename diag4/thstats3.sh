#!/bin/sh
#
# This script is only included in the merged_diag_script grouping when
# gas-phase chemistry is run in MAM mode. BW, June 2023
#
# Amendments to the original: source the file of switches
#
#
#  THSTATS  
#        - Modified for potential temperature SRB Aug96.
#        - Calculates theta statistics and covariances with u v and w.
#
# --- 2023-11-23: Removed save of instantaneous _gptheta because it 
#                  doesn't seem to be used by any other decks
# --- 2022-09-28: converted to shell script and removed ztsave so
#                  zonal averages always done (D. Plummer)
# --- 2005-12-14: modified to remove geqtz.dk from string with
#                  necessary calculations included here
# ------------------------------------------------------------------------------

# Source the list of on/off switches
source ${CANESM_SRC_ROOT}/CanDIAG/diag4/gaschem_diag_switches.sh

#   ---------------------------------- spectral sigma case.
    if [ "$datatype" = "specsig" ] ; then
.     spfiles.cdk

      echo "SELECT.   STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME PHIS LNSP" |
         ccc select npaksp ssphis sslnsp
      if [ "$gcmtsav" = on ] ; then
        echo "SELECT.   STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME TEMP" |
           ccc select npaksp sstemp
      else
        echo "SELECT    STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME  PHI" |
           ccc  select npaksp ssphi
        ccc ctemps ssphis ssphi sstemp
      fi
      echo "COFAGG.  $lon$lat    0    1" > .cofagg_input_card
      cofagg sstemp gstemp input=.cofagg_input_card
      cofagg sslnsp gslnsp input=.cofagg_input_card
      cofagg ssphis gsphis input=.cofagg_input_card
      rm sstemp sslnsp ssphis
    fi

# ------------------------- COMMON CALCULATIONS -------------------------

# convert temp to theta
# --- requires pressure on model layers - ${flabel}_gep calculated 
#     in density.dk
#
    access oldgpe ${flabel}_gep
    echo "SELECT.   STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME P(E)" |
       ccc select oldgpe press input=.select_input_card
#
# ---------------- create p0 file
    echo "XLIN              0.   101325." | ccc xlin press p0
#
# ---------------- divide p0 by p; raise to power R/Cp
    div  p0 press pratio
    echo "FPOW R/CP      0.286" | ccc fpow pratio a
#
# ---------------- evaluate pot.temp from T.
    mlt gstemp a getheta
#
    echo "  GSAPL.  $plv       0.0       0.0$coord$plid
$p01$p02$p03$p04$p05$p06$p07$p08$p09$p10$p11$p12$p13$p14$p15$p16
$p17$p18$p19$p20$p21$p22$p23$p24$p25$p26$p27$p28$p29$p30$p31$p32
$p33$p34$p35$p36$p37$p38$p39$p40$p41$p42$p43$p44$p45$p46$p47$p48
$p49$p50$p51$p52$p53$p54$p55$p56$p57$p58$p59$p60$p61$p62$p63$p64
$p65$p66$p67$p68$p69$p70$p71$p72$p73$p74$p75$p76$p77$p78$p79$p80
$p81$p82$p83$p84$p85$p86$p87$p88$p89$p90$p91$p92$p93$p94$p95$p96
$p97$p98$p99$p100" | ccc gsapl getheta gslnsp gptheta

    timavg gptheta theta_tav
    zonavg theta_tav theta_ztav

    release oldxp
    access oldxp ${flabel}xp
    echo "XSAVE.        POT. TEMP.
NEWNAM.    POTT" | ccc xsave oldxp  theta_ztav  new1
    save new1 ${flabel}xp
    delete oldxp
    rm new1
#
    release oldgp
    access oldgp ${flabel}gp
    echo "XSAVE.        POT. TEMP.
NEWNAM.    POTT" | ccc xsave oldgp  theta_tav   new1
    save new1 ${flabel}gp
    delete oldgp
    rm new1

# --------------------- # rename field as TEMP
    echo "NEWNAM.    TEMP" | ccc newnam gptheta x

    rm getheta gstemp a pratio p0 press
    rm gslnsp gsphis gptheta oldgpe

#   *******************************************************************
#
    if [ "$vxstats" = on ] ; then
#
#  --- vx statistics ---
      if [ "$gridpr" != on ] ; then
        access gpv ${flabel}_gpv
      fi

      access beta1 ${flabel}_gpbeta
#
# <a> = zonal average, a* = zonal deviation from average.
# [a] = time average,  a" = time deviation from average.
#
      rzonavg x beta1 rzx
# <x> = zonal average of x.
      zondev x rzx xdsh
#  x* = xdsh = zonal deviation of x field.
      rzonavg gpv beta1  rzv
# <v> = zonal average of v.
      zondev gpv rzv   vdsh 
#  v* = vdsh = zonal deviation of v field.
      mlt vdsh xdsh a 
#  v*x*   = a =
      rzonavg a beta1  rzxdshvdsh 
# <v*x*>  = rzxdshvdsh = eddy flux (total)
      rm vdsh a rzv beta1
      timavg rzxdshvdsh rtzxdshvdsh
# [<v*x*>] = rtzxdshvdsh = time averaged total meridional eddy flux.
#   ----------------------------------- output <v*x*> r
#
#   ----------------------------------- saves time average [<v*x*>]
      release oldxp
      access oldxp ${flabel}xp
      echo "XSAVE...      [<V*TH*>]R
NEWNAM.    V.TH" | ccc  xsave oldxp rtzxdshvdsh newxp
      save newxp ${flabel}xp
      delete oldxp
      rm newxp
#   ..........................end of ............................ fluxes.
#
    fi
