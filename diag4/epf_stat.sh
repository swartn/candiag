#!/bin/sh
#
# This script is only included in the merged_diag_script grouping when
# gas-phase chemistry is run in MAM mode. BW, June 2023
#
# Convert to shell script - D. Plummer, Nov 3,2022
#
# ---------------------------------------------------------------------------
# Stationary wave EP flux diagnostics: outputs EP flux divergence,
# meridional and vertical components of the EP flux, and meridional heat 
# flux. Log pressure coordinates are used. 
# Options to: (1) decompose all fluxes into different zonal wavenumber bands
# (used only if nbands > 0); (2) plot monthly mean results.
# ----------------------------------------------- C.McLandress (Jan 9/2007)
#

    access u ${flabel}_gpu
    access v ${flabel}_gpv
    access w ${flabel}_gpw
    access t ${flabel}_gpt
     
# Time averages
    timavg u ut
    timavg v vt
    timavg w wt
    timavg t tt

# Calculate zonal means and deviations about the zonal mean
    zonavg ut uz udev
    zonavg vt vz vdev
    zonavg wt wz wdev
    zonavg tt tz tdev

# Deviations about the zonal mean 
#    zondev ut uz udev
#    zondev vt vz vdev
#    zondev wt wz wdev
#    zondev tt tz tdev
    rm ut vt wt tt

# ****************************************************************
# EP flux diags for all zonal wavenumbers 
# ****************************************************************

    cp udev up
    cp vdev vp
    cp wdev wp
    cp tdev tp
    mlt    up vp upvp
    mlt    up wp upwp
    mlt    vp tp vptp
    zonavg upvp upvpz
    zonavg upwp upwpz
    zonavg vptp vptpz
    rm upvp upwp vptp

    ccc epflux4gaschem upvpz upwpz vptpz uz tz vz wz epfy epfz epfd vres wres
    echo "RELABL.    ZONL
                          VPTP" | ccc relabl vptpz htflux
    echo "XSAVE.        MERIDIONAL COMPONENT EP FLUX FOR STAT WAVES
NEWNAM.
              VERTICAL COMPONENT EP FLUX FOR STAT WAVES
NEWNAM.
              EP FLUX DIVERGENCE FOR STAT WAVES
NEWNAM.
              MERIDIONAL HEAT FLUX FOR STAT WAVES 
NEWNAM." | ccc xsave epfile epfy epfz epfd htflux new
    mv new epfile
    
# ****************************************************************
# EP flux diags for zonal wavenumber bands 
# ****************************************************************

    if [ "$nbands" -ge 1 ] ; then

      cp udev up
      cp vdev vp
      cp wdev wp
      cp tdev tp

      echo "LLAFRB.    $maxf $mfirst_band1 $mlast_band1" | ccc llafrb up um
      echo "FRALL.    $lon" | ccc frall  um up     
      echo "LLAFRB.    $maxf $mfirst_band1 $mlast_band1" | ccc llafrb vp vm
      echo "FRALL.    $lon" | ccc frall  vm vp
      echo "LLAFRB.    $maxf $mfirst_band1 $mlast_band1" | ccc llafrb wp wm
      echo "FRALL.    $lon" | ccc frall  wm wp
      echo "LLAFRB.    $maxf $mfirst_band1 $mlast_band1" | ccc llafrb tp tm
      echo "FRALL.    $lon" | ccc frall  tm tp
      rm um vm wm tm

      mlt up vp upvp
      mlt up wp upwp
      mlt vp tp vptp
      zonavg upvp upvpz
      zonavg upwp upwpz
      zonavg vptp vptpz
      rm upvp upwp vptp

ccc epflux4gaschem upvpz upwpz vptpz uz tz vz wz epfy1 epfz1 epfd1 vdum wdum
      echo "RELABL.    ZONL
                          VPTP" | ccc relabl vptpz htflux1
      echo "XSAVE.        MERIDIONAL COMPONENT EP FLUX FOR STAT M=$mfirst_band1 TO$mlast_band1
NEWNAM.
              VERTICAL COMPONENT EP FLUX FOR STAT M=$mfirst_band1 TO$mlast_band1
NEWNAM.
              EP FLUX DIVERGENCE FOR STAT M=$mfirst_band1 TO$mlast_band1
NEWNAM.
              MERIDIONAL HEAT FLUX FOR STAT M=$mfirst_band1 TO$mlast_band1
NEWNAM." | ccc xsave epfile epfy1 epfz1 epfd1 htflux1 new
      mv new epfile

      if [ "$nbands" -ge 2 ] ; then

        cp udev up
        cp vdev vp
        cp wdev wp
        cp tdev tp
        echo "LLAFRB.    $maxf $mfirst_band2 $mlast_band2" | ccc llafrb up um
        echo "FRALL.    $lon" | ccc frall  um up     
        echo "LLAFRB.    $maxf $mfirst_band2 $mlast_band2" | ccc llafrb vp vm
        echo "FRALL.    $lon" | ccc frall  vm vp
        echo "LLAFRB.    $maxf $mfirst_band2 $mlast_band2" | ccc llafrb wp wm
        echo "FRALL.    $lon" | ccc frall  wm wp
        echo "LLAFRB.    $maxf $mfirst_band2 $mlast_band2" | ccc llafrb tp tm
        echo "FRALL.    $lon" | ccc frall  tm tp
        rm um vm wm tm

        mlt up vp upvp
        mlt up wp upwp
        mlt vp tp vptp
        zonavg upvp upvpz
        zonavg upwp upwpz
        zonavg vptp vptpz
        rm upvp upwp vptp

ccc epflux4gaschem upvpz upwpz vptpz uz tz vz wz epfy2 epfz2 epfd2 vdum wdum
        echo "RELABL.    ZONL
                          VPTP" | ccc relabl vptpz htflux2
        echo "XSAVE.        MERIDIONAL COMPONENT EP FLUX FOR STAT M=$mfirst_band2 TO$mlast_band2
NEWNAM.
              VERTICAL COMPONENT EP FLUX FOR STAT M=$mfirst_band2 TO$mlast_band2
NEWNAM.
              EP FLUX DIVERGENCE FOR STAT M=$mfirst_band2 TO$mlast_band2
NEWNAM.
              MERIDIONAL HEAT FLUX FOR STAT M=$mfirst_band2 TO$mlast_band2
NEWNAM." | ccc xsave epfile epfy2 epfz2 epfd2 htflux2 new
        mv new epfile
	 
        if [ "$nbands" -ge 3 ] ; then

          cp udev up
          cp vdev vp
          cp wdev wp
          cp tdev tp
          echo "LLAFRB.    $maxf $mfirst_band3 $mlast_band3" | ccc llafrb up um
          echo "FRALL.    $lon" | ccc frall um up     
          echo "LLAFRB.    $maxf $mfirst_band3 $mlast_band3" | ccc llafrb vp vm
          echo "FRALL.    $lon" | ccc frall vm vp
          echo "LLAFRB.    $maxf $mfirst_band3 $mlast_band3" | ccc llafrb wp wm
          echo "FRALL.    $lon" | ccc frall wm wp
          echo "LLAFRB.    $maxf $mfirst_band3 $mlast_band3" | ccc llafrb tp tm
          echo "FRALL.    $lon" | ccc frall tm tp
          rm um vm wm tm

          mlt up vp upvp
          mlt up wp upwp
          mlt vp tp vptp
          zonavg upvp upvpz
          zonavg upwp upwpz
          zonavg vptp vptpz
          rm upvp upwp vptp

ccc epflux4gaschem upvpz upwpz vptpz uz tz vz wz epfy3 epfz3 epfd3 vdum wdum
          echo "RELABL.    ZONL
                          VPTP" | ccc relabl vptpz htflux3
          echo "XSAVE.        MERIDIONAL COMPONENT EP FLUX FOR STAT M=$mfirst_band3 TO$mlast_band3
NEWNAM.
              VERTICAL COMPONENT EP FLUX FOR STAT M=$mfirst_band3 TO$mlast_band3
NEWNAM.
              EP FLUX DIVERGENCE FOR STAT M=$mfirst_band3 TO$mlast_band3
NEWNAM.
              MERIDIONAL HEAT FLUX FOR STAT M=$mfirst_band3 TO$mlast_band3
NEWNAM." | ccc xsave epfile epfy3 epfz3 epfd3 htflux3 new
          mv new epfile
	   
          if [ "$nbands" -ge 4 ] ; then

            cp udev up
            cp vdev vp
            cp wdev wp
            cp tdev tp
            echo "LLAFRB.    $maxf $mfirst_band4 $mlast_band4" | ccc llafrb up um
            echo "FRALL.    $lon" | ccc frall um up     
            echo "LLAFRB.    $maxf $mfirst_band4 $mlast_band4" | ccc llafrb vp vm
            echo "FRALL.    $lon" | ccc frall vm vp
            echo "LLAFRB.    $maxf $mfirst_band4 $mlast_band4" | ccc llafrb wp wm
            echo "FRALL.    $lon" | ccc frall wm wp
            echo "LLAFRB.    $maxf $mfirst_band4 $mlast_band4" | ccc llafrb tp tm
            echo "FRALL.    $lon" | ccc frall tm tp
            rm um vm wm tm

            mlt up vp upvp
            mlt up wp upwp
            mlt vp tp vptp
            zonavg upvp upvpz
            zonavg upwp upwpz
            zonavg vptp vptpz
            rm upvp upwp vptp

ccc  epflux4gaschem upvpz upwpz vptpz uz tz vz wz epfy4 epfz4 epfd4 vdum wdum
            echo "RELABL.    ZONL
                          VPTP" | ccc relabl vptpz htflux4
            echo "XSAVE.        MERIDIONAL COMPONENT EP FLUX FOR STAT M=$mfirst_band4 TO$mlast_band4
NEWNAM.
              VERTICAL COMPONENT EP FLUX FOR STAT M=$mfirst_band4 TO$mlast_band4
NEWNAM.
              EP FLUX DIVERGENCE FOR STAT M=$mfirst_band4 TO$mlast_band4
NEWNAM.
              MERIDIONAL HEAT FLUX FOR STAT M=$mfirst_band4 TO$mlast_band4
NEWNAM." | ccc xsave epfile epfy4 epfz4 epfd4 htflux4 new
            mv new epfile
	     
            if [ "$nbands" -ge 5 ] ; then

              cp udev up
              cp vdev vp
              cp wdev wp
              cp tdev tp
              echo "LLAFRB.    $maxf $mfirst_band5 $mlast_band5" | ccc llafrb up um
              echo "FRALL.    $lon" | ccc frall um up     
              echo "LLAFRB.    $maxf $mfirst_band5 $mlast_band5" | ccc llafrb vp vm
              echo "FRALL.    $lon" | ccc frall vm vp
              echo "LLAFRB.    $maxf $mfirst_band5 $mlast_band5" | ccc llafrb wp wm
              echo "FRALL.    $lon" | ccc frall wm wp
              echo "LLAFRB.    $maxf $mfirst_band5 $mlast_band5" | ccc llafrb tp tm
              echo "FRALL.    $lon" | ccc frall tm tp
              rm um vm wm tm

              mlt up vp upvp
              mlt up wp upwp
              mlt vp tp vptp
              zonavg upvp upvpz
              zonavg upwp upwpz
              zonavg vptp vptpz
              rm upvp upwp vptp

ccc epflux4gaschem upvpz upwpz vptpz uz tz vz wz epfy5 epfz5 epfd5 vdum wdum
              echo "RELABL.    ZONL
                          VPTP" | ccc relabl vptpz htflux5
              echo "XSAVE.        MERIDIONAL COMPONENT EP FLUX FOR STAT M=$mfirst_band5 TO$mlast_band5
NEWNAM.
              VERTICAL COMPONENT EP FLUX FOR STAT M=$mfirst_band5 TO$mlast_band5
NEWNAM.
              EP FLUX DIVERGENCE FOR STAT M=$mfirst_band5 TO$mlast_band5
NEWNAM.
              MERIDIONAL HEAT FLUX FOR STAT M=$mfirst_band5 TO$mlast_band5
NEWNAM." | ccc xsave epfile epfy5 epfz5 epfd5 htflux5 new
              mv new epfile
	       
            fi 
          fi 
        fi 
      fi 
    fi


# Save in xp file
    release oldxp
    access oldxp ${flabel}xp
    xjoin oldxp epfile newxp 
    save newxp ${flabel}xp
    delete oldxp

# Check that sum of EP flux & div for all wavenumber bands equals total
# (works only for 4 or 5 bands and only if the sum of the wavenumber bands 
# equals the total)

    if [ "$epchecksum" = on ] ; then

      add epfy1 epfy2 sum1
      add epfy3 epfy4 sum2
      add sum1  sum2  epfy_sum
      if [ "$nbands" = 5 ] ; then
        mv epfy_sum sum3
        add epfy5 sum3  epfy_sum 
      fi
      sub epfy epfy_sum epfy_diff 
      ggstat epfy_diff  
      ggstat epfy
      ggstat epfy_sum 

      add epfz1 epfz2 sum1
      add epfz3 epfz4 sum2
      add sum1  sum2  epfz_sum
      if [ "$nbands" = 5 ] ; then
        mv epfz_sum sum3
        add epfz5 sum3  epfz_sum 
      fi
      sub epfz epfz_sum epfz_diff 
      ggstat epfz_diff  
      ggstat epfz
      ggstat epfz_sum

      add epfd1 epfd2 sum1
      add epfd3 epfd4 sum2
      add sum1  sum2  epfd_sum
      if [ "$nbands" = 5 ] ; then
        mv epfd_sum sum3
        add epfd5 sum3  epfd_sum 
      fi
      sub epfd epfd_sum epfd_diff 
      ggstat epfd_diff  
      ggstat epfd
      ggstat epfd_sum

      add htflux1 htflux2 sum1
      add htflux3 htflux4 sum2
      add sum1  sum2  htflux_sum
      if [ "$nbands" = 5 ] ; then
        mv htflux_sum sum3
        add htflux5 sum3  htflux_sum 
      fi
      sub htflux htflux_sum htflux_diff 
      ggstat htflux_diff  
      ggstat htflux
      ggstat htflux_sum

    fi
