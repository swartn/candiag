#!/bin/sh
#   sk: replace representative zonal means (X)R with plain zonal means (X)
#
#                  zoncloud            ml,sk - jun 24, 2004
#   ---------------------------------- module to produce zonally averaged statistics
#                                      of cloud fields from "gp" file.
#                                      note that these are done on ** MODEL **
#                                      levels.
#                                      first, however, the "beta" and "del"
#                                      fields are done, since they are
#                                      needed by the model diagnostics.

      access gpfile ${flabel}gp

#   ---------------------------------- zonal means [x] only.
      vars="d b rhc scld clw cic tacn"
echo "XFIND.        D
XFIND.        B
XFIND.        RHC
XFIND.        SCLD
XFIND.        CLW
XFIND.        CIC
XFIND.        TACN" | ccc xfind gpfile $vars
      for x in $vars ; do
	zonavg  $x z$x ; mv z$x $x
      done
      xsave old_xp $vars new_xp

#   ---------------------------------- finally save the results.
      relase oldxp
      access oldxp ${flabel}xp na
      xjoin  oldxp new_xp newxp
      save   newxp ${flabel}xp
      delete oldxp na
