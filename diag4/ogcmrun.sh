#!/bin/bash

# Get useful CanESM shell functions
source ${CANESM_SRC_ROOT}/CCCma_tools/tools/CanESM_shell_functions.sh

# ---------------------------------- Perform code checks
if (( CODE_CHECKS == 1 )); then
    event_year=$(echo $year | awk '{printf "%04d",$1}')
    event_mon=$(echo $mon | awk '{printf "%02d",$1}')
    (( PRODUCTION == 1 )) && flgs="" || flgs="-d"   # use 'development' mode if production is off
    strict_check $flgs $runid model-run-${event_year}m${event_mon}
fi

#  ----------------------------------- Setup to run in parallel mode.

source gcm_parallel_setup.sh

#  ----------------------------------- Determine initial chunk dates and if we're using internal chunking
#                                       Noting that _if_ job_stop_date is before the calculated chunk_stop_date
#                                       then it is assumed the model is to shut down earlier
months_run=${months_run:-1}
months_sofar=$months_run
chunk_start_year=$(strip_leading_zeros $year)
chunk_start_month=$(strip_leading_zeros $mon)
chunk_start_date=$( printf "%04d-%02d-%02d" $chunk_start_year $chunk_start_month 1 )
chunk_stop_date=$( calc_chunk_stop_date $chunk_start_date $months_run )

if date_precedes $job_stop_date $chunk_stop_date; then
    # TODO: this is pretty hacky.. we should refactor this script to allow for this is
    #           a clearer manner
    #
    # override the stop date to shut the model down early (for short integration tests)
    chunk_stop_date=$job_stop_date
    number_of_internal_chunks=1
else
    # following the chunking behaviour defined by months/months_run
    number_of_internal_chunks=$(( ( months - 1 )/months_run + 1 ))
fi

# get chunk stop year/month in integer values (no leading zeros)
chunk_stop_date_array=( ${chunk_stop_date//-/ } )
chunk_stop_year=$( strip_leading_zeros ${chunk_stop_date_array[0]} )
chunk_stop_month=$( strip_leading_zeros ${chunk_stop_date_array[1]} )
chunk_stop_day=$( strip_leading_zeros ${chunk_stop_date_array[2]} )

# derive the date string for the input restart (if being used)
if (( chunk_start_month == 1 )); then
  input_rs_mon=12
  input_rs_year=$(( chunk_start_year - 1 ))
else
  input_rs_mon=$(( chunk_start_month - 1 ))
  input_rs_year=$chunk_start_year
fi
input_rs_date_string=$( printf "%04d_m%02d" $input_rs_year $input_rs_mon )

#  ----------------------------------- Loop over internal chunks, if needed
for chunk_counter in $(seq $number_of_internal_chunks -1 1) ; do # count DOWN
  if (( chunk_counter == 1 )); then
    final_internal_chunk=1
  fi

  # remove execution specific files (for internal chunking)
  rm -f ocean.output* ${RANK_OUTPUT_PRFX}.* time.step*

  echo chunk_start_year=$chunk_start_year chunk_start_month=$chunk_start_month
  echo chunk_stop_year=$chunk_stop_year chunk_stop_month=$chunk_stop_month

  # model/restart names
  model=$( printf "%s_%04d_m%02d_" $prefix $chunk_start_year $chunk_start_month )
  model_rs=$( printf "%s_%04d_m%02d_" $prefix $chunk_stop_year $chunk_stop_month )
  echo model=$model
  echo model_rs=$model_rs

  #  ----------------------------------- NEMO setup
  if [[ $use_nemo == on ]]; then
    . ${CANESM_SRC_ROOT}/CanNEMO/lib/nemo_prelude
    update_nemo_counters start_date=$chunk_start_date \
                         stop_date=$chunk_stop_date \
                         nemo_timestep=$nemo_rn_rdt \
                         namelist_file=namelist
  fi
  #  ------------------------------- Initialize rank specific stdout/stderr files with appropriate read
  #                                   permissions to allow others to inspect stdout/stderr files
  total_mpi_tasks=$nnode_o
  for i in $(seq 1 $total_mpi_tasks); do
      rank=$((i - 1))
      touch ${RANK_OUTPUT_PRFX}.${rank}.${RANK_OUTPUT_SFX}
      chmod +r ${RANK_OUTPUT_PRFX}.${rank}.${RANK_OUTPUT_SFX}
  done

  #  ------------------------------- Run the model.
  cat prunsc
  launch_message="\n\nModel launching!\n"
  launch_message+=" \tTo see model stdout/stderr as it progresses, go to work dir at\n"
  launch_message+=" \t\t$(pwd)\n"
  launch_message+=" \tand see the ${RANK_OUTPUT_PRFX}.\$RANK.${RANK_OUTPUT_SFX} files\n"
  launch_message+=" \tfor ranks $ocn_rank_range\n"
  printf "$launch_message\n\n"
  ./prunsc && model_status=$? || model_status=$?
  printf "Model run completed!\n\t model_status=$model_status\n"

  #  ------------------------------- Concatenate the stdout/stderr files
  if is_defined ${ocn_rank_range}; then
      # concatenate each rank specific file together
      component_output_file=${model_rs}ocn_stdout_stderr
      touch $component_output_file
      for rank in $(seq ${ocn_rank_range//:/ }); do
          echo "========== ${component} mpi rank ${rank} =============" >> $component_output_file
          rank_id=$(printf "mpi rank %4s:" $rank)
          sed -e "s/^/$rank_id/" ${RANK_OUTPUT_PRFX}.${rank}.${RANK_OUTPUT_SFX} >> $component_output_file
      done

      # save for diagnostic/debugging purposes
      save ${component_output_file} ${component_output_file}
  fi

  #  ------------------------------- If it exists, save the ocean output file
  produced_ocean_output_file=ocean.output
  saved_ocean_output_file=${model_rs}ocean_output
  if exists $produced_ocean_output_file; then
    save $produced_ocean_output_file $saved_ocean_output_file
  fi

  #  ------------------------------- Exit if model is aborted.
  if (( model_status != 0 )) ; then
    bail_message="The model existed with a non-zero exit status!"
    bail_message+=" See the saved $component_output_file file or rank specific files at $(pwd)!"
    bail "$bail_message"
  fi

  #  ----------------------------------- CanNEMO postlude
  if [[ $use_nemo == on ]]; then
    . ${CANESM_SRC_ROOT}/CanNEMO/lib/nemo_postlude
  fi

  # ------------------------------------ Determine start/stop times next internal chunk (if needed)
  if (( final_internal_chunk != 1 )); then
      # Determine start time
      if (( chunk_stop_month < 12 )); then
          chunk_start_year=$chunk_stop_year
          chunk_start_month=$(( chunk_stop_month + 1 ))
      else
          # at a year boundary, roll over
          chunk_start_year=$(( chunk_stop_year + 1 ))
          chunk_start_month=1
      fi
      chunk_start_date=$( printf "%04d-%02d-%02d" $chunk_start_year $chunk_start_month 1 )

      # Determine stop time - make sure we don't exceed job total (defined by months)
      if (( months_sofar + months_run > months )); then
          # running for months_run would exceed the total number of months we want for this job
          #  so we instead only go the remainder
          last_chunk=$(( months_sofar + months_run - months ))
          chunk_stop_date=$( calc_chunk_stop_date $chunk_start_date $last_chunk )
          months_sofar=$(( months_sofar + last_chunk ))
      else
          # we can do another clean chunk
          chunk_stop_date=$( calc_chunk_stop_date $chunk_start_date $months_run )
          months_sofar=$(( months_sofar + months_run ))
      fi
      chunk_stop_date_array=( ${chunk_stop_date//-/ } )
      chunk_stop_year=$( strip_leading_zeros ${chunk_stop_date_array[0]} )
      chunk_stop_month=$( strip_leading_zeros ${chunk_stop_date_array[1]} )
      chunk_stop_day=$( strip_leading_zeros ${chunk_stop_date_array[2]} )

      # update restart date string
      if (( chunk_start_month == 1 )); then
        input_rs_mon=12
        input_rs_year=$(( chunk_start_year - 1 ))
      else
        input_rs_mon=$(( chunk_start_month - 1 ))
        input_rs_year=$chunk_start_year
      fi
      input_rs_date_string=$( printf "%04d_m%02d" $input_rs_year $input_rs_mon )
  fi

  #  --------------------------------- End of loop.
done

# save environment
env > ${model_rs}env
save ${model_rs}env ${model_rs}env
