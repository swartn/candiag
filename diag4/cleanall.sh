#!/bin/sh
#  Add "check_diag.cdk" call to allow checking on saved diagnostic datasets.
#
#                  cleanall            sk - Mar 19/14 - fm --------------cleanall
#   --- Purge all temporary files from a diagnostic string.
if [ "$SITE_ID" = 'Dorval' -o "$SITE_ID" = 'DrvlSC' ] ; then dpalist=${dpalist:='on'} ; export dpalist ; fi
.     check_diag.cdk
#                                      remove split variables
.     delsplt.cdk

    if [ "$nocleanup" != "on" ] ; then
#                                      remove gridded pressure level files
.     deletgps.cdk
#                                      remove spectral pressure level files
.     deletsp.cdk

#                                      remove gridded eta level files
      if [ "$gssave" = "on" ] ; then
        for v in gsq gsz gst gsu gsv gsw ; do
          access $v ${flabel}_$v nocp na
          if [ -f $v ] ; then
            delete $v na
          fi
        done
      fi

#     remove tracers on pressure levels
      if [ "$gpxsave" = "on" ] ; then
        eval ntrac=\${ntrac:=0}
        i=1
        while [ $i -le $ntrac ] ; do
          i2=`echo "0$i" | tail -3c` # 2-digit tracer number
          # evaluate tracers parameters
          eval x=\${it$i2}
          access $x ${flabel}_gp$x nocp na
          if [ -f $x ] ; then
            delete $x na
          fi
          access $x ${flabel}_gs$x nocp na
          if [ -f $x ] ; then
            delete $x na
          fi
          i=`expr $i + 1`
        done
      fi
    fi # "$nocleanup" = "on"
