#!/bin/sh
# Based on "xtraconv" but with extraction of all levels.
#                 xtraconv2            jcl - Feb 25/14 - fm
#   ---------------------------------- module to compute monthly mean fields
#                                      related to output created by xtraconv

.   spfiles.cdk
.   ggfiles.cdk

#  ---------------------------------- Input fields
# BCD  -> Unadjusted cloud base pressure deep convection
# BCS  -> Unadjusted cloud base pressure shallow convection
# CAPE -> Convective available potential energy
# CDCB -> Deep convection indicator
# CINH -> Convective inhibition
# CSCB -> Shallow convection indicator
# DMC  -> Deep convection net mass flux
# DMCD -> Deep convection downward mass flux
# DMCU -> Deep convection upward mass flux
# SMC  -> Shallow convection net mass flux
# TCD  -> Unadjusted cloud top pressure deep convection
# TCS  -> Unadjusted cloud top pressure shallow convection
#
#  ---------------------------------- Output fields
#
# TBCD  -> Unadjusted cloud base pressure deep convection
# TBCS  -> Unadjusted cloud base pressure shallow convection
# TCAPE -> Convective available potential energy
# TCDCB -> Deep convection indicator
# TCINH -> Convective inhibition
# TCSCB -> Shallow convection indicator
# TDMC  -> Deep convection net mass flux
# TDMCD -> Deep convection downward mass flux
# TDMCU -> Deep convection upward mass flux
# TSMC  -> Shallow convection net mass flux
# TTCD  -> Unadjusted cloud top pressure deep convection
# TTCS  -> Unadjusted cloud top pressure shallow convection

#  ---------------------------------- Notes
# The cloud bottom and cloud top pressures (TBCD, TBCS, TTCD, TTCS) are special since
# they are effectively weighted by cloud occurrence (1 if convection, 0 if no
# convection).  Therefore their time averages need to be divided by the time
# averages of the weights (TCDCB and TCSCB) to compute the proper weighted
# time means.  One needs to do the following with the variables saved to the
# diagnostic file:
#
# TBCD = TBCD/TCDCB
# TBCS = TBCS/TCSCB
# TTCD = TTCD/TCDCB
# TTCS = TTCS/TCSCB
#
# before interpreting and plotting the mean cloud top and cloud base pressures.

echo "SELECT          $r1 $r2 $r3 LEVS-9001 1000       BCD  BCS CAPE CDCB
SELECT     CINH CSCB  DMC DMCD DMCU  SMC  TCD  TCS" | ccc select npakgg BCD BCS CAPE CDCB CINH CSCB DMC DMCD DMCU SMC TCD TCS

# Time average the fields

    timavg BCD  TBCD
    timavg BCS  TBCS
    timavg CAPE TCAPE
    timavg CDCB TCDCB
    timavg CINH TCINH
    timavg CSCB TCSCB
    timavg DMC  TDMC
    timavg DMCD TDMCD
    timavg DMCU TDMCU
    timavg SMC  TSMC
    timavg TCD  TTCD
    timavg TCS  TTCS

    rm BCD BCS CAPE CDCB CINH CSCB DMC DMCD DMCU SMC TCD TCS

# Variable to be saved to a file

    vars="TBCD TBCS TTCD TTCS TCDCB TCSCB TDMC TDMCD TDMCU TSMC TCAPE TCINH"

# Save the fields to a file

    statsav $vars new_gp new_xp $stat2nd

    rm ${vars}

#  ---------------------------------- save results.

    release oldgp
    access  oldgp ${flabel}gp
    xjoin   oldgp new_gp newgp
    save    newgp ${flabel}gp
    delete  oldgp

    if [ "$rcm" != "on" ] ; then
    release oldxp
    access  oldxp ${flabel}xp
    xjoin   oldxp new_xp newxp
    save    newxp ${flabel}xp
    delete  oldxp
    fi
