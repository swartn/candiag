#!/bin/sh
#  Convert to script - 29/DEC/2021 - DA
#
#  Fix a bug.
#                  pool4               sk - Nov 23/09 - sk.
#   ---------------------------------- multi-year pooling of means and
#                                      (optionally) variances.
#  Pool up to 50 diagnostic/model and/or pooled files.
#
#  Input parameters:
#
# pool_suffix_list = (optional) the list of suffixes of diagnostic/model files
#            to be pooled. For example, pool_suffix_list="gp xp cp".
#            Default value (if not defined) is pool_suffix_list="gp xp".
#
# ny01,..., ny50 = the number of years pooled in each input file.
#            The value 0 is used to indicate diagnostic/model files,
#            i.e., files that are not pooled yet.
#            Note: when pooling together pooled and diagnostic/model files
#            the 1st input file must be a pooled file.
#
# pool_var = off, don't pool variances and other 2nd order statistics.
#            on, pool second order statistics (default).
#            this switch affects pooling of gp files only.
#
# poolsubset = (optional) a list of variables (superlabels) to be pooled.
#            Variable names must be separated by the semicolon ';'.
#            Double quotes '"' must be protected by the backslash '\'.
#            If not defined, all variables are pooled.
#
# poolabort = off, do not abort if some variables are missing.
#           = on, abort if any variables are missing (default).
#

#   ---------------------------------- set the default value of pool_suffix_list
      if [ -z "$pool_suffix_list" ] ; then
        pool_suffix_list="gp xp"
      fi

#   ---------------------------------- check the number of input files
      if [ $join -gt 50 ] ; then
        echo " *** ERROR in pool4.dk: number of files must be <= 50."
        exit 1
      fi

#   ---------------------------------- check 'poolabort'
#                                      (default is strict behaviour)
      if [ "$poolabort" != "off" ] ; then
        pool_abort="  1"
      else
        pool_abort="  0"
      fi

      for sfx in $pool_suffix_list ; do

#   ---------------------------------- access diagnostic files
        if [ "$sfx" = "gp" ] ; then
          loopline='release gg$counter\; access gg$counter \${model$counter}gp'
.         looper.cdk
        elif [ "$sfx" = "xp" ] ; then
          loopline='release gg$counter\; access gg$counter \${model$counter}xp'
.         looper.cdk
        elif [ "$sfx" = "cp" ] ; then
          loopline='release gg$counter\; access gg$counter \${model$counter}cp'
.         looper.cdk
        elif [ "$sfx" = "gz" ] ; then
          loopline='release gg$counter\; access gg$counter \${model$counter}gz'
.         looper.cdk
        fi

#   ---------------------------------- set pooling type
        if [ "$sfx" = "gp" -a \( "$pool_var" = "on" -o -z "$pool_var" \) ] ; then
          pool_type=" 0"             # pool time means and variances
        else
          pool_type=" 1"             # pool time means only
        fi

#   ---------------------------------- accumulate input card for 'pool'
        pool_files=""
        n=1
        echo "          ${pool_abort}${pool_type}" > .pool_input_card
        echo "              DATA DESCRIPTION" > .modinfo_input_card
        pool_input_card="          "
        while [ $n -le $join ] ; do
          eval ny=\$ny$n
#                                      check for ny0?, if ny? is undefined
          if [ $n -le 9 -a -z "$ny" ] ; then
            eval ny=\$ny0$n
          fi
          pool_input_card=${pool_input_card}`echo "     $ny" | tail -6c`
          pool_files="$pool_files gg$n"
          remnd=`expr $n % 14` || true
#                                      select DATA DESCRIPTION records from gg1
          if [ $n -eq 1 ] ; then
            rm -f modinfo
            xfind gg$n modinfo input=.modinfo_input_card || true
            if [ -s modinfo ] ; then
              rm -f empty
              xsave empty modinfo xmodinfo input=.modinfo_input_card
            else
              echo " *** WARNING in pool4.dk:"
              echo " *** There are no DATA DESCRIPTION records."
            fi
          fi
#                                      strip off DATA DESCRIPTION records from gg?
          xdelet gg$n .gg$n input=.modinfo_input_card
          rm -f gg$n .gg${n}_Link ; mv .gg$n gg$n
          if [ $remnd -eq 0 -o $n -eq $join ] ; then
            echo "$pool_input_card" >> .pool_input_card
            pool_input_card="          "
          fi
          n=`expr $n + 1`
        done

#   ---------------------------------- select subset if 'poolsubset' is defined
        if [ ! -z "$poolsubset" ] ; then
#                                      determine the number of variables
          nvar=`echo $poolsubset | tr ';' '\n' | wc -l`
          if [ $nvar -gt 80 ] ; then
            echo " *** ERROR in pool4.dk:"
            echo " *** number of variables in 'poolsubset' must be <= 80."
            exit 1
          fi
#                                      build input cards for xfind and xsave
          args=""
          rm -f .xfind_input_card .xsave_input_card
          n=1
          while [ $n -le $nvar ] ; do
            var=`echo $poolsubset|cut -f$n -d';'|sed -e's/^ *//g' -e's/ *$//g'`
#                                      skip empty names
            if [ ! -z "$var" ] ; then
              echo "              $var"   >> .xfind_input_card
              echo "              $var
" >> .xsave_input_card
              args="$args v$n"
            fi
            n=`expr $n + 1`
          done
#                                      select and save requested variables
          pool_files1=""
          rm -f dummy
          for f in $pool_files ; do
            rm -f $args
            xfind $f    $args     input=.xfind_input_card
            xsave dummy $args .$f input=.xsave_input_card
            rm -f $args
            pool_files1="$pool_files1 .$f"
          done
          pool_files=$pool_files1
        fi
#   ---------------------------------- run 'pool' on input files:
#                                      tavg = output with time averages
#                                      stdv = output with standard deviations
#                                      covd = output with daily (co)variances
#                                      covm = output with monthly (co)variances
        cat .pool_input_card
        pool tavg stdv covd covm $pool_files input=.pool_input_card
        release $pool_files
        rm -f *ANO_POOL fort.*

#   ---------------------------------- save the results
        joinup newgp xmodinfo tavg stdv covd covm
        save   newgp ${flabel}$sfx
        release newgp xmodinfo tavg stdv covd covm

      done # sfx
