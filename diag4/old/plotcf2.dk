#deck plotcf2
jobname=plotcf2 ; time=$stime ; memory=$memory1
. comjcl.cdk

cat > Execute_Script <<end_of_script

# 
#   like previous plotcf except clear-sky components are determined by
#   "xfind" since saved in model version gcm7.
#                   plotcf2             ml. aug 6/92.
#   ----------------------------------- plot cloud forcing statistics in gcm.
#                                       plots cloud forcing at toa and ground 
#                                       for each of solar and terrestial
#                                       components.
#                                       also plots clear-sky components at
#                                       toa and ground.
#                                       used with gcm7 onward  and deck
#                                       radiag3 .
#
    libncar plunit=$plunit 
#   ----------------------------------- access saved fields from file.
    access ggfile ${flabel}gp
    xfind ggfile tcfst 
    xfind ggfile tcfsb 
    xfind ggfile tcflt 
    xfind ggfile tcflb 
    xfind ggfile tcft 
    xfind ggfile tcfb 
    xfind ggfile tfstc 
    xfind ggfile tfsgc 
    xfind ggfile tfltc 
    xfind ggfile tflgc 
#   ----------------------------------- zonally average the fields.
    zonavg tcfst ztcfst 
    zonavg tcfsb ztcfsb 
    zonavg tcflt ztcflt 
    zonavg tcflb ztcflb 
    zonavg tcft ztcft 
    zonavg tcfb ztcfb 
    zonavg tfstc ztfstc 
    zonavg tfsgc ztfsgc 
    zonavg tfltc ztfltc 
    zonavg tflgc ztflgc 
#   ----------------------------------- plot out the results.
.   plotid.cdk
    globavg tcfst output=dummy 
    globavg tcflt output=dummy 
    globavg tcft output=dummy 
    globavg tcfsb output=dummy 
    globavg tcflb output=dummy 
    globavg tcfb output=dummy 
    txtplot input=dummy 
# 
    ggplot tcfst 
    ggplot tcfsb 
    ggplot tcflt 
    ggplot tcflb 
    ggplot tcft 
    ggplot tcfb 
    ggplot tfstc 
    ggplot tfsgc 
    ggplot tfltc 
    ggplot tflgc 
    rm tcfst tcfsb tcflt tcflb tcft tcfb
    rm tfstc tfsgc tfltc tflgc
    xplot ztcfst 
    xplot ztcfsb 
    xplot ztcflt 
    xplot ztcflb 
    xplot ztcft 
    xplot ztcfb 
    xplot ztfstc 
    xplot ztfsgc 
    xplot ztfltc 
    xplot ztflgc 
.   plotid.cdk
.   plot.cdk

end_of_script

cat > Input_Cards <<end_of_data

+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
         DAYS      = $days                                                       
         RUN       = $run                                                        
         FLABEL    = $flabel                                                     
0 PLOTCF ------------------------------------------------------------ PLOTCF      
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  XFIND.      CFST                                                                
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  XFIND.      CFSB                                                                
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  XFIND.      CFLT                                                                
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  XFIND.      CFLB                                                                
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  XFIND.      CFT                                                                 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  XFIND.      CFB                                                                 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  XFIND.      FSTC                                                  
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  XFIND.      FSGC                     
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  XFIND.      FLTC                                                                
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  XFIND.      FLGC                                                                
. headfram.cdk                                                                    
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOTCFST         0 NEXT    1    0        1.     -200.      200.       20.0${b}8  
   RUN $run. DAYS $days. SOLAR CLOUD FORCING AT TOA. UNITS W/M-2.               
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOTCFSB         0 NEXT    1    0        1.     -200.      200.       20.0${b}8  
   RUN $run. DAYS $days. SOLAR CLOUD FORCING AT GROUND. UNITS W/M-2.            
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOTCFLT         0 NEXT    1    0        1.     -200.      200.       20.0${b}8  
   RUN $run. DAYS $days. TERRESTIAL CLOUD FORCING AT TOA. UNITS W/M-2.          
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOTCFLB         0 NEXT    1    0        1.     -200.      200.       20.0${b}8  
   RUN $run. DAYS $days. TERRESTIAL CLOUD FORCING AT GROUND. UNITS W/M-2.       
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT CFT         0 NEXT    1    0        1.     -200.      200.       20.0${b}8  
   RUN $run. DAYS $days. TOTAL CLOUD FORCING AT TOA. UNITS W/M-2.               
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT CFB         0 NEXT    1    0        1.     -200.      200.       20.0${b}8  
   RUN $run. DAYS $days. TOTAL CLOUD FORCING AT GROUND. UNITS W/M-2.            
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOTFSTC         0 NEXT    1    0        1.        0.      600.       50.0${b}8  
   RUN $run. DAYS $days. CLEAR-SKY SOLAR FLUX AT TOA. UNITS W/M-2.              
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOTFSGC         0 NEXT    1    0        1.        0.      600.       50.0${b}8  
   RUN $run. DAYS $days. CLEAR-SKY SOLAR FLUX AT GROUND. UNITS W/M-2.           
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOTFLTC         0 NEXT    1    0        1.     -400.      400.       25.0${b}8  
   RUN $run. DAYS $days. CLEAR-SKY TERRESTIAL FLUX AT TOA. UNITS W/M-2.         
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOTFLGC         0 NEXT    1    0        1.     -400.      400.       25.0${b}8  
   RUN $run. DAYS $days. CLEAR-SKY TERRESTIAL FLUX AT GROUND. UNITS W/M-2.      
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XPLOT CFST         0 NEXT    1     -200.        0.    1     -200.                 
   RUN $run. DAYS $days. SOLAR CLOUD FORCING AT TOA. UNITS W/M-2.               
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XPLOT CFSB         0 NEXT    1     -200.        0.    1     -200.                 
   RUN $run. DAYS $days. SOLAR CLOUD FORCING AT GROUND. UNITS W/M-2.            
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XPLOT CFLT         0 NEXT    1        0.      100.    1        0.                 
   RUN $run. DAYS $days. TERRESTIAL CLOUD FORCING AT TOA. UNITS W/M-2. 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XPLOT CFLB         0 NEXT    1        0.      100.    1        0.                 
   RUN $run. DAYS $days. TERRESTIAL CLOUD FORCING AT GROUND. UNITS W/M-2.       
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XPLOT  CFT         0 NEXT    1     -100.      100.    1     -100.                 
   RUN $run. DAYS $days. TOTAL CLOUD FORCING AT TOA. UNITS W/M-2.               
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XPLOT  CFB         0 NEXT    1     -100.      100.    1     -100.                 
   RUN $run. DAYS $days. TOTAL CLOUD FORCING AT GROUND. UNITS W/M-2.            
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XPLOT FSTC         0 NEXT    1        0.      500.    1        0.                 
   RUN $run. DAYS $days. CLEAR-SKY SOLAR FLUX AT TOA. UNITS W/M-2.              
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XPLOT FSGC         0 NEXT    1        0.      500.    1        0.                 
   RUN $run. DAYS $days. CLEAR-SKY SOLAR FLUX AT GROUND. UNITS W/M-2.           
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XPLOT FLTC         0 NEXT    1     -400.        0.    1     -400.                 
   RUN $run. DAYS $days. CLEAR-SKY TERRESTIAL FLUX AT TOA. UNITS W/M-2.         
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XPLOT FLGC         0 NEXT    1     -400.        0.    1     -400.                 
   RUN $run. DAYS $days. CLEAR-SKY TERRESTIAL FLUX AT GROUND. UNITS W/M-2.      
. tailfram.cdk                                                                    

end_of_data

. endjcl.cdk


