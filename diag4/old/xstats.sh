#!/bin/sh
#                  xstats              rl,dl,mh,sk - Apr 18/06 - fm
#
#   sk: rewrite for multiple tracers; use input=.input_card for input cards
#   fm: ensure "select" program call is preceded by "ccc" prefix.

#   ---------------------------------- calculate tracer statistics
#                                      and covariances with u, v and w.
#                                      tracer names are specified in variable $trac.
#

      access oldgp ${flabel}gp
#   ---------------------------------- get beta for zonal averaging
      echo "XFIND.        $d" | ccc xfind  oldgp beta

#   ---------------------------------- access u and v
      access u ${flabel}_gpu
      access v ${flabel}_gpv
#                                      calculate time deviations for u and v
      timavg u tu up
      timavg v tv vp
      rm u v
#                                      calculate deviations from zonal mean
      rzonavg tv beta rztv tvs
#                                      access w and perform calculations for w
      if [ "$wxstats" = "on" ]; then
        access w ${flabel}_gpw
        timavg w tw wp
        rm w
        rzonavg tw beta rztw tws
      fi

      xtstat_getdata="y"
#   ---------------------------------- process all tracers
      for x in $trac ; do
#                                      form a 4-character field name
	NAME=`echo "    $x" | tail -5c`

#   ---------------------------------- try to access the tracer
	access gpx ${flabel}_gpx_${x} na

	if [ ! -f gpx ] ; then

#   ---------------------------------- get the tracer    

	  if [ "$xtstat_getdata" = "y" ] ; then
#   ---------------------------------- get model files
#                                      spectral sigma case
	    if [ "$datatype" = "specsig" ] ; then
.             spfiles.cdk
echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME LNSP" | ccc select npaksp sslnsp
echo "COFAGG    $lon$lat" | ccc cofagg sslnsp gslnsp
              if [ "$itrvar" = "SL3D" ] ; then
.               ggfiles.cdk
	      fi
	    fi
#                                      spectral pressure case
	    if [ "$datatype" = "specpr" ] ; then
.             spfiles.cdk
	    fi
#                                      grid pressure case
	    if [ "$datatype" = "gridpr" ] ; then
.             ggfiles.cdk
	    fi
	    xtstat_getdata="n"
	  fi

#   ---------------------------------- get tracer on pressure levels.
#                                      spectral-sigma case.
	  if [ "$datatype" = "specsig" ] ; then
	    if [ "$itrvar" = "SL3D" ] ; then
	      echo "SELECT    STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME $NAME" > .select_input_card
ccc           select npakgg gsx input=.select_input_card
	    else
	      echo "SELECT    STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME $NAME" > .select_input_card
ccc           select npaksp ssx input=.select_input_card
	      echo "COFAGG.   $lon$lat    0$npg" > .cofagg_input_card
              cofagg ssx gsx input=.cofagg_input_card
              rm ssx
	    fi
	    echo "GSAPL.    $plv       0.0       0.0$coord$plid
$p01$p02$p03$p04$p05$p06$p07$p08$p09$p10$p11$p12$p13$p14$p15$p16
$p17$p18$p19$p20$p21$p22$p23$p24$p25$p26$p27$p28$p29$p30$p31$p32
$p33$p34$p35$p36$p37$p38$p39$p40$p41$p42$p43$p44$p45$p46$p47$p48
$p49$p50$p51$p52$p53$p54$p55$p56$p57$p58$p59$p60$p61$p62$p63$p64
$p65$p66$p67$p68$p69$p70$p71$p72$p73$p74$p75$p76$p77$p78$p79$p80
$p81$p82$p83$p84$p85$p86$p87$p88$p89$p90$p91$p92$p93$p94$p95$p96
$p97$p98$p99$p100" > .gsapl_input_card
            gsapl gsx gslnsp gpx input=.gsapl_input_card
            rm gsx
	  fi
#                                      spectral-pressure case.
	  if [ "$datatype" = "specpr" ] ; then
	    echo "SELECT    STEPS $t1 $t2 $t3 LEVS$p01$pmaxl NAME $NAME" > .select_input_card
ccc         select npaksp spx input=.select_input_card
	    echo "COFAGG.   $lon$lat    0$npg" > .cofagg_input_card
            cofagg spx gpx input=.cofagg_input_card
            rm spx
	  fi
#                                      grid-pressure case.
	  if [ "$datatype" = "gridpr" ] ; then
	    echo "SELECT    STEPS $t1 $t2 $t3 LEVS$p01$pmaxl NAME $NAME" > .select_input_card
ccc         select npaksp gpx input=.select_input_card
	  fi
	fi

#                                      calculate time derivatives
        if [ "$datatype" = "specsig" ]; then
	  echo "DIFF       $NAME $t1 $t2 $delt"           > .dif_input_card
        else
	  echo "DIFF       $NAME $t1 $t2 $delt       1.0" > .dif_input_card
        fi
	dif gpx dxdt input=.dif_input_card
#                                      calculate mean, deviations and variance
        timavg gpx tx xp txp2
        rm gpx
#                                      calculate covariances
        timcov xp up txpup
        timcov xp vp txpvp
#                                      xsave gridded statistics
	echo "XSAVE.        $x
NEWNAM.
XSAVE.        D${x}/DT
NEWNAM.      X.
XSAVE.        ${x}\"${x}\"
NEWNAM.    X\"X\"
XSAVE.        ${x}\"U\"
NEWNAM.    X\"U\"
XSAVE.        ${x}\"V\"
NEWNAM.    X\"V\"" > .xsave_input_card
        xsave new_gp tx dxdt txp2 txpup txpvp new. input=.xsave_input_card
        mv new. new_gp

#                                      zonal averages
#                                      (x)r, (x*x*)r and (x*v*)r
        rzonavg tx    beta rztx  txs rztxs2
        mlt     txs   tvs  txsvs
        rzonavg txsvs beta rztxsvs
#                                      (dx/dt)r
        rzonavg dxdt  beta rzdxdt
#                                      (x"x")r and (x"v")r
        rzonavg txp2  beta rztxp2
        rzonavg txpvp beta rztxpvp
#                                      xsave zonal statistics
	echo "XSAVE.        (${x})R
NEWNAM.
XSAVE.        (D${x}/DT)R
NEWNAM.    X/DT
XSAVE.        (${x}\"${x}\")R
NEWNAM.    X\"X\"
XSAVE.        (${x}*${x}*)R
NEWNAM.    X*X*
XSAVE.        (${x}\"V\")R
NEWNAM.    X\"V\"
XSAVE.        (${x}*V*)R
NEWNAM.    X*V*" > .xsave_input_card
        xsave new_xp rztx rzdxdt rztxp2 rztxs2 rztxpvp rztxsvs new. input=.xsave_input_card
        mv new. new_xp

#   ---------------------------------- perform calculations involving w
        if [ "$wxstats" = "on" ]; then
	  timcov xp wp txpwp
#                                      xsave gridded statistics
	  echo "XSAVE.        ${x}\"W\"
NEWNAM.    X\"W\"" > .xsave_input_card
          xsave new_gp txpwp new. input=.xsave_input_card
          mv new. new_gp

#                                      (x"w")r and (x*w*)r
          rzonavg txpwp beta rztxpwp
          mlt     txs   tws  txsws
          rzonavg txsws beta rztxsws
#                                      xsave zonal statistics
	  echo "XSAVE.        (${x}\"W\")R
NEWNAM.    X\"W\"
XSAVE.        (${x}*W*)R
NEWNAM.    X*W*" > .xsave_input_card
          xsave new_xp rztxpwp rztxsws new. input=.xsave_input_card
          mv new. new_xp
        fi
      done

#   ---------------------------------- save results.
      release oldgp
      access  oldgp ${flabel}gp
      xjoin   oldgp new_gp newgp
      save    newgp ${flabel}gp
      delete  oldgp

      if [ "$rcm" != "on" ] ; then
      release oldxp
      access  oldxp ${flabel}xp
      xjoin   oldxp new_xp newxp
      save    newxp ${flabel}xp
      delete  oldxp
      fi
