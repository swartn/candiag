#deck anomaly
jobname=anomaly ; time=$stime ; memory=$memory1
. comjcl.cdk

cat > Execute_Script <<end_of_script

#   Replace -273. by -273.16
#                   anomaly             gjb,ec. - Nov 21/00.
#   ----------------------------------- computes differences of mean fields
#                                       for basic data. adjusts the layering
#                                       and resolution so that model and
#                                       observed data may be used or models
#                                       with different resolution.
# 
#                                       model1 and letter x refer to control
#                                       or observations, while model2 and
#                                       letter y refer to experiment.
# 
#                                       obsfile refers to monthly, seasonally,
#                                       or annually averaged surface
#                                       observations.
# 
#
    libncar plunit=$plunit 
.   plotid.cdk
# 
#   ----------------------------------- access the data.
# 
    access filex ${model1}gp
    access filey ${model2}gp
    if [ "$obsdat" = on ] ; then
      access sfcdat $obsfile
    fi
    if [ "$obsdat" != on ] ; then
      cp filex sfcdat 
    fi
# 
#   ----------------------------------- surface anomalies.
# 
#                                       pmsl.
    xfind filey y 
    xfind sfcdat x 
    sub y x dif 
    ggplot dif 
    rm y x dif
# 
#                                       precipitation (convert to mm/day).
    xfind filey y 
    xfind sfcdat x 
    sub y x dif1 
    xlin dif1 dif2 
    ggplot dif2 
    rm y x dif1 dif2
# 
#                                       outgoing longwave radiation - positive
#                                       upwards.
    xfind filey flg 
    xfind filey fla 
    add flg fla flag 
    xlin flag y 
    rm flg fla flag
# 
    if [ "$obsdat" = on ] ; then
      xfind sfcdat x 
      fmask x mask 
      mlt y mask my 
      mlt x mask mx 
      mv my y 
      mv mx x 
    fi
    if [ "$obsdat" != on ] ; then
      xfind filex flg 
      xfind filex fla 
      add flg fla flag 
      xlin flag x 
      rm flg fla flag
    fi
# 
    sub y x dif 
    ggplot dif 
    rm y x dif mask
# 
#                                       (local) planetary albedo.
    xfind filey y 
    xfind sfcdat x 
    fmask x mask 
    mlt y mask my 
    mlt x mask mx 
    sub my mx dif 
    ggplot dif 
    rm y x dif mask my mx
# 
#                                       balance at top of atmosphere.
    xfind filey y 
    xfind sfcdat x 
    fmask x mask 
    mlt y mask my 
    mlt x mask mx 
    sub my mx dif 
    ggplot dif 
    rm y x dif mask my mx
# 
#                                       screen temperature.
    xfind filey y 
    xfind sfcdat x 
    if [ "$legates" = on ] ; then
      xlin x z
    mv z x
    fi
    sub y x dif 
    ggplot dif 
    rm y sfcdat x dif
# 
#   ----------------------------------- atmospheric anomalies.
# 
#                                       adjust observed levels if needed.
.   datfix.cdk
# 
#                                       get beta = beta1*beta2
#                                       (0 if either beta1 or beta2 is zero).
    xfind filex x 
    xfind filey y 
.   rslfix.cdk
    mlt x y beta 
    rm x y
#                                       get zonal and meridional velocities and
#                                       calculate simple (no beta) version of
#                                       v* to be used by comdeck testx. also
#                                       calculate various u,v standing eddy
#                                       statistics to be plotted later.
    xfind filex ux 
    xfind filex vx 
    xfind filey uy 
    xfind filey vy 
.   testv.cdk
# 
#                                       temperature.
.   testx.cdk
#                                       850 and 200 mb.
    sub y x tdif 
ccc select tdif tdiff 
ccc select tdif tdif200 
    rm x y tdif
# 
ccc select beta beta850 
    mlt tdiff beta850 tdif850 
    ggplot tdif850 
    ggplot tdif200 
    rm beta850 tdiff tdif850 tdif200
#                                       t, t*t*, v*t*
    zxlook zdif 
    zxplot zdif 
    if [ "$xtradif" = on ] ; then
      zxplot sesd 
      zxplot setran 
      rm x y zdif sesd setran
#                                         t't'
.     testsq.cdk
      zxplot zdif 
      rm x y zdif
#                                         v't'
.     testx.cdk
      zxplot zdif 
    fi
    rm x y zdif sesd setran
# 
#                                       zonal velocity.
# 
#                                       u, u*u*, u*v*
    zxplot zudif 
    rm zudif
# 
    if [ "$xtradif" = on ] ; then
      zxplot seusd 
      zxplot seutran 
#                                         u'u'
.     testsq.cdk
      zxplot zdif 
      rm x y zdif seusd seutran
#                                         u'v'
.     testx.cdk
      zxplot zdif 
      rm x y zdif sesd setran
#   
#                                         meridional velocity.
#   
#                                         v*v*
      zxplot sevsd 
      rm sevsd
#                                         v'v'
.     testsq.cdk
      zxplot zdif 
      rm x y zdif
    fi
# 
#                                       vector plots of (u,v).
#                                       850 and 200 mb.
    joinup x ux vx 
    joinup y uy vy 
ccc select x u850x v850x 
ccc select x u200x v200x 
ccc select y u850y v850y 
ccc select y u200y v200y 
    rm uy vy ux vx x y
# 
    sub u850y u850x udif850 
    sub v850y v850x vdif850 
    square udif850 u2 
    square vdif850 v2 
    add u2 v2 amp2 
    sqroot amp2 amp 
    ggplot amp udif850 vdif850 
    rm udif850 vdif850 u2 v2 amp2 amp
# 
    sub u200y u200x udif200 
    sub v200y v200x vdif200 
    square udif200 u2 
    square vdif200 v2 
    add u2 v2 amp2 
    sqroot amp2 amp 
    ggplot amp udif200 vdif200 
# 
    rm udif200 vdif200 u2 v2 amp2 amp
# 
#                                       velocity potential at 850 and 200 mbs
#                                       (mean fields).
# 
#                                       model run.
    gwtqd u200y v200y vor200 div200 
    gwtqd u850y v850y vor850 div850 
    splinv div200 nchi200 
    splinv div850 nchi850 
    rm u200y v200y u850y v850y vor200 div200 vor850 div850
    xlin nchi200 schi200 
    xlin nchi850 schi850 
    cofagg schi200 c200y 
    cofagg schi850 c850y 
    rm nchi200 nchi850 schi200 schi850
# 
#                                       control run.
    gwtqd u200x v200x vor200 div200 
    gwtqd u850x v850x vor850 div850 
    splinv div200 nchi200 
    splinv div850 nchi850 
    rm u200x v200x u850x v850x vor200 div200 vor850 div850
    xlin nchi200 schi200 
    xlin nchi850 schi850 
    cofagg schi200 c200x 
    cofagg schi850 c850x 
    rm nchi200 nchi850 schi200 schi850
# 
    joinup chi c850y c200y c850x c200x 
    ggplot chi 
    rm c200y c850y c200x c850x chi
# 
#                                       specific humidity.
.   testx.cdk
#                                       q
    zxplot zdif 
    rm x y zdif sesd setran
# 
.   plotid.cdk
.   plot.cdk

end_of_script

cat > Input_Cards <<end_of_data

+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
         DAYS      = $days
         RUN       = $run
         MODEL1    = $model1
         MODEL2    = $model2
         OBSFILE   = $obsfile
0 ANOMALY -------------------------------------------------------------- ANOMALY
. headfram.cdk
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        PMSL
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
if [ "$obsdat" = on ] ; then
XFIND.        PMSL*${obsday}*NMC (MB)
fi
if [ "$obsdat" != on ] ; then
XFIND.        PMSL
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  GGPLOT.          0 NEXT    1$map        1.     -100.      100.        2.0${b}8
    RUN $run.  DAYS $days. (EXPMT-CNTRL) FOR MSL PRESSURE.  UNITS MB.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        PCP
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
if [ "$obsdat" = on ] ; then
if [ "$legates" != on ] ; then
XFIND.        PCP*${obsday}*JAEGER (KG/M2-S)
fi
if [ "$legates" = on ] ; then
XFIND.        PCP*${obsday}*(LEGATES AND WILLMOTT)
fi
fi
if [ "$obsdat" != on ] ; then
XFIND.        PCP
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  XLIN.       86400.        0.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT PCP         0 NEXT    1 1  0        1.      -50.       50.        2.0${b}8
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR PRECIPITATION RATE. UNITS MM/DAY.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        FLG
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        FLA
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  XLIN.          -1.        0.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
if [ "$obsdat" = on ] ; then
XFIND.        OLR*${obsday}*ERBE (W/M2)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  FMASK.             NEXT   LT      500.
fi
if [ "$obsdat" != on ] ; then
XFIND.        FLG
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        FLA
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  XLIN.          -1.        0.
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT OLR         0 NEXT    1    0        1.     -200.      200.       10.0${b}8
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR OLR. UNITS W/M2
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        LPA
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
if [ "$obsdat" = on ] ; then
XFIND.        LPA*${obsday}*ERBE (FRACTION)
fi
if [ "$obsdat" != on ] ; then
XFIND.        LPA
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  FMASK.             NEXT   LT        2.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT LPA         0 NEXT    1    0      100.     -100.      100.        5.0${b}8
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR LPA (PERCENTAGE).
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        BALT
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
if [ "$obsdat" = on ] ; then
XFIND.        BALT*${obsday}*ERBE (W/M2)
fi
if [ "$obsdat" != on ] ; then
XFIND.        BALT
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  FMASK.             NEXT   LT      200.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOTBALT         0 NEXT    1    0        1.     -200.      200.       10.0${b}8
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR BALANCE AT TOP OF ATM. UNITS W/M2
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        ST
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
if [ "$obsdat" = on ] ; then
if [ "$legates" != on ] ; then
XFIND.        ST*${obsday}*NCAR (DEG C)
fi
if [ "$legates" = on ] ; then
XFIND.        ST*${obsday}*(LEGATES AND WILLMOTT)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
 XLIN.            1.   -273.16
fi
fi
if [ "$obsdat" != on ] ; then
XFIND.        ST
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT  ST         0 NEXT    1    0        1.      -50.       50.        2.0${b}8
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR SCREEN TEMPERATURE. UNITS DEG C.
. fixdat.cdk
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        $d
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        $d
. rsldat.cdk
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        U
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        V
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        U
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        V
. rsldatv.cdk
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        T
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        T
. rsldat.cdk
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  SELECT.       $t1 $t2 $t3 LEVS  850  850 NAME  ALL
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  SELECT.       $t1 $t2 $t3 LEVS  200  200 NAME  ALL
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  SELECT.       $t1 $t2 $t3 LEVS  850  850 NAME  ALL
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT   T         0 NEXT   -1    0        1.      -50.       50.        2.0${b}8
RUN $run. DAYS $days. $model2-$model1 FOR T AT 850 MBS. UNITS DEG K.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT   T         0 NEXT   -1    0        1.      -50.       50.        2.0${b}8
RUN $run. DAYS $days. $model2-$model1 FOR T AT 200 MBS. UNITS DEG K.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXLOOK.    NEXT$plv      1.E0      1.E0   22   33$kax$kin
RUN $run  DAYS $days. $model2-$model1 FOR (T)R. UNITS DEG K.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    NEXT    0    0$lxp      1.E0    -40.E0     40.E0      1.E0$kax$kin
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR (T)R. UNITS DEG K.
if [ "$xtradif" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    NEXT    0    0$lxp      1.E0     -1.E2      1.E2     5.E-1$kax$kin
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR ((T*T*)R)**0.5. UNITS DEG K.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    NEXT    0    0$lxp      1.E0     -2.E2      2.E2      2.E0$kax$kin
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR (V*T*)R. UNITS M*DEGK/SEC.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        T"T"
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        T"T"
. rsldat.cdk
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    NEXT    0    0$lxp      1.E0     -1.E2      1.E2     5.E-1$kax$kin
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR ((T+T+)R)**0.5. UNITS DEG K.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        T"V"
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        T"V"
. rsldat.cdk
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    NEXT    0    0$lxp      1.E0     -2.E2      2.E2      2.E0$kax$kin
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR (V+T+)R. UNITS M*DEGK/SEC.
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    NEXT    0    0$lxp      1.E0    -40.E0     40.E0       2.5$kax$kin
RUN $run. DAYS $days. $model2-$model1 FOR (U)R. UNITS M/SEC.
if [ "$xtradif" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    NEXT    0    0$lxp      1.E0     -1.E2      1.E2     5.E-1$kax$kin
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR ((U*U*)R)**0.5. UNITS M/SEC.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    NEXT    0    0$lxp      1.E0     -2.E2      2.E2      2.E0$kax$kin
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR (U*V*)R. UNITS (M/SEC)2.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        U"U"
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        U"U"
. rsldat.cdk
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    NEXT    0    0$lxp      1.E0     -1.E2      1.E2     5.E-1$kax$kin
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR ((U+U+)R)**0.5. UNITS M/SEC.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        U"V"
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        U"V"
. rsldat.cdk
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    NEXT    0    0$lxp      1.E0     -2.E2      2.E2      2.E0$kax$kin
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR (U+V+)R. UNITS (M/SEC)2.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    NEXT    0    0$lxp      1.E0     -1.E2      1.E2     5.E-1$kax$kin
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR ((V*V*)R)**0.5. UNITS M/SEC.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        V"V"
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        V"V"
. rsldat.cdk
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    NEXT    0    0$lxp      1.E0     -1.E2      1.E2     5.E-1$kax$kin
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR ((V+V+)R)**0.5. UNITS M/SEC.
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  SELECT.       $t1 $t2 $t3 LEVS  850  850 NAME    U    V
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  SELECT.       $t1 $t2 $t3 LEVS  200  200 NAME    U    V
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  SELECT.       $t1 $t2 $t3 LEVS  850  850 NAME    U    V
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  SELECT.       $t1 $t2 $t3 LEVS  200  200 NAME    U    V
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1   -1  850$map        1.        0.       60.        4.2${b}8
RUN $run. DAYS $days. $model2-$model1 FOR MEAN WIND AT 850 MBS. UNITS M/SEC
                  1.        0.       10.$ncx$ncy
                  VECPLOT OF (U,V).                  UNITS M/SEC
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1   -1  200$map        1.        0.       60.        4.2${b}8
RUN $run. DAYS $days. $model2-$model1 FOR MEAN WIND AT 200 MBS. UNITS M/SEC
                  1.        0.       20.$ncx$ncy
                  VECPLOT OF (U,V).                  UNITS M/SEC
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
   GWTQD  $lrt$lmt$typ    1
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
   GWTQD  $lrt$lmt$typ    1
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  XLIN.          -1.        0.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  XLIN.          -1.        0.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
   COFAGG $lon$lat
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
   COFAGG $lon$lat
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
   GWTQD  $lrt$lmt$typ    1
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
   GWTQD  $lrt$lmt$typ    1
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  XLIN.          -1.        0.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  XLIN.          -1.        0.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
   COFAGG $lon$lat
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
   COFAGG $lon$lat
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1 NEXT  850$map     1.E-6     -100.      100.        1.0${b}8
 RUN $run. DAYS $days. $model2 850 MB VELOCITY POTENTIAL. UNITS 1.E6*M2/SEC.
GGPLOT.           -1 NEXT  200$map     1.E-6     -100.      100.        1.0${b}8
 RUN $run. DAYS $days. $model2 200 MB VELOCITY POTENTIAL. UNITS 1.E6*M2/SEC.
GGPLOT.           -1 NEXT  850$map     1.E-6     -100.      100.        1.0${b}8
 RUN $run. DAYS $days. $model1 850 MB VELOCITY POTENTIAL. UNITS 1.E6*M2/SEC.
GGPLOT.           -1 NEXT  200$map     1.E-6     -100.      100.        1.0${b}8
 RUN $run. DAYS $days. $model1 200 MB VELOCITY POTENTIAL. UNITS 1.E6*M2/SEC.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        Q
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        Q
. rsldat.cdk
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    NEXT    0    0$lxp      1.E3    -40.E0     40.E0      0.25    0$kin
RUN $run. DAYS $days. $model2-$model1 FOR (Q)R. UNITS G*KG.
. tailfram.cdk

end_of_data

. endjcl.cdk


