#deck mergets
jobname=mergets ; time=$stime ; memory=$memory1
.   comjcl.cdk

cat > Execute_Script <<'end_of_script'

# 
#                  mergets             sk - Apr 27 2009 - sk 
#   ---------------------------------- Merge time series
#
# Input time series chunks are defined as
#
# ${mts_uxxx}_${runid}_${time_years1}_${sfx}_${var}
# ${mts_uxxx}_${runid}_${time_years2}_${sfx}_${var}
# ...
# where ${time_years1} are in ${year1}_${year2} format,
# sfx is in the list diag_suffix_list, e.g., diag_suffix_list="gp xp cp",
#
#                                      make sure that all necessary parameters are defined
    if [ ! -n "$runid" ] ; then
      echo " *** Error in mergets.dk: Model runid is undefined. ***"
      echo " *** Error in mergets.dk: Model runid is undefined. ***" >> haltit
      exit 1
    fi

    if [ ! -n "$mts_uxxx" ] ; then
      echo " *** Error in mergets.dk: Monthly time series prefix mts_uxxx is undefined. ***"
      echo " *** Error in mergets.dk: Monthly time series prefix mts_uxxx is undefined. ***" >> haltit
      exit 1
    fi

    if [ ! -n "$diag_suffix_list" ] ; then
      echo " *** Error in mergets.dk: Diagnostic file suffix list diag_suffix_list is undefined. ***"
      echo " *** Error in mergets.dk: Diagnostic file suffix list diag_suffix_list is undefined. ***" >> haltit
      exit 1
    fi

    if [ -n "$vars" -a `echo ${diag_suffix_list} | wc -w` -gt 1 ] ; then
      echo " *** Error in mergets.dk: Variables are defined for more than one suffix. ***"
      echo " *** Error in mergets.dk: Variables are defined for more than one suffix. ***" >> haltit
      exit 1
    fi

    if [ ! -n "$time_chunks" ] ; then
      echo " *** Error in mergets.dk: time_chunks is undefined. ***"
      echo " *** Error in mergets.dk: time_chunks is undefined. ***" >> haltit
      exit 1
    fi
#                                      default edition number
    if [ ! -n "$time_ed" ] ; then
      edition=""
    else
      edition="ed=$time_ed"
    fi
#                                      default maximum time series size
    if [ ! -n "$time_max_size" ] ; then
      time_max_size="8192"
    fi

#                                      set default check_time_merged, if not defined
    if [ ! -n "$check_time_merged" ] ; then
      check_time_merged="on"
    fi

#                                      set default time_input_dir, if not defined
    if [ ! -n "$time_input_dir" ] ; then
      time_input_dir="$RUNPATH"
    fi

#                                      set resave_chunks
    resave_chunks="on" # default value
    if [ "$time_input_dir" = "$RUNPATH" ] ; then
      resave_chunks="off" # don't resave if RUNPATH=time_input_dir
    fi

#                                      check continuity of years/months in chunks
    nch=1
    while [ $nch -le ${time_chunks} ] ; do
      eval yrs=\${time_years$nch}
      if [ ! -n "$yrs" ] ; then
        echo " *** Error in mergets.dk: time_years$nch is undefined. ***"
        echo " *** Error in mergets.dk: time_years$nch is undefined. ***" >> haltit
        exit 1
      fi
      yr1=`echo $yrs | cut -f1 -d'_'`
      yr2=`echo $yrs | cut -f2 -d'_'`
      lyr1=${#yr1}
      lyr2=${#yr2}
      if [ $lyr2 -eq 3 -a `echo $yr2 | cut -c1` = "m" ] ; then
	# chunks are monthly files
	monthly="on"
	mon1=`echo $lyr2 | cut -c2-3`
      else
	# chunks are (multi)yearly files
	monthly="off"
	if [ $lyr1 -ne $lyr2 ] ; then
	  echo " *** Error in mergets.dk: First and last dates in $yrs must have the same format. ***"
	  echo " *** Error in mergets.dk: First and last dates in $yrs must have the same format. ***" >> haltit
	  exit 1
	fi
      fi
      if [ $lyr1 -le 4 ] ; then
        datefmt="yyyy"
      elif [ $lyr1 -gt 4 -a $lyr1 -le 6 ] ; then
        datefmt="yyyymm"
      else
        echo " *** Error in mergets.dk: Invalid date format in $yrs. ***"
        echo " *** Error in mergets.dk: Invalid date format in $yrs. ***" >> haltit
        exit 1
      fi
      if [ $nch -gt 1 ] ; then
	if [ "$monthly" = "on" ] ; then
	  if [ $yr1 -ne $yr1p -o $mon1 -ne $mon1p ] ; then
	    echo " *** Error in mergets.dk: Years $yrs are not continuous. ***"
	    echo " *** Error in mergets.dk: Years $yrs are not continuous. ***" >> haltit
	    exit 1
	  fi
	else
	  if [ $yr1 -ne $yr2p ] ; then
	    echo " *** Error in mergets.dk: Years $yrs are not continuous. ***"
	    echo " *** Error in mergets.dk: Years $yrs are not continuous. ***" >> haltit
	    exit 1
	  fi
	fi
      fi
      if [ "$monthly" = "on" ] ; then
	mon1p=`expr $mon1 + 1`
	if [ $mon1p -gt 12 ] ; then
	  yr1p=`expr $yr1 + 1`
	  mon1p=`expr $mon1p - 12`
	else
	  yr1p=$yr1
	fi
      else
	yr2p=`expr $yr2 + 1`
	if [ "$datefmt" = "yyyymm" ] ; then
	  rem=`expr $yr2p % 100`
	  if [ $rem -eq 13 ] ; then
	    yr2p=`expr $yr2p + 88`
	  fi
	fi
      fi
      nch=`expr $nch + 1`
    done

    if [ -z "$vars" ] ; then
      find_vars="on"
    else
      find_vars="off"
    fi
#                                      diagnostic file type loop
    for sfx in ${diag_suffix_list} ; do
      sfxs="${sfx}_" 
      echo "sfx=$sfx sfxs=$sfxs"
#                                      first year
      nch=1
      eval yrs1=\${time_years$nch}
      if [ "$monthly" = "on" ] ; then
	yrf=`echo $yrs1 | tr -d '_'`
      else
	yrf=`echo $yrs1 | cut -f1 -d'_'`
	if [ "$datefmt" = "yyyy" ] ; then
	  yrf4=`echo $yrf | awk '{printf "%4d", $0}'`
	  yrmmf=`echo $yrf | awk '{printf "%4d01", $0}'`
	elif [ "$datefmt" = "yyyymm" ] ; then
	  yrmmf=`echo $yrf | awk '{printf "%6d", $0}'`
	fi
      fi
#                                      last year
      nch=${time_chunks}
      eval yrs2=\${time_years$nch}
      if [ "$monthly" = "on" ] ; then
	yrl=`echo $yrs2 | tr -d '_'`
      else
	yrl=`echo $yrs2 | cut -f2 -d'_'`
	if [ "$datefmt" = "yyyy" ] ; then
	  yrl4=`echo $yrl | awk '{printf "%4d", $0}'`
	  yrmml=`echo $yrl | awk '{printf "%4d12", $0}'`
	elif [ "$datefmt" = "yyyymm" ] ; then
	  yrmml=`echo $yrl | awk '{printf "%6d", $0}'`
	fi
      fi
#                                      list of variables (determined from the first time perid)
      pwd=`pwd`
      cd $time_input_dir
      if [ "$find_vars" = "on" ] ; then
	if [ `echo "$sfx" | cut -c1` = "_" ] ; then
	  sfxs="`echo $sfx | cut -c2-`_"
	  vars=`ls ${mts_uxxx}_${runid}_${yrs1}_${sfx}*.[0-9][0-9][0-9] | cut -f1 -d'.' | sed -e "s/${mts_uxxx}_${runid}_${yrs1}_${sfx}//g" | sort -u`
	else
	  sfxs="${sfx}_" 
	  vars=`ls ${mts_uxxx}_${runid}_${yrs1}_${sfx}_*.[0-9][0-9][0-9] | cut -f1 -d'.' | sed -e "s/${mts_uxxx}_${runid}_${yrs1}_${sfx}_//g" | sort -u`
	fi
      fi
      echo sfxs=$sfxs 
      echo vars=$vars
      cd $pwd
#                                      variable loop
      for v in $vars ; do
        var_merged="off"
#                                      skip *_file_list
        if [ `echo "$v" | cut -c1-9` != "file_list" -a "$v" != "ipcc_file_list" ] ; then
#                                      time series loop
          missing="off"
          list=""
          nch=1
          while [ $nch -le ${time_chunks} ] ; do
            eval yrs=\${time_years$nch}
            release x$nch
            access  x$nch ${mts_uxxx}_${runid}_${yrs}_${sfxs}${v} $edition na
            if [ -s x$nch ] ; then
              list="$list x$nch"
            else
              missing="on"
            fi
            nch=`expr $nch + 1`
          done
          echo list=$list
          if [ "$missing" = "off" ] ; then
#                                      joinup and save
            if [ "$v" = "data_description" -o `echo "$v" | cut -f1-2 -d'_'` = "ocean_model" ] ; then
#                                      data description and ocean_model_* parms
              save x1 ${mts_uxxx}_${runid}_${yrf}_${yrl}_${sfxs}${v}
#                                      resave data description files for each chunk
              if [ "$resave_chunks" != "off" ] ; then
                nch=1
                while [ $nch -le ${time_chunks} ] ; do
                  eval yrs=\${time_years$nch}
                  save x$nch ${mts_uxxx}_${runid}_${yrs}_${sfxs}${v}
                  nch=`expr $nch + 1`
                done
                var_merged="on"
              fi
            else
#                                      determine the size of output merged file     
              outsize=`du -cmL $list | tail -1 | cut -f1`
              echo outsize=$outsize
              if [ $outsize -le $time_max_size ] ; then
                joinupts xall $list
#                                      check continuity of merged files
                if [ "$check_time_merged" = "on" ] ; then
                  if [ "$sfx" = "gp" -o "$sfx" = "cp" ] ; then
                    #                                    monthly means
                    echo "C*TSTEPCHK    ${yrmmf}    ${yrmml}    YYYYMM" | ccc tstepchk xall
                  elif [  "$sfx" = "gz" ] ; then
                    if [ `echo $v | cut -f1 -d'_'` = "annual" ] ; then
                      #                                  annual
                      echo "C*TSTEPCHK      ${yrf4}      ${yrl4}      YYYY" | ccc tstepchk xall
                    else
                      #                                  monthly means
                      echo "C*TSTEPCHK    ${yrmmf}    ${yrmml}    YYYYMM" | ccc tstepchk xall
                    fi
                  elif [ "$sfx" = "dd" ] ; then
                    if [ `echo $v | cut -f1 -d'_'` = "derived" ] ; then
                      #                                  monthly means
                      echo "C*TSTEPCHK    ${yrmmf}    ${yrmml}    YYYYMM" | ccc tstepchk xall
                    elif [ `echo $v | cut -f2 -d'_'` = "6h" ] ; then
                      #                                  6-hourly
                      echo "C*TSTEPCHK${yrmmf}0100${yrmml}3118YYYYMMDDHH    4   00   06   12   18" | ccc tstepchk xall
                    else
                      #                                  daily means
                      echo "C*TSTEPCHK  ${yrmmf}01  ${yrmml}31  YYYYMMDD" | ccc tstepchk xall
                    fi
                  elif [ "$sfx" = "dp" ] ; then
                    if [ `echo $v | cut -f2 -d'_'` = "6h" ] ; then
                      #                                  6-hourly
                      echo "C*TSTEPCHK${yrmmf}0100${yrmml}3118YYYYMMDDHH    4   00   06   12   18" | ccc tstepchk xall
                    else
                      #                                  daily means
                      echo "C*TSTEPCHK  ${yrmmf}01  ${yrmml}31  YYYYMMDD" | ccc tstepchk xall
                    fi
                  elif [ "$sfx" = "ds" ] ; then
                    #                                    6-hourly
                    echo "C*TSTEPCHK${yrmmf}0100${yrmml}3118YYYYMMDDHH    4   00   06   12   18" | ccc tstepchk xall
                  elif [ "$sfx" = "orb2gg" ] ; then
                    #                                  monthly means
                    echo "C*TSTEPCHK    ${yrmmf}    ${yrmml}    YYYYMM" | ccc tstepchk xall
                  elif [ "$sfx" = "dsd" ] ; then
                    #                                  daily means
                    echo "C*TSTEPCHK  ${yrmmf}01  ${yrmml}31  YYYYMMDD" | ccc tstepchk xall
                  elif [ "$sfx" = "dcosp" ] ; then
                    #                                  daily means
                    echo "C*TSTEPCHK  ${yrmmf}01  ${yrmml}31  YYYYMMDD" | ccc tstepchk xall
#                   elif [ "$sfx" = "dorb" ] ; then
#                   #                                  hourly means
#                     echo "C*TSTEPCHK${yrmmf}0100${yrmml}3121YYYYMMDDHH   24   00   01   02   03   04   05   06
# C*TSTEPCHK   07   08   09   10   11   12   13   14   15   16   17   18   19   20C5
# C*TSTEPCHK   21   22   23" | ccc tstepchk xall
#                   elif [ "$sfx" = "dthr" ] ; then
#                     #                                  3-hourly
#                     echo "C*TSTEPCHK${yrmmf}0100${yrmml}3121YYYYMMDDHH    8   00   03   06   09   12   15   18
# C*TSTEPCHK   21" | ccc tstepchk xall
                  else
                    echo "***WARNING in mergets.dk: unknown tstepchk suffix."
                  fi
                fi
		save    xall ${mts_uxxx}_${runid}_${yrf}_${yrl}_${sfxs}${v}
                release xall
                var_merged="on"
              else
#                                     resave chunks, if too big to merge
                if [ "$resave_chunks" != "off" ] ; then
                  nch=1
                  while [ $nch -le ${time_chunks} ] ; do
                    eval yrs=\${time_years$nch}
		    save x$nch ${mts_uxxx}_${runid}_${yrs}_${sfx}${v}
                    nch=`expr $nch + 1`
                  done
                  var_merged="on"
                fi
              fi # outsize
            fi # data_description
#                                      delete chunks
            if [ "$delete_time_chunks" = "on" -a "$var_merged" = "on" ] ; then
              for f in $list ; do
                delete $f
              done
            fi
          fi # missing = off
        fi # $v != file_list
      done # variable
    done # sfx

end_of_script

. endjcl.cdk
