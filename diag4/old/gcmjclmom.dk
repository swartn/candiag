#deck gcmjclmom
jobname=gcmjclmom.$model ; time=$initime ; memory=$initmem
: ; . comjcl.cdk

#  ****************************************************************************
#  ***********************  Model Initiation Job  *****************************

cat > Execute_Script <<'end_of_script'

#  Revised for "modver" support.
#                                      FM - Jun 15/99 - FM.
#  Add options to handle new optimized model version gcm6u and
#  to handle mom ocean model (mom=on).
#                  gcmjclmom           M.Lazare/D.Ramsden - Aug 23/95.
#  ----------------------------------- Script to generate necessary files and
#                                      executables to run the model as a string
#                                      of jobs. Based on early Unix version.

#  ----------------------------------- Reset compile parameters if 32-bit reals
#                                      are selected. If using an IBM RS6000,
#                                      then set compile parameters for 64 bit
#                                      reals and integers and also adjust source
#                                      code to work around problem with 4H 
#                                      Hollerith literals when 64 bit integers
#                                      are used.

if [ "$float1" = on ] ; then

  F77=$F77_float1
  LOMODEL=$LOMODEL_float1
  LOCOMM=$LOCOMM_float1

elif [ "$HOSTID" = 'orion' -o "$SITE_ID" = Victoria -o "$SITE_ID" = Downsview -o "$SITE_ID" = York ] ; then

  F77=$F77_float2
  LOMODEL=$LOMODEL_float2
  LOCOMM=$LOCOMM_float2
  if [ "$HOSTID" = 'orion' ] ; then QSRC='-listing' ; else QSRC='-qsource' ; fi

  fix77 source.f

  if [ "$ocean" = on ] ; then
    fix77 ocean.f
  fi

fi

#  ----------------------------------- Setup for fast ftp if going from the NEC
#                                      to the MIPS.




ftpdest=$morigin


if [ "$fansx" != on ] ; then

  #  --------------------------------- Create the executable for the GCM.

  if [ "$samerun" = on ] ; then

    access AB ${start}ab

  else

    #  ------------------------------- Compile source code for the model
    #                                  using CCRN specific compiler options
    #                                  and link the subroutine libraries.
                                      
    #                                  Use version of subroutine "hornel"
    #                                  optimized for the SX-3 only on the SX-3.

#   if [ "$host" = cidsc03 ] ; then
#     HORNEL='/data/c3accc2s/executables/hornel.o'
#   fi

    if [ "$prof" = on ] ; then

      if [ "$SITE_ID" = Victoria -o "$SITE_ID" = Downsview -o "$SITE_ID" = York ] ; then
        PROF='-pg'
      else
        PROF='-p'
      fi

    fi
    if [ -n "$modver" ] ; then LOMODEL_ver=`echo $LOMODEL | eval sed 's/_model/_model_${modver}/g'` ; else LOMODEL_ver="$LOMODEL" ; fi
    $F77 $QSRC $PROF -o AB source.f $HORNEL $LOMODEL_ver $LOCOMM

    #  ------------------------------- Copy the compiler listing to the front-
    #                                  end server destination, if desired.
    if [ -f source.L -a "$nolist" != on -a \
        \( "$host" = 'hiru' -o "$host" = 'yonaka' -o "$host" = 'asa' -o "$host" = 'kaze' \) ] ; then
      ftp $ftpdest <<......endftp
        cd .queue
        put source.L ${model}_f77list
......endftp
    fi
    if [ -f source.lst -a "$nolist" != on -a \( "$SITE_ID" = Victoria -o "$SITE_ID" = Downsview \
         -o "$SITE_ID" = York  \) ] ; then

     \cp source.lst $HOME/.queue/${model}_f77list

    fi

  fi

#  ----------------------------------- "else" associated with
#                                      if [ "fansx" != on ] from above.

else

  #  --------------------------------- Use performance analysis tools. Since
  #                                    the source code is required by "fan", the
  #                                    code is stored temporarily in CCRNTMP
  #                                    in this job.  It is retrieved and used
  #                                    in a subsequent job when fan is actually
  #                                    run.

  if [ "$host" != 'hiru' -a "$host" != 'asa' ] ; then
    echo "\n Current machine is: $host"
    echo "\n fan (performance analysis tool) can only be used on the SX!\n"
    exit 1
  fi
  tCCRNTMP=$CCRNTMP ; if [ "$mdest" = 'sx' -a "$SITE_ID" = Dorval ] ; then tCCRNTMP=$RUNPATH ; fi
  mv source.f $tCCRNTMP/${model}.f

#  ----------------------------------- "fi" associated with if [ "fansx" != on ]
#                                      from above.

fi

if [ "$ocean" = on ] ; then

  if [ "$samerun" != on ] ; then

    #  ------------------------------- Create executable for the ocean model.
    if [ -n "$modver" ] ; then LOMODEL_ver=`echo $LOMODEL | eval sed 's/_model/_model_${modver}/g'` ; else LOMODEL_ver="$LOMODEL" ; fi
    $F77 $QSRC ocean.f -o OB $LOMODEL_ver $LOCOMM
    if [ -f ocean.L -a "$nolist" != on -a \
          \( "$host" = 'hiru' -o "$host" = 'yonaka' -o "$host" = 'asa' -o "$host" = 'kaze' \) ] ; then
      ftp $ftpdest <<......endftp
        cd .queue
        put ocean.L ${model}_f77list_ocean
......endftp
    fi
    if [ -f ocean.lst -a "$nolist" != on -a \( "$SITE_ID" = Victoria -o "$SITE_ID" = Downsview \
         -o "$SITE_ID" = York \) ] ; then

     \cp ocean.lst $HOME/.queue/${model}_f77list_ocean

    fi

  else

    access OB ${start}ob

  fi

fi

#  ----------------------------------- Save the model executables.
#                                      In order to keep only the latest version
#                                      of a file on the CCRN file system the
#                                      script must first access the current
#                                      copy. This is because the delete utility
#                                      will only function on a file pointer 
#                                      created using the access utility. The 
#                                      current file is then saved using "save". 
#                                      This ensures that the edition number
#                                      is incremented properly. The dummy
#                                      file is then deleted.
#                                    

if [ "$samerun" != on -o "$start" != "$model" ] ; then

  if [ "$fansx" != on ] ; then
    access OLDAB ${model}ab na
    save AB ${model}ab
    delete OLDAB na
  fi

  if [ "$ocean" = on ] ; then
    access OLDOB ${model}ob na
    save OB ${model}ob
    delete OLDOB na
  fi

fi

#  ----------------------------------- Prepare initial conditions.
#                                      Note that these are if-then-else
#                                      statements and as such the first true
#                                      statement encountered will set the
#                                      value of the startup variable. This is
#                                      to protect the user who is restarting a
#                                      run from accidentally starting at the 
#                                      initial conditions due to "initsp"  
#                                      being set to "on".

if   [ "$fiz" = on ]          ; then
  startup='initsp'
elif [ "$start" != "$model" ] ; then
  startup='restart'
elif [ "$initsp" = on ]       ; then
  startup='initsp'
else
  startup='restart'
fi

# ------------------------------------ Initialization mode.

if [ "$startup" = initsp ] ; then

  # ---------------------------------- Run initialization programs.
  #                                    The start file generated is in "native
  #                                    machine format (nmf)". Also, delete
  #                                    existing crawork file used to store
  #                                    "pakjob" submission jobs if the
  #                                    pakjob option is used (described later).

  # ---------------------------------- "inphys.cdk" runs the Physics
  #                                    Initialization programs.

  : ; . $inphys.cdk

  # ---------------------------------- "inatmo.cdk" runs the Atmospheric
  #                                    Initialization programs.
  #                                    "inatmo" generates the start file
  #                                    which is in native machine format (nmf).
  : ; . $inatmo.cdk

  # ---------------------------------- "rsnmf" sets the condition to save a
  #                                    restart file in native machine format
  #                                    later on.
  rsnmf=yes
  kount=0
  rm -f $HOME/.queue/.crawork/$model.pak_string

else

  # ---------------------------------- Restart Mode.

  #  --------------------------------- Use existing AN file. Recreate
  #                                    if non-existent.

  access AN ${start}an na

  if [ ! -f AN ] ; then
    : ; . $inphys.cdk
  fi

  #  --------------------------------- Use restart file saved in "native
  #                                    machine format", if it exists.

  access ST_nmf ${start}rs_nmf na

  if [ ! -f ST_nmf ] ; then

    #  ------------------------------- Otherwise, restart file is stored in
    #                                  "portable" format. The data must be
    #                                  converted to native machine format.

    access ST ${start}rs
    unpakrs ST ST_nmf
    rsnmf=yes

  fi

  #  --------------------------------- Ensure previous restart file is
  #                                    stored in "portable" format if
  #                                    running with a new model name.

  if [ "$start" != "$model" -a ! -f ST ] ; then
    pakrs ST_nmf ST
    save ST ${start}rs
  fi

  #  --------------------------------- In order to clearly distinguish model
  #                                    output, job execution names and
  #                                    various output filenames are denoted by
  #                                    appending the "model" parmsub parameter
  #                                    followed by the current "kount" (obtained
  #                                    by reading the restart file using the 
  #                                    program "getstep").

  #                                    The "getstep" program returns the
  #                                    value of "kount" in I20 format (to
  #                                    allow for very large values), and
  #                                    "sed" is used to remove the
  #                                    leading blanks.

  getstep ST_nmf kount_file
  kount=`sed 's/ *\([^ ]*\) */\1/' kount_file`
  rm kount_file

  if [ "$start" != "$model" ] ; then

    #  --------------------------------- Get ocean restart files.

    if [ "$ocean" = on ] ; then
      access RO ${start}ro
      save   RO ${model}ro
      access IA ${start}ia
      save   IA ${model}ia
    fi
    if [ "$mom" = on ] ; then
      access oc ${start}os
      save oc ${model}os 
      access IA ${start}ia
      save   IA ${model}ia
    fi
  fi

#  ----------------------------------- "fi" associated with if [ "startup" =
#                                      initsp ] from above

fi


#
#  ----------------------------------- Save the AN file.
#

if [ "$startup" = initsp -o "$start" != "$model" ] ; then
  save AN ${model}an
fi

#  ----------------------------------- Save the native-mode model restart file.

if [ "$rsnmf" = yes ] ; then
  save ST_nmf ${model}rs_nmf
fi

#  ----------------------------------- Remove old script, if it exists. Then
#                                      save the new script used to run the
#                                      model.
#
#                                      "gcmjob" is the script created in the
#                                      Model Execution Job portion of gcmjcl.dk.
#                                      Since the Model Initiation Job
#                                      script does not run until
#                                      gcmjcl.dk reaches the endjcl.cdk line
#                                      at the end of the deck, the gcmjob
#                                      script is already created in the current
#                                      directory by the time this stage of the
#                                      deck is reached during execution.

access oldjob ${model}_script na
save gcmjob ${model}_script
delete oldjob na

#  ----------------------------------- Generate a submission job to access
#                                      and run the saved script. The variable
#                                      "kount" is different for each
#                                      re-submission of the model script and
#                                      is set to 0 for the initial startup.
 
cat > job <<endcat
#! $CCCSHELL
mkdir $CCRNTMP/tmp\$\$
cd $CCRNTMP/tmp\$\$
access gcmjob ${model}_script
set -a
kount=$kount
. ./gcmjob
endcat

#  ----------------------------------- Launch the model integration provided
#                                      that the file "haltit" does not exist
#                                      (see scripts access/save for more info).

if [ ! -f haltit ] ; then
  nqs_jobname=`echo ${model}_$kount | cut -c1-14`
  if [ "$SITE_ID" != Victoria -a  "$SITE_ID" != Downsview -a "$SITE_ID" != York ] ; then
    Tqueue=$mdest ; if [ "$mdest" = 'sx' -a "$SITE_ID" = Dorval ] ; then Tqueue=${DEFBEQ:='hiru'} ; fi
    qsub job -q $Tqueue -lT $gcmtime -lM $gcmmem -r ${nqs_jobname} \
             -eo -o /dev/null

  else

    Cwd=`pwd` 
    ln -s $Cwd/job $HOME/tmp/job.$$ 
    cd $HOME/tmp
    qsub job.$$ -q $mdest -lT $gcmtime -lM $gcmmem -r ${nqs_jobname} \
                -eo -o /dev/null
    rm job.$$
    cd $Cwd

  fi

fi

#  ----------------------------------- Obtain a local copy of the model
#                                      script if the "gcmlist" option is used.

if [ "$gcmlist" = on ] ; then
    ftp $ftpdest <<....endftp
      cd .queue
      put gcmjob ${model}_script
....endftp
fi

cat > NOSTRING <<endcat
  File used to suppress string continuation in endjcl.cdk
endcat

#  ----------------------------------- End of script gcmjcl.

end_of_script

#  ********************  end of Model Initation Job  **************************
#  ****************************************************************************

#  ****************************************************************************
#  ********************  Model Initiation Job Input Section *******************

#  ----------------------------------- Generate data files used in 
#                                      initialization.

cat > icntrl.dat <<end_of_data
icntrl    $slv$lon$lat$lrt$lmt$ktr$iday
$lay$gmt$coord$plid$moist$ihyd
$g01$g02$g03$g04$g05$g06$g07$g08$g09$g10
$g11$g12$g13$g14$g15$g16$g17$g18$g19$g20
$g21$g22$g23$g24$g25$g26$g27$g28$g29$g30
$g31$g32$g33$g34$g35$g36$g37$g38$g39$g40
$g41$g42$g43$g44$g45$g46$g47$g48$g49$g50
$h01$h02$h03$h04$h05$h06$h07$h08$h09$h10
$h11$h12$h13$h14$h15$h16$h17$h18$h19$h20
$h21$h22$h23$h24$h25$h26$h27$h28$h29$h30
$h31$h32$h33$h34$h35$h36$h37$h38$h39$h40
$h41$h42$h43$h44$h45$h46$h47$h48$h49$h50
end_of_data

if [ "$inatmo" = inatmo6 ] ; then

  cat > intrac.dat <<..end_of_data
  initrac $itrac     1000.      180.       30.       10.        0.
..end_of_data

else

  cat > intrac.dat <<..end_of_data
  intrac2 $itrac $itrvar     1.e-2      240.       45.       30.       00.
..end_of_data

fi

#  ============================================================================

#  ----------------------------------- Unset parameters not used in GCM6. This
#                                      is done so that the same input card
#                                      images can be used for all supported
#                                      versions of the GCM (this must be done
#                                      before the following "cat").

if [ "$inphys" = inphys6 ] ; then
  unset icon irad itrvar iyear
fi

#  ============================================================================

#  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#  @@@@@@@@@@@@@@@@@@@@@  Model Execution Job  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

#  ----------------------------------- Generate model script to be saved
#                                      and submitted by gcmjcl.

cat > gcmjob <<end_of_gcmjob
oldiag=$oldiag ; modver=$modver
. betapath2

morigin=$morigin ; mdest=$mdest ;
output_dir=$output_dir ; oldiag=$oldiag ; modver=$modver

model=$model ; scanobe=$scanobe ; datadest=$datadest ; datadir=$datadir
crawork=$crawork ; noprint=$noprint ; nextjob=$nextjob ; pakjob=$pakjob
lopgm=$lopgm ; acctcom=$acctcom ; debug=$debug ; xmu=$xmu ; CCRNTMP=$CCRNTMP
fiz=$fiz ; F_PROGINF=$F_PROGINF ; F_FILEINF=$F_FILEINF ; F_SETBUF=$F_SETBUF
RUNPATH=$RUNPATH ; DATAPATH=$DATAPATH 
#deck gcmjob
jobname=gcmjob ; time=$gcmtime ; memory=$gcmmem
: ; . comgcm.cdk

cat > Execute_Script1 <<'end_of_script'

#  Re-design to conform to new deck format.
#                  gcmjob              E. Chan - Jan 19/95.
#  ----------------------------------- Script to run GCM as a string of jobs.
#                                      Based on early Unix version. This
#                                      job is launched by the deck "gcmjcl".

#  ----------------------------------- Generate heading for printout.

echo "1 -------------------------------------------------------------------"
echo  "                       CCC GCM run ${model}"
echo  " -------------------------------------------------------------------"
echo "\\n            looping $loop times of ksteps=$ksteps each \\n\\n"

#  ----------------------------------- Access executables saved by "gcmjcl".

access AN ${model}an

if [ "$myrssti" = on ] ; then

#  ----------------------------------- Access multi-year boundary condition
#                                      files.

  access NUGC ${myrfile}_gc
  access NUGT ${myrfile}_gt
  access NUSIC ${myrfile}_sic
fi

#  ----------------------------------- Since the FORTRAN analyser uses source
#                                      code instead of executable files, there
#                                      is no need to obtain a copy of the
#                                      executable when the analyser option
#                                      is switched on.

if [ "$fansx" != on ] ; then
  access GCM ${model}ab
fi

if [ "$ocean" = on ] ; then
  access OCEAN ${model}ob
  access RESID $resid
fi
if [ "$mom" = on ] ; then
  access OCEAN ${mom_ab}
  access kmt ${kmt}
  access target ${target}
  access oceanin ${oceanin}
  access flux ${flux}
  access RESID $resid
fi

#  ----------------------------------- Initialize the counter for the
#                                      loop. The counter is set to 1 for "fiz"
#                                      runs and also if "loop" is undefined.

if [ "$fiz" = on ] ; then
  counter=1
else
  counter=${loop:=1} ; tksteps=\`expr \$counter * $ksteps\` ; tkfinal=\`expr \$kount + $tksteps\` ; kfinal=\`expr $kfinal + 0\`
  if [ "\$tkfinal" -eq "\$kfinal" ] ; then counter=\`expr \$counter + 1\` ; fi
fi
#  ----------------------------------- Set the version of pakgcm used later
#                                      in gcmpak.cdk.

if [ "$inphys" = inphys6 -o "$inphys" = inphys6u ] ; then
  pakgcm=pakgcmh
else
  pakgcm=pakgcm7
fi

#  ----------------------------------- Loop over number of sets of ksteps for
#                                      this job.

#                                      Note that a for-do-done loop is used
#                                      although a while-do-done loop is more
#                                      natural. This is because a limitation
#                                      in the Bourne shell prevents correct
#                                      flow control when "set -e" is used
#                                      in conjunction with while-do-done loops.
#                                      These loops tend to execute once and
#                                      only once if "set -e" has been executed.

#                                      This problem is circumvented by
#                                      using a while-do-done loop outside
#                                      the influence of "set -e" to help
#                                      set up an equivalent for-do-done loop.

set +e
while [ \$counter -gt 0 ]
do
looplist="loop \$looplist"
counter=\`expr \$counter - 1\`
done
set -e

for item in \$looplist
do

  counter=\`expr \$counter + 1\`

  #  --------------------------------- Adding zero to the string kfinal
  #                                    removes leading zero's such that it can
  #                                    be used in the following "if" block.

  kfinal=\`expr $kfinal + 0\`

  if [ "$ksteps" -lt "\$kfinal" ] ; then
    kount=\`expr \$kount + $ksteps\`
  else
    kount=\$kfinal
  fi

  #  --------------------------------- Access restart file.

  if [ "\$counter" -le 1 ] ; then

    access OLDRS ${model}rs_nmf

  else

    #  ------------------------------- Restart file was created from the
    #                                  last iteration of the loop. In
    #                                  this case, the restart file is
    #                                  simply renamed, as is the symbolic
    #                                  link generated by "save" in
    #                                  "gcmpak.cdk", if it exists.
    mv NEWRS OLDRS
    if [ "$SITE_ID" = 'Victoria' ] ; then
      if [ -f .NEWRS_Link -o -L .NEWRS_Link ] ; then mv .NEWRS_Link .OLDRS_Link ; fi
    else
      if [ -f .NEWRS_Link ] ; then mv .NEWRS_Link .OLDRS_Link ; fi
    fi
  fi

  #  --------------------------------- Access various files depending on
  #                                    the "ocean" and/or "fiz" options.

  if [ "$ocean" = on ] ; then
    access OLDIA ${model}ia na
  fi
  if [ "$mom" = on ] ; then
    if [ "$initsp" != on ] ; then
      access OLDIA ${model}ia
    fi
    if [ "$initsp" = on ] ; then
      access OLDIA ${mom_ia}
    fi
  fi 

  if [ "$fiz" = on ] ; then
    access FIZGS ${start}gs
    access FIZSS ${start}ss
  fi

  if [ "$fansx" != on ] ; then

    #  ------------------------------- Run the model the requested number of
    #                                  steps.

    GCM  < modl.dat

    if [ "$prof" = on ] ; then

      #  ----------------------------- Use the profiling utility.

      if [ "$SITE_ID" != Victoria -a "$SITE_ID" != Downsview -a "$SITE_ID" != York  ] ; then
        prof -ts GCM > prof.L
      else
        gprof > prof.L
      fi

      echo "\\n\\n"
      echo ">>>>>>>>>>>>>>> UNIX Profiling Utility - prof <<<<<<<<<<<<<<<"
      cat prof.L
      echo "\\n\\n"
      echo ">>>>>>>>>>>>>>> UNIX Profiling Utility - prof <<<<<<<<<<<<<<<"

      exit

    fi

  else
    #  ------------------------------- Run the analyzer in the current working
    #                                  directory.
    tCCRNTMP=$CCRNTMP ; if [ "$mdest" = 'sx' -a "$SITE_ID" = Dorval ] ; then tCCRNTMP=$RUNPATH ; fi
    mv \$tCCRNTMP/${model}.f source.f
    if [ "\$HOSTID" = 'asa' -o "\$HOSTID" = 'hiru' ] ; then 

      FANCMD='fanp -tm -amm -AE tcm -AL f=fan.L -b -float0 -w '





    else FANCMD='fan -t -mm -AL f=fan.L -b -O nodarg -float2 ' ; fi

    if [ "$fullfan" != on ] ; then
    if [ -n "\$modver" ] ; then LOMODEL_ver=\`echo \$LOMODEL | eval sed 's/_model/_model_\${modver}/g'\` ; else LOMODEL_ver="\$LOMODEL" ; fi
      \$FANCMD -e1 -pvctl noassume source.f \$LOMODEL_ver \$LOCOMM < modl.dat

    else

      #  ----------------------------- Collect subroutine source code in sub.f
      #                                so that a full fan may be run.
      cwd=\`pwd\`
      eval "modver=\${modver:=\$DEFMODL}" ; eval "modver=\${modver:='gcm6u'} "
      lssublib=\`find \$CCRNSRC/source/lssub/model/agcm -name \$modver -print 2>/dev/null | tail \`
      if [ -d "\$lssublib" ] ; then cd \$lssublib 
      else echo " GCMJCLMOM: Problem setting lssublib" ; echo " GCMJCLMOM: Problem setting lssublib" >> haltit ; exit 1 ; fi
      #  ----------------------------- The following redistribution of the cat
      #                                into several sub-sections is to overcome
      #                                an apparent bug in the implementation
      #                                of the cat on the NEC supercomputer,
      #                                having to do with a limitation on the
      #                                length of the Unix command referencing
      #                                the files to be concatenated.
      cat  [a-d]*.f >  \$cwd/sub.f
      cat  [e-i]*.f >> \$cwd/sub.f ; cat  [j-p]*.f >> \$cwd/sub.f
      cat  [q-s]*.f >> \$cwd/sub.f ; cat  [t-z]*.f >> \$cwd/sub.f
      if [ "\$modver" = 'gcm6u' ] ; then COMMdir="\$CCRNSRC/source/lssub/comm" ; else COMMdir="\$CCRNSRC/source/lssub_comm_fullfan" ; fi
      if [ -d "\$COMMdir" ] ; then cd \$COMMdir ; cat [a-i]*.f >> \$cwd/sub.f ; cat [j-z]*.f >> \$cwd/sub.f ; fi ; cd \$cwd
      if [ "\$HOSTID" != 'asa' -a "\$HOSTID" != 'hiru' ] ; then  
       if [ -n "\$modver" ] ; then LOMODEL_ver=\`echo \$LOMODEL | eval sed 's/_model/_model_\${modver}/g'\` ; else LOMODEL_ver="\$LOMODEL" ; fi
         FANLIB=" \$LOMODEL_ver \$LOCOMM \$LINKPLT \$LINKNCAR " ; fi
      \$FANCMD -e1 -pvctl noassume source.f sub.f \$FANLIB < modl.dat
    fi

    echo "\\n\\n"
    echo ">>>>>>>>>>>>>>>> F O R T R A N   A N A L Y Z E R <<<<<<<<<<<<<<<<<"
    echo "\\n\\n"
    cat fan.L
    echo "\\n\\n"
    echo ">>>>>>>>>>>>>>>> F O R T R A N   A N A L Y Z E R <<<<<<<<<<<<<<<<<"
    echo "\\n\\n"
    exit
  fi

  #  --------------------------------- If have reached here, model has exited
  #                                    normally and the results may be saved
  #                                    using the gcmpak.cdk common deck.

  : ; . gcmpak.cdk

  if [ "$ocean" = on ] ; then

    #  ------------------------------- Run the ocean model.

    access OLDRO ${model}ro na

    OCEAN RESID OLDRO NEWRO OS IA NEWIA < omodl.dat

    save NEWRO ${model}ro
    delete OLDRO na
    access OLDOS ${model}os na
    joinup NEWOS OLDOS OS
    save NEWOS ${model}os
    delete OLDOS na
    save NEWIA ${model}ia
    delete OLDIA na
    rm -f NEWRO NEWOS NEWIA OS IA OLDIA
    rm -f .NEWRO_Link .NEWOS_Link .NEWIA_Link .OS_Link

  fi
  if [ "$mom" = on ] ; then
    access oldav ${model}oc na
    if [ "$fluxsav" = on ] ; then
      access oldfl ${model}fl na
    fi
    if [ "$initsp" = on ] ; then
      access ocstart ${mom_os}
    fi
    if [ "$initsp" != on ] ; then
      access ocstart ${model}os
    fi
    cp ocstart tempos
    chmod +w tempos
    lakes RESID IA ia2 input=omodl.dat
    fluxadj OLDIA ia2 flux target ia2oc fl
    OCEAN tempos xxx ia2oc oc2ia ocdata oceanin oceanout
    comboa ia2 oc2ia kmt flux NEWIA ta
    avocean oldav ocdata newav
    save tempos ${model}os
    save newav ${model}oc
    delete oldav na
    if [ "$fluxsav" = on ] ; then
      joinup newfl oldfl fl
      save newfl ${model}fl
      delete oldfl na
      rm -f oldfl newfl
    fi
    save NEWIA ${model}ia
    if [ "$initsp" != on ] ; then
      delete ocstart
      delete OLDIA na
    fi
    rm -f oldav tempos ocstart ocdata
    rm -f NEWIA ia2 ia2oc oc2ia newav
  fi

  #  --------------------------------- End of loop.

done

#  ----------------------------------- End the job if the "fiz" option is used.

if [ "$fiz" = on ] ; then
  if [ -n "$datadest" ] ; then . gcmftp.cdk ; fi ; exit
else

rm -f OCEAN GCM AN RESID

#  ----------------------------------- Generate a submission job to access
#                                      and run the model script (saved by
#                                      "gcmjcl") using the new "kount".

cat > job <<endcat
#! \$CCCSHELL
mkdir $CCRNTMP/tmp\\\$\\\$
cd $CCRNTMP/tmp\\\$\\\$
access gcmjob ${model}_script
set -a
kount=\$kount
. ./gcmjob
endcat
#  ----------------------------------- Submit job to continue the model
#                                      integration provided that the file
#                                      "haltit" does not exist (see the
#                                      scripts access/save for more info).
nqs_jobname=\`echo ${model}_\$kount | cut -c1-14\`
if [ ! -f haltit ] ; then
  Tqueue=$mdest ; if [ "$mdest" = 'sx' -a "$SITE_ID" = Dorval ] ; then Tqueue=${DEFBEQ:='hiru'} ; fi
  qsub job -r \${nqs_jobname} -q \$Tqueue -lT $gcmtime -lM $gcmmem \\
           -eo -o /dev/null
fi

cat > NOSTRING <<endcat
  File used to suppress string continuation in endgcm.cdk
endcat
fi # Closes 'if [ "$fiz" = on ] ; then ... else ...'
#  ----------------------------------- End of first part of gcmjob.

end_of_script


cat > Execute_Script2 <<'end_of_script'
if [ "$fiz" != on ] ; then
#
#  ----------------------------------- Second part of gcmjob. This script
#                                      executes only if the first part exits
#                                      abnormally (controlled in endgcm.cdk).

#  ----------------------------------- Determine whether or not the model
#                                      has run successfully. Get variable
#                                      g0 by reading it from the file
#                                      "Switches" generated by the GCM when
#                                      it has completed successfully.

get_switch g0

if [ -n "\$g0" -a "\$g0" -eq 0  ] ; then

  #  .........................................................................
  echo '\\n\\n           === GCM run completed successfully === \\n\\n'
  #  .........................................................................

  #  --------------------------------- Convert the restart file to pure
  #                                    IEEE (portable) format.

  pakrs OLDRS RESTART
  save RESTART ${model}rs
  delete OLDRS

  #  --------------------------------- Automatically transfer model output
  #                                    files to another machine, if desired.

  if [ -n "$datadest" ] ; then
    : ; . gcmftp.cdk
  fi

else

  #  .........................................................................
  echo '\\n\\n           === GCM run aborted abnormally === \\n\\n'
  #  .........................................................................

  cat > NOSTRING <<'  endcat'
    File used to suppress string continuation in endgcm.cdk
  endcat
  access EN ${model}en na
fi
fi # Closes 'if [ "$fiz" != on ] ; then ...'
#  ----------------------------------- Run egrafs.


if [ -f EN ] ; then egrafs EN input=egrafs.dat ; fi

#  ----------------------------------- End of second part of gcmjob.

#  @@@@@@@@@@@@@@@@@@@@  end of Model Execution Job  @@@@@@@@@@@@@@@@@@@@@@@@@@
#  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

end_of_script


#  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#  @@@@@@@@@@@@@@@@@@@@  Model Execution Job Input Section @@@@@@@@@@@@@@@@@@@

#  ----------------------------------- Generate data files used in gcmjob.

cat > modl.dat <<endcat
  gcm     $ksteps$kfinal    0$levs$incd
          $issp$isgg$israd$isen$iepr$icsw$iclw$isbeg$jlatpr
          $iocean$icemod$ifixcld$ihyd$icon$irad$itrac$itrvar
    $delt$ifdiff$coord$moist$iyear
endcat

if [ "$ocean" = on -o "$mom" = on ] ; then
  cat > omodl.dat <<..endcat
    ocean $iday$ksteps$delt$isoc$icemod$ires
..endcat
fi

cat > egrafs.dat <<endcat
  egrafs           0 999999999
    6    1    1
    1   -1    1
endcat

#  @@@@@@@@@@@@@@  end of Model Execution Job Input Section @@@@@@@@@@@@@@@@@@@
#  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


: ; . endgcm.cdk

end_of_gcmjob

#  ----------------------------------- The "nogcmsub" option allows a user to
#                                      submit a model job without running the
#                                      gcmsub deck which requires "update". In
#                                      this case, user must do the job of the
#                                      gcmsub deck. The model submission job
#                                      must then resemble that of a standard
#                                      diagnostic submission job with the
#                                      following exceptions:
#
#                                      1) The parmsub and condef sections must
#                                         those appropriate for "gcmjcl" deck.
#                                         The "nogcmsub" option must be used.
#                                      2) This deck must be called (i.e. via
#                                         ". gcmjcl.dk").
#                                      3) The code contained within the
#                                         "nogcmsub" block below must be
#                                         inserted at the end of the job.
#                                      4) The user must collect and modify the
#                                         required source code using whatever
#                                         means desired.
#                                      5) The source code must be inserted
#                                         manually within the "cat" here
#                                         document.

if [ "$nogcmsub" != on ] ; then

#  ************************  Source Code Section  *****************************

cat > source.f <<'end_of_source'

#  ----------------------------------- Insert source code.

end_of_source

#  ********************** end of Source Code Section  *************************

#  **************  end of Model Initiation Job Input Section ******************
#  ****************************************************************************

: ; . endjcl.cdk

fi
