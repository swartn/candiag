#deck gpstats
jobname=gpstats ; time=$stime ; memory=$memory1
. comjcl.cdk

cat > Execute_Script <<'end_of_script'
#  Add rcm switch, remove ccc.
#                  gpstats             sk - Apr 24/07 - ec,sk ---------gpstats
#   ---------------------------------- calculate q,t,z,vort,u,v and w statistics.
#                                      Merger of ustats.dk, vstats.dk, wstats.dk, 
#                                      qstats.dk, tstats.dk, zstats.dk, vortsta.dk,
#                                      and zonstat.dk
#


#   ---------------------------------- get beta for zonal averaging
      if [ "$rcm" != "on" ] ; then
      access gpfile ${flabel}gp
      xfind  gpfile beta
      fi

#   ---------------------------------- first do calculations for u and v
      access u ${flabel}_gpu
      access v ${flabel}_gpv
#                                      calculate time derivatives
      dif u dudt
      dif v dvdt
#                                      calculate mean, deviations and variance
      timavg u tu up tup2
      timavg v tv vp tvp2
      rm u v
#                                      calculate covariances
      timcov up vp tupvp
#                                      xsave gridded statistics for u and v
      xsave new_gp tu dudt tup2 tv dvdt tvp2 tupvp new.
      mv new. new_gp

#   ---------------------------------- perform calculations involving w
      if [ "$wxstats" = "on" ]; then
        access w ${flabel}_gpw
        dif w dwdt
        timavg w tw wp twp2
        rm w
        timcov up wp tupwp
        timcov vp wp tvpwp
#                                      xsave gridded statistics for w
        xsave new_gp tw dwdt twp2 tupwp tvpwp new.
        mv new. new_gp
      fi

#   ---------------------------------- calculate representative zonal averages
#                                      (u)r, (u*u*)r, (v)r, (v*v*)r, (u*v*)r
      if [ "$rcm" != "on" ] ; then
      rzonavg tu    beta rztu  tus rztus2
      rzonavg tv    beta rztv  tvs rztvs2
      mlt     tus   tvs  tusvs
      rzonavg tusvs beta rztusvs
#                                      (du/dt)r, (dv/dt)r
      rzonavg dudt  beta rzdudt
      rzonavg dvdt  beta rzdvdt
#                                      (u"u")r, (v"v")r, (u"v")r
      rzonavg tup2  beta rztup2
      rzonavg tvp2  beta rztvp2
      rzonavg tupvp beta rztupvp
#                                      xsave zonal statistics for u and v
      xsave new_xp rztu rzdudt rztup2 rztus2 \
                   rztv rzdvdt rztvp2 rztvs2 \
                   rztupvp rztusvs new.
      mv new. new_xp

#   ---------------------------------- perform similar calculations for w
      if [ "$wxstats" = "on" ] ; then
#                                      (dw/dt)r
        rzonavg dwdt  beta rzdwdt
#                                      (w)r, (w*w*)r, (u*w*)r and (v*w*)r
        rzonavg tw    beta rztw  tws rztws2
        mlt     tus   tws  tusws
        mlt     tvs   tws  tvsws
        rzonavg tusws beta rztusws
        rzonavg tvsws beta rztvsws
#                                      (w"w")r, (u"w")r and (v"w")r
        rzonavg twp2  beta rztwp2
        rzonavg tupwp beta rztupwp
        rzonavg tvpwp beta rztvpwp
#                                      xsave zonal statistics for w
        xsave new_xp rztw rzdwdt rztwp2 rztws2 \
                     rztupwp rztusws rztvpwp rztvsws new.
        mv new. new_xp
      fi
      fi

#   ---------------------------------- do calculations for q, t, z and vr
      for x in q t z vr ; do
        access x ${flabel}_gp$x
#                                      calculate time derivatives
        dif x dxdt
#                                      calculate mean, deviations and variance
        timavg x tx xp txp2
        rm x
#                                      calculate covariances
        timcov xp up txpup
        timcov xp vp txpvp
#                                      xsave gridded statistics
        xsave new_gp tx dxdt txp2 txpup txpvp new.
        mv new. new_gp

        if [ "$rcm" != "on" ] ; then
#                                      zonal averages
#                                      (x)r, (dx/dt)r, (x*x*)r and (x*v*)r
        rzonavg tx    beta rztx  txs rztxs2
        mlt     txs   tvs  txsvs
        rzonavg txsvs beta rztxsvs
#                                      (dx/dt)r
        rzonavg dxdt  beta rzdxdt
#                                      (x"x")r and (x"v")r
        rzonavg txp2  beta rztxp2
        rzonavg txpvp beta rztxpvp
#                                      xsave zonal statistics
        xsave new_xp rztx rzdxdt rztxp2 rztxs2 rztxpvp rztxsvs new.
        mv new. new_xp
        fi

#   ---------------------------------- perform calculations involving w
        if [ "$wxstats" = "on" ]; then
          timcov xp wp txpwp
#                                      xsave gridded statistics
          xsave new_gp  txpwp new.
          mv new. new_gp
          if [ "$rcm" != "on" ] ; then
#                                      (x"w")r and (x*w*)r
          rzonavg txpwp beta rztxpwp
          mlt     txs   tws  txsws
          rzonavg txsws beta rztxsws
#                                      xsave zonal statistics
          xsave new_xp rztxpwp rztxsws new.
          mv new. new_xp
          fi
        fi
      done

#   ---------------------------------- save results.
      access oldgp ${flabel}gp
      xjoin oldgp new_gp newgp
      save newgp ${flabel}gp
      delete oldgp

      if [ "$rcm" != "on" ] ; then
      access oldxp ${flabel}xp
      xjoin oldxp new_xp newxp
      save newxp ${flabel}xp
      delete oldxp
      fi

end_of_script

cat > Input_Cards <<end_of_data
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
         DAYS      = $days
         RUN       = $run
         FLABEL    = $flabel
         MODEL1    = $model1
         T1        = $t1
         T2        = $t2
         T3        =      $t3
         DELT      = $delt
0 GPSTATS -------------------------------------------------------------- GPSTATS
if [ "$rcm" != "on" ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        $d
fi
if [ "`echo $datatype | tail -c4`" = "sig" ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
DIFF          U $t1 $t2 $delt
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
DIFF          V $t1 $t2 $delt
else
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
DIFF          U $t1 $t2 $delt       1.0
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
DIFF          V $t1 $t2 $delt       1.0
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE.        U
NEWNAM.
XSAVE.        DU/DT
NEWNAM.      U.
XSAVE.        U"U"
NEWNAM.    U"U"
XSAVE.        V
NEWNAM.
XSAVE.        DV/DT
NEWNAM.      V.
XSAVE.        V"V"
NEWNAM.    V"V"
XSAVE.        U"V"
NEWNAM.    U"V"
if [ "$wxstats" = "on" ]; then
if [ "`echo $datatype | tail -c4`" = "sig" ] ; then
if [ "$rcm" != "on" ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
DIFF       OMEG $t1 $t2 $delt
else
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
DIFF          W $t1 $t2 $delt 
fi
else
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
DIFF       OMEG $t1 $t2 $delt       1.0
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE.        W
NEWNAM.
XSAVE.        DW/DT
NEWNAM.      W.
XSAVE.        W"W"
NEWNAM.    W"W"
XSAVE.        U"W"
NEWNAM.    U"W"
XSAVE.        V"W"
NEWNAM.    V"W"
fi
if [ "$rcm" != "on" ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE.        (U)R
NEWNAM.       U
XSAVE.        (DU/DT)R
NEWNAM.      U.
XSAVE.        (U"U")R
NEWNAM.    U"U"
XSAVE.        (U*U*)R
NEWNAM.    U*U*
XSAVE.        (V)R
NEWNAM.       V
XSAVE.        (DV/DT)R
NEWNAM.      V.
XSAVE.        (V"V")R
NEWNAM.    V"V"
XSAVE.        (V*V*)R
NEWNAM.    V*V*
XSAVE.        (U"V")R
NEWNAM.    U"V"
XSAVE.        (U*V*)R
NEWNAM.    U*V*
if [ "$wxstats" = "on" ]; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE.        (W)R
NEWNAM.       W
XSAVE.        (DW/DT)R
NEWNAM.      W.
XSAVE.        (W"W")R
NEWNAM.    W"W"
XSAVE.        (W*W*)R
NEWNAM.    W*W*
XSAVE.        (U"W")R
NEWNAM.    U"W"
XSAVE.        (U*W*)R
NEWNAM.    U*W*
XSAVE.        (V"W")R
NEWNAM.    V"W"
XSAVE.        (V*W*)R
NEWNAM.    V*W*
fi
fi
if [ "`echo $datatype | tail -c4`" = "sig" ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
DIFF       SHUM $t1 $t2 $delt
else
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
DIFF       SHUM $t1 $t2 $delt       1.0
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE.        Q
NEWNAM.
XSAVE.        DQ/DT
NEWNAM.      Q.
XSAVE.        Q"Q"
NEWNAM.    Q"Q"
XSAVE.        Q"U"
NEWNAM.    Q"U"
XSAVE.        Q"V"
NEWNAM.    Q"V"
if [ "$rcm" != "on" ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE.        (Q)R
NEWNAM.       Q
XSAVE.        (DQ/DT)R
NEWNAM.      Q.
XSAVE.        (Q"Q")R
NEWNAM.    Q"Q"
XSAVE.        (Q*Q*)R
NEWNAM.    Q*Q*
XSAVE.        (Q"V")R
NEWNAM.    Q"V"
XSAVE.        (Q*V*)R
NEWNAM.    Q*V*
fi
if [ "$wxstats" = "on" ]; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE.        Q"W"
NEWNAM.    Q"W"
if [ "$rcm" != "on" ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE.        (Q"W")R
NEWNAM.    Q"W"
XSAVE.        (Q*W*)R
NEWNAM.    Q*W*
fi
fi
if [ "`echo $datatype | tail -c4`" = "sig" ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
DIFF       TEMP $t1 $t2 $delt
else
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
DIFF       TEMP $t1 $t2 $delt       1.0
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE.        T
NEWNAM.
XSAVE.        DT/DT
NEWNAM.      T.
XSAVE.        T"T"
NEWNAM.    T"T"
XSAVE.        T"U"
NEWNAM.    T"U"
XSAVE.        T"V"
NEWNAM.    T"V"
if [ "$rcm" != "on" ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE.        (T)R
NEWNAM.       T
XSAVE.        (DT/DT)R
NEWNAM.      T.
XSAVE.        (T"T")R
NEWNAM.    T"T"
XSAVE.        (T*T*)R
NEWNAM.    T*T*
XSAVE.        (T"V")R
NEWNAM.    T"V"
XSAVE.        (T*V*)R
NEWNAM.    T*V*
fi
if [ "$wxstats" = "on" ]; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE.        T"W"
NEWNAM.    T"W"
if [ "$rcm" != "on" ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE.        (T"W")R
NEWNAM.    T"W"
XSAVE.        (T*W*)R
NEWNAM.    T*W*
fi
fi
if [ "`echo $datatype | tail -c4`" = "sig" ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
DIFF        PHI $t1 $t2 $delt
else
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
DIFF        PHI $t1 $t2 $delt       1.0
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE.        GZ
NEWNAM.
XSAVE.        DGZ/DT
NEWNAM.     GZ.
XSAVE.        GZ"GZ"
NEWNAM.    Z"Z"
XSAVE.        GZ"U"
NEWNAM.    Z"U"
XSAVE.        GZ"V"
NEWNAM.    Z"V"
if [ "$rcm" != "on" ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE.        (GZ)R
NEWNAM.      GZ
XSAVE.        (DGZ/DT)R
NEWNAM.     GZ.
XSAVE.        (GZ"GZ")R
NEWNAM.    Z"Z"
XSAVE.        (GZ*GZ*)R
NEWNAM.    Z*Z*
XSAVE.        (GZ"V")R
NEWNAM.    Z"V"
XSAVE.        (GZ*V*)R
NEWNAM.    Z*V*
fi
if [ "$wxstats" = "on" ]; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE.        GZ"W"
NEWNAM.    Z"W"
if [ "$rcm" != "on" ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE.        (GZ"W")R
NEWNAM.    Z"W"
XSAVE.        (GZ*W*)R
NEWNAM.    Z*W*
fi
fi
if [ "`echo $datatype | tail -c4`" = "sig" ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
DIFF       VORT $t1 $t2 $delt
else
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
DIFF       VORT $t1 $t2 $delt       1.0
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE.        VORT
NEWNAM.
XSAVE.        DVORT/DT
NEWNAM.      P.
XSAVE.        VORT"VORT"
NEWNAM.    P"P"
XSAVE.        VORT"U"
NEWNAM.    P"U"
XSAVE.        VORT"V"
NEWNAM.    P"V"
if [ "$rcm" != "on" ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE.        (VORT)R
NEWNAM.    VORT
XSAVE.        (DVORT/DT)R
NEWNAM.      P.
XSAVE.        (VORT"VORT")R
NEWNAM.    P"P"
XSAVE.        (VORT*VORT*)R
NEWNAM.    P*P*
XSAVE.        (VORT"V")R
NEWNAM.    P"V"
XSAVE.        (VORT*V*)R
NEWNAM.    P*V*
fi
if [ "$wxstats" = "on" ]; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE.        VORT"W"
NEWNAM.    P"W"
if [ "$rcm" != "on" ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE.        (VORT"W")R
NEWNAM.    P"W"
XSAVE.        (VORT*W*)R
NEWNAM.    P*W*
fi
fi
end_of_data

. endjcl.cdk
