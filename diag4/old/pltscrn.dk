#deck pltscrn
jobname=pltscrn ; time=$stime ; memory=$memory1
. comjcl.cdk

cat > Execute_Script <<end_of_script

#   add plots of temporal std deviations of selected variables.
#                   pltscrn             ml. jun 27/90 - ec.
#   ----------------------------------- stevensen screen plots.
#
    libncar plunit=$plunit 
.   plotid.cdk
    access ggfile ${flabel}gp
# 
    xfind ggfile tst 
    xfind ggfile tstmx 
    xfind ggfile tstmn 
    xfind ggfile tsq1 
    xfind ggfile tsu 
    xfind ggfile tsv 
    xfind ggfile tswmx 
#                                       convert specific humidity to g/kg.
    xlin tsq1 tsq 
    rm tsq1
# 
#   ----------------------------------- ggplots.
    ggplot tst 
    ggplot tstmx 
    ggplot tstmn 
    ggplot tsq 
    ggplot tswmx tsu tsv 
# 
#   ----------------------------------- zonally average the fields.
    zonavg tst ztst 
    zonavg tstmx ztstmx 
    zonavg tstmn ztstmn 
    zonavg tsq ztsq 
    zonavg tsu ztsu 
    zonavg tsv ztsv 
    zonavg tswmx ztswmx 
    rm tst tstmx tstmn tsq tsu tsv tswmx
# 
#   ----------------------------------- now xplots.
    xplot ztst 
    xplot ztstmx 
    xplot ztstmn 
    xplot ztsq 
    xplot ztsu 
    xplot ztsv 
    xplot ztswmx 
    rm ztst ztstmx ztstmn ztsq ztsu ztsv ztswmx
# 
#   ----------------------------------- additional plots of selected temporal
#                                       variances and associated zonal means.
    xfind ggfile tstp2 
    xfind ggfile tqp2 
    xfind ggfile tsup2 
    xfind ggfile tsvp2 
#                                       convert specific humidity to g/kg.
    xlin tqp2 tsqp2 
    rm ggfile tqp2
# 
    sqroot tstp2 stdst 
    sqroot tsqp2 stdsq 
    sqroot tsup2 stdsu 
    sqroot tsvp2 stdsv 
# 
    ggplot stdst 
    ggplot stdsq 
    ggplot stdsu 
    ggplot stdsv 
    rm stdst stdsq stdsu stdsv
# 
    zonavg tstp2 ztstp2 
    zonavg tsqp2 ztsqp2 
    zonavg tsup2 ztsup2 
    zonavg tsvp2 ztsvp2 
    sqroot ztstp2 stdzst 
    sqroot ztsqp2 stdzsq 
    sqroot ztsup2 stdzsu 
    sqroot ztsvp2 stdzsv 
    rm tstp2 tsqp2 tsup2 tsvp2 ztstp2 ztsqp2 ztsup2 ztsvp2
# 
    xplot stdzst 
    xplot stdzsq 
    xplot stdzsu 
    xplot stdzsv 
    rm stdzst stdzsq stdzsu stdzsv
# 
.   plotid.cdk
.   plot.cdk

end_of_script

cat > Input_Cards <<end_of_data

+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
         DAYS      = $days 
         RUN       = $run
         FLABEL    = $flabel 
0 PLTSCRN -------------------------------------------------------------- PLTSCRN
. headfram.cdk
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND         ST
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND         STMX
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND         STMN
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND         SQ
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND         SU
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND         SV
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND         SWMX
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN.          1.0E3
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
 GGPLOT.           0 NEXT    1    0        1.     -100.       50.        5.0${b}8
  RUN $run.  DAYS $days.   SCREEN TEMPERATURE.  UNITS  DEG C. 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
 GGPLOT.           0 NEXT    1    0        1.     -100.       50.        5.0${b}8
  RUN $run.  DAYS $days.   MAXIMUM SCREEN TEMPERATURE.  UNITS  DEG C. 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
 GGPLOT.           0 NEXT    1    0        1.     -100.       50.        5.0${b}8
  RUN $run.  DAYS $days.   MINIMUM SCREEN TEMPERATURE.  UNITS  DEG C. 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
 GGPLOT.           0 NEXT    1    0        1.        0.       50.        5.0${b}8
  RUN $run.  DAYS $days.   SCREEN SPECIFIC HUMIDITY.  UNITS  DEG G/KG.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
 GGPLOT.          -1 NEXT   -1    0        1.        0.       50.        5.2${b}8
RUN $run. DAYS $days. AMPLITUDE OF MAXIMUM SCREEN WIND.  UNITS M/SEC. 
                  1.        0.       10.$ncx$ncy
                      VECPLOT OF SCREEN (U,V).                     UNITS M/SEC. 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XPLOT   ST         0 NEXT    1     -100.       50.    1     -100. 
   RUN $run. DAYS $days.   SCREEN TEMPERATURE.  UNITS DEG. C. 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XPLOT STMX         0 NEXT    1     -100.       50.    1     -100. 
   RUN $run. DAYS $days.   MAXIMUM SCREEN TEMPERATURE.  UNITS DEG. C. 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XPLOT STMN         0 NEXT    1     -100.       50.    1     -100. 
   RUN $run. DAYS $days.   MINIMUM SCREEN TEMPERATURE.  UNITS DEG. C. 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XPLOT   SQ         0 NEXT    1        0.       30.    1        0. 
   RUN $run. DAYS $days.   SCREEN SPECIFIC HUMIDITY.  UNITS G/KG. 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XPLOT   SU         0 NEXT    1      -25.       25.    1      -25. 
   RUN $run. DAYS $days.   SCREEN ZONAL WIND.  UNITS M/SEC. 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XPLOT   SV         0 NEXT    1      -25.       25.    1      -25. 
   RUN $run. DAYS $days.   SCREEN MERIDIONAL WIND.  UNITS M/SEC.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XPLOT SWMX         0 NEXT    1        0.       25.    1        0. 
   RUN $run. DAYS $days.   SCREEN MAXIMUM WIND.  UNITS M/SEC. 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND         ST"2
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND         SQ"2
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND         SU"2
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND         SV"2
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN.          1.0E6
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
 GGPLOT.           0 NEXT    1    0        1.        0.       50.        2.0${b}8
  RUN $run.  DAYS $days.   SCREEN TEMPERATURE STD. DEV.  UNITS  DEG C.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
 GGPLOT.           0 NEXT    1    0        1.        0.       50.        2.0${b}8
  RUN $run.  DAYS $days.   SCREEN SPECIFIC HUMIDITY STD. DEV.  UNITS DEG G/KG.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
 GGPLOT.           0 NEXT    1    0        1.        0.       50.        2.0${b}8
  RUN $run.  DAYS $days.   SCREEN ZONAL WIND STD. DEV.  UNITS M/SEC.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
 GGPLOT.           0 NEXT    1    0        1.        0.       50.        2.0${b}8
  RUN $run.  DAYS $days.   SCREEN MERIDIONAL WIND STD. DEV.  UNITS M/SEC. 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XPLOT ST"2         0 NEXT    1        0.       50.    1        0. 
   RUN $run. DAYS $days.   SCREEN TEMPERATURE STD. DEV.  UNITS DEG. C.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XPLOT SQ"2         0 NEXT    1        0.        5.    1        0. 
   RUN $run. DAYS $days.   SCREEN SPECIFIC HUMIDITY STD. DEV.  UNITS G/KG.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XPLOT SU"2         0 NEXT    1        0.       20.    1        0. 
   RUN $run. DAYS $days.   SCREEN ZONAL WIND STD. DEV.  UNITS M/SEC.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XPLOT SV"2         0 NEXT    1        0.       20.    1        0. 
   RUN $run. DAYS $days.   SCREEN MERIDIONAL WIND STD. DEV.  UNITS M/SEC. 
. tailfram.cdk

end_of_data

. endjcl.cdk


