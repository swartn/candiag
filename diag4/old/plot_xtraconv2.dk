Adeck plot_xtraconv2
jobname=plot_xtraconv2; time=$stime ; memory=$memory1
. comjcl.cdk

cat > Execute_Script <<end_of_script
#
#
#                plot_xtraconv2        jcl - Feb 12/15 ---------- plot_xtraconv2
# ------------------------------------ Plots of diagnostic output
#                                      from xtraconv2 diagnostic
#                                      deck.  Model only since 
#                                      observations not available.
#                                      Since it is applicable include
#                                      precipitation as well.
# ---------------------------------------------------------------
#
    libncar plunit=$plunit 
.   plotid.cdk

# Information about fields
# TBCD  -> Unadjusted cloud base pressure deep convection
# TBCS  -> Unadjusted cloud base pressure shallow convection
# TCAPE -> Convective available potential energy
# TCDCB -> Deep convection indicator
# TCINH -> Convective inhibition
# TCSCB -> Shallow convection indicator
# TDMC  -> Deep convection net mass flux
# TDMCD -> Deep convection downward mass flux
# TDMCU -> Deep convection upward mass flux
# TSMC  -> Shallow convection net mass flux
# TTCD  -> Unadjusted cloud top pressure deep convection
# TTCS  -> Unadjusted cloud top pressure shallow convection
# PCPC  -> Deep convection precipitation
#
#  ---------------------------------------------- Notes
#
# The cloud bottom and cloud top pressures (TBCD, TBCS, TTCD, TTCS) are 
# special since they are effectively weighted by cloud occurrence
# (1 if convection, 0 if no # convection). Therefore their time 
# averages need to be divided by the time averages of the weights 
# (TCDCB and TCSCB) to compute the proper weighted time means. One 
# needs to do the following with the variables saved to the diagnostic 
# file:
# 
# TBCD = TBCD/TCDCB
# TBCS = TBCS/TCSCB
# TTCD = TTCD/TCDCB
# TTCS = TTCS/TCSCB
#
# before interpreting and plotting the mean cloud top and cloud base 
# pressures.

# 
#   ---------------------------------------- access the model output
# 
    access filex ${model1}gp

# -------------- Plot the 2D fields first

# ------ Grab all of the 2D fields in one go

echo "XFIND.        TBCD
XFIND.        TBCS
XFIND.        TCAPE
XFIND.        TCDCB
XFIND.        TCINH
XFIND.        TCSCB
XFIND.        TTCD
XFIND.        TTCS
XFIND.        PCP
XFIND.        PCPC" > .ic_xfind

    xfind filex TBCD TBCS TCAPE TCDCB TCINH TCSCB TTCD TTCS\
                PCP PCPC input=.ic_xfind

# ---------------------------------------------------  Plot occurrence

echo "GGPLOT             0 NEXT    14   0        1.        0.        1.       30.0${b}8" > .input_card
echo "$run $days DEEP CONV OCCURENCE ()" >> .input_card
echo "          0  10  122  120  118  114  108  106  105" >> .input_card
echo "                 104  103  102" >> .input_card
echo "                  .1        .2        .3        .4        .5        .6        .7" >> .input_card
echo "                  .8        .9       1.0" >> .input_card
    ggplot TCDCB input=.input_card

echo "GGPLOT             0 NEXT    14   0        1.        0.        1.       30.0${b}8" > .input_card
echo "$run $days SHALLOW CONV OCCURENCE ()" >> .input_card
echo "          0  10  122  120  118  114  108  106  105" >> .input_card
echo "                 104  103  102" >> .input_card
echo "                  .1        .2        .3        .4        .5        .6        .7" >> .input_card
echo "                  .8        .9       1.0" >> .input_card
    ggplot TCSCB input=.input_card

#echo "XMPLOT             0 NEXT    1        0.        1.    1    2" > .input_card
#echo "$run $days MODEL,ISCCP TOT CLD FRAC ()" >> .input_card
#    joinup PLTS zDICCF zOCFTOT
#    xmplot PLTS input=.input_card
#
#    rm PLTS CMASK MASK_SUNL

# ---------------------------------------------------------------- Plot CAPE, CIN    

echo "GGPLOT             0 NEXT    14   0        1.        0.        1.       30.0${b}8" > .input_card
echo "$run $days CAPE (J/kg)" >> .input_card
echo "          0  12  122  120  118  114  109  108  107" >> .input_card
echo "                 106 105 104  103  102" >> .input_card
echo "                50.0     100.0     150.0     200.0     250.0     300.0     350.0" >> .input_card
echo "               400.0     450.0     500.0     550.0     600.0" >> .input_card
    ggplot TCAPE input=.input_card

echo "GGPLOT             0 NEXT    14   0        1.        0.        1.       30.0${b}8" > .input_card
echo "$run $days CIN (J/kg)" >> .input_card
echo "          0  12  122  120  118  114  109  108  107" >> .input_card
echo "                 106 105 104  103  102" >> .input_card
echo "              -550.0    -500.0    -450.0    -400.0    -350.0    -300.0    -250.0" >> .input_card
echo "              -200.0    -150.0    -100.0     -50.0       0.0" >> .input_card
    ggplot TCINH input=.input_card

# -------------------------------------------- Plot bottom and top pressure convection.  
#                                              Mask as necessary.

# -------------- Deep Convection

# ------ Bottom

     div TBCD TCDCB DTBCD

# ------ Convert from Pa to hPa     

echo "XLIN          1.0E-2     0.0E0" > .input_card_xlin
     ccc xlin DTBCD X input=.input_card_xlin
     mv X DTBCD

     echo ' FMASK            -1   -1   GT        0.' > .fmask_input_card     

     fmskplt DTBCD MDTBCD TCDCB  input=.fmask_input_card

echo "GGPLOT             0 NEXT    14   0        1.        0.        1.       30.0${b}8" > .input_card
echo "$run $days DEEP CONV BASE (hPa)" >> .input_card
echo "          0  12  122  120  118  114  109  108  107" >> .input_card
echo "                 106 105 104  103  102" >> .input_card
echo "               450.0     500.0     550.0     600.0     650.0     700.0     750.0" >> .input_card
echo "               800.0     850.0     900.0     950.0    1000.0" >> .input_card
    ggplot MDTBCD input=.input_card

# ------ Top

     div TTCD TCDCB DTTCD

# ------ Convert from Pa to hPa     

echo "XLIN          1.0E-2     0.0E0" > .input_card_xlin
     ccc xlin DTTCD X input=.input_card_xlin
     mv X DTTCD

     echo ' FMASK            -1   -1   GT        0.' > .fmask_input_card     

     fmskplt DTTCD MDTTCD TCDCB  input=.fmask_input_card

echo "GGPLOT             0 NEXT    14   0        1.        0.        1.       30.0${b}8" > .input_card
echo "$run $days DEEP CONV TOP (hPa)" >> .input_card
echo "          0  10  122  120  118  114  108  106  105" >> .input_card
echo "                 104  103  102" >> .input_card
echo "               100.0     200.0     300.0     400.0     500.0     600.0     700.0" >> .input_card
echo "               800.0     900.0    1000.0" >> .input_card
    ggplot MDTTCD input=.input_card

# -------------- Shallow Convection

# ------ Bottom

     div TBCS TCSCB DTBCS

# ------ Convert from Pa to hPa     

echo "XLIN          1.0E-2     0.0E0" > .input_card_xlin
     ccc xlin DTBCS X input=.input_card_xlin
     mv X DTBCS

     echo ' FMASK            -1   -1   GT        0.' > .fmask_input_card     

     fmskplt DTBCS MDTBCS TCSCB  input=.fmask_input_card

echo "GGPLOT             0 NEXT    14   0        1.        0.        1.       30.0${b}8" > .input_card
echo "$run $days SHALLOW CONV BASE (hPa)" >> .input_card
echo "          0  11  122  120  118  114  109  108  107" >> .input_card
echo "                 105 104  103  102" >> .input_card
echo "               500.0     550.0     600.0     650.0     700.0     750.0     800.0" >> .input_card
echo "               850.0     900.0     950.0    1000.0" >> .input_card
    ggplot MDTBCS input=.input_card

# ------ Top

     div TTCS TCSCB DTTCS

# ------ Convert from Pa to hPa     

echo "XLIN          1.0E-2     0.0E0" > .input_card_xlin
     ccc xlin DTTCS X input=.input_card_xlin
     mv X DTTCS

     echo ' FMASK            -1   -1   GT        0.' > .fmask_input_card     

     fmskplt DTTCS MDTTCS TCSCB  input=.fmask_input_card

echo "GGPLOT             0 NEXT    14   0        1.        0.        1.       30.0${b}8" > .input_card
echo "$run $days SHALLOW CONV TOP (hPa)" >> .input_card
echo "          0  11  122  120  118  114  109  108  107" >> .input_card
echo "                 105 104  103  102" >> .input_card
echo "               500.0     550.0     600.0     650.0     700.0     750.0     800.0" >> .input_card
echo "               850.0     900.0     950.0    1000.0" >> .input_card
    ggplot MDTTCS input=.input_card

# --------------------------------- Plot precipitation (total and convective)

# ------ First adjust the precipitation rates to mm/day

echo "XLIN          86400.        0." > .input_card_xlin
     ccc xlin PCP X input=.input_card_xlin ; mv X PCP
     ccc xlin PCPC X input=.input_card_xlin ; mv X PCPC
#     sub PCP PCPC PCPL

# ------ Total

echo "GGPLOT PCP         0 NEXT    13   0        1.        0.       50.        2.0${b}8" > .input_card
echo "$run $days TOTAL MODEL PRECIP.[MM/DAY]" >> .input_card
echo "              7  140  180  185  187  189  153  159" >> .input_card
echo "                  2.        4.        6.        8.       12.       20.       24." >> .input_card
    ggplot PCP input=.input_card

# ------ Large-scale

#echo "GGPLOT PCPL        0 NEXT    13   0        1.        0.       50.        2.0${b}8" > .input_card
#echo "$run $days LARGE-SCALE MODEL PRECIP.[MM/DAY]" >> .input_card
#echo "              7  140  180  185  187  189  153  159" >> .input_card
#echo "                  2.        4.        6.        8.       12.       20.       24." >> .input_card
#    ggplot PCPL input=.input_card

# ------ Deep

echo "GGPLOT PCPC        0 NEXT    13   0        1.        0.       50.        2.0${b}8" > .input_card
echo "$run $days CONV MODEL PRECIP.[MM/DAY]" >> .input_card
echo "              7  140  180  185  187  189  153  159" >> .input_card
echo "                  2.        4.        6.        8.       12.       20.       24." >> .input_card
    ggplot PCPC input=.input_card


# ----------------------------------------------------- Plot the 3D fields

# ------ Grab all of the 3D fields in one go

echo "XFIND.        TDMC
XFIND.        TSMC" > .ic_xfind

    xfind filex TDMC TSMC input=.ic_xfind

# ----------------------------------------------------- Plot the 3D mass fluxes

# ------ Deep

   zonavg TDMC ZTDMC

echo "ZXPLOT.    NEXT    0    0   -1     1.0E0        0.       .01      .001     0$kin" > .input_card
echo "$run $days DEEP NET MASS FLUX ()" >> .input_card
echo "                 90.      -90.     $m01     1000.    0" >> .input_card

    zxplot ZTDMC input=.input_card

# ------ SHALLOW

   zonavg TSMC ZTSMC

echo "ZXPLOT.    NEXT    0    0   -1     1.0E0        0.       .01      .001    0$kin" > .input_card
echo "$run $days SHALLOW NET MASS FLUX ()" >> .input_card
echo "                 90.      -90.     $m01     1000.    0"  >> .input_card

    zxplot ZTSMC input=.input_card

.     plotid.cdk
.     plot.cdk

end_of_script

cat > Input_Cards <<end_of_data
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
         DAYS      = $days
         RUN       = $run
         FLABEL    = $flabel
         OBSFILE   = $obsfile
         OBSDAY    = $obsday
0 PLOT_XTRACONV2 ------------------------------------------------ PLOT_XTRACONV2
. headfram.cdk
. tailfram.cdk

end_of_data

. endjcl.cdk
