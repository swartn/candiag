#deck landstat
jobname=landstat ; time=$stime ; memory=$memory1
. comjcl.cdk

cat > Execute_Script <<end_of_script

#   Correct TBAS select input card upper level range (to 1 from 3).
#   Add fields tbas, snof, fsno, wlas, wfas, and replace thlq and thic
#   with wla and wfa.
#                  landstat            E.Chan. - Jun 28/02 - FM.
#   ---------------------------------- Land-surface statistics.
# 
.   ggfiles.cdk
#
#   ---------------------------------- Select fields from three-level variables.
ccc select npakgg qfc1 hmfg1 htc1 
ccc select npakgg qfc2 hmfg2 htc2
ccc select npakgg qfc3 hmfg3 htc3
#
#   ---------------------------------- Surface net radiation
#                                        -- BALG: see radiag4.
# 
#   ---------------------------------- Surface latent heat flux.
ccc select npakgg hevc hevs hevg
    add hevc hevs sum1
    add sum1 hevg hev
    timavg hev thev
    rm sum1 hev
# 
#   ---------------------------------- Surface sensible heat flux
#                                        -- HFS: see flxstat2.
# 
#   ---------------------------------- Surface heat of melting.
#                                      Save snow and soil separately.
ccc select npakgg hmfc hmfn
    add hmfg1 hmfg2 sum1
    add hmfg3 sum1 hmfg
    add hmfc hmfn sum2
    add sum2 hmfg hmf
    timavg hmf thmf
    timavg hmfn thmfn
    timavg hmfg thmfg
    rm hmfg sum1 sum2 hmf
# 
#   ---------------------------------- Heat of percolation/conduction.
ccc select npakgg htcc htcs 
    add htc1 htc2 sum1
    add htc3 sum1 htc
    add htcc htcs sum2
    add sum2 htc hpc
    timavg hpc thpc
    rm sum1 sum2 hpc htc
# 
#   ---------------------------------- Precipitation rate 
#                                        -- PCP: see flxstat2.
# 
#   ---------------------------------- Surface evapotranspiration rate.
ccc select npakgg qfcl qfcf qfg qfn
    add qfc1 qfc2 sum1
    add qfc3 sum1 qfc
    add qfcl qfg sum2
    add sum2 qfc qfsuml
    add qfcf qfn qfsumf
    rm qfc1 qfc2 qfc3 sum1 sum2
    add qfsuml qfsumf qf
    timavg qf tqf
    rm qfsuml qfsumf qf
#
#   ---------------------------------- Runoff - total and overland.
ccc select npakgg rof rofo
    timavg rof trof
    timavg rofo trofo
    rm rofo
#
#   ---------------------------------- Screen temperature
#                                        -- ST: see sscreen.
# 
#   ---------------------------------- Canopy temperature.
ccc select npakgg x
.   timav.cdk
    mv tx ttcan
# 
#   ---------------------------------- Snow temperature.
ccc select npakgg x
.   timav.cdk
    mv tx ttsno
# 
#   ---------------------------------- Soil layer temperatures.
ccc select npakgg tbar
    timavg tbar ttbar1
    xlin ttbar1 ttbar
    rm tbar ttbar1
# 
#   ---------------------------------- Bedrock temperature.
ccc select npakgg tbas
    timavg tbas ttbas1
    xlin ttbas1 ttbas
    rm tbas ttbas1
# 
#   ---------------------------------- Canopy intercepted water.
ccc select npakgg wcan
    timavg wcan twcan
    rm wcan
# 
#   ---------------------------------- Snowfall rate (water equivalent).
ccc select npakgg snof
    timavg snof tsnof
    rm snof
# 
#   ---------------------------------- Snow depth.
ccc select npakgg zsno
    timavg zsno tsdep 
    rm zsno
# 
#   ---------------------------------- Snow fraction.
ccc select npakgg fsno
    timavg fsno tfsno
    rm fsno
# 
#   ---------------------------------- Surface layer liquid soil water content.
ccc select npakgg wlas
    timavg wlas twlas
    rm wlas
# 
#   ---------------------------------- Surface layer frozen soil water content.
ccc select npakgg wfas
    timavg wfas twfas
    rm wfas
# 
#   ---------------------------------- Total liquid soil water content.
ccc select npakgg wla
    timavg wla twla
    rm wla
# 
#   ---------------------------------- Total frozen soil water content.
ccc select npakgg wfa
    timavg wfa twfa
    rm wfa
# 
#   ---------------------------------- Canopy energy flux convergence.
ccc select npakgg fsgv flgv hfsc
    add fsgv flgv sum1
    sub sum1 hfsc dif1
    sub dif1 hevc dif2
#cc sub dif2 hmfc edivc
    sub dif2 hmfc dif3
    add dif3 htcc edivc
    timavg edivc tedivc
    rm fsgv flgv hfsc hevc sum1 dif1 dif2 dif3 hmfc htcc edivc
# 
#   ---------------------------------- Snow energy flux convergence.
ccc select npakgg fsgs flgs hfss
    add fsgs flgs sum1
    add hfss hmfn sum2
    sub sum1 sum2 dif1
#cc sub dif1 hevs edivs
    sub dif1 hevs dif2
    add dif2 htcs edivs
    timavg edivs tedivs
    rm fsgs flgs hfss hmfn hevs sum1 sum2 dif1 dif2 htcs edivs
# 
#   ---------------------------------- Soil energy flux convergence.
#                                      Layer 1.
ccc select npakgg fsgg flgg hfsg
    add fsgg flgg sum1
    add hfsg hevg sum2
    sub sum1 sum2 dif1
#cc sub dif1 hmfg edivg
    sub dif1 hmfg1 dif2
    add dif2 htc1 ediv1
    timavg ediv1 tediv1
    rm fsgg flgg hfsg hevg sum1 sum2 dif1 dif2 hmfg1 htc1 ediv1
# 
#   ---------------------------------- Soil energy flux convergence.
#                                      Layer 2.
    sub htc2 hmfg2 ediv2
    timavg ediv2 tediv2
    rm htc2 hmfg2 ediv2
# 
#   ---------------------------------- Soil energy flux convergence.
#                                      Layer 3.
    sub htc3 hmfg3 ediv3
    timavg ediv3 tediv3
    rm htc3 hmfg3 ediv3
#
#   ---------------------------------- Canopy water flux convergence.
ccc select npakgg pclc pcfc rofc wtrc
    add pclc pcfc sum1
    sub sum1 qfcf dif1
    sub dif1 qfcl dif2
    sub dif2 rofc dif3
    add dif3 wtrc qdivc
    timavg qdivc tqdivc
    rm pclc pcfc rofc wtrc sum1 qfcf qfcl dif1 dif2 dif3 qdivc
#
#   ---------------------------------- Snow water flux convergence.
ccc select npakgg pcpn rofn wtrs
    sub pcpn qfn dif1
    sub dif1 rofn dif2
    add dif2 wtrs qdivs
    timavg qdivs tqdivs
    rm pcpn rofn qfn wtrs dif1 dif2 qdivs
#
#   ---------------------------------- Soil water flux convergence.
ccc select npakgg pcpg wtrg
    add qfg qfc sum1
    sub pcpg sum1 dif1
    sub dif1 rof dif2
    add dif2 wtrg qdivg
    timavg qdivg tqdivg
    rm pcpg qfg qfc rof wtrg sum1 dif1 dif2 qdivg
#
#   ---------------------------------- Land ice mask.
ccc select npakgg tt
    timavg tt ttt
    fmask ttt mi
    rm tt ttt
#   ---------------------------------- Save gaussian grids.
#
    xsaveup new_gp thev thmf thmfn thmfg thpc tqf \
                   trof trofo twcan ttcan ttsno \
                   ttbar ttbas tsnof tsdep tfsno \
                   twlas twfas twla twfa tedivc \
                   tedivs tediv1 tediv2 tediv3 \
                   tqdivc tqdivs tqdivg mi
#
    rm thev thmf thmfn thmfg thpc tqf trof trofo 
    rm twcan ttcan ttsno ttbar ttbas tsnof tsdep tfsno 
    rm twlas twfas twla twfa tedivc tedivs 
    rm tediv1 tediv2 tediv3 tqdivc tqdivs tqdivg mi
#
    access oldgp ${flabel}gp
    xjoin oldgp new_gp newgp
    save newgp ${flabel}gp
    delete oldgp

end_of_script

cat > Input_Cards <<end_of_data
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
         DAYS      = $days 
         RUN       = $run
         FLABEL    = $flabel 
         MODEL1    = $model1 
         T1        = $t1 
         T2        = $t2 
         T3        =      $t3
         DELT      = $delt 
0 LANDSTAT ------------------------------------------------------------ LANDSTAT
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT NPAKGG   $t1 $t2 $t3 LEVS    1    1 NAME  QFC HMFG  HTC
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT NPAKGG   $t1 $t2 $t3 LEVS    2    2 NAME  QFC HMFG  HTC
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT NPAKGG   $t1 $t2 $t3 LEVS    3    3 NAME  QFC HMFG  HTC
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT NPAKGG   $t1 $t2 $t3 LEVS    1    1 NAME HEVC HEVS HEVG
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT NPAKGG   $t1 $t2 $t3 LEVS    1    1 NAME HMFC HMFN
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT NPAKGG   $t1 $t2 $t3 LEVS    1    1 NAME HTCC HTCS
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT NPAKGG   $t1 $t2 $t3 LEVS    1    1 NAME QFCL QFCF  QFG  QFN
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT NPAKGG   $t1 $t2 $t3 LEVS    1    1 NAME  ROF ROFO
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT NPAKGG   $t1 $t2 $t3 LEVS    1    1 NAME TCAN
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  FMASK              NEXT   LT       0.5
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  FMASK              NEXT   LE        0.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
 XLIN          1.0E0 -2.7316E2
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT NPAKGG   $t1 $t2 $t3 LEVS    1    1 NAME TSNO
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  FMASK              NEXT   LT       0.5
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  FMASK              NEXT   LE        0.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
 XLIN          1.0E0 -2.7316E2
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT NPAKGG   $t1 $t2 $t3 LEVS    1    3 NAME TBAR
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
 XLIN          1.0E0 -2.7316E2
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT NPAKGG   $t1 $t2 $t3 LEVS    1    1 NAME TBAS
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
 XLIN          1.0E0 -2.7316E2
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT NPAKGG   $t1 $t2 $t3 LEVS    1    1 NAME WCAN
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT NPAKGG   $t1 $t2 $t3 LEVS    1    1 NAME SNOF
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT NPAKGG   $t1 $t2 $t3 LEVS    1    1 NAME ZSNO
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT NPAKGG   $t1 $t2 $t3 LEVS    1    1 NAME FSNO
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT NPAKGG   $t1 $t2 $t3 LEVS    1    1 NAME WLAS
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT NPAKGG   $t1 $t2 $t3 LEVS    1    1 NAME WFAS
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT NPAKGG   $t1 $t2 $t3 LEVS    1    1 NAME  WLA
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT NPAKGG   $t1 $t2 $t3 LEVS    1    1 NAME  WFA
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT NPAKGG   $t1 $t2 $t3 LEVS    1    1 NAME FSGV FLGV HFSC
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT NPAKGG   $t1 $t2 $t3 LEVS    1    1 NAME FSGS FLGS HFSS
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT NPAKGG   $t1 $t2 $t3 LEVS    1    1 NAME FSGG FLGG HFSG
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT NPAKGG   $t1 $t2 $t3 LEVS    1    1 NAME PCLC PCFC ROFC WTRC
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT NPAKGG   $t1 $t2 $t3 LEVS    1    1 NAME PCPN ROFN WTRS
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT NPAKGG   $t1 $t2 $t3 LEVS    1    1 NAME PCPG WTRG
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT NPAKGG   $t1 $t2 $t3 LEVS    1    1 NAME   TT
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  FMASK              NEXT   LT      -1.9
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         HEV 
NEWNAM      HEV
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         HMF 
NEWNAM      HMF
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         HMFN
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         HMFG 
NEWNAM     HMFG
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         HPC 
NEWNAM      HPC
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         QF
NEWNAM       QF
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         ROF
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         ROFO
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         WCAN 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         TCAN 
NEWNAM     TCAN
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         TSNO 
NEWNAM     TSNO
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         TBAR 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         TBAS 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         SNOF 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         ZSNO 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         FSNO 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         WLAS
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         WFAS
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         WLA
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         WFA
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         EDVC 
NEWNAM     EDVC
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         EDVS 
NEWNAM     EDVS
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         EDV1 
NEWNAM     EDV1
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         EDV2 
NEWNAM     EDV2
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         EDV3 
NEWNAM     EDV3
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         QDVC 
NEWNAM     QDVC
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         QDVS 
NEWNAM     QDVS
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         QDVG 
NEWNAM     QDVG
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         LIC
NEWNAM      LIC
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
end_of_data

. endjcl.cdk
