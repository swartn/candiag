#!/bin/sh
#                  xstats4             SK - Feb 06/2009 - SK -----------XSTATS4
#
#   sk: based on xstats3 but for tracer parameters as in model string
#
#  ----------------------------------- calculate tracer statistics.
#   The list of tracers to process can be defined in variable "trac".
#   If $trac is undefined, or if $trac="ALL" then all tracers, as specified in
#   a model submission string,  are processed.
#   Single level diagnostics include:
#   VInn, ... - monthly mean vertical intergrals, where nn=01,02,...
#   VInn FIRST TIME STEP - the very first time step in each month (needed for budgets)
#   XKnn/XLnn, ... - lowest model level concentrations. 
#   The deck will try to process XKnn first, and then XLnn, if XKnn does not exist.
#
#   The following tracer parameters are used:
#     ntrac = (mandatory) the number of all tracers.
#      itNN = (mandatory) tracer names, where NN=01,...,$ntrac.
#     advNN = (mandatory) tracer advection parameter (=1 for advective tracers).
#    xrefNN = (mandatory) xref tracer parameter (xref=0. for non-hybrid tracers),
#    xpowNN = (optional) xpow tracer parameter (default=1.)
#

#   ---------------------------------- determine list of tracers to process.

      if [ -z "$ntrac" ] ; then
#                                      ntrac must be defined.
	echo "\nError: **** Number of tracer ntrac is undefined. ****\n"
	exit 1
      fi
#                                      check if $trac is undefined or trac="ALL"
      tracs=`echo "$trac" | sed 's/^ *//g' | sed 's/ *$//g'` # remove leading/trailing spaces
      if [ -z "$tracs" -o "$tracs" = "ALL" ] ; then
#                                      try to diagnose all $ntrac tracers
	trac=""
	i2=1
	while [ $i2 -le $ntrac ] ; do
#                                      determine tracer name
	  i2=`echo "0$i2" | tail -3c` # 2-digit tracer number
	  eval x=\${it$i2}
	  if [ -z "$x" ] ; then
	    echo "\nError: **** tracer name for it$i2 is undefined. ****\n"
	    exit 1
	  fi
	  trac="$trac $x"
	  i2=`expr $i2 + 1`
	done
	ntracf=$ntrac
      else
#                                      process only tracers specified by $trac
	ntracf=`echo $trac | wc -w`
      fi
      echo "\n==>Process $ntracf tracers $trac\n"

#   ---------------------------------- input cards for gsapl and cofagg

      echo "GSAPL.    $plv       0.0       0.0$coord$plid
$p01$p02$p03$p04$p05$p06$p07$p08$p09$p10$p11$p12$p13$p14$p15$p16
$p17$p18$p19$p20$p21$p22$p23$p24$p25$p26$p27$p28$p29$p30$p31$p32
$p33$p34$p35$p36$p37$p38$p39$p40$p41$p42$p43$p44$p45$p46$p47$p48
$p49$p50$p51$p52$p53$p54$p55$p56$p57$p58$p59$p60$p61$p62$p63$p64
$p65$p66$p67$p68$p69$p70$p71$p72$p73$p74$p75$p76$p77$p78$p79$p80
$p81$p82$p83$p84$p85$p86$p87$p88$p89$p90$p91$p92$p93$p94$p95$p96
$p97$p98$p99$p100" > .gsapl_input_card

      echo "COFAGG.   $lon$lat    0$npg" > .cofagg_input_card

#   ---------------------------------- access beta for zonal averging

      lzon="    0"
      lbeta="    1"
      if [ "$rcm" != "on" ] ; then
        lzon="    1"
#                                      try first to access beta directly
	access beta ${flabel}_gptbeta na
	if [ ! -s beta ] ; then
#                                      if not available, get it from gp file
           access oldgp ${flabel}gp
	   echo "XFIND.        $d" | ccc xfind oldgp beta
	fi
      fi

#   ---------------------------------- access u,v,w and set parameters for gpxstat

      luvw="    0"
      if [ "$stat2nd" != "off" ] ; then
        lstat="    1"
        ldt="    1"
	access u    ${flabel}_gpu
	access v    ${flabel}_gpv
	if [ "$wxstats" = "on" ] ; then
	  lw="    1"
	  access w ${flabel}_gpw
	else
	  lw="    0"
	fi
      else
        lstat="    0"
        ldt="    0"
	lw="    0"
      fi
      echo "          $luvw$lw$ldt$lzon$lbeta$lstat$delt" > .ic_gpxstat

#   ---------------------------------- process tracers

      gpx="" # 3d tracers
      vin="" # tracer vertical integrals
      xkn="" # lowest level concentrations
      xfn="" # lowest level fluxes

      xtstat_getdata="y"
      i=1
      while [ $i -le $ntracf ] ; do
#                                      determine tracer name
	x=`echo $trac | cut -f$i -d' '`
#                                      determine tracer number
	i2=1
	while [ $i2 -le $ntrac ] ; do
	  i2=`echo "0$i2" | tail -3c` # 2-digit tracer number
	  eval xn=\${it$i2}
	  if [ "$x" = "$xn" ] ; then
	    break;
	  fi
	  i2=`expr $i2 + 1`
	done
        if [ $i2 -gt $ntrac ] ; then
          echo "\nError: **** tracer number for $x is not found. ****\n"
          exit 1
        fi
	echo "\n==>Tracer number for $x is $i2.\n"

#                                      determine tracer parameters adv,xref,xpow

        eval adv=\${adv$i2}
        if [ -z "$adv" ] ; then
          echo "\nError: **** tracer parameter adv for it$i2 is undefined. ****\n"
          exit 1
        fi
        eval xref=\${xref$i2}
        if [ -z "$xref" ] ; then
          echo "\nError: **** tracer parameter xref for it$i2 is undefined. ****\n"
          exit 1
        fi
        eval xpow=\${xpow$i2:=1.} # default 1.

#                                      build fixed-format strings

        NAME=`echo "    $x" | tail -5c` # right-aligned 4-character name
#                                      XREF
	xref=`echo $xref | tr -d ' '`
        XREF=`echo "          $xref" | tail -11c` # 10-character XREF
#                                      XPOW
	xpow=`echo $xpow | tr -d ' '`
        XPOW=`echo "          $xpow" | tail -11c` # 10-character XPOW

        echo "\n==> For tracer i=$i it=$x xref=$xref xpow=$xpow adv=$adv\n"

#   ---------------------------------- get model files (do this only once)

	if [ "$xtstat_getdata" = "y" ] ; then
          xtstat_getdata="n"
.         ggfiles.cdk
#                                      spectral sigma case
          if [ "$datatype" = "specsig" ] ; then
.           spfiles.cdk
            access gslnsp ${flabel}_gslnsp na
	    if [ ! -s gslnsp ] ; then
	      echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME LNSP" > .select_input_card_lnsp
ccc           select npaksp sslnsp input=.select_input_card_lnsp
              cofagg sslnsp gslnsp input=.cofagg_input_card
	    fi
#                                      grid sigma case
	  elif [ "$datatype" = "gridsig" ] ; then
	    access gslnsp ${flabel}_gslnsp na
	    if [ ! -s gslnsp ] ; then
	      echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME LNSP" > .select_input_card_lnsp
ccc           select npakgg sslnsp input=.select_input_card_lnsp
	    fi
	  fi
	fi # xtstat_getdata

#   ---------------------------------- get the tracer on pressure levels
#                                      (process only advective tracers)

	echo "SELECT    STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME $NAME" > .select_input_card

#                                      spectral-sigma and adv=1 case.

	if [ "$datatype" = "specsig" -a  $adv -eq 1 ] ; then
#                                      spectral tracer
	  rm sstrac
ccc       select npaksp sstrac input=.select_input_card || true
          if [ ! -s sstrac ] ; then
	    echo "\nWarning: **** spectral tracer $NAME is not found in the model ss file. ***\n"
	  else
	    if [ "$xref" = "0." ] ; then
#                                      non-hybrid tracer
	      cofagg sstrac gsx input=.cofagg_input_card
	      rm sstrac
	      gsapl gsx gslnsp $x input=.gsapl_input_card
	      rm gsx
	    else
#                                      hybrid tracer
	      echo " GSTRACX. $coord $itrvar$XREF$XPOW    1" > .gstracx_input_card
	      cat .cofagg_input_card >> .gstracx_input_card
	      cat .gsapl_input_card >> .gstracx_input_card
	      gstracx sstrac $x gslnsp input=.gstracx_input_card
	      rm sstrac
	    fi
	    gpx="$gpx $x"
	  fi

#                                      grid-sigma or adv=0 case.

	else
#                                      gridded tracer
	  rm gstrac
ccc       select npakgg gstrac input=.select_input_card || true
          if [ ! -s gstrac ] ; then
	    echo "\nWarning: **** gridded tracer $NAME is not found in the model gs file. ****\n"
	  else
	    if [ "$xref" = "0." ] ; then
	      mv gstrac gsx
	      gsapl gsx gslnsp $x input=.gsapl_input_card
	      rm gsx
	    else
	      echo " GSTRACX. $coord $itrvar$XREF$XPOW    1" > .gstracx_input_card
	      cat .gsapl_input_card >> .gstracx_input_card
	      gstracx gstrac $x gslnsp input=.gstracx_input_card
	      rm gstrac
	    fi
	    gpx="$gpx $x"
	  fi
	fi

#   ---------------------------------- single-level tracer diagnostics.
#                                      VInn - vertical integral, 
#                                      XKnn/XLnn - lowest level concentration,
#                                      XFnn - surface flux.
	vinn="VI$i2"
	xfnn="XF$i2"
	xknn="XK$i2"
	xlnn="XL$i2"
	rm $vinn $xfnn $xknn $xlnn
        echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME $vinn $xfnn $xknn $xlnn" > .select_input_card
ccc     select npakgg $vinn $xfnn $xknn $xlnn input=.select_input_card || true
	if [ -s $vinn ] ; then
	  vin="$vin $vinn"
	else
	  echo "\nWarning: **** $vinn is not found in the model gs file. ****\n"
	fi
	if [ -s $xfnn ] ; then
	  xfn="$xfn $xfnn"
	else
	  echo "\nWarning: **** $xfnn is not found in the model gs file. ****\n"
	fi
	if [ -s $xknn ] ; then
	  xkn="$xkn $xknn"
	else
	  echo "\nWarning: **** $xknn is not found in the model gs file. ****\n"
#                                      try XLnn if XKnn is not found  
	  if [ -s $xlnn ] ; then
	    xkn="$xkn $xlnn"
	  else
	    echo "\nWarning: **** $xlnn is not found in the model gs file. ****\n"
	  fi
	fi

        i=`expr $i + 1`
      done
      ngpx=`echo $gpx | wc -w`
      echo "\n Found $ngpx 3D tracers $gpx"

      nvin=`echo $vin | wc -w`
      echo "\n Found $nvin diagnostics $vin"

      nxkn=`echo $xkn | wc -w`
      echo "\n Found $nxkn diagnostics $xkn"

      nxfn=`echo $xfn | wc -w`
      echo "\n Found $nxfn diagnostics $xfn"

      slx="$vin $xkn $xfn"
      nslx=`echo $vin $xkn $xfn | wc -w`
      echo "\n Found $nslx single-level diagnostics $slx"

#   ---------------------------------- 3d tracer statistics
      if [ $ngpx -ge 1 ] ; then
	gpx=`echo "$gpx" | sed -e 's/^ *//' -e 's/  */ /g' -e 's/ *$/ /'`
	while [ -n "$gpx" ] ; do
	  # process maximum 80 tracers at a time
	  gpx1=`echo "$gpx" | cut -f1-80 -d' '`
	  gpxstat beta u v w $gpx1 new_gp new_xp input=.ic_gpxstat
	  gpx=`echo "$gpx" | cut -f81- -d' '`
	done
      fi
#                                      single-level statistics
      if [ $nslx -ge 1 ] ; then
	slx=`echo "$slx" | sed -e 's/^ *//' -e 's/  */ /g' -e 's/ *$/ /'`
	while [ -n "$slx" ] ; do
	  # process maximum 180 tracers at a time
	  slx1=`echo "$slx" | cut -f1-180 -d' '`
	  statsav $slx1 new_gp new_xp $stat2nd
	  slx=`echo "$slx" | cut -f181- -d' '`
	done
      fi
#                                      save the first time step for VInn
#                                      (needed for budget calculations)
      if [ $nvin -ge 1 ] ; then
	echo "                   1         1" > ic.rcopy
	vin=`echo "$vin" | sed -e 's/^ *//' -e 's/  */ /g' -e 's/ *$/ /'`
	while [ -n "$vin" ] ; do
	  vin1=""
	  rm ic.xsave
	  for vi in $vin ; do
	    rcopy $vi $vi.1 input=ic.rcopy
	    vin1="$vin1 $vi.1"
	    echo "C*XSAVE       $vi FIRST TIME STEP
" >> ic.xsave
	  done
	  xsave new_gp $vin1 newgp input=ic.xsave
	  mv newgp new_gp
	  vin=`echo "$vin" | cut -f81- -d' '`
	done
      fi

#   ---------------------------------- save results.

      if [ ! -s oldgp ] ; then
#                                      access only if not already available
	access oldgp ${flabel}gp
      fi
      xjoin  oldgp new_gp newgp
      save   newgp ${flabel}gp
      delete oldgp

      if [ "$rcm" != "on" ] ; then
	access oldxp ${flabel}xp
	xjoin  oldxp new_xp newxp
	save   newxp ${flabel}xp
	delete oldxp
      fi

#   ---------------------------------- save (sub)daily 3-d tracers, if requested

      if [ "$gpxsave" = "on" ] ; then
	for x in $gpx ; do
	  save ${x} ${flabel}_gp${x}
	done
      fi
