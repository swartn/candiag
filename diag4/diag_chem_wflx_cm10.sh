#!/bin/sh
#
# This script is only included in the merged_diag_script grouping whengas-phase
# chemistry is run in MAM mode. In this version of the script, with the "cm10"
# extension, variable WF07 has been removed. BW, August 2023
#
#  DESCRIPTION (original name: diag_chem_wflx_971p.sh)

#  Create monthly averages of the diagnostic deposition fields
#  saved by the model
#
#  ---- composition of WFnn beginning with mam_945a:
#   WF01 = wet deposition of HNO3    ; WF02 = wet deposition of HNO4
#   WF03 = dry deposition of HNO3    ; WF04 = dry deposition of O3
#   WF05 = dry deposition of NO2     ; WF06 = flux of O3 to plant stomata
#
#  ---- all fluxes are time averaged as moles/m^2/sec
#
#  PARMSUB PARAMETERS
#
#    -- chem, flabel, memory1, model1, pmax, pmin, stime, r1,
#       r2, r3
#
#  PREREQUISITES
#
#    -- GS history files
#
#  CHANGELOG
#
#    2023-01-29: Converted to shell script (D. Plummer)
#    2013-06-26: Using r1, r2, r3(==1) for time intervals on select
#                 to catch all outputs
#    2012-12-05: Changed diagnostic deck to reflect modified output
#                 into the WF field from the model and other
#                 modifications to improve efficiency
#    2010-05-17: Modification of superlabels to reflect new units
#    2008-05-31: Initial version (D. Plummer)
#

#
#  ----  access model history files
#
.   ggfiles.cdk
#
#  ----  select out required fields
#
    echo "SELECT    STEPS $r1 $r2 $r3 LEVS    1    1 NAME WF01 WF02 WF03 WF04" |
       ccc   select npakgg flx1 flx2 flx3 flx4
    echo "SELECT    STEPS $r1 $r2 $r3 LEVS    1    1 NAME WF05 WF06" |
       ccc   select npakgg flx5 flx6
    rm npakgg
#
#  ----  create monthly averages
#
    timavg flx1 flx1_tav
    timavg flx2 flx2_tav
    timavg flx3 flx3_tav
    timavg flx4 flx4_tav
    timavg flx5 flx5_tav
    timavg flx6 flx6_tav
#
    rm flx1 flx2 flx3 flx4
    rm flx5 flx6
#
#  ----  save time average fields
#     gp - time averaged data on lat/lon
#
    release oldgp
    access oldgp ${flabel}gp      na
    echo "XSAVE.        HNO3 WETDEP FLUX - MOLES OVER M2 OVER SEC
NEWNAM.
XSAVE.        HNO4 WETDEP FLUX - MOLES OVER M2 OVER SEC
NEWNAM.
XSAVE.        HNO3 DRYDEP FLUX - MOLES OVER M2 OVER SEC
NEWNAM.
XSAVE.        O3 DRYDEP FLUX - MOLES OVER M2 OVER SEC
NEWNAM.
XSAVE.        NO2 DRYDEP FLUX - MOLES OVER M2 OVER SEC
NEWNAM.
XSAVE.        O3 STOMATAL FLUX - MOLES OVER M2 OVER SEC
NEWNAM." | ccc xsave  oldgp flx1_tav flx2_tav flx3_tav flx4_tav \
                 flx5_tav flx6_tav newgp
    save newgp ${flabel}gp
    delete oldgp                  na
