#!/bin/sh
#  Convert to script - 08/DEC/2017 - DY
#   Phase out disabling the trap in optional processing of
#   "gs" dataset(s) via "ggfiles.cdk" call.
#
#  dp - Sep 22/22 - bug fix on GSOMGAH call for gcm2plus=on
#
#                gpintstat2            ec,fm,gjb,dl,sk - Nov 21/13 - fm
#   ---------------------------------- compute and save Q,T,Z,U,V,W,[VR,DIV]
#                                      on pressure grids

#   ---------------------------------- spectral sigma case.

      if [ "$datatype" = "specsig" ] ; then
.        spfiles.cdk
         access GsCheck ${model1}gs nocp na
         if [ -s "./GsCheck" ] ; then
          release GsCheck
.         ggfiles.cdk
         else
          release GsCheck
         fi
         npakgg="off"
         if [ -s npakgg ] ; then
           npakgg="on"
         fi
                                       # get q, t, and z on pressure grid.

echo "SELECT.   STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME PHIS LNSP" | ccc select npaksp ssphis sslnsp
echo "COFAGG.   $lon$lat    0    1" | ccc cofagg ssphis gsphis
echo "COFAGG.   $lon$lat    0    1" | ccc cofagg sslnsp gslnsp
         if [ "$npakgg" = "on" -a \( "$moist" = " SL3D" -o "$moist" = " SLQB" \) ] ; then
echo "SELECT.   STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME   ES" | ccc select npakgg gses
         else
echo "SELECT.   STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME   ES" | ccc select npaksp sses
echo "COFAGG.   $lon$lat    0$npg" | ccc cofagg sses gses
         fi
                                       # select RH from gs files
         gsrh="off"
         if [ "$npakgg" = "on" ] ; then
echo "SELECT          $t1 $t2 $t3 LEVS-9001 1000        RH" | ccc select npakgg gsrh
            gsrh="on"
         fi

                                       # calculate gstemp.

         if [ "$gcmtsav" = on ] ; then
echo "SELECT          $t1 $t2 $t3 LEVS-9001 1000      TEMP" | ccc select npaksp sstemp
         else
echo "SELECT          $t1 $t2 $t3 LEVS-9001 1000       PHI" | ccc select npaksp ssphi
            ctemps ssphis ssphi sstemp
         fi
echo "COFAGG.   $lon$lat    0$npg" | ccc cofagg sstemp gstemp
                                       # calculates gsshum, gsrhum.

echo "GSHUMH.   $coord$moist$plid$sref$spow" | ccc gshumh gses gstemp gslnsp gsshum gsrhum

         if [ "$gcmtsav" = on ] ; then
                                       # calculate gsphi.
echo "XLIN.      174.52032    287.04 RGAS" | ccc xlin gsshum gsrgasm
echo "TAPHI.    $coord$lay$plid" | ccc taphi gstemp gsphis gslnsp gsrgasm gsphi

                                       # calculate phi and t on pressure grid.

echo "GSAPZ(T)  $plv       0.0    6.5E-3$coord$plid
$p01$p02$p03$p04$p05$p06$p07$p08$p09$p10$p11$p12$p13$p14$p15$p16
$p17$p18$p19$p20$p21$p22$p23$p24$p25$p26$p27$p28$p29$p30$p31$p32
$p33$p34$p35$p36$p37$p38$p39$p40$p41$p42$p43$p44$p45$p46$p47$p48
$p49$p50$p51$p52$p53$p54$p55$p56$p57$p58$p59$p60$p61$p62$p63$p64
$p65$p66$p67$p68$p69$p70$p71$p72$p73$p74$p75$p76$p77$p78$p79$p80
$p81$p82$p83$p84$p85$p86$p87$p88$p89$p90$p91$p92$p93$p94$p95$p96
$p97$p98$p99$p100" | ccc gsaplt gstemp gsphi gsrgasm gslnsp gpt gpphi
         else
                                       # calculate gsphi.
echo "COFAGG.   $lon$lat    0$npg" | ccc cofagg ssphi gsphi
                                       # calculate phi and t on pressure grid.

echo "GSAPZ(T)  $plv       0.0    6.5E-3$coord$plid
$p01$p02$p03$p04$p05$p06$p07$p08$p09$p10$p11$p12$p13$p14$p15$p16
$p17$p18$p19$p20$p21$p22$p23$p24$p25$p26$p27$p28$p29$p30$p31$p32
$p33$p34$p35$p36$p37$p38$p39$p40$p41$p42$p43$p44$p45$p46$p47$p48
$p49$p50$p51$p52$p53$p54$p55$p56$p57$p58$p59$p60$p61$p62$p63$p64
$p65$p66$p67$p68$p69$p70$p71$p72$p73$p74$p75$p76$p77$p78$p79$p80
$p81$p82$p83$p84$p85$p86$p87$p88$p89$p90$p91$p92$p93$p94$p95$p96
$p97$p98$p99$p100" | ccc gsapzl gsphi gsphis gslnsp gpphi gpt

         fi
                                       # calculate msl pressure

echo "GSMSLPH       6.5E-3    0$lay$coord$plid" | ccc gsmslph gstemp gslnsp gsphis pmsl

                                       # calculate q on pressure grid.

echo "GSAPL(Q)  $plv       0.0       0.0$coord$plid
$p01$p02$p03$p04$p05$p06$p07$p08$p09$p10$p11$p12$p13$p14$p15$p16
$p17$p18$p19$p20$p21$p22$p23$p24$p25$p26$p27$p28$p29$p30$p31$p32
$p33$p34$p35$p36$p37$p38$p39$p40$p41$p42$p43$p44$p45$p46$p47$p48
$p49$p50$p51$p52$p53$p54$p55$p56$p57$p58$p59$p60$p61$p62$p63$p64
$p65$p66$p67$p68$p69$p70$p71$p72$p73$p74$p75$p76$p77$p78$p79$p80
$p81$p82$p83$p84$p85$p86$p87$p88$p89$p90$p91$p92$p93$p94$p95$p96
$p97$p98$p99$p100" | ccc gsapl gsshum gslnsp gpq

                                       # calculate RH on pressure grid.

         if [ "$gsrh" = "on" ] ; then
echo "GSAPL(RH) $plv       0.0       0.0$coord$plid
$p01$p02$p03$p04$p05$p06$p07$p08$p09$p10$p11$p12$p13$p14$p15$p16
$p17$p18$p19$p20$p21$p22$p23$p24$p25$p26$p27$p28$p29$p30$p31$p32
$p33$p34$p35$p36$p37$p38$p39$p40$p41$p42$p43$p44$p45$p46$p47$p48
$p49$p50$p51$p52$p53$p54$p55$p56$p57$p58$p59$p60$p61$p62$p63$p64
$p65$p66$p67$p68$p69$p70$p71$p72$p73$p74$p75$p76$p77$p78$p79$p80
$p81$p82$p83$p84$p85$p86$p87$p88$p89$p90$p91$p92$p93$p94$p95$p96
$p97$p98$p99$p100" | ccc gsapl gsrh gslnsp gprh
         fi
                                       # calculate u and v on pressure grid.

echo "SELECT    STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME VORT  DIV" | ccc select npaksp ssvort ssdiv
         cwinds ssvort ssdiv ssbigu ssbigv
echo "COFAGG.   $lon$lat    1$npg" | ccc cofagg ssbigu gsu
echo "COFAGG.   $lon$lat    1$npg" | ccc cofagg ssbigv gsv
echo "GSAPL(U)  $plv       0.0       0.0$coord$plid
$p01$p02$p03$p04$p05$p06$p07$p08$p09$p10$p11$p12$p13$p14$p15$p16
$p17$p18$p19$p20$p21$p22$p23$p24$p25$p26$p27$p28$p29$p30$p31$p32
$p33$p34$p35$p36$p37$p38$p39$p40$p41$p42$p43$p44$p45$p46$p47$p48
$p49$p50$p51$p52$p53$p54$p55$p56$p57$p58$p59$p60$p61$p62$p63$p64
$p65$p66$p67$p68$p69$p70$p71$p72$p73$p74$p75$p76$p77$p78$p79$p80
$p81$p82$p83$p84$p85$p86$p87$p88$p89$p90$p91$p92$p93$p94$p95$p96
$p97$p98$p99$p100" | ccc gsapl gsu gslnsp gpu
echo "GSAPL(V)  $plv       0.0       0.0$coord$plid
$p01$p02$p03$p04$p05$p06$p07$p08$p09$p10$p11$p12$p13$p14$p15$p16
$p17$p18$p19$p20$p21$p22$p23$p24$p25$p26$p27$p28$p29$p30$p31$p32
$p33$p34$p35$p36$p37$p38$p39$p40$p41$p42$p43$p44$p45$p46$p47$p48
$p49$p50$p51$p52$p53$p54$p55$p56$p57$p58$p59$p60$p61$p62$p63$p64
$p65$p66$p67$p68$p69$p70$p71$p72$p73$p74$p75$p76$p77$p78$p79$p80
$p81$p82$p83$p84$p85$p86$p87$p88$p89$p90$p91$p92$p93$p94$p95$p96
$p97$p98$p99$p100" | ccc gsapl gsv gslnsp gpv

                                       # calculate w on pressure grid.

         if [ "$wxstats" = "on" ] ; then
                                       # try to select w from gs file
echo "SELECT          $t1 $t2 $t3 LEVS-9001 1000      OMET" | ccc select npakgg gsomeg || true
            if [ ! -s gsomeg ] ; then
                                       # w is not found
              if [ "$gcm2plus" = on ] ; then
echo "GSOMEGA/H $lon$lat$npg$coord$plid$lay" | ccc gsomgah sslnsp sstemp ssvort ssdiv gsomeg
              fi
              if [ "$gcm1" = on ] ; then
echo "GSOMEGA/H $lon$lat$npg$coord$plid$lay" | ccc gsomega sslnsp ssvort ssdiv gsomeg
              fi
            fi
echo "GSAPL(W)  $plv       0.0       0.0$coord$plid
$p01$p02$p03$p04$p05$p06$p07$p08$p09$p10$p11$p12$p13$p14$p15$p16
$p17$p18$p19$p20$p21$p22$p23$p24$p25$p26$p27$p28$p29$p30$p31$p32
$p33$p34$p35$p36$p37$p38$p39$p40$p41$p42$p43$p44$p45$p46$p47$p48
$p49$p50$p51$p52$p53$p54$p55$p56$p57$p58$p59$p60$p61$p62$p63$p64
$p65$p66$p67$p68$p69$p70$p71$p72$p73$p74$p75$p76$p77$p78$p79$p80
$p81$p82$p83$p84$p85$p86$p87$p88$p89$p90$p91$p92$p93$p94$p95$p96
$p97$p98$p99$p100" | ccc gsapl gsomeg gslnsp gpw
         fi
                                       # calculate vorticity and divergence
         if [ "$vorstat" = "on" ] ; then
echo "GWTQD.    $lrt$lmt$typ    1" | ccc gwtqd gpu gpv spvort spdiv
           splinv spvort nsppsi
           splinv spdiv  nspchi
           echo "  XLIN.          -1.        0.  PSI" | ccc xlin nsppsi sppsi
           echo "  XLIN.          -1.        0.  CHI" | ccc xlin nspchi spchi
echo "COFAGG.   $lon$lat    0$npg" | ccc cofagg spvort gpvort
echo "COFAGG.   $lon$lat    0$npg" | ccc cofagg spdiv  gpdiv
echo "COFAGG.   $lon$lat    0$npg" | ccc cofagg sppsi  gppsi
echo "COFAGG.   $lon$lat    0$npg" | ccc cofagg spchi  gpchi
         fi
      fi

#   ---------------------------------- spectral pressure case.

      if [ "$datatype" = "specpr" ] ; then
.        spfiles.cdk
                                       # calculate q,t,phi on pressure grid.

echo "SELECT    STEPS $t1 $t2 $t3 LEVS$p01$pmaxl NAME  PHI TEMP SHUM" | ccc select npaksp spphi sptemp spshum
echo "COFAGG.   $lon$lat    0$npg" | ccc cofagg spphi gpphi
echo "COFAGG.   $lon$lat    0$npg" | ccc cofagg sptemp gpt
echo "COFAGG.   $lon$lat    0$npg" | ccc cofagg spshum gpq

                                       # calculate u and v on pressure grid.

echo "SELECT    STEPS $t1 $t2 $t3 LEVS$p01$pmaxl NAME VORT  DIV" | ccc select npaksp spvort spdiv
         cwinds spvort spdiv spbigu spbigv
echo "COFAGG.   $lon$lat    1$npg" | ccc cofagg spbigu gpu
echo "COFAGG.   $lon$lat    1$npg" | ccc cofagg spbigv gpv
                                       # calculate w on pressure grid.

         if [ "$wxstats" = "on" ] ; then
echo "SELECT    STEPS $t1 $t2 $t3 LEVS$p01$pmaxl NAME OMEG" | ccc select npaksp spomeg
echo "COFAGG.   $lon$lat    0$npg" | ccc cofagg spomeg gpw
         fi
                                       # calculate vorticity and divergence
         if [ "$vorstat" = "on" ] ; then
echo "COFAGG.   $lon$lat    0$npg" | ccc cofagg spvort gpvort
echo "COFAGG.   $lon$lat    0$npg" | ccc cofagg spdiv gpdiv
         fi
      fi

#   ---------------------------------- grid pressure case.
      if [ "$datatype" = "gridpr" ] ; then
.        ggfiles.cdk
echo "SELECT    STEPS $t1 $t2 $t3 LEVS$p01$pmaxl NAME TEMP SHUM  PHI
SELECT        U    V" | ccc select npakgg gpt gpq gpphi gpu gpv
         if [ "$wxstats" = "on" ] ; then
echo "SELECT    STEPS $t1 $t2 $t3 LEVS$p01$pmaxl NAME OMEG" | ccc select npakgg gpw
         fi
                                       # calculate vorticity and divergence
         if [ "$vorstat" = "on" -a "$rcm" != "on" ] ; then
echo "GWTQD.    $lrt$lmt$typ    1" | ccc gwtqd  gpu gpv spvort spdiv
echo "COFAGG.   $lon$lat    0$npg" | ccc cofagg spvort gpvort
echo "COFAGG.   $lon$lat    0$npg" | ccc cofagg spdiv  gpdiv
         fi
      fi

#   ---------------------------------- grid sigma case.
      if [ "$datatype" = "gridsig" ] ; then
.        ggfiles.cdk
                                       # calculate q,t,phi on pressure grid.

echo "SELECT.   STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME   ES TEMP LNSP PHIS" | ccc select npakgg gses gstemp gslnsp gsphis
if [[ $issp -gt $isgg ]]; then  ### add by ryj001
  ### For CORDEX, 3D dynamics saved 6-hourly; but lnsp saved 1-hourly
  s1=$(echo $issp|awk '{printf "%9d",$1}')
  s3=$(echo $issp|awk '{printf "%4d",$1}')
  echo "SELECT    STEPS $s1 $t2 $s3 LEVS    1    1 NAME LNSP" | ccc select gslnsp dummy; mv dummy gslnsp
fi

                                       # select RH from gs files
echo "SELECT.   STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME   RH" | ccc select npakgg gsrh

         if [[ $gempsav == on ]] ; then
echo "SELECT.   STEPS $t1 $t2 $t3 LEVS-9001  999 NAME  APT  APM" | ccc select npakgg gsapt gsapm    # 3D pressures on thermodynamic & momentum levels in hPa
         fi

         if [[ $gempsav == on ]] ; then
echo "GSHUMH.   $coord$moist$plid$sref$spow" | ccc gsphumh gses gstemp gslnsp gsapt gsshum gsrhum
echo "GSAPL.    $plv       0.0       0.0$coord$plid
$p01$p02$p03$p04$p05$p06$p07$p08$p09$p10$p11$p12$p13$p14$p15$p16
$p17$p18$p19$p20$p21$p22$p23$p24$p25$p26$p27$p28$p29$p30$p31$p32
$p33$p34$p35$p36$p37$p38$p39$p40$p41$p42$p43$p44$p45$p46$p47$p48
$p49$p50$p51$p52$p53$p54$p55$p56$p57$p58$p59$p60$p61$p62$p63$p64
$p65$p66$p67$p68$p69$p70$p71$p72$p73$p74$p75$p76$p77$p78$p79$p80
$p81$p82$p83$p84$p85$p86$p87$p88$p89$p90$p91$p92$p93$p94$p95$p96
$p97$p98$p99$p100" | ccc gspapl gsshum gslnsp gsapt gpq
echo "GSAPL(RH) $plv       0.0       0.0$coord$plid
$p01$p02$p03$p04$p05$p06$p07$p08$p09$p10$p11$p12$p13$p14$p15$p16
$p17$p18$p19$p20$p21$p22$p23$p24$p25$p26$p27$p28$p29$p30$p31$p32
$p33$p34$p35$p36$p37$p38$p39$p40$p41$p42$p43$p44$p45$p46$p47$p48
$p49$p50$p51$p52$p53$p54$p55$p56$p57$p58$p59$p60$p61$p62$p63$p64
$p65$p66$p67$p68$p69$p70$p71$p72$p73$p74$p75$p76$p77$p78$p79$p80
$p81$p82$p83$p84$p85$p86$p87$p88$p89$p90$p91$p92$p93$p94$p95$p96
$p97$p98$p99$p100" | ccc gspapl gsrh   gslnsp gsapt gprh
         else
echo "GSHUMH.   $coord$moist$plid$sref$spow" | ccc gshumh gses gstemp gslnsp gsshum gsrhum
echo "GSAPL.    $plv       0.0       0.0$coord$plid
$p01$p02$p03$p04$p05$p06$p07$p08$p09$p10$p11$p12$p13$p14$p15$p16
$p17$p18$p19$p20$p21$p22$p23$p24$p25$p26$p27$p28$p29$p30$p31$p32
$p33$p34$p35$p36$p37$p38$p39$p40$p41$p42$p43$p44$p45$p46$p47$p48
$p49$p50$p51$p52$p53$p54$p55$p56$p57$p58$p59$p60$p61$p62$p63$p64
$p65$p66$p67$p68$p69$p70$p71$p72$p73$p74$p75$p76$p77$p78$p79$p80
$p81$p82$p83$p84$p85$p86$p87$p88$p89$p90$p91$p92$p93$p94$p95$p96
$p97$p98$p99$p100" | ccc gsapl gsshum gslnsp gpq
echo "GSAPL(RH) $plv       0.0       0.0$coord$plid
$p01$p02$p03$p04$p05$p06$p07$p08$p09$p10$p11$p12$p13$p14$p15$p16
$p17$p18$p19$p20$p21$p22$p23$p24$p25$p26$p27$p28$p29$p30$p31$p32
$p33$p34$p35$p36$p37$p38$p39$p40$p41$p42$p43$p44$p45$p46$p47$p48
$p49$p50$p51$p52$p53$p54$p55$p56$p57$p58$p59$p60$p61$p62$p63$p64
$p65$p66$p67$p68$p69$p70$p71$p72$p73$p74$p75$p76$p77$p78$p79$p80
$p81$p82$p83$p84$p85$p86$p87$p88$p89$p90$p91$p92$p93$p94$p95$p96
$p97$p98$p99$p100" | ccc gsapl gsrh   gslnsp gprh
         fi
         rm gses gsrhum
         if [ "$gcmtsav" = on ] ; then
            if [[ $gempsav == on ]] ; then
echo "XLIN.      174.52032    287.04 RGAS" | ccc xlin gsshum gsrgasm
echo "TAPHI.    $coord$lay$plid" | ccc tpaphi gstemp gsphis gslnsp gsapt gsapm gsrgasm gsphi
echo "GSAPZ(T)  $plv       0.0    6.5E-3$coord$plid
$p01$p02$p03$p04$p05$p06$p07$p08$p09$p10$p11$p12$p13$p14$p15$p16
$p17$p18$p19$p20$p21$p22$p23$p24$p25$p26$p27$p28$p29$p30$p31$p32
$p33$p34$p35$p36$p37$p38$p39$p40$p41$p42$p43$p44$p45$p46$p47$p48
$p49$p50$p51$p52$p53$p54$p55$p56$p57$p58$p59$p60$p61$p62$p63$p64
$p65$p66$p67$p68$p69$p70$p71$p72$p73$p74$p75$p76$p77$p78$p79$p80
$p81$p82$p83$p84$p85$p86$p87$p88$p89$p90$p91$p92$p93$p94$p95$p96
$p97$p98$p99$p100" | ccc gspaplt gstemp gsphi gsrgasm gsapt gpt gpphi
            else
echo "XLIN.      174.52032    287.04 RGAS" | ccc xlin gsshum gsrgasm
echo "TAPHI.    $coord$lay$plid" | ccc taphi gstemp gsphis gslnsp gsrgasm gsphi
echo "GSAPZ(T)  $plv       0.0    6.5E-3$coord$plid
$p01$p02$p03$p04$p05$p06$p07$p08$p09$p10$p11$p12$p13$p14$p15$p16
$p17$p18$p19$p20$p21$p22$p23$p24$p25$p26$p27$p28$p29$p30$p31$p32
$p33$p34$p35$p36$p37$p38$p39$p40$p41$p42$p43$p44$p45$p46$p47$p48
$p49$p50$p51$p52$p53$p54$p55$p56$p57$p58$p59$p60$p61$p62$p63$p64
$p65$p66$p67$p68$p69$p70$p71$p72$p73$p74$p75$p76$p77$p78$p79$p80
$p81$p82$p83$p84$p85$p86$p87$p88$p89$p90$p91$p92$p93$p94$p95$p96
$p97$p98$p99$p100" | ccc gsaplt gstemp gsphi gsrgasm gslnsp gpt gpphi
            fi
            rm gsshum gsrgasm
         else
echo "SELECT.   STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME  PHI" | ccc select npakgg gsphi
echo "GSAPZ(T)  $plv       0.0    6.5E-3$coord$plid
$p01$p02$p03$p04$p05$p06$p07$p08$p09$p10$p11$p12$p13$p14$p15$p16
$p17$p18$p19$p20$p21$p22$p23$p24$p25$p26$p27$p28$p29$p30$p31$p32
$p33$p34$p35$p36$p37$p38$p39$p40$p41$p42$p43$p44$p45$p46$p47$p48
$p49$p50$p51$p52$p53$p54$p55$p56$p57$p58$p59$p60$p61$p62$p63$p64
$p65$p66$p67$p68$p69$p70$p71$p72$p73$p74$p75$p76$p77$p78$p79$p80
$p81$p82$p83$p84$p85$p86$p87$p88$p89$p90$p91$p92$p93$p94$p95$p96
$p97$p98$p99$p100" | ccc gsapzl gsphi gsphis gslnsp gpphi gpt
         fi
                                       # calculate msl pressure

         if [[ $gempsav == on ]] ; then
echo "GSMSLPH       6.5E-3    0$lay$coord$plid" | ccc gspmslph gstemp gslnsp gsphis gsapt pmsl
         else
echo "GSMSLPH       6.5E-3    0$lay$coord$plid" | ccc gsmslph gstemp gslnsp gsphis pmsl
         fi
         rm gsphi gstemp gsphis
                                       # calculate u and v on pressure grid.
echo "SELECT    STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME    U    V" | ccc select npakgg gsu gsv
         if [[ $gempsav == on ]] ; then
echo "GSAPL.    $plv       0.0       0.0$coord$plid
$p01$p02$p03$p04$p05$p06$p07$p08$p09$p10$p11$p12$p13$p14$p15$p16
$p17$p18$p19$p20$p21$p22$p23$p24$p25$p26$p27$p28$p29$p30$p31$p32
$p33$p34$p35$p36$p37$p38$p39$p40$p41$p42$p43$p44$p45$p46$p47$p48
$p49$p50$p51$p52$p53$p54$p55$p56$p57$p58$p59$p60$p61$p62$p63$p64
$p65$p66$p67$p68$p69$p70$p71$p72$p73$p74$p75$p76$p77$p78$p79$p80
$p81$p82$p83$p84$p85$p86$p87$p88$p89$p90$p91$p92$p93$p94$p95$p96
$p97$p98$p99$p100" | ccc gspapl gsu gslnsp gsapm gpu
echo "GSAPL.    $plv       0.0       0.0$coord$plid
$p01$p02$p03$p04$p05$p06$p07$p08$p09$p10$p11$p12$p13$p14$p15$p16
$p17$p18$p19$p20$p21$p22$p23$p24$p25$p26$p27$p28$p29$p30$p31$p32
$p33$p34$p35$p36$p37$p38$p39$p40$p41$p42$p43$p44$p45$p46$p47$p48
$p49$p50$p51$p52$p53$p54$p55$p56$p57$p58$p59$p60$p61$p62$p63$p64
$p65$p66$p67$p68$p69$p70$p71$p72$p73$p74$p75$p76$p77$p78$p79$p80
$p81$p82$p83$p84$p85$p86$p87$p88$p89$p90$p91$p92$p93$p94$p95$p96
$p97$p98$p99$p100" | ccc gspapl gsv gslnsp gsapm gpv
         else
echo "GSAPL.    $plv       0.0       0.0$coord$plid
$p01$p02$p03$p04$p05$p06$p07$p08$p09$p10$p11$p12$p13$p14$p15$p16
$p17$p18$p19$p20$p21$p22$p23$p24$p25$p26$p27$p28$p29$p30$p31$p32
$p33$p34$p35$p36$p37$p38$p39$p40$p41$p42$p43$p44$p45$p46$p47$p48
$p49$p50$p51$p52$p53$p54$p55$p56$p57$p58$p59$p60$p61$p62$p63$p64
$p65$p66$p67$p68$p69$p70$p71$p72$p73$p74$p75$p76$p77$p78$p79$p80
$p81$p82$p83$p84$p85$p86$p87$p88$p89$p90$p91$p92$p93$p94$p95$p96
$p97$p98$p99$p100" | ccc gsapl gsu gslnsp gpu
echo "GSAPL.    $plv       0.0       0.0$coord$plid
$p01$p02$p03$p04$p05$p06$p07$p08$p09$p10$p11$p12$p13$p14$p15$p16
$p17$p18$p19$p20$p21$p22$p23$p24$p25$p26$p27$p28$p29$p30$p31$p32
$p33$p34$p35$p36$p37$p38$p39$p40$p41$p42$p43$p44$p45$p46$p47$p48
$p49$p50$p51$p52$p53$p54$p55$p56$p57$p58$p59$p60$p61$p62$p63$p64
$p65$p66$p67$p68$p69$p70$p71$p72$p73$p74$p75$p76$p77$p78$p79$p80
$p81$p82$p83$p84$p85$p86$p87$p88$p89$p90$p91$p92$p93$p94$p95$p96
$p97$p98$p99$p100" | ccc gsapl gsv gslnsp gpv
         fi
         rm gsu gsv

                                       # calculate w on pressure grid.
         if [[ $gempsav == on ]] ; then
echo "SELECT.   STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME OMEG" | ccc select npakgg gsw
echo "GSAPL.    $plv       0.0       0.0$coord$plid
$p01$p02$p03$p04$p05$p06$p07$p08$p09$p10$p11$p12$p13$p14$p15$p16
$p17$p18$p19$p20$p21$p22$p23$p24$p25$p26$p27$p28$p29$p30$p31$p32
$p33$p34$p35$p36$p37$p38$p39$p40$p41$p42$p43$p44$p45$p46$p47$p48
$p49$p50$p51$p52$p53$p54$p55$p56$p57$p58$p59$p60$p61$p62$p63$p64
$p65$p66$p67$p68$p69$p70$p71$p72$p73$p74$p75$p76$p77$p78$p79$p80
$p81$p82$p83$p84$p85$p86$p87$p88$p89$p90$p91$p92$p93$p94$p95$p96
$p97$p98$p99$p100" | ccc gspapl gsw gslnsp gsapt gpw
         else
echo "SELECT.   STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME OMEG" | ccc select npakgg gsw
echo "GSAPL.    $plv       0.0       0.0$coord$plid
$p01$p02$p03$p04$p05$p06$p07$p08$p09$p10$p11$p12$p13$p14$p15$p16
$p17$p18$p19$p20$p21$p22$p23$p24$p25$p26$p27$p28$p29$p30$p31$p32
$p33$p34$p35$p36$p37$p38$p39$p40$p41$p42$p43$p44$p45$p46$p47$p48
$p49$p50$p51$p52$p53$p54$p55$p56$p57$p58$p59$p60$p61$p62$p63$p64
$p65$p66$p67$p68$p69$p70$p71$p72$p73$p74$p75$p76$p77$p78$p79$p80
$p81$p82$p83$p84$p85$p86$p87$p88$p89$p90$p91$p92$p93$p94$p95$p96
$p97$p98$p99$p100" | ccc gsapl gsw gslnsp gpw
         fi
         rm gsw
                                       # calculate vorticity and divergence
         if [ "$vorstat" = "on" -a "$rcm" != "on" ] ; then
echo "GWTQD.    $lrt$lmt$typ    1" | ccc gwtqd  gpu gpv spvort spdiv
echo "COFAGG.   $lon$lat    0$npg" | ccc cofagg spvort gpvort
echo "COFAGG.   $lon$lat    0$npg" | ccc cofagg spdiv  gpdiv
         fi
      fi

#   ---------------------------------- time-averaged statistics (skip if gpintstat=off)

      if [ "$gpintstat" != "off" ] ; then

#   ---------------------------------- get time-averaged beta for zonal averaging

      lzon="    0"
      if [ "$rcm" != "on" ] ; then
        lzon="    1"
#                                      try first to access it directly

        access beta ${flabel}_gptbeta na
        if [ ! -s beta ] ; then
#                                      if not available, get it from gp file
           access oldgp ${flabel}gp
           echo "XFIND.        $d" | ccc xfind oldgp beta
        fi
      fi

#                                      set vertical velocity switch
      lw="    0"
      if [ "$wxstats" = "on" ]; then
        lw="    1"
      fi

#   ---------------------------------- compute gp statistics
      luvw="    1"
      lbeta="    1"
      if [ "$stat2nd" != "off" ] ; then
        lstat="    1"
        ldt="    1"
      else
        lstat="    0"
        ldt="    0"
      fi
      echo "          $luvw$lw$ldt$lzon$lbeta$lstat$delt" > .ic_gpxstat
      if [ "$vorstat" = "on" -a "$rcm" != "on" ] ; then
        gpxstat beta gpu gpv gpw gpq gpt gpphi gpvort new_gp new_xp input=.ic_gpxstat
      else
        gpxstat beta gpu gpv gpw gpq gpt gpphi new_gp new_xp input=.ic_gpxstat
      fi

#   ---------------------------------- Relative Humidity

      if [ "$gsrh" = "on" ] ; then
         ln -s gprh RH
         statsav RH new_gp new_xp $stat2nd
      fi

#   ---------------------------------- compute PMSL statistics

      ln -s pmsl PMSL
      statsav PMSL new_gp new_xp $stat2nd

#   ---------------------------------- compute GEM output PMSL statistics
     ###ryj001: PMSL SAVED IN GS FILE IS PN OUTPUT FROM GEM; HERE WE RENAME IT AS PNSL to avoid conflict with GCM diag PMSL
     if [[ -e npakgg/PMSL ]]; then
       echo "XLIN.           0.01           PNSL" |ccc xlin npakgg/PMSL PNSL
       statsav PNSL new_gp new_xp $stat2nd
     fi

#   ---------------------------------- save results.

      if [ ! -s oldgp ] ; then
        access oldgp ${flabel}gp
      fi
      xjoin  oldgp new_gp newgp
      save   newgp ${flabel}gp
      delete oldgp

      if [ "$rcm" != "on" ] ; then
        access oldxp ${flabel}xp
        xjoin  oldxp new_xp newxp
        save   newxp ${flabel}xp
        delete oldxp
      fi

      fi # gpintstat

#   ---------------------------------- save qp files.

      save gpq   ${flabel}_gpq
      save gpphi ${flabel}_gpz
      save gpt   ${flabel}_gpt
      save gpu   ${flabel}_gpu
      save gpv   ${flabel}_gpv
      if [ "$wxstats" = "on" ] ; then
        save gpw   ${flabel}_gpw
      fi
      if [ "$vorstat" = "on" -a "$rcm" != "on" ] ; then
         save gpvort ${flabel}_gpvr
         save gpdiv  ${flabel}_gpdiv
         if [ "$datatype" = "specsig" ] ; then
            save gppsi ${flabel}_gppsi
            save gpchi ${flabel}_gpchi
         fi
      fi
      if [ "$gsrh" = "on" ] ; then
         save gprh  ${flabel}_gprh
      fi
      save pmsl  ${flabel}_pmsl

#   ---------------------------------- save gs files (only for specsig)

      if [ "$datatype" = "specsig" -a "$gssave" = "on" ] ; then
        save gsshum ${flabel}_gsq
        save gsphi  ${flabel}_gsz
        save gstemp ${flabel}_gst
        save gsu    ${flabel}_gsu
        save gsv    ${flabel}_gsv
        if [ "$wxstats" = "on" ] ; then
          save gsomeg ${flabel}_gsw
        fi
      fi

#   ---------------------------------- save sp vort, div, q, t and z.
      if [ "$spsave" = "on" -a \( "$datatype" = "specsig" -o "$datatype" = "specpr" \) ] ; then
        if [ "$datatype" = "specsig" ] ; then
echo "GGACOF    $lrt$lmt$typ" | ccc ggacof gpq   spshum
echo "GGACOF    $lrt$lmt$typ" | ccc ggacof gpt   sptemp
echo "GGACOF    $lrt$lmt$typ" | ccc ggacof gpphi spphi
        fi
        if [ "$vorstat" = "on" ] ; then
        save spvort ${flabel}_spvr
        save spdiv  ${flabel}_spdiv
        fi
        save spshum ${flabel}_spq
        save sptemp ${flabel}_spt
        save spphi  ${flabel}_spz
      fi
