#!/bin/sh
#
# This script is only included in the merged_diag_script grouping when
# gas-phase chemistry is run in MAM mode. BW, June 2023
#
# Amendments to the original: source the file of switches
#
# ERTELPV - compute Ertel potential vorticity from model files.
#
# ---- Calculation of variables required by selstep and daily save 
#         from available CanESM5 variables, D. Plummer October 2023
# ---- converted to shell script, D. Plummer January 2023
# ---- created for CMAM  S.R.Beagley Jan 98.  
# ---- modified from jnk nov 8/93
#
# ---------------------------------------------------------------
# Pre-requisites:
#     geqtz.dk (now pqtphirho.sh) to provide Z on eta surfaces (gephifile=on)
# 
# CONDEF parameters:
#     datatype (diag3) must be set.
#     diagnostic suitable for specsig ONLY.
#     gcmtsav,
#     etalvs  = if +specsig,
#                  + for input on eta levels
#                  sigma otherwise,
#   ---------------------------------------------------------------
# 
# PARMSUB parameters:
#     model1,model2...,
#     flabel,
#     run,
#     days,
#     t1, t2, t3, lon, lat,
#     plv, p01-p50(if,+preslvsonly),
#     lxp, kax, kind -(for  zxplot),
#     etatop,r,r_1-(if,+etalvsonly)
#     r=exponent in expression for p(eta), r_1=r-1;
# -----------------------------------------------------------------------------

# Source the list of on/off switches
source ${CANESM_SRC_ROOT}/CanDIAG/diag4/gaschem_diag_switches.sh

#
# -- variables for selstep
yearoff=`echo "$year_offset"  | $AWK '{printf "%4d", $0+1}'`
deltmin=`echo "$delt"         | $AWK '{printf "%5d", $0/60.}'`

# -----------------------------------------------------------------------------

if [ "$datatype" = "gridpr" ] ; then
  echo " JOB not setup for gridpr option"
  exit
fi

# Spectral input case
. spfiles.cdk

# Pre-computed fields of p, dp/de and phi on model levels from pqtphirho script
access gepres ${flabel}_gep
access gedpde ${flabel}_gedpde
access gez    ${flabel}_gez

if [ "$datatype" = "specsig" ] ; then
  echo "SELECT.   STEPS $t1 $t2 $t3 LEVS    1    1 NAME PHIS LNSP" | ccc select npaksp sphis slnsp
  if [ "$gcmtsav" != on ] ; then
    echo "SELECT          $t1 $t2 $t3 LEVS-9001 1000       PHI VORT  DIV" | ccc select npaksp spphi spvort spdiv 
    ctemps sphis  spphi sptemp 
    rm sphis spphi
  else
    echo "SELECT          $t1 $t2 $t3 LEVS-9001 1000      TEMP VORT  DIV" | ccc select npaksp sptemp spvort spdiv 
  fi
  echo "COFAGG    $lon$lat    0    1" | ccc cofagg slnsp glnsp 
  rm slnsp
fi

# Pressure levels case.
if [ "$datatype" = "specpr" ] ; then
  echo "SELECT          $t1 $t2 $t3 LEVS-9001 1000 NAME TEMP VORT  DIV" | ccc select npaksp sptemp spvort spdiv 
fi
rm npaksp

# Computations common to sigma/eta and pressure case.
echo "COFAGG    $lon$lat    0    1" > .cofagg_input_card1
cofagg spvort ggvort input=.cofagg_input_card1
cofagg sptemp ggtemp input=.cofagg_input_card1
cwinds spvort spdiv spu spv

echo "COFAGG    $lon$lat    1    1" > .cofagg_input_card2
cofagg spu ggu input=.cofagg_input_card2
cofagg spv ggv input=.cofagg_input_card2
rm spvort sptemp spdiv spu spv
rm .cofagg_input_card1 .cofagg_input_card2

echo "XLIN              0.        1." | ccc xlin gepres one

# Computations common to all cases
# 
# Potential temperature, theta.

echo "XLINP0            0.   101320." | ccc xlin gepres p0 
div p0 gepres p0opres 
echo "FPOWGAMMA      0.286" | ccc fpow p0opres ratio 
mlt ratio ggtemp ggtheta 
rm p0 gepres p0opres ratio
 
# Horizontal gradient of theta.
ggdlon ggtheta dthdlam 
ggdlat ggtheta dthdphi 
echo "XLININVRAD  1.569E-7        0." | ccc xlin dthdlam dthdx 
echo "XLININVRAD  1.569E-7        0." | ccc xlin dthdphi dthdy 
rm dthdlam dthdphi

# Vertical derivatives of theta, u and v.
dxdp ggtheta dthdht 
dxdp ggu dudht 
dxdp ggv dvdht 
rm ggtheta ggu ggv

if [ "$datatype" = "specsig" ] ; then

# Vertical sig/eta derivative: use dxdp, which multiplies input levels by 100
# (hPa to pa) to obtain d/d(sig/eta). Where 0<sig,eta<1, multiply output from
# dxdp by 1.e5.

  echo "XLIN            1.E5        0." > .xlin_input_card
  xlin dthdht dthdeta input=.xlin_input_card
  xlin dudht dudeta input=.xlin_input_card
  xlin dvdht dvdeta input=.xlin_input_card
  mv dthdeta dthdht
  mv dudeta dudht
  mv dvdeta dvdht
fi

# Vertical component of absolute vorticity.
echo "GGTRIGF     1.454E-4  SIN    1" | ccc ggtrig one f 
add ggvort f ggavort 
rm one ggvort f
 
# Ertel's PV.

mlt dvdht dthdx pvsx 
mlt dudht dthdy pvsy 
mlt ggavort dthdht pvsz 
rm dudht dvdht dthdx dthdy dthdht ggavort

sub pvsx pvsy pvs1 
sub pvs1 pvsz pvog 
rm pvsx pvsy pvs1 pvsz

if [ "$datatype" = specsig ] ; then
  div pvog gedpde pvograv 
  mv pvograv pvog 
  rm gedpde
fi

# Multiply by gravitational acceleration.
echo "XLINGRAV       9.806        0." | ccc xlin pvog pv_gs 
rm pvog


# ******************* Options for Output Levels *********************

# Isobaric coordinates.
echo "GSAPL.    $plv       0.0       0.0$coord$plid
$p01$p02$p03$p04$p05$p06$p07$p08$p09$p10$p11$p12$p13$p14$p15$p16
$p17$p18$p19$p20$p21$p22$p23$p24$p25$p26$p27$p28$p29$p30$p31$p32
$p33$p34$p35$p36$p37$p38$p39$p40$p41$p42$p43$p44$p45$p46$p47$p48
$p49$p50$p51$p52$p53$p54$p55$p56$p57$p58$p59$p60$p61$p62$p63$p64
$p65$p66$p67$p68$p69$p70$p71$p72$p73$p74$p75$p76$p77$p78$p79$p80
$p81$p82$p83$p84$p85$p86$p87$p88$p89$p90$p91$p92$p93$p94$p95$p96
$p97$p98$p99$p100" | ccc gsapl pv_gs glnsp pv_gp
timavg pv_gp pv_gp_tav

# Isentropic coordinates.
echo "GSATHL       47        0.        0.$coord$plid
9000 8000 7000 6000 5000 4000 3050 2950 2850 2750 2650 2550 2450 2350 2250 2150
2050 1950 1850 1750 1650 1550 1450 1350 1250 1150 1050  950  850  840  750  650
 550  525  500  480  475  450  430  410  380  350  330  310  290  270  250" | ccc gsathl pv_gs glnsp ggtemp pv_gth
echo "GSATHL       47        0.        0.$coord$plid
9000 8000 7000 6000 5000 4000 3050 2950 2850 2750 2650 2550 2450 2350 2250 2150
2050 1950 1850 1750 1650 1550 1450 1350 1250 1150 1050  950  850  840  750  650
 550  525  500  480  475  450  430  410  380  350  330  310  290  270  250" | ccc gsathl gez glnsp ggtemp Z_gth
rm gez glnsp ggtemp 

ggdlat pv_gth dpvdlat_gth

# Latitude gradient per radian..

# Time-average PV.
timavg pv_gth pv_gth_tav 
timavg Z_gth  Z_gth_tav

timavg pv_gth_tav  pv_gth_tav0
echo "NEWNAM       PV" | ccc newnam pv_gth_tav0 pv_gth_tav1

# *********************** Save Output File **************************
#
# (1) save unaveraged PV data on P surfaces
if [ "$ipsave" = "on" ] ; then
  access oldip ${flabel}ip     na
  echo "XSAVE.        INSTANTANEOUS ERTELS PV ON PRESSURE SURFACE
NEWNAM        PV" | ccc xsave oldip pv_gp newip
  save newip ${flabel}ip
  delete oldip                 na
  rm newip
fi

# (2) save unaveraged PV data on model and theta levels
if [ "$iesave" = "on" ] ; then
  if [ "$ieslct" = "on" ] ; then
    echo "C*TSTEP   $deltmin     ${yearoff}010100          MODEL     YYYYMMDDHH" > ic.tstep_model
    selstep  pv_gs   PV_ie input=ic.tstep_model
    selstep  pv_gth  PVgth_ie input=ic.tstep_model

    access oldie ${flabel}ie      na
    echo "XSAVE.        INSTANTANEOUS ERTELS PV ON ETA SURFACE
NEWNAM        PV
XSAVE.        INSTANTANEOUS ERTELS PV ON THETA SURFACE
NEWNAM        PV" | ccc xsave oldie PV_ie PVgth_ie newie
    save   newie  ${flabel}ie
    delete oldie                  na
  fi
  if [ "$ieslct" = "off" ] ; then
    access oldie ${flabel}ie      na
    echo "XSAVE.        INSTANTANEOUS ERTELS PV ON ETA SURFACE
NEWNAM        PV
XSAVE.        INSTANTANEOUS ERTELS PV ON THETA SURFACE
NEWNAM        PV" | ccc xsave oldie pv_gs pv_gth newie
    save   newie  ${flabel}ie
    delete oldie                  na
  fi
fi

# (3) save time averaged PV (on p-levels only) fields to gp file.
access oldgp ${flabel}gp     na
echo "XSAVE.        TIME AVG ERTELS PV ON P SURFACE
NEWNAM        PV" | ccc xsave oldgp pv_gp_tav newgp
save   newgp ${flabel}gp
delete oldgp                 na
rm newgp

# (4) temporary save of unaveraged fields
if [ "$dailysv" = on ] ; then
  echo "NEWNAM       PV" | ccc newnam pv_gth ggout 
  save ggout ${flabel}_gthpv
  echo "NEWNAM       PV" | ccc newnam pv_gp PVp
  save PVp   ${flabel}_gppv
  save Z_gth ${flabel}_gthz
fi
