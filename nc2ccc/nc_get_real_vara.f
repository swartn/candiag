      subroutine nc_get_real_vara(fid,vid,start,count,ncdata,
     &                         total_sz,ndims,xtype)
c***********************************************************************
c Read an array section from a netcdf variable of any type and return the
c data in a real array
c***********************************************************************
      implicit none

      integer fid, vid, start(ndims), count(ndims), total_sz
      real ncdata(total_sz)
      integer :: ndims, xtype

      include "netcdf.inc"

      !--- local
      integer :: ret

      !--- pointers for data read from the netcdf file
      integer(1), pointer :: byte_vals(:)
      integer(2), pointer :: short_vals(:)
      integer(4), pointer :: int_vals(:)
      real(8),    pointer :: double_vals(:)

      !--- allocate space and read in data from the netcdf file
      if (xtype.eq.NF_BYTE) then
        allocate(byte_vals(total_sz))
        ret = nf_get_vara_int1(fid,vid,start,count,byte_vals)
        call nc_handle_err ("nf_get_vara_int1", ret, fid)
        ncdata(1:total_sz) = byte_vals(1:total_sz)
        deallocate(byte_vals)
      else if (xtype.eq.NF_SHORT) then
        allocate(short_vals(total_sz))
        ret = nf_get_vara_int2(fid,vid,start,count,short_vals)
        call nc_handle_err ("nf_get_vara_int2", ret, fid)
        ncdata(1:total_sz) = short_vals(1:total_sz)
        deallocate(short_vals)
      else if (xtype.eq.NF_INT) then
        allocate(int_vals(total_sz))
        ret = nf_get_vara_int(fid,vid,start,count,int_vals)
        call nc_handle_err ("nf_get_vara_int", ret, fid)
        ncdata(1:total_sz) = int_vals(1:total_sz)
        deallocate(int_vals)
      else if (xtype.eq.NF_FLOAT) then
        ret = nf_get_vara_real(fid,vid,start,count,ncdata)
        call nc_handle_err ("nf_get_vara_real", ret, fid)
      else if (xtype.eq.NF_DOUBLE) then
        allocate(double_vals(total_sz))
        ret = nf_get_vara_double(fid,vid,start,count,double_vals)
        call nc_handle_err ("nf_get_vara_double", ret, fid)
        ncdata(1:total_sz) = double_vals(1:total_sz)
        deallocate(double_vals)
      else
        write(6,*)'nc_get_real_vara: xtype is out of range.',
     &           ' xtype = ',xtype
      endif

      return
      end
