      subroutine nc_handle_err(str, status, fileid)
c***********************************************************************
c Handle error conditions returned from calls to netcdf routines.
c $Id: nc_handle_err.f,v 1.1 2005/06/22 18:21:32 acrnrls Exp $
c***********************************************************************
      include "netcdf.inc"

      !----- input -----
      character(*), intent(in) :: str     ! Information string to print on error
      integer*4, intent(in) :: status       ! return code from nf_* call
      integer, intent(in) :: fileid       ! netCDF file id from last nf_* call

      !----- local -----
      integer ret

      !----- If no error, do nothing -----
      if (status.eq.NF_NOERR) return

      print *, trim(str)
      print *, '***** netCDF error: ', nf_strerror (status),' *****'
      ret = nf_close (fileid)
      stop
      end
