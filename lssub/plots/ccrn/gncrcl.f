      SUBROUTINE GNCRCL(XCNTR, YCNTR, RAD, NPTS, XCOORD, YCOORD)
 
C     * AUG 23/07 - B.MIVILLE
C     * This function generates the coordinates for a circle with
C     * center at XCNTR, YCNTR, and a radius of RAD.  There are
C     * NPTS in the circle and the coordinates are returned in the
C     * arrays XCOORD and YCOORD.

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      INTEGER NPTS
      REAL XCNTR, YCNTR, RAD, XCOORD(NPTS), YCOORD(NPTS)
C-----------------------------------------------------------------------------
C
C     * Compute number of radians per degree
C
      RADPDG = 2.*3.14159/360.
C
C     * Initialize the angle
C
       ANGLE = 0.
C
C     * Calculate the change in angle (360./number of points in circle)
C
       DELTA = 360./(NPTS-1)
C
C     * Convert to radians
C
       DELTA = DELTA * RADPDG
C
C     * Calculate each coordinate
C
       DO 10 I=1,NPTS
          XCOORD(I) = RAD * (COS(ANGLE)) + XCNTR
          YCOORD(I) = RAD * (SIN(ANGLE)) + YCNTR
          ANGLE = ANGLE + DELTA
 10    CONTINUE
C
      RETURN
      END
