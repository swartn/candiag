      SUBROUTINE GLOAVG(NF,SFMEAN,MAXX,OKOK)

C     * AUG 23/07 - B.MIVILLE
C     * This subroutine reads the surface mean as the first value from a field 
C     * produced by "globavg" prpogram and returns it in SFMEAN.
C     * NF is the input file unit number. One record is read from NF per call.
C     * Note: No rewind on NF is performed.
C

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      COMMON/F/F(1)
      COMMON/ICOM/IBUF(8),IDAT(1)
      LOGICAL OK,OKOK
C-----------------------------------------------------------------------------
C
C     * READ IN THE FIRST FIELD IN CHAMP
C
      CALL GETFLD2 (NF,F,-1,-1,-1,-1,IBUF,MAXX,OK)
      OKOK = OK
      IF (.NOT.OK) RETURN
      SFMEAN=F(1)
C
      RETURN
      END
