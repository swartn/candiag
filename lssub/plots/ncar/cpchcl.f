      SUBROUTINE CPCHCL (IFLG)

C     * MAR 11/2004 - M.BERKLEY (MODIFIED for CCCMA)
C
C This routine is a dummy.  It is called just before and just after each
C contour line is drawn.  A user version may be substituted to change
C dash pattern, color, and/or line width.
C
C IFLG is +1 if a contour line is about to be drawn, -1 if a contour
C line has just been drawn.
C
C When CPCHCL is called, the internal parameter 'PAI' will have been
C set to the index of the appropriate contour level.  Thus, parameters
C associated with that level may easily be retrieved by calls to CPGETx.
C
      IMPLICIT NONE

      INTEGER IFLG

      LOGICAL DONEINIT
      DATA DONEINIT/.FALSE./
      SAVE DONEINIT

      CHARACTER*16 CPACDASHPAT

      INTEGER IPASS
      DATA IPASS/0/
      SAVE IPASS

      INTEGER LSTRBEG,LSTREND
      EXTERNAL LSTRBEG,LSTREND

      COMMON /SMFLAG/ IOFFS
      COMMON /INTPR/ IPAU,FPART,TENSN,NP,SMALL,L1,ADDLR,ADDTB,
     1     MLLINE,ICLOSE
      COMMON /DASHD1/ ISL,L,ISIZE,IP(100),NWDSM1,IPFLAG(100),
     1     MNCSTR,IGP
      INTEGER IOFFS
      INTEGER IPAU,NP,L1,MLLINE,ICLOSE
      REAL FPART,TENSN,SMALL,ADDLR,ADDTB
      INTEGER ISL,L,ISIZE,IP,NWDSM1,IPFLAG,MNCSTR,IGP

      IF(.NOT.DONEINIT) THEN
         CALL RANDOM_SEED
         DONEINIT=.TRUE.
      ENDIF

      IF(IFLG.GT.0) THEN

         IPASS=IPASS+1

C!!!         CALL CPGETC('CTM',CPACDASHPAT)
C!!!         WRITE(*,*) CPACDASHPAT

C!!!         IF(CPACDASHPAT.NE.'$$$$$$$$$$$$$$$$') THEN
C!!!            CALL DASHDB(B"1111110000000000")
C!!!            write(*,*) 'FOUND IT'
C!!!         else
C!!!            CALL DASHDB(B"1111111111111111")
C!!!         ENDIF

C!!!         WRITE(*,*)

      ENDIF

      RETURN
C
      END
