      SUBROUTINE CONLS(FLOW, FHIGH, FINC, NZERO, PB, FHIGHN, FLOWN)

C     * OCT 19/2007 - F.MAJAESS (CORRECT AND CLEAN THE CODE)
C     * MAY 13/2004 - F.MAJAESS (ENSURE "ITYPE" CHECK IS DONE AS INTEGER)
C     * MAR 11/2004 - M.BERKLEY (MODIFIED for CCCMA)

C CONtour Level Selection routine.
C This routine sets Conpack parameters.

      REAL FLOW, FHIGH, FINC
      REAL FHIGHN, FLOWN
      LOGICAL NZERO,PB
C
      CHARACTER*4 ATYPE, CSPPL
      INTEGER*4 ITYPE, ISPPL
C ITYPE is used by all plotting programs.
      COMMON /CCCPLOT/ ITYPE

      INTEGER NCL, I, NCIT
      PARAMETER (NCIT = 10)
      REAL FLVAL,TOL, TOLN, TOLT, TOLV
      PARAMETER (TOL = 1.D-6, TOLT = 1.D-6, TOLV=1.0D-20, TOLA = 5.D-6)
      INTEGER MAJLW,MINLW,LOWMKR,HIMKR,INZERO
      LOGICAL LAB_FLAG
      CHARACTER*10 LLTXT

      INTEGER LSTRBEG,LSTREND
      EXTERNAL LSTRBEG,LSTREND

      COMMON /CONCCC/MAJLW,MINLW,LOWMKR,HIMKR,INZERO
      EQUIVALENCE (CSPPL,ISPPL)

C     ARBITRARY LINE CONTOURS
      INTEGER NIPATS
      PARAMETER (NIPATS = 28)
      INTEGER NPATS,IARBI
      REAL ZLEVS(NIPATS), CWM
      COMMON /ARBI/ ZLEVS,NPATS,IARBI,CWM
      INTEGER IZ,IC,IZLEV
      REAL RZLEV,DIF
C     ----------------------------------------------------

      CSPPL='SPPL'
      
      IF (        IARBI.GT.0 
     1    .AND. ( NPATS.LE.0 .OR. NPATS.GT.NIPATS) ) THEN

          WRITE(6,*) '# ERROR: INVALID NPATS (=',NPATS,') VALUE #'
          CALL                                    PXIT('CONLS',-1)

      ENDIF

      CALL CPGETR('ZMN - Z Minimum Value',ZMN)
      CALL CPGETR('ZMX - Z Maximum Value',ZMX)

C See if there are too many contours under given card data
C
      IF(IARBI.GT.0 ) THEN
         FHIGHN=ZLEVS(1)
         FLOWN=ZLEVS(1)
         IF ( NPATS.GT.1) THEN
          DO I=2,NPATS
            FHIGHN=AMAX1(FHIGHN,ZLEVS(I))
            FLOWN =AMIN1(FLOWN,ZLEVS(I))
          ENDDO
         ENDIF
         FINC=ABS((FHIGHN-FLOWN)/NPATS)
         NCL=NPATS
C
      ELSEIF (FINC.GE.TOL .AND. FLOW.LT.FHIGH) THEN
         NCL = INT((FHIGH - FLOW)/FINC) + 1
         IF (NCL.LE.256) THEN
            FLOWN = FLOW
            FHIGHN = FHIGH
         ELSE
            IF((FHIGH.LT.ZMN).AND.(FLOW.GT.ZMX)) THEN
               WRITE(6,*) 'ZMN: ', MINVAL, 'ZMX: ',MAXVAL
               WRITE(6,*) '###############################'
               WRITE(6,*) '## ERROR - TOO MANY CONTOURS ##'
               WRITE(6,*) '###############################'
               CALL                               PXIT('CONLS',-2)
            ELSE
               FLOWN = MAX(FLOW,ZMN)
               FHIGHN = MIN(FHIGH,ZMX)
            ENDIF
            TOLN = FINC/10.0
            IF(MOD(FLOW,FINC) .LT. TOLN) THEN
               FLOWN = INT(FLOWN/FINC)*FINC
               FHIGHN = INT(FHIGHN/FINC)*FINC+1
            ENDIF
            WRITE(6,*) 'FLOW RESET FROM', FLOW, 'TO ', FLOWN
            WRITE(6,*) 'FHIGH RESET FROM', FHIGH, 'TO ', FHIGHN
            NCL = INT((FHIGHN - FLOWN)/FINC) + 1
            IF(NCL.GT.256) THEN
              WRITE(6,*) '###########################################'
              WRITE(6,*) '# ERROR: FINC TOO SMALL TOO MANY CONTOURS #'
              WRITE(6,*) '###########################################'
              CALL                                PXIT('CONLS',-3)
            ENDIF
         ENDIF
      ELSE
         FLOWN = INT(MAX(FLOW,ZMN))
         FHIGHN = INT(MIN(FHIGH,ZMX))+1
      ENDIF
C
      CALL CPSETR('HLW',.002)
      CALL CPSETR('LLW',.002)
      IF (ITYPE .EQ. ISPPL) THEN
        LAB_FLAG=NZERO
        CALL CPSETR('HLS', .010)
      ELSE
        CALL CPSETR('HLS', .012)
      ENDIF
      CALL CPSETI('ILC',1)
      CALL CPSETC('ILT', '  ')
      CALL CPSETR('LLS - LINE LABEL SIZE',.012)
      CALL CPSETI('LLP - LINE LABEL POSITIONING',-3)
      CALL CPSETR('RC1',.10)
      CALL CPSETR('RC2',.32)
      CALL CPSETR('RC3',.05)
      CALL CPSETI('RWC',1000)
      CALL CPSETI('RWG',10000)
      CALL CPSETR('PC1',2.0)
      CALL CPSETR('PC2',4.0)
      CALL CPSETR('PC3',90.0)
      CALL CPSETR('PC4',0.05)
      CALL CPSETR('PC5',0.15)
      CALL CPSETR('PC6',0.3)
      CALL CPSETR('PW1',2.0)
      CALL CPSETR('PW2',1.0)
      CALL CPSETR('PW3',1.0)
      CALL CPSETR('PW4',1.0)
      CALL CPSETR('HLW',.004)
      CALL CPSETI('NOF',1)
      CALL CPSETI('NLZ',0)
C
      IF(IARBI.GT.0) THEN
C     
         NCL = NPATS
         CALL CPSETI('CLS - CONTOUR LEVEL SELECTION FLAG',0)
         CALL CPSETI('NCL - NUMBER OF CONTOUR LEVELS', NCL)
         DO I = 1, NCL
            FLVAL=ZLEVS(I)
            CALL CPSETI('PAI - PARAMETER ARRAY INDEX', I)
            CALL CPSETR('CLV - CONTOUR LEVEL VALUES',FLVAL)
C
            IZZ=0
            IF((FLVAL.EQ.0.0).OR.(ABS(FLVAL).LE.TOL)) THEN
               WRITE(LLTXT,2002)
            ELSEIF(ABS(FLVAL).GT.9999..OR.ABS(FLVAL).LT.0.001) 
     1              THEN
               WRITE(LLTXT,2020) FLVAL            
            ELSE
               FVAL=ABS(1.0D0*FLVAL)+1.D0*TOLA
               FREST=10.D0*(1.D0*FVAL-AINT(1.D0*FVAL))
               DO J=1,3
                  IFINT=AINT(FREST)
                  IF(IFINT.GT.0) IZZ=J
                  FREST=(10.D0*(1.D0*FREST-AINT(1.D0*FREST)))
               ENDDO
               IF(IZZ.EQ.0) THEN
                  IF(FLVAL.LT.0.) THEN
                     IFLVAL=AINT(FLVAL-TOLA)
                  ELSE
                     IFLVAL=AINT(FLVAL+TOLA)
                  ENDIF
                  WRITE(LLTXT,2000) IFLVAL
               ELSEIF(IZZ.EQ.1) THEN
                  WRITE(LLTXT,2030) FLVAL
               ELSEIF(IZZ.EQ.2) THEN
                  WRITE(LLTXT,2031) FLVAL
               ELSEIF(IZZ.EQ.3) THEN
                  WRITE(LLTXT,2032) FLVAL
               ELSE
                  WRITE(LLTXT,2020) FLVAL
               ENDIF
            ENDIF
            CALL CPSETC('LLT - LINE LABEL TEXT STRING',
     1           LLTXT(LSTRBEG(LLTXT):LSTREND(LLTXT)))
            IF(FLVAL.LT.-TOLV) THEN
               CALL CPSETC('CLD', '$''$''')
            ENDIF
         ENDDO 
C
      ELSEIF (FINC.GE.TOL) THEN
C
         IF (FLOWN .LT. FHIGHN) THEN
            CALL CPSETI('CLS - CONTOUR LEVEL SELECTION FLAG',0)
            CALL CPSETR('CIS - CONTOUR INTERVAL SPECIFIER', FINC)
            CALL CPSETR('CMN - MINIMUM CONTOUR LEVEL', FLOWN)
            CALL CPSETR('CMX - MAXIMUM CONTOUR LEVEL', FHIGHN)
            NCL = INT((FHIGHN - FLOWN)/FINC) + 1
            CALL CPSETI('NCL - NUMBER OF CONTOUR LEVELS', NCL)
C
            DO I = 1, NCL
              FLVAL=FLOWN + (I - 1) * FINC
              CALL CPSETI('PAI - PARAMETER ARRAY INDEX', I)
              CALL CPSETR('CLV - CONTOUR LEVEL VALUES',FLVAL)
C
             IZZ=0
             IF((FLVAL.EQ.0.0).OR.(ABS(FLVAL).LE.TOL)) THEN
               WRITE(LLTXT,2002)
             ELSEIF(ABS(FLVAL).GT.9999..OR.ABS(FLVAL).LT.0.001) 
     1              THEN
               WRITE(LLTXT,2020) FLVAL            
             ELSE
               FVAL=ABS(1.0D0*FLVAL)+1.D0*TOLA
               FREST=10.D0*(1.D0*FVAL-AINT(1.D0*FVAL))
               DO J=1,3
                  IFINT=AINT(FREST)
                  IF(IFINT.GT.0) IZZ=J
                  FREST=(10.D0*(1.D0*FREST-AINT(1.D0*FREST)))
               ENDDO
               IF(IZZ.EQ.0) THEN
                  IF(FLVAL.LT.0.) THEN
                     IFLVAL=AINT(FLVAL-TOLA)
                  ELSE
                     IFLVAL=AINT(FLVAL+TOLA)
                  ENDIF
                  WRITE(LLTXT,2000) IFLVAL
               ELSEIF(IZZ.EQ.1) THEN
                  WRITE(LLTXT,2030) FLVAL
               ELSEIF(IZZ.EQ.2) THEN
                  WRITE(LLTXT,2031) FLVAL
               ELSEIF(IZZ.EQ.3) THEN
                  WRITE(LLTXT,2032) FLVAL
               ELSE
                  WRITE(LLTXT,2020) FLVAL
               ENDIF
             ENDIF
C
             IF (ITYPE .NE. ISPPL) THEN
              IF(NZERO .AND. (ABS(FLVAL) .LT. TOL)) THEN
                 INZERO=I
              ENDIF
             ENDIF
             CALL CPSETC('LLT - LINE LABEL TEXT STRING',
     1             LLTXT(LSTRBEG(LLTXT):LSTREND(LLTXT)))

C Protect against dashed zero line.
             IF(FLVAL.LT.-TOL) THEN
              CALL CPSETC('CLD', '$''$''')
             ENDIF

            ENDDO
C
         ELSE
C
            WRITE(6,*) 'WARNING: FINC IS LESS THAN TOLORANCE;'
C The default values of CIT are fine, set LIT instead
            DO I = 1, NCIT
               CALL CPSETI('PAI - PARAMETER ARRAY INDEX', I)
               CALL CPSETI('LIT - LABEL INTERVAL TABLE',2)
            ENDDO
         ENDIF
C
         CALL CPSETR('CIU - CONTOUR INTERVAL USED', FINC)

      ELSE IF (ABS(FINC) .LT. TOL) THEN
C Some settings have been moved to INITCL.
         CALL CPGETI('NCL - NUMBER OF CONTOUR LEVELS', NCL)
         DO I = 1, NCL
            CALL CPSETI('PAI - PARAMETER ARRAY INDEX', I)
            CALL CPGETR('CLV - CONTOUR LEVEL VALUE', FLVAL)
C     Protect against dashed zero line.
            IF(FLVAL.LE.-TOL) THEN
               CALL CPSETC('CLD', '$''$''')
            ENDIF
         ENDDO
      ELSE

         WRITE(6,*) '#########################################'
         WRITE(6,*) '#### SHOULD NOT BE HERE - FINC .LT. 0 ###'
         WRITE(6,*) '#########################################'
         CALL CPSETI('CLS - CONTOUR LEVEL SELECTION FLAG',
     1        -(MAX(1, INT(-FINC))))
         IF (FLOWN .LT. FHIGHN) THEN
            CALL CPSETR('CMN - MINIMUM CONTOUR LEVEL', FLOWN)
            CALL CPSETR('CMX - MAXIMUM CONTOUR LEVEL', FHIGHN)
         ENDIF
      ENDIF
C
 2000 FORMAT(I7)
 2002 FORMAT('0')
 2020 FORMAT(1P1E10.1)
 2030 FORMAT(F6.1)
 2031 FORMAT(F7.2)
 2032 FORMAT(F8.3)
      RETURN
      END
