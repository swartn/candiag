      SUBROUTINE SETPAT(NPAT) 

C     * MAR 11/2004 - M.BERKLEY (MODIFIED for NCAR V3.2)

C      ************************************************************ 
C      * NCAR SUBROUTINE
C      *    - S. WILSON - 23 JUL 1991 MODIFY PATTERNS 
C      *    - F. ZWIERS - 12 MAR 1991 NCAR V1.0 PATTERNS
C      ************************************************************ 
  
      INTEGER IFOT(16), JFOT(16), ID1(8,8),ID2(8,8),ID3(8,8)
      INTEGER ID4(8,8),ID5(8,8),ID6(8,8),ID7(8,8),ID8(8,8)
  
      COMMON /FILLCO/ LAN,LSP,LPA,LCH,LDP(8,8)
  
      DATA (IFOT(I), I=1,16) /1,3,3,1,2,4,4,2,2,4,4,2,3,1,1,3/
      DATA (JFOT(I), I=1,16) /1,3,1,3,2,4,2,4,1,3,1,3,2,4,2,4/
C
      DATA ID1 / 0,0,0,0,0,0,0,0,
     1           0,1,1,1,1,1,1,0,
     2           0,1,0,0,0,0,1,0,
     3           0,1,0,0,0,0,1,0,
     4           0,1,0,0,0,0,1,0,
     5           0,1,0,0,0,0,1,0,
     6           0,1,1,1,1,1,1,0,
     7           0,0,0,0,0,0,0,0/
      DATA ID2 / 0,0,0,0,0,0,0,0,
     1           0,1,0,0,0,0,1,0,
     2           0,0,1,0,0,1,0,0,
     3           0,0,0,1,1,0,0,0,
     4           0,0,0,1,1,0,0,0,
     5           0,0,1,0,0,1,0,0,
     6           0,1,0,0,0,0,1,0,
     7           0,0,0,0,0,0,0,0/
      DATA ID3 / 0,0,0,0,0,0,0,0,
     1           0,0,0,1,1,0,0,0,
     2           0,0,1,0,0,1,0,0,
     3           0,1,0,0,0,0,1,0,
     4           0,1,0,0,0,0,1,0,
     5           0,0,1,0,0,1,0,0,
     6           0,0,0,1,1,0,0,0,
     7           0,0,0,0,0,0,0,0/
      DATA ID4 / 0,0,0,0,0,0,0,0,
     1           0,0,0,1,1,0,0,0,
     2           0,0,1,1,1,1,0,0,
     3           0,1,1,1,1,1,1,0,
     4           0,1,1,1,1,1,1,0,
     5           0,0,1,1,1,1,0,0,
     6           0,0,0,1,1,0,0,0,
     7           0,0,0,0,0,0,0,0/
      DATA ID5 / 0,0,0,0,0,0,0,0,
     1           0,0,0,1,1,0,0,0,
     2           0,0,0,1,1,0,0,0,
     3           0,1,1,1,1,1,1,0,
     4           0,1,1,1,1,1,1,0,
     5           0,0,0,1,1,0,0,0,
     6           0,0,0,1,1,0,0,0,
     7           0,0,0,0,0,0,0,0/
      DATA ID6 / 0,0,0,0,0,0,0,0,
     1           0,0,0,1,1,0,0,0,
     2           0,0,0,1,1,0,0,0,
     3           0,0,1,1,1,1,0,0,
     4           0,0,1,1,1,1,0,0,
     5           0,1,1,1,1,1,1,0,
     6           0,1,1,1,1,1,1,0,
     7           0,0,0,0,0,0,0,0/
      DATA ID7 / 0,0,0,0,0,0,0,0,
     1           0,1,1,1,1,1,1,0,
     2           0,1,1,1,1,1,1,0,
     3           0,1,1,1,1,1,1,0,
     4           0,1,1,1,1,1,1,0,
     5           0,1,1,1,1,1,1,0,
     6           0,1,1,1,1,1,1,0,
     7           0,0,0,0,0,0,0,0/
      DATA ID8 / 0,0,0,0,0,0,0,0,
     1           0,1,1,1,1,1,1,0,
     2           0,1,1,0,0,1,1,0,
     3           0,1,0,1,1,0,1,0,
     4           0,1,0,1,1,0,1,0,
     5           0,1,1,0,0,1,1,0,
     6           0,1,1,1,1,1,1,0,
     7           0,0,0,0,0,0,0,0/
C 
C     * CLEAR OUT ARRAY 
C 
      DO 120 I=1,8
        DO 110 J=1,8
          LDP(I,J)=0
  110   CONTINUE
  120 CONTINUE
C 
      IF(NPAT .GE. 1 .AND. NPAT .LE. 16) THEN 
         DO 200 I = 1, 17 - NPAT
            LDP(IFOT(I), JFOT(I)) = 1 
            LDP(IFOT(I) + 4, JFOT(I)) = 1 
            LDP(IFOT(I), JFOT(I) + 4) = 1 
            LDP(IFOT(I) + 4, JFOT(I) + 4) = 1 
 200     CONTINUE 
      ELSE IF (NPAT .EQ. 17) THEN
         LDP(IFOT(1), JFOT(1)) = 1 
      ENDIF 
  
C      IF(NPAT.EQ.28) THEN
C         DO I=1,8
C            DO J=1,8
C               LDP(I,J)=ID1(I,J)
C            ENDDO
C         ENDDO
C
C      ELSEIF(NPAT.EQ.29) THEN
C
C         DO I=1,8
C            DO J=1,8
C               LDP(I,J)=ID2(I,J)
C            ENDDO
C         ENDDO
C
C      ELSEIF(NPAT.EQ.30) THEN
C
C         DO I=1,8
C            DO J=1,8
C               LDP(I,J)=ID3(I,J)
C            ENDDO
C         ENDDO
C
C      ELSEIF(NPAT.EQ.31) THEN
C
C         DO I=1,8
C            DO J=1,8
C               LDP(I,J)=ID4(I,J)
C            ENDDO
C         ENDDO
C
C      ELSEIF(NPAT.EQ.32) THEN
C
C         DO I=1,8
C            DO J=1,8
C               LDP(I,J)=ID5(I,J)
C            ENDDO
C         ENDDO
C
C      ELSEIF(NPAT.EQ.33) THEN
C
C         DO I=1,8
C            DO J=1,8
C               LDP(I,J)=ID6(I,J)
C            ENDDO
C         ENDDO
C
C      ELSEIF(NPAT.EQ.34) THEN
C
C         DO I=1,8
C            DO J=1,8
C               LDP(I,J)=ID7(I,J)
C            ENDDO
C         ENDDO
C
C      ELSEIF(NPAT.EQ.35) THEN
C
C         DO I=1,8
C            DO J=1,8
C               LDP(I,J)=ID8(I,J)
C            ENDDO
C         ENDDO
C
C      ENDIF
         
C      ENDIF
      RETURN
      END 
