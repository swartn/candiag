      SUBROUTINE INIT_MAPLIMITS

C     * MAR 11/2004 - M.BERKLEY (MODIFIED for CCCMA)

C--------------------------------------------------------------------
C Data declarations - initialize variables

      REAL RLATMN(2),RLATMX(2),RLONMN(2),RLONMX(2),DGRW
      COMMON /GGP_MAP_LIMITS/ RLATMN,RLATMX,RLONMN,RLONMX,DGRW
      RLATMN(1)=-90.
      RLATMN(2)=0.
      RLATMX(1)=90.
      RLATMX(2)=0.
      RLONMN(1)=-180.
      RLONMN(2)=0.
      RLONMX(1)=180.
      RLONMX(2)=0. 
      DGRW=0.

      END
