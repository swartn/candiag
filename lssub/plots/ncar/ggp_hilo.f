      SUBROUTINE GGP_HILO(ZREG, MREG, NREG,
     1     FLOW, FHIGH, FINC, FLOWN, FHIGHN, SPVAL)

C     * SEP 18/2006 - F.MAJAESS (MODIFIED FOR TOLERANCE IN SPVAL)
C     * MAR 11/2004 - M.BERKLEY (MODIFIED for CCCMA)

      IMPLICIT NONE

C     HIgh LOw bound modification routine. If there are too many contour
C     levels, GGP_HILO does what it can to shorten the range to avoid
C     crashing. The rest is up the the person who makes the cards.

      INTEGER MREG, NREG
      REAL ZREG(MREG,NREG)
      REAL FLOW, FHIGH, FINC
      REAL FLOWN, FHIGHN
      REAL SPVAL,SPVALT

      INTEGER NCLTMP
      INTEGER IVAL, JVAL
      INTEGER I,J
      REAL MINVAL,MAXVAL
      REAL TOL

      INTEGER MAXNUMCONT
      PARAMETER (MAXNUMCONT=256)

C     * SETUP IN "SPVALT" THE TOLERANCE TO CHECK "SPVAL" AGAINST.

      SPVALT=1.E-6*SPVAL

      IF(FINC.LE.0.0) THEN
         FLOWN = FLOW
         FHIGHN = FHIGH
         RETURN
      ENDIF

      NCLTMP = INT((FHIGH - FLOW)/FINC) + 1

C     If NCLTMP .LE. MAXNUMCONT, nothing needs to be done
      IF (NCLTMP.LE.MAXNUMCONT) THEN
         FLOWN = FLOW
         FHIGHN = FHIGH
         RETURN
      ENDIF

C Find the first value in the field that is not equal to SPVAL
      DO IVAL=1,MREG
         DO JVAL=1,NREG
            IF (ABS(ZREG(IVAL,JVAL)-SPVAL).GT.SPVALT) GOTO 100
         ENDDO
      ENDDO

C     Search for MINVAL and MAXVAL in field
 100  MINVAL=ZREG(IVAL,JVAL)
      MAXVAL=ZREG(IVAL,JVAL)
      DO I=IVAL,MREG
         DO J=JVAL,NREG
            IF (ABS(ZREG(I,J)-SPVAL).GT.SPVALT) THEN
               MINVAL=MIN(MINVAL,ZREG(I,J))
               MAXVAL=MAX(MAXVAL,ZREG(I,J))
            ENDIF
         ENDDO
      ENDDO


C     If the high and low values are within the upper and lower bounds
C     of the field, then nothing can be done. If only one of them is
C     within, then the other can be changed.

      IF((FHIGH.LT.MAXVAL).AND.(FLOW.LT.MINVAL)) THEN
         WRITE(6,*) 'ZMN: ', MINVAL, 'ZMX: ',MAXVAL
         WRITE(6,*) '###############################'
         WRITE(6,*) '## ERROR - TOO MANY CONTOURS ##'
         WRITE(6,*) '###############################'
         CALL PXIT('GGP_HILO',-1)
      ENDIF

      FLOWN = MAX(FLOW,MINVAL)
      FHIGHN = MIN(FHIGH,MAXVAL)

C     Modify FLOWN and FHIGHN to be FINC multiples

      TOL = FINC/10.0
      IF(MOD(FLOW,FINC) .LT. TOL) THEN
         FLOWN = INT(FLOWN/FINC)*FINC
         FHIGHN = INT(FHIGHN/FINC)*FINC+1
      ENDIF

      WRITE(6,90) 'MORE THAN ',MAXNUMCONT,' CONTOURS:'
 90   FORMAT(A,I4,A)
      WRITE(6,91) '  FLOW AND FHIGH RESET FROM', FLOW, ' <=> ', FHIGH,
     1     ' TO ', FLOWN, ' <=> ', FHIGHN
 91   FORMAT(4(A,G9.2))

      NCLTMP = INT((FHIGHN - FLOWN)/FINC) + 1

      IF(NCLTMP.GT.MAXNUMCONT) THEN
         WRITE(6,*) '###############################################'
         WRITE(6,*) '## ERROR - FINC TOO SMALL, TOO MANY CONTOURS ##'
         WRITE(6,*) '###############################################'
         CALL PXIT('GGP_HILO',-2)
      ENDIF

      RETURN
      END
