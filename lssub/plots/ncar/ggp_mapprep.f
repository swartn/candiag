      SUBROUTINE GGP_MAPPREP(ZREG, MREG, NREG,
     1     WHITFG, PROJ, MAP, NSETC, NHEM, SUBAREA,
     2     SQUARE,IMAPC)

C     * MAR 11/2004 - M.BERKLEY (MODIFIED for CCCMA)

      IMPLICIT NONE
C     This routine sets Ezmap parameters


      INTEGER MREG, NREG, IMAPC
      REAL ZREG(MREG, NREG)
      INTEGER WHITFG
      CHARACTER*(*) PROJ
      LOGICAL MAP
      INTEGER NSETC
      INTEGER NHEM
      LOGICAL SUBAREA
      LOGICAL SQUARE

      INTEGER IGGP_CPAREA_SIZE
      PARAMETER (IGGP_CPAREA_SIZE=4000000)
      INTEGER IGGP_CPAREA(IGGP_CPAREA_SIZE)
      COMMON /GGP_AREA/IGGP_CPAREA

      COMMON /GGP_MAP_LIMITS/ RLATMN,RLATMX,RLONMN,RLONMX,DGRW
      REAL RLATMN(2),RLATMX(2),RLONMN(2),RLONMX(2),DGRW

      REAL VPL, VPR, VPB, VPT
      REAL ZA, ZB, ZC, ZD, ZE, ZF, ZG, ZH, LZZ

C!!!       PRINT*, "MAPPREP"

C Define blackground/whiteground colour scheme.
      CALL BFCRDF(0)

C Initialize Areas
      CALL ARINAM(IGGP_CPAREA, IGGP_CPAREA_SIZE)

      IF(.NOT.MAP) THEN
         IF(SQUARE) THEN
            VPL = 0.1
            VPR = 0.9
            VPB = 0.1
            VPT = 0.9
         ELSE
            VPL = 0.02
            VPR = 0.98
            VPB = 0.22
            VPT = 0.78
         ENDIF
         CALL GETSET(ZA,ZB,ZC,ZD,ZE,ZF,ZG,ZH,LZZ)
         CALL SET(VPL, VPR, VPB, VPT, ZE, ZF, ZG, ZH, LZZ)

         RETURN
      ENDIF


C Initialize Ezmap and add to area map
      IF(IMAPC.NE.1) THEN
         CALL MPSETC ('OU','CO')
      ELSE
         CALL MPSETC ('OU','PO')
      ENDIF
      IF (MAP) THEN
        CALL MPSETI ('C5',1)
        CALL MPSETI ('C7',1)
      ELSE
        CALL MPSETI ('C5',0)
        CALL MPSETI ('C7',0)
      ENDIF
      CALL MPSETI('LA - MERIDIAN/POLE LABEL FLAG', 1)
      CALL MPSETI('DA - GRID DASHLINE PATTERN', 65535)
      IF (PROJ.EQ.'ST') THEN
        VPL = 0.05
        VPR = 0.85
        VPB = 0.04
        VPT = 0.86
        IF(NHEM.EQ.2) THEN
          CALL MAPROJ(PROJ,-90.0,0.0,(90.0-DGRW))
C          CALL MAPROJ(PROJ,-90.0,0.0,mod((90.0-DGRW),180.0))
        ELSE
          CALL MAPROJ(PROJ,90.0,0.0,(-90.0-DGRW))
C          CALL MAPROJ(PROJ,90.0,0.0,mod((-90.0-DGRW),180.0))
        ENDIF
        CALL MPSETR('GR',30.)
        CALL MAPSET('CO',RLATMN,RLONMN,RLATMX,RLONMX)
        CALL GETSET(ZA,ZB,ZC,ZD,ZE,ZF,ZG,ZH,LZZ)
        CALL SET(VPL, VPR, VPB, VPT, ZE, ZF, ZG, ZH, LZZ)
        CALL MAPPOS(VPL, VPR, VPB, VPT)
      ELSE
         VPL = 0.02
         VPR = 0.98
         VPB = 0.22
         VPT = 0.78

        CALL MAPROJ(PROJ,0.0,DGRW,0.0)
        CALL MPSETR ('GR',90.)
        CALL MAPSET('CO',RLATMN,RLONMN,RLATMX,RLONMX)
C        IF (NSETC .EQ. 0) THEN
         CALL GETSET(ZA,ZB,ZC,ZD,ZE,ZF,ZG,ZH,LZZ)
         CALL SET(VPL, VPR, VPB, VPT, ZE, ZF, ZG, ZH, LZZ)
         CALL MAPPOS(VPL, VPR, VPB, VPT)
C        ENDIF
      ENDIF
      CALL MAPINT
      CALL MAPBLA(IGGP_CPAREA)
      
      RETURN
      END
