      SUBROUTINE GGP_CONHLD(LHI, ZDAT, MZDT, NZDT, PROJ,
     1     FLOW, FHIGH, FINC) 

C     * MAR 11/2004 - M.BERKLEY (MODIFIED for CCCMA)

      IMPLICIT NONE
C CONtour plot High/Low/Data-point labelling routine.
C
      INTEGER MZDT, NZDT
      REAL ZDAT(MZDT, NZDT)
      INTEGER LHI
      CHARACTER*2 PROJ
      REAL FLOW, FHIGH, FINC

      REAL XVPL,XVPR,YVPB,YVPT,XWDL,XWDR,YWDB,YWDT
      INTEGER LNLG
      REAL CHWM, ANGD, SIZE
      INTEGER IMAP
      REAL ORVA, SPVA
      REAL XCA1, XCAM, YCA1, YCAN
      REAL XPOS, YPOS
      INTEGER LOCV
      PARAMETER (LOCV=10)
      CHARACTER*(LOCV) CROZ
      INTEGER LCRZ
      REAL XMPD,YMPD
      CHARACTER*60 HLTSTR
      INTEGER IPH,IPW
      INTEGER I, J, K
      REAL FMIN,FMAX

      INTEGER STRINDEX,LSTREND
      EXTERNAL STRINDEX,LSTREND


C     * IF LHI >= 0, HIGHS AND LOWS LABELLED, 
C              <  0, .. NOT LABELLED.

C     If the following line is modified, the sub-routine CPCHHL must be
C     changed as well.

      CALL PCGETI('PH - Digitized principal height',IPH)
      CALL PCGETI('PW - Digitized principal width',IPW)
      IF (LHI.GE.0) THEN
         WRITE(HLTSTR,99)'H:V',-INT(IPH*1.2),'H',-IPW*3,':$ZDV$:E:''',
     1        'L:V',-INT(IPH*1.2),'H',-IPW*3,':$ZDV$:E:'
 99      FORMAT(2(A,I3,A,I3,A))
C!!!         WRITE(*,*) 'HLTSTR: "',HLTSTR(1:LSTREND(HLTSTR)),'"'

         CALL CPSETC('HLT',HLTSTR(1:LSTREND(HLTSTR)))
      ENDIF

C     Omit trailing decimals 
      CALL CPSETI ('NOF - NUMERIC OMMISION FLAG',2)

C     Add leading zero to numbers |n| < 1
      CALL CPSETI ('NLZ - NUMERIC LEADING ZERO',1)

C     If NSD changed, then CPCHHL must be changed as well.
      CALL CPSETI ('NSD - NUMBER OF SIGNIFICANT DIGITS',-4)

      IF (PROJ .EQ. 'CE') THEN 
        CALL CPSETR ('HLS',.010)
        CALL CPSETR ('HLW',.002)
        CALL CPSETR ('LLS',.009)
        CALL CPSETR ('LLW',.002)
      ELSE
        CALL CPSETR ('HLS',.014)
        CALL CPSETR ('HLW',.002) 
        CALL CPSETR ('LLS',.011)
        CALL CPSETR ('LLW',.002)
      ENDIF  
      IF (LHI .LT. 0) THEN
         CALL CPSETC('HLT - HIGH/LOW LABEL TEXT', ' ')
      ELSE
         IF (LHI .NE. 0) THEN 
            CALL GETSET (XVPL,XVPR,YVPB,YVPT,XWDL,XWDR,YWDB,YWDT,LNLG)
            CALL CPGETR ('CWM - CHARACTER WIDTH MULTIPLIER',CHWM)
            CALL CPGETR ('HLA - HIGH/LOW LABEL ANGLE',ANGD)
            CALL CPGETR ('HLS - HIGH/LOW LABEL SIZE',SIZE)
            CALL CPGETI ('MAP - MAPPING FLAG',IMAP)
            CALL CPGETR ('ORV - OUT-OF-RANGE VALUE',ORVA) 
            CALL CPGETR ('SPV - SPECIAL VALUE',SPVA)
            CALL CPGETR ('XC1 - X COORDINATE AT I = 1',XCA1)
            CALL CPGETR ('XCM - X COORDINATE AT I = M',XCAM)
            CALL CPGETR ('YC1 - Y COORDINATE AT J = 1',YCA1)
            CALL CPGETR ('YCN - Y COORDINATE AT J = N',YCAN)
            SIZE=(XVPR-XVPL)*CHWM*SIZE
            IF (XCA1.EQ.XCAM) THEN
               XCA1=1.
               XCAM=REAL(MZDT)
            END IF
            IF (YCA1.EQ.YCAN) THEN
               YCA1=1.
               YCAN=REAL(NZDT)
            END IF
            DO 10002 J=1,NZDT
               YPOS=YCA1+REAL(J-1)*(YCAN-YCA1)/REAL(NZDT-1)
               DO 10003 I=1,MZDT
                  XPOS=XCA1+REAL(I-1)*(XCAM-XCA1)/REAL(MZDT-1)
                  IF (SPVA.EQ.0..OR.ZDAT(I,J).NE.SPVA) THEN
                     CALL CPSETR ('ZDV - Z DATA VALUE',ZDAT(I,J))
                     CALL CPGETC ('ZDV - Z DATA VALUE',CROZ)
                     DO 10004 K=LOCV,2,-1
                        IF (CROZ(K:K).NE.' ') THEN
                           LCRZ=K
                           GO TO 101
                        END IF
10004                CONTINUE
                     LCRZ=1
 101                 IF (IMAP.EQ.0) THEN
                        CALL PCHIQU (XPOS,YPOS,CROZ(1:LCRZ),SIZE,ANGD,
     +                               0.)
                     ELSE
                        CALL CPMPXY (IMAP,XPOS,YPOS,XMPD,YMPD)
                        IF (ORVA.EQ.0..OR.XMPD.NE.ORVA)
     +                       CALL PCHIQU (XMPD,YMPD,CROZ(1:LCRZ),SIZE,
     +                                    ANGD,0.)
                     END IF
                  END IF
10003          CONTINUE
10002       CONTINUE
         ENDIF
      ENDIF


      RETURN
      END
