      SUBROUTINE AGCHCU (IFLG,KDSH)

C     * FEB 02/2101 - S.KHARIN (ADDED "NCURRCOLRESET" FOR BETTER
C     *                         CONTROL OF CURVE LINE PATTERN)
C     * MAR 11/2004 - M.BERKLEY (MODIFIED for NCAR V3.2)

      IMPLICIT NONE

      INTEGER IFLG,KDSH
C
C The routine AGCHCU is called by AGCURV just before and just after each
C curve is drawn.  The default version does nothing.  A user may supply
C a version to change the appearance of the curves.  The arguments are
C as follows:
C
C - IFLG is zero if a curve is about to be drawn, non-zero if a curve
C   has just been drawn.
C
C - KDSH is the last argument of AGCURV, as follows:
C
C      AGCURV called by   Value of KDSH
C      ----------------   ----------------------------------------
C      EZY                1
C      EZXY               1
C      EZMY               "n" or "-n", where n is the curve number
C      EZMXY              "n" or "-n", where n is the curve number
C      the user program   the user value
C
C   The sign of KDSH, when AGCURV is called by EZMY or EZMXY, indicates
C   whether the "user" dash patterns or the "alphabetic" dash patterns
C   were selected for use.
C
      LOGICAL COLOURCURVES
      INTEGER NCURVCOL,MAXNCURVCOL,ICURVCOL(26)
      INTEGER IPATNE, J,NCURRCOLRESET
      CHARACTER PAT(26)*16
      COMMON /CCCAGCHCU/ COLOURCURVES,NCURVCOL,MAXNCURVCOL,ICURVCOL,
     1     PAT,IPATNE,NCURRCOLRESET

      INTEGER NCURRCOL
      SAVE NCURRCOL
      DATA NCURRCOL/0/
      CHARACTER CDASH*17

      IF(NCURRCOLRESET.EQ.1)THEN
        NCURRCOLRESET=0
        NCURRCOL=0
      ENDIF

      IF (IFLG .EQ. 0) THEN
        CALL GSLWSC(4.)
      ELSE
        CALL GSLWSC(2.)
      ENDIF

CDEBUG
C!!!      WRITE(*,*)'AGCHCU: ',COLOURCURVES,IFLG
CDEBUG

      CALL SFLUSH

      IF(COLOURCURVES) THEN
CDEBUG
C!!!         WRITE(*,*)'PATTERNS: ',NCURVCOL,(ICURVCOL(I),I=1,NCURVCOL)
C!!!         WRITE(*,*)NCURRCOL,IFLG,MOD(NCURRCOL,NCURVCOL),
C!!!     1        ICURVCOL(MOD(NCURRCOL,NCURVCOL)+1)-98
CDEBUG
         IF(IFLG.EQ.0) THEN

            CALL GSPLCI(ICURVCOL(MOD(NCURRCOL,NCURVCOL)+1)-98)
            IF((NCURRCOL/NCURVCOL).GT.0) THEN
               CALL DASHDC(PAT(NCURRCOL/NCURVCOL+1),8,1)
CC               CALL DASHDB(PAT(NCURRCOL/NCURVCOL))
CC               CALL GSPLCI(1)
CC               CALL DASHDC(PAT(NCURRCOL),8,1)
            ENDIF
            NCURRCOL=NCURRCOL+1
         ELSE
            CALL GSPLCI(1)
         ENDIF
      ELSE
         CALL GSPLCI(1)
      ENDIF
      RETURN

      END
