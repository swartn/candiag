      SUBROUTINE INDEXAO(ELONA,ELATA,NLONA,NLATA,LONO,LATO,NLONO,NLATO,
     1                   DAO,BETAO,INDI,INDJ,INDII,INDJJ,DAA,BETAA)
C
C     * JAN 20/04 - B. MIVILLE - FIXED BETAA AND DAA NOT BEING INITIALIZED
C                                AT THE CORNERS OF THE GRID
C     * APR 04/02 - B. MIVILLE - REVISED FOR NEW DATA DESCRIPTION FORMAT
C     * JUN 08/01 - B. MIVILLE
C
C     * SEARCH FOR OCEAN POINTS WHICH SURROUNDS THE ATMOSPHERE 
C     * POINT AND THE OCEAN POINTS WITHIN THE 4 ATMOSPHERE POINTS
C     * NEEDED TO DO THE INTERPOLATION
C
C     * INPUT PARAMETERS...
C
C     * ELONA = ATMOSPHERE LONGITUDES (PLUS ONE EXTRA TO THE WEST OF THE 
C     *         FIRST LONGITUDE
C     * ELATA = ATMOSPHERE LATITUDES (PLUS ONE EXTRA TO THE NORTH AND 
C     *         SOUTH)
C     * NLONA = NUMBER OF ATMOSPHERE LONGITUDES INCLUDING CYCLIC
C     * NLATA = NUMBER OF ATMOSPHERE LATITUDES 
C     * LONO  = OCEAN LONGITUDES
C     * LATO  = OCEAN LATITUDES
C     * NLONO = NUMBER OF OCEAN LONGITUDES INCLUDING CYCLIC
C     * NLATO = NUMBER OF OCEAN LATITUDES
C     * DAO   = AREA OF OCEAN GRID CELL
C     * BETAO = HEAVISIDE FUNCTION FOR OCEAN GRID CELL
C
C     * OUTPUT PARAMETERS...
C
C     * INDI  = INDEX THE ATMOSPHERE II POINT TO OCEAN I POINT FOR 
C     *         INTERPOLATION
C     * INDJ  = INDEX THE ATMOSPHERE JJ POINT TO OCEAN J POINT FOR 
C     *         INTERPOLATION
C     * INDII = INDEX THE OCEAN I POINT TO ATMOSPHERE II POINT FOR 
C     *         ADJUSTMENT
C     * INDJJ = INDEX THE OCEAN J POINT TO ATMOSPHERE JJ POINT FOR 
C     *         ADJUSTMENT
C     * DAA   = AREA OF ATMOSPHERE GRID CELL
C     * BETAA = HEAVISIDE FUNCTION FOR ATMOSPHERE GRID CELL
C
C-----------------------------------------------------------------------
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      INTEGER NLONA,NLATA,NLONO,NLATO
      INTEGER INDI(NLONO,NLATO),INDJ(NLONO,NLATO)
      INTEGER INDII(NLONO,NLATO),INDJJ(NLONO,NLATO)
      REAL LONO(NLONO,NLATO),LATO(NLONO,NLATO)
      REAL ELONA(0:NLONA,0:NLATA),ELATA(0:NLONA,0:NLATA+1)
      REAL DAO(NLONO,NLATO),BETAO(NLONO,NLATO)
      REAL DAA(0:NLONA,0:NLATA+1),BETAA(0:NLONA,0:NLATA+1)
      REAL DLATM,DLATP,DLONM,DLONP
C-----------------------------------------------------------------------
C
      DLATM=0.E0
      DLATP=0.E0
      DLONM=0.E0
      DLONP=0.E0
C
      DO 152 J=0,NLATA+1
         DO 150 I=0,NLONA
            DAA(I,J)=0.E0
 150     CONTINUE
 152  CONTINUE
C
      DO 172 J=1,NLATO
         JJ=0
         DO 170 I=1,NLONO
            II=0
 160        IF((LATO(I,J).GE.ELATA(II,JJ)).AND.
     1         (LATO(I,J).LT.ELATA(II,JJ+1)))THEN
               INDJ(I,J)=JJ
               DLATM=LATO(I,J)-ELATA(II,JJ)
               DLATP=ELATA(II,JJ+1)-LATO(I,J)
C
 162           IF((LONO(I,J).GE.ELONA(II,JJ)).AND.
     1            (LONO(I,J).LT.ELONA(II+1,JJ)))THEN
                  INDI(I,J)=II
                  DLONM=LONO(I,J)-ELONA(II,JJ)
                  DLONP=ELONA(II+1,JJ)-LONO(I,J)
                  IF(DLATM.GT.DLATP)THEN
                     INDJJ(I,J)=JJ+1
                     IF(DLONM.GT.DLONP)THEN
                        INDII(I,J)=II+1
                        DAA(II+1,JJ+1)=DAA(II+1,JJ+1)+DAO(I,J)
                        BETAA(II+1,JJ+1)=BETAO(I,J)
                     ELSE
                        INDII(I,J)=II
                        DAA(II,JJ+1)=DAA(II,JJ+1)+DAO(I,J)
                        BETAA(II,JJ+1)=BETAO(I,J)
                     ENDIF
                  ELSE             
                     INDJJ(I,J)=JJ
                     IF(DLONM.GT.DLONP)THEN
                        INDII(I,J)=II+1
                        DAA(II+1,JJ)=DAA(II+1,JJ)+DAO(I,J)
                        BETAA(II+1,JJ)=BETAO(I,J)
                     ELSE
                        INDII(I,J)=II
                        DAA(II,JJ)=DAA(II,JJ)+DAO(I,J)
                        BETAA(II,JJ)=BETAO(I,J)
                     ENDIF
                  ENDIF
               ELSE
                  II=II+1
                  IF(II.GT.(NLONA-1)) GOTO 170
                  GOTO 162
               ENDIF
C
            ELSE
               JJ=JJ+1
               IF(JJ.GT.(NLATA)) GOTO 172
               GOTO 160
            ENDIF
C
 170        CONTINUE
C
 172  CONTINUE            
C
C   * INITIALIZE CORNERS
C
        DO 174 II=1,NLONA-1
           DAA(II,0)=DAA(II,1)
           DAA(II,NLATA+1)=DAA(II,NLATA)
           BETAA(II,0)=BETAA(II,1)
           BETAA(II,NLATA+1)=BETAA(II,NLATA)
 174    CONTINUE
C
        DO 176 JJ=0,NLATA+1
           DAA(0,JJ)=DAA(NLONA-1,JJ)
           DAA(NLONA,JJ)=DAA(1,JJ)
           BETAA(0,JJ)=BETAA(NLONA-1,JJ)
           BETAA(NLONA,JJ)=BETAA(1,JJ)
 176    CONTINUE
C
      RETURN
      END
