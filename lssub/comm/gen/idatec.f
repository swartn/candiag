      SUBROUTINE IDATEC(MONTH,MDAY,IDAY) 
C 
C     * OCT 05/93 - M.LAZARE. NEW NAME FOR IDATE TO AVOID CONFLICT
C     *                       WITH SYSTEM ROUTINE OF SAME NAME.
C     * FEB 20/81 - J.D.HENDERSON. PREVIOUS NAME IDATE.
C
C     * DETERMINES THE DATE FROM THE DAY OF THE YEAR. 
C     * MONTH = MONTH (JAN,FEB,...,ETC.)
C     *  MDAY = DAY OF THE MONTH (1 TO 31)
C     *  IDAY = DAY OF THE YEAR (1 TO 365)
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      INTEGER NFDM(13)
      CHARACTER*3 MON(12),MONTH
      DATA MON/'JAN','FEB','MAR','APR','MAY','JUN',
     1         'JUL','AUG','SEP','OCT','NOV','DEC'/ 
      DATA NFDM/1,32,60,91,121,152,182,213,244,274,305,335,366/ 
C-------------------------------------------------------------------- 
C     * IF IDAY IS NOT BETWEEN 1 AND 365 (INCLUSIVE)
C     * THE MONTH RETURNS AS XXX AND MDAY IS SET TO IDAY. 
C 
      MONTH='XXX' 
      MDAY=IDAY 
      IF(IDAY.LT.1) RETURN
C 
      DO 210 I=1,12 
      IF(IDAY.GE.NFDM(I+1)) GO TO 210 
      MDAY=IDAY-NFDM(I)+1 
      MONTH=MON(I)
      RETURN
  210 CONTINUE
C 
      RETURN
      END 
