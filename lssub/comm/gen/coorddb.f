      SUBROUTINE COORDDB (DB,D2B, BG,BH, NK)
  
C     * FEB 29/88 - R.LAPRISE.
C     * DEFINITION DES PARAMETRES DB ET D2B DE LA DISCRETISATION
C     * VERTICALE, A PARTIR DE BG ET BH, POUR LE MODELE HYBRIDE.
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      REAL BG  (NK),BH  (NK)
      REAL DB  (NK),D2B (NK)
C-----------------------------------------------------------------------
      DB (1) = BG(1)
      D2B(1) = 0.E0 
      DO 100 K=2,NK 
        DB (K) = BG(K  )-BG(K-1)
        D2B(K) = BH(K-1)-BG(K-1)
  100 CONTINUE
  
      RETURN
      END
