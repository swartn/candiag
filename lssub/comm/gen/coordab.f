      SUBROUTINE COORDAB (A,B, NK,ETA,ICOORD,PTOIT) 
  
C     * SEP 27/06 - F.MAJAESS - ADD ICOORD=4HET16 (R=1.6) OPTION.
C     * JAN 30/91 - M.LAZARE. - ADD ICOORD=4HET10 (R=1.0) OPTION. 
C     *                         FORCE ETOIT=0 FOR 4H SIG OPTION.
C     * JAN 16/91 - M.LAZARE. - ADD ICOORD=4HET15 (R=1.5) OPTION. 
C     * FEB 29/88 - R.LAPRISE.
C     * DEFINITION DES PARAMETRES A ET B DE LA DISCRETISATION VERTICALE 
C     * A PARTIR D'UNE COLONNE DE VALEUR ETA POUR LE MODELE HYBRIDE.
C     * ICOORD CONTROLE LE TYPE DE COORDONNEES ( 4H SIG OU 4H ETA). 
  
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      REAL A  (NK),B  (NK)
      REAL ETA(NK)
  
      PARAMETER(P0=101320.E0) 
C---------------------------------------------------------------------
      ETOIT=PTOIT/P0
  
C     * COORDONNEES SIGMA * 
  
      IF(ICOORD.EQ.NC4TO8(" SIG") ) THEN
         ETOIT=0.E0
         DO 50 K=1,NK
            B(K)=ETA(K)
            A(K)=0.E0
   50    CONTINUE 
  
C     * COORDONNEES NON-HYBRIDE * 
  
      ELSEIF(ICOORD.EQ.NC4TO8("ET10")) THEN
         DO 100 K=1,NK
            B(K)=(ETA(K)-ETOIT)/(1.E0-ETOIT)
            A(K)=P0*(ETA(K)-B(K)) 
  100    CONTINUE 
  
C     * COORDONNEES HYBRIDE (ETA) *
  
      ELSEIF(ICOORD.EQ.NC4TO8(" ETA")) THEN
         DO 200 K=1,NK
            B(K)=((ETA(K)-ETOIT)/(1.E0-ETOIT))**2 
            A(K)=P0*(ETA(K)-B(K)) 
  200    CONTINUE 
  
C     * COORDONNEES HALF-HYBRIDE (ET15) *
  
      ELSEIF(ICOORD.EQ.NC4TO8("ET15")) THEN
         DO 300 K=1,NK
            B(K)=((ETA(K)-ETOIT)/(1.E0-ETOIT))**1.5E0 
            A(K)=P0*(ETA(K)-B(K)) 
  300    CONTINUE 
  
C     * COORDONNEES "0.6" HYBRIDE (ET16) *
  
      ELSEIF(ICOORD.EQ.NC4TO8("ET16")) THEN
         DO 400 K=1,NK
            B(K)=((ETA(K)-ETOIT)/(1.E0-ETOIT))**1.6E0 
            A(K)=P0*(ETA(K)-B(K)) 
  400    CONTINUE 
  
      ELSE
         CALL XIT('COORDAB',-1) 
      ENDIF 
  
      RETURN
      END 
