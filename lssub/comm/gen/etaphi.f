      SUBROUTINE ETAPHI (PHI, T,PSMBLN, 
     1                   AG,BG, RGAS, LEN, NSL,NSLP, SIG) 
  
C     * FEB 12/88 - R.LAPRISE.
C     * COMPUTE PHI FROM T ON ETA COORDINATES.
C     * PHI ANF T MAY BE EQUIVALENCED IN THE CALLING PROGRAM. 
C     * AG AND BG CONTAIN THE VERTICAL COORDINATE INFORMATION FOR PHI.
C     * PHIS, THE SURFACE GEOPOTENTIAL, MUST BE IN PHI( ,NSL+1) 
C     * PSMBLN IS LN(PS), FOR PS IN MB. 
C     * NSLP = NSL + 1. 
  
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      REAL PHI(LEN,NSLP),T(LEN,NSL),PSMBLN(LEN) 
  
      REAL AG(NSL),BG(NSL), SIG(NSLP) 
C-----------------------------------------------------------------------
      SIG(NSL+1)=1.E0 
  
      DO 500 N=1,LEN
  
         CALL NIVCAL (SIG, AG,BG,100.E0*EXP(PSMBLN(N)),NSL,1,1) 
         DO 200 L=1,NSL 
            SIG(L)=LOG(SIG(L+1)/SIG(L))
  200    CONTINUE 
  
         DO 300 L=NSL,1,-1
            PHI(N,L)=PHI(N,L+1) +RGAS*T(N,L)*SIG(L) 
  300    CONTINUE 
  
  500 CONTINUE
      RETURN
      END 
