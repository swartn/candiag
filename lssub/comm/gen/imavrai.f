      SUBROUTINE IMAVRAI (UG,VG,PSDLG,PSDPG, COSJ,ILG,LON,ILEV,A) 
C 
C     * JUL 14/92 - E. CHAN (ADD REAL*8 DECLARATIONS)
C     * JAN 12/88 - R.LAPRISE.
C     * CONVERT WIND IMAGES AND D(PS)/DX AND /DY TO REAL VALUES.
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      REAL   UG(ILG,ILEV),VG(ILG,ILEV),PSDLG(ILG),PSDPG(ILG)
      REAL*8 COSJ
C---------------------------------------------------------------- 
      DO 200 L=1,ILEV 
         DO 100 I=1,LON 
            UG(I,L)   = UG(I,L)     *(A/COSJ) 
            VG(I,L)   = VG(I,L)     *(A/COSJ) 
  100    CONTINUE 
  200 CONTINUE
C 
      DO 300 I=1,LON
            PSDLG(I)  = PSDLG(I)*(1.E0/(A*COSJ))
            PSDPG(I)  = PSDPG(I)*(1.E0/(A*COSJ))
  300 CONTINUE
      RETURN
C-----------------------------------------------------------------------
      END
