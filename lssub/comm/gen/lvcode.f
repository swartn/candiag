      SUBROUTINE LVCODE (IBUF4,ETA,NLEV)
C     *
C     * APR 20/09  - Attempting to fix bug in level coding by
C     *              adjusting "CASE 1" cutoff values.
C     * MAR  2007  - J. SCINOCCA/SLAVA KHARIN
C     *              MODIFY TO NEW HIGH PRECISION LEVEL SCHEME
C     *              BETWEEN 10 AND 100HPA
C
C     * JAN 22/93 - E.CHAN.
C     *
C     * GENERATES CODED LEVEL INFORMATION TO BE INCLUDED IN CCRN
C     * STANDARD LABELS.
C     *
C     * ETA = ETA COORDINATE OR PRESSURE IN MB DIVIDED BY 1000.
C     * IBUF4 = CODED LABEL FOR LEVEL IN THE FOLLOWING FORMS:
C     *   1)  IF ETA <  .0100          A.AA E-X   ->  -XAAA
C     *   2)  IF .0100 <= ETA <= 0.100 AND
C     *           IF C. NE. 0          A.BC E+1   ->  99ABC
C     *           IF C. EQ. 0          A.BC E+1   ->     AB
C     *   3)    OTHERWISE                  AAAA   ->   AAAA
C     *
C     * USE SUBROUTINE LVDCODE FOR REVERSE OPERATION.
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      DIMENSION IBUF4(NLEV), ETA(NLEV)
      CHARACTER STRING*9
C-----------------------------------------------------------------------
      DO 100 L = 1, NLEV
C
C       * CONVERT TO VERTICAL COORDINATE (PRESSURE IN MB OR ETA*1000).
C

        VC =  1000.0 * ETA(L)
C
        IF (VC .GE. 10.00 .AND. VC .LE. 99.95) THEN
C
C         * CASE 1: 10.0 <= VC <  100.
C

          WRITE (STRING,2000) VC
          READ  (STRING,2005) ID3
          IF (ID3.EQ.0) THEN
             IBUF4(L) = INT(VC + 0.5E0)
          ELSE
             READ  (STRING,2010) IAAA, IXP1
             IBUF4(L) = 99000 + IAAA
          ENDIF

        ELSEIF (VC .GT. 99.95) THEN
C         * CASE 2: 1001. <= VC <=  1099.

          IBUF4(L) = INT(VC + .5)

C
        ELSEIF ( VC .GT. 1.E-9 ) THEN
C
C         * CASE 3: 10 > VC > 1E-9
C
C         * THE INTEGER EXPONENT IX AND MANTISSA IAAA ARE GENERATED
C         * FROM THE REAL NUMBER VC BY FIRST WRITING VC AS CHARACTER
C         * DATA INTO STRING. IX AND IAAA ARE THEN EXTRACTED FROM
C         * THE APPROPRIATE LOCATIONS IN STRING.
C
          WRITE (STRING,2000) VC
          READ  (STRING,2010) IAAA, IXP1
          IX = IXP1 - 1
          IBUF4(L) = IX*1000 - IAAA
C
        ELSE
C
C         CASE 4: 1E-9 >= VC >= 1E-11
C
          IBUF4(L) = -9000 - INT( VC*1.E11 + .5 )
C
        ENDIF
C
  100 CONTINUE
C
      RETURN
C-----------------------------------------------------------------------
 2000 FORMAT (E9.3)
 2005 FORMAT (4X,I1,4X)
 2010 FORMAT (2X,I3,1X,I3)
      END
