      SUBROUTINE CGTORG(PAKO,PAKI,LON1,ILAT)

C     * APRIL 08, 2003 - M.LAZARE.
C
C     * THIS ROUTINE TAKES AN ORDERED S-N PAIR GRID FIELD, "PAKI" 
C     * AND CONVERTS IT INTO A NORMAL GRID, "PAKO",
C     * WHICH IS SUBSEQUENTLY WRITTEN TO OUTPUT.
C     * THIS ROUTINE IS THE OPPOSITE OPERATION OF "RGTOCG".
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)                                         
C
      REAL PAKO(LON1,ILAT), PAKI(LON1*ILAT)
C==================================================================
      NI=0
      IF(MOD(ILAT,4).NE.0) CALL                    XIT('CGTORG',-1)
      ILATH=ILAT/2 
      ILATQ=ILAT/4
C
      DO J=1,ILATQ
          JSP=J
          DO I=1,LON1
            NI=NI+1
            PAKO(I,JSP) = PAKI(NI) 
          ENDDO
C
          JNP=ILAT-J+1 
          DO I=1,LON1
            NI=NI+1
            PAKO(I,JNP) = PAKI(NI) 
          ENDDO
C
          JSE=ILATH-J+1
          DO I=1,LON1
            NI=NI+1
            PAKO(I,JSE) = PAKI(NI) 
          ENDDO
C
          JNE=ILATH+J
          DO I=1,LON1
            NI=NI+1
            PAKO(I,JNE) = PAKI(NI) 
          ENDDO
      ENDDO
C
      RETURN
      END
