      INTEGER FUNCTION ISRCHFGT(N,SX,INCX,TARGET)
C
C     * FINDS THE INDEX OF FIRST ELEMENT OF ARRAY SX HAVING VALUE
C     * GREATER THAN TARGET. 
C
C     * M.LAZARE. JAN 31/92.
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      REAL SX(N)
C
      ISRCHFGT = 0
      IF(N .LT. 1) RETURN
      IF(N.EQ.1 .AND. SX(1).GT.TARGET)          THEN
         ISRCHFGT = 1
         RETURN
      ELSE IF (N.EQ.1 .AND. SX(1).LE.TARGET)    THEN
         RETURN 
      ELSE IF(INCX.NE.1)                        THEN
C
C        * CODE FOR INCREMENT NOT EQUAL TO 1.
C
         IX = 1
         ITRUE=0
         DO 10 I = 2,N
            IX = IX + INCX
            IF(IX.LE.N .AND. ITRUE.EQ.0 .AND. SX(IX).GT.TARGET)    THEN
               ISRCHFGT = IX
               ITRUE = 1
            ENDIF
   10    CONTINUE
      ELSE
C
C        * CODE FOR INCREMENT EQUAL TO 1.
C
         ITRUE=0
         DO 30 I = 2,N
            IF(ITRUE.EQ.0 .AND. SX(I).GT.TARGET)    THEN
               ISRCHFGT = I
               ITRUE = 1
            ENDIF
   30    CONTINUE
      ENDIF
C
      RETURN
      END
