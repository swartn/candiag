      SUBROUTINE NIVCAL 
     1                  (SJ, A,B,PSJ, NK,ILG,LON) 
  
C     * FEB 02/88 - R.LAPRISE.
C     * CALCUL LA VALEUR DE LA COORDONNEE SIGMA POUR UNE TRANCHE DE 
C     * LATITUDE A PARTIR DES PARAMETRES A ET B DE LA DISCRETISATION
C     * VERTICALE TELS QUE CALCULES DANS COORDAB POUR LE MODELE HYBRIDE.
C     * 
C     * SJ (K,I)   : VALEUR SIGMA DE LA COORDONNEE VERTICALE. 
C     * A(K) ,B(K) : INFO. SUR POSITION DES NIVEAUX ETA.
C     * NK         : NOMBRE DE COUCHES. 
C     * ILG        : DIMENSION EN LONGITUDE.
C     * LON        : NOMBRE DE LONGITUDES DISTINCTES. 
  
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      REAL SJ  (NK ,ILG),PSJ (    ILG)
      REAL A   (NK),     B   (NK) 
C-------------------------------------------------------------------- 
      DO 500 I=1,LON
      DO 100 K=1,NK 
          SJ(K  ,I) =A(K)/PSJ(I) +B(K)
  100 CONTINUE
  500 CONTINUE
  
      RETURN
C-------------------------------------------------------------- 
      END
