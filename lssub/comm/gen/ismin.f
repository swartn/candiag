      INTEGER FUNCTION ISMIN(N,SX,INCX)
C
C     * FINDS THE INDEX OF ELEMENT HAVING MINIMUM VALUE.
C
C     * JACK DONGARRA, LINPACK, 3/11/78. - ISAMAX.
C     * CLEANUP AND VECTORIZED BY: M.LAZARE, SEPT. 24/91.
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      REAL SX(N)
C
      ISMIN = 0
      IF(N .LT. 1) RETURN
      ISMIN = 1
      IF(N .EQ. 1) RETURN
C
      IF(INCX.NE.1)                             THEN
C
C        * CODE FOR INCREMENT NOT EQUAL TO 1.
C
         IX = 1
         SMIN = SX(1)
         DO 10 I = 2,N
            IX = IX + INCX
            IF(IX.LE.N .AND. SX(IX).LT.SMIN)    THEN
               ISMIN = IX
               SMIN = SX(IX)
            ENDIF
   10    CONTINUE
      ELSE
C
C        * CODE FOR INCREMENT EQUAL TO 1.
C
         SMIN = SX(1)
         DO 30 I = 2,N
            IF(SX(I).LT.SMIN)                   THEN
               ISMIN = I
               SMIN = SX(I)
            ENDIF
   30    CONTINUE
      ENDIF
C
      RETURN
      END
