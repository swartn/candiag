      SUBROUTINE GETZX2(NF,ZX,LR,LEV,NLEV,IBUF,MAXPK,OK)
C 
C     * JUL 07/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)
C     * MAR 31/92 - E.CHAN (MODIFY I/O FOR 2-RECORD FORMAT)
C     * NOV  5/80 - J.D.HENDERSON 
C     * READS NEXT CROSS-SECTION FROM FILE NF INTO ARRAY ZX(LR,NLEV). 
C     * IT ASSUMES THAT THE SET ENDS WHEN IT ENCOUNTERS EITHER
C     * AN END OF FILE, A NEW NAME, OR THE FIRST LEVEL IS REPEATED. 
C     * LABL RECORDS ARE IGNORED. 
C     * LEV(NLEV) IS SET TO THE LEVEL VALUES FROM IBUF(4).
C 
C     * IBUF MUST CONTAIN 8 WORDS FOR THE LABEL AND ENOUGH EXTRA WORDS
C     *  TO CONTAIN A PACKED ROW. 
C     * IBUF(1-8) RETURNS WITH LAST LABEL IN THE SET. 
C     * AN IMMEDIATE E-O-F RETURNS WITH NLEV=0. 
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      REAL ZX(LR,1) 
C 
      LOGICAL OK,REC
      INTEGER LEV(1),IBUF(8)
      INTEGER IB(8) 
C-------------------------------------------------------------------
C     * GET THE NEXT RECORD THAT IS PART OF A CROSS-SECTION.
C 
      NLEV=0
      CALL RECGET(NF,NC4TO8("ZONL"),-1,-1,-1,IBUF,MAXPK,OK)
      IF(.NOT.OK) RETURN
      INITN=IBUF(3) 
      INITL=IBUF(4) 
C 
C     * READ THE REST OF THE CROSS-SECTION. 
C 
  150 NLEV=NLEV+1 
      LEV(NLEV)=IBUF(4) 
      CALL RECUP2(ZX(1,NLEV),IBUF)
      DO 170 I=1,8
  170 IB(I)=IBUF(I) 
      CALL RECGET(NF,-1,-1,-1,-1,IBUF,MAXPK,REC)
      IF(.NOT.REC) RETURN 
      IF(IBUF(1).NE.NC4TO8("ZONL")) GO TO 210
      IF(IBUF(3).NE.INITN) GO TO 210
      IF(IBUF(4).EQ.INITL) GO TO 210
      GO TO 150 
C 
C     * BACKSPACE AND RESTORE IBUF BEFORE RETURNING IF NOT AT EOF.
C 
  210 BACKSPACE NF
      BACKSPACE NF
      DO 220 I=1,8
  220 IBUF(I)=IB(I) 
      RETURN
      END 
