      SUBROUTINE NOM(NAM,IC,INT)
  
C     * APRIL 26/90 - J. DE GRANDPRE (U.Q.A.M.) 
C     * SUBROUTINE QUI DEFINIE LE NOM DES CHAMPS ASSOCIE AUX DIFFERENTS 
C     * TRACEURS EN FONCTION DU NOMBRE DE TRACEURS. 
  
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      CHARACTER NAM*8 
C---------------------------------------------------------------------- 
      N3=INT/100
      NN=INT-N3*100 
      N2=NN/10
      N1=NN-N2*10 
      IF(INT.GT.999) CALL                           XIT('NOM',-1) 
      WRITE(NAM,1000)IC,N3,N2,N1
  
 1000 FORMAT(A1,3I1,'    ') 
      RETURN
      END 
