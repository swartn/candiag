      SUBROUTINE HRTOLR2(GLR,GCLR,ILG1,ILAT,DLON,DLAT,ILG,
     1                   GHR,GCHR,NLG,NLAT,IOPTION,ICHOICE,IGRID,
     2                   OK,LONBAD,LATBAD,
     3                   GCLRTMP,GCHRTMP,RLON,RLAT,LATL,LATH,LONL,LONH,
     4                   VAL,DIST,LOCAT,IVAL,LOCFST,NMAX)
C     * APR  26/96 - F.MAJAESS(USE WHENEQI INSTEAD OF WHENEQ FOR INTEGER)
C     * NOV 6/95 - M.LAZARE. CHANGE "IGRD" TO "IGRID".
C     * JULY 30/93 - M.LAZARE,CHRISTINE WILLISTON. 
C 
C     * TRANSFORMS DATA FROM AN OFFSET HIGH RESOLUTION GRID(NLG X NLAT)
C     * TO A LOWER RESOLUTION GRID(ILG X ILAT), BY TAKING INTO ACCOUNT 
C     * ALL THE HIGH-RESOLUTION GRID POINTS IN EACH LOWER-RESOLUTION
C     * GRID SQUARE.
C 
C     * THE LONGITUDES AND LATITUDES OF THE HIGH-RESOLUTION FIELD GHR ARE 
C     * CONTAINED IN THE ARRAYS RLON AND RLAT RESPECTIVELY, WHILE THOSE OF THE
C     * LOW-RESOLUTION FIELD GLR ARE CONTAINED IN DLON AND DLAT RESPECTIVELY. 
C 
C     * IF IOPTION.EQ.1: ONLY THOSE HIGH-RESOLUTION POINTS WITH LAND MASK 
C     *              IS THE SAME AS THE LOW-RESOLUTION MASK ARE PROCESSED
C     *              (LIKE FORMER ROUTINE HRALR).
C     * IF IOPTION.EQ.0: THE RESPECTIVE LAND MASKS ARE NOT TAKEN INTO 
C     *              ACCOUNT (LIKE FORMER ROUTINE HRTOLR).
C
C     * IF ICHOICE.EQ.0: THE MOST FREQUENT VALUE IN THE SQUARE IS RETURNED. IF
C     *              THERE IS A TIE, THE VALUE OF THE POINT CLOSEST TO THE
C     *              LOW-RESOLUTION GRID POINT IS RETURNED. 
C     * IF ICHOICE.EQ.1: THE PARTIAL BOX AREA-AVERAGED VALUE IS RETURNED. 
C     * IF ICHOICE.EQ.2: THE FIELD BEING SMOOTHED IS THE GROUND COVER FIELD.
C     *              THIS IS HANDLED AS IN ICHOICE=0, EXCEPT THAT A DECISION
C     *              IS MADE FIRST ON THE MOST FREQUENT VALUE BEING LAND OR 
C     *              WATER (FROZEN OR OPEN). FOR RESULTING NON-LAND POINTS, 
C     *              A FINAL DECISION IS MADE ON WHETHER THE MOST FREQUENT
C     *              VALUE IS WATER OR SEA-ICE. 
C     * IF ICHOICE.EQ.3: THE SIMPLE SUM OF THE CONTRIBUTIONS FROM EACH OF 
C     *              THE HIGH-RESOLUTION VALUES WITHIN THE LOW-RESOLUTION
C     *              GRID SQUARE IS RETURNED.
C
C     * IF IGRID.EQ.0: THE TARGET GRID IS A GAUSSIAN-GRID.
C     * IF IGRID.EQ.1: THE TARGET GRID IS A "REGULAR" LAT-LON GRID WITH
C     *                VALUES DEFINED AT THE POLES, BEGINNING AND ENDING
C     *                AT GREENWICH.
C     * IF IGRID.EQ.2: THE TARGET GRID IS AN "OFFSET" LAT-LON GRID.
C
C     * IF NO HIGH-RESOLUTION POINTS ARE FOUND WITHIN THE LOW-RESOLUTION GRID 
C     * SQUARE, THE SUBROUTINE RETURNS WITH OK=.FALSE. AND PASSES BACK THE
C     * LOCATION OF THE BAD POINT IN (I,J) COORDINATES DEFINED BY 
C     * (LONBAD,LATBAD).
C------------------------------------------------------------------------------ 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      REAL GLR(ILG1,ILAT),GHR(NLG,NLAT),GCLR(ILG1,ILAT),GCHR(NLG,NLAT)
      REAL DLON(ILG),DLAT(ILAT)
C
C     * WORK ARRAYS FOR SUBROUTINE HRTOLR2.
C     * NOTE THAT THE SIZES OF ARRAYS VAL,DIST,LOCAT,IVAL,LOCFST AND
C     * NMAX REALLY REPRESENT THE NUMBER OF HIGH-RESOLUTION GRID 
C     * POINTS WITHIN A LOW-RESOLUTION GRID SQUARE. FOR CONVENIENCE
C     * SAKE, THEY ARE DIMENSIONED WITH SIZE "NLG" AND AN ABORT 
C     * CONDITION IS GENERATED WITHIN THE SUBROUTINE.
C
      REAL GCHRTMP(NLG,NLAT)
      REAL RLON(NLG), RLAT(NLAT)

      REAL GCLRTMP(ILG,ILAT)
      INTEGER LATL(ILAT), LATH(ILAT), LONL(ILG), LONH(ILG)

      REAL VAL(NLG), DIST(NLG) 
      INTEGER LOCAT(NLG), IVAL(NLG), LOCFST(NLG), NMAX(NLG)       
C 
      REAL LATM,LATP,LONM,LONP
      LOGICAL OK
C 
      DATA PI/3.1415926535898E0/
C-------------------------------------------------------------------------------
      LONBAD=0
      LATBAD=0
      OK=.TRUE. 
C    
C     * ABORT IF NUMBER OF HIGH-RESOLUTION POINTS WITHIN LOW-RESOLUTION
C     * GRID SQUARE EXCEEDS THAT DIMENSIONED IN MAIN PROGRAM ("NLG").
C
      NPOINTS=(NLG*NLAT)/(ILG*ILAT)
      IF(NPOINTS.GT.NLG)                     CALL XIT('HRTOLR2',-1)
C
C     * DISALLOWED COMBINATION OF OPTIONS:
C
      IF(IOPTION.EQ.1 .AND. ICHOICE.EQ.2)    CALL XIT('HRTOLR2',-2)
      IF(IOPTION.NE.0 .AND. IOPTION.NE.1)    CALL XIT('HRTOLR2',-3)
      IF(ICHOICE.LT.0 .OR.  ICHOICE.GT.3)    CALL XIT('HRTOLR2',-4)
      IF(IGRID  .LT.0 .OR.  IGRID  .GT.2)    CALL XIT('HRTOLR2',-5)
C 
C     * DEFINE LONGITUDE AND LATITUDE VECTORS FOR HIGH-RESOLUTION GRID. 
C 
      DEGHX=360.E0/(FLOAT(NLG)) 
      DEGHY=180.E0/(FLOAT(NLAT)) 
      DEGHXST=DEGHX*0.5E0
      DEGHYST=DEGHY*0.5E0
C 
      DO  10 I=1,NLG  
   10 RLON(I)=DEGHXST+DEGHX*FLOAT(I-1) 
      DO  20 J=1,NLAT 
   20 RLAT(J)=(-90.E0+DEGHYST)+DEGHY*FLOAT(J-1)
C 
C     * FIND HIGH-RESOLUTION GRID POINTS WITHIN EACH LOW-RESOLUTION GRID SQUARE.
C     * THIS REQUIRES SPECIAL HANDLING OF POINTS NEAR THE POLES OR AT THE 
C     * GREENWICH MERIDIAN. 
C
      DO 40 J=1,ILAT
        IF(J.EQ.ILAT) THEN
          LATP=90.E0
        ELSE
          LATP=0.5E0*(DLAT(J+1)+DLAT(J))
        ENDIF 
        IF(J.EQ.1) THEN 
          LATM=-90.E0 
        ELSE
          LATM=0.5E0*(DLAT(J-1)+DLAT(J))
        ENDIF 
        DO 30 JJ=1,NLAT 
          IF(JJ.NE.1) THEN
            IF(RLAT(JJ-1).LT.LATM.AND.RLAT(JJ).GE.LATM) LATL(J)=JJ
          ELSE
            IF(RLAT(JJ).GE.LATM) LATL(J)=JJ 
          ENDIF 
          IF(JJ.NE.NLAT) THEN 
            IF(RLAT(JJ).LE.LATP.AND.RLAT(JJ+1).GT.LATP) LATH(J)=JJ
          ELSE
            IF(RLAT(JJ).LE.LATP) LATH(J)=JJ 
          ENDIF 
   30   CONTINUE
   40 CONTINUE
C 
      DO 75 I=1,ILG 
        IF(I.EQ.ILG) THEN
          IF(IGRID.EQ.2) THEN
            LONP=360.E0
          ELSE
            LONP=0.5E0*(360.E0+DLON(I))
          ENDIF  
        ELSE
          LONP=0.5E0*(DLON(I+1)+DLON(I))
        ENDIF
        IF(I.EQ.1) THEN 
          IF(IGRID.EQ.2) THEN
            LONM=0.E0
          ELSE
            LONM=0.5E0*(360.E0+DLON(ILG))
          ENDIF
        ELSE
          LONM=0.5E0*(DLON(I-1)+DLON(I))
        ENDIF 
        DO 50 II=1,NLG
          IF(II.NE.1) THEN
            IF(RLON(II-1).LT.LONM.AND.RLON(II).GE.LONM) LONL(I)=II
          ELSE
            IF(RLON(II).GE.LONM) LONL(I)=II 
          ENDIF 
          IF(II.NE.NLG) THEN
            IF(RLON(II).LE.LONP.AND.RLON(II+1).GT.LONP) LONH(I)=II
          ELSE
            IF(RLON(II).LE.LONP) LONH(I)=II         
          ENDIF
   50   CONTINUE
        IF(LONL(I).GT.LONH(I))       LONH(I)=LONH(I)+NLG
   75 CONTINUE
C
C     * DEFINE WORK ARRAYS FOR POSSIBLE SUBSEQUENT USE OF LAND MASK
C     * INFORMATION TO DO "REDUCING". IF IOPTION.EQ.0, THIS INFO IS
C     * NOT USED AND THIS IS DONE TRANSPARENTLY BY SETTING BOTH THE
C     * LOW AND HIGH RESOLUTION TEMPORARY MASKS TO ZERO, IN THAT
C     * THESE WILL AGREE FOR ALL POINTS REGARDLESS OF THEIR VALUE.
C     * IF IOPTION.EQ.1, THESE TEMPORARY ARRAYS WILL CONTAIN THE
C     * NECESSARY LAND MASK INFORMATION TO PROPERLY "REDUCE".
C
      DO 85 J=1,ILAT
        DO 80 I=1,ILG
          IF(IOPTION.EQ.0) THEN
            GCLRTMP(I,J)=0.E0 
          ELSE
            GCLRTMP(I,J)=GCLR(I,J)
          ENDIF
   80   CONTINUE
   85 CONTINUE     
C
      DO 95 JJ=1,NLAT
        DO 90 II=1,NLG
          IF(IOPTION.EQ.0) THEN
            GCHRTMP(II,JJ)=0.E0 
          ELSE
            GCHRTMP(II,JJ)=GCHR(II,JJ)
          ENDIF
   90   CONTINUE
   95 CONTINUE     
C  
C     * LOOP OVER POINTS IN THE LOW-RESOLUTION FIELD, KEEPING TRACK OF ALL
C     * HIGH-RESOLUTION POINTS WITHIN EACH LOW-RESOLUTION GRID SQUARE 
C     * HAVING THE SAME GROUND COVER AS THE LOW-RESOLUTION GRID SQUARE. 
C 
      DO 900 J=1,ILAT 
        FACTL=(COS(DLAT(J)*PI/180.E0))**2 
        DO 800 I=1,ILG
          NLPTS=0 
          IF(ICHOICE.EQ.1) THEN 
C 
C           * FOR CONTINUOUS FIELDS, AREA-AVERAGE THESE POINTS. 
C 
            BIGAREA=0.E0
            SUM=0.E0
            DO 200 JJ=LATL(J),LATH(J) 
              IF(JJ.EQ.NLAT) THEN 
                SLATP=90.E0 
              ELSE
                SLATP=0.5E0*(RLAT(JJ+1)+RLAT(JJ)) 
              ENDIF 
              IF(JJ.EQ.1) THEN
                SLATM=-90.E0
              ELSE
                SLATM=0.5E0*(RLAT(JJ-1)+RLAT(JJ)) 
              ENDIF 
              FACTLAT=(COS(RLAT(JJ)*PI/180.E0))*(SLATP-SLATM)*PI/180.E0 
              DO 100 LL=LONL(I),LONH(I) 
                IF(LL.GT.(NLG+1))THEN
                  II=LL-NLG
                  IIP1=II+1
                  IIM1=II-1
                  SLONP=0.5E0*(RLON(IIP1)+RLON(II))
                  SLONM=0.5E0*(RLON(IIM1)+RLON(II))
                ELSE IF(LL.EQ.(NLG+1))THEN
                  II=LL-NLG
                  IIP1=II+1
                  SLONP=0.5E0*(RLON(IIP1)+RLON(II))
                  SLONM=0.E0
                ELSE IF(LL.EQ.NLG)THEN
                  II=LL
                  IIM1=II-1
                  SLONP=360.E0
                  SLONM=0.5E0*(RLON(IIM1)+RLON(II))
                ELSE IF(LL.EQ.1)THEN
                  II=LL
                  IIP1=II+1
                  SLONP=0.5E0*(RLON(IIP1)+RLON(II))
                  SLONM=0.E0
                ELSE 
                  II=LL
                  IIP1=II+1
                  IIM1=II-1
                  SLONP=0.5E0*(RLON(IIP1)+RLON(II))
                  SLONM=0.5E0*(RLON(IIM1)+RLON(II))
                ENDIF
                IF(GCHRTMP(II,JJ).EQ.GCLRTMP(I,J)) THEN
                  NLPTS=NLPTS+1
                  AREA=FACTLAT*((SLONP-SLONM)*PI/180.E0)
                  SUM=SUM+AREA*GHR(II,JJ) 
                  BIGAREA=BIGAREA+AREA
                ENDIF
  100         CONTINUE
  200       CONTINUE
            IF(NLPTS.NE.0) THEN 
              GLR(I,J)=SUM/BIGAREA
            ENDIF 
C
          ELSE IF(ICHOICE.EQ.3) THEN
C
C         * RETURN THE SIMPLE SUM OF ALL CONTRIBUTIONS OF HIGH-
C         * RESOLUTION GRID POINTS WITHIN LOW-RESOLUTION GRID
C         * SQUARE.
C
            SUM=0.E0
            DO 275 JJ=LATL(J),LATH(J) 
              DO 250 LL=LONL(I),LONH(I) 
                IF(LL.GT.NLG) THEN
                  II=LL-NLG 
                ELSE
                  II=LL 
                ENDIF 
                IF(GCHRTMP(II,JJ).EQ.GCLRTMP(I,J)) THEN
                  NLPTS=NLPTS+1
                  SUM=SUM+GHR(II,JJ) 
                ENDIF
 250          CONTINUE
 275        CONTINUE   
            IF(NLPTS.NE.0) THEN 
              GLR(I,J)=SUM
            ENDIF 
C
          ELSE
C 
C           * FOR DISCRETE-VALUED FIELDS, KEEP TRACK OF EACH QUALIFYING HIGH- 
C           * RESOLUTION POINT VALUE IN ARRAY VAL, AND THEIR DISTANCE FROM THE
C           * LOW-RESOLUTION GRID POINT IN ARRAY DIST.
C 
            DO 400 JJ=LATL(J),LATH(J) 
              DISTLAT=DLAT(J)-RLAT(JJ)
              DO 300 LL=LONL(I),LONH(I) 
                IF(LL.GT.NLG) THEN
                  II=LL-NLG 
                ELSE
                  II=LL 
                ENDIF 
                IF(GCHRTMP(II,JJ).EQ.GCLRTMP(I,J)) THEN
                  NLPTS=NLPTS+1
                  VAL(NLPTS)=GHR(II,JJ) 
                  DISTRY=ABS(DLON(I)-RLON(II))
                  DISTLON=MIN(DISTRY,(360.E0-DISTRY)) 
                  DIST(NLPTS)=(FACTL*(DISTLON**2)+DISTLAT**2)*
     1                        ((PI/180.E0)**2)
                ENDIF
  300         CONTINUE
  400       CONTINUE
          ENDIF
C 
C         * RETURN WITH OK=.FALSE. IF NO HIGH-RESOLUTION POINTS IN
C         * LOW-RESOLUTION GRID SQUARE DEFINED BY (LONBAD,LATBAD)=(I,J).
C 
          IF(NLPTS.EQ.0) THEN 
            OK=.FALSE.
            LONBAD=I
            LATBAD=J
            RETURN
          ENDIF 
C 
C         * THE REST OF DO-LOOP 800 IS CONCERNED WITH THE MOST FREQUENTLY-
C         * OCCURRING CASES.
C 
          IF(ICHOICE.NE.1 .AND. ICHOICE.NE.3) THEN
            IF(NLPTS.EQ.1) THEN
C
C             * IF ONLY ONE HIGH-RESOLUTION GRID POINT EXISTS, RETURN ITS VALUE.
C
              GLR(I,J)=VAL(1) 
            ELSE
C 
C             * CALCULATE NUMBER OF DISTINCT-VALUED POINTS (DEFINED BY NT), THEIR 
C             * FIRST-OCCURRING LOCATION (DEFINED BY ARRAY LOCFST) AND THE
C             * ASSOCIATED LOCATIONS WHERE THEY OCCUR IN ARRAY VAL (DEFINED BY
C             * ARRAY NMAX).
C 
              NEWMAX=0
              IF(ICHOICE.EQ.2) THEN 
C 
C               * THE FIELD BEING SMOOTHED IS GROUND COVER AND REQUIRES SPECIAL 
C               * TREATMENT FOR LOW-RESOLUTION GRID SQUARES HAVING ALL THREE TYPES
C               * OF GROUND COVER. IF THE MOST FREQUENTLY-OCCURRING VALUE IS LAND 
C               * BUT THERE ARE MORE (WATER+ICE) POINTS THAN LAND, THE RESULTING
C               * LOW-RESOLUTION POINT SHOULD BE RETURNED AS THE MORE FREQUENT
C               * VALUE OF WATER VERSUS ICE.
C 
                NT=0
                CALL WHENEQ(NLPTS,VAL,1, 0.E0,IVAL,NVALW) 
                IF(NVALW.NE.0) THEN 
                  NT=NT+1 
                  NMAX(NT)=NVALW
                  LOCFST(NT)=IVAL(1)
                ENDIF 
                CALL WHENEQ(NLPTS,VAL,1,+1.E0,IVAL,NVALI) 
                IF(NVALI.NE.0) THEN 
                  NT=NT+1 
                  NMAX(NT)=NVALI
                  LOCFST(NT)=IVAL(1)
                ENDIF 
                CALL WHENEQ(NLPTS,VAL,1,-1.E0,IVAL,NVALL) 
                IF(NVALL.NE.0) THEN 
                  NT=NT+1 
                  NMAX(NT)=NVALL
                  LOCFST(NT)=IVAL(1)
                ENDIF 
                MAXINT=MAX(NVALL,NVALW)
                NEWMAX=MAX(MAXINT,NVALI) 
                IF(NT.EQ.3.AND.(NVALL.LT.(NVALW+NVALI)).AND.NVALL.EQ. 
     1             NEWMAX) THEN 
                  NT=2
                  NEWMAX=MAX(NVALW,NVALI)
                ELSE IF(NT.EQ.3.AND.(NVALL.EQ.(NVALW+NVALI)).AND.
     1             NVALL.EQ.NEWMAX) THEN 
                  NT=3
                  NMAX(1)=NVALL 
                  NMAX(2)=NVALL 
                  NMAX(3)=NVALL 
                ENDIF 
              ELSE  
C 
C               * DO THE GENERAL CALCULATION. 
C 
                DO 500 N=1,NLPTS
                  IF(N.EQ.1) THEN 
                    NT=0
                    NUM=0 
                  ELSE
                    CALL WHENEQ(N-1,VAL,1,VAL(N),IVAL,NUM)
                  ENDIF 
                  IF(NUM.EQ.0) THEN 
                    NT=NT+1 
                    CALL WHENEQ(NLPTS,VAL,1,VAL(N),IVAL,NVAL) 
                    LOCFST(NT)=N
                    NMAX(NT)=NVAL 
                    IF(NVAL.GT.NEWMAX) NEWMAX=NVAL
                  ENDIF 
  500           CONTINUE
              ENDIF 
C 
C             * IF ONLY ONE VALUE EXISTS IN THE LOW-RESOLUTION GRID SQUARE, 
C             * RETURN ITS VALUE. 
C 
              IF(NT.EQ.1) THEN
                GLR(I,J)=VAL(LOCFST(1)) 
              ELSE
C 
C               * IF ONLY ONE UNIQUE-VALUED POINT HAS THE MOST FREQUENT OCCURRENCE
C               * NEWMAX, RETURN ITS VALUE. 
C 
                CALL WHENEQI(NT,NMAX,1,NEWMAX,IVAL,NVAL) 
                IF(NVAL.EQ.1) THEN
                  GLR(I,J)=VAL(LOCFST(IVAL(1))) 
                ELSE
C 
C                 * OTHERWISE, SEARCH THROUGH THE UNIQUE-VALUED POINTS TO 
C                 * DETERMINE THE VALUES HAVING THE MOST FREQUENT OCCURRENCE AS 
C                 * NEWMAX AND THEIR LOCATION IN ARRAY VAL (DEFINED BY ARRAY
C                 * LOCAT). 
C 
                  NPP=0 
                  DO 600 N=1,NT 
                    IF(NMAX(N).EQ.NEWMAX) THEN
                      TEMP=VAL(LOCFST(N)) 
                      CALL WHENEQ(NLPTS,VAL,1,TEMP,IVAL,NVAL) 
                      DO 550 IJK=1,NVAL 
                        NPP=NPP+1 
                        LOCAT(NPP)=IVAL(IJK)
  550                 CONTINUE
                    ENDIF 
  600             CONTINUE
C 
C                 * CHOOSE THE CLOSEST POINT TO THE TARGET HAVING THAT MAXIMUM- 
C                 * OCCURRING VALUE.
C 
                  DISTMIN=1.E20 
                  DO 700 N=1,NPP
                    IF(DIST(LOCAT(N)).LT.DISTMIN) THEN
                      DISTMIN=DIST(LOCAT(N))
                      NCHOICE=LOCAT(N)
                    ENDIF 
  700             CONTINUE
                  GLR(I,J)=VAL(NCHOICE) 
                ENDIF 
              ENDIF 
            ENDIF
          ENDIF
  800   CONTINUE
C
C       * REPEAT GREENWICH MERIDIAN AS EXTRA CYCLIC LONGITUDE, IF
C       * TARGET GRID IS NOT OF THE OFFSET TYPE.
C
        IF(IGRID.NE.2) GLR(ILG+1,J)=GLR(1,J)
  900 CONTINUE
C
      RETURN
      END
