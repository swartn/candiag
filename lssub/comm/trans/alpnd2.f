      SUBROUTINE ALPND2(DALP,ALP,NLP,ILAT,EPSI) 
C 
C     * JUL 14/92 - E. CHAN (ADD REAL*8 DECLARATIONS)
C     * JUL 12/78 - J.D.HENDERSON 
C     * COMPUTES DERIVATIVES OF LEGENDRE POLYNOMIALS. 
C 
C     * ALP(NLP,ILAT) = LEGENDRE POLYNOMIALS (NLP VALUES AT ILAT LATS). 
C     * DALP(NLP,ILAT) = CORRESPONDING DERIVATIVES OF THE POLYNOMIALS.
C 
C     * LEVEL 2,ALP,DALP
      IMPLICIT REAL*8 (A-H,O-Z),
     +INTEGER (I-N)
      REAL*8 DALP(NLP,ILAT),ALP(NLP,ILAT) 
      REAL*8 EPSI(NLP)
C-------------------------------------------------------------------- 
C 
C     * PRECOMPUTE CONSTANTS IN EPSI. 
C 
      NLPM=NLP-1
      EPSI(1)=0.E0
      DO 110 N=2,NLP
      NS=N-1
  110 EPSI(N)=NS/SQRT(4.E0*NS*NS-1.E0)
C 
      DO 210 J=1,ILAT 
      DALP(1,J)=0.E0
      DALP(NLP,J)=0.E0
      DO 210 N=2,NLPM 
      FNS=FLOAT(N-1)
      DALP(N,J)=(FNS+1.E0)*EPSI(N)*ALP(N-1,J)-FNS*EPSI(N+1)*ALP(N+1,J)
  210 CONTINUE
C 
      RETURN
      END 
