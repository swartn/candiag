      SUBROUTINE ALPDL2(DELALP,ALP,LSR,LM)
C 
C     * JUL 14/92 - E. CHAN (ADD REAL*8 DECLARATIONS)
C     * JUL 17/79 - J.D.HENDERSON 
C     * COMPUTES LAPLACIAN OF LEGENDRE POLYNOMIALS. 
C     * LSR CONTAINS ROW LENGTH INFO. 
C 
C     * LEVEL 2,DELALP,ALP
      IMPLICIT REAL*8 (A-H,O-Z),
     +INTEGER (I-N)
      REAL*8 DELALP(1),ALP(1) 
      INTEGER LSR(2,1)
C-------------------------------------------------------------------- 
C 
      DO 210 M=1,LM 
      KL=LSR(2,M) 
      KR=LSR(2,M+1)-1 
      DO 210 K=KL,KR
      NS=(M-1)+(K-KL) 
      FNS=FLOAT(NS) 
      DELALP(K)=-FNS*(FNS+1.E0)*ALP(K)
  210 CONTINUE
C 
      RETURN
      END 
