#define FNAME1 CONCAT(checksum_array,ARRAYKIND)
#define FNAME  CONCAT(FNAME1,1d)

!> Checksum a 1d array of type ARRAYTYPE and kind ARRAYKIND
integer(kind=int64) function FNAME( array, is_in, ie_in, halo ) result (chksum)
  ARRAYTYPE(kind=ARRAYKIND), dimension(:) :: array   !< Array to be checksummed
  integer, optional          :: is_in   !< Starting index of array along i-dimension
  integer, optional          :: ie_in   !< Ending index of array along i-dimension
  integer, optional          :: halo    !< The size of the halo

  integer :: is, ie
  logical :: indices_present

  indices_present = present(is_in) .or. present(ie_in)

  if (present(halo) .and. indices_present) &
    error stop "Checksumming accepts only index range OR halo size, and both were supplied"

  is = LBOUND(array,1)
  ie = UBOUND(array,1)

  if (present(is_in)) is = is_in
  if (present(ie_in)) ie = ie_in

  if (present(halo)) then
    is = is + halo
    ie = ie - halo
  endif

  chksum = SUM(bitcount(array(is:ie)))

end function FNAME

#undef ARRAYTYPE
#undef ARRAYKIND
#undef FNAME1
#undef FNAME