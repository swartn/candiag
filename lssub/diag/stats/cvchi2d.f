      FUNCTION CVCHI2D(P,DF)
C
C     * CVCHI2D RETURNS P UPPER TAIL CRITICAL VALUE OF THE 
C     * CHI^2-DISTRIBUTION WITH DF DEGREES OF FREEDOM.
C
C     * 7/APR 1999
C
C     * AUTHOR: SLAVA KHARIN
C     * (BISECTION METHOD)
C--------------------------------------------------------------------
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      DATA MAXIT/100/,EPS/1.E-6/
C--------------------------------------------------------------------
      T1=0.E0
      T2=1.E+3
      P1=GAMMP1(0.5E0*DF,0.5E0*T1)
      P2=GAMMP1(0.5E0*DF,0.5E0*T2)
      IF ((P1-P)*(P2-P).GT.0.E0) THEN
         WRITE(6,'(A)')
     1        ' *** ERROR IN CVCHI2D: ROOT MUST BE BRACKETED.'
         CALL                                      XIT('CVCHI2D',-1)
      ENDIF
      DO IT=1,MAXIT
         T=0.5E0*(T1+T2)
         P1=GAMMP1(0.5E0*DF,0.5E0*T)
         IF (P1.GT.P) THEN
            T2=T
         ELSE
            T1=T
         ENDIF
         IF (T2-T1.LT.EPS) GOTO 100
      ENDDO
C
      WRITE(6,'(A)')
     1     ' *** ERROR IN CVCHI2D: TOO MANY BISECTIONS.'
      CALL                                         XIT('CVCHI2D',-2)
C
 100  CVCHI2D=T
      RETURN
      END
