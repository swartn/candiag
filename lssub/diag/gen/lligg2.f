      SUBROUTINE LLIGG2(GG,ILG1,ILAT,DLAT, GLL,NLG1,NLAT,INTERP)
C 
C     * APR 30/80 - J.D.HENDERSON 
C     * INTERPOLATES GLOBAL GAUSSIAN GRID GG(ILG1,ILAT) 
C     * FROM GLOBAL LAT-LONG GRID GLL(NLG1,NLAT). 
C     * DLAT = LAT (DEG) OF GG ROWS.
C     * INTERP = (1,3) FOR (LINEAR,CUBIC) INTERPOLATION.
C     *          (OTHERWISE THE GRID GG IS SET TO ZERO).
C 
C     * LEVEL 2,GG,GLL
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      REAL GG(ILG1,ILAT),GLL(NLG1,NLAT) 
      REAL DLAT(ILAT) 
C-------------------------------------------------------------------- 
      ILG=ILG1-1
      DEGX=360.E0/FLOAT(NLG1-1) 
      DEGY=180.E0/FLOAT(NLAT-1) 
C 
C     * ROWS ARE DONE FROM SOUTH TO NORTH.
C     * DROW = DEGREES OF ROW J FROM S POLE.
C     * (X,Y) = COORD OF GG(I,J) IN LAT-LONG GRID.
C 
      DO 240 J=1,ILAT 
      DROW=DLAT(J)+90.E0
      Y=DROW/DEGY+1.E0
C 
C     * POINTS ARE INTERPOLATED FROM LEFT TO RIGHT. 
C     * DLON = DEGREES OF POINT I EAST FROM GREENWICH.
C 
      DO 220 I=1,ILG
      DLON=FLOAT(I-1)/FLOAT(ILG)*360.E0 
      X=DLON/DEGX+1.E0
      VAL=0.E0
      IF(INTERP.EQ.1) CALL GINTL2(VAL,GLL,NLG1,NLAT,X,Y)
      IF(INTERP.EQ.3) CALL GINTC2(VAL,GLL,NLG1,NLAT,X,Y)
      GG(I,J)=VAL 
  220 CONTINUE
C 
      GG(ILG1,J)=GG(1,J)
  240 CONTINUE
C 
      RETURN
      END 
