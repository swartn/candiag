      SUBROUTINE UPRTLWR (STRING,LENGTH)
  
C     * SEP 10/91 - F. MAJAESS
C     * CONVERTS "LENGTH" ASCII CHARACTERS IN "STRING" FROM 
C     * UPPER TO LOWER CASE.
  
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      CHARACTER*(*) STRING
C---------------------------------------------------------------- 
      DO 10 I=1,LENGTH
         IF(ICHAR (STRING(I:I)) .GE. 65 .AND. 
     +      ICHAR (STRING(I:I)) .LE. 90                ) THEN 
            STRING(I:I)=CHAR ( ICHAR(STRING(I:I)) + 32 )
         ENDIF
   10 CONTINUE
C---------------------------------------------------------------- 
      RETURN
      END 
