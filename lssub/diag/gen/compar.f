      SUBROUTINE COMPAR(OK,I,J,L) 
C 
C     * FEB 11/83 - R.LAPRISE.
C 
C     * RETURN OK=.TRUE. IF 2 VECTORS I AND J, OF LENGTH L, 
C     * ARE IDENTICAL.
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      LOGICAL OK
C 
      INTEGER I(L), J(L)
C-----------------------------------------------------------------------
      OK=.TRUE. 
      DO 100 N=1,L
  100 OK=OK .AND. (I(N).EQ.J(N))
      RETURN
      END 
