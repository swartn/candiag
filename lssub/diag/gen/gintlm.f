      SUBROUTINE GINTLM(VAL,F,NI,NJ,FI,FJ,SPVAL)
C 
C     * APR 14/97 - F.MAJAESS (MODIFIED FOR TOLERANCE IN SPVAL)
C     * FEB 25/93 - F.MAJAESS (MODIFIED FROM GINTL2 ROUTINE) 
C     * LINEAR INTERPOLATION AT POINT (FI,FJ) IN F(NI,NJ) 
C     * PROVIDED THE NEEDED GRID POINTS FOR THE COMPUTATION
C     * ARE DIFFERENT FROM THE MASK VALUE "SPVAL".
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      REAL F(NI,NJ), SPVAL
C-------------------------------------------------------------------- 
      VAL=SPVAL
      SPVALT=1.E-6*SPVAL
      I=INT(FI)
      J=INT(FJ)
      IF(1.0E0*I.EQ.FI.AND.1.0E0*J.EQ.FJ) THEN 
C     * GG POINT COINCIDE WITH LL-GRID POINT.
       IF(ABS(F(I,J)-SPVAL).GT.SPVALT) THEN 
        VAL=F(I,J)
       ENDIF
      ELSE
       IF(1.0E0*I.EQ.FI) THEN 
C      * LONGITUDE COINCIDE FOR GG AND LL-GRID.
        IF(J.LT.1) J=1
        IF(J.GE.NJ) J=NJ-1
        Y=FJ-FLOAT(J) 
C       IF(F(I,J).NE.SPVAL.AND.F(I,J+1).NE.SPVAL)
        IF(ABS(F(I,J)  -SPVAL).GT.SPVALT.AND.
     +     ABS(F(I,J+1)-SPVAL).GT.SPVALT)
     +    VAL=(1.E0-Y)*F(I,J)+Y*F(I,J+1)
       ELSE
        IF(1.0E0*I.EQ.FI) THEN 
C       * LATITUDE COINCIDE FOR GG AND LL-GRID.
          IF(I.LT.1) I=1
          IF(I.GE.NI) I=NI-1
          X=FI-FLOAT(I) 
C         IF(F(I,J).NE.SPVAL.AND.F(I+1,J).NE.SPVAL)
          IF(ABS(F(I,J)  -SPVAL).GT.SPVALT.AND.
     +       ABS(F(I+1,J)-SPVAL).GT.SPVALT)
     +      VAL=(1.E0-X)*F(I,J)+X*F(I+1,J)
        ELSE
C         * NEITHER LATITUDES NOR LONGITUDES COINCIDE
          IF(I.LT.1) I=1
          IF(I.GE.NI) I=NI-1
          X=FI-FLOAT(I) 
          IF(J.LT.1) J=1
          IF(J.GE.NJ) J=NJ-1
          Y=FJ-FLOAT(J) 
C         IF(F(I,J)  .NE.SPVAL.AND.F(I+1,J)  .NE.SPVAL.AND.
C    +       F(I,J+1).NE.SPVAL.AND.F(I+1,J+1).NE.SPVAL) THEN
          IF(ABS(F(I,J)    -SPVAL).GT.SPVALT.AND.
     +       ABS(F(I+1,J)  -SPVAL).GT.SPVALT.AND.
     +       ABS(F(I,J+1)  -SPVAL).GT.SPVALT.AND.
     +       ABS(F(I+1,J+1)-SPVAL).GT.SPVALT) THEN
C 
            BOT=(1.E0-X)*F(I,J)  +X*F(I+1,J)
            TOP=(1.E0-X)*F(I,J+1)+X*F(I+1,J+1)
C 
            VAL=(1.E0-Y)*BOT+Y*TOP
          ENDIF
        ENDIF
       ENDIF
      ENDIF
C 
      RETURN
      END
