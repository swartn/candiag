      SUBROUTINE DDDT (  SCU,  SCV,  SCW,  SCD,  SCP,  SCA, 
     1                     U,    V,    W,    D,    P, 
     2                  ULNG, VLNG, WLNG, DLNG, 
     3                  ULAT, VLAT, WLAT, DLAT, UP, VP, DP, ANS,
     4                    UF,   VF,   WF,   DF, 
     5                 UFLNG,VFLNG,WFLNG,DFLNG, 
     6                 UFLAT,VFLAT,WFLAT,DFLAT,UFP,VFP,DFP, PR, 
     7                   ALP, DALP, EPSI, 
     8                  LSR, LRW, S, WEIGHT, TRIGS, IFAX, 
     9                  ILONG, NLAT, NLEV, ILH, MAXLG, LA, LAW, LM, 
     A                  WRKS                                      ) 
  
C     * M.LAZARE - AUGUST 13/90.  REMOVE HARD-COAT ON "WRKS" BY PASSING 
C     *                           IT IN CALL FROM PROGRAM SPDDDT. 
C     *                           ALSO REMOVE OTHER HARD-COATS AS WELL
C     *                           SINCE ALL ARRAYS ARE PASSED.
C     * B.DUGAS - JANVIER 28/85.
C 
C     * THIS ROUTINE COMPUTES THE TIME TENDENCY OF THE DIVERGENCE 
C     * FROM BIGU, BIGV, OMEGA, DIV AND VORT. THE GEOPOTENTIAL TERM 
C     * IS LEFT OUT. THE REAL SLICES AND FOURIER SERIES SHOULD BE 
C     * EQUIVALENCED IN THE CALLING PROGRAM IN ORDER TO SAVE SPACE. 
C     * THE ROUTINE ALSO SUPPOSES A CERTAIN ORDER IN THE SPECTRAL 
C     * AND FOURIER DATA FIELDS IN MEMORY. THAT ORDER IS THE ONE
C     * FOUND IN THE ROUTINE'S CALLING SEQUENCE.
  
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      COMPLEX SCU(  LAW, 1),SCV(  LAW, 1),SCW(   LA, 1) 
      COMPLEX SCD(   LA, 1),SCP(   LA, 1),SCA(   LA, 1) 
      COMPLEX    UF(ILH,*),   VF(ILH,*),   WF(ILH,1)
      COMPLEX UFLNG(ILH,1),VFLNG(ILH,1),WFLNG(ILH,1)
      COMPLEX    DF(ILH,*),DFLNG(ILH,1),DFLAT(ILH,1)
      COMPLEX UFLAT(ILH,1),VFLAT(ILH,1),WFLAT(ILH,1)
      COMPLEX   UFP(ILH,*),  VFP(ILH,*),  DFP(ILH,*)
  
      DIMENSION    U(MAXLG,1),   V(MAXLG,1),   W(MAXLG,1) 
      DIMENSION ULNG(MAXLG,1),VLNG(MAXLG,1),WLNG(MAXLG,1) 
      DIMENSION    D(MAXLG,1),DLNG(MAXLG,1),DLAT(MAXLG,1) 
      DIMENSION ULAT(MAXLG,1),VLAT(MAXLG,1),WLAT(MAXLG,1) 
      DIMENSION   UP(MAXLG,1),  VP(MAXLG,1),  DP(MAXLG,1) 
      DIMENSION  ANS(MAXLG,1),   P(MAXLG,1) 
  
      DIMENSION ALP(*),DALP(*),EPSI(*),TRIGS(*),IFAX(*) 
      DIMENSION S(*),WEIGHT(*)
      DIMENSION LSR(2,1),LRW(2,1),WRKS(*) 
      DIMENSION PR(*) 
  
      DATA TWOMEG / 1.4584E-4 / 
C------------------------------------------------------------------------ 
      IR=LM-1 
      NLEV1=NLEV-1
      NLEV2=NLEV*2
      NLEV3=NLEV*3
      NLEV16=NLEV*16
  
C     * LOOP OVER TRANSFORM GRID LATITUDES
  
      DO 600 IH=1,NLAT
         CALL ALPST2(ALP,LRW,LM,S(IH),EPSI) 
         CALL ALPDY2(DALP,ALP,LRW,LM,EPSI)
  
C     * COMPUTE FOURIER COEFFICIENTS, DOING UF AND VF, THEN WF, DF
C     * AND P AND SO-ON ... 
  
         CALL STAF (UF,SCU,LRW,LM,LAW,ILH,NLEV2,ALP)
         CALL STAF (WF,SCW,LSR,LM, LA,ILH,NLEV3,ALP)
         CALL STAF (UFLNG,SCU,LRW,LM,LAW,ILH,NLEV2,DALP)
         CALL STAF (WFLNG,SCW,LSR,LM, LA,ILH,NLEV2,DALP)
  
         DO 520 L=1,NLEV
            DO 520 M1=1,LM
               AM=FLOAT(M1-1) 
               UFLAT(M1,L)=CMPLX(-AM* IMAG(UF(M1,L)),AM*REAL(UF(M1,L))) 
               VFLAT(M1,L)=CMPLX(-AM* IMAG(VF(M1,L)),AM*REAL(VF(M1,L))) 
               WFLAT(M1,L)=CMPLX(-AM* IMAG(WF(M1,L)),AM*REAL(WF(M1,L))) 
               DFLAT(M1,L)=CMPLX(-AM* IMAG(DF(M1,L)),AM*REAL(DF(M1,L))) 
  520    CONTINUE 
  
         DO 530 K=2,NLEV1 
            DO 530 M=1,LM 
               UFP(M,K)=(UF(M,K+1)-UF(M,K-1))/(PR(K+1)-PR(K-1)) 
               VFP(M,K)=(VF(M,K+1)-VF(M,K-1))/(PR(K+1)-PR(K-1)) 
               DFP(M,K)=(DF(M,K+1)-DF(M,K-1))/(PR(K+1)-PR(K-1)) 
  530    CONTINUE 
  
         DO 535 M=1,LM
            UFP(M,1)=(UF(M,2)-UF(M,1))/(PR(2)-PR(1))
            VFP(M,1)=(VF(M,2)-VF(M,1))/(PR(2)-PR(1))
            DFP(M,1)=(DF(M,2)-DF(M,1))/(PR(2)-PR(1))
            UFP(M,NLEV)=(UF(M,NLEV)-UF(M,NLEV1))/(PR(NLEV)-PR(NLEV1)) 
            VFP(M,NLEV)=(VF(M,NLEV)-VF(M,NLEV1))/(PR(NLEV)-PR(NLEV1)) 
            DFP(M,NLEV)=(DF(M,NLEV)-DF(M,NLEV1))/(PR(NLEV)-PR(NLEV1)) 
  535    CONTINUE 
  
C     * COMPUTE GRID POINT VALUES FOR ALL FIELDS AT THE SAME TIME.
  
         CALL FFGFW (U,MAXLG,UF,ILH,IR,ILONG,WRKS,NLEV16,IFAX,TRIGS)
  
         COSSQ=1.E0-S(IH)*S(IH) 
         SINLAT=S(IH) 
         DO 540 L=1,NLEV
  
C     * COMPUTE TENDENCY ON THE GRID
  
            DO 540 I=1,ILONG
  
       ANS(I,L)=-(U(I,L)*DLAT(I,L)+V(I,L)*DLNG(I,L))/COSSQ-W(I,L)*
     1 DP(I,L)-D(I,L)*D(I,L)-(WLAT(I,L)*UP(I,L)+WLNG(I,L)*VP(I,L))/COSSQ
     2 -2.E0*(VLAT(I,L)*(ULNG(I,L)+U(I,L)*SINLAT)-ULAT(I,L)*(VLNG(I,L)+ 
     3 V(I,L)*SINLAT))/(COSSQ*COSSQ)-2.E0*SINLAT*(U(I,L)*(ULNG(I,L)+
     4 U(I,L)*SINLAT)+V(I,L)*(VLNG(I,L)+V(I,L)*SINLAT))/(COSSQ*COSSQ)-
     5 (U(I,L)*U(I,L)+V(I,L)*V(I,L))/COSSQ-TWOMEG*(U(I,L)-P(I,L)*SINLAT)
  
  540    CONTINUE 
  
C     * COMPUTE FOURIER COEFFICIENTS
  
         CALL FFWFG (ANS,ILH,ANS,MAXLG,IR,ILONG,WRKS,NLEV,IFAX,TRIGS) 
  
C     * COMPUTE SPHERICAL HARMONIC COEFFICIENTS 
  
         WLJ=WEIGHT(IH) 
         DO 550 L=1,LAW 
            ALP(L)=ALP(L)*WLJ 
  550    CONTINUE 
  
         CALL FAST (SCA,ANS,LSR,LM,LA,ILH,NLEV,ALP) 
  
  600 CONTINUE
  
C---------------------------------------------------------------------- 
      RETURN
      END 
