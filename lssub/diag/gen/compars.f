      SUBROUTINE COMPARS(OK,S1,S2,L) 
C 
C     * MAR 07/2003 - F.MAJAESS.
C 
C     * RETURN OK=.TRUE. IF 2 CHARACTER ARRAYS S1 AND S2, OF LENGTH L, 
C     * ARE IDENTICAL.
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      LOGICAL OK
C 
      CHARACTER*1 S1(L),S2(L)
C-----------------------------------------------------------------------
      OK=.TRUE. 
      DO 100 N=1,L
  100 OK=OK .AND. (S1(N).EQ.S2(N))
      RETURN
      END 
