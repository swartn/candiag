      SUBROUTINE CALPHA(G,LSR,LM,LA)
C 
C     * AUGUST 21, 1979 - TED SHEPHERD. 
C     * SUBROUTINE RETURNS G(K)=C-ALPHA=NS*(NS+1).
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      INTEGER LSR(2,1)
C     * LEVEL 2,G 
      COMPLEX G(LA) 
C-----------------------------------------------------------------------
      DO 100 M=1,LM 
      KL=LSR(1,M) 
      KR=LSR(1,M+1)-1 
      DO 100 K=KL,KR
      NS=(M-1)+(K-KL) 
      GR=FLOAT(NS*(NS+1)) 
      GI=0.E0 
  100 G(K)=CMPLX(GR,GI) 
      RETURN
      END
