      SUBROUTINE GINTL2(VAL,F,NI,NJ,FI,FJ)
C 
C     * LINEAR INTERPOLATION AT POINT (FI,FJ) IN F(NI,NJ).
C     * EXTRAPOLATION IS DONE IF POINT IS OUTSIDE THE GRID
C 
C     * LEVEL 2,F 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      REAL F(NI,NJ) 
C-------------------------------------------------------------------- 
      I=INT(FI) 
      IF(I.LT.1) I=1
      IF(I.GE.NI) I=NI-1
      X=FI-FLOAT(I) 
      J=INT(FJ) 
      IF(J.LT.1) J=1
      IF(J.GE.NJ) J=NJ-1
      Y=FJ-FLOAT(J) 
C 
      BOT=(1.E0-X)*F(I,J)  +X*F(I+1,J)
      TOP=(1.E0-X)*F(I,J+1)+X*F(I+1,J+1)
C 
      VAL=(1.E0-Y)*BOT+Y*TOP
C 
      RETURN
      END 
