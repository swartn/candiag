      SUBROUTINE DVDT (  SCU,  SCV,  SCW,  SCP,  SCD,  SCA, 
     1                     U,    V,    W,    P,    D, 
     2                              WLNG, PLNG, 
     3                              WLAT, PLAT, UP, VP, PP, ANS,
     4                    UF,   VF,   WF,   PF, 
     5                             WFLNG,PFLNG, 
     6                             WFLAT,PFLAT,UFP,VFP,PFP,PR,ALP,
     7                  DALP, EPSI, LSR, LRW, S, WEIGHT, TRIGS, IFAX, 
     8                  ILONG, NLAT, NLEV, ILH, MAXLG, LA, LAW, LM, 
     9                  WRKS                                         )
  
C     * M.LAZARE - AUGUST 13/90.  REMOVE HARD-COAT ON "WRKS" BY PASSING 
C     *                           IT IN CALL FROM PROGRAM SPDDDT. 
C     *                           ALSO REMOVE OTHER HARD-COATS AS WELL
C     *                           SINCE ALL ARRAYS ARE PASSED.
C     * B.DUGAS - JANVIER 28/85.
C 
C     * THIS ROUTINE COMPUTES THE TIME TENDENCY OF THE VORTICITY
C     * FROM BIGU, BIGV, OMEGA, VORT AND DIV. 
C     * THE REAL SLICES AND FOURIER SERIES SHOULD BE
C     * EQUIVALENCED IN THE CALLING PROGRAM IN ORDER TO SAVE SPACE. 
C     * THE ROUTINE ALSO SUPPOSES A CERTAIN ORDER IN THE SPECTRAL 
C     * AND FOURIER DATA FIELDS IN MEMORY. THAT ORDER IS THE ONE
C     * FOUND IN THE ROUTINE'S CALLING SEQUENCE.
  
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      COMPLEX SCU(  LAW, 1),SCV(  LAW, 1),SCW(   LA, 1) 
      COMPLEX SCP(   LA, 1),SCD(   LA, 1),SCA(   LA, 1) 
      COMPLEX    UF(ILH,*),   VF(ILH,*),   WF(ILH,1)
      COMPLEX                           WFLNG(ILH,1)
      COMPLEX    PF(ILH,*),PFLNG(ILH,1),PFLAT(ILH,1)
      COMPLEX                           WFLAT(ILH,1)
      COMPLEX   UFP(ILH,*),  VFP(ILH,*),  PFP(ILH,*)
  
      DIMENSION    U(MAXLG,1),   V(MAXLG,1),   W(MAXLG,1) 
      DIMENSION                             WLNG(MAXLG,1) 
      DIMENSION    P(MAXLG,1),PLNG(MAXLG,1),PLAT(MAXLG,1) 
      DIMENSION                             WLAT(MAXLG,1) 
      DIMENSION   UP(MAXLG,1),  VP(MAXLG,1),  PP(MAXLG,1) 
      DIMENSION  ANS(MAXLG,1),   D(MAXLG,1) 
  
      DIMENSION ALP(*),DALP(*),EPSI(*),TRIGS(*),IFAX(*) 
      DIMENSION S(*),WEIGHT(*)
      DIMENSION LSR(2,1),LRW(2,1),WRKS(*) 
      DIMENSION PR(*) 
      COMPLEX FCOR
  
  
C------------------------------------------------------------------------ 
      IR=LM-1 
      NLEV1=NLEV-1
      NLEV2=NLEV*2
      NLEV3=NLEV*3
      NLEV12=NLEV*12
  
C     * ADD CORIOLIS PARAMETER TO P TO GET ABSOLUTE VORTICITY.
  
      PI=3.1415926535898E0
      FCOR = CMPLX ( PI*SQRT(2.E0/3.E0)/21600.E0 , 0.0E0 )
      DO 500 L=1,NLEV 
         SCP(2,L)=SCP(2,L)+FCOR 
  500 CONTINUE
  
C     * LOOP OVER TRANSFORM GRID LATITUDES
  
      DO 600 IH=1,NLAT
         CALL ALPST2(ALP,LRW,LM,S(IH),EPSI) 
         CALL ALPDY2(DALP,ALP,LRW,LM,EPSI)
  
C     * COMPUTE FOURIER COEFFICIENTS, DOING UF AND VF, THEN WF, XF
C     * AND Y AND SO-ON ... 
  
         CALL STAF (UF,SCU,LRW,LM,LAW,ILH,NLEV2,ALP)
         CALL STAF (WF,SCW,LSR,LM, LA,ILH,NLEV3,ALP)
         CALL STAF (WFLNG,SCW,LSR,LM, LA,ILH,NLEV2,DALP)
  
         DO 520 L=1,NLEV
            DO 520 M1=1,LM
               AM=FLOAT(M1-1) 
               WFLAT(M1,L)=CMPLX(-AM* IMAG(WF(M1,L)),AM*REAL(WF(M1,L))) 
               PFLAT(M1,L)=CMPLX(-AM* IMAG(PF(M1,L)),AM*REAL(PF(M1,L))) 
  520    CONTINUE 
  
         DO 530 K=2,NLEV1 
            DO 530 M=1,LM 
               UFP(M,K)=(UF(M,K+1)-UF(M,K-1))/(PR(K+1)-PR(K-1)) 
               VFP(M,K)=(VF(M,K+1)-VF(M,K-1))/(PR(K+1)-PR(K-1)) 
               PFP(M,K)=(PF(M,K+1)-PF(M,K-1))/(PR(K+1)-PR(K-1)) 
  530    CONTINUE 
  
         DO 535 M=1,LM
            UFP(M,1)=(UF(M,2)-UF(M,1))/(PR(2)-PR(1))
            VFP(M,1)=(VF(M,2)-VF(M,1))/(PR(2)-PR(1))
            PFP(M,1)=(PF(M,2)-PF(M,1))/(PR(2)-PR(1))
            UFP(M,NLEV)=(UF(M,NLEV)-UF(M,NLEV1))/(PR(NLEV)-PR(NLEV1)) 
            VFP(M,NLEV)=(VF(M,NLEV)-VF(M,NLEV1))/(PR(NLEV)-PR(NLEV1)) 
            PFP(M,NLEV)=(PF(M,NLEV)-PF(M,NLEV1))/(PR(NLEV)-PR(NLEV1)) 
  535    CONTINUE 
  
C     * COMPUTE GRID POINT VALUES FOR ALL FIELDS AT THE SAME TIME.
  
         CALL FFGFW (U,MAXLG,UF,ILH,IR,ILONG,WRKS,NLEV12,IFAX,TRIGS)
  
         COSSQ=1.E0-S(IH)*S(IH) 
         DO 540 L=1,NLEV
  
C     * COMPUTE TENDENCY ON THE GRID
  
            DO 540 I=1,ILONG
  
       ANS(I,L)=-(U(I,L)*PLAT(I,L)+V(I,L)*PLNG(I,L))/COSSQ-W(I,L)*
     1 PP(I,L)-P(I,L)*D(I,L)-(WLAT(I,L)*VP(I,L)-WLNG(I,L)*UP(I,L))/COSSQ
  
  540    CONTINUE 
  
C     * COMPUTE FOURIER COEFFICIENTS
  
         CALL FFWFG (ANS,ILH,ANS,MAXLG,IR,ILONG,WRKS,NLEV,IFAX,TRIGS) 
  
C     * COMPUTE SPHERICAL HARMONIC COEFFICIENTS 
  
         WLJ=WEIGHT(IH) 
         DO 550 L=1,LAW 
            ALP(L)=ALP(L)*WLJ 
  550    CONTINUE 
  
         CALL FAST (SCA,ANS,LSR,LM,LA,ILH,NLEV,ALP) 
  
  600 CONTINUE
  
C---------------------------------------------------------------------- 
      RETURN
      END 
