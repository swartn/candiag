      SUBROUTINE PRINTA (X,ILON,ILAT,I1,J1,I2,J2,HIGH)
C 
C     * JEAN-FRANCOIS FORTIN - C.C.R.N. 
C     * FEB. 23/84. 
C     * MAR.18/86 J.HASLIP - 6030 FORMAT CHANGED 1PE9.3 TO 1PE9.2 
C 
C     * THIS SUBROUTINE PRINTS THE CURRENT FIELD X. 
C     * IT REDIMENSIONS IT IN TWO DIMENSIONS FOR EASE OF USE. 
C     * THE NEW ARRAY IS OF THE FORM X(ILON,ILAT) 
C 
C     * THE ALGORITHM PRINTS ILAT*IWIDE NUMBERS, NTI+IFTI TIMES.
C     * THAT MEANS THAT WE SEPARATE THE FIELD IN BUNCHES OF 
C     * IWIDE NUMBERS WIDE AND PRINT THEM ON A SEQUENTIAL BASIS.
C 
C     * HIGH RESOLUTION IS  5 NUMBERS WIDE DISPLAY, 
C     * LOW  RESOLUTION IS 12 NUMBERS WIDE DISPLAY. 
C 
C     *                ---------------- 
C     *                !    !    !   !! 
C     *                !    !    !   !! 
C     *                ---------------- 
C 
C     * WOULD BECOME :  
C 
C     *                ------ 
C     *                !    ! 
C     *                !    ! 
C     *                ------ 
C     *                !    ! 
C     *                !    ! 
C     *                ------ 
C     *                !    ! 
C     *                !    ! 
C     *                ------ 
C     *                !! 
C     *                !! 
C     *                -- 
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      DIMENSION X(ILON,ILAT)
      LOGICAL HIGH
C ------------------------------------------------------------------- 
      IWIDE = 12
      IF (HIGH) IWIDE = 5 
      NTI=(I2-I1+1)/IWIDE 
      IFTI=MOD((I2-I1+1),IWIDE) 
      K=I1
      IF (.NOT. HIGH) GO TO 700 
C 
      DO 500 I=1,NTI
         WRITE(6,6010) ((K+L),L=0,IWIDE-1)
         WRITE(6,6020)
         DO 400 J=J2,J1,-1
  400    WRITE(6,6000) J,(X(N,J),N=K,K+IWIDE-1) 
         K=K+IWIDE
         WRITE(6,6020)
  500 CONTINUE
C 
      IF (IFTI .EQ. 0) RETURN 
C ------------------------------------------------------------------- 
      WRITE(6,6010) ((K+L),L=0,IWIDE-1) 
      WRITE(6,6020) 
C 
      DO 600 J=J2,J1,-1 
         WRITE(6,6000) J,(X(N,J),N=K,K+IFTI-1)
  600 CONTINUE
C 
      WRITE(6,6020) 
      RETURN
C ------------------------------------------------------------------- 
  700 DO 900 I=1,NTI
         WRITE(6,6040) ((K+L),L=0,IWIDE-1)
         WRITE(6,6020)
         DO 800 J=J2,J1,-1
  800    WRITE(6,6030) J,(X(N,J),N=K,K+IWIDE-1) 
         K = K+IWIDE
         WRITE(6,6020)
  900 CONTINUE
C 
      IF (IFTI .EQ. 0) RETURN 
C ------------------------------------------------------------------- 
      WRITE(6,6040) ((K+L),L=0,IWIDE-1) 
      WRITE(6,6020) 
C 
      DO 1000 J=J2,J1,-1
         WRITE(6,6030) J,(X(N,J),N=K,K+IFTI-1)
 1000 CONTINUE
C 
      WRITE(6,6020) 
      RETURN
C ------------------------------------------------------------------- 
 6000 FORMAT (1X,I3,3X,5(1PE22.15,2X))
 6010 FORMAT ('1',15X,4(I3,22X),I3) 
 6020 FORMAT (1X,126('-'))
 6030 FORMAT (1X,I3,3X,12(1PE9.2,1X)) 
 6040 FORMAT ('1',8X,I3,11(7X,I3))
      END 
