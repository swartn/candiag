      SUBROUTINE PERM(WORKA,WORKB,AA,BB,KP6,K3) 
C 
C     * FEB 28/83 - R.LAPRISE.
C     *****   FEB 1976  -  ROGER DALEY   *****
C    *   CALCULATES SETUP FIELD FOR 3 TIMES POWER OF 2 TRANSFORM
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      COMPLEX WORKA(1),WORKB(1),AA,BB 
C-----------------------------------------------------------------------
      PI = 3.1415926535898E0
      FACT = 2.E0*PI/3.E0 
      AA = CMPLX(COS(FACT),SIN(FACT)) 
      BB = CONJG(AA)
      FT = FACT/FLOAT(K3) 
      DO 125 K=1,KP6
      FK = FT*FLOAT(K-1)
      WORKA(K) = CMPLX(COS(FK),SIN(FK)) 
  125 WORKB(K) = CONJG(WORKA(K))
C 
      RETURN
      END 
