      SUBROUTINE LEITH(G,LSR,LM,LA,NKNOT,NSTAR,ETA3)
C 
C     * AUGUST 21, 1979 - TED SHEPHERD. 
C     * SUBROUTINE RETURNS K(K)=LEITH EDDY VISCOSITY COEFF. 
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      INTEGER LSR(2,1)
C     * LEVEL 2,G 
      COMPLEX G(LA) 
C-----------------------------------------------------------------------
      DO 100 M=1,LM 
      KL=LSR(1,M) 
      KR=LSR(1,M+1)-1 
      DO 100 K=KL,KR
      NS=(M-1)+(K-KL) 
      IF(NS.LE.NKNOT) G(K)=0.E0 
      IF(NS.GT.NKNOT) 
     1   G(K)=4.0E0*ETA3*((FLOAT(NS-NKNOT)/FLOAT(NSTAR))**2) 
  100 CONTINUE
      RETURN
      END 
