      FUNCTION GAMMP1(A,X)
C
C     * THIS FUNCTION IS TAKEN FROM NUMERICAL RECIPES.
C     * PAUSE STATEMENT IS REPLACED BY A CALL TO "XIT" 
C     * SUBROUTINE.
C     * THE ORIGINAL NAME "GAMMP" IS CHANGED TO "GAMMP1".
C
C     JUL 25/96 - SLAVA KHARIN
C
C  (C) COPR. 1986-92 NUMERICAL RECIPES SOFTWARE #=!E=#,)]UBCJ.
C--------------------------------------------------------------------
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      REAL A,GAMMP1,X
CU    USES GCF1,GSER1
      REAL GAMMCF,GAMSER,GLN
C--------------------------------------------------------------------
C
      IF(X.LT.0.E0.OR.A.LE.0.E0) THEN
         WRITE (6,'(A)') 
     1        ' *** ERROR IN GAMMP1: BAD ARGUMENT.'
         CALL                                      XIT('GAMMP1',-1)
      ENDIF
      IF(X.LT.A+1.E0)THEN
        CALL GSER1(GAMSER,A,X,GLN)
        GAMMP1=GAMSER
      ELSE
        CALL GCF1(GAMMCF,A,X,GLN)
        GAMMP1=1.E0-GAMMCF
      ENDIF
      RETURN
      END
