      SUBROUTINE ASMRV (DATA,NPREV,N,NREM,IFACT,NFACT,WORK) 
C     SHUFFLE THE DATA ARRAY BY REVERSING THE DIGITS OF ONE INDEX.
C     THE OPERATION IS THE SAME AS IN SYMRV, EXCEPT THAT THE FACTORS
C     NEED NOT BE SYMMETRICALLY ARRANGED, I.E., GENERALLY IFACT(IF) NOT=
C     IFACT(NFACT+1-IF).  CONSEQUENTLY, A WORK ARRAY OF LENGTH N IS 
C     NEEDED. 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      DIMENSION DATA(1), WORK(1), IFACT(1)
      IF (NFACT-1) 60,60,10 
 10   IP0=2 
      IP1=IP0*NPREV 
      IP4=IP1*N 
      IP5=IP4*NREM
      DO 50 I1=1,IP1,IP0
      DO 50 I5=I1,IP5,IP4 
      IWORK=1 
      I4REV=I5
      I4MAX=I5+IP4-IP1
      DO 40 I4=I5,I4MAX,IP1 
      WORK(IWORK)=DATA(I4REV) 
      WORK(IWORK+1)=DATA(I4REV+1) 
      IP3=IP4 
      DO 30 IF=1,NFACT
      IP2=IP3/IFACT(IF) 
      I4REV=I4REV+IP2 
      IF (I4REV-IP3-I5) 40,20,20
 20   I4REV=I4REV-IP3 
 30   IP3=IP2 
 40   IWORK=IWORK+IP0 
      IWORK=1 
      DO 50 I4=I5,I4MAX,IP1 
      DATA(I4)=WORK(IWORK)
      DATA(I4+1)=WORK(IWORK+1)
 50   IWORK=IWORK+IP0 
 60   RETURN
      END 
