module derived_vars

  !--- Define netcdf library parameters (requred for NF_CHAR,...)
  !--- use netcdf

  implicit none

  private

  public :: expand_vdef, parse_vdef
  public :: required_by_vdef, vdef_req_vid
  public :: var_defs, nvar_defs, nvar_defs_max
  public :: vdef_req_names, nvdef_req_names, nvdef_req_names_max
  public :: print_var_def_t

  !--- Define netcdf library parameters (requred for NF_CHAR,...)
  include "netcdf.inc"

  !--- Define a derived type to hold variable definitions
  type var_def_t
    !--- vdef stores the variable definition as a character string
    character(512) :: vdef = ' '
    !--- name is a string to be used to identify this defined var uniquely
    character(64)  :: name = ' '
    !--- ncname is the netcdf variable name to be used for this defined variable
    character(64)  :: ncname = ' '
    !--- the variable definition may be a linear combination of terms of the form
    !---    coef(i)*vars(i)
    !--- where one vars(i) entry may be empty (blank) meaning the associated coefficient
    !--- is to be added as a stand alone constant
    integer :: nterms = 0
    real(kind=8),     dimension(:), pointer :: coef => null()
    character(len=64), dimension(:), pointer :: vars => null()
  end type var_def_t

  !--- var_defs will contain information about all user defined variables
  type (var_def_t), pointer :: var_defs(:)
  integer, save :: nvar_defs=0, nvar_defs_max=1000

  type vdef_req_names_t
    character(64)  :: name = " "
    integer :: vid = -1
  end type vdef_req_names_t

  !--- vdef_req_names will contain a list of CCCma variable names that are
  !--- required by the variable definitions found in var_defs
  type (vdef_req_names_t), pointer :: vdef_req_names(:)
  integer, save :: nvdef_req_names=0 , nvdef_req_names_max = 1000

  contains

  logical function required_by_vdef(vname) result(yes)
    character(*) :: vname
    integer :: idx
    yes = .false.
    if (nvdef_req_names.gt.0) then
      do idx=1,nvdef_req_names
        if (trim(adjustl(vname)) .eq. &
            trim(adjustl(vdef_req_names(idx)%name))) then
          yes = .true.
          exit
        endif
      enddo
    endif
  end function required_by_vdef

  integer function vdef_req_vid(vname) result(vid)
    character(*) :: vname
    integer :: idx
    vid = -1
    if (nvdef_req_names.gt.0) then
      do idx=1,nvdef_req_names
        if (trim(adjustl(vname)) .eq. &
            trim(adjustl(vdef_req_names(idx)%name))) then
          vid = vdef_req_names(idx)%vid
          exit
        endif
      enddo
    endif
  end function vdef_req_vid

  subroutine expand_vdef(vlist, nvlist, lhcast, allow_name_alone)
    !----------------------------------------------------------------------
    !--- expand_vdef will find any variable definitions that the user has
    !--- provided on the command line (passed here as elements of vlist)
    !--- and assign those variable definitions to internal storage.
    !--- It will also append all variable names required by each definition
    !--- to the internal array (include_name) that holds the names of all
    !--- variables to be extracted from the input CCCma binary files.
    !----------------------------------------------------------------------
    use nccrip_params, only : include_name, n_include_name
    use var_meta_data, only : var_att_t, destroy_var_att_t, append_usr_meta

    implicit none
    character(*), intent(inout) :: vlist(*)
    integer,      intent(inout) :: nvlist

    !--- lhcast is used to determine if the input variable definitions
    !--- found in vlist are left hand cast of not
    !--- if lhcast = T then vdef is of the form DEF = VAR
    !--- if lhcast = F then vdef is of the form VAR = DEF
    !--- where VAR is the output variable name and DEF is the variable definition
    logical, optional, intent(in) :: lhcast

    !--- allow_name_alone determines if a single variable name by itself in a
    !--- vlist element is to be allowed
    logical, optional, intent(in) :: allow_name_alone

    !--- Local
    integer :: verbose=2
    integer :: idx1, idx2, idx3, nolist
    character(len=64), allocatable :: olist(:)
    character(len=512) :: strng, ncname
    character(len=64)   :: vars(50)
    real(kind=8) :: coef(50)
    integer :: nterms
    logical :: found, add_extra_meta
    type (var_att_t) :: tmp_vatt

    !--- make olist big enough to accomodate 10 term definitions
    allocate( olist(10*nvlist) )

    !--- Set a logical flag that will allow extra meta data (currently just the CCCma variable name)
    !--- to be added to a variable when a simple name change is requested via ncname=ccname
    add_extra_meta = .false.

    nolist=0
    do idx1=1,nvlist
      idx2 = index(trim(adjustl(vlist(idx1))), "=")

      if (idx2.le.0) then
        !--- The current vlist item does not contain an equal sign
        !--- assume this is a stand alone variable name
        !--- Abort unless stand alone variable names are allowed
        if ( .not. allow_name_alone ) then
          write(6,*)'expand_vdef: Stand alone variable names are not allowed here --> ', &
            trim(adjustl(vlist(idx1))),' <--'
          call abort
        endif
        !--- Add this variable name to the include_name internal list
        nolist = nolist+1
        olist(nolist) = trim(adjustl(vlist(idx1)))
        cycle
      endif

      if (idx2.eq.1) then
        !--- An equal sign is the first character in the string
        !--- No output variable name (or variable definition) has been provided
        write(6,*)'expand_vdef: Invalid variable definition --> ', &
          trim(adjustl(vlist(idx1))),' <--'
        call abort
      endif

      !--- The current vlist item contains an equal sign
      !--- assume this is a variable definition
      ncname = " "
      strng  = " "
      if ( present(lhcast) ) then
        if (lhcast) then
          !--- Assume the variable def is on the LHS of the equal sign
          strng  = trim(vlist(idx1)(1:idx2-1))
          ncname = trim(vlist(idx1)(idx2+1:))
        else
          !--- Assume the variable def is on the RHS of the equal sign
          ncname = trim(vlist(idx1)(1:idx2-1))
          strng  = trim(vlist(idx1)(idx2+1:))
        endif
      else
        !--- Assume the variable def is on the RHS of the equal sign
        ncname = trim(vlist(idx1)(1:idx2-1))
        strng  = trim(vlist(idx1)(idx2+1:))
      endif
      call parse_vdef(trim(strng),coef,vars,nterms)

      if (nterms.eq.1 .and. coef(1).eq.1.d0) then
        !--- This is just a defintion for the netcdf variable name
        !--- Do not add this as a varaible definition but do modify the global usr_attr array
        !--- to use this netcdf variable name when the current CCCma variable is written

        !--- Create a usr_attr entry for the current derived variable
        call destroy_var_att_t(tmp_vatt)
        tmp_vatt%vname  = trim(adjustl(vars(1)))
        tmp_vatt%ncname = trim(adjustl(ncname))
        tmp_vatt%label  = " "

        if (add_extra_meta) then
          !--- Add the CCCma variable name as meta data
          tmp_vatt%nattr  = 1
          allocate( tmp_vatt%attr( tmp_vatt%nattr ) )

          tmp_vatt%attr(1)%name      = "CCCma_var"
          tmp_vatt%attr(1)%vid       = -1
          tmp_vatt%attr(1)%xtype     = NF_CHAR
          tmp_vatt%attr(1)%char_vals = trim(adjustl(vars(1)))
          tmp_vatt%attr(1)%length    = len_trim(tmp_vatt%attr(1)%char_vals)
        else
          !--- Do not add any extra attributes to this variable
          tmp_vatt%nattr  = 0
        endif

        !--- Append this entry to the global array usr_attr
        call append_usr_meta(tmp_vatt,1)
        call destroy_var_att_t(tmp_vatt)

      else if (nterms.gt.0) then
        !--- Add a new variable definition to var_defs
        nvar_defs = nvar_defs + 1
        if ( nvar_defs .gt. nvar_defs_max) then
          write(6,'(a,i6)')'expand_vdef: nvar_defs exceeds maximum value of ',nvar_defs_max
          call abort
        endif

        if ( present(lhcast) ) then
          if (lhcast) then
            !--- Assume the variable def is on the LHS of the equal sign
            var_defs(nvar_defs)%ncname = trim(adjustl(vlist(idx1)(idx2+1:)))
            var_defs(nvar_defs)%vdef   = trim(adjustl(vlist(idx1)(:idx2-1)))
          else
            !--- Assume the variable def is on the RHS of the equal sign
            var_defs(nvar_defs)%ncname = trim(adjustl(vlist(idx1)(:idx2-1)))
            var_defs(nvar_defs)%vdef   = trim(adjustl(vlist(idx1)(idx2+1:)))
          endif
        else
          !--- Assume the variable def is on the RHS of the equal sign
          var_defs(nvar_defs)%ncname = trim(adjustl(vlist(idx1)(:idx2-1)))
          var_defs(nvar_defs)%vdef   = trim(adjustl(vlist(idx1)(idx2+1:)))
        endif

        !--- Create a string that will be used to identify this variable definition by name
        !--- dname = " "
        !--- write(dname,'(a,i3.3,a)')'_',nvar_defs,'_vdef'
        !--- var_defs(nvar_defs)%name = trim(adjustl(dname))
        var_defs(nvar_defs)%name = trim(adjustl(var_defs(nvar_defs)%ncname))

        var_defs(nvar_defs)%nterms = nterms
        allocate( var_defs(nvar_defs)%coef(nterms) )
        allocate( var_defs(nvar_defs)%vars(nterms) )
        do idx2=1,nterms
          var_defs(nvar_defs)%coef(idx2) = coef(idx2)
          var_defs(nvar_defs)%vars(idx2) = trim(adjustl(vars(idx2)))

          !--- Add the names of any CCCma variables that are required to define
          !--- the current derived variable (avoid repeating any names)

          !--- Ignore blank variable names when determining required names
          if (len_trim(vars(idx2)).le.0) cycle

          found=.false.
          if (nvdef_req_names.gt.0) then
            do idx3=1,nvdef_req_names
              if (trim(adjustl(vars(idx2))) .eq. &
                  trim(adjustl(vdef_req_names(idx3)%name)) ) then
                found = .true.
                exit
              endif
            enddo
          endif
          if (.not.found) then
            nvdef_req_names = nvdef_req_names + 1
            if ( nvdef_req_names .gt. nvdef_req_names_max ) then
              write(6,'(a,i6)')'expand_vdef: nvdef_req_names exceeds maximum value of ', &
                nvdef_req_names_max
              call abort
            endif
            vdef_req_names(nvdef_req_names)%name = trim(adjustl(vars(idx2)))
            vdef_req_names(nvdef_req_names)%vid = -1
          endif
        enddo

        if (verbose.gt.1) call print_var_def_t(var_defs(nvar_defs),0)

        if (allow_name_alone ) then
          !--- Add these variable names to olist
          do idx2=1,nterms
            !--- Ignore missing variable names
            if ( len_trim(vars(idx2)).le.0 ) cycle
            nolist = nolist+1
            olist(nolist) = trim(adjustl(vars(idx2)))
          enddo
        endif

        !--- Create a usr_attr entry for the current derived variable
        call destroy_var_att_t(tmp_vatt)
        tmp_vatt%vname  = trim(adjustl(var_defs(nvar_defs)%ncname))
        tmp_vatt%ncname = trim(adjustl(var_defs(nvar_defs)%ncname))
        tmp_vatt%label  = " "
        tmp_vatt%nattr  = 1
        allocate( tmp_vatt%attr( tmp_vatt%nattr ) )

        tmp_vatt%attr(1)%name      = "vardef"
        tmp_vatt%attr(1)%vid       = -1
        tmp_vatt%attr(1)%xtype     = NF_CHAR
        tmp_vatt%attr(1)%char_vals = trim(adjustl(var_defs(nvar_defs)%vdef))
        tmp_vatt%attr(1)%length    = len_trim(tmp_vatt%attr(1)%char_vals)

        !--- Append this entry to global array usr_attr
        call append_usr_meta(tmp_vatt,0)
        call destroy_var_att_t(tmp_vatt)
      endif
    enddo

    if ( allow_name_alone .and. nolist.gt.0 ) then
      !--- Append the variable names in olist to include_name
      do idx1=1,nolist
        !--- Do not append names if they already exists in include_name
        found=.false.
        if ( n_include_name.gt.0 ) then
          do idx2=1,n_include_name
            if (trim(adjustl(olist(idx1))) .eq. &
                trim(adjustl(include_name(idx2))) ) then
              found = .true.
              exit
            endif
          enddo
        endif
        if ( found ) cycle

        !--- This variable name does not exist in the include_name list
        n_include_name = n_include_name + 1
        include_name(n_include_name) = trim(adjustl(olist(idx1)))
      enddo
    endif

    !--- Clean up
    deallocate( olist )

    if (verbose.gt.10 .and. nvdef_req_names.gt.0) then
      write(6,*)'CCCma variables required to define derived variables.'
      do idx1=1,nvdef_req_names
        write(6,*)idx1,trim(vdef_req_names(idx1)%name)
      enddo
    endif

  end subroutine expand_vdef

  subroutine parse_vdef(vdefin,coef,vars,nvars)
    !--- Given a single string that contains a variable definition
    !--- return the list of coefficients and variables that constitute
    !--- this definition.

    !--- A variable definition is assumed to be a linear combination
    !--- of variables in the form
    !---   +/- T1 +/- T2 +/- T3 +/- ...
    !--- Where each term (T1,T2,T3,...) is of the form
    !---   V1  or  c1  or  c1*V1  or  c1*V1/c2
    !--- where c1,c2,c3,... are constants and V1,V2,V3,... are the names
    !--- of variables (ibuf3 values) that appear in an input file.
    !--- Only 1 variable name is allowed per term.

    implicit none

    integer :: nvars
    character(*) :: vdefin
    real(kind=8) :: coef(*)
    character(len=64) :: vars(*)

    !--- local
    integer :: verbose=0
    logical :: ok, first_pass, denom(10)
    integer :: ip, im, it, id, ix, nterms, natoms, ios
    real(kind=8) :: tsign(100), rval
    character(len=1024) :: vdef, strng
    character(len=128)  :: atom, atoms(10), term, terms(100)
    real(kind=8) :: t_coef(100)
    character(len=64) :: t_vars(100)

    vdef=" "
    vdef=trim(adjustl(vdefin))

    nvars=0
    if (len_trim(vdef).le.0) return

    !--- Scan vdef for terms separated by + or -
    nterms=0
    tsign(:)=1.0
    if (vdef(1:1).eq.'+') then
      !--- The leading character is "+"
      !--- Simply strip it off
      vdef=trim(vdef(2:))
    else if (vdef(1:1).eq.'-') then
      !--- The leading character is "-"
      !--- Strip it off and set the sign of this term negative
      vdef=trim(vdef(2:))
      tsign(1)=-1.0
    endif
    ip=index(vdef,"+")
    iploop: do while (ip.gt.0)
      !--- Find terms separated by "+"

      !--- Check for the string "e+" which is allowed as part
      !--- of a coefficient (e.g. 1e+20) and therefore should
      !--- not delimit a term
      do while (ip.gt.0 .and. vdef(ip-1:ip-1).eq."e")
        ix=index(vdef(ip+1:),"+")
        if (ix.le.0) exit iploop
        ip=ip+ix
      enddo

      strng=' '
      strng=trim(adjustl(vdef(1:ip-1)))
      im=index(strng,"-")
      first_pass=.true.
      imloop1: do while (im.gt.0)
        !--- Find terms separated by "-"

        !--- Check for the string "e-" which is allowed as part
        !--- of a coefficient (e.g. 1e-20) and therefore should
        !--- not delimit a term
        do while (im.gt.0 .and. strng(im-1:im-1).eq."e")
          ix=index(strng(im+1:),"-")
          if (ix.le.0) exit imloop1
          im=im+ix
        enddo

        term=' '
        term=trim(adjustl(strng(1:im-1)))
        nterms=nterms+1
        terms(nterms)=trim(adjustl(term))
        if (first_pass) then
          first_pass=.false.
        else
          tsign(nterms)=-1.0
        endif
        strng = trim(adjustl(strng(im+1:)))
        im = index(strng,"-")
      enddo imloop1
      nterms=nterms+1
      terms(nterms)=trim(adjustl(strng))
      if (.not.first_pass) then
        tsign(nterms)=-1.0
      endif
      vdef = trim(adjustl(vdef(ip+1:)))
      ip = index(vdef,"+")
    enddo iploop

    !--- Process everything after the last "+"
    im=index(vdef,"-")
    first_pass=.true.
    imloop2: do while (im.gt.0)
      !--- Find terms separated by "-"

      !--- Check for the string "e-" which is allowed as part
      !--- of a coefficient (e.g. 1e-20) and therefore should
      !--- not delimit a term
      do while (im.gt.0 .and. vdef(im-1:im-1).eq."e")
        ix=index(vdef(im+1:),"-")
        if (ix.le.0) exit imloop2
        im=im+ix
      enddo

      term=' '
      term=trim(adjustl(vdef(1:im-1)))
      nterms=nterms+1
      terms(nterms)=trim(adjustl(term))
      if (first_pass) then
        first_pass=.false.
      else
        tsign(nterms)=-1.0
      endif
      vdef = trim(adjustl(vdef(im+1:)))
      im = index(vdef,"-")
    enddo imloop2
    nterms=nterms+1
    terms(nterms)=trim(adjustl(vdef))
    if (.not.first_pass) then
      tsign(nterms)=-1.0
    endif

    !--- Look for empty terms and abort if any are found
    do it=1,nterms
      if (len_trim(adjustl(terms(it))) .le. 0) then
        write(6,'(2a)')'parse_vdef: Invalid variable definition ',trim(adjustl(vdefin))
        call abort
      endif
    enddo

    !--- Initialize the coefficient array
    coef(1:nterms)=tsign(1:nterms)

    !--- Scan each term and split into atoms on "*" or "/"
    do it=1,nterms
      vars(it)=' '
      denom(:)=.false.
      term=' '
      term=trim(adjustl(terms(it)))
      im=len_trim(term)
      if (term(1:1).eq.'*' .or. term(1:1).eq.'/' .or. &
          term(im:im).eq.'*' .or. term(im:im).eq.'/' ) then
        !--- A term may not begin or end with "*" or "/"
        write(6,'(2a)')'parse_vdef: Invalid term: ',trim(adjustl(terms(it)))
        write(6,'(2a)')'   found in variable def: ',trim(adjustl(vdefin))
        call abort
      endif
      natoms=0
      im=index(term,"*")
      do while (im.gt.0)
        !--- Find atoms separated by "*"
        strng=' '
        strng=trim(adjustl(term(1:im-1)))
        id=index(strng,"/")
        first_pass=.true.
        do while (id.gt.0)
          !--- Find atoms separated by "/"
          atom=' '
          atom=trim(adjustl(strng(1:id-1)))
          natoms=natoms+1
          atoms(natoms)=' '
          atoms(natoms)=trim(adjustl(atom))
          if (first_pass) then
            first_pass=.false.
          else
            denom(natoms)=.true.
          endif
          strng = trim(adjustl(strng(id+1:)))
          id = index(strng,"/")
        enddo
        atom=trim(adjustl(strng))
        natoms=natoms+1
        atoms(natoms)=' '
        atoms(natoms)=trim(atom)
        if (.not.first_pass) then
          denom(natoms)=.true.
        endif

        term = trim(adjustl(term(im+1:)))
        im = index(term,"*")
      enddo

      !--- Process everything after the last "*"
      id=index(term,"/")
      first_pass=.true.
      do while (id.gt.0)
        !--- Find atoms separated by "/"
        atom=' '
        atom=trim(adjustl(term(1:id-1)))
        natoms=natoms+1
        atoms(natoms)=' '
        atoms(natoms)=trim(adjustl(atom))
        if (first_pass) then
          first_pass=.false.
        else
          denom(natoms)=.true.
        endif
        term = trim(adjustl(term(id+1:)))
        id = index(term,"/")
      enddo
      atom=trim(adjustl(term))

      natoms=natoms+1
      atoms(natoms)=' '
      atoms(natoms)=trim(atom)
      if (.not.first_pass) then
        denom(natoms)=.true.
      endif

      !--- Look for empty atoms and abort if any are found
      do im=1,natoms
        if (len_trim(adjustl(atoms(im))) .le. 0) then
          write(6,'(2a)')'parse_vdef: Invalid term: ',trim(adjustl(terms(it)))
          write(6,'(2a)')'   found in variable def: ',trim(adjustl(vdefin))
          call abort
        endif
      enddo

      !--- Combine scalar coefficients into a single number
      !--- and assign it to the coef array.
      !--- Also assign the vars array. This will contain at most
      !--- a single variable name per element.
      do im=1,natoms
        atom=' '
        atom=trim(adjustl(atoms(im)))
        if (verify(trim(atom),'0123456789.+-e').eq.0) then
          !--- Assume this atom is numeric
          read(atom,*,iostat=ios)rval
          if (ios.ne.0) then
            write(6,'(2a)')'PARSE_VDEF: Error converting atom: ',trim(atom)
            write(6,'(2a)')'            in term: ',trim(adjustl(terms(it)))
            call abort
          endif
          if (denom(im)) then
            !--- This a denominator
            if (rval.eq.0.0) then
              write(6,'(2a)')'PARSE_VDEF: Zero denominator in term: ',trim(adjustl(terms(it)))
              write(6,'(2a)')'                     of variable def: ',trim(adjustl(vdefin))
              call abort
            endif
            coef(it) = coef(it)/rval
          else
            !--- Assume this is a numerator
            coef(it) = coef(it)*rval
          endif
        else
          !--- Assume this atom is a variable name
          if (len_trim(vars(it)) .gt. 0) then
            !--- Only one variable name per term is allowed
            write(6,'(2a)')'PARSE_VDEF: More than one variable name found in term: ', &
                            trim(adjustl(terms(it)))
            write(6,'(2a)')'            of variable def: ',trim(adjustl(vdefin))
            call abort
          endif
          vars(it)=trim(atom)
        endif
      enddo
    enddo

    if (verbose.gt.1) then
      write(6,'(2a)')'parse_vdef: ',trim(vdefin)
      do it=1,nterms
        write(6,'(i3,1x,f5.1,1x,a30,1x,1pe12.3,1x,a10)') &
         it,tsign(it),trim(terms(it)),coef(it),trim(vars(it))
      enddo
    endif

    !--- Look for any coef/vars pairs with a null variable name
    !--- and combine all similar coefficients into a single element
    nvars=0
    rval=0.0
    do it=1,nterms
      if (len_trim(vars(it)) .le. 0) then
        rval=rval+coef(it)
      else
        nvars = nvars+1
        t_coef(nvars) = coef(it)
        t_vars(nvars) = vars(it)
        !--- Check for repeat variable names
        !--- Assume this is a typo on the part of the user
        if (nvars.gt.1) then
          do ip=1,nvars-1
            if (trim(adjustl(t_vars(ip))) .eq. &
               trim(adjustl(t_vars(nvars))) ) then
              write(6,'(2a)')'PARSE_VDEF: Duplicate variable name ',trim(adjustl(t_vars(ip)))
              write(6,'(2a)')'            found in vdef: ',trim(vdefin)
              call abort
            endif
          enddo
        endif
      endif
    enddo
    if (nvars.le.0) then
      !--- This check needs to be here rather than after the following
      !--- conditional to ensure that only non null variable names are
      !--- counted by nvars
      write(6,'(2a)')'PARSE_VDEF: No variable names present in vdef: ',trim(vdefin)
      call abort
    endif
    if (rval.ne.0.0) then
      nvars = nvars+1
      t_coef(nvars) = rval
      t_vars(nvars) = ' '
    endif
    coef(1:nterms)=0.0_8
    vars(1:nterms)=' '
    coef(1:nvars) = t_coef(1:nvars)
    vars(1:nvars) = t_vars(1:nvars)

    if (verbose.gt.0) then
      write(6,'(2a)')'parse_vdef: ',trim(vdefin)
      do it=1,nvars
        write(6,'(i3,1x,1pg24.16,2x,a)')it,coef(it),vars(it)
      enddo
    endif

  end subroutine parse_vdef

  subroutine init_var_def_t(vdef)
    type(var_def_t) :: vdef
    vdef % vdef   = ' '
    vdef % ncname = ' '
    vdef % nterms = 0
    nullify(vdef % coef)
    nullify(vdef % vars)
  end subroutine init_var_def_t

  subroutine print_var_def_t(vdef,noisy)
    type(var_def_t) :: vdef
    integer :: idx, noisy
    character(1)  :: sign
    character(16) :: coef
    if (noisy.gt.0) then
      write(6,'(a)')'=============================='
      write(6,'(2a)')  '  vdef: ',trim(adjustl(vdef%vdef))
      write(6,'(2a)')  '  name: ',trim(adjustl(vdef%name))
      write(6,'(2a)')  'ncname: ',trim(adjustl(vdef%ncname))
      write(6,'(a,i3)')'nterms =',vdef%nterms
    endif
    if (vdef%nterms.gt.0) then
      if (noisy.le.0) then
        write(6,'("variable def: ",a," = ",$)')trim(adjustl(vdef%ncname))
      else
        write(6,'(a," = ",$)')trim(adjustl(vdef%ncname))
      endif
      do idx=1,vdef%nterms
        if (vdef%coef(idx).lt.0.0) then
          sign = "-"
          write(coef,'(g16.8)')-vdef%coef(idx)
        else
          sign = "+"
          write(coef,'(g16.8)')vdef%coef(idx)
        endif
        if (len_trim(vdef%vars(idx)).gt.0) then
          !--- This term contains a variable name
          if (idx.gt.1) then
            write(6,'(1x,a,1x,a," * ",a,$)')sign,trim(adjustl(coef)),trim(adjustl(vdef%vars(idx)))
          else
            if (sign.eq."-") then
              write(6,'(a,1x,a," * ",a,$)')sign,trim(adjustl(coef)),trim(adjustl(vdef%vars(idx)))
            else
              write(6,'(a," * ",a,$)')trim(adjustl(coef)),trim(adjustl(vdef%vars(idx)))
            endif
          endif
        else
          !--- This term does not contain a variable name
          if (idx.gt.1) then
            write(6,'(1x,a,1x,a,$)')sign,trim(adjustl(coef))
          else
            if (sign.eq."-") then
              write(6,'(a,1x,a,$)')sign,trim(adjustl(coef))
            else
              write(6,'(a,$)')trim(adjustl(coef))
            endif
          endif
        endif
      enddo
      write(6,*)
    endif
  end subroutine print_var_def_t

end module derived_vars
