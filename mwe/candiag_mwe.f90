PROGRAM candiag_mwe
  ! A minimal working example, which aims to produce monthly mean
  ! global surface temperature output. The steps are:
  ! - read, join and repack tiled files
  ! - extract and compute monthly mean ST, etc
  !
  ! The major difference between this and the standard CanDIAG is
  ! using a main program, and not writing every stage to file.
  !
  ! Neil Swart, September 2020 (against my better judgement).
  !
  ! TODO:
  !
  ! - listed by module, at the top
  ! - create "operations" module, that does a list of operations
  ! - enable different operations by field. Create separate cccdata objects
  !   for rebuilding, then for operations, and for output.
  ! - consider a type of namelist and/or xml like configuration.
  
  use ccc_data, only: ccc_data_array
  use rebuild, only: rebuild_gcm
  use operations, only: apply_processing
  use utilities, only: timer_start, timer_stop
  !use mpi
  
  implicit none
  character (len=4), dimension(999)               :: invariables      ! variables to process
  character (len=4), dimension(:)   , allocatable :: variables        ! variables to process  
  type(ccc_data_array), dimension(:), allocatable :: cccd_rebuild_array  ! for rebuilding
  type(ccc_data_array), dimension(:), allocatable :: cccd_ops_array      ! for operations
  type(ccc_data_array), dimension(:), allocatable :: cccd_out_array      ! for outputting  

  integer iu_namelist, nml_stat, num_tiles, year, month
  integer nelements
  integer i, var_count, ierr
  character (len=24) :: file_prefix
  namelist /candiag/ file_prefix, year, month, num_tiles, invariables

  !call mpi_init(ierr)
  call timer_start('candiag_mwe')
  
  open(newunit=iu_namelist, file='candiag.nml', action='read', iostat=nml_stat)
  file_prefix=''
  year=0
  month=0
  invariables(:) =''
  read(nml=candiag, iostat=nml_stat, unit=iu_namelist)

  nelements = size(invariables)
  var_count = 0
  do i=1,nelements
!     write(*,*) i, invariables(i)
     if (invariables(i) == "") then
        exit
     else
        var_count = var_count + 1
     end if
  end do
  write(*,*) var_count

  allocate(variables(var_count))
  variables(:) = invariables(1:var_count)
  write(*,*) variables
  
  ! This is the actual number of fields to process
  nelements = size(variables)
  allocate(cccd_rebuild_array(nelements), cccd_ops_array(nelements))

  ! Join the dates into the file prefix
  write (file_prefix,"(A,I0.4, I0.2)") trim(file_prefix), year, month


  
  call rebuild_gcm(file_prefix=file_prefix, num_tiles=num_tiles,    &
                   variables=variables, cccdata=cccd_rebuild_array)
  call apply_processing(cccdata_input=cccd_rebuild_array,           &
                        cccdata_output=cccd_ops_array,              &
                        year=year, month=month)

  call timer_stop('candiag_mwe')
  
end program candiag_mwe
