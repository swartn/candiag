      PROGRAM PERTURB
C     PROGRAM PERTURB (X,       Y,       OUTPUT,                        )       C2
C    1           TAPE1=X, TAPE2=Y, TAPE6=OUTPUT)
C     -----------------------------------------------                           C2
C                                                                               C2
C     FEB 01/08 - S. KHARIN.                                                    C2
C                                                                               C2
CPERTURB - PERTURBS INPUT FILE                                          1  1 C  C1
C                                                                               C3
CAUTHOR  - S.KHARIN                                                             C3
C                                                                               C3
CPURPOSE - PERTURBS INPUT FILE                                                  C3
C                                                                               C3
CINPUT FILES...                                                                 C3
C                                                                               C3
C      X = FIRST  INPUT FILE                                                    C3
C
CINPUT PARAMETERS...
C                                                                               C5
C E10  SEED = SEED TO INITIALIZE RANDOM NUMBER GENERATOR                        C5
C                                                                               C3
COUTPUT FILE...                                                                 C3
C                                                                               C3
C      Y = X + EPS                                                              C3
C
CINPUT PARAMETERS...
C                                                                               C5
C11-20 ISEED  = SEED OF RANDOM NUMBER GENERATOR                                 C5
C21-30 SMALL  = AMPLITUDE OF PERTURBATION                                       C5
C31-35 LTYPE  = PERTURBATION TYPE:                                              C5
C             = 0,  MULTIPLICATIVE PERTURBATION Y=X*(1+SMALL*RND(-1...1))       C5
C             = 1,  ADDITIVE PERTURBATION Y=X+SMALL*RND(-1...1)                 C5
C                                                                               C5
CEXAMPLE OF INPUT CARD...                                                       C5
C                                                                               C5
C*PERTURB          1     1.E-2    1                                             C5
C*PERTURB          1     1.E-3    0                                             C5
C*PERTURB          1    1.E-15    1                                             C5
C----------------------------------------------------------------------
C
      REAL A(130381),B(130381)
      LOGICAL OK,SPEC
      COMMON/ICOM/IBUF(8),IDAT(260762)
      DATA MAXX/260762/,SPVAL/1.E38/,SPVALT/1.E32/
C---------------------------------------------------------------------
      NFF=4
      CALL JCLPNT(NFF,1,2,5,6)
      REWIND 1
      REWIND 2
C
C     * READ A,B AND NEWNAM FROM CARD.
C
      READ(5,5010,END=900)ISEED,SMALL,LTYPE                                     C4
      WRITE(6,6010)ISEED,SMALL,LTYPE
      IF(LTYPE.EQ.0)THEN
        WRITE(6,'(A)')' MULTIPLICATIVE PERTURBATION 1+SMALL*(-1...+1)'
      ELSE
        WRITE(6,'(A)')' ADDITIVE PERTURBATION SMALL*(-1...+1)'
      ENDIF
      R=RAN0(ISEED)
C
C     * READ THE NEXT FIELD.
C
      NR=0
C 140 CALL GETFLD2(1,A, -1 ,0,0,0,IBUF,MAXX,OK)
  140 CALL RECGET(1, -1 ,0,0,0,IBUF,MAXX,OK)
      IF(.NOT.OK)THEN
        IF(NR.EQ.0) CALL                           XIT('PERTURB',-1)
        WRITE(6,6020) NR
        CALL                                       XIT('PERTURB',0)
      ENDIF
      IF(NR.EQ.0)CALL PRTLAB(IBUF)
      KIND=IBUF(1)
      SPEC=(KIND.EQ.NC4TO8("SPEC") .OR. KIND.EQ.NC4TO8("FOUR"))
C
C     * DO NOT PROCESS SUPER LABELS
C
      IF(KIND.EQ.NC4TO8("LABL").OR.
     1   KIND.EQ.NC4TO8("CHAR").OR.
     2   KIND.EQ.NC4TO8("DATA")) THEN
C       CALL PUTFLD2(2,A,IBUF,MAXX)
        CALL RECPUT(2,IBUF,MAXX)
        GO TO 140
      ENDIF
      CALL RECUP2(A,IBUF)
      NWDS=IBUF(5)*IBUF(6)
      IF(SPEC) NWDS=NWDS*2
C
C     * DON'T PERTURB GLOBAL AND ZONAL MEAN IN SPECTRAL FIELDS
C
      I0=1
      IF(KIND.EQ.NC4TO8("SPEC"))THEN
        LRLMT=IBUF(7)
        IF (LRLMT .LE. 99999 ) THEN
          LR=LRLMT/1000 
        ELSE
          LR=LRLMT/10000 
        ENDIF
        I0=LR*2+1
      ENDIF
C
C     * PERTURB INPUT FIELDS.
C
      DO I=I0,NWDS
        IF(ABS(A(I)-SPVAL).GT.SPVALT)THEN
          R=SMALL*(2.E0*RAN0(ISEED)-1.E0)
          IF(LTYPE.EQ.0)THEN
            A(I)=A(I)*(1.E0+R)
          ELSE
            A(I)=A(I)+R
          ENDIF
        ENDIF
      ENDDO
C
C     * SAVE THE RESULT.
C
      CALL PUTFLD2(2,A,IBUF,MAXX)
      IF(NR.EQ.0) CALL PRTLAB(IBUF)
      NR=NR+1
      GO TO 140
  900 CALL                                         XIT('PERTURB',-2)
C---------------------------------------------------------------------
 5010 FORMAT(10X,I10,E10.0,I5)                                                  C4
 6010 FORMAT('0 SEED =',I10,' SMALL =',1P1E12.2,' LTYPE =',I5)
 6020 FORMAT('0',I6,'  PAIRS OF RECORDS PROCESSED')
      END

      FUNCTION ran0(idum)
      INTEGER idum,IA,IM,IQ,IR,MASK
      REAL ran0,AM
      PARAMETER (IA=16807,IM=2147483647,AM=1./IM,IQ=127773,IR=2836,
     *MASK=123459876)
      INTEGER k
      idum=ieor(idum,MASK)
      k=idum/IQ
      idum=IA*(idum-k*IQ)-IR*k
      if (idum.lt.0) idum=idum+IM
      ran0=AM*idum
      idum=ieor(idum,MASK)
      return
      END
C  (C) Copr. 1986-92 Numerical Recipes Software #=!E=#,)]uBcj.
