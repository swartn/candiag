      PROGRAM GGPLOT
C     PROGRAM GGPLOT ( Z,       U,       V,       X,       INPUT,               A2
C    1                                                     OUTPUT,      )       A2
C    2           TAPE1=Z, TAPE2=U, TAPE13=V, TAPE4=X, TAPE5=INPUT,
C    3                                               TAPE6=OUTPUT)
C     ------------------------------------------------------------              A2
C                                                                               A2
C     DEC 22/10 - S.KHARIN (ADJUSTMENTS TO THE PREVIOUS VECTOR PLOT CORRECTIONS)A2
C     DEC 07/10 - S.KHARIN (CORRECT THE START POSITION AND ANGLE OF VECTORS     
C                           WHEN INCX>1)                                        
C     FEB 20/08 - F.MAJAESS (FORCE READING "PLTINFO" FILE AS WELL WHEN POLAR    
C                            STEREOGRAPHIC PROJECTION IS REQUESTED VIA "JPS=1"  
C                            OR "JPS=11" CASES (INLINE WITH "COFAPS"/"GGAPS"    
C                            MODIFICATIONS TO GENERATE "PLTINFO" FILE).         
C     AUG 23/07 - B.MIVILLE (GGPLOT UPGRADE, NEW LEGEND, NEW COLORS, MORE COLOR
C                            CONTOURS, ARBITRARY CONTOURS, COUNTRY BORDERS)
C     NOV 08/04 - S.KHARIN (CORRECT A BUG IN HANDLING SUBAREA PREPARED FIELD(S))
C     MAY 13/04 - F.MAJAESS (ENSURE "ITYPE" CHECK IS DONE AS INTEGER)
C     MAR 11/04 - M.BERKLEY (MODIFIED for NCAR V3.2)
C     DEC 31/97 - R. TAYLOR (CLEAN UP GGPLOT UPGRADE, FIX BUGS, TEST, TEST, TEST)
C     AUG 31/96 - M. BERKLEY, T. GUI, M. DAVIS (UPDATE GGPLOT TO NCAR V3.2)
C     JUL 09/96 - TAO,FM (ENABLE SKIPPING "SPVAL" VALUES IN HAFTNP)
C     MAY 07/96 - F.MAJAESS (ENSURE GLOBAL GAUSSIAN GRID DATA ARE PLOTTED
C                            CORRECTLY AND REVISE DFCLRS CALL)
C     MAR 11/96 - F.MAJAESS (ISSUE WARNING WHEN PACKING DENSITY <= 0)
C     OCT  3/95 - M.BERKLEY (ADDED SPECIAL GRID BOX SHADING OPTION WITH )
C                            SOLID CONTOURS - NOTE: USES ICOSH = 'A')
C     JAN 12/95 - F.MAJAESS (ADDED GRID BOX SHADING OPTION)
C     JAN 12/94 - J.FYFE (CORRECT PLOTTING MASKED VECTOR FIELD)
C     JAN 12/94 - F.MAJAESS (CORRECT "SKIP" OPTION )
C     SEP 21/93 - M. BERKLEY (changed Holleriths to strings)
C     AUG 26/93 - F. MAJAESS (changed usage of real "IXA,IXB,IYA,IYB"
C                                                to "XA, XB, YA, YB")
C     AUG 05/93 - F. MAJAESS (modified for plotting on a shifted grid)
C     Jul 21/93 - M. BERKLEY (changed GGPLOT to save data files instead of
C                             generating meta code.)
C     MAY 12/93 - M. BERKLEY (changed MOD to MODEGG - MOD is an intrinsic)
C     FEB 10/93 - T. JANG   (corrected the plotting of vector fields)
C     JAN 13/93 - T. JANG   (changed units 1,2,3,4 to units 11,12,13,14 due to
C                            possible conflict with units used by NCAR)
C     OCT  5/92 - T. JANG   (replaced CRAY's "assign" with open to unit 45)
C     SEP  1/92 - T. JANG   (add format to write to unit 44)
C     JUL 20/92 - T. JANG   (changed variable "MAX" to "MAXX" so not to
C                            conflict with the generic function "MAX")
C     JUL 24/91 - S. WILSON (FIX COLOUR SHADING BUG AND ADD BLACK/WHITE
C                            CONTOUR LINE OPTION)
C     DEC 20/90 - S. WILSON (ADOPT COMMON GRAPH STYLE)
C     JUL 12/90 - F. MAJAESS (FIX BACKGROUND WITH 180 IN THE MIDDLE)
C     JUL 04/90 - F. MAJAESS (ADD A CHECK ON SUBAREA'S NHEM VALUES)
C     JUN 15/90 - F. ZWIERS (CORRECT POSITIONING OF PLOT TITLES)
C     APR 17/90 - C. BERGSTEIN (ADJUST THE LINE WIDTH CONTROL TO STANDARD,
C                               INCLUDE TENSION CONTROL,MODIFY POLSTR AND
C                               CYLEQ CONDITIONS, SET SPVAL=1.E38       )
C     MAR 07/90 - F. MAJAESS  (SET CONREC MASK VALUE = "10.E1500")
C     FEB 22/90 - C. BERGSETIN (SET UP FT44 FOR OVERLAY CONTROLING)
C     OCT 30/89 - F. MAJAESS (CORRECT LAT/LON VALUES FOR P.S. PLOT)
C     SEP 01/89 - H. REYNOLDS AND F. ZWEIRS (UPDATE TO NCAR V2.0)
C     JUN 13/88 - F. MAJAESS  (ADD A CHECK FOR KIND WITH WARNING EXIT)
C     APR 14/88 - F. MAJAESS  (REPLACE CALLS TO MOVLEV SUBROUTINE BY
C                              CALLS TO THE CRAY SUBROUTINE SCOPY)
C     FEB 29/88 - F. MAJAESS  (DISABLE HEAVY CONTINENTAL OUTLINE FOR
C                              PUBLICATION QUALITY OPTION)
C     AUG 10/87 - M.SUTCLIFFE (REWRITE..........................
C                              GENERALIZE PLOTTED AREA, ADD SECOND SCALAR
C                              FIELD OPTION, ADD CONTOUR LEVEL SHADING,
C                              THICKEN PUBLICATION QUALITY LINES, LABEL
C                              CORNER COORDINATES OF C.E. PROJ'N SUB-AREA
C                              PLOTS.)
C     AVR 10/85 - R.LAPRISE, B.DUGAS. (ADD PUBLICATION QUALITY OPTION,
C                              AUTOMATIC SCALING AND 2-D SIMPLE MAP FILLING
C                              AND ZEROING-OUT OF THE VECTOR POLE VALUES)
C
CGGPLOT  - CREATES NCAR MAP AND/OR VECTOR PLOT FROM FILE(S)             3  1 C GA1
C                                                                               A3
CAUTHORS - J.D.HENDERSON, L.LEFAIVRE, R.LAPRISE...                              A3
C                                                                               A3
CPURPOSE - PLOT MAPS OF Z, Z AND X, VECTOR FIELD OF (U,V), Z AND (U,V), OR      A3
C          Z, X AND (U,V), WITH OR WITHOUT CONTINENTAL OUTLINE, ON LAT-LON      A3
C          OR POLAR STEREOGRAPHIC PROJECTION. CONTOURS OF Z AND X MAY BY        A3
C          PLOTTED BY CONTOURING ISOTROPIC LINES, OR FILLING CONTOUR LEVELS     A3
C          WITH PATTERNS, OR A COMBINATION OF BOTH FOR EACH FIELD.              A3
C                                                                               A3
C          TO PLOT A SUB-AREA, USE SUBAREA PROGRAM TO PRODUCE SUB-AREA          A3
C          Z, X, U, V, AND PLTINFO FILE  AS NEEDED. IF SUBAREA IS NOT           A3
C          CALLED, Z, X, U, V WILL BE TREATED AS GLOBAL DATA, IE PLOTTED        A3
C          OVER A GLOBAL MAP.  THE PROGRAM USES THE NCAR PLOT PACKAGE.          A3
C                                                                               A3
C          NOTE - GGPLOT PROGRAM USES AND KNOWS ONLY OF THE SUB-AREA            A3
C                 COORDINATE VALUES IN THE PLTINFO FILE GENERATED               A3
C                 BY THE LAST CALL TO THE SUBAREA PROGRAM.  THEREFORE, THE      A3
C                 USER SHOULD NOT CALL GGPLOT PROGRAM TO PLOT FIELDS            A3
C                 PRODUCED BY CALLS TO SUBAREA PROGRAM WHERE THE SUB-AREA       A3
C                 COORDINATES VARIED. INSTEAD, THE USER SHOULD REARRANGE        A3
C                 AND PROCESS THE FIELDS IN GROUPS WHERE THE DATA WOULD         A3
C                 HAVE THE SAME SUB-AREA COORDINATES, (I.E. FOR EACH            A3
C                 GROUP, THE SUBAREA PROGRAM HAVE TO BE CALLED FIRST            A3
C                 (ONE OR SEVERAL TIMES WITH THE SAME SPECIFIED SUB-AREA        A3
C                 COORDINATES) FOLLOWED BY CALL(S) TO GGPLOT PROGRAM).          A3
C                                                                               A3
C                 THE SAME "PLTINFO" SCENARIO APPLIES FOR "POLAR STEREOGRAPHIC  A3
C                 PROJECTION" OPTION,(VIA "JPS=1"  OR "JPS=11"),WHERE THE DATA  A3
C                 CAN BE ASSUMED TO BE GENERATED SIMILAR TO "SUBAREA" CASE BY   A3
C                 "COFAPS"/"GGAPS" AND THE CORRESPONDING "PLTINFO" FILE         A3
C                 PRODUCED IN THE SAME FASHION AS WELL.                         A3
C                                                                               A3
C               - VALUES EQUAL TO "SPVAL" (SEE BELOW) ARE NOT PLOTTED.          A3
C                                                                               A3
C               - 2 TO 10 CARDS ARE READ IN.                                    A3
C                                                                               A3
C                                                                               A3
CINPUT FILES...                                                                 A3
C                                                                               A3
C      Z    = FILE CONTAINING GRIDS TO BE CONTOURED/SHADED                      A3
C      U,V  = FILES CONTAINING THE U AND V WIND COMPONENTS TO BE PLOTTED        A3
C      X    = POSSIBLE SECOND FILE CONTAINING GRIDS TO BE CONTOURED/SHADED      A3
C
CCARDS READ...                                                                  A5
C                                                                               A5
C      FOR CONTOUR MAP...                                                       A5
C      ===============                                                          A5
C      CARD 1-                                                                  A5
C      ------                                                                   A5
C      READ(5,5010)NSTEP,IGGLL,NAME,LEVEL,ICOSH1,ICOSH2,MS,SCAL,                A5
C                  FLO,HI,FINC,MODEGG,LHI,JPS                                   A5
C 5010 FORMAT(10X,I10,I1,A4,I5,2I1,I3,4E10.0,I1,2I2)                            A5
C                                                                               A5
C      NSTEP      TIMESTEP NUMBER                                               A5
C      IGGLL      SWITCH CONTROLS PLOTTING SCALAR FIELD(S) ON A SHIFTED GRID.   A5
C                 (APPLIES ONLY FOR PLOTTING SCALAR FIELDS WITH CYLINDRICAL     A5
C                  EQUIDISTANT PROJECTION)                                      A5
C                 = 0, SCALAR FIELD DATA IS ON A GAUSSIAN GRID,                 A5
C                 = 1, SCALAR FIELD DATA IS ON LAT-LON GRID SPANNING THE POLES, A5
C                 = 2, SCALAR FIELD DATA IS ON SHIFTED EAST-WEST (E-W) LAT-LON  A5
C                      GRID WHERE THE FIRST GRID POINT REFER TO THE POSITION AT A5
C                      THE SOUTH POLE AND AT HALF THE (E-W) GRID SPACING EAST   A5
C                      OF GREENWICH,                                            A5
C                 = 3, SCALAR FIELD DATA IS ON SHIFTED NORTH-SOUTH (N-S)        A5
C                      LAT-LON GRID WHERE THE FIRST GRID POINT REFER TO THE     A5
C                      POSITION AT GREENWICH AND AT HALF THE (N-S) GRID SPACING A5
C                      FROM THE SOUTH POLE,                                     A5
C                 = 4, SCALAR FIELD DATA IS ON SHIFTED (N-S & E-W) LAT-LON GRID A5
C                      WHERE THE FIRST GRID POINT REFER TO THE POSITION AT HALF A5
C                      THE (N-S) GRID SPACING FROM THE SOUTH POLE AND HALF THE  A5
C                      (E-W) GRID SPACING EAST OF GREENWICH.                    A5
C                                                                               A5
C                 NOTE : THE PROGRAM SUPPORT PLOTTING VECTOR FIELDS ONLY WITH   A5
C                        IGGLL=0 OR IGGLL=1.                                    A5
C                                                                               A5
C      NAME       NAME OF THE VARIABLE TO BE CONTOURED.                         A5
C                 IF NAME IS SKIP, THEN GGPLOT SKIPS ONE FIELD IN EACH INPUT    A5
C                 FILE.  MODEGG MUST BE CORRECTLY SPECIFIED, E.G., IF MODEGG=1  A5
C                 THEN GGPLOT WILL NOT SKIP A FIELD IN SCALAR FIELD INPUT       A5
C                 FILE "Z".                                                     A5
C      LEVEL      LEVEL NUMBER                                                  A5
C      ICOSH1     CONTOUR LINE/FILL CODE FOR FIRST SCALAR FIELD.                A5
C      ICOSH2     CONTOUR LINE/FILL CODE FOR SECOND SCALAR FIELD.               A5
C                 = 0, CONTOUR LINES ONLY                                       A5
C                 = 1, CONTOUR LINES, NO ZERO LINE                              A5
C                 = 2, CONTOURS AND SHADING                                     A5
C                 = 3, CONTOURS AND SHADING, NO ZERO LINE                       A5
C                 = 4, SHADING ONLY                                             A5
C                 = 5, CONTOURS & GRID BOX SHADING,                             A5
C                 = 6, CONTOURS & GRID BOX SHADING,NO ZERO LINE                 A5
C                 = 7, CONTOURS & GRID BOX SHADING,NO ZERO LINE,BOX SIDES DRAWN A5
C                 = 8, GRID BOX SHADING ONLY                                    A5
C                 = 9, GRID BOX SHADING WITH BOX SIDES DRAWN                    A5
C                 = A, CONTOURS & GRID BOX SHADING,BOX SIDES DRAWN,             A5
C                      SOLID CONTOURS - NOTE A IS HEX 10.                       A5
C                                                                               A5
C                 NOTE : FOR THE NO ZERO LINE OPTION, IT IS ASSUMED THAT THE    A5
C                        SCAL,FLO,HI,FINC ARE CHOSEN SO THAT (0.-FLO)/FINC      A5
C                        IS AN INTEGER DIVISIBLE BY TWO, AND THAT NEITHER       A5
C                        FLO OR HI = 0.                                         A5
C                        IN CASE OF PLOTTING 2 SCALAR FIELDS WITH SHADING, THE  A5
C                        FIELD TO BE SHADED MUST BE DONE FIRST.                 A5
C                                                                               A5
C      MS         PUBLICATION QUALITY OPTIONS:                                  A5
C                                                                               A5
C                 IF MS.LT.0, THEN PUBLICATION QUALITY OPTION IS SET, AND       A5
C                 PLOTS HAVE: TITLE, THICK LINES, NO INFORMATION LABEL,         A5
C                 LEGENDS, AND ALL CONTOUR LINES ARE SAME WIDTH.  LHI IS SET    A5
C                 TO -1, SO NO HIGH/LOW LABELS ARE DISPLAYED, AND MAP IS        A5
C                 CENTRED ON LON 0.                                             A5
C                                                                               A5
C                 IF MS.GE.0, THEN NORMAL QUALITY OPTION IS SET, AND PLOTS      A5
C                 HAVE THICK/THIN LINES, AND HIGH/LOW LABELS (DEPENDING UPON    A5
C                 LHI SETTING.  IN ADDITION, IF MS                              A5
C                  =7,  NO DASHED LINES - ALL CONTOURS SOLID                    A5
C                  =6,  SMOOTH SHADING INSTEAD OF CONTOURS.  ALSO -6.           A5
C                       WITH SMOOTH SHADING, GGPLOT INTERPOLATES COLOURS ON     A5
C                       HSV COLOUR WHEEL BETWEEN LEVELS.  WITH THE FOLLOWING    A5
C                       EXAMPLE CARDS, GGPLOT WILL:                             A5
C                            IF (Z.LT.-3) SHADE=130                             A5
C                            IF (Z.GE.-1) SHADE=106                             A5
C                            IF (Z.GE.-3.AND.Z.LT.-2) THEN                      A5
C                                   SHADE INTERPOLATED BETWEEN 130 AND 100.     A5
C                            IF (Z.GE.-2.AND.Z.LT.-1) THEN                      A5
C                                   SHADE INTERPOLATED BETWEEN 100 AND 106.     A5
C                        SHADING   0   3  130  100  106                         A5
C                        VALUES        -3.000    -2.000   -1.0000               A5
C                       ONLY 100 COLOURS ARE ALLOCATED.  SMOOTH IMAGES CAN      A5
C                       BE OBTAINED BY SPECIFYING FEW LEVELS AND LETTING        A5
C                       GGPLOT INTERPOLATE OVER ENTIRE RANGE.                   A5
C                  =4,  NO INFO LABEL, NO TITLE, NO LEGEND                      A5
C                  =3,  NO INFO LABEL, NO TITLE                                 A5
C                  =2,  NO INFO LABEL                                           A5
C                  =1,  NO TITLE                                                A5
C                  =0   NORMAL INFO LABEL AND TITLE                             A5
C                                                                               A5
C      SCAL             MULTIPLIES INPUT VALUES BY SCAL                         A5
C                  =0   SCAL,FLO,HI AND FINC ARE CHOSEN BY THE PROGRAM.         A5
C      FLO        LOWEST VALUE TO BE CONTOURED                                  A5
C      HI         HIGEST VALUE TO BE CONTOURED                                  A5
C      FINC       => 0 CONTOUR INTERVAL                                         A5
C                 <  0 WILL DO ARBITRARY CONTOUR LINES (SCALE .NE. 0) AND       A5
C                      ABS(FINC) WILL EQUAL THE NUMBER OF CONTOURS. AN EXTRA    A5
C                      INPUT CARD WILL BE READ AFTER THE SHADING CONTOUR VALUES A5
C                      IF PRESENT.                                              A5
C      MODEGG     = 0, 1 SCALAR FIELD ONLY.                                     A5
C                 = 1, VECTOR PLOT ONLY (CARD 1 USED ONLY TO DEFINE MODEGG      A5
C                      AND JPS, CARD 2 IS IGNORED, CARDS 3 AND 4 SHOULD         A5
C                      CONTAIN WHAT'S DESCRIBED BELOW FOR CARDS 9 AND 10,       A5
C                      AN ARGUMENT FOR FILE Z MUST BE SPECIFIED AS WELL AS THE  A5
C                      U AND V FILES ON THE PROGRAM CALL)                       A5
C                 = 2, BOTH SCALAR AND VECTOR FIELDS ARE PLOTTED.               A5
C                 = 3, TWO SCALAR FIELDS (IF BOTH CONTOURED, THE FIRST          A5
C                      WILL USE SOLID LINES, THE SECOND WILL USE DASHED)        A5
C                 = 4, TWO SCALAR FIELDS, 1 VECTOR FIELD.                       A5
C      LHI        =0,1,HIGHS,LOWS LABELLED. LON   0 IN CENTRE. M,P DRAWN.       A5
C                 =-1, .......NOT LABELLED. LON   0 IN CENTRE. M,P DRAWN.       A5
C                 = 2, HIGHS,LOWS LABELLED. LON 180 IN CENTRE. M,P DRAWN.       A5
C                 =-2, .......NOT LABELLED. LON 180 IN CENTRE. M,P DRAWN.       A5
C                 = 3, HIGHS,LOWS LABELLED. LON   0 IN CENTRE. M,P NOT DRAWN.   A5
C                 =-3, .......NOT LABELLED. LON   0 IN CENTRE. M,P NOT DRAWN.   A5
C                 = 4, HIGHS,LOWS LABELLED. LON 180 IN CENTRE. M,P NOT DRAWN.   A5
C                 =-4, .......NOT LABELLED. LON 180 IN CENTRE. M,P NOT DRAWN.   A5
C                 =+5, AS LHI=0, CONTOUR LINES NOT LABELLED.                    A5
C                 =+6, AS LHI=+1, CONTOUR LINES NOT LABELLED.                   A5
C                 =-6, AS LHI=-1, CONTOUR LINES NOT LABELLED.                   A5
C                 = ...                                                         A5
C                      .                                                        A5
C                       ...                                                     A5
C                 =-9, AS LHI=-4, CONTOUR LINES NOT LABELLED.                   A5
C                                                                               A5
C                 NOTE : THE LON 0/180 OPTION IS ONLY EXECUTED FOR GLOBAL       A5
C                        LAT-LON PLOTS. OTHERWISE IT IS IGNORED.                A5
C                                                                               A5
C                 NOTE : - EVERY 2ND CONTOUR IS LABELLED FOR STANDARD PLOTS.    A5
C                        - THE 'NO ZERO LINE OPTION' FORCES LABELS ON EVERY     A5
C                          SECOND CONTOUR.                                      A5
C                                                                               A5
C      ABS(JPS)   = 1, POLAR STEREOGRAPHIC PROJECTION                           A5
C                      (REQUIRES INFORMATION SUPPLIED VIA "PLTINFO" FILE).      A5
C                 = 7, SQUARE PLOT IRREGARDLESS OF ASPECT RATIO OF INPUT        A5
C                      FILE. TICKS ON SIDE OF PLOT AT EACH DATA POINT. THE      A5
C                      DATA POINTS ARE THE ORIGINAL GRID POINTS IF PLOTTING     A5
C                      A CYLINDRICAL EQUIDISTANT PLOT. OTHERWISE THEY ARE       A5
C                      INTERPOLATED POINTS, AND BEAR NO RELATION TO THE         A5
C                      ORIGINAL MODEL GRID. NO CONTINENTAL OUTLINE.             A5
C                 =0,8,CYLINDRICAL EQUIDISTANT PROJECTION.                      A5
C                 = 9, SAME AS FOR JPS=7, BUT THE ASPECT RATIO OF THE           A5
C                      PLOT IS THE SAME AS THAT OF THE INPUT FILE.              A5
C                 >=10 PLOT COUNTRIES BORDERS (JUST ADD 10 TO THE PROJECTION    A5
C                      VALUE YOU WANT (11,17,10,18 OR 19)                       A5
C      JPS        < 0, NO CONTINENTAL OUTLINE IS PLOTTED.                       A5
C                                                                               A5
C                 NOTE : IF JPS = 7 OR 9 WHEN PLOTTING A VECTOR FIELD ONLY,     A5
C                        OR NOT CONTOURING THE FIRST SCALAR FIELD (MODEGG=1 OR  A5
C                        ICOSH=4 OR ICOSH>7) JPS IS SET TO 8, AND A WARNING     A5
C                        MESSAGE IS PRINTED.                                    A5
C                        IF JPS = 1 ( OR 11) FOR POLAR STEREOGRAPHIC PROJECTION A5
C                        OPTION, THE DATA IS ASSUMED TO BE GENERATED BY         A5
C                        "SUBAREA","COFAPS" OR "GGAPS" WITH THE APPROPRIATE     A5
C                        INFORMATION PROPAGATED VIA "PLTINFO" FILE.             A5
C                                                                               A5
C      CARD 2-                                                                  A5
C      ------                                                                   A5
C      READ(5,5015) (LABEL1(I),I=1,80)                                          A5
C 5015 FORMAT(80A1)                                                             A5
C                                                                               A5
C      80 CHARACTER LABEL FOR CONTOUR MAP.                                      A5
C      ABSENT IF NAME=NC4TO8('SKIP').                                           A5
C                                                                               A5
C      Super- and sub- scripting can be performed by using standard             A5
C      NCAR PCHIQU character function codes.  For example, to make              A5
C      the following subscript:                                                 A5
C                   THERM   DEGREE                                              A5
C                        953                                                    A5
C      Use the :Bn: function code in the label:                                 A5
C                   THERM:B3:953   DEGREE                                       A5
C      Note the B3 means three subscripted characters and 3 blanks keep         A5
C      DEGREE from printing over top of 953.                                    A5
C                                                                               A5
C      Superscripts are similar, but use function code :Sn:                     A5
C                                                                               A5
C                                                                               A5
C      CARD 3-                                                                  A5
C      -------                                                                  A5
C      IF NPAT <= 7, CARD 3 will read one line.                                 A5
C                                                                               A5
C      READ(5,5020) WHITFG, NPAT, (IPAT(I),I=1,7)                               A5
C 5020 FORMAT(10X,I1,I4,7I5)                                                    A5
C                                                                               A5
C      WHITFG      .NE. 0 IF COLOUR PLOTS ARE TO HAVE WHITE CONTOURS            A5
C      NPAT        NUMBER OF DIFFERENT RANGES TO SHADE                          A5
C      IPAT(NPAT)  NPAT SHADING PATTERN CODES                                   A5
C                                                                               A5
C      IF 8 <= NPAT <= 14,  CARD 3 will read two lines.                         A5
C                                                                               A5
C      READ(5,5020) WHITFG, NPAT, (IPAT(I),I=1,7)                               A5
C 5020 FORMAT(10X,I1,I4,7I5)                                                    A5
C      READ(5,5080) (IPAT(I),I=8,NPAT)                                          A5
C 5080 FORMAT(15X,7I5)                                                          A5
C                                                                               A5
C      IF 15 <= NPAT <= 21,  CARD 3 will read three lines.                      A5
C                                                                               A5
C      READ(5,5020) WHITFG, NPAT, (IPAT(I),I=1,7)                               A5
C 5020 FORMAT(10X,I1,I4,7I5)                                                    A5
C      READ(5,5080) (IPAT(I),I=8,14)                                            A5
C      READ(5,5080) (IPAT(I),I=8,NPAT)                                          A5
C 5080 FORMAT(15X,7I5)                                                          A5
C                                                                               A5
C      IF 22 <= NPAT <= 28,  CARD 3 will read four lines.                       A5
C                                                                               A5
C      READ(5,5020) WHITFG, NPAT, (IPAT(I),I=1,7)                               A5
C 5020 FORMAT(10X,I1,I4,7I5)                                                    A5
C      READ(5,5080) (IPAT(I),I=8,14)                                            A5
C      READ(5,5080) (IPAT(I),I=14,21)                                           A5
C      READ(5,5080) (IPAT(I),I=22,NPAT)                                         A5
C 5080 FORMAT(15X,7I5)                                                          A5
C                                                                               A5
C      CARD 4-                                                                  A5
C      -------                                                                  A5
C      IF NPAT <= 7, CARD 4 will read one line.                                 A5
C                                                                               A5
C      READ(5,5025) (ZLEV(I),I=1,7)                                             A5
C 5025 FORMAT(10X,7E10.0)                                                       A5
C                                                                               A5
C      ZLEV(I) =    NPAT OR NPAT-1 DATA VALUES AT WHICH TO CHANGE SHADING       A5
C                   PATTERNS.                                                   A5
C                                                                               A5
C                   VAL < ZLEV(1)               - REGIONS < ZLEV(1) ARE SHADED  A5
C                                                 WITH FIRST SHADING PATTERN.   A5
C                                                                               A5
C                   ZLEV(I-1) <= VAL < ZLEV(I)  - REGIONS BETWEEN ZLEV(I-1)     A5
C                                                 AND ZLEV(I) ARE SHADED WITH   A5
C                                                 IPAT(I).                      A5
C                                                                               A5
C                   ZLEV(NPAT-1) <= VAL < ZLEV(NPAT)                            A5
C                                               - IF ZLEV(NPAT) IS SPECIFIED,   A5
C                                                 THEN REGIONS BETWEEN          A5
C                                                 ZLEV(NPAT-1) AND ZLEV(NPAT)   A5
C                                                 ARE SHADED WITH IPAT(NPAT).   A5
C                                                 REGIONS WITH VALUES LARGER    A5
C                                                 THAN ZLEV(NPAT) ARE           A5
C                                                 EFFECTIVELY LEFT UNSHADED.    A5
C                   ZLEV(NPAT-1) <= VAL < SPVAL/10                              A5
C                                               - IF ZLEV(NPAT) IS NOT          A5
C                                                 SPECIFIED, THEN REGIONS       A5
C                                                 BETWEEN ZLEV(NPAT-1) AND      A5
C                                                 SPVAL/10 ARE SHADED WITH      A5
C                                                 IPAT(NPAT).                   A5
C                                                                               A5
C      IF 8 <= NPAT <= 14,  CARD 4 will read two lines.                         A5
C                                                                               A5
C      READ(5,5025) (ZLEV(I),I=1,7)                                             A5
C 5025 FORMAT(10X,7E10.0)                                                       A5
C      READ(5,5090) (ZLEV(I),I=8,NPAT)                                          A5
C 5090 FORMAT(10X,7E10.0)                                                       A5
C                                                                               A5
C      IF 14 <= NPAT <= 21,  CARD 4 will read three lines.                      A5
C                                                                               A5
C      READ(5,5025) (ZLEV(I),I=1,7)                                             A5
C 5025 FORMAT(10X,7E10.0)                                                       A5
C      READ(5,5090) (ZLEV(I),I=8,14)                                            A5
C      READ(5,5090) (ZLEV(I),I=15,NPAT)                                         A5
C 5090 FORMAT(10X,7E10.0)                                                       A5
C                                                                               A5
C      IF 22 <= NPAT <= 28,  CARD 4 will read four lines.                       A5
C                                                                               A5
C      READ(5,5025) (ZLEV(I),I=1,7)                                             A5
C 5025 FORMAT(10X,7E10.0)                                                       A5
C      READ(5,5090) (ZLEV(I),I=8,14)                                            A5
C      READ(5,5090) (ZLEV(I),I=15,21)                                           A5
C      READ(5,5090) (ZLEV(I),I=22,NPAT)                                         A5
C 5090 FORMAT(10X,7E10.0)                                                       A5
C                                                                               A5
C                                                                               A5
C      FOR ARBITRARY CONTOUR LINES AND SHADING CONTOUR LEVELS OF FIRST SCALAR   A5
C      FIELD                                                                    A5
C      ================================================                         A5
C                                                                               A5
C     IF FINC < 0                                                               A5
C                                                                               A5
C      NPATS=-FINC                                                              A5
C                                                                               A5
C      NOTE: SCAL CAN NOT BE EQUAL TO ZERO IF YOU ARE REQUESTIONG ARBITRARY     A5
C            LINE CONTOURS.                                                     A5
C                                                                               A5
C      CARD 5-                                                                  A5
C      -------                                                                  A5
C                                                                               A5
C      DO K=1,NPATS,7                                                           A5
C         READ(5,5025,END=920) (ZLEVS(I),I=K,K+6)                               A5
C      ENDDO                                                                    A5
C                                                                               A5
C 5025 FORMAT(10X,7E10.0)                                                       A5
C                                                                               A5
C      NPATS       EXACT NUMBER OF CONTOUR LINES (NPATS=-FINC)                  A5
C      ZLEVS(I) =  NPATS DATA VALUES OF THE CONTOUR LINES.                      A5
C                                                                               A5
C     ELSE                                                                      A5
C                                                                               A5
C      FOR SECOND SCALAR FIELD                                                  A5
C      =======================                                                  A5
C      CARD 5-                                                                  A5
C      ------                                                                   A5
C      READ(5,5010)NSTEP,IGGLL,NAME,LEVEL,ICOSH1,ICOSH2,MS,SCAL,FLO,            A5
C                  HI,FINC,MODEGG,LHI,JPS                                       A5
C 5010 FORMAT(10X,I10,I1,A4,I5,2I1,I3,4E10.0,I1,2I2)                            A5
C                                                                               A5
C      VARIABLES IN () ARE NOT USED, BUT THEIR PLACES HAVE BEEN KEPT.           A5
C                                                                               A5
C      NSTEP      TIMESTEP NUMBER                                               A5
C      (IGGLL)    NOT USED                                                      A5
C      NAME       NAME OF THE VARIABLE TO BE CONTOURED.                         A5
C      LEVEL      LEVEL NUMBER                                                  A5
C      (ICOSH1)   NOT USED                                                      A5
C      (ICOSH2)   NOT USED                                                      A5
C      (MS)       PUBLICATION QUALITY OPTIONS.                                  A5
C      SCAL       SCALING FACTOR. IF SCAL = 0. THEN SCAL,FLO,HI AND             A5
C                 FINC ARE CHOSEN BY THE PROGRAM.                               A5
C      FLO        LOWEST VALUE TO BE CONTOURED                                  A5
C      HI         HIGEST VALUE TO BE CONTOURED                                  A5
C      FINC       CONTOUR INTERVAL                                              A5
C      (MODEGG)   NOT USED                                                      A5
C      LHI        =0,1,HIGHS,LOWS LABELLED                                      A5
C                 =-1, .......NOT LABELLED                                      A5
C      (JPS)      NOT USED                                                      A5
C                                                                               A5
C      CARD 6-                                                                  A5
C      -------                                                                  A5
C      READ(5,5015) (LABEL2(I),I=1,80)                                          A5
C 5015 FORMAT(80A1)                                                             A5
C                                                                               A5
C      80 CHARACTER LABEL FOR SECOND SCALAR FIELD                               A5
C                                                                               A5
C      FOR SHADING CONTOUR LEVELS OF SECOND SCALAR FIELD                        A5
C      =================================================                        A5
C      CARD 7-                                                                  A5
C      -------                                                                  A5
C      SAME AS CARD 3 ( UP TO 28 COLORS/PATTERNS)                               A5
C                                                                               A5
C      CARD 8-                                                                  A5
C      -------                                                                  A5
C      SAME AS CARD 4 ( UP TO 28 COLORS/PATTERNS)                               A5
C                                                                               A5
C      IF FINC < 0                                                              A5
C                                                                               A5
C      CARD 9-                                                                  A5
C      -------                                                                  A5
C      SAME AS CARD 5 ( UP TO 28 CONTOURS)                                      A5
C                                                                               A5
C      ELSE                                                                     A5
C                                                                               A5
C      FOR VECTOR PLOT...                                                       A5
C      ===============                                                          A5
C      CARD 9-                                                                  A5
C      -------                                                                  A5
C      READ(5,5030) VSCAL,VLO,VI,INCX,INCY                                      A5
C 5030 FORMAT(10X,3E10.0,2I5)                                                   A5
C                                                                               A5
C      VSCAL      SCALING FACTOR FOR VECTOR MAGNITUDE                           A5
C      VLO        LOWEST VECTOR MAGNITUDE TO BE PLOTTED                         A5
C      VI         VECTOR MAGNITUDE TO BE DRAWN AS ARROW OF LENGTH  DX           A5
C      INCX       EVERY INCX GRID POINT VECTOR IS PLOTTED ALONG X AXIS          A5
C      INCY       EVERY INCY GRID POINT VECTOR IS PLOTTED ALONG Y AXIS          A5
C                                                                               A5
C      NOTE : NO SCALING IS DONE IF INCX OR INCY IS LESS THAN 1 .               A5
C                                                                               A5
C      CARD 10-                                                                 A5
C      --------                                                                 A5
C      READ(5,5015) (LABELV(I),I=1,80)                                          A5
C 5015 FORMAT(80A1)                                                             A5
C                                                                               A5
C      80 CHARACTER LABEL FOR VECTOR PLOT.                                      A5
C                                                                               A5
CEXAMPLE OF INPUT CARDS...                                                      A5
C                                                                               A5
C*GGPLOT.         -1   -1   -134  0        1.     -100.      100.        5.4 3 9A5
C* CONTOUR AND SHADE FIRST FIELD FROM -100 TO 100 BY 5. (SCREEN FILLING).       A5
C*            7   52    0   48    0   48    0   52                              A5
C*                0.       10.       15.       20.       25.       30.          A5
C*GGPLOT.         -1   -1   -1             1.        0.      100.        5.  1  A5
C*  SHADE SECOND FIELD WITH SEVEN SHADING LEVELS.                               A5
C*            7   52    0   48    0   48    0   52                              A5
C*                0.       10.       15.       20.       25.       30.          A5
C*                1.        0.       10.    2    2                              A5
C*VECPLOT OF EVERY SECOND (U,V) VECTOR.                                         A5
C-------------------------------------------------------------------------------
C

C
C     MAXIMUM GRID SIZE
C
      use diag_sizes, only : SIZES_BLAT,
     &                       SIZES_BLONP1,
     &                       SIZES_NWORDIO
      PARAMETER (IM=SIZES_BLONP1,JM=SIZES_BLAT,
     & IJM=IM*JM,IJMV=IJM*SIZES_NWORDIO)

      INTEGER IDPU,NCL,TESTBM
      CHARACTER*32 CCLD

C--------------------------------------------------------------------
C Conditions/Counters/Dummies/Temp vars
      LOGICAL OK,EXX,LEAD,TRAIL,CLRPLT,CLRPLT2,PATPLT,PATPLT2
      INTEGER I,I1,I2,I3,I4,IDMY,ITMP,IX,IY,J,JR,K,NR,NL
      INTEGER LZZ,IGRID,MLEN,NC,ITSIZ,LTYPE,IFAC,NCLRS
      INTEGER LYR,LXR,ISX,KKK,LLEN
      REAL ASPECT,TDX,TDY,VMAX,VLEN,VMHI,SIDE
      REAL*8 SL,CL,WL,WOSSL,RAD
      REAL DX,DY,AVG,CHYPOS,CHRHGT,CHRATIO
      REAL ZA,ZB,ZC,ZD,ZE,ZF,ZG,ZH
      REAL XA,XB,YA,YB,XC,XD,YC,YD
      REAL VPL, VPR, VPB, VPT
      REAL LZZZ
      REAL RDM
      INTEGER IDM

      REAL DBGXA,DBGXB,DBGYA,DBGYB,DBGXC,DBGXD,DBGYC,DBGYD
      INTEGER DBGLTYPE

C Storage for adjusted HI and FLO
      REAL FHIGHN, FLOWN

C Flags - graphics qualities
C     Rotate plot
      LOGICAL FLIP
C     Draw map over plots
      LOGICAL MAP
C     M (meridians) and P (parallels) should be drawn
      LOGICAL MERPRLL
C     Want a square plot
      LOGICAL SQUARE
C     Tick marks are required
      LOGICAL TICK
C     Draw legend
      LOGICAL LGND
C     white contours versus black contours for colour plots
      INTEGER  WHITFG1, WHITFG2

C Graphics variables
C     Array to pass user defined colours to dfclrs - unused in ggplot
      REAL HSVV(3, 28)
C     Spacing between points of background on polar stereographic plots - POLSTR
C         - overloaded as positive/negative for continuous/dotted bacground
      INTEGER MR
C     Passed to CONREC, controls scaling
      INTEGER NSETC
C     passed to VELVCT, controls scaling
      INTEGER NSETV
C     Line width scaling factors
      REAL RLWBG, RLWBGP, RLWBGC, RLWBGCP

C Plot Flags
C     Plot a subarea
      LOGICAL SUBAREA
C     Draw contour lines for 1st/2nd field
      LOGICAL CONT1,CONT2
C     Draw shaded contours for 1st/2nd field
      LOGICAL SHAD1,SHAD2
C     Don't draw zero line for FIRST/SECOND field.
      LOGICAL NZERO1,NZERO2
C     Use box shading for 1st/2nd field
      LOGICAL BOXSHD1,BOXSHD2
C     Draw box for 1st/2nd field
      LOGICAL BOXDRW1,BOXDRW2
C     Smooth Shading
      LOGICAL SMSHAD1,SMSHAD2
C     Drawing the second plot
      LOGICAL SECOND

C Plot Variables
C     Number of plots so far
      INTEGER NPLOT
C     Labels for 1st/2nd/vector fields with equivalent array variables
      CHARACTER*1 LABEL1(83),LABEL2(83),LABELV(83)
      CHARACTER*83 ALABL1,ALABL2,ALABLV
      EQUIVALENCE (LABEL1,ALABL1),(LABEL2,ALABL2),(LABELV,ALABLV)
C     Type of projection to draw
C        - ST for polar stereographic or CE for cylindrical equidistant
      CHARACTER*(2) PROJ
C     Special value
      REAL SPV(2)
C     Interval for vectors - along X/Y axis
      INTEGER INCX, INCY
C     Vector magnitude to be drawn as arrow of length  dx
      REAL VI
C     Lowest vector magnitude to plot
      REAL VLO
C     MAPPOS Parameters
      REAL XX1,DXX,YY1,DYY

C     CARD INPUT
C     timestep number
      INTEGER NSTEP,NSTEP1,NSTEP2
C     Labelling of highs/lows, centre longitude, meridian/parallel drawing
C      - LHI2 comes from second scalar card, but is unused
      INTEGER LHI1,LHI2
C     Control scalar field plotting (grid shift)
      INTEGER IGGLL
C     Name of the variable/field to be contoured
      INTEGER NAME1,NAME2
C     Level number
      INTEGER LEVEL,LEVEL1,LEVEL2
C     Type of contours for scalar fields (lines, shading, boxed, etc.)
      INTEGER ICOSH1,ICOSH2
C     Quality options
      INTEGER MS1,MS2
      LOGICAL PUB, DOTITLE, DOINFOLABEL, DOLGND, DOLABEL
C     Scaling Factor
      REAL SCAL1,SCAL2
C     Lowest/highest value to be contoured
      REAL FLO1,HI1
      REAL FLO2,HI2
C     Contour interval
      REAL FINC1,FINC2
C     Plotting mode - up to 2 scalar and 1 vector
      INTEGER MODEGG
C     Projection mode
C       - overloaded to control aspect ratio
C       - overloaded to control whether continental outline is plotted
      INTEGER JPS
C     Vector scaling
      REAL VSCAL
C     Parameters to define area of interest
C       - maxsize or modified by subarea
      REAL RLATMN(2),RLATMX(2),RLONMN(2),RLONMX(2),DGRW
      INTEGER NHEM, NOUTYP, IOST
C     Logaritmic BASE 10 plot
      INTEGER ILOG1,ILOG2
C CCRN Field Variables
C     Number of fields
      INTEGER NF
C     Max size of field buffer
      INTEGER MAXX
C     8 word labels
      INTEGER IBUF,JBUF
C     Data
      INTEGER IDAT,JDAT
C     large working storage
      REAL GG2,U,V,GG22,U2
C     size of field, copied from IBUF
      INTEGER LX,LY,LXY,LYH,LX2,LY2,LXY2
C     logical unit numbers for saving card images, data, etc.
      INTEGER LUNOFFSET
      INTEGER LUNFIELD1,LUNFIELD2,LUNFIELD3,LUNFIELD4
      LOGICAL LFOPEN1,LFOPEN2,LFOPEN3,LFOPEN4
      INTEGER IOERR

C NCAR Variables
      REAL SPVAL
C     * SOLID LINE MAGIC NUMBER
      INTEGER ISOLID
C     * LAND OVERLAP OPTION
      INTEGER ISHADSPVAL
C     Dash patterns for use by CPDRPL.
C     - these are magic numbers - not to be changed without thought.
      INTEGER NDASH1,NDASH2
      PARAMETER (NDASH1 =  B"11111111111111111111110101010101")
      PARAMETER (NDASH2 =  B"00000000000000000000000101010101")
      CHARACTER*4 POSDASHPAT1,NEGDASHPAT1,POSDASHPAT2,NEGDASHPAT2
      CHARACTER*4 DASHPATSOL,DASHPATDASH
      PARAMETER (DASHPATSOL='$$$$')
      PARAMETER (DASHPATDASH='$''$''')

C     Conversion subroutine function
      REAL CFUX
      EXTERNAL CFUX

C Workspace arrays for NCARG routines.
      INTEGER LRWK, LIWK
      PARAMETER (LRWK=15000, LIWK=7000)
      REAL RWRK(LRWK)
      INTEGER IWRK(LIWK)
C
      CHARACTER*4 ATYPE, CGGPL
      INTEGER*4 ITYPE, IGGPL
C ITYPE is used by all plotting programs.
      COMMON /CCCPLOT/ ITYPE
C ARG is used by LBFILL
      INTEGER ARG
      COMMON /PAT/ ARG
C Publication common block
      COMMON /PPPP/ PUB, SHAD1, DOTITLE, DOINFOLABEL, DOLGND, DOLABEL
C     HI LO COMMON BLOCK FOR HI AND LOW
      INTEGER ILOG
      REAL OFFSET
      COMMON /HILO/ ILOG, OFFSET
C--------------------------------------------------------------------
C     DASHLINE Common Block
      COMMON /INTPR/
     1     IPAU,FPART,TENSN,NP,SMALL,L1,ADDLR,ADDTB,MLLINE,ICLOSE
      INTEGER IPAU,NP,L1,MLLINE,ICLOSE
      REAL FPART,TENSN,SMALL,ADDLR,ADDTB

C--------------------------------------------------------------------
C For passing colour index array into colour FILL routines through
C Common Block.
      INTEGER NIPAT,NIPAT1,NIPAT2,NPAT,NPAT1,NPAT2,MAXPAT,MAXCLRS
      INTEGER NIPATS,IARBI,IARBI1,IARBI2,NPATS,NPATS1,NPATS2
      PARAMETER(NIPAT = 28, NIPATS = 28)
      INTEGER IPAT(NIPAT),IPAT1(NIPAT),IPAT2(NIPAT)
      REAL ZLEV(NIPAT),ZLEV1(NIPAT),ZLEV2(NIPAT)
      COMMON /FILLCB/ IPAT,ZLEV,NPAT,MAXPAT,MAXCLRS
      INTEGER IHSV
      REAL ZLEVS(NIPATS),ZLEVS1(NIPATS),ZLEVS2(NIPATS),CWM
      COMMON /ARBI/ ZLEVS,NPATS,IARBI, CWM
      REAL SABR(NIPATS)
      COMMON /RGB/ SABR

      INTEGER IGGP_CPAREA_SIZE
      PARAMETER (IGGP_CPAREA_SIZE=2000000)
      INTEGER IGGP_CPAREA(IGGP_CPAREA_SIZE)
      COMMON /GGP_AREA/IGGP_CPAREA

      COMMON /GGP_MAP_LIMITS/ RLATMN,RLATMX,RLONMN,RLONMX,DGRW

C     logical unit numbers for saving card images, data, etc.
      INTEGER LUNCARD,LUNDATAFILES(4),UNIQNUM
      PARAMETER (LUNOFFSET=10,
     1     LUNFIELD1=LUNOFFSET+1,LUNFIELD2=LUNOFFSET+2,
     2     LUNFIELD3=LUNOFFSET+3,LUNFIELD4=LUNOFFSET+4)
C
      COMMON/GAUS/SL(JM),CL(JM),WL(JM)
      COMMON/GAUS/WOSSL(JM),RAD(JM)
C
C
C     * DECLARE NCAR PLUGINS EXTERNAL, SO CORRECT VERSION ON SGI
C
      EXTERNAL AGCHCU,CPCHHL,CPCHIL,CONTDF,DFNCLR,GGP_COLSHD,
     1         GGP_COLSMSHD,GGP_CONLS,GGP_ILDEF,GGP_LEGEND,
     2         GGP_PATTERN,HOV_LEGEND,LBFILL,MAPUSR,NEW_HAFTNP,
     3         PATTERN,PRECON3,SAMPLE,SETPAT,SP_LEGEND,STRINDEX

C
C     * DECLARE BLOCK DATAS EXTERNAL, SO INITIALIZATION HAPPENS
C
      EXTERNAL AGCHCU_BD
      EXTERNAL FILLCBBD
      EXTERNAL FILLPAT_BD

      INTEGER NC4TO8
      EXTERNAL NC4TO8

C     CCRN field data common blocks - 8 word label + field data
C
C     4672082
C
      COMMON /ICOM/ IBUF(8),IDAT(IJMV)
      COMMON /JCOM/ JBUF(8),JDAT(IJMV)
C     common block for large working storage
      COMMON /BLANCK/ GG2(IJM),U(IJM),V(IJM),
     1     GG22(IJM),U2(IJM)
      INTEGER BU(IJM)
      EQUIVALENCE (CGGPL,IGGPL)

      DATA MAXX/IJMV/
C     DATA SPV/0.0,0.0/
      DATA SPV/1.0E38,1.0E38/
      DATA ITSIZ /16/, CHRATIO /1.52381/
C
C     * LINE WIDTHS (BACKGROUD, MAJOR AND MINOR)
C
C     * STANDARD MONOCHROME
      DATA RLWBG/1.000/
C     * PUBLICATION MONOCHROME
      DATA RLWBGP/2.000/
C     * STANDARD COLOUR
      DATA RLWBGC/2.000/
C     * PUBLICATION COLOUR
      DATA RLWBGCP/2.000/

      CGGPL='GGPL'

      SPVAL=1.0E38
CCCC      WRITE(*,*) MLLINE
      MLLINE=40
C
C--------------------------------------------------------------------
C
      NF=6
      CALL JCLPNT(NF,LUNFIELD1,LUNFIELD2,LUNFIELD3,LUNFIELD4,5,6)
      CALL PSTART
      CALL INIT_MAPLIMITS
      INQUIRE(FILE='AAAZMAP',EXIST=EXX)

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C     * REWIND NECESSARY FILES ONCE
C
      INQUIRE(LUNFIELD1,IOSTAT=IOERR,OPENED=LFOPEN1)
      IF(IOERR.EQ.0.AND.LFOPEN1) REWIND LUNFIELD1
      INQUIRE(LUNFIELD2,IOSTAT=IOERR,OPENED=LFOPEN2)
      IF(IOERR.EQ.0.AND.LFOPEN2) REWIND LUNFIELD2
      INQUIRE(LUNFIELD3,IOSTAT=IOERR,OPENED=LFOPEN3)
      IF(IOERR.EQ.0.AND.LFOPEN3) REWIND LUNFIELD3
      INQUIRE(LUNFIELD4,IOSTAT=IOERR,OPENED=LFOPEN4)
      IF(IOERR.EQ.0.AND.LFOPEN4) REWIND LUNFIELD4

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C     * INITIALIZE PLOT OPTIONS
C
      NPLOT=0
 110  WRITE(6,'(1X,A)') "Initialize plot options..."

C
C     * SET NCAR OPTIONS TO DEFAULTS
C
CC Set line width to default value for Ezmap.
      CALL GSLWSC(RLWBG)
C  Set contour line label flag
      DOLABEL=.TRUE.
C     +/- FOR CONTINUOUS/DOTTED BACKGROUND
      MR=0
C
C     * SET THE MASK VALUE FOR CONREC
C
      WRITE(6,6000) SPVAL

C     ITYPE    = 'GGPL'
      ITYPE    = IGGPL
      SECOND   = .FALSE.
      SQUARE   = .FALSE.
      TICK     = .FALSE.
      FLIP     = .FALSE.
      MERPRLL  = .FALSE.
      MAP      = .FALSE.
      PUB      = .FALSE.
      DOTITLE  = .TRUE.
      DOINFOLABEL = .TRUE.
      NZERO1   = .FALSE.
      NZERO2   = .FALSE.
      CONT1    = .FALSE.
      CONT2    = .FALSE.
      SHAD1    = .FALSE.
      SHAD2    = .FALSE.
      CLRPLT   = .FALSE.
      CLRPLT2  = .FALSE.
      PATPLT   = .FALSE.
      PATPLT2  = .FALSE.
      WHITFG1  = 0
      WHITFG2  = 0
      BOXSHD1  = .FALSE.
      BOXSHD2  = .FALSE.
      BOXDRW1  = .FALSE.
      BOXDRW2  = .FALSE.
      SMSHAD1  = .FALSE.
      SMSHAD2  = .FALSE.
      PROJ     = 'CE'
      SUBAREA  = .FALSE.
      ISOLID = 65535
      NSETC = -1
      NSETV = -1
      NPAT=0
      NPATS=0
      NPATS1=0
      NPATS2=0
      NPAT1=0
      NPAT2=0
      IARBI1=0
      IARBI2=0
      ARG=0
      ILOG=0
      OFFSET=0.0
      ILOG1=0
      ILOG2=0
      ISHADSPVAL = 1
      IHSV = 0
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C     INPUT CARD SECTION
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C     * CARD 1 ******************************
C
      NAME1=NC4TO8('SKIP')
      DO WHILE(NAME1.EQ.NC4TO8('SKIP'))
         READ(5,5010,END=905) NSTEP1,IGGLL,NAME1,LEVEL1,ICOSH1,ICOSH2,
     1        MS1,SCAL1,FLO1,HI1,FINC1,MODEGG,LHI1,JPS
C
         CWM=1.0
C
         IF(MS1.GT.10) THEN
            CWM=1.0*INT(MS1/10)
            MS1=MS1-INT(CWM)*10
            CWM=CWM*0.1
         ENDIF
C
         IMAPC=0
         IF(JPS.GE.10) THEN
            IMAPC=1
            JPS=JPS-10
         ENDIF
C
         IFINC=0
         IARBI1=0
C
         IF(FINC1.LT.0.) THEN
            IF(SCAL1.EQ.0.) THEN
               WRITE(6,*) 'CAN NOT HAVE BOTH SCALE = 0 AND FINC < 0'
               CALL                                PXIT('GGPLOT',-1)
            ENDIF
            FINC1=-FINC1
            IARBI1=INT(FINC1)
         ENDIF
C
         IF(JPS.EQ.2) THEN
            JPS=1
         ENDIF
C
         IF(MODEGG.EQ.5)THEN
            MODEGG=0
            ILOG1=1
         ELSEIF(MODEGG.EQ.6)THEN
            MODEGG=3
            ILOG1=1
         ELSEIF(MODEGG.EQ.7)THEN
            MODEGG=0
            ILOG1=1
            READ(5,5012) OFFSET
         ELSEIF(MODEGG.EQ.8)THEN
            MODEGG=3
            ILOG1=1
            READ(5,5012) OFFSET
         ENDIF
         IF (NAME1.EQ.NC4TO8('SKIP')) THEN
            IF(LFOPEN1.AND.MODEGG.NE.1) THEN
               CALL FBUFFIN(LUNFIELD1,IBUF,MAXX,KKK,LLEN)
               IF (KKK.GE.0) GO TO 990
            ENDIF
            IF(LFOPEN2.AND
     1           .(MODEGG.EQ.1.OR.MODEGG.EQ.2.OR.MODEGG.EQ.4)) THEN
               CALL FBUFFIN(LUNFIELD2,IBUF,MAXX,KKK,LLEN)
               IF (KKK.GE.0) GO TO 990
            ENDIF
            IF(LFOPEN3.AND.(MODEGG.EQ.1.OR.MODEGG.EQ.2)) THEN
               CALL FBUFFIN(LUNFIELD3,IBUF,MAXX,KKK,LLEN)
               IF (KKK.GE.0) GO TO 990
            ENDIF
            IF(LFOPEN4.AND.(MODEGG.EQ.3.OR.MODEGG.EQ.4)) THEN
               CALL FBUFFIN(LUNFIELD4,IBUF,MAXX,KKK,LLEN)
               IF (KKK.GE.0) GO TO 990
            ENDIF
            GOTO 110
         ENDIF
      ENDDO

C     SHADING LOGIC CONTROLLED BY CARD 1
      IF (MODEGG.NE.1.AND.ICOSH1.GT.1)     SHAD1 =  .TRUE.
      IF (MODEGG.GT.2.AND.ICOSH2.GT.1)     SHAD2 =  .TRUE.

C
C     * CARD 2 *************************
C
      READ(5,5015,END=910) (LABEL1(I),I=1,80)

C
C     * CARDS 3 AND 4 ******************
C
      IF (SHAD1) THEN
C       READ PATTERNS
        READ(5,5020,END=915) WHITFG1,NPAT1,(IPAT1(I),I=1,7)
        IF(ABS(NPAT1).GT.NIPATS) CALL              PXIT('GGPLOT',-2)
        IF (NPAT1.LT.0) THEN
           NPAT1=-NPAT1
           IHSV=1
           READ(5,5082) (SABR(I),I=1,7)
        ENDIF
        IF (NPAT1 .GT. 7) THEN
          READ(5,5080) (IPAT1(I),I=8,14)
          IF(IHSV.EQ.1) THEN
             READ(5,5082) (SABR(I),I=8,14)
          ENDIF
        ENDIF

        IF (NPAT1 .GT. 14) THEN
           READ(5,5080) (IPAT1(I),I=15,21)
          IF(IHSV.EQ.1) THEN
             READ(5,5082) (SABR(I),I=15,21)
          ENDIF
        ENDIF
        IF (NPAT1 .GT. 21) THEN
           READ(5,5080) (IPAT1(I),I=22,28)
          IF(IHSV.EQ.1) THEN
             READ(5,5082) (SABR(I),I=22,28)
          ENDIF
        ENDIF

C       READ LEVELS
        READ(5,5025,END=920) (ZLEV1(I),I=1,7)
        IF (NPAT1 .GT. 7) THEN
          READ(5,5090) (ZLEV1(I),I=8,14)
        ENDIF
         IF (NPAT1 .GT. 14) THEN
            READ(5,5090) (ZLEV1(I),I=15,21)
         ENDIF
         IF (NPAT1 .GT. 21) THEN
            READ(5,5090) (ZLEV1(I),I=22,28)
         ENDIF



C       * ADD DEFAULT FOR LAST LEVEL
        IF((ZLEV1(NPAT1).LE.ZLEV1(NPAT1-1))
     1       .AND.(ZLEV1(NPAT1).EQ.0.0)) THEN
           ZLEV1(NPAT1)=SPVAL/10.0
        ENDIF

C       * CHECK THAT LEVELS INCREASE
        DO I=1,NPAT1-1
           IF(ZLEV1(I+1).LE.ZLEV1(I)) THEN
              WRITE(6,6003)I+1,I
              CALL                                 PXIT('GGPLOT',-3)
           ENDIF
        ENDDO
C
         DO I=1,NPAT-1
            IF(ZLEV(I).GE.1E37) THEN
               ISHADSPVAL=0
            ENDIF
         ENDDO

      ENDIF
C
C     * ARBITRARY LINE CONTOURS
C
      IF (IARBI1.NE.0) THEN
         NPATS1=IARBI1
C     READ LEVELS
         IF(NPATS1.GT.NIPATS) THEN
            WRITE(6,*) 'TOO MANY CONTOUR LEVELS, NPATS=',NPATS1
            CALL                                   PXIT('GGPLOT',-4)
         ENDIF
         DO K=1,NPATS1,7
            READ(5,5025,END=920) (ZLEVS1(I),I=K,K+6)
         ENDDO
C
      ENDIF
C
C
C      * READ SECOND SCALAR FIELD AND ITS CARDS
C
      IF (MODEGG.GT.2) THEN
C        * CARDS 5 AND 6 ***************
         READ(5,5010,END=935) NSTEP2,IDMY,NAME2,LEVEL2,I1,I2,
     1        MS2,SCAL2,FLO2,HI2,FINC2,I3,LHI2,I4
C
         IFINC=0
         IARBI2=0
C
         IF(FINC2.LT.0.) THEN
            IF(SCAL2.EQ.0.) THEN
               WRITE(6,*) 'CAN NOT HAVE BOTH SCALE = 0 AND FINC < 0'
               CALL                                PXIT('GGPLOT',-5)
            ENDIF
            FINC2=-FINC2
            IARBI2=INT(FINC2)
         ENDIF
C
C         IF(SCAL2.LT.0.) THEN
C            SCALE2=SCAL2
C            SCAL2=-SCAL2
C         ENDIF
C
C        * FOR LOG CONTOURS CHOOSE MODEGG=5
C
         IF(I3.EQ.5.OR.I3.EQ.6.OR.I3.EQ.7.OR.I3.EQ.8) ILOG2=1
C
         READ(5,5015,END=940) (LABEL2(I),I=1,80)


C        * CARDS 7 AND 8 **********************
C
C        * READ SECOND SCALAR, UP TO 28 COLOR SHADINGS
C
         IF (SHAD2) THEN
            READ(5,5020,END=945) WHITFG2, NPAT2,(IPAT2(I),I=1,7)
            IF(NPAT2.GT.NIPATS) CALL               PXIT('GGPLOT',-6)
C
            IF (NPAT2 .GT. 7) THEN
               READ(5,5080,END=950) (IPAT2(I),I=8,14)
            ENDIF
C
            IF (NPAT2 .GT. 14) THEN
               READ(5,5080,END=950) (IPAT2(I),I=15,21)
            ENDIF
            IF (NPAT2 .GT. 21) THEN
               READ(5,5080,END=950) (IPAT2(I),I=22,28)
            ENDIF
C       READ LEVELS
            READ(5,5025,END=920) (ZLEV2(I),I=1,7)
            IF (NPAT2 .GT. 7) THEN
               READ(5,5090) (ZLEV2(I),I=8,14)
            ENDIF
            IF (NPAT2 .GT. 14) THEN
               READ(5,5090) (ZLEV2(I),I=15,21)
            ENDIF
            IF (NPAT2 .GT. 21) THEN
               READ(5,5090) (ZLEV2(I),I=22,28)
            ENDIF
C          * ADD DEFAULT FOR LAST LEVEL
           IF((ZLEV2(NPAT2).LE.ZLEV2(NPAT2-1))
     1          .AND.(ZLEV2(NPAT2).EQ.0.0)) THEN
              ZLEV2(NPAT2)=SPVAL/10.0
           ENDIF
C          * CHECK THAT LEVELS INCREASE
           DO I=1,NPAT2-1
              IF(ZLEV2(I+1).LE.ZLEV2(I)) THEN
                 WRITE(6,6003)I+1,I
                 CALL                              PXIT('GGPLOT',-7)
              ENDIF
           ENDDO
         ENDIF
      ENDIF
C
C     * ARBITRARY LINE CONTOURS
C
      IF (IARBI2.NE.0) THEN
         NPATS2=IARBI2
C     READ LEVELS
         IF(NPATS2.GT.NIPATS) THEN
            WRITE(6,*) 'TOO MANY CONTOUR LEVELS, NPATS=',NPATS2
            CALL                                   PXIT('GGPLOT',-8)
         ENDIF
         DO K=1,NPATS2,7
            READ(5,5025,END=920) (ZLEVS2(I),I=K,K+6)
         ENDDO
C
      ENDIF
C
      IF (MODEGG.NE.0.AND.MODEGG.NE.3) THEN
C        * READ THE VECTOR PLOT CARDS AND THE U FIELD.
C        * CARDS 9 AND 10 **********************
         READ(5,5030,END=955) VSCAL,VLO,VI,INCX,INCY
         READ(5,5015,END=960) (LABELV(I),I=1,80)
      ENDIF


CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C     Initialize NCAR Graphics
      CALL GSFAIS (1)

      CALL SET(0.,1.,0.,1.,0.,1.,0.,1.,1)
      CALL RESET
      CALL DASHDB(ISOLID)
      CALL PCSETI('CD - DATASET - COMPLEX OR DUPLEX', 1)
      CALL GSTXFP(-4,2)
      CALL PCSETI('OF - OUTLINE FLAG', 1)
      CALL PCSETI('OL - OUTLINE WIDTH', 2)
      CALL PCSETR('CL - PRINCIPAL LINE WIDTH',.2)

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C     * SET-UP STATE FLAGS AND SET PARAMETER VARIABLES FROM INPUT
C
C     * ENSURE ONLY SCALAR FIELD(S) ARE PLOTTED ON A SHIFTED GRID
C
      IF ( IGGLL.GT.1) THEN
         IF (MODEGG.EQ.1 .OR. MODEGG.EQ.2 .OR. MODEGG.EQ.4 ) THEN
            WRITE(6,6070)
            CALL                                   PXIT('GGPLOT',-101)
         ENDIF
      ENDIF

C
C     * SET PLOT OPTIONS ACCORDING TO FIRST CARD
C
      IF (MODEGG.NE.1.AND.(ICOSH1.LT.8.AND.ICOSH1.NE.4)) CONT1=.TRUE.
      IF (MODEGG.GT.2.AND.(ICOSH2.LT.8.AND.ICOSH2.NE.4)) CONT2=.TRUE.
      IF (CONT1.AND.CONT2) THEN
C        MAGIC CONREC NUMBER:
C            If ABS(NDOT) = 0, 1, or 1023, solid lines are drawn.
         POSDASHPAT1=DASHPATSOL
         NEGDASHPAT1=DASHPATSOL
         POSDASHPAT2=DASHPATDASH
         NEGDASHPAT2=DASHPATDASH
      ELSE IF(CONT1) THEN
         POSDASHPAT1=DASHPATSOL
         NEGDASHPAT1=DASHPATDASH
      ELSE IF(CONT2) THEN
         POSDASHPAT2=DASHPATSOL
         NEGDASHPAT2=DASHPATDASH
      ENDIF

      WRITE(6,6010) NPLOT+1

C     OPTION - NO ZERO LINE ?
      IF (CONT1.AND.(ICOSH1.EQ.1.OR.ICOSH1.EQ.3
     1           .OR.ICOSH1.EQ.6.OR.ICOSH1.EQ.7))   NZERO1 = .TRUE.
      IF (CONT2.AND.(ICOSH2.EQ.1.OR.ICOSH2.EQ.3
     1           .OR.ICOSH2.EQ.6.OR.ICOSH2.EQ.7))   NZERO2 = .TRUE.
C     IF NO ZERO LINE, THEN LABELS EITHER

C     BOX SHADING?
      IF (SHAD1.AND.ICOSH1.GT.4) THEN
         BOXSHD1 =  .TRUE.
      ENDIF
      IF (SHAD2.AND.ICOSH2.GT.4) THEN
         BOXSHD2 =  .TRUE.
      ENDIF

C     DRAW BOX?
      IF(BOXSHD1.AND.(ICOSH1.EQ.7.OR.ICOSH1.EQ.9)) THEN
         BOXDRW1 =  .TRUE.
      ENDIF
      IF(BOXSHD2.AND.(ICOSH2.EQ.7.OR.ICOSH2.EQ.9)) THEN
         BOXDRW2 =  .TRUE.
      ENDIF
C     0 and 8 ARE THE SAME
      IF (JPS.EQ.0) JPS = 8
C     7 and 9 INDICATE SQUARE PLOT
      IF (ABS(JPS).EQ.7.OR.ABS(JPS).EQ.9) THEN
         IF (MODEGG.EQ.1.OR.(SHAD1.AND..NOT.CONT1)) THEN
            JPS = 8
            WRITE(6,6005)
         ELSE
            TICK =   .TRUE.
            IF (ABS(JPS).EQ.7) SQUARE = .TRUE.
         ENDIF
      ENDIF
C     DRAW CONTINENTAL MAP?
      IF (JPS.GE.0) MAP = .TRUE.
      JPS = ABS(JPS)
C     CHOOSE PROJECTION
      IF (JPS.EQ.1) THEN
         PROJ = 'ST'
      ELSE
         PROJ = 'CE'
      ENDIF
C
C       * IF DRAWING TICKS, LET CONREC SET THE VIEWPORT.
C
      IF (TICK) THEN
         NSETC = 0
      ENDIF

C
C     SELECT HIGH/LOW LABELLING, CENTRE LON AND DRAWING OF MERIDIANS/PARALLELS
      IF (LHI1.EQ.0) THEN
         LHI1 = 1
      ELSE IF (LHI1.LT.0) THEN
         IF(LHI1.LT.-4) THEN
            LHI1=LHI1+5
            DOLABEL=.FALSE.
         ENDIF
      ELSE
         IF(LHI1.GT.4) THEN
            LHI1=LHI1-5
            IF(LHI1.EQ.0) LHI1=1
            DOLABEL=.FALSE.
         ENDIF
      ENDIF


C     FLIP PLOT TO GET LON 0 IN CENTRE
      IF ((PROJ.NE.'ST').AND.(ABS(LHI1).EQ.1.OR.ABS(LHI1).EQ.3)) THEN
         FLIP=.TRUE.
      ENDIF

C     DRAW MERIDIANS/PARALLELS?
      IF (ABS(LHI1).LT.3) THEN
         MERPRLL = .TRUE.
      ENDIF

C     FOR REST OF PROGRAM, LHI ONLY CONTROLS HIGH/LOW LABELLING yes/no
      IF (LHI1.GT.0) THEN
         LHI1 = 0
      ENDIF

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C     * SET QUALITY OPTIONS
      IF(MS1.LT.0) THEN
         PUB=.TRUE.
         DOTITLE=.TRUE.
         DOINFOLABEL=.FALSE.
         DOLGND=.TRUE.
         DOLABEL=.TRUE.
         MERPRLL=.TRUE.
         LHI1=-1
CCCC         MS1=ABS(MS1+1)
         IF(ABS(MS1).EQ.6) THEN
            SMSHAD1=.TRUE.
            DOLGND=.FALSE.
         ENDIF
      ELSE
         PUB=.FALSE.
         DOTITLE=.TRUE.
         DOINFOLABEL=.TRUE.
         DOLGND=.TRUE.

         IF(MS1.EQ.1) THEN
            DOTITLE=.FALSE.
         ELSE IF(MS1.EQ.2) THEN
            DOINFOLABEL=.FALSE.
         ELSE IF(MS1.EQ.3) THEN
            DOTITLE=.FALSE.
            DOINFOLABEL=.FALSE.
         ELSE IF(MS1.EQ.4) THEN
            DOTITLE=.FALSE.
            DOINFOLABEL=.FALSE.
            DOLGND=.FALSE.
         ELSE IF(MS1.EQ.5) THEN
            DOTITLE=.FALSE.
            DOINFOLABEL=.FALSE.
            DOLGND=.FALSE.
         ELSE IF(ABS(MS1).EQ.6) THEN
            SMSHAD1=.TRUE.
            DOLGND=.FALSE.
         ELSE IF(MS1.EQ.7) THEN
            POSDASHPAT1=DASHPATSOL
            NEGDASHPAT1=DASHPATSOL
            POSDASHPAT2=DASHPATSOL
            NEGDASHPAT2=DASHPATSOL
         ENDIF
      ENDIF
C
C       * ASSUME SHADING IS EITHER COLOUR SHADING OR LINE PATTERN
C       * SHADING.  COLOUR SHADING MUST BE DRAWN BEFORE THE CONTOUR
C       * LINES ARE DRAWN, OTHERWISE, THE SHADING OVERWRITES THE
C       * CONTOUR LINES AND YOU CAN'T SEE THEM.  THIS CHECKS THE
C       * PATTERN ARRAY TO SEE IF THERE IS A COLOUR PATTERN.  IF
C       * THERE IS, THEN THIS PLOT MUST USE COLOUR SHADING.
C
C     * IF COLOUR SHADING, THEN LOAD REQUIRED COLOURS
      IF (SHAD1) THEN
         IF(DOLGND) LGND=.TRUE.
C
         NPAT=NPAT1
         DO J=1,NPAT1
            IPAT(J)=IPAT1(J)
            ZLEV(J)=ZLEV1(J)
         ENDDO
C     Changed definition of colour plot to mean CLRPLT is true if
C     there is at least one level of colour shading.
         DO 95 I = 1, NPAT
            IF ((IPAT1(I) .GE. 100 .AND. IPAT1(I) .LE. 199).OR.
     1          (IPAT1(I) .GE. 350 .AND. IPAT1(I) .LE. 420)) THEN
               CLRPLT = .TRUE.
            ELSE
               PATPLT = .TRUE.
            ENDIF
 95      CONTINUE

         IF(CLRPLT) THEN
            IF(NPAT.GT.0) THEN
               NCLRS=-NPAT
            ELSE
               NCLRS=0
            ENDIF
            IF(SMSHAD1) THEN
               CALL DFCLRRAMP(MAXCLRS,NPAT,IPAT)
            ELSE
               CALL DFCLRS(NCLRS,HSVV,IPAT)
            ENDIF
         ENDIF
      ENDIF

      IF (SHAD2) THEN
         IF(DOLGND) LGND=.TRUE.
         DO 96 I = 1, NPAT2
            IF ((IPAT2(I) .GE. 100 .AND. IPAT2(I) .LE. 199).OR.
     1          (IPAT1(I) .GE. 350 .AND. IPAT1(I) .LE. 420)) THEN
               CLRPLT2 = .TRUE.
            ELSE
               PATPLT2 = .TRUE.
            ENDIF
 96      CONTINUE
         IF(CLRPLT2) THEN
            IF(NPAT2.GT.0) THEN
               NCLRS=-NPAT2
            ELSE
               NCLRS=0
            ENDIF
            IF(SMSHAD2) THEN
               CALL DFCLRRAMP(MAXCLRS,NCLRS,IPAT2)
            ELSE
               CALL DFCLRS(NCLRS,HSVV,IPAT2)
            ENDIF
         ENDIF
      ENDIF
      IF (PUB) THEN
CC      Double line width
        CALL GSLWSC(RLWBGCP)
      ELSE
        CALL GSLWSC(RLWBGC)
      ENDIF
C--------------------------------------------------------------
C
C     * READ FIELD FROM FILE Z IF AVAILABLE, ELSE FROM FILE U
C
      IF (MODEGG.EQ.1) THEN
         CALL GETFLD2(LUNFIELD2,GG2,-1,NSTEP1,  -1,LEVEL1,IBUF,MAXX,OK)
         IF (.NOT.OK) THEN
           WRITE(6,6015) NSTEP1,  -1,LEVEL1
           CALL                                    PXIT('GGPLOT',-102)
         ENDIF
         NLON=IBUF(5)
         NLAT=IBUF(6)
         IF((NLON.GT.IM).OR.(NLAT.GT.JM)) THEN
           WRITE(6,*) 'INPUT GRID LARGER THAN: ',IM, ' X ',JM
           CALL                                    PXIT('GGPLOT',-103)
         ENDIF
         NSTEP = NSTEP1
         LEVEL = LEVEL1
      ELSE
         CALL GETFLD2(LUNFIELD1,GG2,-1,NSTEP1,NAME1,LEVEL1,IBUF,MAXX,OK)
         IF (.NOT.OK) THEN
            WRITE(6,6015) NSTEP1,NAME1,LEVEL1
            CALL                                   PXIT('GGPLOT',-104)
         ENDIF
         NLON=IBUF(5)
         NLAT=IBUF(6)
         IF((NLON.GT.IM).OR.(NLAT.GT.JM)) THEN
           WRITE(6,*) 'INPUT GRID LARGER THAN: ',IM, ' X ',JM
           CALL                                    PXIT('GGPLOT',-105)
         ENDIF
         NSTEP = NSTEP1
         LEVEL = LEVEL1
      ENDIF
C
C     * ILLEGAL KIND ENCOUNTERED, WARNING EXIT.
C
         IF (IBUF(1).NE.NC4TO8('GRID').AND.
     1       IBUF(1).NE.NC4TO8('SUBA'))THEN
           WRITE(6,6050) IBUF(1)
           CALL                                    PXIT('GGPLOT',-106)
         ENDIF

C
C     * MOVE THE FIELD TO THE U ARRAY
C
         LX = IBUF(5)
         LY = IBUF(6)
         LXY = LX*LY
         CALL SCOPY(LXY,GG2,1,U,1)
         XX1=0.01
         DXX=0.98
         YY1=0.01
         DYY=0.98
         SIDE=0.98

         IF (IBUF(1).EQ.NC4TO8('SUBA').OR.(IABS(JPS).EQ.1
     1                                .OR. IABS(JPS).EQ.11)) THEN
            IF (NPLOT.EQ.0) THEN
               OPEN(45, FILE='pltinfo',IOSTAT=IOERR,
     1              FORM='UNFORMATTED')
               IF(IOERR.NE.0) CALL                 PXIT('GGPLOT',-107)
            ENDIF
            REWIND 45
            CALL READPLTINFO(45,RLATMN(1),RLONMN(1),
     1           RLATMX(1),RLONMX(1),DGRW,NHEM,NOUTYP,IOST)
            WRITE(*,6065) RLATMN(1),RLONMN(1),
     1           RLATMX(1),RLONMX(1),DGRW,NHEM,NOUTYP,IOST
            IF (NHEM.NE.IBUF(7)) WRITE(6,6060)
            SUBAREA = .TRUE.

            IF (NOUTYP.EQ.0) THEN
               PROJ = 'CE'
            ELSE
               PROJ = 'ST'
            ENDIF
C
C          * NEW VERIFY ITS EFFECT
C
            FLIP = .FALSE.
C
C          *
C
            IF(PROJ.NE.'ST') THEN
               FLIP = .FALSE.
               IF(RLONMX(1).LT.RLONMN(1))RLONMX(1)=RLONMX(1)+360.0
               DGRW=(RLONMN(1)+RLONMX(1))/2.0
C               IF((RLONMN(1).LT.0).AND.(RLONMX(1).GT.0)) THEN
C                  DGRW=0
C                  RLONMN(1)=RLONMN(1)+360.0
C                  RLONMX(1)=RLONMX(1)+360.0
C               ELSE IF((RLONMN(1).GT.0).AND.(RLONMX(1).LT.0)) THEN
C                  DGRW=180
C                  RLONMX(1)=RLONMX(1)+360.0
C               ELSE
C                  DGRW=180
C               ENDIF
            ENDIF
         ELSE
            IF (PROJ.EQ.'ST') THEN
               NHEM = IBUF(7)
               DGRW = -10.
               IF (NHEM.NE.2) THEN
                  RLATMN(1) = -9.4
                  RLATMX(1) = -9.4
                  RLONMN(1) = -122.8
                  RLONMX(1) = 57.2
               ELSE
                  RLATMN(1) = 9.4
                  RLATMX(1) = 9.4
                  RLONMN(1) = 122.8
                  RLONMX(1) = -57.2
               ENDIF
            ELSE
               IF (TICK) THEN
                 ASPECT = FLOAT(LY)/FLOAT(LX)
                 IF (ASPECT.GE..95.OR.SQUARE) SIDE=.8
C                 DXX=SIDE
C                 DYY=SIDE
                 IF (IGGLL.EQ.0) THEN
                   LYH=LY/2
                   CALL GAUSSG(LYH,SL,WL,CL,RAD,WOSSL)
                   CALL TRIGL (LYH,SL,WL,CL,RAD,WOSSL)
                   RLATMN(1) = RAD(1) *180./3.14159
                   RLATMX(1) = RAD(LY)*180./3.14159
                   DY = (RLATMX(1)-RLATMN(1))/LY
                   DY = 1.E-10*ANINT(1.E10*DY)
                 ELSE
                   DY = 180./LY
                   DY = 1.E-10*ANINT(1.E10*DY)
                   RLATMN(1) = -90. + .5*DY
                   RLATMX(1) = RLATMN(1) + (LY-1.)*DY
                 ENDIF
                 IF (FLIP) THEN
                   DGRW = 0.
                   RLONMN(1) = -179.9
                   RLONMX(1) = 179.9
                 ELSE
                   DGRW = 180.
                   RLONMN(1) = .1
                   RLONMX(1) =-.1
                   PRINT*, "111"
                 ENDIF
               ELSE
                 IF(IGGLL.EQ.1.OR.IGGLL.EQ.2) THEN
                   RLATMN(1) = -90.
                   RLATMX(1) =  90.
                 ELSE
                   IF (IGGLL.EQ.0) THEN
                     LYH=LY/2
                     CALL GAUSSG(LYH,SL,WL,CL,RAD,WOSSL)
                     CALL TRIGL (LYH,SL,WL,CL,RAD,WOSSL)
                     RLATMN(1) = RAD(1) *180./3.14159
                     RLATMX(1) = RAD(LY)*180./3.14159
                     DY = (RLATMX(1)-RLATMN(1))/LY
                     DY = 1.E-10*ANINT(1.E10*DY)
                   ELSE
                     DY = 180./LY
                     DY = 1.E-10*ANINT(1.E10*DY)
                     RLATMN(1) = -90. + .5*DY
                     RLATMX(1) = RLATMN(1) + (LY-1.)*DY
                   ENDIF
                   DY = 180./LY
                   DY = 1.E-10*ANINT(1.E10*DY)
                   RLATMN(1) = -90. + .5*DY
                   RLATMX(1) = RLATMN(1) + (LY-1.)*DY
                   IF(IGGLL.NE.0) THEN
                     TDY=DYY/LY
                     TDY = 1.E-10*ANINT(1.E10*TDY)
                     YY1=YY1+0.5*TDY
                     DYY=DYY-TDY
                   ENDIF
                 ENDIF
                 IF(IGGLL.EQ.2.OR.IGGLL.EQ.4) THEN
                   DX = 360./LX
                   DX = 1.E-10*ANINT(1.E10*DX)
                   IF (FLIP) THEN
                     DGRW = 0.
                     RLONMN(1) = -180. + .5*DX
                     RLONMX(1) = RLONMN(1) + (LX-1.)*DX
                   ELSE
                     DGRW = 180.
                     RLONMN(1) = .5*DX
                     RLONMX(1) = RLONMN(1) + (LX-1.)*DX
                     PRINT*, "222"
                   ENDIF
                   TDX=DXX/LX
                   TDX = 1.E-10*ANINT(1.E10*TDX)
                   XX1=XX1+0.5*TDX
                   DXX=DXX-TDX
                 ELSE
                   IF (FLIP) THEN
                     DGRW = 0.
                     RLONMN(1) = -179.9
                     RLONMX(1) = 179.9
                   ELSE
                     PRINT*, "333"
                     DGRW = 180.
                     RLONMN(1) = 0
                     RLONMX(1) =360
                   ENDIF
                 ENDIF
               ENDIF
            ENDIF
         ENDIF

C
C---------------------------------------------------------------------
C     * PRODUCE PLOT(S) FROM FILE Z (AND X) ACCORDING TO CARDS 1 THU 8
      IF (MODEGG.NE.1) THEN
C
C        * CALCULATE SCALE FACTOR AUTOMATICALLY IF DESIRED
C
         CALL GGP_DATPRP(LX,LY,LXY,U,
     1        GG2,FLIP,FLO1,HI1,FINC1,SCAL1,SPVAL)

C         IF(SCALE1.GE.0.) SCALE1=SCAL1
C
C        * USING EZMAP TO SET PROJECTION.
C        * BFCRDF is called by GGP_MAPPREP.
C
         CALL GGP_MAPPREP(U, LX, LY,
     1        WHITFG1, PROJ, MAP, NSETC, NHEM, SUBAREA,
     2        SQUARE,IMAPC)
C
C
C       * ASSUME SHADING IS EITHER COLOUR SHADING OR LINE PATTERN
C       * SHADING.  COLOUR SHADING MUST BE DRAWN BEFORE THE CONTOUR
C       * LINES ARE DRAWN, OTHERWISE, THE SHADING OVERWRITES THE
C       * CONTOUR LINES AND YOU CAN'T SEE THEM.  THIS CHECKS THE
C       * THE FIRST PATTERN TO SEE IF IT IS A COLOUR PATTERN.  IF
C       * IT IS, THEN THIS PLOT MUST USE COLOUR SHADING.
C
C       * COLOUR SHADING
C       * CONTDF, GGP_CPCPRP, and PATTERN
C         (through ARSCAM) are called by GGP_COLSHD.
C
C
         IF (SHAD1) THEN
            IF (BOXSHD1) THEN
               CALL CONTDF(PUB)
               CALL NEW_HAFTNP(U,LX,LX,LY,
     1              1,SPVAL,BOXDRW1)
            ELSE IF(SMSHAD1) THEN
               CALL GGP_COLSMSHD(U, LX, LY, -15,
     1              PROJ, RWRK, LRWK, IWRK, LIWK,
     2              FLO1, HI1, FINC1,
     3              NSETC, LHI1,
     4              PUB, CLRPLT, PATPLT, SPVAL, SUBAREA,
     5              MAP,
     6              RLATMN,RLATMX,RLONMN,RLONMX,BU)
            ELSE
               CALL GGP_COLSHD(U, LX, LY, -15,
     1              PROJ, RWRK, LRWK, IWRK, LIWK,
     2              FLO1, HI1, FINC1,
     3              NSETC, LHI1,
     4              PUB, CLRPLT, PATPLT, SPVAL, SUBAREA,
     5              MAP)
            ENDIF
            CALL GGP_ILDEF(U, LX, LY, PROJ, SCAL1, SUBAREA)
C
            IF (LGND) THEN
               IF (.NOT.SHAD2) THEN
                  CALL GGP_LEGEND(PROJ,CLRPLT,PATPLT,ILOG1)
               ELSE
                  CALL GGP_LEGEND(PROJ,CLRPLT2,PATPLT2,ILOG2)
               ENDIF
            ENDIF
         ELSE
           CALL CPSETI('CLS - CONTOUR LEVEL SELECTION FLAG', 0)
         ENDIF
         IX = LX
         IY = LY
C
C       * DRAWING CONTOURS OF THE FIELD WITH OR WITHOUT THE
C       * ZERO CONTOUR LINE
C       * GGP_CONLS, GGP_CONHLD, GGP_ILDEF
C       * and GGP_CPCPRP are called by GGP_COLCON.
C
         IF (CONT1) THEN
            NPATS=NPATS1
            IARBI=IARBI1
            DO I=1,NPATS
               ZLEVS(I)=ZLEVS1(I)
            ENDDO
C
           IF(ILOG1.EQ.1) ILOG=1
C           IF(IFINC.NE.0.) THEN
C              SCALE1=-SCAL1
C           ELSE
C              SCALE1=SCAL1
C           ENDIF
           CALL GGP_COLCON(U, LX, LY, -15,
     1           PROJ, RWRK, LRWK, IWRK, LIWK,
     2           FLO1, HI1, FINC1, SCAL1,
     3           NSETC, LHI1, ICOSH1,
     4           PUB, SECOND,
     5           NZERO1, SPVAL, SUBAREA,
     6           POSDASHPAT1,NEGDASHPAT1,
     7           MAP)

CDEBUG
CCCC           CALL CPGETI('DPU', IDPU)
CCCC           CALL CPSETI('DPU', -IDPU)
CCCC           CALL CPGETI('NCL - NUMBER OF CONTOUR LEVELS', NCL)
CCCC           DO I = 1, NCL
CCCC              CALL CPSETI('PAI', I)
CCCC              CALL CPGETI('DPU', IDPU)
CCCC              CALL CPGETC('CLD', CCLD)
CCCC              WRITE(*,*) 'end DPU ',IDPU,' and CLD for ',
CCCC     1             I,' is ',CCLD
CCCC           ENDDO
CDEBUG

         ENDIF
C
C    * Finished the first scalar field
C------------------------------------------------------------------
C      * READ SECOND SCALAR FIELD, THEN PRODUCE ITS PLOT
C
         IF (MODEGG.GT.2) THEN
           WRITE(6,'(A)') 'Second Scalar'

           IF(CONT1) THEN
              SECOND = .TRUE.
           ENDIF
C
C        * READ SECOND SCALAR FIELD
C
      CALL GETFLD2(LUNFIELD4,GG22,-1,NSTEP2,NAME2,LEVEL2,IBUF,MAXX,OK)
           IF (.NOT.OK) THEN
             WRITE(6,6015) NSTEP2,NAME2,LEVEL2
             CALL                                  PXIT('GGPLOT',-108)
           ENDIF
           NLON=IBUF(5)
           NLAT=IBUF(6)
           IF((NLON.GT.IM).OR.(NLAT.GT.JM)) THEN
              WRITE(6,*) 'INPUT GRID LARGER THAN: ',IM, ' X ',JM
              CALL                                 PXIT('GGPLOT',-109)
           ENDIF
           NSTEP = NSTEP2
           LEVEL = LEVEL2
C
C        * ILLEGAL KIND ENCOUNTERED, WARNING EXIT.
C
           IF (IBUF(1).NE.NC4TO8('GRID')
     1          .AND.IBUF(1).NE.NC4TO8('SUBA'))THEN
             WRITE(6,6050) IBUF(1)
             CALL                                  PXIT('GGPLOT',-110)
           ENDIF
C
C        * MOVE FIELD TO ARRAY U2
C
           LX2 = IBUF(5)
           LY2 = IBUF(6)
           IF (LX2.NE.IX.OR.LY2.NE.IY) CALL        PXIT('GGPLOT',-9)
           LXY2 = LX2*LY2

           CALL SCOPY(LXY2,GG22,1,U2,1)
C
C        * SET OPTIONS FOR PRODUCING PLOT
C
           IF (LHI2.GT.0) LHI2 = 0
           IF (PUB) LHI2 = -1
           IF(ABS(MS2).EQ.6) THEN
              SMSHAD1=.TRUE.
           ENDIF
           IF (MS2.LT.0) MS2 = ABS(MS2+1)
C
C        * CALCULATE SCALE FACTOR AUTOMATICALLY IF DESIRED
C
           CALL GGP_DATPRP(LX2,LY2,LXY2,U2,GG22,
     1          FLIP,FLO2,HI2,FINC2,SCAL2,SPVAL)
C           IF(SCAL2.GE.0.) SCALE2=SCAL2
C
C
            IF (SHAD2) THEN
               NPAT=NPAT2
               DO J=1,NPAT2
                  IPAT(J)=IPAT2(J)
                  ZLEV(J)=ZLEV2(J)
               ENDDO
               IF (BOXSHD2) THEN
                  CALL CONTDF(PUB)
                  CALL NEW_HAFTNP(U2,LX2,LX2,LY2,
     1                 1,SPVAL,BOXDRW2)
               ELSE IF(SMSHAD2) THEN
                  CALL GGP_COLSMSHD(U2, LX2, LY2, -15,
     1                 PROJ, RWRK, LRWK, IWRK, LIWK,
     2                 FLO2, HI2, FINC2,
     3                 NSETC, LHI2,
     4                 PUB, CLRPLT2, PATPLT2, SPVAL, SUBAREA,
     5                 MAP,
     6                 RLATMN,RLATMX,RLONMN,RLONMX,BU)
               ELSE
                  CALL GGP_COLSHD(U2, LX2, LY2, -15,
     1                 PROJ, RWRK, LRWK, IWRK, LIWK,
     2                 FLO2, HI2, FINC2,
     3                 NSETC, LHI2,
     4                 PUB, CLRPLT2, PATPLT2, SPVAL, SUBAREA,
     5                 MAP)
               ENDIF
            ENDIF
C
             CALL CPSETI('CLS - CONTOUR LEVEL SELECTION FLAG', 0)
C
C       * DRAWING CONTOURS OF THE FIELD WITH OR WITHOUT THE
C       * ZERO CONTOUR LINE
C       * GGP_CONLS, GGP_CONHLD, GGP_ILDEF
C       * and GGP_CPCPRP are called by GGP_COLCON.
C
           IF (CONT2) THEN
              ILOG=0
              NPATS=NPATS2
              IARBI=IARBI2
              DO I=1,NPATS
                 ZLEVS(I)=ZLEVS2(I)
              ENDDO
              IF(ILOG2.EQ.1) ILOG=1
             CALL GGP_COLCON(U2, LX2, LY2, -15,
     1              PROJ, RWRK, LRWK, IWRK, LIWK,
     2              FLO2, HI2, FINC2, SCAL2,
     3              NSETC, LHI2, ICOSH2,
     4              PUB, SECOND,
     5              NZERO2, SPVAL, SUBAREA,
     6              POSDASHPAT2,NEGDASHPAT2,
     7              MAP)
           ENDIF
C
C     * Finish the second scalar field
C
         ENDIF
C
C     Drawing continental outline, grid lines and labelling in Ezmap
C     if they are required.
C
         CALL GGP_PSTPLT(PUB,MERPRLL,MAP)
C         CALL GGP_CONTI(PUB,MERPRLL,MAP)
C
C    Finished drawing Z, X
C
      ENDIF
C-----------------------------------------------------------------------
      IF (MODEGG.NE.0.AND.MODEGG.NE.3) THEN

        IF (VSCAL.EQ.0.) VSCAL = 1.

        IF (MODEGG.NE.1) THEN
          CALL GETFLD2(LUNFIELD2,GG2,-1,NSTEP,-1,LEVEL,IBUF,MAXX,OK)
          IF(.NOT.OK) THEN
            WRITE(6,6015)NSTEP,IBUF(3),LEVEL
            CALL                                   PXIT('GGPLOT',-111)
          ENDIF
          NLON=IBUF(5)
          NLAT=IBUF(6)
          IF((NLON.GT.IM).OR.(NLAT.GT.JM)) THEN
             WRITE(6,*) 'INPUT GRID LARGER THAN: ',IM, ' X ',JM
             CALL                                  PXIT('GGPLOT',-112)
          ENDIF
C
C     * ILLEGAL KIND ENCOUNTERED, WARNING EXIT.
C
          IF (IBUF(1).NE.NC4TO8('GRID')
     1         .AND.IBUF(1).NE.NC4TO8('SUBA'))THEN
            WRITE(6,6050) IBUF(1)
            CALL                                   PXIT('GGPLOT',-113)
          ENDIF

          IF(IBUF(8).LE.0) WRITE(6,6090) IBUF(8)
C
          LX=IBUF(5)
          LY=IBUF(6)
          IF (LX.NE.IX) CALL                       PXIT('GGPLOT',-10)
          IF (LY.NE.IY) CALL                       PXIT('GGPLOT',-11)
          LXY=LX*LY
        ENDIF
C
        CALL VVSETR('LWD', 1.5)
C!      CALL GGP_U_DATPRP(LX,LY,LXY,U,GG2,FLIP,VLO,VI,
C!   1           VSCAL,SPVAL,INCX,INCY,LYR,LXR)
        CALL GGP_UV_DATPRP(GG2,U,LX,LY,FLIP,VSCAL,SPVAL,INCX,INCY)
C.......................................................................
C     * READ IN V FIELD.

        CALL GETFLD2(LUNFIELD3,GG2,-1,NSTEP,-1,LEVEL,JBUF,MAXX,OK)
        IF(.NOT.OK)THEN
          WRITE(6,6015)NSTEP,JBUF(3),LEVEL
          CALL                                     PXIT('GGPLOT',-114)
        ENDIF
        NLON=IBUF(5)
        NLAT=IBUF(6)
        IF((NLON.GT.IM).OR.(NLAT.GT.JM)) THEN
          WRITE(6,*) 'INPUT GRID LARGER THAN: ',IM, ' X ',JM
          CALL                                     PXIT('GGPLOT',-115)
        ENDIF
C
C     * ILLEGAL KIND ENCOUNTERED, WARNING EXIT.
C
        IF (JBUF(1).NE.NC4TO8('GRID')
     1       .AND.JBUF(1).NE.NC4TO8('SUBA'))THEN
          WRITE(6,6050) JBUF(1)
          CALL                                     PXIT('GGPLOT',-116)
        ENDIF
        IF(JBUF(5).NE.LX.OR.JBUF(6).NE.LY)CALL     PXIT('GGPLOT',-12)

        IF(JBUF(8).LE.0) WRITE(6,6090) JBUF(8)

        CALL VVSETR('LWD', 1.5)
C!      CALL GGP_V_DATPRP(LX,LY,LXY,V,GG2,FLIP,VLO,VI,
C!   1       VSCAL,SPVAL,INCX,INCY,LYR,LXR)
        CALL GGP_UV_DATPRP(GG2,V,LX,LY,FLIP,VSCAL,SPVAL,INCX,INCY)

C     * If only a vector field is plotted, use Ezmap to set projection.
C     * Use Ezmap to draw continental outline and grid lines.
C
        IF(MODEGG.EQ.1) THEN
C         CALL GGP_PSTPLT(PUB,MERPRLL,MAP)
C         CALL GGP_CONTI(PUB,MERPRLL,MAP)
         CALL GGP_MAPPREP(U, LX, LY,
     1        WHITFG1, PROJ, MAP, NSETV, NHEM, SUBAREA,
     2        SQUARE,IMAPC)
        ENDIF

C       Find the maximum vector length (VMAX)

        VMAX = 0.0
C       DO 495 I=1,LXR*LY
        DO 495 I=1,LX*LY
          IF(U(I).NE.SPVAL .AND. V(I).NE.SPVAL) THEN
            VLEN = SQRT(U(I)*U(I) + V(I)*V(I))
            IF (VLEN .GT. VMAX) VMAX = VLEN
          ENDIF
  495   CONTINUE

C       Calculate the multiplying factor

        IFAC = INT(VMAX/VI + 1.0)

C       Calculate the maximum vector magnitude to plot

        VMHI = IFAC * VI

C       Calculate the grid length with respect to the plotting area

        CALL GETUSV('XF',ISX)
        CALL GETSET(XA,XB,YA,YB,XC,XD,YC,YD,LTYPE)
C!      IGRID = INT(((XB - XA) * 2**ISX + 1) / (LXR - 1))
C       IGRID = INT(((XB - XA) * 2**ISX + 1) / (LX/INCX - 1))
        LXR=(LX-1)/INCX+1
        IGRID = INT(((XB - XA) * 2**ISX + 1) / (LXR - 1))

C       Calculate the maximum vector length to plot


        MLEN = IFAC * IGRID

        CALL VVSETI('CPM',-1)
CCCC        CALL VVSETI('SET -- Set Call Flag', 0)
CCCC        CALL GETSET(ZA,ZB,ZC,ZD,ZE,ZF,ZG,ZH,LZZZ)
CCCC        CALL VVSETI('MAP - MAPPING FLAG', 1)
CCCC        CALL VVSETR('XC1', -180.)
CCCC        CALL VVSETR('XCM', 180.)
CCCC        CALL VVSETR('YC1', -90.)
CCCC        CALL VVSETR('YCN', 90.)

C!      CALL VELVCT(U,LXR,V,LXR, LXR,LY,
        CALL VELVCT(U,LX,V,LX, LX,LY, 
     1        VLO,VMHI,NSETV,MLEN,3,SPV)
CCCC        IDM=0
CCCC        RDM=0.0
CCCC        CALL VVINIT(U,LXR,V,LXR,RDM,IDM,LXR,LY,RDM,IDM)
CCCC        CALL VVECTR(U,V,RDM,IDM,IDM,RDM)

C
C     Finished the vector field
C
         CALL GGP_PSTPLT(PUB,MERPRLL,MAP)
C
      ENDIF

C Plot the title.
      IF(DOTITLE) THEN
         IF (PROJ .EQ. 'ST') THEN
            IF (MODEGG .EQ. 0) CALL GGP_TITLE(LABEL1,2,PROJ)
            IF (MODEGG .EQ. 1) CALL GGP_TITLE(LABELV,2,PROJ)
            IF (MODEGG .EQ. 2) THEN
               CALL GGP_TITLE(LABEL1,2,PROJ)
               CALL GGP_TITLE(LABELV,3,PROJ)
            ENDIF
            IF (MODEGG .EQ. 3) THEN
               CALL GGP_TITLE(LABEL1,2,PROJ)
               CALL GGP_TITLE(LABEL2,3,PROJ)
            ENDIF
         ELSE
            IF (MODEGG .EQ. 0) CALL GGP_TITLE(LABEL1,3,PROJ)
            IF (MODEGG .EQ. 1) CALL GGP_TITLE(LABELV,3,PROJ)
            IF (MODEGG .EQ. 2) THEN
               CALL GGP_TITLE(LABEL1,2,PROJ)
               CALL GGP_TITLE(LABELV,3,PROJ)
            ENDIF
            IF (MODEGG .EQ. 3) THEN
               CALL GGP_TITLE(LABEL1,2,PROJ)
               CALL GGP_TITLE(LABEL2,3,PROJ)
            ENDIF
         ENDIF
         IF (MODEGG .EQ. 4) THEN
            CALL GGP_TITLE(LABEL1,1,PROJ)
            CALL GGP_TITLE(LABEL2,2,PROJ)
            CALL GGP_TITLE(LABELV,3,PROJ)
         ENDIF
      ENDIF

C-----------------------------------------------------------------------

C
C     * WRITE LABELS ABOVE PLOT AND TO OUTPUT FILE
C
      CALL GETSET(XA,XB,YA,YB,XC,XD,YC,YD,LTYPE)
      CALL SET(0.0,1.0,0.0,1.0,0.0,1.0,0.0,1.0,LTYPE)
CCCC      CALL PCHIQU(0.97, 0.97, 'X', 8.5, 0.0, 0.0)

C     * PRINT THE LABELS ABOVE THE GRAPH.  THE LABELS WILL APPEAR AS
C     * FOLLOWS:
C     *                   LABEL FOR VECTOR FIELD
C

C-----------------------------------------------------------------------
      WRITE(6,6030)IBUF
      IF(MODEGG.NE.0.AND.MODEGG.NE.3) WRITE(6,6030) JBUF
      IF(MODEGG.NE.1)                 WRITE(6,6040) (LABEL1(I),I=1,80)
      IF(MODEGG.GT.2)                 WRITE(6,6040) (LABEL2(I),I=1,80)
      IF(MODEGG.NE.0.AND.MODEGG.NE.3) WRITE(6,6040) (LABELV(I),I=1,80)

      CALL FRAME
      NPLOT=NPLOT+1
      GO TO 110
C
C-------------------------------------------------
C
  905 IF (NPLOT.EQ.0) CALL                         PXIT('GGPLOT',-13)
      CALL                                         PXIT('GGPLOT',0)
  910 CALL                                         PXIT('GGPLOT',-14)
  915 CALL                                         PXIT('GGPLOT',-15)
  920 CALL                                         PXIT('GGPLOT',-16)
  925 CALL                                         PXIT('GGPLOT',-17)
  930 CALL                                         PXIT('GGPLOT',-18)
  935 CALL                                         PXIT('GGPLOT',-19)
  940 CALL                                         PXIT('GGPLOT',-20)
  945 CALL                                         PXIT('GGPLOT',-21)
  950 CALL                                         PXIT('GGPLOT',-22)
  955 CALL                                         PXIT('GGPLOT',-23)
  960 CALL                                         PXIT('GGPLOT',-24)
C
C     * EOF ON FILE Z AFTER SKIP
C
  990 CALL                                         PXIT('GGPLOT',-117)
C------------------------------------------------------------------
C
 5010 FORMAT(10X,I10,I1,A4,I5,2I1,I3,4E10.0,I1,2I2)
 5012 FORMAT(10X,1E10.0)
 5015 FORMAT(80A1)
 5020 FORMAT(10X,I1,I4,7I5)
 5025 FORMAT(10X,7E10.0)
 5080 FORMAT(15X,7I5)
 5082 FORMAT(15X,7E5.0)
 5090 FORMAT(10X,7E10.0)
 5030 FORMAT(10X,3E10.0,2I5)
 6000 FORMAT(' CONREC MASK VALUE = ',E16.5)
 6003 FORMAT(' LEVELS MUST INCREASE.  ZLEV(',I3,
     1     ') is less than ZLEV(',I3,')')
 6005 FORMAT(' WARNING : SCREEN FILLING OPTION NOT COMPATIBLE WITH ',
     1  /,'           MODEGG,ICOSH1 CHOICES. CONTINUING WITH JPS = 8')
 6010 FORMAT('     BEGIN PLOT NUMBER',I5)
 6015 FORMAT('0..EOF LOOKING FOR',I10,2X,A4,I6)
 6020 FORMAT('0 FLO= ',G12.4,', HI= ',G12.4,', FINC= ',G12.4,
     1                                      ', SCAL= ',G12.4)
 6030 FORMAT('IBUF= ',9X,'    STEP  NAME LEVEL  LX  LY  KHEM NPACK',
     1       /3X, A4,I10,2X,A4,I6,2I4,2I6)
 6040 FORMAT('0',80A1)
 6050 FORMAT('  GGPLOT WARNING EXIT CAUSED BY ILLEGAL KIND = ',A4)
 6060 FORMAT(/,
     1       ' ** WARNING - UNMATCHED "NHEM" VALUE IS FOUND IN ',
     2       ' SUBAREA PROGRAM GENERATED FILES!'/
     3   13X,' MAKE SURE YOU ARE PLOTTING "SUBAREAED" FIELDS HAVING',
     4       ' THE SAME SUB-AREA COORDINATES AS'/
     5   13X,' THE ONES USED IN THE LAST CALL TO THE SUBAREA PROGRAM.'
     6   /)
 6062 FORMAT(1X,L10)
 6065 FORMAT(1X,'SUBAREA: ',5F10.3,3I3)
 6070 FORMAT('  GGPLOT WARNING EXIT: PLOTTING VECTOR FIELD ON A ',
     1       'SHIFTED GRID IS NOT SUPPORTED')
 6090 FORMAT(/,' *** WARNING: PACKING DENSITY = ', I4,' ***',/)
      END
