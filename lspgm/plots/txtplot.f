      PROGRAM TXTPLOT
C     PROGRAM TXTPLOT (INPUT,       OUTPUT,                             )       A2
C    1           TAPE5=INPUT, TAPE6=OUTPUT,
C    2                        TAPE8       )
C     -------------------------------------                                     A2
C
C     MAR 11/04 - M.BERKLEY (MODIFIED for NCAR V3.2)                            A2
C     DEC 31/97 - R. TAYLOR (CLEAN UP GGPLOT UPGRADE, FIX BUGS, TEST, TEST, TEST)
C     AUG 31/96 - T. GUI (UPDATE TO NCAR GRAPHICS V3.2)
C     SEP 21/93 - M. BERKLEY (changed Holleriths to strings)                    A2
C     SEP  1/92 - T. JANG   (add format to write to unit 44)                    A2
C     AUG 17/92 - T. JANG (CALLED SUBROUTINE LWRTUPR TO CHANGE LOWER CASE
C                          CHARACTERS TO UPPER CASE FOR INPUT TO PCHIQU.
C                          ALSO CHANGED UNDERSCORE '_' TO A BLANK ' '.
C     NOV 29/90 - S. WILSON (MODIFY PCHIQU TO USE DUPLEX FONT)
C     MAR 06/90 - C. BERGSTEIN (UPDATE TO INCLUDE WRITE TO FT44)
C     JAN 22/90 - C. BERGSTEIN (TEST SYSTEM LINKING)
C     JUN 14/89 - H. REYNOLDS (UPDATE TO NCAR GRAPHICS V2.0)
C     DEC 19/84 - R.LAPRISE.
C                                                                               A2
CTXTPLOT - PLOT A TEXT RECORD ON A FRAME                                0  0 C  A1
C                                                                               A3
CAUTHOR  - R.LAPRISE                                                            A3
C                                                                               A3
CPURPOSE - COPY A SECTION OF INPUT ONTO A PLOT FRAME.                           A3
C
CINPUT PARAMETER...
C                                                                               A5
C      LINE = 80 CHARACTERS LINE OF TEXT                                        A5
C                                                                               A5
CEXAMPLE OF INPUT CARD(S)...                                                    A5
C                                                                               A5
C* PLEASE SEND THESE PLOTS TO...                                                A5
C*                                                                              A5
C*                    R.LAPRISE                                                 A5
C*                    CCRN                                                      A5
C*                    4905 DUFFERIN ST                                          A5
C*                    DOWNSVIEW                                                 A5
C*                    CCRN - BOX 1006                                           A5
C*                                                                              A5
C* END-END-END-END-END-END-END-END-END-END-END-END-END-END-END-END              A5
C----------------------------------------------------------------------------
C
C     NOTE THAT ONLY ABOUT 60 CHARACTERS WILL FIT ON A LINE.
C
C     INTEGER LINE(10)
      LOGICAL LEAD,TRAIL,EX
      CHARACTER*1 LINE(83)
      CHARACTER*83 ALINE
      EQUIVALENCE (LINE,ALINE)

      INTEGER LUNCARD,LUNSCRIPT,UNIQNUM
     +     LUNDATAFILES(1)

C     * INCLUDE THE COMMON BLOCK USED BY PCHIQU TO CHANGE THE FONT
      COMMON / PUSER / MODE

      DATA OR/0./, XSTART/0./, YSTART/0.9/, YINC/0.05/, YMIN/.05/
      DATA SIZ/13.5/, LINE/83*' '/
C-----------------------------------------------------------------------
      NFF=2
      CALL JCLPNT(NFF,5,6)
      CALL PSTART
      CALL BFCRDF(0)

C     * TELL PCHIQU TO USE THE DUPLEX FONT
      CALL PCSETI('FN',12)

      CALL GSLWSC(2.0)
      CALL SET(0.01,0.99,0.01,0.99, 0.,1.,0.,1., 1)
C
      I=0
      Y=YSTART
C
C     * READ AND PLOT LINES OF TEXT.
C
  100 READ(5,5010,END=700) (LINE(K),K=1,80)                                     A4


      CALL LWRTUPR(LINE,80)
      DO 200 II=1,80
       IF (LINE(II) .EQ. '_') LINE(II) = ' '
  200 CONTINUE
      I=I+1
      WRITE(6,6010) I,(LINE(K),K=1,80)
      IF(Y.LE.YMIN) THEN
CCCC         CALL PCHIQU(.97,.97,'X',10,IOR,-1)
         CALL FRAME
         Y=YSTART
      ENDIF
C
C  WANT TO STRIP TRAILING BLANKS, BUT NOT LEADING BLANKS, FROM INPUT CARD.
C  FONT SIZE CONTROL CHARACTER 'K' IS USELESS AND IS IGNORED.
C
       LEAD = .FALSE.
      TRAIL= .TRUE.
      CALL SHORTN(LINE,83,LEAD,TRAIL,NC,IAPFLG)
      CALL PCHIQU(XSTART,Y,ALINE(4:NC),SIZ,OR,-1.)
      Y=Y-YINC
      GO TO 100                                                                 A4
C
C     * INPUT SECTION IS FINISHED.
C
  700 CONTINUE
CCCC      CALL PCHIQU(.97,.97,'X',10,IOR,0)
      CALL FRAME
      CALL                                         PXIT('TXTPLOT',0)
C
C     * E.O.F. ON INPUT.
C
  901 CALL                                         PXIT('TXTPLOT',-1)
C-----------------------------------------------------------------------
 5010 FORMAT(80A1)                                                              A4
 6010 FORMAT(1X,I5,5X,80A1)
      END
