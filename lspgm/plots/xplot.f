      PROGRAM XPLOT
C     PROGRAM XPLOT (XIN,       INPUT,       OUTPUT,                    )       A2
C    1         TAPE1=XIN, TAPE5=INPUT, TAPE6=OUTPUT,
C    2                                 TAPE8       )
C     ----------------------------------------------                            A2
C                                                                               A2
C     NOV 20/07 - F.MAJAESS (MAKE "INTPR" COMMON BLOCK DECLARATION CONSISTENT   A2
C                            WITH THAT IN THE OTHER SOURCE CODE)                A2
C     MAR 11/04 - M.BERKLEY (MODIFIED for NCAR V3.2)                            
C     DEC 31/97 - R. TAYLOR (CLEAN UP GGPLOT UPGRADE, FIX BUGS,TEST,TEST,TEST)
C     AUG 31/96 - T. GUI (UPDATE TO NCAR GRAPHICS V3.2)
C     MAR 11/96 - F.MAJAESS (ISSUE WARNING WHEN PACKING DENSITY <= 0)           
C     NOV 19/93 - F.MAJAESS (USE DEFAULT TENSION, POSSIBLY INTERPOLATE)         
C     SEP 21/93 - M. BERKLEY (changed Holleriths to strings)                    
C     SEP 22/93 - M. BERKLEY (changed to save data files instead of            
C                             generating meta code.)                          
C     SEP  1/92 - T. JANG   (add format to write to unit 44)                 
C     JUL 20/92 - T. JANG  (changed variable "MAX" to "MAXX" so not to
C                           conflict with the generic function "MAX")
C     JUL  4/91 - S. WILSON (ADD LABEL FOR SCIENTIFIC NOTATION AND CHANGE
C                            X-AXIS LABELS FOR ZONAL PLOTS)
C     OCT  9/90 - S. WILSON (CONVERT TO AUTOGRAPH PLOTTING ROUTINES)
C     APR  1/90 - C. BERGSTEIN (INCLUDE FT44 (AAASOVL) TO CONTROL OVERLAYS )
C     SEP 21/89 - F.MAJAESS (MODIFIED TO HANDLE BOTH ZONAL AND MERIDIONAL FLDS)
C     JUN 16/89 - H. REYNOLDS (UPDATE TO NCAR V2.O)
C     NOV 06/85 - F.ZWIERS (CHANGE LABELS)
C     JAN 16/85 - B.DUGAS, R.LAPRISE, N.E.SARGENT. (CHANGE FMISS, ISIZ)
C                                                                               A2
CXPLOT   - PLOTS SINGLE LEVEL MERIDIONAL CROSS SECTION CURVE            1  1 C GA1
C                                                                               A3
CAUTHOR  - N.E.SARGENT                                                          A3
C                                                                               A3
CPURPOSE - PLOTS GLOBAL ZONALLY AVERAGED SINGLE LEVEL FIELDS                    A3
C          VERSUS LATITUDE.                                                     A3
C                                                                               A3
CINPUT FILE...                                                                  A3
C                                                                               A3
C      XIN = FILE CONTAINING CURVES TO BE PLOTTED.                              A3
C
CINPUT PARAMETERS...
C                                                                               A5
C      CARD1                                                                    A5
C      -----                                                                    A5
C      READ(5,5010,END=900) NSTEP,NAME,LEVEL,YLO,YHI,MS,FMISS,NZMS              A5
C 5010 FORMAT(10X,I10,1X,A4,I5,2E10.0,I5,E10.0,I5)                              A5
C                                                                               A5
C      NSTEP   = TIMESTEP NUMBER                                                A5
C      NAME    = NAME OF VARIABLE TO BE PLOTTED                                 A5
C                THE NAMES NC4TO8('NEXT') AND NC4TO8('SKIP') ARE RECOGNIZED     A5
C                BUT FILE XIN IS REWOUND BEFORE EACH SEARCH. THIS ALLOWS        A5
C                FIELDS TO BE IN ANY ORDER ON THE FILE, BUT NC4TO8('NEXT')      A5
C                ALWAYS SELECTS THE FIRST FIELD AND NC4TO8('SKIP') IS A NULL    A5
C                OPERATION.                                                     A5
C      LEVEL   = LEVEL NUMBER                                                   A5
C      YLO,YHI = LOWER,UPPER LIMITS OF VERTICAL AXIS.                           A5
C                IF YLO = YHI, THEN THE RANGE WILL AUTOMATICALLY BE             A5
C                DETERMINED BY EXAMINING THE DATA.  THE NUMBER OF MAJOR         A5
C                TICK MARKS WILL VARY, DEPENDING ON THE RANGE OF THE            A5
C                PLOT.  BUT, THERE IS ALWAYS 1 MINOR TICK MARK BETWEEN          A5
C                THE MAJOR TICK MARKS.                                          A5
C      MS      = PRINTER PLOT PARAMETER.                                        A5
C                IF MS.NE.0 THEN A GRAPH IS PRODUCED ON THE LINE PRINTER.       A5
C      FMISS   = LOWER BOUND FOR VALUES TO BE PLOTTED.  IF VALUES ARE           A5
C                MISSING AT SOME LATITUDE,  CODE THEM AS LESS THAN FMISS,       A5
C                AND NO POINT WILL BE PLOTTED FOR THAT LATITUDE, (VALUES        A5
C                LESS THAN FMISS ARE NOT PLOTTED).                              A5
C                FMISS=0 OR BLANK DEFAULTS TO FMISS=-9.E36 .                    A5
C      NZMS    = 0/1 FOR ZONAL/MERIDIONAL PLOT.                                 A5
C                                                                               A5
C      CARD2                                                                    A5
C      -----                                                                    A5
C      READ(5,5012,END=902) (LABEL(I),I=1,80)                                   A5
C 5012 FORMAT(80A1)                                                             A5
C                                                                               A5
C      LABEL   = 80 CHARACTER LABEL PRINTED ABOVE PLOT.                         A5
C                                                                               A5
C EXAMPLE OF INPUT CARDS...                                                     A5
C                                                                               A5
C* XPLOT         182  SUB    1   -5.0E-5    5.0E-5    1   -1.0E-4    0          A5
C* JUL ZONALLY AVERAGED E-P (KG M-2 S-1), SHUTZ AND GATES.                      A5
C----------------------------------------------------------------------------
C
      use diag_sizes, only : SIZES_BLAT,
     &                       SIZES_BLONP1,
     &                       SIZES_MAXBLONP1BLAT,
     &                       SIZES_NWORDIO
      integer, parameter :: 
     & MAXX = max(SIZES_BLONP1,SIZES_BLAT)*SIZES_NWORDIO

      LOGICAL OK,LAT
      CHARACTER*83 ALABEL
      CHARACTER*1 LABEL(83)
      CHARACTER*8 PLTNAM
      INTEGER NCURV

      REAL YHI, YLO

C     * ARRAYS FOR GETTING INFO ABOUT THE NUMERIC LABELS AND SCALING OF THE
C     * Y AXIS.
      REAL NMLBDT(11), TCKDAT(3), FSCALE

C     * STRINGS FOR THE SCALING LABEL.
      CHARACTER STRING*24, TMP*8

C     * THESE VARIABLES ARE FOR DRAWING THE ZERO LINE.
      REAL UCOORD(4), FXLO, FXHI, FYLO, FYHI

      DIMENSION X(SIZES_MAXBLONP1BLAT)
      EQUIVALENCE (LABEL,ALABEL)
      EQUIVALENCE (UCOORD(1), FXLO), (UCOORD(2), FXHI)
      EQUIVALENCE (UCOORD(3), FYLO), (UCOORD(4), FYHI)

C     logical unit numbers for saving card images, data, etc.
      INTEGER LUNCARD,UNIQNUM,LUNDATAFILES(1)
      PARAMETER (LUNOFFSET=10,
     +     LUNFIELD1=LUNOFFSET+1)

C
C     * COMMON BLOCK TO TELL AGCHNL WHICH PROGRAM IS CALLING IT AND
C     * WHAT TYPE OF X-AXIS (ZONAL/MERIDIONAL) IS BEING DRAWN.
C
      COMMON / AGNMLB / PLTNAM, LAT

C
C     * DECLARE NCAR PLUGINS EXTERNAL, SO CORRECT VERSION ON SGI
C
      EXTERNAL AGCHCU
      EXTERNAL AGCHNL
      EXTERNAL AGPWRT
C
C     * DECLARE BLOCK DATAS EXTERNAL, SO INITIALIZATION HAPPENS
C
      EXTERNAL AGCHCU_BD
      EXTERNAL FILLCBBD
      EXTERNAL FILLPAT_BD


C
C     * COMMON BLOCK FOR SETTING LINE THICKNESSES.
C
      COMMON /AGLW/ LWBG, LWMJ, LWMN


      COMMON/BLANCK/ G(SIZES_MAXBLONP1BLAT),
     &               Y(SIZES_MAXBLONP1BLAT)

      COMMON /ICOM/ IBUF(8),IDAT(MAXX)

C
C     *NCAR COMMON WHICH CONTROLS SMOOTHING PARAMETERS INCLUDING TENSION
C
      COMMON /INTPR/
     1     IPAU,FPART,TENSN,NP,SMALL,L1,ADDLR,ADDTB,MLLINE,ICLOSE
      INTEGER IPAU,NP,L1,MLLINE,ICLOSE
      REAL FPART,TENSN,SMALL,ADDLR,ADDTB

C     * COMMON BLOCK FOR PCHIQU - NEEDED TO CHANGE FONTS
      COMMON /PUSER/ MODE


      DATA NCURV / 1 /

C
C
C     * SET TRESHOLD FOR THE MINIMUM NUMBER OF POINTS
C     * PASSED TO NCAR PLOTTING ROUTINES.
C
      MINPTS=144

C
C     * SET NCAR TENSN TO TURN CURVE SMOOTHING OFF
C
C     TENSN=9.0
C
C     * CHANGE DEFAULT MAX SEGMENT LENGTH
C
      MLLINE=43

C
C     * SET THE PLOT NAME FOR AGCHNL AND SET THE LINE THICKNESSES FOR
C     * AGCHCU.
C
      PLTNAM = 'XPLOT'
      LWBG = 2000
      LWMJ = 4000
      LWMN = 2000

      NFF=3
      CALL JCLPNT(NFF,LUNFIELD1,5,6)


C     * OPEN NCAR GRAPHICS PACKAGE
      CALL PSTART



C     * DOUBLE THE LINE WIDTH
      CALL SETUSV('LW', LWBG)

C     * TELL PCHIQU TO USE DUPLEX CHARACTER FONT
      CALL PCSETI('FN', 12)
      CALL PCSETI('OF', 1)
      CALL PCSETI('OL', 2)
      CALL PCSETR('CL',.2)
      CALL BFCRDF(0)

C     *  MAKE THE NUMERIC LABELS BIGGER
      CALL AGSETF('LEFT/WIDTH/MANTISSA.', 0.020)
      CALL AGSETF('LEFT/WIDTH/EXPONENT.', 0.013333)
      CALL AGSETF('BOTTOM/WIDTH/MANTISSA.', 0.020)
      CALL AGSETF('BOTTOM/WIDTH/EXPONENT.', 0.013333)

C     * MAKE THE TOP LABEL BIGGER.
      CALL AGSETC('LABEL/NAME.', 'T')
      CALL AGSETF('LINE/NUMBER.', 100.0)
      CALL AGSETF('LINE/CHARACTER.', 0.026666)

C     * MAKE THE BOTTOM LABEL BIGGER.
      CALL AGSETC('LABEL/NAME.', 'B')
      CALL AGSETF('LINE/NUMBER.', -100.0)
      CALL AGSETF('LINE/CHARACTER.', 0.020)

C     * SET UP THE SCALE LABEL IN THE TOP LEFT HAND CORNER FOR
C     * SCIENTIFIC NOTATION.
      CALL AGSETC('LABEL/NAME.', 'SCALE')
      CALL AGSETF('LABEL/BASEPOINT/X.', 0.0)
      CALL AGSETF('LABEL/BASEPOINT/Y.', 0.9)
      CALL AGSETF('LABEL/OFFSET/X.', -0.015)
      CALL AGSETF('LABEL/ANGLE.', 90.0)

C     * MAKE THE SCALE LABEL BIGGER.
      CALL AGSETF('LINE/NUMBER.', 100.0)
      CALL AGSETF('LINE/CHARACTER.', 0.020)

C
C     * SHUT OFF INFORMATION LABEL SCALING.  AUTOGRAPH WILL ONLY BE ABLE TO
C     * PREVENT INFORMATION LABELS FROM OVERLAPPING BY MOVING THEM.
C
      CALL AGSETF('LABEL/CONTROL.', 1.0)


C     * SUPPRESS THE FRAME ADVANCE SO THAT THE ZERO LINE CAN BE DRAWN
C     * IF NEEDED.
      CALL AGSETF('FRAME.', 2.)


      REWIND LUNFIELD1
      NPLOT=0

C     * READ THE CONTROL CARDS.
C     * SIMULATE A 'WHILE NOT(EOF)' STATEMENT
  150 READ(5,5010,END=900) NSTEP,NAME,LEVEL,YLO,YHI,MS,FMISS,NZMS               


         IF(FMISS.EQ.0.0) FMISS=-9.E36

C
C        * IF WE ARE DRAWING A ZONAL PLOT, SET LAT TO .TRUE.
C
         IF (NZMS .EQ. 0) THEN
              LAT = .TRUE.
         ELSE
              LAT = .FALSE.
         ENDIF


         READ(5,5012,END=902) (LABEL(I),I=1,80)                                 

         WRITE(6,6012) (LABEL(I),I=1,80)
         NPLOT=NPLOT+1

C        * GET THE FIELD.

         CALL GETFLD2(LUNFIELD1,G,NC4TO8('ZONL'),NSTEP,NAME,
     +    LEVEL,IBUF,MAXX,OK)

         IF(.NOT.OK) THEN
             WRITE(6,6010) NAME,NSTEP,LEVEL
             CALL                                  PXIT('XPLOT',-101)
         ENDIF
         IF(IBUF(8).LE.0) WRITE(6,6090) IBUF(8)

C        * COPY INTO ARRAY Y, (REVERSING ARRAY ELEMENTS FOR LATITUDES).


         NPTS=IBUF(5)
         DO 210 J=1,NPTS
            IF(NZMS.EQ.0) THEN
               Y(J) = G(NPTS-J+1)
            ELSE
               Y(J) = G(J)
            ENDIF
            IF (Y(J) .LE. FMISS) THEN
               X(J) = 1.E36
               Y(J) = 1.E36
            ELSEIF (NZMS .EQ. 0) THEN
               X(J) = 90.0 - (180.0 * (J - 1)) / (NPTS - 1)
            ELSE
               X(J) = (360.0 * (J - 1)) / (NPTS - 1)
            ENDIF
 210     CONTINUE


C
C        *  DOUBLE RESOLUTION BY INTERPOLATION IF THE NUMBER OF
C        *  POINTS TO PLOT IS LESS THAN MINPTS.
C
         NOPTS=NPTS
  250    IF ( NPTS .GE. MINPTS ) GO TO 300

  270    CONTINUE
         X(2*NPTS-1)=X(NPTS)
         Y(2*NPTS-1)=Y(NPTS)
         DO 280 J = NPTS-1, 1, -1
          IF ( Y(J).NE.1.E36 .AND. Y(J+1).NE.1.E36) THEN
            Y(2*J)   = 0.5*(Y(J)+Y(J+1))
          ELSE
            Y(2*J)   = 1.E36
          ENDIF
          Y(2*J-1) = Y(J)
          IF ( X(J).NE.1.E36 .AND. X(J+1).NE.1.E36) THEN
            X(2*J)   = 0.5*(X(J)+X(J+1))
          ELSE
            X(2*J)   = 1.E36
          ENDIF
          X(2*J-1) = X(J)
  280    CONTINUE
         NPTS=2*NPTS-1
         GO TO 250

C
C        *  CHANGE THE LINE-END CHARACTER TO '!'.
C
  300    CALL AGSETC('LINE/END.','!')

C
C        *  TURN ON WINDOWING AND DO NOT FORCE ENDPOINTS AT MAJOR
C        *  TICK MARKS IF A SPECIFIC RANGE HAS BEEN REQUESTED.
C        *  OTHERWISE, SET YHI AND YLO TO 'NULL 1' SO AUTOGRAPH WILL
C        *  CALCULATE THE ACTUAL MIN AND MAX VALUES FOR Y.
C
         IF (YHI .GT. YLO) THEN
              CALL AGSETI('WINDOWING.', 1)
              CALL AGSETF('Y/NICE.', 0.0)
         ELSE
              CALL AGSETI('WINDOWING.', 0)
              CALL AGSETF('Y/NICE.', -1.0)
              YHI = 1.E36
              YLO = 1.E36
         ENDIF
         CALL AGSETF('Y/MAXIMUM.', YHI)
         CALL AGSETF('Y/MINIMUM.', YLO)

C
C        *  SET THE X MIN AND MAX
C
         IF (NZMS .EQ. 0) THEN
              CALL AGSETI('X/ORDER.', 1)
              CALL AGSETF('X/MAXIMUM.', 90.0)
              CALL AGSETF('X/MINIMUM.', -90.0)
         ELSE
              CALL AGSETF('X/MAXIMUM.', 360.0)
              CALL AGSETF('X/MINIMUM.', 0.0)
         ENDIF

C
C        *  SET THE LEFT AND RIGHT TICKS
C
         CALL AGSETI('LEFT/MINOR/SPACING.', 1)
         CALL AGSETI('RIGHT/MINOR/SPACING.', 1)

C
C        *  SET THE TOP AND BOTTOM TICKS
C
         IF (NZMS .EQ. 0) THEN
              CALL AGSETF('TOP/MAJOR/BASE.', 30.0)
              CALL AGSETF('BOTTOM/MAJOR/BASE.', 30.0)
              CALL AGSETI('TOP/MINOR/SPACING.', 2)
              CALL AGSETI('BOTTOM/MINOR/SPACING.', 2)
         ELSE
              CALL AGSETF('TOP/MAJOR/BASE.', 30.0)
              CALL AGSETF('BOTTOM/MAJOR/BASE.', 30.0)
              CALL AGSETI('TOP/MINOR/SPACING.', 0)
              CALL AGSETI('BOTTOM/MINOR/SPACING.', 0)
         ENDIF
         CALL AGSETF('TOP/NUMERIC/TYPE.', 0.0)

C
C        *  SET UP THE BOTTOM AND LEFT LABELS (THE LEFT LABEL IS JUST
C        *  A SPACE) AND SET THE LINE PATTERNS IN THE ANOTAT' CALL.
C
         IF (NZMS .EQ. 0) THEN
              CALL ANOTAT('LATITUDE!', ' !',  0, 0, 0, 0)
         ELSE
              CALL ANOTAT('LONGITUDE!', ' !', 0, 0, 0, 0)

              CALL AGSETC('LABEL/NAME.','EAST')
              CALL AGSETF('LABEL/BASEPOINT/X.', .999999)
              CALL AGSETF('LABEL/BASEPOINT/Y.', 0.0)
              CALL AGSETF('LABEL/OFFSET/Y.', -0.015)
              CALL AGSETI('LINE/NUMBER.', -100)
              CALL AGSETF('LINE/CHARACTER.', 0.020)
              CALL AGSETC('LINE/TEXT.', 'E!')
         ENDIF

C
C        *  CHANGE THE MAXIMIMUM LINE LENGTH.
C
         CALL AGSETF('LINE/MAXIMUM.', 80.0)



C
C        * RUN AGSTUP TO MAKE AUTOGRAPH CALCULATE THE NUMERIC LABEL
C        * SETTINGS.  WE CAN CHECK THE 'SECONDARY' PARAMETERS TO MAKE
C        * SURE THE THE LABELS WILL NOT USE SCIENTIFIC NOTATION OR HAVE
C        * LABELS OF THE FORM 0.00001, 0.00002, 0.00003, ETC.  WHERE THERE
C        * ARE TOO MANY ZEROS.
C
         CALL AGSTUP(X,NCURV,NPTS,NPTS,1,Y,NCURV,NPTS,NPTS,1)

C
C        * GET THE NUMERIC LABEL INFORMATION ON THE Y AXIS.
C
         CALL AGGETP('SECONDARY/LEFT/NUMERIC.', NMLBDT, 11)
         CALL AGGETP('SECONDARY/LEFT/TICKS.', TCKDAT, 3)

C
C        * GET THE USER COORDINATES OF THE GRID WINDOW.
C
         CALL AGGETP('SECONDARY/USER.', UCOORD, 4)
         IF (FYLO * FYHI .NE. 0) THEN
              FSCALE = MAX(ANINT(LOG10(ABS(FYLO))),
     +                       ANINT(LOG10(ABS(FYHI))))
         ELSE
              FSCALE = ANINT(LOG10(MAX(ABS(FYLO), ABS(FYHI))))
         ENDIF


C
C        * IF (THE NUMERIC LABEL USES SCIENTIFIC NOTATION) OR (THE
C        * NUMERIC LABEL *DOESN'T* USE SCIENTIFIC NOTATION *AND*
C        * THE LARGEST ABSOLUTE VALUE IS LESS THAN 0.01) THEN
C        * SCALE THE DATA ACCORDINGLY AND SHOW THE SCALING LABEL
C        * ALONG THE Y AXIS.
C
         IF ((NMLBDT(1) .EQ. 2.0 .AND. TCKDAT(1) .EQ. 1.0) .OR.
     +       (NMLBDT(1) .EQ. 3.0 .AND. FSCALE .LT. -2.0)) THEN

C
C             * IF SCIENTIFIC NOTATION WAS USED, THE LOG OF THE
C             * SCALING FACTOR IS STORED IN NMLBDT(2) (WHICH IS ACTUALLY
C             * THE VALUE OF 'LEFT/EXPONENT.')
C
              IF (NMLBDT(1) .EQ. 2 .AND. TCKDAT(1) .EQ. 1) THEN
                   WRITE(TMP, 6000) INT(NMLBDT(2))
                   FSCALE = 10.0**(-1*NMLBDT(2))

C
C             * IF SCIENTIFIC NOTATION WASN'T USED, THEN WE'VE ALREADY
C             * CALCULATED THE LOG OF THE SCALING FACTOR IN FSCALE.
C
              ELSE
                   WRITE(TMP, 6000) INT(FSCALE)
                   FSCALE = 10.0**(-1*FSCALE)
              ENDIF


C
C             * HERE IS WHERE WE SCALE THE DATA.
C
              DO 400 I = 1, NPTS
                   IF (Y(I) .NE. 1.E36) THEN
                        Y(I) = Y(I) * FSCALE
                   ENDIF
  400         CONTINUE

C
C             * CHANGE THE YHI AND YLO SETTINGS IF THEY WERE SPECIFIED
C             * BY THE USER
C
              IF (YLO .NE. 1.E36) THEN
                   CALL AGSETF('Y/MAXIMUM.', YHI*FSCALE)
                   CALL AGSETF('Y/MINIMUM.', YLO*FSCALE)
              ENDIF


C
C             * SET NUMERIC LABEL TYPE TO 'NO-EXPONENT' NOTATION.
C
              CALL AGSETF('LEFT/TYPE.', 3.)

C             * SET POSITONING OF NUMERIC LABELS
              CALL AGSETF('AXIS/LEFT/NUMERIC/OFFSET', .2)
C
C             * TURN ON THE SCALE LABEL IN THE TOP LEFT HAND CORNER FOR
C             * SCIENTIFIC NOTATION.
C
              CALL AGSETC('LABEL/NAME.', 'SCALE')
              CALL AGSETF('LABEL/SUPPRESSION.', 0.0)

C
C             * SET THE TEXT OF THE LABEL.
C
              CALL AGSETF('LINE/NUMBER.', 100.0)
              CALL SHORTN(TMP, 8, .TRUE., .TRUE., ILEN)
              STRING='''L1''X10''H-7'''//TMP(4:ILEN)//'''H7''!'
              CALL AGSETC('LINE/TEXT.', STRING)

              WRITE(6, *) 'SCALING LABEL SHOULD BE DRAWN.'

         ELSE
C
C             * MAKE SURE THE SCALE LABEL IS TURNED OFF.
C
              CALL AGSETC('LABEL/NAME.', 'SCALE')
              CALL AGSETF('LABEL/SUPPRESSION.', 1.0)

              WRITE (6, *) 'NO SCALING LABEL NEEDED.'
         ENDIF

         WRITE(6, 6997) NMLBDT
         WRITE(6, 6996) TCKDAT
 6997    FORMAT(1X,'NMLBDT:', 11E11.4)
 6996    FORMAT(1X,'TCKDAT:',  3E11.4)



C
C        * FIX UP THE TOP LABEL SO EZXY WILL CENTER IT PROPERLY
C        * AND DO SOME CRUDE SCALING TO MAKE IT FIT IF IT@S TOO LONG.
C
         CALL SHORTN(LABEL, 83, .TRUE., .TRUE., ILEN)
         IF (ILEN - 3 .GT. 40) THEN
              CALL AGSETC('LABEL/NAME.', 'T')
              CALL AGSETF('LINE/NUMBER.', 100.0)

              IF (ILEN - 3 .LE. 60) THEN
                   CALL AGSETF('LINE/CHARACTER.', 0.020)
              ELSE
                   CALL AGSETF('LINE/CHARACTER.', 0.015)
              ENDIF
         ENDIF

C
C        * PLOT THE CURVE
C
         CALL EZXY(X,Y,NPTS,ALABEL(4:ILEN)//'!')



C        * DRAW Y=0 LINE IF 0 IS IN THE RANGE
         IF(FYLO*FYHI .LT. 0.0) THEN
              CALL LINE (FXLO, 0.0, FXHI, 0.0)
         ENDIF

C        *  ADVANCE TO THE NEXT FRAME AND RESET THE PLOT POINTS TO
C        *  UNDRAWN.
         CALL FRAME
         CALL RESET


         PRINT *, 'YHI =', YHI, ' YLO =', YLO



C        * IF MS.NE.0 DRAW A GRAPH ON THE PRINTER.
c!!!         IF(MS.NE.0)THEN
c!!!  750     IF(NPTS.EQ.NOPTS) GO TO 800
c!!!          IF(NPTS.LT.NOPTS) CALL                   PXIT('XPLOT',-102)
c!!!          NNPTS=(NPTS+1)/2
c!!!          DO 775 J=1,NNPTS
c!!!           X(J)=X(2*J-1)
c!!!           Y(J)=Y(2*J-1)
c!!!  775     CONTINUE
c!!!          NPTS=NNPTS
c!!!          GO TO 750
c!!!
c!!!  800     CALL SPLAT(Y,NPTS,1,NPTS,1,YLO,YHI)
c!!!          WRITE(6,6012) (LABEL(I),I=4,83)
c!!!         ENDIF



C        * E.O.F. ON INPUT.  A.K.A. 'END WHILE LOOP'
         GO TO 150
  900 CONTINUE
      IF(NPLOT.EQ.0) CALL                          PXIT('XPLOT',-1)

      CALL                                         PXIT('XPLOT',0)
  902 CALL                                         PXIT('XPLOT',-2)
C-----------------------------------------------------------------------
 5010 FORMAT(10X,I10,1X,A4,I5,2E10.0,I5,E10.0,I5)                               
 5012 FORMAT(80A1)                                                              
 6000 FORMAT(I8)
 6010 FORMAT(' UNABLE TO FIND-',A4,' AT TIMESTEP-',I10,' AND LEVEL-',
     1       I5)
 6012 FORMAT(' ',80A1)
 6090 FORMAT(/,' *** WARNING: PACKING DENSITY = ', I4,' ***',/)
      END
