      PROGRAM GGSIGN
C     PROGRAM GGSIGN (GGIN,       INPUT,       OUTPUT,                  )       A2
C    1          TAPE1=GGIN, TAPE5=INPUT, TAPE6=OUTPUT)
C     ------------------------------------------------                          A2
C                                                                               A2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       A2
C     JAN 12/94 - F.MAJAESS (CORRECT "SKIP" OPTION )                            
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                
C     MAR 27/86 - J.HASLIP. (CHANGED FIRST CALL XIT-2 TO CALL XIT-1)            
C     NOV 24/83 - B.DUGAS.
C     NOV 24/80 - J.D.HENDERSON 
C                                                                               A2
CGGSIGN  - MAPS THE SIGN (-,0,+) OF EACH POINT IN A GRID                1  0 C  A1
C                                                                               A3
CAUTHOR  - J.D.HENDERSON                                                        A3
C                                                                               A3
CPURPOSE - DRAWS A COMPACT MAP OF THE SIGN OF EACH GRID POINT FOR EACH          A3
C          GRID IN A GCM DIAGNOSTIC OUTPUT FILE AS REQUESTED ON CARDS.          A3
C          GRIDS MUST BE REQUESTED IN THE SAME ORDER AS THEY OCCUR IN           A3
C          THE FILE SINCE THE FILE IS SCANNED ONLY ONCE.                        A3
C                                                                               A3
CINPUT FILE...                                                                  A3
C                                                                               A3
C      GGIN = FILE CONTAINING GRIDS TO BE MAPPED                                A3
C 
CINPUT PARAMETERS...
C                                                                               A5
C      NSTEP,                                                                   A5
C      NAME,                                                                    A5
C    & LEVEL = STEP,NAME,LEVEL FROM THE LABEL                                   A5
C              IF NAME=4HNEXT THE NEXT GRID ON THE FILE IS MAPPED.              A5
C      LABEL = 80 CHARACTERS LABEL                                              A5
C                                                                               A5
CEXAMPLE OF INPUT CARDS...                                                      A5
C                                                                               A5
C*  GGSIGN         6  PHI  500                                                  A5
C*        500 MB GEOPOTENTIAL                                                   A5
C-------------------------------------------------------------------------- 
C 
      use diag_sizes, only : SIZES_BLONP1,
     &                       SIZES_LONP1xLAT,
     &                       SIZES_LONP1xLATxNWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      COMMON/BLANCK/F(SIZES_LONP1xLAT) 
C 
      LOGICAL OK
      INTEGER LABEL(10) 
      CHARACTER*1 ROW(SIZES_BLONP1+3)
      COMMON/ICOM/IBUF(8),IDAT(SIZES_LONP1xLATxNWORDIO)
      DATA MAXX/SIZES_LONP1xLATxNWORDIO/
C---------------------------------------------------------------------
      NFF=3 
      CALL JCLPNT(NFF,1,5,6)
      REWIND 1
C 
C     * READ A CARD IDENTIFYING THE FIELD TO BE CONTOURED.
C     * IF NAME=4HSKIP JUST SKIP THE NEXT RECORD ON THE FILE. 
C     * OTHERWISE READ THE LABEL TO BE PRINTED UNDER THE MAP. 
C 
  110 READ(5,5010,END=900) NSTEP,NAME,LEVEL                                     A4
      IF(NAME.EQ.NC4TO8("STOP")) CALL              XIT('GGSIGN',1)
C     IF(NAME.EQ.4HSKIP) READ(1,END=903)
C     IF(NAME.EQ.4HSKIP) GO TO 110
      IF(NAME.EQ.NC4TO8("SKIP")) THEN
        CALL FBUFFIN(1,IBUF,MAXX,KK,LEN)
        IF (KK.GE.0) GO TO 903
        GO TO 110
      ENDIF
      READ(5,5012,END=902) LABEL                                                A4
C 
C     * FIND THE REQUESTED FIELD. 
C 
      CALL GETFLD2(1,F,NC4TO8("GRID"),NSTEP,NAME,LEVEL,IBUF,MAXX,OK)
      IF(.NOT.OK)THEN 
        WRITE(6,6020) NSTEP,NAME,LEVEL
        CALL                                       XIT('GGSIGN',-1) 
      ENDIF 
      LX=IBUF(5)
      LY=IBUF(6)
C 
C     *  MAP  THE FIELD AND GO BACK FOR THE NEXT ONE. 
C 
      WRITE(6,6001) 
      DO 220 JR=1,LY
      N=(LY-JR)*LX
      DO 210 I=1,LX 
      K=N+I 
      ROW(I)='0'
      IF(F(K).GT.0.E0) ROW(I)='+' 
      IF(F(K).LT.0.E0) ROW(I)='-' 
  210 CONTINUE
      J=LY+1-JR 
      WRITE(6,6025) J,(ROW(I),I=1,LX) 
  220 CONTINUE
      WRITE(6,6030) IBUF
      WRITE(6,6040) LABEL 
      GO TO 110 
C 
C     * E.O.F. ON INPUT.
C 
  900 CALL                                         XIT('GGSIGN',0)
  902 CALL                                         XIT('GGSIGN',-2) 
C 
C     * E.O.F. ON FILE GGIN.
C 
  903 CALL                                         XIT('GGSIGN',-3) 
C---------------------------------------------------------------------
 5010 FORMAT(10X,I10,1X,A4, I5)                                                 A4
 5012 FORMAT(10A8)                                                              A4
 6001 FORMAT('1')
 6020 FORMAT('0..EOF LOOKING FOR',I10,2X,A4,I6)
 6025 FORMAT(' ',I4,2X,120A1)
 6030 FORMAT('0', 8X,'    STEP  NAME     LEVEL  LX  LY  KHEM NPACK',
     1 /' ',2X, A4,I10,2X,A4,I10,2I4,2I6)
 6040 FORMAT('+',48X,10A8)
      END
