      PROGRAM FTEST 
C     PROGRAM FTEST (SDX,        SDY,          FRATIO,        KVALU,            H2
C    1                           DVALU,        MASK,          OUTPUT,   )       H2
C    2         TAPE1=SDX,  TAPE2=SDY,   TAPE11=FRATIO, TAPE12=KVALU,
C    3                    TAPE13=DVALU, TAPE14=MASK,    TAPE6=OUTPUT) 
C     ---------------------------------------------------------------           H2
C                                                                               H2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       H2
C     AUG 21/00 - F.MAJAESS (ENSURE FIELD DIMENSION CHECK VERSUS "MAXRSZ")      
C     JUN 05/94 - B.DENIS  (MODIFY CALCULATION OF XSCALE/YSCALE)                
C     MAR 07/94 - F.MAJAESS (MODIFY DATA SECTION)
C     SEP 07/93 - J. FYFE  (FIX "JCLPNT" CALL )                                 
C     AUG 04/92 - E. CHAN  (MODIFY EXTRACTION/CALCULATION OF XMAX, XMIN,        
C                           AND XSCALE DUE TO IMPLEMENTATION OF NEW PACKER)    
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                
C     F. ZWIERS  -- AUG 31, 1989 (FIX AN EXIT CONDITION ERROR)                  
C     F. MAJAESS -- APR 29, 1988 (REPLACE ABORT EXIT BY WARNING EXIT
C                                 FOR UNMATCHED MEAN FIELD NAMES) 
C     F. MAJAESS -- NOV 06, 1987 (ADD COMPUTATIONS FOR K AND D-VALUES). 
C     F. ZWIERS  -- NOV 28, 1984
C                                                                               H2
CFTEST   - CONDUCT AN EQUALITY OF VARIANCE TEST (F-TEST).               2  4 C  H1
C                                                                               H3
CAUTHOR - F. ZWIERS, F. MAJAESS                                                 H3
C                                                                               H3
CPURPOSE - CONDUCT AN EQUALITY OF VARIANCES TEST ( AN F-TEST ) AND              H3
C          REPORT THE 'F-VALUE' IN FRATIO, AS WELL AS RETURNING THE             H3
C          K-VALUE AND D-VALUE WHICH ARE RESPECTIVELY THE TRANSFORMED           H3
C          SIGNIFICANCE LEVELS AND THE ACCEPTANCE/REJECTION FLAG.               H3
C                                                                               H3
C   NOTE - TESTS ARE NOT PERFORMED WHEN EITHER OF THE VARIANCES IS              H3
C          APPROXIMATELY EQUAL TO THE NOISE INDUCED BY THE PACKING              H3
C          ALGORITHM.                                                           H3
C          INTERPRETATION OF SDX AND SDY FILES IS CONTROLLED BY INPUT           H3
C          PARAMETER ISTD (SEE INPUT CARD PARAMETERS DESCRIPTION BELOW).        H3
C          ALSO, MASK FILE NEED NOT BE SPECIFIED IN THE PROGRAM CALLING         H3
C          SEQUENCE.                                                            H3
C                                                                               H3
CINPUT FILES...                                                                 H3
C                                                                               H3
C    IF (ISTD.EQ.0) THEN                                                        H3
C      SDX    = FIELDS OF THE STANDARD DEVIATION OF THE X-VARIABLE.             H3
C               SQRT(SUM(X(I)-XBAR)**2/(NX-1)), I=1,...,NX.                     H3
C    OTHERWISE,                                                                 H3
C      SDX    = FIELDS OF THE VARIANCE OF THE X-VARIABLE.                       H3
C               SUM(X(I)-XBAR)**2/NX, I=1,...,NX.                               H3
C                                                                               H3
C      SDY    = AS SDX EXCEPT FOR THE Y-VARIABLE.                               H3
C                                                                               H3
COUTPUT FILES...                                                                H3
C                                                                               H3
C      FRATIO = FIELDS OF F-VALUES COMPUTED AS :                                H3
C                                                                               H3
C                        F=SX**2/SY**2                                          H3
C                                                                               H3
C               WHERE, SX AND SY ARE THE STANDARD DEVIATIONS OF X               H3
C               AND Y RESPECTIVELY.                                             H3
C                                                                               H3
C      KVALU  = FIELDS OF K-VALUES OF TRANSFORMED SIGNIFICANCE LEVELS           H3
C               COMPUTED AS K IN:                                               H3
C                                                                               H3
C                          P=ALPHA/(5**(K-1))                                   H3
C                                                                               H3
C               OR         K=LOG (ALPHA/P)+1                                    H3
C                               5                                               H3
C                                                                               H3
C               WHICH INDICATES THAT THE OBSERVED STATISTIC IS                  H3
C               SIGNIFICANT AT THE                                              H3
C                          ALPHA/(5**(K-1))                                     H3
C               SIGNIFICANCE LEVEL.                                             H3
C               THUS K=1 INDICATES THAT THE OBSERVED STATISTIC IS JUST          H3
C               SIGNIFICANT AT THE ALPHA SIGNIFICANCE LEVEL.                    H3
C               IF PLOTTED WITH UNIT CONTOUR INTERVALS, SUCCESSIVE              H3
C               CONTOURS WILL ENCLOSE REGIONS WHERE LOCALLY IT IS               H3
C               FIVE TIMES AS UNLIKELY THAT VALUES OF THE OBSERVED              H3
C               STATISTICS ARE CONSISTENT WITH THE NULL HYPOTHESIS              H3
C               THAN IN REGIONS OUTSIDE THE NEXT LOWER CONTOUR.                 H3
C                                                                               H3
C                                                                               H3
C      DVALU  = FIELDS OF D-VALUES COMPUTED AS:                                 H3
C                           __                                                  H3
C                           ! 0  IF THE NULL HYPOTHESIS IS ACCEPTED             H3
C                       D = !                                                   H3
C                           ! 1  IF THE NULL HYPOTHESIS IS REJECTED             H3
C                           --                                                  H3
C                                                                               H3
C      MASK   = FIELDS OF MASK-VALUES COMPUTED AS:                              H3
C                           __                                                  H3
C                           ! 1  IF THE TEST WAS     CONDUCTED                  H3
C                    MASK = !                                                   H3
C                           ! 0  IF THE TEST WAS NOT CONDUCTED                  H3
C                           --                                                  H3
C                                                                               H3
C               THE MASK FILE IS NOT RETURNED IF THE PROGRAM IS                 H3
C               NOT CALLED WITH OUTPUT FILE MASK.                               H3
C 
CINPUT PARAMETERS...
C                                                                               H5
C     NX=INT(ANX) & NY=INT(ANY)                                                 H5
C                                                                               H5
C   WHERE,                                                                      H5
C                                                                               H5
C     ALPHA = THE SIGNIFICANCE LEVEL OF THE TEST.                               H5
C     NX    = THE NUMBER OF OBSERVATIONS IN THE X-VARIABLE DATA SET,            H5
C     NY    = THE NUMBER OF OBSERVATIONS IN THE Y-VARIABLE DATA SET.            H5
C             IF NX > 1 AND NY > 1 THEN                                         H5
C                NX AND NY READ FROM THE INPUT CARD ARE USED IN THE             H5
C                COMPUTATION OF THE NUMBER OF DEGREES OF FREEDOM.               H5
C             OTHERWISE,                                                        H5
C                THE NX AND NY VALUES NEEDED IN THE COMPUTATION ARE             H5
C                OBTAINED FROM THE LABELS OF THE FILES SDX AND SDY.             H5
C     ISTD  = A FLAG USED TO DETERMINE THE CONTENTS OF SDX AND SDY FILES.       H5
C             THAT IS :                                                         H5
C             IF (ISTD.EQ.0) THEN                                               H5
C                SDX AND SDY FILES CONTAIN THE UNBIASED ESTIMATES OF THE        H5
C                STANDARD DEVIATIONS OF X AND Y RESPECTIVELY.                   H5
C             OTHERWISE,                                                        H5
C                SDX AND SDY FILES CONTAIN THE VARIANCES OF X AND Y             H5
C                RESPECTIVELY.                                                  H5
C                                                                               H5
CEXAMPLE OF INPUT CARD...                                                       H5
C                                                                               H5
C*FTEST.       5.E-2  10.   5.    1                                             H5
C---------------------------------------------------------------------------
C 
      use diag_sizes, only : SIZES_LONP1xLAT,
     &                       SIZES_LONP1xLATxNWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      DIMENSION IBUF(8),IDAT(SIZES_LONP1xLATxNWORDIO),
     & IBUFX(8),IBUFY(8),
     & WK(SIZES_LONP1xLAT),SX(SIZES_LONP1xLAT),SY(SIZES_LONP1xLAT),
     & F(SIZES_LONP1xLAT),P(SIZES_LONP1xLAT),D(SIZES_LONP1xLAT),
     & FMASK(SIZES_LONP1xLAT) 
      REAL K(SIZES_LONP1xLAT)
      COMMON/BLANCK/SX,SY,F,P,WK,FMASK
      COMMON/MACHTYP/MACHINE,INTSIZE
      COMMON/ICOM/IBUF,IDAT 
      EQUIVALENCE (SX(1),K(1)),(SY(1),D(1)) 
      LOGICAL OK,LNXNY,STD,NRMLXIT,MASK,MATCH 
C     DATA NGRID/4HGRID/,NZONL/4HZONL/,NF/4H   F/
C     DATA NK   /4H   K/,ND   /4H   D/,NMASK/4HMASK/
      DATA MAXX,FMAX,NBW/SIZES_LONP1xLATxNWORDIO,100.E0,64/ 
      DATA MAXRSZ /SIZES_LONP1xLAT/
C----------------------------------------------------
C 
      NGRID=NC4TO8("GRID")
      NZONL=NC4TO8("ZONL")
      NF   =NC4TO8("   F")
      NK   =NC4TO8("   K")
      ND   =NC4TO8("   D")
      NMASK=NC4TO8("MASK")
      NRMLXIT=.TRUE.
      MATCH=.TRUE.
      NFILES=8
      CALL JCLPNT(NFILES,1,2,11,12,13,14,5,6) 
      IF(NFILES.GT.7)THEN 
        MASK=.TRUE. 
      ELSE
        MASK=.FALSE.
      ENDIF 
      REWIND 1
      REWIND 2
C 
C     * GET ALPHA, ANX, ANY AND ISTD. 
C 
      READ(5,5010,END=8000)ALPHA,ANX,ANY,ISTD                                   H4
      GO TO 8010
C 
C-----------------------------------------------------------------------
C     * EOF ON UNIT #5. 
C 
 8000 CONTINUE
      WRITE(6,6070) 
      CALL                                         XIT('FTEST',-1)
 8010 CONTINUE
      WRITE(6,6050)ALPHA,ANX,ANY,ISTD 
      NX=INT(ANX+0.0001E0)
      NY=INT(ANY+0.0001E0)
C 
C     * CHECK NX, NY AND ISTD.
C 
      IF((NX.LE.1).OR.(NY.LE.1))THEN
        LNXNY=.TRUE.
      ELSE
        LNXNY=.FALSE. 
      ENDIF 
      IF(ISTD.EQ.0)THEN 
        STD=.TRUE.
      ELSE
        STD=.FALSE. 
      ENDIF 
C 
C---------------------------------------------------------------------- 
C 
      IKIND=1 
      NREC=0
 1000 CONTINUE
C 
C     * READ SDX INTO SX. 
C 
      CALL GETFLD2(1,SX,-1,-1,-1,-1,IBUF,MAXX,OK)
      IF(.NOT.OK)THEN 
         IF(NREC.EQ.0)THEN
            WRITE(6,6010) 
            CALL                                   XIT('FTEST',-2)
         ENDIF
         WRITE(6,6040) NREC 
         IF(NRMLXIT.AND.MATCH)THEN
           CALL                                    XIT('FTEST',0) 
         ELSE 
           CALL                                    XIT('FTEST',-101)
         ENDIF
      ENDIF 
      DO 1010 I=1,8 
         IBUFX(I)=IBUF(I) 
 1010 CONTINUE
      IF(NREC.EQ.0)WRITE(6,6060)IBUFX 
      IF(LNXNY)NX=IBUFX(2)
      IF(NX.LE.1)THEN 
         WRITE(6,6080)NX
         CALL                                      XIT('FTEST',-3)
      ENDIF 
C     XMIN=DECODR(IDAT(1))
C     XSCALE=1./DECODR(IDAT(2)) 
C     XMAX=XMIN+XSCALE*(2**(NBW/IBUF(8))-1.0) 
      CALL DECODR2(IBUF(9),XMIN)
      CALL DECODR2(IBUF(8+MACHINE+1),XMAX)
      XSCALE=(XMAX-XMIN)/(2.E0**(NBW/IBUF(8))-1.0E0) 
      IF(XMIN.LE.1.E-3*XMAX)THEN
        EPSX=2.0E0*(XMIN+XSCALE)
      ELSE
        EPSX=2.0E0*XSCALE 
      ENDIF 
C 
C     * READ SDY INTO SY. 
C 
      CALL GETFLD2(2,SY,-1,-1,-1,-1,IBUF,MAXX,OK)
      IF(.NOT.OK)THEN 
         WRITE(6,6020)
         CALL                                      XIT('FTEST',-4)
      ENDIF 
      DO 1020 I=1,8 
         IBUFY(I)=IBUF(I) 
 1020 CONTINUE
      IF(NREC.EQ.0)WRITE(6,6060)IBUFY 
      IF(LNXNY)NY=IBUFY(2)
      IF(NY.LE.1)THEN 
         WRITE(6,6090)NY
         CALL                                      XIT('FTEST',-5)
      ENDIF 
C     YMIN=DECODR(IDAT(1))
C     YSCALE=1./DECODR(IDAT(2)) 
C     YMAX=YMIN+YSCALE*(2**(NBW/IBUF(8))-1.0) 
      CALL DECODR2(IBUF(9),YMIN)
      CALL DECODR2(IBUF(8+MACHINE+1),YMAX)
      YSCALE=(YMAX-YMIN)/(2.E0**(NBW/IBUF(8))-1.0E0) 
      IF(YMIN.LE.1.E-3*YMAX)THEN
        EPSY=2.0E0*(YMIN+YSCALE)
      ELSE
        EPSY=2.0E0*YSCALE 
      ENDIF 
C 
C     * CHECK LABELS
C 
      IF( (IBUFX(1).NE.NGRID .AND. IBUFX(1).NE.NZONL) .OR.
     1    (IBUFY(1).NE.NGRID .AND. IBUFY(1).NE.NZONL) .OR.
     2    (IBUFX(1).NE.IBUFY(1)) .OR. (IBUFX(4).NE.IBUFY(4)) .OR. 
     3    (IBUFX(5).NE.IBUFY(5)) .OR. (IBUFX(6).NE.IBUFY(6)))THEN 
         WRITE(6,6030)
         WRITE(6,6060)IBUFX 
         WRITE(6,6060)IBUFY 
         CALL                                      XIT('FTEST',-6)
      ENDIF 
C 
      IF( (IBUFX(3).NE.IBUFY(3)) .AND. MATCH ) THEN 
         MATCH=.FALSE.
         WRITE(6,6120)
      ENDIF 
C 
      NDFX=NX-1 
      NDFY=NY-1 
      NWDS=IBUFX(5)*IBUFX(6)
      IF(NWDS.GT.MAXRSZ)THEN 
         WRITE(6,6100)MAXRSZ,NWDS
         CALL                                      XIT('FTEST',-7)
      ENDIF 
C 
C     * COMPUTE F-RATIOS. 
C 
      ICNT=0
      IF(STD)THEN 
         DO 1090 I=1,NWDS 
           IF((SX(I).LE.EPSX).OR.(SY(I).LE.EPSY))THEN 
             FMASK(I)=0.0E0 
             F(I)=1.0E0 
             ICNT=ICNT+1
           ELSE 
             FMASK(I)=1.0E0 
             F(I)=(SX(I)*SX(I))/(SY(I)*SY(I)) 
           ENDIF
 1090    CONTINUE 
      ELSE
         FNX=FLOAT(NX)
         FNY=FLOAT(NY)
         FACTX=FNX/(FNX-1.0E0)
         FACTY=FNY/(FNY-1.0E0)
         DO 1100 I=1,NWDS 
           IF((SX(I).LE.EPSX).OR.(SY(I).LE.EPSY))THEN 
             FMASK(I)=0.0E0 
             F(I)=1.0E0 
             ICNT=ICNT+1
           ELSE 
             FMASK(I)=1.0E0 
             F(I)=(FACTX*SX(I))/(FACTY*SY(I)) 
           ENDIF
 1100    CONTINUE 
      ENDIF 
C 
      IF(ICNT.GE.1)THEN 
        NRMLXIT=.FALSE. 
        WRITE(6,6110)ICNT,NWDS,IBUFX(4) 
      ENDIF 
C 
      DO 1105 I=1,NWDS
         F(I)=MERGE(FMAX,F(I),F(I).GE.FMAX)
 1105 CONTINUE
C 
C     * PREPARE TO WRITE OUT THE RESULTS. 
C 
         DO 1110 I=1,8
            IBUF(I)=IBUFX(I)
 1110    CONTINUE 
C 
C     * WRITE OUT THE F-RATIOS. 
C 
         IBUF(3)=NF 
         CALL PUTFLD2(11,F,IBUF,MAXX)
         IF(NREC.EQ.0)WRITE(6,6060)IBUF 
C 
C     * COMPUTE P-VALUES AS 
C     * 
C     *                    P=PROB(F-RATIO > OBSERVED F-RATIO) 
C     * 
C     *         (IE, THE PROBABILITY OF OBSERVING AN F-RATIO GREATER
C     *         THAN THE CALCULATED F-RATIO UNDER THE HYPOTHESIS THAT 
C     *         THE VARIANCES ARE EQUAL). 
C 
         CALL PROBF(F,NDFX,NDFY,NWDS,P,WK,SX,SY)
C 
C     * COMPUTE K AND D-VALUES. 
C 
         CALL HTEST(P,NWDS,ALPHA,IKIND,K,D) 
C 
C     * WRITE OUT THE K-VALUES. 
C 
         IBUF(3)=NK 
         CALL PUTFLD2(12,K,IBUF,MAXX)
         IF(NREC.EQ.0)WRITE(6,6060)IBUF 
C 
C     * WRITE OUT THE D-VALUES. 
C 
         IBUF(3)=ND 
         CALL PUTFLD2(13,D,IBUF,MAXX)
         IF(NREC.EQ.0)WRITE(6,6060)IBUF 
C 
C     * WRITE OUT THE MASK-VALUES IF REQUIRED.
C 
         IF(MASK)THEN 
           IBUF(3)=NMASK
           CALL PUTFLD2(14,FMASK,IBUF,MAXX)
           IF(NREC.EQ.0)WRITE(6,6060)IBUF 
         ENDIF
C 
         NREC=NREC+1
         GOTO 1000
C 
C-------------------------------------------------------------------------
C 
 5010 FORMAT(10X,E10.0,2F5.0,I5)                                                H4
 6010 FORMAT('0FIRST FILE OF STANDARD DEVIATIONS WAS EMPTY.')
 6020 FORMAT('0UNEXPECTED EOF ON SDY FILE.')
 6030 FORMAT('0CORRESPONDING FIELD LABELS DO NOT MATCH.')
 6040 FORMAT('0CONDUCTED F-TESTS ON ',I5,' PAIRS OF FIELDS.')
 6050 FORMAT('0FTEST.    ',E10.1,2F5.0,I5)
 6060 FORMAT(' ',A4,I10,2X,A4,I10,4I6)
 6070 FORMAT('0 THE FTEST CARD IS MISSING.')
 6080 FORMAT('0ILLEGAL # OF OBS. NX= ',I10)
 6090 FORMAT('0ILLEGAL # OF OBS. NY= ',I10)
 6100 FORMAT('0INSUFFICIENT ARRAY SIZE= ',I10,', FOR # PTS= ',I10)
 6110 FORMAT('0WARNING ',I6,' DETECTED VERY SMALL DENOMINATORS ',
     1       'OUT OF ',I6,' COMPUTED F-VALUES AT LEVEL ',I6)
 6120 FORMAT('0** NOTE - UNMATCHED MEAN FIELD NAMES **.')
C 
C-------------------------------------------------------------------------
C 
      END
