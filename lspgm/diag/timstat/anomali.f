      PROGRAM ANOMALI
C     PROGRAM ANOMALI (TSIN,       GGOUT,       MASK,       INPUT,              H2
C    1                                                      OUTPUT,     )       H2
C    2           TAPE1=TSIN, TAPE2=GGOUT, TAPE3=MASK, TAPE5=INPUT,
C    3                                                TAPE6=OUTPUT)
C     -------------------------------------------------------------             H2
C                                                                               H2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       H2
C     AUG 21/00 - F.MAJAESS (ENSURE TIME SERIES CHECK VERSUS "MAXTSZ")          
C     JAN 03/97 - D. LIU (LSR PASSED AS ARGUMENT TO SUBROUTINE T2LBL)           
C     FEB 22/96 - D. LIU (1.CONVERSION OF TIME-SERIES REPLACED BY T2LBL         
C                         2.ENGLISH VERSION OF 'PURPOSE' REWRITTEN              
C                         3.LOOP 400 MODIFIED)                                  
C     JUL 22/92 - E. CHAN  (REPLACE UNFORMATTED I/O WITH I/O ROUTINES)          
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)
C     JUL 21/86 - J.HASLIP (CORRECTION IN DO 350 LOOP.
C                            MASK(L)=UN CHANGED FROM MASK(I)=UN)
C     DEC 22/85 - B.DUGAS.
C                                                                               H2
CANOMALI - TESTING A SET OF CONDITIONS ON SETS OF TIME SERIES           1  2 C  H1
C                                                                               H3
CAUTHOR  - B.DUGAS                                                              H3
C                                                                               H3
CPURPOSE - EACH VALUE OF A TIME SERIES OF DATA IS COMPARED WITH THE             H3
C          VARIABLE M. IF THE NUMBER OF CONSECUTIVE DATA POINTS WHOSE VALUES    H3
C          ARE GREATER THAN OR EQUAL TO M FALLS BETWEEN THE PRESCIBED T1 AND    H3
C          T2 (>=T1 AND <T2), IT IS RECORDED AS ONE EVENT. THE MASK FOR THE     H3
C          DATA POINTS OF AN EVENT IS SET TO 1. ALL OTHER DATA POINTS HAVE      H3
C          A MASK OF VALUE ZERO. EVENT NUMBER IS RECORDED FOR THE               H3
C          'GEORGRAPHICAL' LOCATION THE TIME SERIES REPRESENTS. IT IS           H3
C          ACCUMULATED OVER ALL TIME SERIES IN THE INPUT FILE.                  H3
C                                                                               H3
C          DETERMINE LE NOMBRE DE FOIS QU'UN ENSEMBLE DE CRITERES EST           H3
C          VERIFIE. DE PLUS LES POSITIONS DANS L'ESPACE ET DANS LE TEMPS        H3
C          DE CES POINTS SONT IMPRIMES. UN POINT EST RETENU SSI LA VALEUR       H3
C          DU CHAMPS Y EST PLUS GRANDE QUE M DEPUIS AU MOINS T1 INTERVAUX       H3
C          ET POUR MOINS QUE T2 INTERVAUX. M, T1 ET T2 SONT LUS EN ENTREE.      H3
C                                                                               H3
CINPUT FILE...                                                                  H3
C                                                                               H3
C      TSIN  = CONTAINS RECORDS OF GEOGRAPHIC POSITION TIME SERIES ON           H3
C              A SINGLE PRESSURE LEVEL                                          H3
C                                                                               H3
C              LE CHAMPS D'ENTREE TSIN EST SUPPOSE NE CORRESPONDRE QU'A         H3
C              UN NIVEAU DE PRESSION ET LES ENREGISTREMENTS INDIVIDUELS         H3
C              SONT DES SERIES TEMPORELLES (UNE SERIE PAR POINT DANS LA         H3
C              GRILLE SPACIALE).                                                H3
C                                                                               H3
COUTPUT FILES...                                                                H3
C                                                                               H3
C      GGOUT = CONTAINS A RECORD FOR GEOGRAPHIC POSITIONS WITH VALUES           H3
C              CONSISTING OF THE NUMBER OF TIMES THE TEST CONDITIONS            H3
C              ARE SATISFIED                                                    H3
C                                                                               H3
C              CONTIENT LE NOMBRE DE FOIS QUE LE CRITERE A ETE VERIFIE          H3
C              A CHAQUE POSITION GEOGRAPHIQUE.                                  H3
C                                                                               H3
C      MASK  = CONTAINS RECORDS OF TIME SERIES CONSISTING OF 1'S FOR            H3
C              POINTS PASSING THE TEST AND O'S OTHERWISE.                       H3
C                                                                               H3
C              EST UN FICHIER DE SERIES TEMPORELLES CONTENANT DES "1.0"S        H3
C              AUX MOMENTS VERIFIANT LE CRITERE ET ZERO AILLEUR.                H3
C
CINPUT PARAMETERS...
C                                                                               H5
C      M  = VALUE AGAINST WHICH THE VALUES READ ARE TO BE TESTED.               H5
C           (M.GT.0.0)                                                          H5
C      T1 = LOWER LIMIT OF THE TIME RANGE (T1.GE.1)                             H5
C      T2 = UPPER LIMIT OF THE TIME RANGE (T2.GT.T1). DEFAULTS TO $TSL$.        H5
C                                                                               H5
C      NOTE - A POINT PASSES THE VALUE TEST IF ITS POSITIONAL VALUE IS          H5
C             GREATER OR EQUAL TO M AND ITS TIME STAMP (TS) FALLS WITHIN        H5
C             THE TIME RANGE (T1,T2); (I.E., TS.GE.T1 .AND. TS.LT.T2).          H5
C                                                                               H5
CEXAMPLE OF INPUT CARD...                                                       H5
C                                                                               H5
C*ANOMALI.        1.    1    0                                                  H5
C----------------------------------------------------------------------------
C

      use diag_sizes, only : SIZES_LMTP1,
     &                       SIZES_LONP1xLAT,
     &                       SIZES_NWORDIO,
     &                       SIZES_TSL

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      integer, parameter :: MAXT = SIZES_TSL*SIZES_NWORDIO
      DATA  MAXW /SIZES_LONP1xLAT/
      DATA  MAXTSZ /SIZES_TSL/

      LOGICAL OK,SPEC

      INTEGER IBOUT(8), POSIT, T1, T2, LSR(2,SIZES_LMTP1+1)
      REAL    EVENT(SIZES_LONP1xLAT), TSIN(SIZES_TSL), M,
     &        MAXL, MASK(SIZES_TSL), ZERO
 
      COMMON/ICOM/IBUF(8),IDAT(MAXT)
 
      DATA  ZERO /   0.0E0/, UN   /   1.0E0/

C---------------------------------------------------------------------
      NF = 5
      CALL JCLPNT(NF,1,2,3,5,6)
      REWIND 1
      REWIND 2
      REWIND 3

C     * LIRE LA CARTE DE CONTROLE.

      READ(5,5000,END=900)  M, T1, T2                                           H4
      IF (T2.EQ.0) T2 = MAXT+1
      IF (T1.LT.1.OR.T2.LE.T1) CALL                XIT('ANOMALI',-1)
      WRITE(6,6000) M, T1, T2

      CALL FBUFFIN(1,IBUF,-8,K,LEN)
      IF (K.GE.0) CALL                             XIT('ANOMALI',-6)
      WRITE(6,6200) IBUF
      REWIND 1

C     * CONVERT TIME-SERIES LABEL TO STANDARD LABEL
      CALL T2LBL(IBUF,IBOUT,LSR)

      IBOUT(2)       = 0
      IBOUT(3)       = NC4TO8("ANOM")
      IBOUT(8)       = 1

      SPEC=(IBOUT(1).EQ.NC4TO8("SPEC"))
      LEN            = IBUF(5)
      NWDS           = IBOUT(5)*IBOUT(6)
      IF (SPEC) NWDS = NWDS*2
      IF (NWDS.GT.MAXW .OR. LEN.GT.MAXTSZ) CALL    XIT('ANOMALI',-3)

C     * INITIALISER LE CHAMPS EVENT.

      DO 100 I=1,NWDS
          EVENT(I)  = 0.E0
  100 CONTINUE

C---------------------------------------------------------------------
C     * ALLER CHERCHER LA PROCHAINE SERIE.

      KOUNT  = 0
      NSERIE = 0
  200 CALL GETFLD2(1,TSIN,-1,-1,-1,-1,IBUF,MAXT,OK)

          IF (.NOT.OK)                                         THEN
              WRITE(6,6030) NSERIE,IBUF(3)
              IF (NSERIE.EQ.0) CALL                XIT('ANOMALI',-4)
              IBOUT(4) = IBUF(4)
              CALL PUTFLD2 (2,EVENT,IBOUT,MAXW)
              CALL                                 XIT('ANOMALI',0)
          ENDIF

C         * A QUEL POINT CETTE SERIE EST-T-ELLE VALABLE ?

          IF (SPEC)                                            THEN
              POSIT = IBUF(2)
          ELSE
              IB2   = IBUF(2)
              LAT   = IB2/1000
              LON   = IB2-LAT*1000
              POSIT = (LAT-1)*IBOUT(5)+LON
          ENDIF

C         * INITIALISER LA GRILLE MASK ET LES VARIABLES
C         * MAXL ET LAST A ZERO.

          DO 300 I=1,LEN
              MASK(I) = ZERO
  300     CONTINUE

          MAXL = TSIN(1)
          LAST = 0
          IEND = 0

C         * CHERCHER LES MOMENTS OU LES CRITERES SONT SATISFAITS.

          DO 400 I=1,LEN

              VALEUR = TSIN(I)
              IF (VALEUR.GE.M)                                 THEN
                  LAST             = LAST+1
                  MAXL             = MAX( MAXL, VALEUR)
                  IF(I.EQ.LEN) IEND= 1
              ENDIF
              IF (VALEUR.LT.M .OR. I.EQ.LEN)                   THEN
                  IF (LAST.GE.T1 .AND. LAST.LT.T2)             THEN
                      KOUNT        = KOUNT+1
                      EVENT(POSIT) = EVENT(POSIT)+1.E0
                      WRITE(6,6300) KOUNT,IBUF(2),I,LAST,MAXL
                      DO 350 J=I-1+IEND,I-LAST+IEND,-1
                          MASK(J)  = UN
  350                 CONTINUE
                      LAST             = 0
                      MAXL             = 0.E0
                  ENDIF
              ENDIF

  400     CONTINUE

          CALL PUTFLD2 (3,MASK,IBUF,MAXT)
          NSERIE = NSERIE+1

      GOTO 200

C     * E.O.F. SUR INPUT ($D$IN).

  900 CALL                                         XIT('ANOMALI',-5)

C     * FIN PREMATUREE DU FICHIER TSIN.


C---------------------------------------------------------------------
 5000 FORMAT(10X,E10.0,2I5)                                                     H4
 6000 FORMAT('0SEARCHING FOR ANOMALIES OF ',E11.4,
     1       ' UNITS'/'0LASTING AT LEAST ',I5,' INTERVALS,',
     2               /'0BUT NO MORE THAN ',I5,' INTERVALS.')
 6030 FORMAT('0ANOMALI READ ',I5,' SERIES OF ',A4,' .')
 6200 FORMAT('0ANOMALI ON :'/'0 ',A4,I10,2X,A4,5I10//
     1 '   N     POSITION      TIME         LENGTH     MAXIMUM '//)
 6300 FORMAT(1X,I5,3X,I6,3X,I10,7X,I5,5X,E11.4)
      END
