      PROGRAM INITOZ7
C     PROGRAM INITOZ7 (ICTL,       OZLX,       OZONE,       OUTPUT,     )       I2
C    1          TAPE99=ICTL, TAPE1=OZLX, TAPE2=OZONE, TAPE6=OUTPUT)
C     -------------------------------------------------------------             I2
C                                                                               I2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       I2
C     OCT 29/96 - M.LAZARE (REVISE FOR NEW WANG INPUT OZONE DATASET)            I2
C     SEP 12/96 - J.DE GRANDPRE (PREVIOUS VERSION INITOZ6)
C                                                                               I2
CINITOZ7 - PRODUCES MONTHLY CROSS-SECTIONS OF ZONAL MEAN OZONE                  I1
C          FOR GCM11                                                    2  1    I1
C                                                                               I3
CAUTHORS - J.P.BLANCHET, M.LAZARE, J.DE GRANDPRE                                I3
C                                                                               I3
CPURPOSE - INTERPOLATES INITIAL MONTHLY ZONAL CROSS-SECTIONS OF PPMV            I3
C          OZONE FROM T42 GAUSSIAN-GRID TO THE GAUSSIAN GRID, AND THEN          I3
C          MULTIPLIES BY 1.E-6 TO CONVERT TO PPV. (WANG DATA)                   I3
C          THE MODEL ROUTINE OZON10 USES THIS RESULT, ALONG WITH THE            I3
C          SURFACE PRESSURE, TO CALCULATE THE OZONE AMOUNT BETWEEN              I3
C          FULL MODEL SIGMA SURFACES.                                           I3
C          IF THE MODEL INITIALIZATION DATE/TIME IS NOT ON A MONTH              I3
C          BOUNDARY, THE PROGRAM INTERPOLATES BETWEEN ADJACENT MONTH            I3
C          BOUNDARIES AND STORES THE RESULT ON THE OZONE FILE AS WELL           I3
C          WITH VALUE OF IDAY IN IBUF(2).                                       I3
C                                                                               I3
CINPUT FILES...                                                                 I3
C                                                                               I3
C      ICTL  = INITIALIZATION CONTROL DATASET (SEE ICNTRL5).                    I3
C      OZLX  = MONTHLY LATITUDINAL GLOBAL CROSS-SECTIONS OF OZONE IN            I3
C              PPMV (ON A T42 GAUSSIAN GRID AND A RELATIVELY DENSE              I3
C              PRESSURE GRID) OBTAINED FROM THE PCMDI OFFICE (WANG DATA).       I3
C                                                                               I3
COUTPUT FILE...                                                                 I3
C                                                                               I3
C      OZONE = MONTHLY GAUSSIAN LATITUDE CROSS-SECTIONS OF OZONE AMOUNT         I3
C              (PPV) FOR EACH INPUT PRESSURE LEVEL, (ON LEVOZ LEVELS,           I3
C              WHERE LEVOZ IS THE NUMBER OF PRESSURE LEVELS IN THE              I3
C              ORIGINAL OZONE DATASET IN THE LLPHYS).                           I3
C              THERE ARE LEVOZ (=59) PRESSURE LEVELS AND ILAT LATITUDES.        I3
C---------------------------------------------------------------------------
C
C
C     *    ILAT = SOUTH - NORTH NUMBER OF GRID POINTS.
C     *    ALAT = VECTOR OF INPUT T42 GAUSSIAN LATITUDES (S.P. TO N.P.) IN
C     *           ORIGINAL OZONE DATA (THE VALUES AT -90. AND 90. WERE ADDED 
C     *           FOR COMPLETENESS BY COPYING THE ADJACENT VALUES AT BOTH 
C     *           POLES).
C
      use diag_sizes, only : SIZES_BLONP1xBLAT,
     &                       SIZES_BLONP1xBLATxNWORDIO,
     &                       SIZES_LAT

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      PARAMETER(LEVOZ=59,NLATG=64,NLAT=NLATG+2) 
      LOGICAL OK
      REAL*8 SL(SIZES_LAT),CL(SIZES_LAT),WL(SIZES_LAT),
     & WOSSL(SIZES_LAT),RADL(SIZES_LAT)
      REAL   DLAT(SIZES_LAT)
C
      REAL*8 SLI(NLATG),CLI(NLATG),WLI(NLATG),WOSSLI(NLATG),RADLI(NLATG)
      REAL   DLATI(NLATG)
      REAL ALAT(NLAT),OZZXG(NLATG,LEVOZ),OZZX(NLAT,LEVOZ)
      REAL O3(SIZES_LAT,LEVOZ)
C
      INTEGER NFDM(12),LBL(SIZES_LAT)
C
      COMMON/AOLD/G(SIZES_BLONP1xBLAT)
      COMMON/ANEW/H(SIZES_LAT),HM(SIZES_LAT),HP(SIZES_LAT)
      COMMON/ICOM/IBUF(8),IDAT(SIZES_BLONP1xBLATxNWORDIO)
      COMMON/JCOM/JBUF(8),JDAT(SIZES_BLONP1xBLATxNWORDIO)
C
      EQUIVALENCE (G(1),OZZXG(1,1))
C
      DATA NFDM/1,32,60,91,121,152,182,213,244,274,305,335/

C     ***************************************************************
C     *    VECTOR OF PRESSURE LEVELS IN ORIGINAL OZONE DATA.        *
C     *    THE SIZE OF THIS VECTOR IS 59. ITS VALUES ARE GIVEN IN   *
C     *    THE DATASET BELOW FOR REFERENCE, ALTHOUGH THEY ARE NOT   *
C     *    USED EXPLICITLY IN THE PROGRAM (THE CORRESPONDING        *
C     *    HEIGHTS IN KILOMETRES ARE ALSO GIVEN).                   *
C     *                                                             *
C          ------------ LAYER PRESSURE (MB) -------------->         *
C     * 2.84191E-01 3.25350E-01 3.71936E-01 4.24615E-01 4.84153E-01 *
C     * 5.51432E-01 6.27490E-01 7.13542E-01 8.11039E-01 9.21699E-01 *
C     * 1.04757E+00 1.19108E+00 1.35511E+00 1.54305E+00 1.75891E+00 *
C     * 2.00739E+00 2.29403E+00 2.62529E+00 3.00884E+00 3.45369E+00 *
C     * 3.97050E+00 4.57190E+00 5.27278E+00 6.09073E+00 7.04636E+00 *
C     * 8.16371E+00 9.47075E+00 1.10000E+01 1.27894E+01 1.48832E+01 *
C     * 1.73342E+01 2.02039E+01 2.35659E+01 2.75066E+01 3.21283E+01 *
C     * 3.75509E+01 4.39150E+01 5.13840E+01 6.01483E+01 7.04289E+01 *
C     * 8.24830E+01 9.66096E+01 1.13155E+02 1.32514E+02 1.55122E+02 *
C     * 1.81445E+02 2.11947E+02 2.47079E+02 2.87273E+02 3.32956E+02 *
C     * 3.84573E+02 4.42610E+02 5.07601E+02 5.80132E+02 6.60837E+02 *
C     * 7.50393E+02 8.49521E+02 9.58981E+02 1.00369E+03             *
C     *                                                             *
C          ------------ LAYER ALTITUDE (KM) -------------->         *
C     * 5.75000E+01 5.65000E+01 5.55000E+01 5.45000E+01 5.35000E+01 *
C     * 5.25000E+01 5.15000E+01 5.05000E+01 4.95000E+01 4.85000E+01 *
C     * 4.75000E+01 4.65000E+01 4.55000E+01 4.45000E+01 4.35000E+01 *
C     * 4.25000E+01 4.15000E+01 4.05000E+01 3.95000E+01 3.85000E+01 *
C     * 3.75000E+01 3.65000E+01 3.55000E+01 3.45000E+01 3.35000E+01 *
C     * 3.25000E+01 3.15000E+01 3.05000E+01 2.95000E+01 2.85000E+01 *
C     * 2.75000E+01 2.65000E+01 2.55000E+01 2.45000E+01 2.35000E+01 *
C     * 2.25000E+01 2.15000E+01 2.05000E+01 1.95000E+01 1.85000E+01 *
C     * 1.75000E+01 1.65000E+01 1.55000E+01 1.45000E+01 1.35000E+01 *
C     * 1.25000E+01 1.15000E+01 1.05000E+01 9.50000E+00 8.50000E+00 *
C     * 7.50000E+00 6.50000E+00 5.50000E+00 4.50000E+00 3.50000E+00 *
C     * 2.50000E+00 1.50000E+00 5.00000E-01 1.18000E-01             *
C     ***************************************************************

      DATA MAXX/SIZES_BLONP1xBLATxNWORDIO/, MAXLAT/SIZES_LAT/
C-----------------------------------------------------------------------
      NFF=4
      CALL JCLPNT(NFF,99,1,2,6)
C
C     * GET PARAMETERS FROM CONTROL FILE.
C
      REWIND 99
      READ(99,END=902) LABL,ILEV,(XXX,L=1,ILEV)
      READ(99,END=903) LABL,IXX,IXX,IXX,ILAT,IXX,IDAY,GMT
      WRITE(6,6010)  ILEV,ILAT,IDAY,GMT
C
C     * GET LAT-HGT CROSS-SECTION FOR EACH MONTH (ARRAY NLATG X LEVOZ),
C     * WHERE NLATG IS THE NUMBER OF T42 GAUSSIAN LATITUDES AND LEVOZ
C     * IS THE NUMBER OF PRESSURE LEVELS.
C
      DO 290 MONTH=1,12
      NDAY=NFDM(MONTH)
      CALL GETFLD2(-1,G,NC4TO8("ZONL"),NDAY,NC4TO8("OZWA"),0,
     +                                          IBUF,MAXX,OK)
      IF(.NOT.OK) CALL                             XIT('INITOZ7',-1)
      WRITE(6,6020) IBUF
      IF(NLATG.NE.IBUF(5)) CALL                    XIT('INITOZ7',-2)    
      IF(LEVOZ.NE.IBUF(6)) CALL                    XIT('INITOZ7',-3)
C
      IF(ILAT.GT.MAXLAT.OR.NLAT.GT.MAXLAT) CALL    XIT('INITOZ7',-4)
C
C     * SKIP FURTHER PROCESSING IF MODEL GRID HAS NLATG GAUSSIAN LATITUDES.
C
      IF(ILAT.EQ.NLATG)       THEN
        DO 50 L=1,LEVOZ
        DO 50 J=1,ILAT
          O3(J,L)=OZZXG(J,L)
   50   CONTINUE
        GO TO 170
      ENDIF
C
C     * SET DLATI TO INPUT T42 GAUSSIAN LATITUDES (DEG).
C
      NLATH=NLATG/2
      CALL GAUSSG(NLATH,SLI,WLI,CLI,RADLI,WOSSLI)
      CALL  TRIGL(NLATH,SLI,WLI,CLI,RADLI,WOSSLI)
      DO 100 J=1,NLATG
  100 DLATI(J)=RADLI(J)*180.E0/3.14159E0
C     
C     * ADD EXTRA LATITUDES AT S.P. AND N.P. FOR INTERPOLATION PURPOSES.
C
      ALAT(1)=-90.E0
      ALAT(NLAT)=90.E0
      DO 103 L=1,LEVOZ 
        OZZX(1,L)=OZZXG(1,L)
        OZZX(NLAT,L)=OZZXG(NLATG,L)
  103 CONTINUE   
C
      DO 107 J=1,NLATG
        ALAT(J+1)=DLATI(J)
        DO 105 L=1,LEVOZ 
          OZZX(J+1,L)=OZZXG(J,L)
  105   CONTINUE   
  107 CONTINUE
C
C     * SET DLAT TO GAUSSIAN LATITUDES (DEG).
C
      ILATH=ILAT/2
      CALL GAUSSG(ILATH,SL,WL,CL,RADL,WOSSL)
      CALL  TRIGL(ILATH,SL,WL,CL,RADL,WOSSL)
      DO 110 J=1,ILAT
  110 DLAT(J)=RADL(J)*180.E0/3.14159E0
C
C     * PERFORM INTERPOLATION ON PRESSURE SURFACES TO MODEL GAUSSIAN LATITUDES.
C     * THE ARRAY LBL HOLDS THE POSITION IN THE ARRAY ALAT WHOSE CORRESPONDING
C     * LATITUDE IS CLOSEST TO, BUT SOUTH OF, (OR POSSIBLY EQUAL TO) THE
C     * GAUSSIAN LATITUDE DLAT(J); HENCE ONE VALUE FOR EACH GAUSSIAN LATITUDE.
C     * THE RESULTING ARRAY IS O3(ILAT,LEVOZ).
C
      DO 145 J=1,ILAT
        IB=0
        DO 140 I=1,NLAT
          IF(DLAT(J).LT.ALAT(I).AND.IB.EQ.0) IB=I
  140   CONTINUE
        IF(IB.EQ.0) THEN
          WRITE(6,6030) DLAT(J)
          CALL                                     XIT('INITOZ7',-5)
        ENDIF
        LBL(J)=IB-1
  145 CONTINUE
C
      DO 160 L=1,LEVOZ
      DO 150 J=1,ILAT
        K=LBL(J)
        CALL LININT(ALAT(K),OZZX(K,L),ALAT(K+1),OZZX(K+1,L),DLAT(J),
     1              O3(J,L))
  150 CONTINUE
  160 CONTINUE
C
C     * EVALUATE THE AMOUNT OF OZONE (IN VOLUME MIXING RATIO).
C
  170 CONTINUE  
      CALL SETLAB(JBUF,NC4TO8("ZONL"),NDAY,NC4TO8("  OZ"),0,ILAT,1,0,1)
      DO 200 L=1,LEVOZ
        DO 180 J=1,ILAT
          H(J)=O3(J,L)*1.0E-6
  180   CONTINUE
        JBUF(4)=L
        CALL PUTFLD2(2,H,JBUF,MAXX)
        WRITE(6,6020) JBUF
  200 CONTINUE
  290 CONTINUE
C
C     * ADD EXTRA DATA FOR STARTING DATE (IDAY) OF A NEW MODEL RUN
C     * WHICH IS NOT A MONTH BOUNDARY. TO DO THIS, INTERPOLATE BETWEEN
C     * THE TWO ADJACENT MONTH-BOUNDARY VALUES.
C
      PARTDAY=GMT/24.E0
      RDAY=FLOAT(IDAY)+PARTDAY
      LL=0
      MM=0
      DO 300 N=1,12
        IF(IDAY.GE.NFDM(N)) LL=N
        IF(IDAY.EQ.NFDM(N)) MM=N
  300 CONTINUE
      IF(LL.EQ.0) CALL                             XIT('INITOZ7',-6)

      IF(MM.EQ.0) THEN

        NDAYM=NFDM(LL)
        RFDM=FLOAT(NDAYM)
        IF(LL.LT.12) THEN
          RFDP=FLOAT(NFDM(LL+1))
          NDAYP=NFDM(LL+1)
        ELSE
          RFDP=FLOAT(NFDM(1)+365)
          NDAYP=NFDM(1)
        ENDIF
        WRITE(6,6040) RDAY,NDAYM,NDAYP
C
        DO 500 L=1,LEVOZ

          CALL GETFLD2(-2,HM,NC4TO8("ZONL"),NDAYM,NC4TO8("  OZ"),
     +                                            L,JBUF,MAXX,OK)
          IF(.NOT.OK) CALL                         XIT('INITOZ7',-7)
          IF(L.EQ.1) THEN
            WRITE(6,6020) JBUF
            NPACK=JBUF(8)
          ENDIF
          CALL GETFLD2(-2,HP,NC4TO8("ZONL"),NDAYP,NC4TO8("  OZ"),
     +                                            L,JBUF,MAXX,OK)
          IF(.NOT.OK) CALL                         XIT('INITOZ7',-8)
          IF(L.EQ.1) THEN
            WRITE(6,6020) JBUF
            NPACK=MIN(NPACK,JBUF(8))
          ENDIF
          NWDS=JBUF(5)*JBUF(6)
          DO 400 I=1,NWDS
            CALL LININT(RFDM,HM(I),RFDP,HP(I),RDAY,H(I))
  400     CONTINUE
          JBUF(2)=IDAY
C
C         * POSITION FILE POINTER TO THE END OF THE FILE AND APPEND DATA.
C
C         ISTAT=FSEEK(2,0,2)
  450     READ(2,END=460)
          GOTO 450
  460     BACKSPACE 2
          JBUF(8)=NPACK
          CALL PUTFLD2(2,H,JBUF,MAXX)
          WRITE(6,6020) JBUF

  500   CONTINUE

      ENDIF
C
      CALL                                         XIT('INITOZ7',0)
C
C     * E.O.F. ON FILE ICTL.
C
  902 CALL                                         XIT('INITOZ7',-9)
  903 CALL                                         XIT('INITOZ7',-10)
C--------------------------------------------------------------------
 6010 FORMAT('0 ILEV,ILAT,IDAY,GMT =',3I5,2X,F5.2)
 6020 FORMAT(' ',A4,I10,2X,A4,I10,4I6)
 6030 FORMAT(////,5X,'**** INTERPOLATION ATTEMPTED OUTSIDE THE RANGE OF
     1THE LATITUDE GRID (-90 TO 90 DEG OF LAT) FOR LAT= ',F6.2,////)
 6040 FORMAT('0INTERPOLATING FOR RDAY= ',F6.2,' BETWEEN ',I3,' AND ',
     1           I3,')')
      END
