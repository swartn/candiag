      PROGRAM INITG14                                                   
C     PROGRAM INITG14 (ICTL,   FWAT,   OROGWD,  SOCI,                           I2                                                  
C    1                 FLKU,   FLKR,   FWATN,   LLPHYS,       GGPHYS,           I2
C    2                 INPUT,  OUTPUT                                   )       I2                                                  
C    2        TAPE99=ICTL, TAPE88=FWAT, TAPE77=OROGWD, TAPE66=SOCI,                                                               
C    3        TAPE55=FLKU, TAPE44=FLKR, TAPE33=FWATN,  TAPE1=LLPHYS, TAPE2=GGPHYS,  
C    4        TAPE5=INPUT, TAPE6=OUTPUT)                                                             
C     ----------------------------------------------------------------          I2                                                  
C
C     FEB 15/20 - M. LAZARE. - POST CMIP6 REVISED VERSION:                      I2
C                              - MODIFIED WATER FRATION WRITTEN OUT ("FWATN").  I2
C                              - REMOVED MONTHLY {GT,SICN,SIC} (NOW DONE IN     I2
C                                COUPLER).                                      I2
C                              - ADD IN CAPABILITY OF REDEFINING LAND POINTS    I2      
C                                AS ROCK FOR ISLANDS NOT IN GLC2000 DATASET.    I2
C                              - GETS "NLPTS" FROM GGFILL->HRTOLR5 AND USES     I2
C                                THIS INSTEAD OF "NBADPTS".                     I2
C                              - HRTOLR4->HRTOLR5 WITH EXTRA ARGUMENTS.         I2
C                              - {LONBAD,LATBAD} REMOVED.                       I2
C     NOV 29/16 - M. LAZARE. - NEW VERSION FOR GCM19:                           I2
C                              - READS IN INLAND LAKE INFORMATION FROM FILE     I2
C                                "FLKU" (UNIT 55) AND "FLKR" (UNIT 44).         I2
C                                THIS INCLUDES ZERO (FOR NOW) FIELDS OF         I2
C                                "LLAK" AND "BLAK" FOR DIANA'S LAKE MODEL.      I2
C                              - READS IN (OPTIONALLY) NEMO FRACTIONAL          I2
C                                OCEAN MASK (IBUF(3)="SRCF") AND INCORPORATE    I2
C                                THIS INTO USUAL METHODOLOGY.                   I2
C                                NEW LO-RES ARRAY "FWAT" ADDED.                 I2
C                              - FUDGE-IN SOIL DATA FOR POINTS WHICH AREN'T     I2
C                                ABLE TO BE INITIALIZED IN THE USUAL FASHION,   I2
C                                FOR NOW.                                       I2
C                              - SET MASK=2 WHEN FLKR.NE.0.                     I2
C                              - PREVIOUS REFERENCES TO "LICN" CHANGED TO       I2
C                                "GICN", SINCE "LICN" NOW USED FOR LAKE ICE     I2
C                                CONCENTRATION.                                 I2
C                              - ADD "LIC" AND "LICN" NEW FIELDS TO OUTPUT      I2
C                                FILE. THESE ARE DONE LIKE THEIR SEA-ICE        I2
C                                COUNTERPARTS, EXCEPT THAT THE LAKE MASK IS     I2
C                                USED.                                          I2
C     MAY 26/14 - M. LAZARE. - PREVIOUS VERSION INITG13 FOR GCM18:              I2                                                  
C                              - REMOVE CALCULATION AND WRITING-OUT OF          I2                                                  
C                                FAREPAT (NOW DONE IN INIT12 COMMON DECK).      I2                                                  
C                              - PROCESSES NEW SOIL COLOUR INDEX FILE (SOCI).   I2                                                  
C                                THIS IS DONE AT THE BEGINNING BECAUSE THE      I2                                                  
C                                RESOLUTION IS DIFFERENT FROM THE OTHERS.       I2                                                  
C                              - READS IN {NTLD,NTLK,NTWT,IFLM} FROM            I2                                                  
C                                CONTROL FILE (FROM NEW VERSION ICNTRLD)        I2                                                  
C                                AND USES THESE TO DEFINE NECESSARY FIELDS      I2                                                  
C                                TO SUPPORT GENERALIZED TILING AND FRACTIONAL   I2                                                  
C                                LAND MASK.                                     I2                                                  
C                              - READ IN NEW FIELDS "FLAK" AND "LICN"           I2                                                  
C                                FROM NEW "LPCREV2", CONVERT TO FRACTION        I2                                                  
C                                ("FLAK" AND "LICN", RESPECTIVELY, AND          I2                                                  
C                                OUTPUT TO AN FILE.                             I2                                                  
C                              - ALSO OUTPUT FRACTION OF LAND FROM HIGH-RES     I2                                                  
C                                LAND MASK ("FLND").                            I2                                                  
C                              - "GICN" FIELD USED TO SET GLACIER FLAGS         I2                                                  
C                                (IE FROM GLC2000) INSTEAD OF SOILS DATA FLAG.  I2                                                  
C                              - "FLKU" AND "FLKR" SET TO ZERO IF NTLK=0.       I2                                                  
C                              - ENSURE FRACTIONAL LAKE AND WATER DO NOT        I2                                                  
C                                CO-EXIST, SO THAT THERE IS NO ISSUE WITH       I2                                                  
C                                DEFINING FRACTIONAL ICE IF IT EXISTS.          I2                                                  
C                                IN THAT CASE, THE FRACTIONAL LAKE IS           I2                                                  
C                                ASSIGNED TO OCEAN.                             I2                                                  
C     MAR 24/09 - M. LAZARE.   PREVIOUS VERSION INITG12 FOR GCM15G+                                                                 
C                              - ADDED "PHIS" FIELD PROCESSING.                                                                     
C                              - USE "-1" FOR IBUF(1-4) IN READING "MASK" RECORDS.                                                  
C                              - HIGHER INPUT RESOLUTION UP TO 1/12X1/12.                                                           
C                              - UNUSED FIELDS REMOVED: PHIS,ENV.                                                                   
C                              - SINCE INPUT VEG FIELD DOESN'T HAVE                                                                 
C                                VALUES OVER ANTARCTICA (WE WERE ABLE TO                                                            
C                                FUDGE IN VALUES USING SCRIPPS TOPGORAPHY                                                           
C                                PREVIOUSLY AT 1X1), THE ROUTINE HRTOLR4                                                            
C                                IS TRICKED WITH A TEMPORARY LOW-RES LAND                                                           
C                                MASK WHICH HAS NO LAND THERE; REPRESENTATIVE                                                       
C                                VALUES FOR EACH FIELD ARE PUT BACK THERE                                                           
C                                SUBSEQUENTLY.                                                                                      
C                              - CALLS NEW HRTOLR4, REMOVING IGRID AND                                                              
C                                PASSING IN ADDED WORK ARRAYS: LONM,LONP,                                                           
C                                LATM, LATP, TO CORRECT AREA-AVERAGING AROUND                                                       
C                                EDGES OF LOW-RES GRID SQUARE.                                                                      
C     FEB 20/06 - M. LAZARE. - PREVIOUS VERSION INITG11.                                                                            
C     MAY 14/04 - M. LAZARE. - PREVIOUS VERSION INITG10.                                                                            
C                                                                               I2                                                  
CINITG14 - GRID PHYSICS INITIALIZATION PROGRAM FOR GCM18+               2  1 C  I1                                                  
C                                                                               I3                                                  
CAUTHOR  - M.LAZARE                                                             I3                                                  
C                                                                               I3                                                  
CPURPOSE - CONVERTS HIGH-RESOLUTION LAT-LONG SURFACE PHYSICS GRIDS TO           I3                                                  
C          GAUSSIAN GRIDS.                                                      I3                                                  
C          NOTE - ALL INITIAL SURFACE FIELDS ARE AT 1X1 DEGREE OFFSET           I3                                                  
C                 RESOLUTION. THE GROUND COVER, GROUND TEMPERATURE,             I3                                                  
C                 AMIP2 SST AND SEA-ICE CONCENTRATION, AS WELL AS THE           I3                                                  
C                 SEA ICE FIELDS MUST BE AVAILABLE FOR ALL MONTHS. THE          I3                                                  
C                 SNOW COVER, LIQUID AND FROZEN WATER FIELDS ARE                I3                                                  
C                 REQUIRED FOR ANY MONTH THE MODEL STARTS FROM. THE             I3                                                  
C                 PROGRAM ABORTS IF FCAN, AVW, AIW, LNZ0, LAMX, LAMN,           I3                                                  
C                 CMAS, ROOT, DPTH, SAND OR CLAY ARE                            I3                                                  
C                 MISSING AT START-UP DATE - THESE FIELDS HAVE ONE              I3                                                  
C                 BASIC ANNUAL VALUE.                                           I3                                                  
C                                                                               I3                                                  
C                 AS WELL, THE SURFACE HEIGHT FIELD (ON THE ANALYSIS            I3                                                  
C                 GAUSSIAN GRID) IS OBTAINED FROM THE CONTROL FILE              I3                                                  
C                 (AFTER CONVERTING FROM GEOPOTENTIAL TO HEIGHT) AND            I3                                                  
C                 SAVED ON THE AN FILE FOR DISPLAY PURPOSES.                    I3                                                  
C                                                                               I3                                                  
C                 INITIAL FIELDS AT HIGH DEGREE RESOLUTION ARE EITHER           I3                                                  
C                 AREA-AVERAGED (CONTINUOUS) OR THEIR MOST-FREQUENTLY           I3                                                  
C                 OCCURRING VALUE IN THE MODEL-RESOLUTION GRID SQUARE           I3                                                  
C                 CHOSEN (DISCRETE-VALUED) USING THE SUBROUTINE HRTOLR4.        I3                                                  
C                 THE ONLY DISCRETE FIELD PROCESSED IS A WORK FIELD FOR         I3                                                  
C                 THE SOIL DATA. ALL OTHERS ARE AREA-AVERAGED.                  I3                                                  
C                 SINCE INPUT VEG FIELD DOESN'T HAVE VALUES OVER ANTARCTICA     I3                                                  
C                 (WE WERE ABLE TO FUDGE IN VALUES USING SCRIPPS TOPGORAPHY     I3                                                  
C                 PREVIOUSLY AT 1X1), THE ROUTINE HRTOLR4 IS TRICKED WITH A     I3                                                  
C                 TEMPORARY LOW-RES LAND MASK WHICH HAS NO LAND THERE;          I3                                                  
C                 REPRESENTATIVE VALUES FOR EACH FIELD ARE PUT BACK THERE       I3                                                  
C                 SUBSEQUENTLY.                                                 I3                                                  
C                                                                               I3                                                  
C                 THE SOIL DATA IS TREATED SOMEWHAT UNIQUELY:                   I3                                                  
C                 1. DATA FLAGS IN THE SAND FIELD ARE USED TO                   I3                                                  
C                    DETERMINE HIGH-RES POINTS WHICH ARE,                       I3                                                  
C                    RESPECTIVELY, WATER (-1), ORGANIC                          I3                                                  
C                    SOIL (-2) OR GLACIER ICE (-4). A RESULTING                 I3                                                  
C                    LOW-RES LAND TYPE FIELD ("GL") IS CREATED                  I3                                                  
C                    AND THIS INFORMATION IS INSERTED INTO THE                  I3                                                  
C                    RESULTING LOW-RES SOIL FIELDS (FOR NOW THIS IS NOT.        I3                                                  
C                    DONE FOR ORGANIC SOILS, SINCE NO WETLAND MODEL YET).       I3                                                  
C                 2. A HIGH-RES SOIL/NOSOIL MASK IS GENERATED                   I3                                                  
C                    FROM THE SAND FIELD AND USED IN THE AREA-                  I3                                                  
C                    AVERAGING.                                                 I3                                                  
C                 3. SAND AND CLAY (AND SOIL DEPTH) ARE AREA-AVERAGED           I3                                                  
C                    FROM THE INPUT HIGH-RESOLUTION DATA VIA HRTOLR4.           I3                                                  
C                    BEFORE WRITING OUT THE RESULT, FRACTION IS CONVERTED TO    I3                                                  
C                    PERCENTAGE SINCE THIS IS WHAT IS USED IN CLASS.            I3                                                  
C                    DRAINAGE IS DETERMINED FROM A SWITCH WHICH IS UNITY FOR    I3                                                  
C                    DEPTHS GE 4.1M OTHERWISE ZERO (AT HIGH-RESOLUTION) AND     I3                                                  
C                    THEN AREA-AVERAGED USING HRTOLR4. FOR NOW, ORGANIC         I3                                                  
C                    MATTER IS SET TO ZERO.                                     I3                                                  
C                 4. NEGATIVE-VALUE FLAGS ARE NOT USED IN THE AREA-AVERAGING,   I3                                                  
C                    SO THAT ONLY DATAPOINTS WITH ACTUAL SOIL ARE COUNTED.      I3                                                  
C                    A WEIGHTED AVERAGE OVER THE ACTUAL SOIL THICKNESS          I3                                                  
C                    IS TAKEN, TO ACCOUNT FOR THE PRESENCE OF BEDROCK.          I3                                                  
C                 5. ENSURE CONSISTENCY BETWEEN LAND MASK ("MASK")              I3                                                  
C                    AND SOIL MASK, AT LOW RESOLUTION. TRUE NON-                I3                                                  
C                    LAND POINTS (MASK=0) MUST BE TURNED INTO WATER             I3                                                  
C                    AT LOW RESOLUTION FOR SOIL DATA MANIPULATION.              I3                                                  
C                    IN THE CASE WHERE THE TRUE MASK INDICATES                  I3                                                  
C                    LAND BUT THE SOIL DATA DOESN'T, AN ABORT                   I3                                                  
C                    CONDITION IS GENERATED.                                    I3                                                  
C                                                                               I3                                                  
CINPUT FILES...                                                                 I3                                                  
C                                                                               I3                                                  
C      ICTL   = CONTAINS GLOBAL LAT-LONG GRIDS WHICH ARE READ AND               I3                                                  
C               HORIZONTALLY SMOOTHED TO GLOBAL GAUSSIAN GRIDS. THE LAT-        I3                                                  
C               LONG GRIDS MAY BE IN ANY ORDER ON THIS FILE.                    I3                                                  
C                                                                               I3                                                  
C      FWAT   = NEMO FRACTIONAL MASK.                                           I3                                                  
C                                                                               I3                                                  
C      OROGWD = CONTAINS THE 6 FIELDS (SD,GAM,PSI,ALPH,DELT,SIGX) DIRECTLY AT   I3                                                  
C               THE MODEL RESOLUTION IN SUPPORT OF NEW OROGRAPHIC GWD SCHEME.   I3                                                  
C                                                                               I3                                                  
C      SOCI   = SOIL COLOUR INDEX FILE AND ASSOCIATED MASK AT 720X360.          I3                                                  
C                                                                               I3
C      FLKU   = UNRESOLVED INLAND LAKE INFORMATION FILE.                        I3       
C      FLKR   = RESOLVED   INLAND LAKE INFORMATION FILE.                        I3
C                                                                               I3                                                  
C      LLPHYS = OFFSET LAT-LONG PHYSICS GRIDS:                                  I3                                                  
C                                                                               I3                                                  
C      NOTE THAT IN THE FOLLOWING, A "*" IN COLUMN 4 INDICATES THAT THE         I3                                                  
C      FIELD CONTAINS "ICAN" GRIDS, I.E. ONE FOR EACH VEGETATION TYPE.          I3                                                  
C      TWO ADJACENT STARS INDICATE THAT THE FIELD CONTAINS "ICANP1" =           I3                                                  
C      ICAN+1 GRIDS, I.E. AN EXTRA ONE FOR URBAN. CURRENTLY, ICAN=4.            I3                                                  
C      IN ADDITION, THREE ADJACENT STARTS INDICATE THAT THE FIELD               I3                                                  
C      CONTAINS "IGND" GRIDS, I.E. ONE FOR EACH SOIL LAYER.                     I3                                                  
C      THESE DEFINE THE FOLLOWING CANOPY TYPES:                                 I3                                                  
C                                                                               I3                                                  
C           CLASS 1: TALL CONIFEROUS.                                           I3                                                  
C           CLASS 2: TALL BROADLEAF.                                            I3                                                  
C           CLASS 3: ARABLE AND CROPS.                                          I3                                                  
C           CLASS 4: GRASS, SWAMP AND TUNDRA (I.E. OTHER).                      I3                                                  
C           CLASS 5: URBAN (ONLY FOR FCAN,LNZ0,AVW,AIW).                        I3                                                  
C                                                                               I3                                                  
C      FINALLY, A "T" IN THE OUTPUT VARIABLES INDICATES THAT THE FIELD          I3                                                  
C      HAS MULTIPLE TILES. LAND-ONLY FIELDS HAVE "NTLD" TILES,                  I3                                                  
C      LAKE-ONLY FIELDS HAVE "NTLK" TILES AND WATER/ICE FIELDS HAVE             I3                                                  
C      "NTWT" TILES. THE FIELDS {GT,SNO} EXIST ACROSS ALL TILES AND             I3                                                  
C      THEREFORE HAVE (NLTD+NTLK+NTWT) TILES.                                   I3                                                  
C                                                                               I3                                                  
C      NAME            VARIABLE                   UNITS                         I3                                                  
C      ----            --------                   -----                         I3                                                  
C                                                                               I3                                                  
C  **   AIW   WEIGHTED NEAR-IR CANOPY ALBEDO    PERCENTAGE                      I3                                                  
C      ALLW   NEAR-IR CANOPY ALBEDO (SFC)       PERCENTAGE                      I3                                                  
C      ALSW   VISIBLE CANOPY ALBEDO (SFC)       PERCENTAGE                      I3                                                  
C  **   AVW   WEIGHTED VISIBLE CANOPY ALBEDO    PERCENTAGE                      I3                                                  
C      BLAK   EXTINCTION COEFFICIENT OF LAKE    M-1                             I3                                                  
C  *** CLAY   FRACTION OF CLAY IN SOIL LAYER    FRACTION                        I3                                                  
C  *   CMAS   CANOPY MASS                       KG/M**2                         I3                                                  
C      DPTH   DEPTH OF SOIL                     METRES                          I3                                                  
C  **  FCAN   AREAL FRACTION OF CANOPY TYPE     FRACTION                        I3                                                  
C      FLKU   UNRESOLVED INLAND LAKE FRACTION   FRACTION                        I3                                                  
C      FLKR   RESOLVED   INLAND LAKE FRACTION   FRACTION                        I3
C        GC   LAND MASK                         -1.=LAND,0.=SEA/ICE             I3                                                  
C        GT   GROUND TEMPERATURE (IDAY ONLY)    DEG K                           I3                                                  
C  *   LAMN   MINIMUM LEAF AREA INDEX               -                           I3                                                  
C  *   LAMX   MAXIMUM LEAF AREA INDEX               -                           I3                                                  
C      LDPT   DEPTH OF INLAND LAKE              METRES                          I3                                                  
C      LLAK   LENGTH OF INLAND LAKE             METRES                          I3                                                  
C  **  LNZ0   LN OF SURFACE ROUGHNESS LENGTH        -                           I3                                                  
C      LICN   PORTION OF LAND ICE (GLACIER)     PERCENTAGE                      I3                                                  
C  *   ROOT   CANOPY ROOTING DEPTH              METRES                          I3                                                  
C  *** SAND   FRACTION OF SAND IN SOIL LAYER    FRACTION                        I3                                                  
C       SIC   SEA ICE AMOUNT                    KG/M**2                         I3                                                  
C      SICN   AMIP2 SEA ICE CONCENTRATION       PERCENT                         I3                                                  
C       SNO   SNOW DEPTH (IDAY ONLY)            KG/M**2 = MM WATER DEPTH        I3                                                  
C      SOCI   SOIL COLOUR INDEX                                                 I3                                                  
C       SST   AMIP2 SEA-SURFACE TEMPERATURE     DEGK                            I3                                                  
C  *** TBAR   SOIL TEMPERATURE  (IDAY ONLY)     DEGK                            I3                                                  
C  *** THIC   SOIL FROZEN WATER (IDAY ONLY)     FRACTION OF TOTAL VOLUME        I3                                                  
C  *** THLQ   SOIL LIQUID WATER (IDAY ONLY)     FRACTION OF TOTAL VOLUME        I3                                                  
C                                                                               I3                                                  
COUTPUT FILE...                                                                 I3                                                  
C                                                                               I3                                                  
C      GGPHYS = GAUSSIAN GRID SURFACE PHYSICS FIELDS:                           I3                                                  
C                                                                               I3                                                  
C      NAME            VARIABLE                   UNITS                         I3                                                  
C      ----            --------                   -----                         I3                                                  
C                                                                               I3                                                  
C  **   AIW T WEIGHTED NEAR-IR CANOPY ALBEDO    FRACTION                        I3                                                  
C      ALLW   NEAR-IR CANOPY ALBEDO (SFC)           -                           I3                                                  
C      ALSW   VISIBLE CANOPY ALBEDO (SFC)           -                           I3                                                  
C  **   AVW T WEIGHTED VISIBLE CANOPY ALBEDO    FRACTION                        I3                                                  
C      BLAK   EXTINCTION COEFFICIENT OF LAKE    M-1                             I3                                                  
C  *** CLAY T PERCENTAGE OF CLAY IN SOIL LAYER  PERCENTAGE                      I3                                                  
C  *   CMAS T CANOPY MASS                       KG/M**2                         I3                                                  
C      DPTH T DEPTH OF SOIL                     METRES                          I3                                                  
C      FARE   AREAL FRACTION OF TILE            FRACTION                        I3                                                  
C  **  FCAN T AREAL FRACTION OF CANOPY TYPE     FRACTION                        I3                                                  
C      FLKU   UNRESOLVED INLAND LAKE FRACTION   FRACTION                        I3                                                  
C      FLKR   RESOLVED   INLAND LAKE FRACTION   FRACTION                        I3
C      FLND   AREAL FRACTION OF LAND            FRACTION                        I3                                                  
C      GICN   AREA FRACTION OF LAND GLACIER     FRACTION                        I3                                                  
C        GT   GROUND TEMPERATURE (IDAY ONLY)    DEG K                           I3                                                  
C  *   LAMN T MINIMUM LEAF AREA INDEX               -                           I3                                                  
C  *   LAMX T MAXIMUM LEAF AREA INDEX               -                           I3                                                  
C      LDPT   DEPTH OF INLAND LAKE              METRES                          I3                                                  
C       LIC   LAKE ICE AMOUNT                   KG/M**2                         I3                                                  
C      LICN   AMIP2 LAKE ICE CONCENTRATION      FRACTION                        I3                                                  
C      LLAK   LENGTH OF INLAND LAKE             METRES                          I3                                                  
C  **  LNZ0 T LN OF SURFACE ROUGHNESS LENGTH        -                           I3                                                  
C  *** ORGM T PERCENTAGE OF ORGM IN SOIL LAYER  PERCENTAGE                      I3                                                  
C      PHIS   SFC GEOPOTENTIAL ON MODEL GRID    GPM                             I3                                                  
C  *   ROOT T CANOPY ROOTING DEPTH              METRES                          I3                                                  
C  *** SAND T PERCENTAGE OF SAND IN SOIL LAYER  PERCENTAGE                      I3                                                  
C       SIC   SEA ICE AMOUNT                    KG/M**2                         I3                                                  
C      SICN   AMIP2 SEA ICE CONCENTRATION       FRACTION                        I3                                                  
C       SNO   SNOW DEPTH (IDAY ONLY)            KG/M**2 = MM WATER DEPTH        I3                                                  
C  *** TBAR T SOIL TEMPERATURE  (IDAY ONLY)     DEGK                            I3                                                  
C  *** THIC T SOIL FROZEN WATER (IDAY ONLY)     FRACTION OF TOTAL VOLUME        I3                                                  
C  *** THLQ T SOIL LIQUID WATER (IDAY ONLY)     FRACTION OF TOTAL VOLUME        I3                                                  
C        ZS   SURFACE HEIGHT ON ANALYSIS GRID   METRES                          I3                                                  
C                                                                               I3
CINPUT PARAMETERS...                                                            I5                                                    
C                                                                               I5                                                  
C      FLAK_BND  = VALUE BELOW WHICH LAKE FRACTION IS CONSIDERED TOO SMALL      I5
C                  TO BE CONSIDERED RELEVANT AND THUS FRACTION SET TO ZERO      I5
C-----------------------------------------------------------------------------                                                      
C     * IF NO HIGH-RESOLUTION POINTS ARE FOUND WITHIN A PARTICULAR LOW-                                                             
C     * RESOLUTION GRID SQUARE, EVEN AFTER CONSIDERING INFORMATION FROM                                                             
C     * NEAREST-NEIGHBOUR POINTS ON THE GAUSSIAN GRID, AN ABORT CONDITION                                                           
C     * IS GENERATED.                                                                                                               
                                                                        
C     * HRMASK  IS THE HIGH-RESOLUTION OFFSET GLC2000 LAND MASK (LAND=-1.,ELSE 0.)                                                  
C     * HRMASKS IS THE HIGH-RESOLUTION OFFSET SOIL COLOUR LAND MASK (1/0).                                                         
C                                                                                                                                   
C     * "FWAT" IS THE NEMO FRACTIONAL WATER FIELD.
C     * DIFFERENT PROCESSING IS DONE ON IT TO PRODUCE THE "MASK" FIELD, DEPENDING
C     * ON WHETHER THE INPUT FILE IS THE OLD BINARY LAND MASK OR THE NEMO FRACTIONAL
C     * AREA OF WATER..
C                                                                                                                                   
C     * FLND IS THE FRACTIONAL LAND MASK FIELD OBTAINED BY DOING A STRAIGHTFORWARD                                                  
C     * AVERAGE OF THE HIGH-RESOLUTION LAND MASK. IT IS USED TO "EXTEND" THE                                                        
C     * NORMAL SPECIFIED LOW-RESOLUTION MASK TO ACCOUNT FOR PARTIAL LAND, 
C     * UNRESOLVED LAKE, RESOLVED LAKE AND WATER.                                                                                                                      
C     * MASKL, MASKKU, MASKKR AND MASKW, RESPECTIVELY, ARE THE RESULTING MASKS WHICH ARE                                                     
C     * USED TO DO THE AREA AVERAGE.                                                                                                
C                                                                                                                                   
C     * GH IS GENERALLY THE RESULTING OUTPUT FIELD ARRAY FOR ALL FIELDS.                                                            
                                                                        
C     * GWL IS ALSO USED TO STORE THE AREAL FRACTIONS OF EACH CANOPY                                                                
C     * CLASS, SINCE THESE ARE REQUIRED TO CALCULATE THE AVERAGED                                                                   
C     * VISIBLE AND NEAR-IR ALBEDOES OF EACH CANOPY TYPE.                                                                           
                                                                        
C     * GT IS A LOW-RESOLUTION WORK FIELD USED FOR INTERIM STORAGE OF                                                               
C     * THE TEMPERATURE OF THE FIRST SOIL LAYER.                                                                                    
C                                                                                                                                   
      IMPLICIT REAL (A-H,O-Z),                                          
     +INTEGER (I-N)                                                     
      PARAMETER (NLGX=360*12,NLATX=180*12,IGND=3,
     1           NLOMAX=64980,NHIMAX=NLGX*NLATX,IJB=NHIMAX)             
      INTEGER BI,BJ                                                     
      PARAMETER ( BI=360, BJ=181)                                       
C                                                                                                                                   
      REAL GLL(NHIMAX),HRMASK(NHIMAX),HRMASKS(NHIMAX),                  
     1     GWH(NHIMAX),GWA(NHIMAX),GHR(NHIMAX),DLAY(NHIMAX,3)           
                                                                        
      REAL GH(NLOMAX),GL(NLOMAX),GC(NLOMAX),GWL(NLOMAX),                
     1     GT(NLOMAX),MASK(NLOMAX),GLR(NLOMAX),
     2     DPTH(NLOMAX),MASKL(NLOMAX),MASKKR(NLOMAX),MASKKU(NLOMAX),
     3     MASKW(NLOMAX)       
      REAL FLND(NLOMAX),FWAT(NLOMAX),FLKU(NLOMAX),LDPT(NLOMAX),
     1     LLAK(NLOMAX),BLAK(NLOMAX),FLKR(NLOMAX),GICN(NLOMAX)          
      REAL FTIL(NLOMAX),FARE(NLOMAX),SOCI(NLOMAX)                       
C                                                                                                                                   
      REAL DLAT(BJ), DLON(BI)                                           
      REAL LONM(BI), LONP(BI), LATM(BJ), LATP(BJ)                       
      REAL*8 SL(BJ), CL(BJ), WL(BJ), WOSSL(BJ), RAD(BJ)                 
                                                                        
      LOGICAL OK                                                        
                                                                        
C     * WORK ARRAYS FOR SUBROUTINE GGFILL CALLED IN HRTOLR5.                                                                        
                                                                        
      INTEGER LONB(NLOMAX), LATB(NLOMAX)

C     * WORK ARRAYS FOR WATER-SURROUNDED LAND POINTS WHICH ARE
C     * TO BE "TURNED INTO ROCK". "ZERO_ROCK" IS USED FOR
C     * WATER/LAKE CASES.      

      REAL MASK_ROCK(NLOMAX), ZERO_ROCK(NLOMAX)
C                                                                                                                                   
C     * WORK ARRAYS FOR SUBROUTINE HRTOLR5.                                                                                         
C     * NOTE THAT THE SIZES OF ARRAYS VAL,DIST,LOCAT,IVAL,LOCFST AND                                                                
C     * NMAX REALLY REPRESENT THE NUMBER OF HIGH-RESOLUTION GRID                                                                    
C     * POINTS WITHIN A LOW-RESOLUTION GRID SQUARE. FOR CONVENIENCE                                                                 
C     * SAKE, THEY ARE DIMENSIONED WITH SIZE "NLGX" AND AN ABORT                                                                    
C     * CONDITION IS GENERATED WITHIN THE SUBROUTINE.                                                                               
C                                                                                                                                   
      REAL GCHRTMP(NLGX,NLATX)                                          
      REAL RLON(NLGX), RLAT(NLATX)                                      
                                                                        
      REAL GCLRTMP(361,181)                                             
      INTEGER LATL(181), LATH(181), LONL(361), LONH(361)                
                                                                        
      REAL VAL(NLGX), DIST(NLGX)                                        
      INTEGER LOCAT(NLGX), IVAL(NLGX), LOCFST(NLGX), NMAX(NLGX)         
C                                                                                                                                   
      INTEGER IBUF(8),IDAT(IJB)
      COMMON/ICOM/IBUF,IDAT
                                                                        
      DATA SICN_MIN/0.0E0/,   SICN_CRT/0.175E0/,                        
     &      SST_FRZ/271.16E0/, SIC_MIN/45.0E0/                          
      DATA GTFSW/271.2E0/, DENI/913.E0/                                 
      DATA MAXX,NPGG/IJB,+1/                                            
      DATA RLIM /1.E-8/                                                 
C---------------------------------------------------------------------                                                              
      NFF=10                                                             
      CALL JCLPNT(NFF,99,88,77,66,55,44,33,1,2,5,6)                                
                                                                        
      REWIND 2                                                          
      INTERP=1                             
C                                                                                                                                    
C     * READ INPUT PARAMETERS.
C                                                                                                                                    
      READ(5,5010,END=905) FLAK_BND                                             I4
C                                                                        
C     * READ DAY OF THE YEAR, GAUSSIAN GRID SIZE AND LAND SURFACE SCHEME                                                            
C     * CODE FROM CONTROL FILE.                                                                                                     
                                                                        
      REWIND 99                                                         
      READ(99,END=910) LABL                                             
      READ(99,END=911) LABL,IXX,IXX,ILG,ILAT,IXX,IDAY,IXX,IXX,          
     1                      NTLD,NTLK,NTWT
      ILG1  = ILG+1                                                     
      ILATH = ILAT/2                                                    
      LGG   = ILG1*ILAT                                                 
      WRITE(6,6010) IDAY,ILG1,ILAT                                      
      WRITE(6,6011) NTLD,NTLK,NTWT
                                                                        
C     * GAUSSG COMPUTES THE VALUE OF THE GAUSSIAN LATITUDES AND THEIR                                                               
C     * SINES AND COSINES. TRIGL MAKES THEM GLOBAL (S TO N).                                                                        
                                                                        
      CALL GAUSSG(ILATH,SL,WL,CL,RAD,WOSSL)                             
      CALL  TRIGL(ILATH,SL,WL,CL,RAD,WOSSL)                             
                                                                        
      DO 100 I=1,ILAT                                                   
          DLAT(I) = RAD(I)*180.E0/3.14159E0                             
  100 CONTINUE                                                          
                                                                        
C     * DEFINE LONGITUDE VECTOR FOR GAUSSIAN GRID.                                                                                  
                                                                        
      DGX=360.E0/(REAL(ILG))                                           
      DO 120 I=1,ILG1                                                   
          DLON(I)=DGX*REAL(I-1)                                        
  120 CONTINUE                                                          
                                                                        
      NDAY=0                                                            
C                                                                                                                                   
C     * READ-IN THE NEMO FRACTIONAL WATER ON THE MODEL GAUSSIAN GRID.                                                                  
C                                                                                                                                   
      CALL GETFLD2(88,MASK,-1,-1,-1,-1,IBUF,MAXX,OK)                  
      IF(.NOT.OK) CALL                                XIT('INITG14',-2)   
      WRITE(6,6025) IBUF                                                
C
      IF(IBUF(3).EQ.NC4TO8("SRCF")) THEN
        IF(IBUF(5).NE.ILG1 .OR. IBUF(6).NE.ILAT) CALL XIT('INITG14',-3) 
      ELSE
        CALL                                          XIT('INITG14',-4)
      ENDIF      
C---------------------------------------------------------------------
C     * GET HIGH-RES GLC2000 LAND MASK (-1./0.) INTO ARRAY "HRMASK", FOR USE                                                        
C     * IN PROCESSING SUBSEQUENT FIELDS.                                                                                            
C                                                                                                                                   
      CALL GETFLD2(-1,HRMASK,NC4TO8("GRID"),NDAY,NC4TO8("  GC"),        
     +                                           1,IBUF,MAXX,OK)        
      IF(.NOT.OK) CALL                                XIT('INITG14',-7)   
      WRITE(6,6025) IBUF                                                
      NLG=IBUF(5)                                                       
      NLAT=IBUF(6)                                                      
      LLL=NLG*NLAT    
C---------------------------------------------------------------------                                                              
C
C     * RENAME MASK TO FWAT, SINCE MASK WILL BE SUBSEQUENTLY
C     * REDEFINED. VALUES VERY CLOSE TO UNITY OR ZERO ARE SET TO THESE
C     * TO AVOID SMALL LAND OR WATER FRACTIONS BEING DONE IN THE MODEL.
C
      FLIMIT=0.01
      DO I=1,LGG
        IF(MASK(I).GE.(1.-FLIMIT)) THEN
          FWAT(I)=1.
        ELSE IF(MASK(I).LE.FLIMIT) THEN
          FWAT(I)=0.
        ELSE
          FWAT(I)=MASK(I)
        ENDIF
      ENDDO
C
C     * REMOVE INLAND LAKES IN HR MASK FIELD (TREATED AS WATER) FOR FURTHER
C     * PROCESSING OF **LAND-ONLY** FIELDS.
C
      DO I=1,LLL                                                        
        HRMASK(I)=MIN(0.E0, HRMASK(I))                                  
      ENDDO                                                       
C---------------------------------------------------------------------                                                              
C     * FRACTION OF LAKE (BOTH RESOLVED AND UNRESOLVED).                                                                                                 
C     * SET FRACTION TO ZERO IF NO LAKE TILES.
C
      REWIND 55
      CALL GETFLD2(55,FLKU,-1,-1,-1,-1,IBUF,MAXX,OK)                  
      IF(.NOT.OK) CALL                              XIT('INITG14',-8)   
      WRITE(6,6025) IBUF                                                
C
      REWIND 44
      CALL GETFLD2(44,FLKR,-1,-1,-1,-1,IBUF,MAXX,OK)                  
      IF(.NOT.OK) CALL                              XIT('INITG14',-9)   
      WRITE(6,6025) IBUF  
C                                              
      IF(NTLK.EQ.0) THEN
        DO I=1,LGG
          FLKU(I)=0.
          FLKR(I)=0.                                        
        ENDDO
      ENDIF
C
C     * CONSTRAINTS AND CONSISTENCIES!
C     * THEN OBTAIN FLND.
C
      DO I=1,LGG
        FWAT(I)=MIN(1., MAX(FWAT(I),0.) )
        FLKU(I)=MIN(1., MAX(FLKU(I),0.) )
        FLKR(I)=MIN(1., MAX(FLKR(I),0.) )
        IF(FLKU(I).LT.FLAK_BND)   FLKU(I)=0.
        IF(FLKR(I).LT.FLAK_BND)   FLKR(I)=0.
        FNOL=FLKU(I)+FLKR(I)+FWAT(I)
        IF(FNOL.GT.1.) THEN
          EXCESS=FNOL-1.
          FLKUR=FLKU(I)+FLKR(I)
          FNOLO=FNOL
          FWATO=FWAT(I)
          FLKUO=FLKU(I)
          FLKRO=FLKR(I)
          IF(FLKU(I).GT.0. .AND. FLKR(I).GT.0.) THEN
            FLKR(I)=FLKR(I)*(1.-EXCESS/FLKUR)
            FLKU(I)=FLKU(I)*(1.-EXCESS/FLKUR)
          ELSE IF(FLKU(I).GT.0. .AND. FLKR(I).EQ.0.) THEN
            FLKU(I)=FLKU(I)-EXCESS
          ELSE IF(FLKR(I).GT.0. .AND. FLKU(I).EQ.0.) THEN
            FLKR(I)=FLKR(I)-EXCESS
          ENDIF
          FNOL=FLKU(I)+FLKR(I)+FWAT(I)
          IF(FNOL.GT.1) THEN
            PRINT *, 'FNOLO,FWATO,FLKUO,FLKRO,FNOL,FWAT,FLKU,FLKR = ',
     1                FNOLO,FWATO,FLKUO,FLKRO,FNOL,
     2                FWAT(I),FLKU(I),FLKR(I)
            CALL                                    XIT('INITG14',-10) 
          ENDIF
        ENDIF
        FLND(I)=MAX(0., MIN(1.-FNOL,1.))
      ENDDO
C---------------------------------------------------------------------                                                              
C     * DEPTH,LENGTH AND EXTINCTION COEFFICIENT OF LAKE.                                                                                                 
C
      CALL GETFLD2(55,LDPT,-1,-1,-1,-1,IBUF,MAXX,OK)                  
      IF(.NOT.OK) CALL                              XIT('INITG14',-11)   
      WRITE(6,6025) IBUF                                                
C
      CALL GETFLD2(55,LLAK,-1,-1,-1,-1,IBUF,MAXX,OK)                  
      IF(.NOT.OK) CALL                              XIT('INITG14',-12)   
      WRITE(6,6025) IBUF                                                
C
      CALL GETFLD2(55,BLAK,-1,-1,-1,-1,IBUF,MAXX,OK)                  
      IF(.NOT.OK) CALL                              XIT('INITG14',-13)   
      WRITE(6,6025) IBUF                                                
C
C     * SET THESE TO ZERO IF NO UNRESOLVED LAKE FRACTION.
C
      DO I=1,LGG
        IF(FLKU(I).EQ.0.)   THEN
          LDPT(I)=0.
          LLAK(I)=0.
          BLAK(I)=0.
        ENDIF 
      ENDDO
C---------------------------------------------------------------------
C     * OUTPUT THE OCEAN FRACTION FOR USE.
C     * THIS IS NECESSARY BECAUSE IT LIKELY HAS BEEN REVISED FOR TOLERANCE
C     * LIMITS.      
C
      IBUF(2)=0                                                         
      IBUF(3)=NC4TO8("SRCF")                                            
      IBUF(4)=1                                                         
      IBUF(5)=ILG1
      IBUF(6)=ILAT
      IBUF(8)=1                                                         
      CALL PUTFLD2(33,FWAT,IBUF,MAXX)                                    
      WRITE(6,6026) IBUF                                                      
C---------------------------------------------------------------------  
C     * "FRACLMT" IS THE VALUE BELOW WHICH FRACTIONAL COVER IS IGNORED,                                                              
C     * DUE TO PROBLEMS WITH MIS-MATCHED LAND/OCEAN HIGH-RES MASKS                                                                  
C     * (IE AMIP BC AND IMPLICIT SOILS MASK).                                                                                       
C                                                                                                                                   
      FRACLMT=0.
      DO I=1,LGG                                                      
        IF(FLND(I).GT.FRACLMT) THEN                                   
          MASKL(I)=-1.                                                
        ELSE                                                          
          MASKL(I)=0.                                                 
        ENDIF                                                         
C                                                                                                                                   
        IF(FLKR(I).GT.FRACLMT) THEN                                        
          MASKKR(I)=0.                                                 
        ELSE                                                          
          MASKKR(I)=-1.                                                 
        ENDIF                                                         
C                                                                                                                                   
        IF(FLKU(I).GT.FRACLMT) THEN                                        
          MASKKU(I)=0.                                                 
        ELSE                                                          
          MASKKU(I)=-1.                                                 
        ENDIF   
C                                                                                                                                   
        IF(FWAT(I).GT.FRACLMT) THEN                              
          MASKW(I)=0.                                                 
        ELSE                                                          
          MASKW(I)=-1.                                                
        ENDIF                                                         
      ENDDO                                                           
C---------------------------------------------------------------------                                                              
C     * CALCULATE FRACTION OF GLACIER.                                                                                              
C                                                                                                                                   
      CALL GETFLD2(-1,GLL,NC4TO8("GRID"),0,NC4TO8("LICN"),1,            
     +                                         IBUF,MAXX,OK)            
      IF(.NOT.OK) CALL                              XIT('INITG14',-14)  
      IF(IBUF(5).NE.NLG .OR. IBUF(6).NE.NLAT) CALL  XIT('INITG14',-15)  
      WRITE(6,6025) IBUF
c
c     * initialize array for land grid points which are 
c     * surrounded by water and which will be "turned into rock" are
c     * initialized to be subsequently filled.
c      
      do i=1,lgg
        mask_rock(i)=0.
        zero_rock(i)=0.
      enddo
C
C     * FIRST CALL TO HRTOLR5 HERE IS USED TO GET THE "MASK_ROCK"
C     * ARRAY DEFINED, IF SUCH POINTS EXIST. A DUMMY VALUE FOR
C     * "ROCK_SPVAL" IS USED.
C
      IOPTION=1
      ICHOICE=1
      ROCK_SPVAL=0.
      CALL HRTOLR5(GICN,MASKL,ILG1,ILAT,DLON,DLAT,ILG,                  
     1             LONM,LONP,LATM,LATP,                                 
     2             GLL,HRMASK,NLG,NLAT,IOPTION,ICHOICE,                 
     3             OK,NBADPTS,MASK_ROCK,ROCK_SPVAL,                                    
     4             GCLRTMP,GCHRTMP,RLON,RLAT,LATL,LATH,LONL,LONH,       
     5             VAL,DIST,LOCAT,IVAL,LOCFST,LONB,LATB,NLOMAX,NMAX)
c     
c     * impose rock values for land points which were turned to rock.
c     * abort if there are other trouble points.            
c      
      if(nbadpts.gt.0)                              then
        nbadrock=0
        do n=1,nbadpts
          i=lonb(n)
          j=latb(n)
          ij=(j-1)*ilg1+i
          if(maskl(ij).eq.-1.) then
            nbadrock=nbadrock+1
            mask_rock(ij)=1.
            gicn(ij)=0.
            print *, '0land turned to rock: lon,lat = ',i,j
          else
            write(6,6030) i,j
            CALL                                     XIT('INITG14',-16)  
          endif
        enddo
        if(nbadrock.gt.0) print *,nbadrock, ' land pts turned to rock'
      endif  
c      
c     * add in cyclic longitude. This would have been done in
c     * hrtolr5 if it had completed successfully.
c     * we need to do it here regardless if gll has changed (ie
c     * nbadrock.gt.0).
c
      do j=1,ilat
        jstart=(j-1)*ilg1+1
        jend  = j*ilg1
        gicn(jend)=gicn(jstart)
        mask_rock(jend)=mask_rock(jstart)
        zero_rock(jend)=zero_rock(jstart)
      enddo
C=====================================================================                                                              
C     * SOIL COLOUR INDEX AND MASK.                                                                                                 
C                                                                                                                                   
      CALL GETFLD2(-66,HRMASKS,NC4TO8("GRID"),1,NC4TO8("LNDM"),1,       
     +                                              IBUF,MAXX,OK)       
      IF(.NOT.OK) CALL                               XIT('INITG14',-17)  
      WRITE(6,6025) IBUF                                                
      CALL GETFLD2(66, GLL,    NC4TO8("GRID"),1,NC4TO8("SOCI"),1,       
     +                                              IBUF,MAXX,OK)       
      IF(.NOT.OK) CALL                               XIT('INITG14',-18)  
      WRITE(6,6025) IBUF                                                
C                                                                                                                                   
C     * CONVERT TO USUAL MASK DEFINITION.                                                                                           
C                                                                                                                                   
      MLG=IBUF(5)                                                       
      MLAT=IBUF(6)                                                      
      LLLS=MLG*MLAT                                                     
      DO I=1,LLLS                                                       
        HRMASKS(I)=-1.*HRMASKS(I)                                       
      ENDDO                                                             
C
      IOPTION=3                                                         
      ICHOICE=1
      ROCK_SPVAL=0.
      CALL HRTOLR5(SOCI,MASKL,ILG1,ILAT,DLON,DLAT,ILG,                  
     1             LONM,LONP,LATM,LATP,                                 
     2             GLL,HRMASKS,MLG,MLAT,IOPTION,ICHOICE,                
     3             OK,NBADPTS,MASK_ROCK,ROCK_SPVAL,                                    
     4             GCLRTMP,GCHRTMP,RLON,RLAT,LATL,LATH,LONL,LONH,       
     5             VAL,DIST,LOCAT,IVAL,LOCFST,LONB,LATB,NLOMAX,NMAX)
c
c     * abort if there are other trouble points.      
c
      if(.not.ok)                     then
        do n=1,nbadpts
          i=lonb(n)
          j=latb(n)
          write(6,6030) i,j
        enddo  
        CALL                                        XIT('INITG14',-19)  
      endif
C=====================================================================                                                              
C     * FIELDS FOR IDAY:                                                                                                            
C---------------------------------------------------------------------                                                              
C                                                                                                                                   
C     * SEA-ICE CONCENTRATION (IDAY ONLY).                                                                                          
                                                                        
      CALL GETFLD2(-1,GWH,NC4TO8("GRID"),IDAY,NC4TO8("SICN"),1,         
     +                                            IBUF,MAXX,OK)         
      IF(.NOT.OK) CALL                              XIT('INITG14',-20)  
      IF(IBUF(5).NE.NLG .OR. IBUF(6).NE.NLAT) CALL  XIT('INITG14',-21)  
      WRITE(6,6025) IBUF                                                
C                                                                                                                                   
      DO 256 I=1,LLL                                                    
        GWH(I)=MAX(0.0E0,GWH(I))                                        
        IF(HRMASK(I).EQ.-1.E0) GWH(I)=0.E0                              
  256 CONTINUE                                                          
C                                                                                                                                   
      IOPTION=2                                                         
      ICHOICE=1                                                         
      CALL HRTOLR5(GH,MASKW,ILG1,ILAT,DLON,DLAT,ILG,                    
     1             LONM,LONP,LATM,LATP,                                 
     2             GWH,HRMASK,NLG,NLAT,IOPTION,ICHOICE,                 
     3             OK,NBADPTS,ZERO_ROCK,ROCK_SPVAL,                                    
     4             GCLRTMP,GCHRTMP,RLON,RLAT,LATL,LATH,LONL,LONH,       
     5             VAL,DIST,LOCAT,IVAL,LOCFST,LONB,LATB,NLOMAX,NMAX)           
C                                                                                                                                   
c
c     * abort if there are other trouble points.
c      
      if(.not.ok)                     then
        do n=1,nbadpts
          i=lonb(n)
          j=latb(n)
          write(6,6030) i,j
        enddo  
        CALL                                         XIT('INITG14',-22)  
      endif
C                                                                                                                                   
      DO 257 I=1,LGG                                                    
        GH(I) = MAX (MIN (GH(I), 1.0E0), 0.0E0)                         
        GH(I) = MERGE(0.E0, GH(I), MASKW(I).EQ.-1.E0)                   
  257 CONTINUE                                                          
                                                                        
      CALL SETLAB(IBUF,NC4TO8("GRID"),IDAY,NC4TO8("SICN"),1,            
     +                                     ILG1,ILAT,0,NPGG)            
      CALL PUTFLD2(2,GH,IBUF,MAXX)                                      
      WRITE(6,6026) IBUF                                                
C---------------------------------------------------------------------                                                              
C     * SEA-ICE MASS (IDAY ONLY).                                                                                                   
                                                                        
      CALL GETFLD2(-1,GWH,NC4TO8("GRID"),IDAY,NC4TO8(" SIC"),           
     +                                        1,IBUF,MAXX,OK)           
      IF(.NOT.OK) CALL                              XIT('INITG14',-23)  
      IF(IBUF(5).NE.NLG .OR. IBUF(6).NE.NLAT) CALL  XIT('INITG14',-24)  
      WRITE(6,6025) IBUF                                                
C                                                                                                                                   
      DO 258 I=1,LLL                                                    
        GWH(I)=MAX(0.0E0,GWH(I))                                        
        IF(HRMASK(I).EQ.-1.E0) GWH(I)=0.E0                              
  258 CONTINUE                                                          
C                                                                                                                                   
      IOPTION=2                                                         
      ICHOICE=1                                                         
      CALL HRTOLR5(GH,MASKW,ILG1,ILAT,DLON,DLAT,ILG,                    
     1             LONM,LONP,LATM,LATP,                                 
     2             GWH,HRMASK,NLG,NLAT,IOPTION,ICHOICE,                 
     3             OK,NBADPTS,ZERO_ROCK,ROCK_SPVAL,                                    
     4             GCLRTMP,GCHRTMP,RLON,RLAT,LATL,LATH,LONL,LONH,       
     5             VAL,DIST,LOCAT,IVAL,LOCFST,LONB,LATB,NLOMAX,NMAX)           
C                                                                                                                                   
c
c     * abort if there are other trouble points.
c      
      if(.not.ok)                     then
        do n=1,nbadpts
          i=lonb(n)
          j=latb(n)
          write(6,6030) i,j
        enddo  
        CALL                                         XIT('INITG14',-25)  
      endif
C                                                                                                                                   
      DO 259 I=1,LGG                                                    
        GH(I) = MERGE(0.E0, MAX(0.E0,GH(I)), MASKW(I).EQ.-1.E0)         
  259 CONTINUE                                                          
                                                                        
      CALL SETLAB(IBUF,NC4TO8("GRID"),IDAY,NC4TO8(" SIC"),1,            
     +                                     ILG1,ILAT,0,NPGG)            
      CALL PUTFLD2(2,GH,IBUF,MAXX)                                      
      WRITE(6,6026) IBUF                                                
C-----------------------------------------------------------------------
C     * UNRESOLVED LAKE-ICE CONCENTRATION (IDAY ONLY).                                                                                          
                                                                        
      CALL GETFLD2(-1,GWH,NC4TO8("GRID"),IDAY,NC4TO8("SICN"),1,         
     +                                            IBUF,MAXX,OK)         
      IF(.NOT.OK) CALL                              XIT('INITG14',-26)  
      IF(IBUF(5).NE.NLG .OR. IBUF(6).NE.NLAT) CALL  XIT('INITG14',-27)  
      WRITE(6,6025) IBUF                                                
C                                                                                                                                   
      DO I=1,LLL                                                    
        GWH(I)=MAX(0.0E0,GWH(I))                                        
      ENDDO
C                                                                                                                                   
      IOPTION=0                                                         
      ICHOICE=1                                                         
      CALL HRTOLR5(GH,MASKKU,ILG1,ILAT,DLON,DLAT,ILG,                    
     1             LONM,LONP,LATM,LATP,                                 
     2             GWH,HRMASK,NLG,NLAT,IOPTION,ICHOICE,                 
     3             OK,NBADPTS,ZERO_ROCK,ROCK_SPVAL,                                    
     4             GCLRTMP,GCHRTMP,RLON,RLAT,LATL,LATH,LONL,LONH,       
     5             VAL,DIST,LOCAT,IVAL,LOCFST,LONB,LATB,NLOMAX,NMAX)           
c
c     * abort if there are other trouble points.
c      
      if(.not.ok)                     then
        do n=1,nbadpts
          i=lonb(n)
          j=latb(n)
          write(6,6030) i,j
        enddo  
        CALL                                         XIT('INITG14',-28)  
      endif
C                                                                                                                                   
      DO I=1,LGG                                                    
        GH(I) = MAX (MIN (GH(I), 1.0E0), 0.0E0)                         
        GH(I) = MERGE(0.E0, GH(I), MASKKU(I).EQ.-1.E0)                   
      ENDDO
C
C     * UNRESOLVED LAKE-ICE MASS (IDAY ONLY).                                                                                                   
C                                                                        
      CALL GETFLD2(-1,GWH,NC4TO8("GRID"),IDAY,NC4TO8(" SIC"),           
     +                                        1,IBUF,MAXX,OK)           
      IF(.NOT.OK) CALL                              XIT('INITG14',-29)  
      IF(IBUF(5).NE.NLG .OR. IBUF(6).NE.NLAT) CALL  XIT('INITG14',-30)  
      WRITE(6,6025) IBUF                                                
C                                                                                                                                   
      DO I=1,LLL                                                    
        GWH(I)=MAX(0.0E0,GWH(I))                                        
      ENDDO
C                                                                                                                                   
      IOPTION=0                                                         
      ICHOICE=1                                                         
      CALL HRTOLR5(GL,MASKKU,ILG1,ILAT,DLON,DLAT,ILG,                    
     1             LONM,LONP,LATM,LATP,                                 
     2             GWH,HRMASK,NLG,NLAT,IOPTION,ICHOICE,                 
     3             OK,NBADPTS,ZERO_ROCK,ROCK_SPVAL,                                    
     4             GCLRTMP,GCHRTMP,RLON,RLAT,LATL,LATH,LONL,LONH,       
     5             VAL,DIST,LOCAT,IVAL,LOCFST,LONB,LATB,NLOMAX,NMAX)           
c
c     * abort if there are other trouble points.
c      
      if(.not.ok)                     then
        do n=1,nbadpts
          i=lonb(n)
          j=latb(n)
          write(6,6030) i,j
        enddo  
        CALL                                         XIT('INITG14',-31)  
      endif
C                                                                                                                                   
      DO I=1,LGG                                                    
        GL(I) = MERGE(0.E0, GL(I), MASKKU(I).EQ.-1.E0)                   
      ENDDO
C                                                                                                                                   
C     * UNRESOLVED LAKE TEMPERATURE (IDAY ONLY).                                                                                             
C     * ONLY USED FOR CONSISTENCY CHECK FOR {LICRN,LICR}, **NOT** OUTPUT!
C                                                                                                                                   
      CALL GETFLD2(-1,GWH,NC4TO8("GRID"),IDAY,NC4TO8("  GT"),1,         
     +                                            IBUF,MAXX,OK)         
      IF(.NOT.OK) CALL                              XIT('INITG14',-32)  
      IF(IBUF(5).NE.NLG .OR. IBUF(6).NE.NLAT) CALL  XIT('INITG14',-33)  
      WRITE(6,6025) IBUF                                                
C                                                                                                                                   
      IOPTION=0                                                         
      ICHOICE=1                                                         
      CALL HRTOLR5(GWL,MASKKU,ILG1,ILAT,DLON,DLAT,ILG,                    
     1             LONM,LONP,LATM,LATP,                                 
     2             GWH,HRMASK,NLG,NLAT,IOPTION,ICHOICE,                 
     3             OK,NBADPTS,ZERO_ROCK,ROCK_SPVAL,                                    
     4             GCLRTMP,GCHRTMP,RLON,RLAT,LATL,LATH,LONL,LONH,       
     5             VAL,DIST,LOCAT,IVAL,LOCFST,LONB,LATB,NLOMAX,NMAX)           
c
c     * abort if there are other trouble points.
c      
      if(.not.ok)                     then
        do n=1,nbadpts
          i=lonb(n)
          j=latb(n)
          write(6,6030) i,j
        enddo  
        CALL                                         XIT('INITG14',-34)  
      endif
C
      DO I=1,LGG                                                    
        GWL(I) = MERGE(0.E0, GWL(I), MASKKU(I).EQ.-1.E0)         
      ENDDO
C
      IF(NTLK.GT.0)                                 THEN
C
C       * CONSISTENCY CHECKS BETWEEN {LIUN,LIU,LGTU}.
C                                                 
        DO I=1,LGG
          IF(MASKKU(I).EQ.0.) THEN
            IF(GWL(I).GT.273.16) THEN
              GH (I)=0.
            ELSE
              GH (I)=MAX(GH(I),0.15)
            ENDIF
C
            IF(GH(I).EQ.0.) THEN
              GL (I)=0.
              GWL(I)=MAX(GWL(I),273.16)
            ELSE
              GL (I)=MAX(45.,MIN(GL(I),900.))
              GWL(I)=273.16
            ENDIF
          ENDIF
        ENDDO
       
      ELSE
C
C       * NO UNRESOLVED LAKE FRACTION, SO SET THESE TO ZERO.
C
        DO I=1,LGG
          GH (I)=0.
          GL (I)=0.
          GWL(I)=0.
        ENDDO
      ENDIF
C                       
      CALL SETLAB(IBUF,NC4TO8("GRID"),IDAY,NC4TO8("LUIN"),1,            
     +                                     ILG1,ILAT,0,NPGG)            
      CALL PUTFLD2(2,GH,IBUF,MAXX)                                      
      WRITE(6,6026) IBUF                                                
C
      CALL SETLAB(IBUF,NC4TO8("GRID"),IDAY,NC4TO8("LUIM"),1,            
     +                                     ILG1,ILAT,0,NPGG)            
      CALL PUTFLD2(2,GL,IBUF,MAXX)                                      
      WRITE(6,6026) IBUF                              
C-----------------------------------------------------------------------
C     * RESOLVED LAKE-ICE CONCENTRATION (IDAY ONLY).                                                                                          
                                                                        
      CALL GETFLD2(-1,GWH,NC4TO8("GRID"),IDAY,NC4TO8("SICN"),1,         
     +                                            IBUF,MAXX,OK)         
      IF(.NOT.OK) CALL                              XIT('INITG14',-35)  
      IF(IBUF(5).NE.NLG .OR. IBUF(6).NE.NLAT) CALL  XIT('INITG14',-36)  
      WRITE(6,6025) IBUF                                                
C                                                                                                                                   
      DO I=1,LLL                                                    
        GWH(I)=MAX(0.0E0,GWH(I))                                        
      ENDDO
C                                                                                                                                   
      IOPTION=0                                                         
      ICHOICE=1                                                         
      CALL HRTOLR5(GH,MASKKR,ILG1,ILAT,DLON,DLAT,ILG,                    
     1             LONM,LONP,LATM,LATP,                                 
     2             GWH,HRMASK,NLG,NLAT,IOPTION,ICHOICE,                 
     3             OK,NBADPTS,ZERO_ROCK,ROCK_SPVAL,                                    
     4             GCLRTMP,GCHRTMP,RLON,RLAT,LATL,LATH,LONL,LONH,       
     5             VAL,DIST,LOCAT,IVAL,LOCFST,LONB,LATB,NLOMAX,NMAX)           
c
c     * abort if there are other trouble points.
c      
      if(.not.ok)                     then
        do n=1,nbadpts
          i=lonb(n)
          j=latb(n)
          write(6,6030) i,j
        enddo  
        CALL                                         XIT('INITG14',-37)  
      endif
C                                                                                                                                   
      DO I=1,LGG                                                    
        GH(I) = MAX (MIN (GH(I), 1.0E0), 0.0E0)                         
        GH(I) = MERGE(0.E0, GH(I), MASKKR(I).EQ.-1.E0)                   
      ENDDO
C
C     * RESOLVED LAKE-ICE MASS (IDAY ONLY).                                                                                                   
C                                                                        
      CALL GETFLD2(-1,GWH,NC4TO8("GRID"),IDAY,NC4TO8(" SIC"),           
     +                                        1,IBUF,MAXX,OK)           
      IF(.NOT.OK) CALL                              XIT('INITG14',-38)  
      IF(IBUF(5).NE.NLG .OR. IBUF(6).NE.NLAT) CALL  XIT('INITG14',-39)  
      WRITE(6,6025) IBUF                                                
C                                                                                                                                   
      DO I=1,LLL                                                    
        GWH(I)=MAX(0.0E0,GWH(I))                                        
      ENDDO
C                                                                                                                                   
      IOPTION=0                                                         
      ICHOICE=1                                                         
      CALL HRTOLR5(GL,MASKKR,ILG1,ILAT,DLON,DLAT,ILG,                    
     1             LONM,LONP,LATM,LATP,                                 
     2             GWH,HRMASK,NLG,NLAT,IOPTION,ICHOICE,                 
     3             OK,NBADPTS,ZERO_ROCK,ROCK_SPVAL,                                    
     4             GCLRTMP,GCHRTMP,RLON,RLAT,LATL,LATH,LONL,LONH,       
     5             VAL,DIST,LOCAT,IVAL,LOCFST,LONB,LATB,NLOMAX,NMAX)           
c
c     * abort if there are other trouble points.
c      
      if(.not.ok)                     then
        do n=1,nbadpts
          i=lonb(n)
          j=latb(n)
          write(6,6030) i,j
        enddo  
        CALL                                         XIT('INITG14',-40)  
      endif
C                                                                                                                                   
      DO I=1,LGG                                                    
        GL(I) = MERGE(0.E0, GL(I), MASKKR(I).EQ.-1.E0)                   
      ENDDO
C                                                                                                                                   
C     * RESOLVED LAKE TEMPERATURE (IDAY ONLY).                                                                                             
C     * ONLY USED FOR CONSISTENCY CHECK FOR {LICRN,LICR}, **NOT** OUTPUT!
C                                                                                                                                   
      CALL GETFLD2(-1,GWH,NC4TO8("GRID"),IDAY,NC4TO8("  GT"),1,         
     +                                            IBUF,MAXX,OK)         
      IF(.NOT.OK) CALL                              XIT('INITG14',-41)  
      IF(IBUF(5).NE.NLG .OR. IBUF(6).NE.NLAT) CALL  XIT('INITG14',-42)  
      WRITE(6,6025) IBUF                                                
C                                                                                                                                   
      IOPTION=0                                                         
      ICHOICE=1                                                         
      CALL HRTOLR5(GWL,MASKKR,ILG1,ILAT,DLON,DLAT,ILG,                    
     1             LONM,LONP,LATM,LATP,                                 
     2             GWH,HRMASK,NLG,NLAT,IOPTION,ICHOICE,                 
     3             OK,NBADPTS,ZERO_ROCK,ROCK_SPVAL,                                    
     4             GCLRTMP,GCHRTMP,RLON,RLAT,LATL,LATH,LONL,LONH,       
     5             VAL,DIST,LOCAT,IVAL,LOCFST,LONB,LATB,NLOMAX,NMAX)           
c
c     * abort if there are other trouble points.
c      
      if(.not.ok)                     then
        do n=1,nbadpts
          i=lonb(n)
          j=latb(n)
          write(6,6030) i,j
        enddo  
        CALL                                         XIT('INITG14',-43)  
      endif
C
      DO I=1,LGG                                                    
        GWL(I) = MERGE(0.E0, GWL(I), MASKKR(I).EQ.-1.E0)         
      ENDDO
C
      IF(NTLK.GT.0)                                 THEN
C
C       * CONSISTENCY CHECKS BETWEEN {LIRN,LIR,LGTR}.
C                                                 
        DO I=1,LGG
          IF(MASKKR(I).EQ.0.) THEN
            IF(GWL(I).GT.273.16) THEN
              GH (I)=0.
            ELSE
              GH (I)=MAX(GH(I),0.15)
            ENDIF
C
            IF(GH(I).EQ.0.) THEN
              GL (I)=0.
              GWL(I)=MAX(GWL(I),273.16)
            ELSE
              GL (I)=MAX(45.,MIN(GL(I),900.))
              GWL(I)=273.16
            ENDIF
          ENDIF
        ENDDO
       
      ELSE
C
C       * NO RESOLVED LAKE FRACTION, SO SET THESE TO ZERO.
C
        DO I=1,LGG
          GH (I)=0.
          GL (I)=0.
          GWL(I)=0.
        ENDDO
      ENDIF
C                       
      CALL SETLAB(IBUF,NC4TO8("GRID"),IDAY,NC4TO8("LRIN"),1,            
     +                                     ILG1,ILAT,0,NPGG)            
      CALL PUTFLD2(2,GH,IBUF,MAXX)                                      
      WRITE(6,6026) IBUF                                                
C
      CALL SETLAB(IBUF,NC4TO8("GRID"),IDAY,NC4TO8("LRIM"),1,            
     +                                     ILG1,ILAT,0,NPGG)            
      CALL PUTFLD2(2,GL,IBUF,MAXX)                                      
      WRITE(6,6026) IBUF                                                
C-----------------------------------------------------------------------
C     * GROUND TEMPERATURE (IDAY ONLY).                                                                                             
C                                                                                                                                   
      CALL GETFLD2(-1,GWH,NC4TO8("GRID"),IDAY,NC4TO8("  GT"),1,         
     +                                            IBUF,MAXX,OK)         
      IF(.NOT.OK) CALL                              XIT('INITG14',-44)  
      IF(IBUF(5).NE.NLG .OR. IBUF(6).NE.NLAT) CALL  XIT('INITG14',-45)  
      WRITE(6,6025) IBUF
C                                                                                                                                   
      IOPTION=1                                                         
      ICHOICE=1
      ROCK_SPVAL=273.16
      CALL HRTOLR5(GH,MASKL,ILG1,ILAT,DLON,DLAT,ILG,                    
     1             LONM,LONP,LATM,LATP,                                 
     2             GWH,HRMASK,NLG,NLAT,IOPTION,ICHOICE,                 
     3             OK,NBADPTS,MASK_ROCK,ROCK_SPVAL,                                    
     4             GCLRTMP,GCHRTMP,RLON,RLAT,LATL,LATH,LONL,LONH,       
     5             VAL,DIST,LOCAT,IVAL,LOCFST,LONB,LATB,NLOMAX,NMAX)           
c
c     * abort if there are other trouble points.
c      
      if(.not.ok)                     then
        do n=1,nbadpts
          i=lonb(n)
          j=latb(n)
          write(6,6030) i,j
        enddo  
        CALL                                        XIT('INITG14',-46)  
      endif
C                                                                                                                                   
      CALL SETLAB(IBUF,NC4TO8("GRID"),IDAY,NC4TO8("  GT"),1,            
     +                                     ILG1,ILAT,0,NPGG)            
      CALL PUTFLD2(2,GH,IBUF,MAXX)                                      
      WRITE(6,6026) IBUF                                                
C---------------------------------------------------------------------                                                              
C     * SNOW DEPTH (KG/M**2 WATER EQUIVALENT = MM SNOW).                                                                            
C     * MAKE SURE SNOW IS NOT NEGATIVE.                                                                                             
C     * IDAY ONLY.                                                                                                                  
                                                                        
      CALL GETFLD2(-1,GLL,NC4TO8("GRID"),IDAY,NC4TO8(" SNO"),1,         
     +                                            IBUF,MAXX,OK)         
      IF(.NOT.OK) CALL                              XIT('INITG14',-47)  
      IF(IBUF(5).NE.NLG .OR. IBUF(6).NE.NLAT) CALL  XIT('INITG14',-48)  
      WRITE(6,6025) IBUF                                                
C                                                                                                                                   
      DO 261 I=1,LLL                                                    
        GLL(I)=MAX(0.0E0,GLL(I))                                        
 261  CONTINUE
C                                                                                                                                   
      IOPTION=1                                                         
      ICHOICE=1
      ROCK_SPVAL=0.
      CALL HRTOLR5(GH,MASKL,ILG1,ILAT,DLON,DLAT,ILG,                    
     1             LONM,LONP,LATM,LATP,                                 
     2             GLL,HRMASK,NLG,NLAT,IOPTION,ICHOICE,                 
     3             OK,NBADPTS,MASK_ROCK,ROCK_SPVAL,                                    
     4             GCLRTMP,GCHRTMP,RLON,RLAT,LATL,LATH,LONL,LONH,       
     5             VAL,DIST,LOCAT,IVAL,LOCFST,LONB,LATB,NLOMAX,NMAX)           
c
c     * abort if there are other trouble points.
c      
      if(.not.ok)                     then
        do n=1,nbadpts
          i=lonb(n)
          j=latb(n)
          write(6,6030) i,j
        enddo  
        CALL                                         XIT('INITG14',-49)  
      endif
C                                                                                                                                   
      DO 262 I=1,LGG                                                    
        GH(I)=MAX(0.0E0,GH(I))                                          
  262 CONTINUE                                                          
C                                                                                                                                   
      CALL SETLAB(IBUF,NC4TO8("GRID"),IDAY,NC4TO8(" SNO"),1,            
     +                                     ILG1,ILAT,0,NPGG)            
      CALL PUTFLD2(2,GH,IBUF,MAXX)                                      
      WRITE(6,6026) IBUF                                                
C---------------------------------------------------------------------                                                              
C     * SOIL TEMPERATURES, LIQUID AND FROZEN WATER CONTENTS (IDAY ONLY).                                                            
C     * ZERO OUT THESE OVER NON-LAND POINTS, SINCE FIELD NOT                                                                        
C     * RELEVANT OTHERWISE. NOTE FIELDS ARE UNPACKED (NPGG=1)                                                                       
C     * SO THAT PACKING IS NOT A CONCERN.                                                                                           
C                                                                                                                                   
      DO 269 L=1,IGND                                                   
          CALL GETFLD2(-1,GHR,NC4TO8("GRID"),IDAY,NC4TO8("TBAR"),       
     +                                            L,IBUF,MAXX,OK)       
          IF(.NOT.OK) CALL                           XIT('INITG14',-50)  
          IF(IBUF(5).NE.NLG.OR.IBUF(6).NE.NLAT) CALL XIT('INITG14',-51) 
          WRITE(6,6025) IBUF
C                                                                                                                                   
          IOPTION=1                                                     
          ICHOICE=1
          ROCK_SPVAL=273.16
          CALL HRTOLR5(GT,MASKL,ILG1,ILAT,DLON,DLAT,ILG,                
     1                 LONM,LONP,LATM,LATP,                             
     2                 GHR,HRMASK,NLG,NLAT,IOPTION,ICHOICE,             
     3                 OK,NBADPTS,MASK_ROCK,ROCK_SPVAL,                                
     4                 GCLRTMP,GCHRTMP,RLON,RLAT,LATL,LATH,LONL,LONH,   
     5                 VAL,DIST,LOCAT,IVAL,LOCFST,LONB,LATB,NLOMAX,NMAX)       
c
c         * abort if there are other trouble points.      
c          
          if(.not.ok)                     then
            do n=1,nbadpts
              i=lonb(n)
              j=latb(n)
              write(6,6030) i,j
            enddo  
            CALL                                     XIT('INITG14',-52)  
          endif
C                                                                                                                                   
          CALL GETFLD2(-1,GWH,NC4TO8("GRID"),IDAY,NC4TO8("THLQ"),       
     +                                            L,IBUF,MAXX,OK)       
          IF(.NOT.OK) CALL                           XIT('INITG14',-53)  
          IF(IBUF(5).NE.NLG.OR.IBUF(6).NE.NLAT) CALL XIT('INITG14',-54) 
          WRITE(6,6025) IBUF                                            
C                                                                                                                                   
          DO 266 I=1,LLL                                                
            GWH(I)=MAX(0.0E0,GWH(I))                                    
            IF(HRMASK(I).NE.-1.E0) GWH(I)=0.E0                          
 266      CONTINUE
C                                                                                                                                   
          IOPTION=1                                                     
          ICHOICE=1
          ROCK_SPVAL=0.
          CALL HRTOLR5(GH,MASKL,ILG1,ILAT,DLON,DLAT,ILG,                
     1                 LONM,LONP,LATM,LATP,                             
     2                 GWH,HRMASK,NLG,NLAT,IOPTION,ICHOICE,             
     3                 OK,NBADPTS,MASK_ROCK,ROCK_SPVAL,                                
     4                 GCLRTMP,GCHRTMP,RLON,RLAT,LATL,LATH,LONL,LONH,   
     5                 VAL,DIST,LOCAT,IVAL,LOCFST,LONB,LATB,NLOMAX,NMAX)       
c
c         * abort if there are other trouble points.      
c
          if(.not.ok)                     then
            do n=1,nbadpts
              i=lonb(n)
              j=latb(n)
              write(6,6030) i,j
            enddo  
            CALL                                     XIT('INITG14',-55)  
          endif
C                                                                                                                                   
          CALL GETFLD2(-1,GWA,NC4TO8("GRID"),IDAY,NC4TO8("THIC"),       
     +                                            L,IBUF,MAXX,OK)       
          IF(.NOT.OK) CALL                           XIT('INITG14',-56)  
          IF(IBUF(5).NE.NLG.OR.IBUF(6).NE.NLAT) CALL XIT('INITG14',-57) 
          WRITE(6,6025) IBUF                                            
C                                                                                                                                   
          DO 267 I=1,LLL                                                
            GWA(I)=MAX(0.0E0,GWA(I))                                    
            IF(HRMASK(I).NE.-1.E0) GWA(I)=0.E0                          
 267      CONTINUE
C                                                                                                                                   
          IOPTION=1                                                     
          ICHOICE=1
          ROCK_SPVAL=0.
          CALL HRTOLR5(GL,MASKL,ILG1,ILAT,DLON,DLAT,ILG,                
     1                 LONM,LONP,LATM,LATP,                             
     2                 GWA,HRMASK,NLG,NLAT,IOPTION,ICHOICE,             
     3                 OK,NBADPTS,MASK_ROCK,ROCK_SPVAL,                                
     4                 GCLRTMP,GCHRTMP,RLON,RLAT,LATL,LATH,LONL,LONH,   
     5                 VAL,DIST,LOCAT,IVAL,LOCFST,LONB,LATB,NLOMAX,NMAX)       
c
c         * abort if there are other trouble points.
c          
          if(.not.ok)                     then
            do n=1,nbadpts
              i=lonb(n)
              j=latb(n)
              write(6,6030) i,j
            enddo  
            CALL                                    XIT('INITG14',-58)  
          endif
C                                                                                                                                   
          THLMIN=0.04E0                                                 
          DO 268 I=1,LGG                                                
              IF(MASKL(I).GT.-1.E0) THEN                                
                GT(I)=0.E0                                              
                GH(I)=0.E0                                              
                GL(I)=0.E0                                              
             ELSE
                GT(I)=MAX(GT(I),273.16)
                GH(I)=MAX(GH(I),THLMIN)                                 
                GL(I)=MAX(GL(I),THLMIN)                                 
              ENDIF                                                     
  268     CONTINUE                                                      
C                                                                                                                                   
          DO N=1,NTLD                                                   
            IBUF4=1000*L + N                                            
            CALL SETLAB(IBUF,NC4TO8("GRID"),IDAY,NC4TO8("TBAR"),IBUF4,  
     +                                           ILG1,ILAT,0,NPGG)      
            CALL PUTFLD2(2,GT,IBUF,MAXX)                                
            WRITE(6,6026) IBUF                                          
C                                                                                                                                   
            CALL SETLAB(IBUF,NC4TO8("GRID"),IDAY,NC4TO8("THLQ"),IBUF4,  
     +                                           ILG1,ILAT,0,NPGG)      
            CALL PUTFLD2(2,GH,IBUF,MAXX)                                
            WRITE(6,6026) IBUF                                          
C                                                                                                                                   
            CALL SETLAB(IBUF,NC4TO8("GRID"),IDAY,NC4TO8("THIC"),IBUF4,  
     +                                           ILG1,ILAT,0,NPGG)      
            CALL PUTFLD2(2,GL,IBUF,MAXX)                                
            WRITE(6,6026) IBUF                                          
         ENDDO                                                          
  269 CONTINUE                                                          
C---------------------------------------------------------------------                                                              
C     * INVARIANT FIELDS:                                                                                                           
C     * JUST READ IN PARAMETERS FOR SUB-GRID TOPOG DIRECTLY                                                                         
                                                                        
      CALL GETFLD2(-77,GH,NC4TO8("GRID"),0,NC4TO8("  SD"),1,            
     +                                         IBUF,MAXX,OK)            
      IF(.NOT.OK) CALL                               XIT('INITG14',-59)  
      IF(IBUF(5).NE.ILG1 .OR. IBUF(6).NE.ILAT)  CALL XIT('INITG14',-60)  
      WRITE(6,6025) IBUF                                                
      CALL SETLAB(IBUF,NC4TO8("GRID"),0,NC4TO8("  SD"),1,               
     +                                  ILG1,ILAT,0,NPGG)               
      CALL PUTFLD2(2,GH,IBUF,MAXX)                                      
      WRITE(6,6026) IBUF                                                
                                                                        
      CALL GETFLD2(-77,GH,NC4TO8("GRID"),0,NC4TO8(" GAM"),1,            
     +                                         IBUF,MAXX,OK)            
      IF(.NOT.OK) CALL                               XIT('INITG14',-61)  
      WRITE(6,6025) IBUF                                                
      CALL SETLAB(IBUF,NC4TO8("GRID"),0,NC4TO8(" GAM"),1,               
     +                                  ILG1,ILAT,0,NPGG)               
      CALL PUTFLD2(2,GH,IBUF,MAXX)                                      
      WRITE(6,6026) IBUF                                                
                                                                        
      CALL GETFLD2(-77,GH,NC4TO8("GRID"),0,NC4TO8(" PSI"),1,            
     +                                         IBUF,MAXX,OK)            
      IF(.NOT.OK) CALL                               XIT('INITG14',-62)  
      IF(IBUF(5).NE.ILG1 .OR. IBUF(6).NE.ILAT) CALL  XIT('INITG14',-63)  
      WRITE(6,6025) IBUF                                                
      CALL SETLAB(IBUF,NC4TO8("GRID"),0,NC4TO8(" PSI"),1,               
     +                                  ILG1,ILAT,0,NPGG)               
      CALL PUTFLD2(2,GH,IBUF,MAXX)                                      
      WRITE(6,6026) IBUF                                                
                                                                        
      CALL GETFLD2(-77,GH,NC4TO8("GRID"),0,NC4TO8("ALPH"),1,            
     +                                         IBUF,MAXX,OK)            
      IF(.NOT.OK) CALL                               XIT('INITG14',-64)  
      IF(IBUF(5).NE.ILG1 .OR. IBUF(6).NE.ILAT) CALL  XIT('INITG14',-65)  
      WRITE(6,6025) IBUF                                                
      CALL SETLAB(IBUF,NC4TO8("GRID"),0,NC4TO8("ALPH"),1,               
     +                                  ILG1,ILAT,0,NPGG)               
      CALL PUTFLD2(2,GH,IBUF,MAXX)                                      
      WRITE(6,6026) IBUF                                                
                                                                        
      CALL GETFLD2(-77,GH,NC4TO8("GRID"),0,NC4TO8("DELT"),1,            
     +                                         IBUF,MAXX,OK)            
      IF(.NOT.OK) CALL                               XIT('INITG14',-66)  
      IF(IBUF(5).NE.ILG1 .OR. IBUF(6).NE.ILAT) CALL  XIT('INITG14',-67)  
      WRITE(6,6025) IBUF                                                
      CALL SETLAB(IBUF,NC4TO8("GRID"),0,NC4TO8("DELT"),1,               
     +                                  ILG1,ILAT,0,NPGG)               
      CALL PUTFLD2(2,GH,IBUF,MAXX)                                      
      WRITE(6,6026) IBUF                                                
                                                                        
      CALL GETFLD2(-77,GH,NC4TO8("GRID"),0,NC4TO8("SIGX"),1,            
     +                                         IBUF,MAXX,OK)            
      IF(.NOT.OK) CALL                               XIT('INITG14',-68)  
      IF(IBUF(5).NE.ILG1 .OR. IBUF(6).NE.ILAT) CALL  XIT('INITG14',-69)  
      WRITE(6,6025) IBUF                                                
      CALL SETLAB(IBUF,NC4TO8("GRID"),0,NC4TO8("SIGX"),1,               
     +                                  ILG1,ILAT,0,NPGG)               
      CALL PUTFLD2(2,GH,IBUF,MAXX)                                      
      WRITE(6,6026) IBUF                                                
C---------------------------------------------------------------------                                                              
C     * CREATE A HIGH-RES MASK OF DISCRETE-VALUED FLAGS (-4=GLACIER,                                                                
C     * -3=ROCK, -2=ORGANIC, -1=WATER, OTHERWISE ZERO) FROM DATA IN                                                                 
C     * HIGH-RES SANDINESS FIELD (STORE IN "GWA") AND USER HRTOLR5 TO PRODUCE                                                       
C     * THE RESULTING LOW-RES DISCRETE-VALUED FIELD ("GL") WHICH IS                                                                 
C     * SUBSEQUENTLY INSERTED BACK INTO THE AREA-AVERAGED SAND/CLAY.                                                                
C******************************************************************                                                                 
C     * FOR THIS VERSION, THIS IS NOT DONE FOR ORGANIC SOILS AS                                                                     
C     * THERE IS NO ORGANIC SOIL MODEL YET. SO, HIGH-RES ORGANIC                                                                    
C     * POINTS ARE *NOT* INCLUDED IN THE AREA-AVERAGING TO GET THE                                                                  
C     * PSEUDO HIGH-RES LAND MASK "GHR". FOR LATER VERSIONS WITH                                                                    
C     * AN ORGANIC MODEL, REMOVE THIS RESTRICTION AND THE REST                                                                      
C     * SHOULD FOLLOW (ALSO REMOVE THE CALL TO ABORT IF GL=-2)!!!!                                                                  
C******************************************************************                                                                 
C     * DEFINE A HIGH-RES MASK OF SOIL/NOSOIL AND STORE IN WORK                                                                     
C     * ARRAY "GHR". GHR=-1 IF GWA.NE.-1; OTHERWISE GHR=0.                                                                          
C     * THIS WILL BE SUBSEQUENTLY USED IN THE AREA-AVERAGING FOR                                                                    
C     * SOIL CHARACTERISTICS.                                                                                                       
C                                                                                                                                   
      CALL GETFLD2(-1,GWA,NC4TO8("GRID"),0,NC4TO8("SAND"),1,            
     +                                         IBUF,MAXX,OK)            
      WRITE(6,6025) IBUF                                                
      IF(.NOT.OK) CALL                               XIT('INITG14',-70)  
      IF(IBUF(5).NE.NLG .OR. IBUF(6).NE.NLAT)  CALL  XIT('INITG14',-71)  
C                                                                                                                                   
      DO 555 I=1,LLL                                                    
        GWA(I)=MIN(GWA(I),0.E0)                                         
C ************************************************************                                                                      
C       * REPLACE IF CONDITION BY THE FOLLOWING WHEN AN ORGANIC                                                                     
C       * SOIL MODEL IS IN PLACE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!                                                                   
C                                                                                                                                   
C       IF(GWA(I).EQ.-1.)    THEN                                                                                                   
C ************************************************************                                                                      
        IF(GWA(I).EQ.-1.E0 .OR. GWA(I).EQ.-2.E0)    THEN                
           GHR(I)=0.E0                                                  
        ELSE                                                            
           GHR(I)=-1.E0                                                 
        ENDIF                                                           
  555 CONTINUE 
C                                                                                                                                   
      IOPTION=1                                                         
      ICHOICE=0
      ROCK_SPVAL=-3.
      CALL HRTOLR5(GL,MASKL,ILG1,ILAT,DLON,DLAT,ILG,                    
     1             LONM,LONP,LATM,LATP,                                 
     2             GWA,GHR,NLG,NLAT,IOPTION,ICHOICE,                    
     3             OK,NBADPTS,MASK_ROCK,ROCK_SPVAL,                                    
     4             GCLRTMP,GCHRTMP,RLON,RLAT,LATL,LATH,LONL,LONH,       
     5             VAL,DIST,LOCAT,IVAL,LOCFST,LONB,LATB,NLOMAX,NMAX)           
c
c     * abort if there are other trouble points.
c      
c     if(.not.ok)                     then
c       do n=1,nbadpts
c         i=lonb(n)
c         j=latb(n)
c         write(6,6030) i,j
c       enddo  
c       CALL                                      XIT('INITG14',-19)  
c     endif
      if(.not.ok)                     then
          do n=1,nbadpts
            i=lonb(n)
            j=latb(n)
            ij=(j-1)*ilg1+i
            if(gicn(ij).gt.0.4) then
             gl(ij)=-4.
            else if(maskl(ij).eq.0.) then
             gl(ij)=-1.
            else
             gl(ij)=0.
            endif
          enddo
c
c         * add in cyclic longitude, which would have been done in                                                                                                  
c         * hrtolr4 if it had completed successfully.                                                                                                                
c                                                                                                                                                                       
          do j=1,ilat
            jstart=(j-1)*ilg1+1
            jend  = j*ilg1
            gl(jend)=gl(jstart)
         enddo
      endif         
C                                                                                                                                  
C     * THIS IS NEW: SET GL=-4. IF GICN>0.4. ALSO, IF GICN SAYS                                                                     
C     * IT IS NON-GLACIER BUT THE OLD GLACIER MASK FROM THE SAND                                                                    
C     * FIELD SAYS IT IS, WE SET THESE POINTS TO ROCK (GL=-3.).                                                                     
C                                                                                                                                   
C     * ENSURE CONSISTENCY BETWEEN LAND MASK ("MASK") AND SOIL                                                                      
C     * MASK, AT LOW RESOLUTION. TRUE NON-LAND POINTS (MASK=0)                                                                      
C     * MUST BE TURNED INTO WATER AT LOW RESOLUTION FOR SOIL                                                                        
C     * DATA MANIPULATION.                                                                                                          
C     * SET LOW-RES VALUES IN "GL" TO -4 S OF 62S IF MASK INDICATES                                                                 
C     * LAND, FOR CONSISTENCY. THIS IS NECESSARY BECAUSE SOME                                                                       
C     * WATER POINTS WERE MANUALLY CHANGED TO GLACIER BY HAND TO                                                                    
C     * SATISFY THE OCEAN MODEL REQUIREMENTS IN PRODUCING THE                                                                       
C     * LOW-RES LAND MASK.                                                                                                          
C     **********************************************************                                                                    
C     * IN THE CASE WHERE THE TRUE MASK INDICATES LAND BUT                                                                          
C     * THE SOIL DATA DOESN'T, THE SOIL DATA IS CHANGED TO                                                                          
C     * "LAND" (NOT GLACIER OR ORGANIC - SO THE PRINTOUT MUST                                                                       
C     * BE CHECKED CAREFULLY!!) AND A WARNING MESSAGE IS GENERATED.                                                                 
C     **********************************************************                                                                    
C     * DEFINE A LOW-RES MASK OF SOIL/NOSOIL AND STORE IN WORK                                                                      
C     * ARRAY "GLR". GLR=-1 IF GL.NE.-1; OTHERWISE GLR=0.                                                                           
C     * THIS WILL BE SUBSEQUENTLY USED IN THE AREA-AVERAGING FOR                                                                    
C     * SOIL CHARACTERISTICS.                                                                                                       
C                                                                                                                                   
C     * WE ENSURE LOOP 558 DOES NOT VECTORIZE, BECAUSE THE                                                                          
C     * COMPILER HAS PROBLEMS WITH RANGE OF "DLAT"'                                                                                 
C                                                                                                                                   
*vdir novector                                                          
      DO 558 I=1,LGG
        IF(MASKL(I).EQ.0.E0) GL(I)=-1.E0
        IF(GICN(I).GE.0.4)   GL(I)=-4.E0         
        LAT=(I-1)/ILG1+1                                                
        LON=I-(LAT-1)*ILG1                                              
        IF(GL(I).EQ.-4. .AND. GICN(I).LT.0.4) THEN                      
          PRINT *, '0I,LON,LAT,GL,GICN = ',I,LON,LAT,GL(I),GICN(I)      
          GL(I)=-3.                                                     
        ENDIF                                                           
        IF(GL(I).NE.-1.E0)    THEN                                      
           GLR(I)=-1.E0                                                 
        ELSE                                                            
           GLR(I)=0.E0                                                  
        ENDIF                                                           
        IF(MASKL(I).EQ.-1.E0 .AND. GLR(I).EQ.0.E0)          THEN        
           WRITE(6,6040) I,LON,LAT,MASKL(I),GLR(I),GL(I)                
           GLR(I)=-1.E0                                                 
           GL(I)=0.E0                                                   
        ENDIF                                                           
        IF(GL(I).EQ.-2.E0)                               THEN           
           WRITE(6,6045) I,LON,LAT,MASKL(I),GL(I)                       
        ENDIF                                                           
        IF(GL(I).EQ.-3.E0)                               THEN           
           WRITE(6,6050) I,LON,LAT,MASKL(I),GL(I)                       
        ENDIF                                                           
  558 CONTINUE                                                          
C---------------------------------------------------------------------                                                              
C     * SOIL DEPTH (METRES) AND DRAINAGE (SCALE OF 0-1).                                                                            
C                                                                                                                                   
C     * USE HIGH-RES INFORMATION OF SOIL DEPTH TO SET A MASK OF 0/1 FOR                                                             
C     * DRAINAGE AT HIGH RESOLUTION (DRN=0 FOR DPTH<4.1M, OTRW DRN=1)                                                               
C     * AND THEN AREA-AVERAGE. USE WORK FIELD GWH TO STORE 0/1 VALUES                                                               
C     * AT HIGH-RESOLUTION AND GWL FOR RESULTING LOW-RES FIELD.                                                                     
C     * INSERT BACK IN FLAGS AT LOW RESOLUTION AFTERWARDS.                                                                          
C     * STORE BACK RESULTING LOW-RESOLUTION DEPTH INTO ARRAY "DPTH"                                                                 
C     * TO RE-SET CONSISTENT ROCK FLAG (-3) USED IN MODEL.                                                                          
                                                                        
      CALL GETFLD2(-1,GLL,NC4TO8("GRID"),0,NC4TO8("DPTH"),1,            
     +                                         IBUF,MAXX,OK)            
      IF(.NOT.OK) CALL                               XIT('INITG14',-72)  
      IF(IBUF(5).NE.NLG .OR. IBUF(6).NE.NLAT) CALL   XIT('INITG14',-73)  
      WRITE(6,6025) IBUF                                                
C                                                                                                                                   
      DO 560 I=1,LLL                                                    
        GLL(I)=MAX(GLL(I),0.E0)                                         
        IF(GLL(I).LT.4.1E0) THEN                                        
          GWH(I)=0.E0                                                   
        ELSE                                                            
          GWH(I)=1.E0                                                   
        ENDIF                                                           
  560 CONTINUE 
C
      IOPTION=1                                                         
      ICHOICE=1
      ROCK_SPVAL=0.
      CALL HRTOLR5(GH,GLR,ILG1,ILAT,DLON,DLAT,ILG,                      
     1             LONM,LONP,LATM,LATP,                                 
     2             GLL,GHR,NLG,NLAT,IOPTION,ICHOICE,                    
     3             OK,NBADPTS,MASK_ROCK,ROCK_SPVAL,                                    
     4             GCLRTMP,GCHRTMP,RLON,RLAT,LATL,LATH,LONL,LONH,       
     5             VAL,DIST,LOCAT,IVAL,LOCFST,LONB,LATB,NLOMAX,NMAX)           
c
c     * abort if there are other trouble points.
c      
c     if(.not.ok)                     then
c       do n=1,nbadpts
c         i=lonb(n)
c         j=latb(n)
c         write(6,6030) i,j
c       enddo  
c       CALL                                         XIT('INITG14',-74)  
c     endif
      if(.not.ok)                     then
        do n=1,nbadpts
          i=lonb(n)
          j=latb(n)
          ij=(j-1)*ilg1+i
          if(gicn(ij).gt.0.4) then
            gh(ij)=-4.
          else if(maskl(ij).eq.0.) then
            gh(ij)=0.
          else
            gh(ij)=0.
          endif
        enddo
c
c       * add in cyclic longitude, which would have been done in
c       * hrtolr4 if it had completed successfully.                                                                                                                
c                                                                                                                                                                       
        do j=1,ilat
          jstart=(j-1)*ilg1+1
          jend  = j*ilg1
          gh(jend)=gh(jstart)
        enddo
      endif  
C
      IOPTION=1                                                         
      ICHOICE=1
      ROCK_SPVAL=0.
      CALL HRTOLR5(GWL,GLR,ILG1,ILAT,DLON,DLAT,ILG,                     
     1             LONM,LONP,LATM,LATP,                                 
     2             GWH,GHR,NLG,NLAT,IOPTION,ICHOICE,                    
     3             OK,NBADPTS,MASK_ROCK,ROCK_SPVAL,                                    
     4             GCLRTMP,GCHRTMP,RLON,RLAT,LATL,LATH,LONL,LONH,       
     5             VAL,DIST,LOCAT,IVAL,LOCFST,LONB,LATB,NLOMAX,NMAX)           
c
c     * abort if there are other trouble points.      
c
c     if(.not.ok)                     then
c       do n=1,nbadpts
c         i=lonb(n)
c         j=latb(n)
c         write(6,6030) i,j
c       enddo  
c       CALL                                         XIT('INITG14',-75)  
c     endif
      
      if(.not.ok)                     then
        do n=1,nbadpts
          i=lonb(n)
          j=latb(n)
          ij=(j-1)*ilg1+i
          if(gicn(ij).gt.0.4) then
            gwl(ij)=-4.
          else if(maskl(ij).eq.0.) then
            gwl(ij)=0.
          else
            gwl(ij)=0.
          endif
        enddo
c
c       * add in cyclic longitude, which would have been done in                                                                                                  
c       * hrtolr4 if it had completed successfully.                                                                                                                
c                                                                                                                                                                       
        do j=1,ilat
          jstart=(j-1)*ilg1+1
          jend  = j*ilg1
          gwl(jend)=gwl(jstart)
        enddo
      endif 
C                                                                                                                                   
      DO 570 I=1,LGG                                                    
        IF(GL(I).LT.0.E0) THEN                                          
         GH (I)=GL(I)                                                   
         GWL(I)=GL(I)                                                   
        ELSE                                                            
         GWL(I) = MAX (MIN (GWL(I), 1.0E0), 0.E0)                       
         GH(I)  = MAX (GH(I), 0.E0)                                     
         IF(GH(I)-0.10E0 .GE. 0.E0 .AND. GH(I)-0.10E0 .LT.RLIM) THEN    
           GH(I)=0.10E0                                                 
         ELSE                                                           
           IF(GH(I)-0.35E0 .GE. 0.E0 .AND. GH(I)-0.35E0 .LT.RLIM)       
     &        GH(I)= 0.35E0                                             
         ENDIF                                                          
        ENDIF                                                           
        DPTH(I)=GH(I)                                                   
  570 CONTINUE             
C                                                                        
      DO N=1,NTLD                                                       
        CALL SETLAB(IBUF,NC4TO8("GRID"),0,NC4TO8("DPTH"),N,             
     +                                    ILG1,ILAT,0,NPGG)             
        CALL PUTFLD2(2,GH,IBUF,MAXX)                                    
        WRITE(6,6026) IBUF                                              
C                          
        IBUF(3)=NC4TO8(" DRN")                                          
        CALL PUTFLD2(2,GWL,IBUF,MAXX)                                   
        WRITE(6,6026) IBUF                                              
C                                              
        IBUF(3)=NC4TO8("SOCI")                                          
        CALL PUTFLD2(2,SOCI,IBUF,MAXX)                                  
      ENDDO                                                             
C                                                                                                                                   
C     * STORE THE DEPTHS OF THE SOIL LAYERS INTO DLAY AND REDUCE                                                                    
C     * TO THE GAUSSIAN GRID FOR SUBSEQUENT USE IN DETERMINING                                                                      
C     * LAYER SAND/CLAY FRACTIONS (IN WORK ARRAY "GT").                                                                             
C                                                                                                                                   
      DO 580 I=1,LLL                                                    
        IF(GLL(I).GE.0.35E0)        THEN                                
           DLAY(I,1)=0.10E0                                             
           DLAY(I,2)=0.25E0                                             
           DLAY(I,3)=GLL(I)-0.35E0                                      
        ELSE IF(GLL(I).GE.0.10E0 .AND. GLL(I).LT.0.35E0) THEN           
           DLAY(I,1)=0.10E0                                             
           DLAY(I,2)=GLL(I)-0.10E0                                      
           DLAY(I,3)=0.E0                                               
        ELSE IF(GLL(I).GE.0.E0 .AND. GLL(I).LT.0.10E0) THEN             
           DLAY(I,1)=GLL(I)                                             
           DLAY(I,2)=0.E0                                               
           DLAY(I,3)=0.E0                                               
        ELSE                                                            
           DLAY(I,1)=0.E0                                               
           DLAY(I,2)=0.E0                                               
           DLAY(I,3)=0.E0                                               
        ENDIF                                                           
  580 CONTINUE                                                          
C---------------------------------------------------------------------                                                              
C     * SOIL: SAND, CLAY AND ORGANIC MATTER.                                                                                        
C                                                                                                                                   
C     * FOR NOW, ORGANIC MATTER IS SET TO ZERO.                                                                                     
C                                                                                                                                   
C     * LOOP OVER "IGND" SOIL LAYERS. USE GLL/GH FOR SAND AND GWH/GWL                                                               
C     * FOR CLAY.                                                                                                                   
C     * THE FIRST TWO SOIL LAYERS ARE AVERAGED AS USUAL, BASED ON                                                                   
C     * SOIL MASK (GHR/GLR).                                                                                                        
C     * A WEIGHTED AVERAGE BASED ON THE LAYER THICKNESS IS USED, SINCE                                                              
C     * PART OF THE LAYER MAY BE BEDROCK.                                                                                           
C     * CONVERT TO PERCENTAGE AND STORE BACK IN LOW-RESOLUTION FLAGS                                                                
C     * AT END.                                                                                                                     
C                                                                                                                                   
      DO 620 K = 1,IGND                                                 
C                                                                                                                                   
C         * FIRST AREA-AVERAGE THE SOIL DEPTH FOR THE LAYER.                                                                        
C
C                                                                                                                                   
          IOPTION=1                                                     
          ICHOICE=1
          ROCK_SPVAL=0.
          CALL HRTOLR5(GT,GLR,ILG1,ILAT,DLON,DLAT,ILG,                  
     1                 LONM,LONP,LATM,LATP,                             
     2                 DLAY(1,K),GHR,NLG,NLAT,IOPTION,ICHOICE,          
     3                 OK,NBADPTS,MASK_ROCK,ROCK_SPVAL,                                
     4                 GCLRTMP,GCHRTMP,RLON,RLAT,LATL,LATH,LONL,LONH,   
     5                 VAL,DIST,LOCAT,IVAL,LOCFST,LONB,LATB,NLOMAX,NMAX)       
c
c         * abort if there are other trouble points.
c
c         if(.not.ok)                     then
c           do n=1,nbadpts
c             i=lonb(n)
c             j=latb(n)
c             write(6,6030) i,j
c           enddo  
c           CALL                                     XIT('INITG14',-76)  
c         endif
c
          if(.not.ok)                     then
            do n=1,nbadpts
              i=lonb(n)
              j=latb(n)
              ij=(j-1)*ilg1+i
              if(gicn(ij).gt.0.4) then
                gt(ij)=-4.
              else if(maskl(ij).eq.0.) then
                gt(ij)=0.
              else
                if(k.eq.1) then
                  gt(ij)=0.10
                else if(k.eq.2) then
                  gt(ij)=0.25
                else
                  gt(ij)=0.65
                endif
              endif
            enddo
c
c           * add in cyclic longitude, which would have been done in                                                                                                  
c           * hrtolr4 if it had completed successfully.                                                                                                                
c                                                                                                                                                                       
            do j=1,ilat
              jstart=(j-1)*ilg1+1
              jend  = j*ilg1
              gt(jend)=gt(jstart)
            enddo
          endif          
C                                                                                                                                   
C         * FIRST GET HIGH-RES FIELDS.                                                                                              
C                                                                                                                                   
          CALL GETFLD2(-1,GLL,NC4TO8("GRID"),0,NC4TO8("SAND"),K,        
     +                                             IBUF,MAXX,OK)        
          IF(.NOT.OK) CALL                           XIT('INITG14',-74)  
          IF(IBUF(5).NE.NLG.OR.IBUF(6).NE.NLAT) CALL XIT('INITG14',-75)  
          WRITE(6,6025) IBUF                                            
                                                                        
          CALL GETFLD2(-1,GWH,NC4TO8("GRID"),0,NC4TO8("CLAY"),K,        
     +                                             IBUF,MAXX,OK)        
          IF(.NOT.OK) CALL                           XIT('INITG14',-76)  
          WRITE(6,6025) IBUF                                            
C                                                                                                                                   
C         * MULTIPLY THE ABOVE FIELDS BY THE HIGH-RES DEPTH TO BE                                                                   
C         * SUBSEQUENTLY NORMALIZED.                                                                                                
C                                                                                                                                   
          DO 585 I=1,LLL                                                
             GLL(I)=MAX(GLL(I),0.E0)                                    
             GWH(I)=MAX(GWH(I),0.E0)                                    
             GLL(I)=GLL(I)*DLAY(I,K)                                    
             GWH(I)=GWH(I)*DLAY(I,K)                                    
  585     CONTINUE                                                      
C                                                                                                                                   
C         * DO THE AREA-AVERAGING.                                                                                                  
C
          IOPTION=1                                                     
          ICHOICE=1
          ROCK_SPVAL=0.
          CALL HRTOLR5(GH,GLR,ILG1,ILAT,DLON,DLAT,ILG,                  
     1                 LONM,LONP,LATM,LATP,                             
     2                 GLL,GHR,NLG,NLAT,IOPTION,ICHOICE,                
     3                 OK,NBADPTS,MASK_ROCK,ROCK_SPVAL,                                
     4                 GCLRTMP,GCHRTMP,RLON,RLAT,LATL,LATH,LONL,LONH,   
     5                 VAL,DIST,LOCAT,IVAL,LOCFST,LONB,LATB,NLOMAX,NMAX)
c          
c         * use default if there are other trouble points.
c
c         if(.not.ok)                     then
c           do n=1,nbadpts
c             i=lonb(n)
c             j=latb(n)
c             write(6,6030) i,j
c           enddo  
c           CALL                                     XIT('INITG14',-76)  
c         endif
c
          if(.not.ok)                     then
            do n=1,nbadpts
              i=lonb(n)
              j=latb(n)
              ij=(j-1)*ilg1+i
              if(gicn(ij).gt.0.4) then
                gh(ij)=-4.
              else if(maskl(ij).eq.0.) then
                gh(ij)=0.
              else
                if(k.eq.1) then
                  gh(ij)=0.5*0.10
                else if(k.eq.2) then
                  gh(ij)=0.5*0.25
                else
                  gh(ij)=0.5*0.65
                endif
              endif
            enddo
c
c           * add in cyclic longitude, which would have been done in                                                                                                  
c           * hrtolr4 if it had completed successfully.                                                                                                                
c                                                                                                                                                                       
            do j=1,ilat
              jstart=(j-1)*ilg1+1
              jend  = j*ilg1
              gh(jend)=gh(jstart)
            enddo
          endif          
C
          IOPTION=1                                                     
          ICHOICE=1
          ROCK_SPVAL=0.
          CALL HRTOLR5(GWL,GLR,ILG1,ILAT,DLON,DLAT,ILG,                 
     1                 LONM,LONP,LATM,LATP,                             
     2                 GWH,GHR,NLG,NLAT,IOPTION,ICHOICE,                
     3                 OK,NBADPTS,MASK_ROCK,ROCK_SPVAL,                                
     4                 GCLRTMP,GCHRTMP,RLON,RLAT,LATL,LATH,LONL,LONH,   
     5                 VAL,DIST,LOCAT,IVAL,LOCFST,LONB,LATB,NLOMAX,NMAX)
c          
c         * use default if there are other trouble points.
c
c         if(.not.ok)                     then
c           do n=1,nbadpts
c             i=lonb(n)
c             j=latb(n)
c             write(6,6030) i,j
c           enddo  
c           CALL                                     XIT('INITG14',-76)  
c         endif
c
          if(.not.ok)                     then
            do n=1,nbadpts
              i=lonb(n)
              j=latb(n)
              ij=(j-1)*ilg1+i
              if(gicn(ij).gt.0.4) then
                gwl(ij)=-4.
              else if(maskl(ij).eq.0.) then
                gwl(ij)=-0.
              else
                if(k.eq.1) then
                  gwl(ij)=0.5*0.10
                else if(k.eq.2) then
                  gwl(ij)=0.5*0.25
                else
                  gwl(ij)=0.5*0.65
                endif
              endif
            enddo
 
c           * add in cyclic longitude, which would have been done in                                                                                                  
c           * hrtolr4 if it had completed successfully.                                                                                                                
c                                                                                                                                                                       
            do j=1,ilat
              jstart=(j-1)*ilg1+1
              jend  = j*ilg1
              gwl(jend)=gwl(jstart)
            enddo
          endif          
C                                                                                                                                   
C         * DIVIDE THE ABOVE FIELDS BY THE AVERAGE LAYER DEPTH TO                                                                   
C         * COMPLETE THE NORMALIZATION.                                                                                             
C         * CONVERT TO PERCENTAGE AND INSERT BACK IN FLAGS FOR ICE/WATER.                                                           
C                                                                                                                                   
          DO 595 I=1,LGG                                                
             IF(GT(I).GT.0.E0) THEN                                     
               GH (I)=(GH (I)/GT(I))*100.E0                             
               GWL(I)=(GWL(I)/GT(I))*100.E0                             
             ELSE                                                       
               GH (I)=-3.E0                                             
               GWL(I)=-3.E0                                             
             ENDIF                                                      
             IF(DPTH(I).GT.0.10E0 .AND. DPTH(I).LE.0.35E0) THEN         
                IF(K.EQ.3) THEN                                         
                   GH (I)=-3.E0                                         
                   GWL(I)=-3.E0                                         
                ENDIF                                                   
             ELSE IF(DPTH(I).GT.0.E0 .AND. DPTH(I).LE.0.10E0) THEN      
                IF(K.GE.2) THEN                                         
                   GH (I)=-3.E0                                         
                   GWL(I)=-3.E0                                         
                ENDIF                                                   
             ELSE IF(DPTH(I).LE.0.E0) THEN                              
                   GH (I)=-3.E0                                         
                   GWL(I)=-3.E0                                         
             ENDIF                                                      
             IF(GL(I).LT.0.E0) THEN                                     
               GH (I)=GL(I)                                             
               GWL(I)=GL(I)                                             
             ENDIF                                                      
  595     CONTINUE                                                      
C                                                                                                                                   
          DO N=1,NTLD                                                   
            IBUF4=1000*K + N                                            
            CALL SETLAB(IBUF,NC4TO8("GRID"),0,NC4TO8("SAND"),IBUF4,     
     +                                        ILG1,ILAT,0,NPGG)         
            CALL PUTFLD2(2,GH,IBUF,MAXX)                                
            WRITE(6,6026) IBUF                                          
          ENDDO                                                         
C                                                                                                                                   
          DO N=1,NTLD                                                   
            IBUF4=1000*K + N                                            
            CALL SETLAB(IBUF,NC4TO8("GRID"),0,NC4TO8("CLAY"),IBUF4,     
     +                                        ILG1,ILAT,0,NPGG)         
            CALL PUTFLD2(2,GWL,IBUF,MAXX)                               
            WRITE(6,6026) IBUF                                          
          ENDDO                                                         
C                                                                                                                                   
C         * FOR NOW, STORE ORGANIC FRACTION AS ZERO, PUTTING BACK                                                                   
C         * IN FLAGS FOR ICE/WATER/ROCK.                                                                                            
C                                                                                                                                   
          DO 618 I = 1, LGG                                             
              GH(I)=0.E0                                                
              IF(GT(I).LE.0.E0) THEN                                    
                 GH (I)=-3.E0                                           
              ELSE IF(GL(I).LT.0.E0) THEN                               
                 GH (I)=GL(I)                                           
              ENDIF                                                     
  618     CONTINUE                                                      
C                                                                                                                                   
          DO N=1,NTLD                                                   
            IBUF4=1000*K + N                                            
            CALL SETLAB(IBUF,NC4TO8("GRID"),0,NC4TO8("ORGM"),IBUF4,     
     +                                        ILG1,ILAT,0,NPGG)         
            CALL PUTFLD2(2,GH,IBUF,MAXX)                                
            WRITE(6,6026) IBUF                                          
          ENDDO                                                         
  620 CONTINUE                                                          
C*********************************************************************                                                              
C     * LOOP OVER VEGETATION TYPES FOR FOLLOWING FIELDS:                                                                            
C                                                                                                                                   
      ICAN=4                                                            
      ICANP1=ICAN+1                                                     
      DO 780 IC=1,ICANP1                                                
                                                                        
C---------------------------------------------------------------------                                                              
C       * AREAL FRACTION OF EACH CANOPY TYPE.                                                                                       
C       * ENSURE CONSISTENCY WITH PRIMARY VEGETATION FIELD OVER GLACIER                                                             
C       * ICE AND WITH GROUND COVER OVER OTHER NON-LAND POINTS.                                                                     
                                                                        
        CALL GETFLD2(-1,GLL,NC4TO8("GRID"),0,NC4TO8("FCAN"),IC,         
     +                                            IBUF,MAXX,OK)         
        IF(.NOT.OK) CALL                             XIT('INITG14',-77)  
        IF(IBUF(5).NE.NLG .OR.IBUF(6).NE.NLAT) CALL  XIT('INITG14',-78)  
        WRITE(6,6025) IBUF
C                                                                                                                                   
        IOPTION=1                                                       
        ICHOICE=1
        ROCK_SPVAL=0.
        CALL HRTOLR5(GWL,MASKL,ILG1,ILAT,DLON,DLAT,ILG,                 
     1               LONM,LONP,LATM,LATP,                               
     2               GLL,HRMASK,NLG,NLAT,IOPTION,ICHOICE,               
     3               OK,NBADPTS,MASK_ROCK,ROCK_SPVAL,                                  
     4               GCLRTMP,GCHRTMP,RLON,RLAT,LATL,LATH,LONL,LONH,     
     5               VAL,DIST,LOCAT,IVAL,LOCFST,LONB,LATB,NLOMAX,NMAX)
c
c       * abort if there are other trouble points.
c      
        if(.not.ok)                     then
          do n=1,nbadpts
            i=lonb(n)
            j=latb(n)
            write(6,6030) i,j
          enddo  
          CALL                                       XIT('INITG14',-79)  
        endif
C                                                                                                                                   
C       * SET CANOPY FRACTIONS TO ZERO OVER ROCK AND GLACIER, AS                                                                    
C       * WELL AS OPEN WATER.                                                                                                       
C                                                                                                                                   
        DO 730 I=1,LGG                                                  
            IF(GL(I).EQ.-3.E0 .AND. GWL(I).NE.0.E0) THEN                
              PRINT *, '0IN LOOP 730, INCONSISTENCY: I,MASK,GL,GWL = ', 
     1                  I,MASKL(I),GL(I),GWL(I)                         
            ENDIF                                                       
            IF(GL(I).LE.-3.E0 .OR. MASKL(I).GT.-0.5E0) GWL(I)= 0.E0     
  730   CONTINUE                                                        
C                                                                                                                                   
        DO N=1,NTLD                                                     
          IBUF4=1000*IC + N                                             
          CALL SETLAB(IBUF,NC4TO8("GRID"),0,NC4TO8("FCAN"),IBUF4,       
     +                                      ILG1,ILAT,0,1)              
          CALL PUTFLD2(2,GWL,IBUF,MAXX)                                 
          WRITE(6,6026) IBUF                                            
        ENDDO                                                           
C---------------------------------------------------------------------                                                              
C       * VISIBLE ALBEDO OF EACH CANOPY TYPE.                                                                                       
C       * CONVERT FROM PERCENT TO FRACTION AND COMPLETE WEIGHTING                                                                   
C       * CALCULATION OVER CANOPY TYPES.                                                                                            
                                                                        
        CALL GETFLD2(-1,GLL,NC4TO8("GRID"),0,NC4TO8(" AVW"),IC,         
     +                                            IBUF,MAXX,OK)         
        IF(.NOT.OK) CALL                             XIT('INITG14',-80)  
        IF(IBUF(5).NE.NLG.OR.IBUF(6).NE.NLAT) CALL   XIT('INITG14',-81)  
        WRITE(6,6025) IBUF
C                                                                                                                                   
        IOPTION=1                                                       
        ICHOICE=1
        ROCK_SPVAL=0.
        CALL HRTOLR5(GH,MASKL,ILG1,ILAT,DLON,DLAT,ILG,                  
     1               LONM,LONP,LATM,LATP,                               
     2               GLL,HRMASK,NLG,NLAT,IOPTION,ICHOICE,               
     3               OK,NBADPTS,MASK_ROCK,ROCK_SPVAL,                                  
     4               GCLRTMP,GCHRTMP,RLON,RLAT,LATL,LATH,LONL,LONH,     
     5               VAL,DIST,LOCAT,IVAL,LOCFST,LONB,LATB,NLOMAX,NMAX)
c
c       * abort if there are other trouble points.
c      
        if(.not.ok)                     then
          do n=1,nbadpts
            i=lonb(n)
            j=latb(n)
            write(6,6030) i,j
          enddo  
          CALL                                      XIT('INITG14',-82)  
        endif
C                                                                                                                                   
        DO 735 I=1,LGG                                                  
            IF(GWL(I).GT.0.0E0) THEN                                    
                GH(I) = 0.01E0*GH(I)/GWL(I)                             
            ELSE                                                        
                GH(I)=0.0E0                                             
            ENDIF                                                       
  735   CONTINUE                                                        
C                                                                                                                                   
        DO N=1,NTLD                                                     
          IBUF4=1000*IC + N                                             
          CALL SETLAB(IBUF,NC4TO8("GRID"),0,NC4TO8("ALVC"),IBUF4,       
     +                                      ILG1,ILAT,0,NPGG)           
          CALL PUTFLD2(2,GH,IBUF,MAXX)                                  
          WRITE(6,6026) IBUF                                            
        ENDDO                                                           
C---------------------------------------------------------------------                                                              
C       * NEAR-IR ALBEDO OF EACH CANOPY TYPE.                                                                                       
C       * CONVERT FROM PERCENT TO FRACTION AND COMPLETE WEIGHTING                                                                   
C       * CALCULATION OVER CANOPY TYPES.                                                                                            
                                                                        
        CALL GETFLD2(-1,GLL,NC4TO8("GRID"),0,NC4TO8(" AIW"),IC,         
     +                                            IBUF,MAXX,OK)         
        IF(.NOT.OK) CALL                             XIT('INITG14',-83)  
        IF(IBUF(5).NE.NLG.OR.IBUF(6).NE.NLAT) CALL   XIT('INITG14',-84)  
        WRITE(6,6025) IBUF
C                                                                                                                                   
        IOPTION=1                                                       
        ICHOICE=1
        ROCK_SPVAL=0.
        CALL HRTOLR5(GH,MASKL,ILG1,ILAT,DLON,DLAT,ILG,                  
     1               LONM,LONP,LATM,LATP,                               
     2               GLL,HRMASK,NLG,NLAT,IOPTION,ICHOICE,               
     3               OK,NBADPTS,MASK_ROCK,ROCK_SPVAL,                                  
     4               GCLRTMP,GCHRTMP,RLON,RLAT,LATL,LATH,LONL,LONH,     
     5               VAL,DIST,LOCAT,IVAL,LOCFST,LONB,LATB,NLOMAX,NMAX)
c
c       * abort if there are other trouble points.
c      
        if(.not.ok)                     then
          do n=1,nbadpts
            i=lonb(n)
            j=latb(n)
            write(6,6030) i,j
          enddo  
          CALL                                       XIT('INITG14',-85)  
        endif
C                                                                                                                                   
        DO 740 I=1,LGG                                                  
          IF(GWL(I).GT.0.0E0) THEN                                      
            GH(I) = 0.01E0*GH(I)/GWL(I)                                 
          ELSE                                                          
            GH(I)=0.0E0                                                 
          ENDIF                                                         
  740   CONTINUE                                                        
C                                                                                                                                   
        DO N=1,NTLD                                                     
          IBUF4=1000*IC + N                                             
          CALL SETLAB(IBUF,NC4TO8("GRID"),0,NC4TO8("ALIC"),IBUF4,       
     +                                      ILG1,ILAT,0,NPGG)           
          CALL PUTFLD2(2,GH,IBUF,MAXX)                                  
          WRITE(6,6026) IBUF                                            
        ENDDO                                                           
C---------------------------------------------------------------------                                                              
C       * LN OF ROUGHNESS LENGTH OF EACH CANOPY TYPE.                                                                               
                                                                        
        CALL GETFLD2(-1,GLL,NC4TO8("GRID"),0,NC4TO8("LNZ0"),IC,         
     +                                            IBUF,MAXX,OK)         
        IF(.NOT.OK) CALL                             XIT('INITG14',-86)  
        IF(IBUF(5).NE.NLG.OR.IBUF(6).NE.NLAT) CALL   XIT('INITG14',-87)  
        WRITE(6,6025) IBUF
C                                                                                                                                   
        IOPTION=1                                                       
        ICHOICE=1
        ROCK_SPVAL=0.
        CALL HRTOLR5(GH,MASKL,ILG1,ILAT,DLON,DLAT,ILG,                  
     1               LONM,LONP,LATM,LATP,                               
     2               GLL,HRMASK,NLG,NLAT,IOPTION,ICHOICE,               
     3               OK,NBADPTS,MASK_ROCK,ROCK_SPVAL,                                  
     4               GCLRTMP,GCHRTMP,RLON,RLAT,LATL,LATH,LONL,LONH,     
     5               VAL,DIST,LOCAT,IVAL,LOCFST,LONB,LATB,NLOMAX,NMAX)
c     
c       * abort if there are other trouble points.
c      
        if(.not.ok)                     then
          do n=1,nbadpts
            i=lonb(n)
            j=latb(n)
            write(6,6030) i,j
          enddo  
          CALL                                       XIT('INITG14',-88)  
        endif
C                                                                                                                                   
        DO 745 I=1,LGG                                                  
          IF(GWL(I).GT.0.0E0) THEN                                      
            GH(I) = GH(I)/GWL(I)                                        
          ELSE                                                          
            GH(I)=0.0E0                                                 
          ENDIF                                                         
  745   CONTINUE                                                        
C                                                                                                                                   
        DO N=1,NTLD                                                     
          IBUF4=1000*IC + N                                             
          CALL SETLAB(IBUF,NC4TO8("GRID"),0,NC4TO8("LNZ0"),IBUF4,       
     +                                      ILG1,ILAT,0,NPGG)           
          CALL PUTFLD2(2,GH,IBUF,MAXX)                                  
          WRITE(6,6026) IBUF                                            
        ENDDO                                                           
C                                                                                                                                   
C       * FOLLOWING FIELDS IN VEGETATION CLASS LOOP NOT DEFINED FOR                                                                 
C       * URBAN CLASS (IC=ICAN+1).                                                                                                  
C                                                                                                                                   
        IF(IC.EQ.ICANP1) GO TO 780                                      
C-----------------------------------------------------------------------                                                            
C       * MAXIMUM LEAF AREA INDEX OF EACH CANOPY TYPE.                                                                              
                                                                        
        CALL GETFLD2(-1,GLL,NC4TO8("GRID"),0,NC4TO8("LAMX"),IC,         
     +                                            IBUF,MAXX,OK)         
        IF(.NOT.OK) CALL                             XIT('INITG14',-89)  
        IF(IBUF(5).NE.NLG.OR.IBUF(6).NE.NLAT) CALL   XIT('INITG14',-90)  
        WRITE(6,6025) IBUF
C                                                                                                                                   
        IOPTION=1                                                       
        ICHOICE=1
        ROCK_SPVAL=0.
        CALL HRTOLR5(GH,MASKL,ILG1,ILAT,DLON,DLAT,ILG,                  
     1               LONM,LONP,LATM,LATP,                               
     2               GLL,HRMASK,NLG,NLAT,IOPTION,ICHOICE,               
     3               OK,NBADPTS,MASK_ROCK,ROCK_SPVAL,                                  
     4               GCLRTMP,GCHRTMP,RLON,RLAT,LATL,LATH,LONL,LONH,     
     5               VAL,DIST,LOCAT,IVAL,LOCFST,LONB,LATB,NLOMAX,NMAX)         
c
c       * abort if there are other trouble points.
c      
        if(.not.ok)                     then
          do n=1,nbadpts
            i=lonb(n)
            j=latb(n)
            write(6,6030) i,j
          enddo  
          CALL                                       XIT('INITG14',-91)  
        endif
C                                                                                                                                   
        DO 750 I=1,LGG                                                  
          IF(GWL(I).GT.0.0E0) THEN                                      
            GH(I) = GH(I)/GWL(I)                                        
          ELSE                                                          
            GH(I)=0.0E0                                                 
          ENDIF                                                         
  750   CONTINUE                                                        
C                                                                                                                                   
        DO N=1,NTLD                                                     
          IBUF4=1000*IC + N                                             
          CALL SETLAB(IBUF,NC4TO8("GRID"),0,NC4TO8("LAMX"),IBUF4,       
     +                                      ILG1,ILAT,0,NPGG)           
          CALL PUTFLD2(2,GH,IBUF,MAXX)                                  
          WRITE(6,6026) IBUF                                            
        ENDDO                                                           
C-----------------------------------------------------------------------                                                            
C       * MINIMUM LEAF AREA INDEX OF EACH CANOPY TYPE.                                                                              
                                                                        
        CALL GETFLD2(-1,GLL,NC4TO8("GRID"),0,NC4TO8("LAMN"),IC,         
     +                                            IBUF,MAXX,OK)         
        IF(.NOT.OK) CALL                             XIT('INITG14',-92)  
        IF(IBUF(5).NE.NLG.OR.IBUF(6).NE.NLAT) CALL   XIT('INITG14',-93)  
        WRITE(6,6025) IBUF
C                                                                                                                                   
        IOPTION=1
        ICHOICE=1
        ROCK_SPVAL=0.
        CALL HRTOLR5(GH,MASKL,ILG1,ILAT,DLON,DLAT,ILG,                  
     1               LONM,LONP,LATM,LATP,                               
     2               GLL,HRMASK,NLG,NLAT,IOPTION,ICHOICE,               
     3               OK,NBADPTS,MASK_ROCK,ROCK_SPVAL,                                  
     4               GCLRTMP,GCHRTMP,RLON,RLAT,LATL,LATH,LONL,LONH,     
     5               VAL,DIST,LOCAT,IVAL,LOCFST,LONB,LATB,NLOMAX,NMAX)
c
c       * abort if there are other trouble points.
c      
        if(.not.ok)                     then
          do n=1,nbadpts
            i=lonb(n)
            j=latb(n)
            write(6,6030) i,j
          enddo  
          CALL                                       XIT('INITG14',-94)  
        endif      
C                                                                                                                                   
        DO 755 I=1,LGG                                                  
          IF(GWL(I).GT.0.0E0) THEN                                      
            GH(I) = GH(I)/GWL(I)                                        
          ELSE                                                          
            GH(I)=0.0E0                                                 
          ENDIF                                                         
  755   CONTINUE                                                        
C                                                                                                                                   
        DO N=1,NTLD                                                     
          IBUF4=1000*IC + N                                             
          CALL SETLAB(IBUF,NC4TO8("GRID"),0,NC4TO8("LAMN"),IBUF4,       
     +                                      ILG1,ILAT,0,NPGG)           
          CALL PUTFLD2(2,GH,IBUF,MAXX)                                  
          WRITE(6,6026) IBUF                                            
        ENDDO                                                           
C-----------------------------------------------------------------------                                                            
C       * CANOPY MASS OF EACH CANOPY TYPE.                                                                                          
                                                                        
        CALL GETFLD2(-1,GLL,NC4TO8("GRID"),0,NC4TO8("CMAS"),IC,         
     +                                            IBUF,MAXX,OK)         
        IF(.NOT.OK) CALL                             XIT('INITG14',-95)  
        IF(IBUF(5).NE.NLG.OR.IBUF(6).NE.NLAT) CALL   XIT('INITG14',-96)  
        WRITE(6,6025) IBUF
C                                                                                                                                   
        IOPTION=1
        ICHOICE=1
        ROCK_SPVAL=0.
        CALL HRTOLR5(GH,MASKL,ILG1,ILAT,DLON,DLAT,ILG,                  
     1               LONM,LONP,LATM,LATP,                               
     2               GLL,HRMASK,NLG,NLAT,IOPTION,ICHOICE,               
     3               OK,NBADPTS,MASK_ROCK,ROCK_SPVAL,                                  
     4               GCLRTMP,GCHRTMP,RLON,RLAT,LATL,LATH,LONL,LONH,     
     5               VAL,DIST,LOCAT,IVAL,LOCFST,LONB,LATB,NLOMAX,NMAX)
c
c       * abort if there are other trouble points.
c      
        if(.not.ok)                     then
          do n=1,nbadpts
            i=lonb(n)
            j=latb(n)
            write(6,6030) i,j
          enddo  
          CALL                                       XIT('INITG14',-97)  
        endif
C                                                                                                                                   
        DO 760 I=1,LGG                                                  
          IF(GWL(I).GT.0.0E0) THEN                                      
            GH(I) = GH(I)/GWL(I)                                        
          ELSE                                                          
            GH(I)=0.0E0                                                 
          ENDIF                                                         
  760   CONTINUE                                                        
C                                                                                                                                   
        DO N=1,NTLD                                                     
          IBUF4=1000*IC + N                                             
          CALL SETLAB(IBUF,NC4TO8("GRID"),0,NC4TO8("CMAS"),IBUF4,       
     +                                      ILG1,ILAT,0,NPGG)           
          CALL PUTFLD2(2,GH,IBUF,MAXX)                                  
          WRITE(6,6026) IBUF                                            
        ENDDO                                                           
C-----------------------------------------------------------------------                                                            
C       * ROOTING DEPTH OF EACH CANOPY TYPE.                                                                                        
                                                                        
        CALL GETFLD2(-1,GLL,NC4TO8("GRID"),0,NC4TO8("ROOT"),IC,         
     +                                            IBUF,MAXX,OK)         
        IF(.NOT.OK) CALL                             XIT('INITG14',-98)  
        IF(IBUF(5).NE.NLG.OR.IBUF(6).NE.NLAT) CALL   XIT('INITG14',-99)  
        WRITE(6,6025) IBUF
C                                                                                                                                   
        IOPTION=1                                                       
        ICHOICE=1
        ROCK_SPVAL=0.
        CALL HRTOLR5(GH,MASKL,ILG1,ILAT,DLON,DLAT,ILG,                  
     1               LONM,LONP,LATM,LATP,                               
     2               GLL,HRMASK,NLG,NLAT,IOPTION,ICHOICE,               
     3               OK,NBADPTS,MASK_ROCK,ROCK_SPVAL,                                  
     4               GCLRTMP,GCHRTMP,RLON,RLAT,LATL,LATH,LONL,LONH,     
     5               VAL,DIST,LOCAT,IVAL,LOCFST,LONB,LATB,NLOMAX,NMAX)
c
c       * abort if there are other trouble points.
c      
        if(.not.ok)                     then
          do n=1,nbadpts
            i=lonb(n)
            j=latb(n)
            write(6,6030) i,j
          enddo  
          CALL                                        XIT('INITG14',-6)  
        endif
C                                                                                                                                   
        DO 765 I=1,LGG                                                  
          IF(GWL(I).GT.0.0E0) THEN                                      
            GH(I) = GH(I)/GWL(I)                                        
          ELSE                                                          
            GH(I)=0.0E0                                                 
          ENDIF                                                         
  765   CONTINUE                                                        
C                                                                                                                                   
        DO N=1,NTLD                                                     
          IBUF4=1000*IC + N                                             
          CALL SETLAB(IBUF,NC4TO8("GRID"),0,NC4TO8("ROOT"),IBUF4,       
     +                                      ILG1,ILAT,0,NPGG)           
          CALL PUTFLD2(2,GH,IBUF,MAXX)                                  
          WRITE(6,6026) IBUF                                            
        ENDDO                                                           
  780 CONTINUE                                                          
C---------------------------------------------------------------------                                                              
C     * GLACIER FRACTION.                                                                                                           
C                                                                                                                                   
      CALL SETLAB(IBUF,NC4TO8("GRID"),0,NC4TO8("GICN"),1,               
     +                                  ILG1,ILAT,0,NPGG)               
      CALL PUTFLD2(2,GICN,IBUF,MAXX)                                    
      WRITE(6,6026) IBUF                                                
C---------------------------------------------------------------------                                                              
C     * LAND FRACTION.                                                                                                              
C                                                                                                                                   
      CALL SETLAB(IBUF,NC4TO8("GRID"),0,NC4TO8("FLND"),1,               
     +                                  ILG1,ILAT,0,NPGG)               
      CALL PUTFLD2(2,FLND,IBUF,MAXX)                                    
      WRITE(6,6026) IBUF                                                
C---------------------------------------------------------------------                                                              
C     * UNRESOLVED LAKE INFORMATION.
C                                                                                                                                   
      CALL SETLAB(IBUF,NC4TO8("GRID"),0,NC4TO8("FLKU"),1,               
     +                                  ILG1,ILAT,0,NPGG)               
      CALL PUTFLD2(2,FLKU,IBUF,MAXX)                                    
      WRITE(6,6026) IBUF                                                
C
      CALL SETLAB(IBUF,NC4TO8("GRID"),0,NC4TO8("LDEP"),1,               
     +                                  ILG1,ILAT,0,NPGG)               
      CALL PUTFLD2(2,LDPT,IBUF,MAXX)                                    
      WRITE(6,6026) IBUF                   
C
      CALL SETLAB(IBUF,NC4TO8("GRID"),0,NC4TO8("LLAK"),1,               
     +                                  ILG1,ILAT,0,NPGG)               
      CALL PUTFLD2(2,LLAK,IBUF,MAXX)                                    
      WRITE(6,6026) IBUF            
C
      CALL SETLAB(IBUF,NC4TO8("GRID"),0,NC4TO8("BLAK"),1,               
     +                                  ILG1,ILAT,0,NPGG)               
      CALL PUTFLD2(2,BLAK,IBUF,MAXX)                                    
      WRITE(6,6026) IBUF            
C---------------------------------------------------------------------                                                              
C     * RESOLVED LAKE FRACTION AND DEPTH.                                                                                                     
C                                                                                                                                   
      CALL SETLAB(IBUF,NC4TO8("GRID"),0,NC4TO8("FLKR"),1,               
     +                                  ILG1,ILAT,0,NPGG)               
      CALL PUTFLD2(2,FLKR,IBUF,MAXX)                                    
      WRITE(6,6026) IBUF                            
C---------------------------------------------------------------------                                                              
C     * SURFACE GEOPOTENTIAL ON MODEL GRID (FROM CONTROL FILE).                                                                     
                                                                        
      CALL GETFLD2(99,GL,NC4TO8("GRID"),0,NC4TO8("PHIM"),1,IBUF,MAXX,OK)
      IF(.NOT.OK) CALL                              XIT('INITG14',-28)  
      IBUF(3)=NC4TO8("PHIS")                                            
      CALL PUTFLD2(2,GL,IBUF,MAXX)                                      
      WRITE(6,6026) IBUF                                                
C---------------------------------------------------------------------
C     * ALL DONE...                                                                                                                 
C                                                                        
      CALL                                          XIT('INITG14',0)    
C                                                                                                                                    
C     * E.O.F. ENCOUNTERED WHILE READING INPUT.                                                                                     
C                                                                                                                                    
  905 CALL                                          XIT('INITG14',-1)                                                                 
C                                                                        
C     * E.O.F. ON FILE ICTL.                                                                                                        
C                                                                        
  910 CALL                                          XIT('INITG14',-29)  
  911 CALL                                          XIT('INITG14',-30)  
C---------------------------------------------------------------------                                                    
 5010 FORMAT(10X,F10.2)                                                         I4                                                            
 6010 FORMAT('0IDAY,ILG1,ILAT =',3I6)                                   
 6011 FORMAT('0NTLD,NTLK,NTWT = ',3I5)                             
 6025 FORMAT(' ',A4,I10,1X,A4,5I6)                                      
 6026 FORMAT(' ',60X,A4,I10,1X,A4,5I6)                                  
 6030 FORMAT('0EXITING PROGRAM; NO POINTS FOUND WITHIN GRID SQUARE',    
     1       ' CENTRED AT (',I3,',',I3,')')                             
 6040 FORMAT('0MISMATCH BETWEEN "TRUE" AND SOIL DATA MASKS:   I,LON,LAT,
     1MASK,GLR,GL=',3I5,3(1X,F3.0))                                     
 6045 FORMAT('0SURFACE ORGANIC POINT:  I,LON,LAT,MASK,GL = ',3I5,       
     1        2(1X,F3.0))                                               
 6050 FORMAT('0SURFACE ROCK    POINT:  I,LON,LAT,MASK,GL = ',3I5,       
     1        2(1X,F3.0))                                               
      END                                                               

      SUBROUTINE HRTOLR5(GLR,GCLR,ILG1,ILAT,DLON,DLAT,ILG,
     1                   LONM,LONP,LATM,LATP,
     2                   GHR,GCHR,NLG,NLAT,IOPTION,ICHOICE,
     3                   OK,NBADPTS,MASK_ROCK,ROCK_SPVAL,
     4                   GCLRTMP,GCHRTMP,RLON,RLAT,LATL,LATH,LONL,LONH,
     5                   VAL,DIST,LOCAT,IVAL,LOCFST,LONB,LATB,
     6                   NLOMAX,NMAX)

C     * FEB 16/20 - M. LAZARE. NEW ROUTINE REPLACING HRTOLR4 WITH:
C     *                        - LONB AND LATB INITIALIZED INSIDE NOW.      
C     *                        - NBADPTS,MASK_ROCK,ROCK_SPVAL ADDED TO CALL.
C     *                        - NLOMAX PASSED IN AND USED TO EXPLICITLY
C     *                          DIMENSION {LONB,LATB}.
C     *                        - LONBAD,LATBAD REMOVED.      
C     * SEP 30/14 - M. LAZARE. ADD NEW IOPTION=3 FOR PROCESSING SOIL
C     *                        COLOUR INDEX FIELD (UPWARDLY COMPATIBLE).
C     * FEB 06/08 - M. LAZARE. NEW VERSION FOR USE WITH INITG12/INCHEM3
C     *                        AND SUBSEQUENT VERSIONS:
C     *                        - ONLY IGRID=0 SUPPORTED; THUS IGRID REMOVED.
C     *                        - LONM,LONP,LATM,LATP PROMOTED TO ARRAYS
C     *                          AND USED TO CORRECT ERROR IN PREVIOUS
C     *                          VERSIONS FOR CONTRIBUTIONS OF HIGH-RES
C     *                          BOXES AT EDGES OF LOW-RES GRID SQUARE
C     *                          TO AREA-WEIGHTED AVERAGE. THUS, THEY
C     *                          MUST BE PASSED IN FROM CALLING PROGRAMS
C     *                          AND THESE (INITG12,INCHEM3) MUST ALSO
C     *                          BE REVISED ACCORDINGLY.   
C     * APR 26/07 - X. MA.     PREVIOUS FINAL VERSION FOR HRTOLR3.
C     *                        ADD ICHOICE=4 FOR VOLCANIC HEIGHT
C     * MAY 14/04 - M. LAZARE. CHANGE THE DIMENSION OF "LONB(ILG), 
C     *                        LATB(ILAT)" TO "*".
C     * DEC 29/98 - M. LAZARE. ADD IOPTION.EQ.2.
C     * OCT 10/96 - M. LAZARE. ADD CALL TO NEW SUBROUTINE "GGFILL" AT
C     *                         THE END, TO ATTEMPT TO BORROW FROM
C     *                         NEAREST NEIGHBOURS IF NO HIGH-RES
C     *                         POINTS OF THE APPROPRIATE LAND TYPE
C     *                         ARE AVAILABLE (FIRST N/S/E/W, THEN, IFF
C     *                         NECESSARY, NE/SE/NW/SW). 
C     * APR 26/96 - M. LAZARE. PREVIOUS VERSION HRTOLR2.
C 
C     * TRANSFORMS DATA FROM AN OFFSET HIGH RESOLUTION GRID(NLG X NLAT)
C     * TO A LOWER RESOLUTION GRID(ILG X ILAT), BY TAKING INTO ACCOUNT 
C     * ALL THE HIGH-RESOLUTION GRID POINTS IN EACH LOWER-RESOLUTION
C     * GRID SQUARE.
C 
C     * THE LONGITUDES AND LATITUDES OF THE HIGH-RESOLUTION FIELD GHR ARE 
C     * CONTAINED IN THE ARRAYS RLON AND RLAT RESPECTIVELY, WHILE THOSE OF THE
C     * LOW-RESOLUTION FIELD GLR ARE CONTAINED IN DLON AND DLAT RESPECTIVELY. 
C 
C     * IF IOPTION.EQ.1: ONLY THOSE HIGH-RESOLUTION POINTS WITH LAND MASK 
C     *              IS THE SAME AS THE LOW-RESOLUTION MASK ARE PROCESSED
C     *              (LIKE FORMER ROUTINE HRALR).
C     * IF IOPTION.EQ.0: THE RESPECTIVE LAND MASKS ARE NOT TAKEN INTO 
C     *              ACCOUNT (LIKE FORMER ROUTINE HRTOLR).
C     * IF IOPTION.EQ.2: THE RESPECTIVE LAND MASKS ARE NOT TAKEN INTO 
C     *              ACCOUNT FOR LOW-RES LAND POINTS BUT ARE FOR LOW-RES
C     *              WATER/ICE POINTS. THIS IS USED TO PROCESS AMIP2
C     *              HIGH-RES DATA FOR OCEANS WHERE LAND VALUES ARE
C     *              NOT CRITICAL FOR {GT,SICN,SIC} BUT UNUSUAL
C     *              HIGH-RES AMIP2 LAND MASK CAUSES PROBLEMS OVER
C     *              LAND IN REDUCING TO THE MODEL GAUSSIAN GRID.     
C     * IF IOPTION.EQ.3: THE RESPECTIVE LAND MASKS ARE NOT TAKEN INTO
C     *              ACCOUNT FOR LOW-RES WATER POINTS BUT ARE FOR LOW-RES
C     *              LAND POINTS. THIS IS USED TO PROCESS THE SOIL COLOUR
C     *              INDEX FIELD WHERE PROBLEMS AROSE FOR BIG INLAND LAKES
C     *              DEFINED IN OUR MASK DATASET BUT NOT IN THE INPUT DATASET.
C
C     * IF ICHOICE.EQ.0: THE MOST FREQUENT VALUE IN THE SQUARE IS RETURNED. IF
C     *              THERE IS A TIE, THE VALUE OF THE POINT CLOSEST TO THE
C     *              LOW-RESOLUTION GRID POINT IS RETURNED. 
C     * IF ICHOICE.EQ.1: THE PARTIAL BOX AREA-AVERAGED VALUE IS RETURNED. 
C     * IF ICHOICE.EQ.2: THE FIELD BEING SMOOTHED IS THE GROUND COVER FIELD.
C     *              THIS IS HANDLED AS IN ICHOICE=0, EXCEPT THAT A DECISION
C     *              IS MADE FIRST ON THE MOST FREQUENT VALUE BEING LAND OR 
C     *              WATER (FROZEN OR OPEN). FOR RESULTING NON-LAND POINTS, 
C     *              A FINAL DECISION IS MADE ON WHETHER THE MOST FREQUENT
C     *              VALUE IS WATER OR SEA-ICE. 
C     * IF ICHOICE.EQ.3: THE SIMPLE SUM OF THE CONTRIBUTIONS FROM EACH OF 
C     *              THE HIGH-RESOLUTION VALUES WITHIN THE LOW-RESOLUTION
C     *              GRID SQUARE IS RETURNED.
C     * IF ICHOICE.EQ.4: AREA-AVERAGED ONLY FOR NON-ZEROES (FOR VOLCANIC HEIGHTS).
C
C     * IF NO HIGH-RESOLUTION POINTS ARE FOUND WITHIN THE LOW-RESOLUTION GRID 
C     * SQUARE, THE SUBROUTINE RETURNS WITH OK=.FALSE. AND PASSES BACK THE
C     * LOCATION OF THE BAD POINT IN (I,J) COORDINATES.
C------------------------------------------------------------------------------ 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      REAL GLR(ILG1,ILAT),GHR(NLG,NLAT),GCLR(ILG1,ILAT),GCHR(NLG,NLAT)
      REAL LONM(ILG),LONP(ILG),LATM(ILAT),LATP(ILAT)
      REAL DLON(ILG),DLAT(ILAT)
      REAL MASK_ROCK(ILG1,ILAT)
C
C     * OUTPUT ARRAYS FOR SUBROUTINE GGFILL.
C
      INTEGER LONB(NLOMAX), LATB(NLOMAX)
C
C     * WORK ARRAYS FOR SUBROUTINE GGFILL.
C
      INTEGER LONBX(NLOMAX), LATBX(NLOMAX)      
C
C     * WORK ARRAYS FOR SUBROUTINE HRTOLR5.
C     * NOTE THAT THE SIZES OF ARRAYS VAL,DIST,LOCAT,IVAL,LOCFST AND
C     * NMAX REALLY REPRESENT THE NUMBER OF HIGH-RESOLUTION GRID 
C     * POINTS WITHIN A LOW-RESOLUTION GRID SQUARE. FOR CONVENIENCE
C     * SAKE, THEY ARE DIMENSIONED WITH SIZE "NLG" AND AN ABORT 
C     * CONDITION IS GENERATED WITHIN THE SUBROUTINE.
C
      REAL GCHRTMP(NLG,NLAT)
      REAL RLON(NLG), RLAT(NLAT)

      REAL GCLRTMP(ILG,ILAT)
      INTEGER LATL(ILAT), LATH(ILAT), LONL(ILG), LONH(ILG)

      REAL VAL(NLG), DIST(NLG) 
      INTEGER LOCAT(NLG), IVAL(NLG), LOCFST(NLG), NMAX(NLG)       

C 
      LOGICAL OK
C 
      DATA PI/3.1415926535898E0/
      DATA SPVAL/1.E38/
C-------------------------------------------------------------------------------
      OK=.TRUE. 
C    
C     * ABORT IF NUMBER OF HIGH-RESOLUTION POINTS WITHIN LOW-RESOLUTION
C     * GRID SQUARE EXCEEDS THAT DIMENSIONED IN MAIN PROGRAM ("NLG").
C
      NPOINTS=(NLG*NLAT)/(ILG*ILAT)
      IF(NPOINTS.GT.NLG)                     CALL XIT('HRTOLR5',-1)
C
C     * DISALLOWED COMBINATION OF OPTIONS:
C
      IF(IOPTION.GE.1 .AND. ICHOICE.EQ.2)    CALL XIT('HRTOLR5',-2)
      IF(IOPTION.LT.0 .OR.  IOPTION.GT.3)    CALL XIT('HRTOLR5',-3)
      IF(ICHOICE.LT.0 .OR.  ICHOICE.GT.4)    CALL XIT('HRTOLR5',-4)
C 
C     * DEFINE LONGITUDE AND LATITUDE VECTORS FOR HIGH-RESOLUTION GRID. 
C 
      DEGHX=360.E0/(FLOAT(NLG)) 
      DEGHY=180.E0/(FLOAT(NLAT)) 
      DEGHXST=DEGHX*0.5E0
      DEGHYST=DEGHY*0.5E0
C 
      DO  10 I=1,NLG  
   10 RLON(I)=DEGHXST+DEGHX*FLOAT(I-1) 
      DO  20 J=1,NLAT 
   20 RLAT(J)=(-90.E0+DEGHYST)+DEGHY*FLOAT(J-1)
C 
C     * FIND HIGH-RESOLUTION GRID POINTS WITHIN EACH LOW-RESOLUTION GRID SQUARE.
C     * THIS REQUIRES SPECIAL HANDLING OF POINTS NEAR THE POLES OR AT THE 
C     * GREENWICH MERIDIAN. 
C
      DO 40 J=1,ILAT
        IF(J.EQ.ILAT) THEN
          LATP(J)=90.E0
        ELSE
          LATP(J)=0.5E0*(DLAT(J+1)+DLAT(J))
        ENDIF 
        IF(J.EQ.1) THEN 
          LATM(J)=-90.E0 
        ELSE
          LATM(J)=0.5E0*(DLAT(J-1)+DLAT(J))
        ENDIF 
        DO 30 JJ=1,NLAT 
          IF(JJ.NE.1) THEN
            IF(RLAT(JJ-1).LT.LATM(J).AND.RLAT(JJ).GE.LATM(J)) LATL(J)=JJ
          ELSE
            IF(RLAT(JJ).GE.LATM(J)) LATL(J)=JJ 
          ENDIF 
          IF(JJ.NE.NLAT) THEN 
            IF(RLAT(JJ).LE.LATP(J).AND.RLAT(JJ+1).GT.LATP(J)) LATH(J)=JJ
          ELSE
            IF(RLAT(JJ).LE.LATP(J)) LATH(J)=JJ 
          ENDIF 
   30   CONTINUE
   40 CONTINUE
C 
      DO 75 I=1,ILG 
        IF(I.EQ.ILG) THEN
          LONP(I)=0.5E0*(360.E0+DLON(ILG))
        ELSE
          LONP(I)=0.5E0*(DLON(I+1)+DLON(I))
        ENDIF
        IF(I.EQ.1) THEN 
          LONM(I)=0.5E0*(360.E0+DLON(ILG))
        ELSE
          LONM(I)=0.5E0*(DLON(I-1)+DLON(I))
        ENDIF 
        DO 50 II=1,NLG
          IF(II.NE.1) THEN
            IF(RLON(II-1).LT.LONM(I).AND.RLON(II).GE.LONM(I)) LONL(I)=II
          ELSE
            IF(RLON(II).GE.LONM(I)) LONL(I)=II 
          ENDIF 
          IF(II.NE.NLG) THEN
            IF(RLON(II).LE.LONP(I).AND.RLON(II+1).GT.LONP(I)) LONH(I)=II
          ELSE
            IF(RLON(II).LE.LONP(I)) LONH(I)=II         
          ENDIF
   50   CONTINUE
        IF(LONL(I).GT.LONH(I))       LONH(I)=LONH(I)+NLG
   75 CONTINUE
C
C     * DEFINE WORK ARRAYS FOR POSSIBLE SUBSEQUENT USE OF LAND MASK
C     * INFORMATION TO DO "REDUCING". IF IOPTION.EQ.0, THIS INFO IS
C     * NOT USED AND THIS IS DONE TRANSPARENTLY BY SETTING BOTH THE
C     * LOW AND HIGH RESOLUTION TEMPORARY MASKS TO ZERO, IN THAT
C     * THESE WILL AGREE FOR ALL POINTS REGARDLESS OF THEIR VALUE.
C     * IF IOPTION.EQ.{1,2,3}, THESE TEMPORARY ARRAYS WILL CONTAIN THE
C     * NECESSARY LAND MASK INFORMATION TO PROPERLY "REDUCE".
C
      DO 85 J=1,ILAT
        DO 80 I=1,ILG
          IF(IOPTION.EQ.0) THEN
            GCLRTMP(I,J)=0.E0 
          ELSE
            GCLRTMP(I,J)=GCLR(I,J)
          ENDIF
   80   CONTINUE
   85 CONTINUE     
C
      DO 95 JJ=1,NLAT
        DO 90 II=1,NLG
          IF(IOPTION.EQ.0) THEN
            GCHRTMP(II,JJ)=0.E0 
          ELSE
            GCHRTMP(II,JJ)=GCHR(II,JJ)
          ENDIF
   90   CONTINUE
   95 CONTINUE     
C  
C     * LOOP OVER POINTS IN THE LOW-RESOLUTION FIELD, KEEPING TRACK OF ALL
C     * HIGH-RESOLUTION POINTS WITHIN EACH LOW-RESOLUTION GRID SQUARE 
C     * HAVING THE SAME GROUND COVER AS THE LOW-RESOLUTION GRID SQUARE. 
C
      NBADPTSX=0
      DO I=1,NLOMAX
        LONBX(I)=0
        LATBX(I)=0
      ENDDO
      
C      
      DO 900 J=1,ILAT 
        FACTL=(COS(DLAT(J)*PI/180.E0))**2 
        DO 800 I=1,ILG
          NLPTS=0 
          IF(ICHOICE.EQ.1 .OR. ICHOICE.EQ.4) THEN 
C 
C           * FOR CONTINUOUS FIELDS, AREA-AVERAGE THESE POINTS, IF ICHOICE.EQ.1. 
C           * AREA-AVERAGE THOSE NON-ZERO POINTS ONLY, IF ICHOICE.EQ.4. 
C 
            BIGAREA=0.E0
            SUM=0.E0
            DO 200 JJ=LATL(J),LATH(J) 
              IF(JJ.EQ.NLAT) THEN 
                SLATP=90.E0 
              ELSE IF(JJ.EQ.LATH(J)) THEN
                SLATP=LATP(J)
              ELSE
                SLATP=0.5E0*(RLAT(JJ+1)+RLAT(JJ)) 
              ENDIF 
              IF(JJ.EQ.1) THEN
                SLATM=-90.E0
              ELSE IF(JJ.EQ.LATL(J)) THEN
                SLATM=LATM(J)
              ELSE
                SLATM=0.5E0*(RLAT(JJ-1)+RLAT(JJ)) 
              ENDIF 
              FACTLAT=(COS(RLAT(JJ)*PI/180.E0))*(SLATP-SLATM)*PI/180.E0 
              DO 100 LL=LONL(I),LONH(I) 
                IF(LL.GT.(NLG+1))THEN
                  II=LL-NLG
                  IIP1=II+1
                  IIM1=II-1
                  SLONP=0.5E0*(RLON(IIP1)+RLON(II))
                  SLONM=0.5E0*(RLON(IIM1)+RLON(II))
                ELSE IF(LL.EQ.(NLG+1) .OR. LL.EQ.1)THEN
                  II=1
                  IIP1=2
                  SLONP=0.5E0*(RLON(IIP1)+RLON(II))
                  SLONM=0.E0
                ELSE IF(LL.EQ.NLG)THEN
                  II=LL
                  IIM1=II-1
                  SLONP=360.E0
                  SLONM=0.5E0*(RLON(IIM1)+RLON(II))
                ELSE 
                  II=LL
                  IIP1=II+1
                  IIM1=II-1
                  SLONP=0.5E0*(RLON(IIP1)+RLON(II))
                  SLONM=0.5E0*(RLON(IIM1)+RLON(II))
                ENDIF
                IF(LL.EQ.LONL(I))                           THEN
                  SLONM=LONM(I)
                ENDIF
                IF(LL.EQ.LONH(I))                           THEN
                  SLONP=LONP(I)
                  IF(SLONM.GT.SLONP) SLONP=SLONP+360.
                ENDIF
C
                IF(GCHRTMP(II,JJ).EQ.GCLRTMP(I,J)
     1        .OR. (IOPTION.EQ.2 .AND. GCLRTMP(I,J).EQ.-1.E0)
     2        .OR. (IOPTION.EQ.3 .AND. GCLRTMP(I,J).EQ.0.)) THEN

                 IF(ICHOICE.EQ.4) THEN
                  IF(GHR(II,JJ).GT.0.) THEN
                   NLPTS=NLPTS+1
                   AREA=FACTLAT*((SLONP-SLONM)*PI/180.E0)
                   SUM=SUM+AREA*GHR(II,JJ) 
                   BIGAREA=BIGAREA+AREA
                  ENDIF
                 ELSE
                   NLPTS=NLPTS+1
                   AREA=FACTLAT*((SLONP-SLONM)*PI/180.E0)
                   SUM=SUM+AREA*GHR(II,JJ)
                   BIGAREA=BIGAREA+AREA
                 ENDIF
                ENDIF
  100         CONTINUE
  200       CONTINUE
            IF(NLPTS.NE.0) THEN 
              GLR(I,J)=SUM/BIGAREA
            ELSE
             IF(ICHOICE.EQ.4) THEN
              GLR(I,J)=0.
             ENDIF
            ENDIF 
C
          ELSE IF(ICHOICE.EQ.3) THEN
C
C         * RETURN THE SIMPLE SUM OF ALL CONTRIBUTIONS OF HIGH-
C         * RESOLUTION GRID POINTS WITHIN LOW-RESOLUTION GRID
C         * SQUARE.
C
            SUM=0.E0
            DO 275 JJ=LATL(J),LATH(J) 
              DO 250 LL=LONL(I),LONH(I) 
                IF(LL.GT.NLG) THEN
                  II=LL-NLG 
                ELSE
                  II=LL 
                ENDIF 
                IF(GCHRTMP(II,JJ).EQ.GCLRTMP(I,J)
     1        .OR. (IOPTION.EQ.2 .AND. GCLRTMP(I,J).EQ.-1.E0)
     2        .OR. (IOPTION.EQ.3 .AND. GCLRTMP(I,J).EQ.0.)) THEN
                  NLPTS=NLPTS+1
                  SUM=SUM+GHR(II,JJ) 
                ENDIF
 250          CONTINUE
 275        CONTINUE   
            IF(NLPTS.NE.0) THEN 
              GLR(I,J)=SUM
            ENDIF 
C
          ELSE
C 
C           * FOR DISCRETE-VALUED FIELDS, KEEP TRACK OF EACH QUALIFYING HIGH- 
C           * RESOLUTION POINT VALUE IN ARRAY VAL, AND THEIR DISTANCE FROM THE
C           * LOW-RESOLUTION GRID POINT IN ARRAY DIST.
C 
            DO 400 JJ=LATL(J),LATH(J) 
              DISTLAT=DLAT(J)-RLAT(JJ)
              DO 300 LL=LONL(I),LONH(I) 
                IF(LL.GT.NLG) THEN
                  II=LL-NLG 
                ELSE
                  II=LL 
                ENDIF 
                IF(GCHRTMP(II,JJ).EQ.GCLRTMP(I,J) 
     1        .OR. (IOPTION.EQ.2 .AND. GCLRTMP(I,J).EQ.-1.E0)
     2        .OR. (IOPTION.EQ.3 .AND. GCLRTMP(I,J).EQ.0.)) THEN
                  NLPTS=NLPTS+1
                  VAL(NLPTS)=GHR(II,JJ) 
                  DISTRY=ABS(DLON(I)-RLON(II))
                  DISTLON=MIN(DISTRY,(360.E0-DISTRY)) 
                  DIST(NLPTS)=(FACTL*(DISTLON**2)+DISTLAT**2)*
     1                        ((PI/180.E0)**2)
                ENDIF
  300         CONTINUE
  400       CONTINUE
          ENDIF
C 
C         * SKIP TO "GGFILL" SUBROUTINE IF NO HIGH-RESOLUTION POINTS IN
C         * LOW-RESOLUTION GRID SQUARE DEFINED BY (I,J).
C 
          IF(NLPTS.EQ.0 .AND. ICHOICE.NE.4) THEN
            IF(MASK_ROCK(I,J).EQ.1.)                         THEN
C
C             * THIS IS A LAND POINT TURNED TO ROCK, SO USE SPECIAL
C             * SPECIAL VALUE.
C
              GLR(I,J)=ROCK_SPVAL
            ELSE   
              PRINT *, '0NO HIGH-RESOLUTION INFORMATION FOR:'
              PRINT *, ' I,J,GCLR = ',I,J,GCLRTMP(I,J)
              PRINT *, ' LATL,LATH,LONL,LONH = ',LATL(J),LATH(J),
     1                   LONL(I),LONH(I)
              DO 475 JJ=LATL(J),LATH(J) 
                DO 450 LL=LONL(I),LONH(I) 
                  IF(LL.GT.NLG) THEN
                    II=LL-NLG 
                  ELSE
                    II=LL 
                  ENDIF 
                  PRINT *, ' II,JJ,GCHR = ',II,JJ,GCHRTMP(II,JJ)
  450           CONTINUE
  475         CONTINUE   
              NBADPTSX=NBADPTSX+1
              LONBX(NBADPTSX)=I
              LATBX(NBADPTSX)=J
              GLR(I,J)=SPVAL
              GO TO 800
            ENDIF  
          ENDIF 
C 
C         * THE REST OF DO-LOOP 800 IS CONCERNED WITH THE MOST FREQUENTLY-
C         * OCCURRING CASES.
C 
          IF(ICHOICE.NE.1 .AND. ICHOICE.NE.3 .AND. ICHOICE.NE.4) THEN
            IF(NLPTS.EQ.1) THEN
C
C             * IF ONLY ONE HIGH-RESOLUTION GRID POINT EXISTS, RETURN ITS VALUE.
C
              GLR(I,J)=VAL(1) 
            ELSE
C 
C             * CALCULATE NUMBER OF DISTINCT-VALUED POINTS (DEFINED BY NT), THEIR 
C             * FIRST-OCCURRING LOCATION (DEFINED BY ARRAY LOCFST) AND THE
C             * ASSOCIATED LOCATIONS WHERE THEY OCCUR IN ARRAY VAL (DEFINED BY
C             * ARRAY NMAX).
C 
              NEWMAX=0
              IF(ICHOICE.EQ.2) THEN 
C 
C               * THE FIELD BEING SMOOTHED IS GROUND COVER AND REQUIRES SPECIAL 
C               * TREATMENT FOR LOW-RESOLUTION GRID SQUARES HAVING ALL THREE TYPES
C               * OF GROUND COVER. IF THE MOST FREQUENTLY-OCCURRING VALUE IS LAND 
C               * BUT THERE ARE MORE (WATER+ICE) POINTS THAN LAND, THE RESULTING
C               * LOW-RESOLUTION POINT SHOULD BE RETURNED AS THE MORE FREQUENT
C               * VALUE OF WATER VERSUS ICE.
C 
                NT=0
                CALL WHENEQ(NLPTS,VAL,1, 0.E0,IVAL,NVALW) 
                IF(NVALW.NE.0) THEN 
                  NT=NT+1 
                  NMAX(NT)=NVALW
                  LOCFST(NT)=IVAL(1)
                ENDIF 
                CALL WHENEQ(NLPTS,VAL,1,+1.E0,IVAL,NVALI) 
                IF(NVALI.NE.0) THEN 
                  NT=NT+1 
                  NMAX(NT)=NVALI
                  LOCFST(NT)=IVAL(1)
                ENDIF 
                CALL WHENEQ(NLPTS,VAL,1,-1.E0,IVAL,NVALL) 
                IF(NVALL.NE.0) THEN 
                  NT=NT+1 
                  NMAX(NT)=NVALL
                  LOCFST(NT)=IVAL(1)
                ENDIF 
                MAXINT=MAX(NVALL,NVALW)
                NEWMAX=MAX(MAXINT,NVALI) 
                IF(NT.EQ.3.AND.(NVALL.LT.(NVALW+NVALI)).AND.NVALL.EQ. 
     1             NEWMAX) THEN 
                  NT=2
                  NEWMAX=MAX(NVALW,NVALI)
                ELSE IF(NT.EQ.3.AND.(NVALL.EQ.(NVALW+NVALI)).AND.
     1             NVALL.EQ.NEWMAX) THEN 
                  NT=3
                  NMAX(1)=NVALL 
                  NMAX(2)=NVALL 
                  NMAX(3)=NVALL 
                ENDIF 
              ELSE  
C 
C               * DO THE GENERAL CALCULATION. 
C 
                DO 500 N=1,NLPTS
                  IF(N.EQ.1) THEN 
                    NT=0
                    NUM=0 
                  ELSE
                    CALL WHENEQ(N-1,VAL,1,VAL(N),IVAL,NUM)
                  ENDIF 
                  IF(NUM.EQ.0) THEN 
                    NT=NT+1 
                    CALL WHENEQ(NLPTS,VAL,1,VAL(N),IVAL,NVAL) 
                    LOCFST(NT)=N
                    NMAX(NT)=NVAL 
                    IF(NVAL.GT.NEWMAX) NEWMAX=NVAL
                  ENDIF 
  500           CONTINUE
              ENDIF 
C 
C             * IF ONLY ONE VALUE EXISTS IN THE LOW-RESOLUTION GRID SQUARE, 
C             * RETURN ITS VALUE. 
C 
              IF(NT.EQ.1) THEN
                GLR(I,J)=VAL(LOCFST(1)) 
              ELSE
C 
C               * IF ONLY ONE UNIQUE-VALUED POINT HAS THE MOST FREQUENT OCCURRENCE
C               * NEWMAX, RETURN ITS VALUE. 
C 
                CALL WHENEQI(NT,NMAX,1,NEWMAX,IVAL,NVAL) 
                IF(NVAL.EQ.1) THEN
                  GLR(I,J)=VAL(LOCFST(IVAL(1))) 
                ELSE
C 
C                 * OTHERWISE, SEARCH THROUGH THE UNIQUE-VALUED POINTS TO 
C                 * DETERMINE THE VALUES HAVING THE MOST FREQUENT OCCURRENCE AS 
C                 * NEWMAX AND THEIR LOCATION IN ARRAY VAL (DEFINED BY ARRAY
C                 * LOCAT). 
C 
                  NPP=0 
                  DO 600 N=1,NT 
                    IF(NMAX(N).EQ.NEWMAX) THEN
                      TEMP=VAL(LOCFST(N)) 
                      CALL WHENEQ(NLPTS,VAL,1,TEMP,IVAL,NVAL) 
                      DO 550 IJK=1,NVAL 
                        NPP=NPP+1 
                        LOCAT(NPP)=IVAL(IJK)
  550                 CONTINUE
                    ENDIF 
  600             CONTINUE
C 
C                 * CHOOSE THE CLOSEST POINT TO THE TARGET HAVING THAT MAXIMUM- 
C                 * OCCURRING VALUE.
C 
                  DISTMIN=1.E20 
                  DO 700 N=1,NPP
                    IF(DIST(LOCAT(N)).LT.DISTMIN) THEN
                      DISTMIN=DIST(LOCAT(N))
                      NCHOICE=LOCAT(N)
                    ENDIF 
  700             CONTINUE
                  GLR(I,J)=VAL(NCHOICE) 
                ENDIF 
              ENDIF 
            ENDIF
          ENDIF
  800   CONTINUE
 900  CONTINUE
C
C     * FOR TROUBLE POINTS WITH NO HIGH-RES CONTRIBUTION, TRY
C     * USING NEAREST-NEIGHBOUR INFORMATION.
C
      NBADPTS=0
      DO I=1,NLOMAX
         LONB(I)=0.
         LATB(I)=0.
      ENDDO    
      IF(NBADPTSX.NE.0)                                 THEN
         CALL  GGFILL(GLR,GCLR,DLON,DLAT,LONB,LATB,LONBX,LATBX,
     1                ILG,ILG1,ILAT,NBADPTS,NBADPTSX,
     2                NLOMAX,ICHOICE,PI,SPVAL,OK)
      ENDIF
C
C     * REPEAT GREENWICH MERIDIAN AS EXTRA CYCLIC LONGITUDE.
C
      DO 950 J=1,ILAT
        GLR(ILG+1,J)=GLR(1,J)
  950 CONTINUE
C
      RETURN
      END
      SUBROUTINE GGFILL(GLR,MASK,DLON,DLAT,LONB,LATB,LONBX,LATBX,
     1                  ILG,ILG1,ILAT,NBADPTS,NBADPTSX,
     2                  NLOMAX,ICHOICE,PI,SPVAL,OK)
c      
C     * FEB 18/20 - M.LAZARE. PASS IN/OUT NBADPTSX,LONBX,LATBX,NLOMAX ALSO.
C     * MAR 07/08 - M.LAZARE. (MODIFIED TO ADD IF/ENDIF ON NLPTS.GT.0
C     *                        WHEN ICHOICE=0, ELSE GETS MEMORY FAULT).
C     * SEP 18/06 - F.MAJAESS (MODIFIED FOR TOLERANCE IN SPVAL)
C     * MAY 28/04 - M. LAZARE. BUGFIX TO INITIALIZE NEWMAX TO ZERO BEFORE
C     *                        LOOP 150.
C     * MAY 14/04 - M. LAZARE. CHANGE THE DIMENSION OF "LONB(1),
C     *                        LATB(1)" TO "*".
C     * OCT 30/97 - M. LAZARE.

C     * SUBROUTINE TO TRY AND "BORROW" INFORMATION FROM SURROUNDING LOW-
C     * RESOLUTION GRID POINTS TO DEFINE INITIAL FIELDS AT POINTS WHERE
C     * THERE IS NO HIGH-RESOLUTION INFORMATION AVAILABLE. 
C
C     * COMPASS DIRECTIONS N,NE,E,SE,S,SW,W,NW ARE CONSIDERED (NDIR=8).
C     * ANY OF THESE POINTS WHOSE LAND MASK MATCHES THAT OF THE POINT
C     * IN QUESTION ARE USED IN AN AREA-AVERAGED SENSE. THE SUBROUTINE
C     * ABORTS IF THIS IS NOT AN AREA-AVERAGING CASE (ICHOICE=1).
C
C     * NOTE THAT IF THE VALUE OF THE SURROUNDING POINT FIELD IS "SPVAL"
C     * THIS POINT IS NOT USED IN THE CALCULATION, SINCE THIS SPECIAL
C     * VALUE IS THE ONE INITIALLY STORED IN THE FIRST PASS THROUGH
C     * THE DATA, I.E. IT HAS NOT BEEN PROCESSED YET (I.E. NORTH AND/OR
C     * EAST OF THE CURRENT PROBLEM POINT).
C
C     * IF THIS IS STILL NOT SUCCESSFUL, THE ROUTINE PASSES "OK=.FALSE."
C     * BACK UP THROUGH "HRTOLR3" TO THE PHYSICS INITIALIZATION
C     * PROGRAM, WHICH SUBSEQUENTLY ABORTS.

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      PARAMETER (NDIR=8)
C
C     * INPUT/OUTPUT FIELDS:
C
      REAL GLR(ILG1,ILAT), MASK(ILG1,ILAT)
      REAL DLON(ILG), DLAT(ILAT)
      INTEGER LONB (NLOMAX), LATB (NLOMAX)
      INTEGER LONBX(NLOMAX), LATBX(NLOMAX)
C
C     * INTERNAL ARRAYS:
C
      REAL VAL(NDIR), DIST(NDIR)
      INTEGER NMAX(NDIR), LOCFST(NDIR), IVAL(NDIR), LOCAT(NDIR)
      INTEGER LON(NDIR), LAT(NDIR) 
C
      LOGICAL OK
C===========================================================================
C     * FIRST ABORT IF NOT AREA-AVERAGING.
C
      IF(ICHOICE.NE.0 .AND. ICHOICE.NE.1)     CALL XIT('GGFILL',-1)

C     * SETUP IN "SPVALT" THE TOLERANCE TO CHECK "SPVAL" AGAINST.

      SPVALT=1.E-6*SPVAL
C
C     * LOOP OVER BAD POINTS.
C
      OK=.TRUE.
      DO 300 NN=1,NBADPTSX
          I=LONBX(NN)
          J=LATBX(NN)
C
C         * DEFINE LAT/LON INDICES OF SURROUNDING POINTS IN A CLOCKWISE
C         * SENSE (NORTH=1,NE=2,EAST=3,...NW=8).
C
          IF(J.EQ.1)            THEN
              NPTS=5   
              LAT(1)=2
              LAT(2)=2
              LAT(3)=1
              LAT(4)=1
              LAT(5)=2    
              LON(1)=I 
              IF(I.EQ.1)               THEN   
                  LON(2)=2
                  LON(3)=2   
                  LON(4)=ILG1-1
                  LON(5)=ILG1-1     
              ELSE IF(I.EQ.ILG1-1)     THEN  
                  LON(2)=1
                  LON(3)=1   
                  LON(4)=ILG1-2
                  LON(5)=ILG1-2     
              ELSE
                  LON(2)=I+1
                  LON(3)=I+1   
                  LON(4)=I-1
                  LON(5)=I-1     
              ENDIF
          ELSE IF(J.EQ.ILAT)    THEN
              NPTS=5
              LAT(1)=ILAT
              LAT(2)=ILAT-1
              LAT(3)=ILAT-1
              LAT(4)=ILAT-1
              LAT(5)=ILAT
              LON(3)=I 
              IF(I.EQ.1)               THEN   
                  LON(1)=2
                  LON(2)=2   
                  LON(4)=ILG
                  LON(5)=ILG     
              ELSE IF(I.EQ.ILG)     THEN  
                  LON(1)=1
                  LON(2)=1   
                  LON(4)=ILG-1
                  LON(5)=ILG-1     
              ELSE
                  LON(1)=I+1
                  LON(2)=I+1   
                  LON(4)=I-1
                  LON(5)=I-1     
              ENDIF
          ELSE      
              NPTS=8
              LAT(1)=J+1
              LAT(2)=J+1
              LAT(3)=J
              LAT(4)=J-1
              LAT(5)=J-1
              LAT(6)=J-1
              LAT(7)=J
              LAT(8)=J+1          
              LON(1)=I 
              LON(5)=I 
              IF(I.EQ.1)               THEN   
                  LON(2)=2
                  LON(3)=2   
                  LON(4)=2
                  LON(6)=ILG
                  LON(7)=ILG     
                  LON(8)=ILG
              ELSE IF(I.EQ.ILG)     THEN  
                  LON(2)=1
                  LON(3)=1   
                  LON(4)=1
                  LON(6)=ILG-1
                  LON(7)=ILG-1     
                  LON(8)=ILG-1 
              ELSE
                  LON(2)=I+1
                  LON(3)=I+1   
                  LON(4)=I+1
                  LON(6)=I-1     
                  LON(7)=I-1
                  LON(8)=I-1
              ENDIF     
          ENDIF
C   
          IF(ICHOICE.EQ.0 )                                   THEN
C
C 
C           * FOR DISCRETE-VALUED FIELDS, KEEP TRACK OF EACH QUALIFYING HIGH- 
C           * RESOLUTION POINT VALUE IN ARRAY VAL, AND THEIR DISTANCE FROM THE
C           * LOW-RESOLUTION GRID POINT IN ARRAY DIST.
C 
            NLPTS=0
            DO 100 ID=1,NPTS
              II=LON(ID)
              JJ=LAT(ID)
              DISTLAT=DLAT(J)-DLAT(JJ)
              FACTL=(COS(DLAT(J)*PI/180.E0))**2 
              IF(MASK(II,JJ).EQ.MASK(I,J) .AND. 
     1              ABS(GLR(II,JJ)-SPVAL).GT.SPVALT)THEN
                NLPTS=NLPTS+1
                VAL(NLPTS)=GLR(II,JJ) 
                DISTRY=ABS(DLON(I)-DLON(II))
                DISTLON=MIN(DISTRY,(360.E0-DISTRY)) 
                DIST(NLPTS)=(FACTL*(DISTLON**2)+DISTLAT**2)*
     1                      ((PI/180.E0)**2)
              ENDIF
  100       CONTINUE
C
C           * NOW DO THE REMAINING CALCULATION. 
C 
            IF(NLPTS.GT.0)                              THEN
             NEWMAX=0
             DO 150 N=1,NLPTS
              IF(N.EQ.1) THEN 
                NT=0
                NUM=0 
              ELSE
                CALL WHENEQ(N-1,VAL,1,VAL(N),IVAL,NUM)
              ENDIF 
              IF(NUM.EQ.0) THEN 
                NT=NT+1 
                CALL WHENEQ(NLPTS,VAL,1,VAL(N),IVAL,NVAL) 
                LOCFST(NT)=N
                NMAX(NT)=NVAL 
                IF(NVAL.GT.NEWMAX) NEWMAX=NVAL
              ENDIF 
  150        CONTINUE
C 
C            * IF ONLY ONE VALUE EXISTS IN THE LOW-RESOLUTION GRID SQUARE, 
C            * RETURN ITS VALUE. 
C 
             IF(NT.EQ.1) THEN
              GLR(I,J)=VAL(LOCFST(1)) 
             ELSE
C 
C             * IF ONLY ONE UNIQUE-VALUED POINT HAS THE MOST FREQUENT 
C             * OCCURRENCE (NEWMAX), RETURN ITS VALUE. 
C 
              CALL WHENEQI(NT,NMAX,1,NEWMAX,IVAL,NVAL) 
              IF(NVAL.EQ.1) THEN
                GLR(I,J)=VAL(LOCFST(IVAL(1))) 
              ELSE
C 
C               * OTHERWISE, SEARCH THROUGH THE UNIQUE-VALUED POINTS TO 
C               * DETERMINE THE VALUES HAVING THE MOST FREQUENT OCCURRENCE AS 
C               * NEWMAX AND THEIR LOCATION IN ARRAY VAL (DEFINED BY ARRAY
C               * LOCAT). 
C 
                NPP=0 
                DO 175 N=1,NT 
                  IF(NMAX(N).EQ.NEWMAX) THEN
                    TEMP=VAL(LOCFST(N)) 
                    CALL WHENEQ(NLPTS,VAL,1,TEMP,IVAL,NVAL) 
                    DO 170 IJK=1,NVAL 
                      NPP=NPP+1 
                      LOCAT(NPP)=IVAL(IJK)
  170               CONTINUE
                  ENDIF 
  175           CONTINUE
C 
C               * CHOOSE THE CLOSEST POINT TO THE TARGET HAVING THAT MAXIMUM- 
C               * OCCURRING VALUE.
C 
                DISTMIN=1.E20 
                DO 180 N=1,NPP
                  IF(DIST(LOCAT(N)).LT.DISTMIN) THEN
                    DISTMIN=DIST(LOCAT(N))
                    NCHOICE=LOCAT(N)
                  ENDIF 
  180           CONTINUE
                GLR(I,J)=VAL(NCHOICE) 
              ENDIF 
             ENDIF 
            ENDIF

          ELSE
C 
C           * FOR CONTINUOUS FIELDS, AREA-AVERAGE THESE POINTS. 
C 
            BIGAREA=0.E0
            SUM=0.E0
            NLPTS=0     
C
            DO 200 ID=1,NPTS
              II=LON(ID)
              JJ=LAT(ID)
C
              IF(JJ.EQ.ILAT) THEN 
                  SLATP=90.E0 
              ELSE
                  SLATP=0.5E0*(DLAT(JJ+1)+DLAT(JJ)) 
              ENDIF 
              IF(JJ.EQ.1) THEN
                  SLATM=-90.E0
              ELSE
                  SLATM=0.5E0*(DLAT(JJ-1)+DLAT(JJ)) 
              ENDIF 
              FACTLAT=(COS(DLAT(JJ)*PI/180.E0))*(SLATP-SLATM)*PI/180.E0 
C
              IF(II.EQ.ILG)     THEN
                  IIM1=II-1
                  SLONP=0.5E0*(DLON(II)+360.E0)
                  SLONM=0.5E0*(0.E0-DLON(ILG))
              ELSE IF(II.EQ.1)THEN
                  IIP1=II+1
                  SLONP=0.5E0*(DLON(IIP1)+DLON(II))
                  SLONM=0.5E0*(DLON(II)+0.E0)
              ELSE 
                  IIP1=II+1
                  IIM1=II-1
                  SLONP=0.5E0*(DLON(IIP1)+DLON(II))
                  SLONM=0.5E0*(DLON(IIM1)+DLON(II))
              ENDIF
C
              IF(MASK(II,JJ).EQ.MASK(I,J) .AND. 
     1              ABS(GLR(II,JJ)-SPVAL).GT.SPVALT)THEN
                  NLPTS=NLPTS+1
                  AREA=FACTLAT*((SLONP-SLONM)*PI/180.E0)
                  SUM=SUM+AREA*GLR(II,JJ) 
                  BIGAREA=BIGAREA+AREA
              ENDIF
  200       CONTINUE     
C
            IF(NLPTS.NE.0) THEN 
              GLR(I,J)=SUM/BIGAREA
            ENDIF
          ENDIF
C
C         * FINALLY, PRINT OUT RESULTS OF CALCULATION OR WARNING
C         * MESSAGE.
C
          IF(NLPTS.NE.0) THEN
              PRINT *, '0SUCCESSFUL FILL! I,J,MASK,GLR = ',I,J,
     1                   MASK(I,J),GLR(I,J)
              PRINT *, ' SURROUNDING POINTS USED WERE:'
              DO 250 ID=1,NPTS
                  II=LON(ID)
                  JJ=LAT(ID) 
                  IF(MASK(II,JJ).EQ.MASK(I,J) .AND. 
     1              ABS(GLR(II,JJ)-SPVAL).GT.SPVALT)THEN
                      PRINT *, ' I,J,MASK,GLR = ',II,JJ,
     1                           MASK(II,JJ),GLR(II,JJ)
                  ENDIF
  250         CONTINUE
          ELSE
              OK=.FALSE.
              NBADPTS=NBADPTS+1
              LONB(NBADPTS)=I
              LATB(NBADPTS)=J
              PRINT *, '0UNSUCCESSFUL FILL! NO VALID SURROUNDING PTS!'
              PRINT *, ' NO POINTS FOUND WITHIN GRID SQUARE CENTRED AT',
     1                 ' I,J = ',I,J
           ENDIF
  300 CONTINUE
C
      RETURN
      END
