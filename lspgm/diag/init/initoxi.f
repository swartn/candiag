      PROGRAM INITOXI
C     PROGRAM INITOXI (ICTL,       GPINIT,       GPOXI,       GSOXI,            I2
C                                                            OUTPUT,    )       I2
C    1           TAPE1=ICTL, TAPE2=GPINIT, TAPE3=GPOXI, TAPE4=GSOXI,
C    2                                                 TAPE6=OUTPUT)
C     --------------------------------------------------------------            I2
C                                                                               I2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       I2
C     JUN 25/2000 - M. LAZARE (BASED ON INITGS8/INITG10)                        I2
C                                                                               I2
CINITOXI - INTERPOLATES CHEM. OXIDANTS FROM PRESS TO HYBRID             2  1    I1
C                                                                               I3
CAUTHOR  - M.LAZARE                                                             I3
C                                                                               I3
CPURPOSE - CONVERTS A PRESSURE LEVEL GG (192X96) DATASET OF INITIAL             I3
C          CHEMISTRY OXIDANTS (OH,H2O2,O3,NO3,HNO3,NH3,NH4) TO A MODEL          I3
C          HYBRID LEVEL DATASET (ON SAME GG).                                   I3
C                                                                               I3
CINPUT FILES...                                                                 I3
C                                                                               I3
C      ICTL   = INITIALIZATION CONTROL DATASET (SEE ICNTRL9).                   I3
C      GPINIT = STANDARD INIT DATASET USED TO GET SURFACE PRESSURE.             I3
C      GPOXI  = PRESSURE LEVEL 192X96 GAUSSIAN GRIDS. (12 MONTHS).              I3
C               THE FIRST FIVE OXIDANTS ARE ON THE STANDARD 19 WMO              I3
C               PRESSURE LEVELS FROM 1 MB TO 1000 MB, WHILE THE LAST            I3
C               ARE ON 10 PRESSURE LEVELS FROM 100 MB TO 1000 MB (DIFFERENT     I3
C               DATASETS.                                                       I3 
C                                                                               I3
C                 NAME   VARIABLE          UNITS                                I3
C                                                                               I3
C                   OH   OH                VMR                                  I3
C                 H2O2   H2O2              VMR                                  I3
C                   O3   O3                VMR                                  I3
C                  NO3   NO3               VMR                                  I3
C                 HNO3   HNO3              VMR                                  I3
C                  NH3   NH3               VMR                                  I3
C                  NH4   NH4               VMR                                  I3
C                                                                               I3
COUTPUT FILE...                                                                 I3
C                                                                               I3
C      GSOXI  = SIGMA/HYBRID LEVEL GAUSSIAN GRIDS OF...  (12 MONTHS)            I3
C                                                                               I3
C                 NAME   VARIABLE          UNITS                                I3
C                                                                               I3
C                   OH   OH                VMR                                  I3
C                 H2O2   H2O2              VMR                                  I3
C                   O3   O3                VMR                                  I3
C                  NO3   NO3               VMR                                  I3
C                 HNO3   HNO3              VMR                                  I3
C                  NH3   NH3               VMR                                  I3
C                  NH4   NH4               VMR                                  I3
C----------------------------------------------------------------------------
C
      use diag_sizes, only : SIZES_LONP1xLAT,
     &                       SIZES_LONP1xLATxNWORDIO,
     &                       SIZES_MAXLEV,
     &                       SIZES_MAXLEVxLONP1xLAT

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      PARAMETER (NMO=12, NOXI=7, NOX1=5, NOX2=2, NLOMAX=SIZES_LONP1xLAT,
     &           MAXLEV=SIZES_MAXLEV, NLDAT=SIZES_MAXLEVxLONP1xLAT, 
     &           NDAT=SIZES_MAXLEV*SIZES_LONP1xLATxNWORDIO,
     &           MAXLVP1=MAXLEV+1)
C
      INTEGER IBUF(8),IDAT(NDAT)
      COMMON/ICOM/IBUF,IDAT
      REAL, ALLOCATABLE, DIMENSION(:) :: X, Y
      REAL, ALLOCATABLE, DIMENSION(:) :: GZS, SP

      LOGICAL OK
      CHARACTER*4 STRING

      INTEGER KBUF(8)
      INTEGER LP(MAXLEV), LP1(MAXLEV), LP2(MAXLEV)
      INTEGER LG(MAXLEV), LH(MAXLEV)
      INTEGER KOXI(NOXI), MMD(NMO)

      REAL, ALLOCATABLE, DIMENSION(:) :: AG, BG, 
     &                                   AH, BH, SG 
      REAL, ALLOCATABLE, DIMENSION(:) :: SH, PR, PR1, PR2,
     &                                   SIG, PRLOG 
      REAL, ALLOCATABLE, DIMENSION(:) :: FPCOL, DFDLNP, DLNP 

C     DATA KOXI/ 4H  OH, 4HH2O2, 4H  O3, 4H NO3, 4HHNO3, 4H NH3, 4H NH4/
      DATA  MMD/ 16, 46, 75,106,136,167,197,228,259,289,320,350/
C-----------------------------------------------------------------------
      KOXI(1)=NC4TO8("  OH")
      KOXI(2)=NC4TO8("H2O2")
      KOXI(3)=NC4TO8("  O3")
      KOXI(4)=NC4TO8(" NO3")
      KOXI(5)=NC4TO8("HNO3")
      KOXI(6)=NC4TO8(" NH3")
      KOXI(7)=NC4TO8(" NH4")
C
      NFIL=5
      CALL JCLPNT(NFIL,1,2,3,4,6)
C
      MAXX=NDAT

C     * GET THE PRESSURE LEVEL VALUES AND THE ANALYSIS GRID SIZE
C     * FROM THE OH FIELDS IN FILE GGOXI.
C     * THIS WILL APPLY FOR THE FIRST FIVE OXIDANTS AND IS
C     * REPEATED FOR THE NH3/NH4 FIELDS WHICH HAVE DIFFERENT LEVELS. 

      REWIND 3
      CALL FIND(3,NC4TO8("GRID"),-1,NC4TO8("  OH"),-1,OK)
      IF(.NOT.OK) CALL                             XIT('INITOXI',-1)
      CALL FILEV(LP1 ,NPL1,KBUF,-3)
      IF((NPL1.LT.1).OR.(NPL1.GT.MAXLEV)) CALL     XIT('INITOXI',-2)
      CALL LVDCODE(PR1,LP1,NPL1)
      ALLOCATE(PR1(NPL1))
      ILG =KBUF(5)-1
      ILAT=KBUF(6)
      WRITE(6,6005) NPL1,(PR1(L),L=1,NPL1)
C
      REWIND 3
      CALL FIND(3,NC4TO8("GRID"),-1,NC4TO8(" NH3"),-1,OK)
      IF(.NOT.OK) CALL                             XIT('INITOXI',-3)
      CALL FILEV(LP2 ,NPL2,KBUF,-3)
      IF((NPL2.LT.1).OR.(NPL2.GT.MAXLEV)) CALL     XIT('INITOXI',-4)
      ALLOCATE(PR2(NPL2))
      CALL LVDCODE(PR2,LP2,NPL2)
      WRITE(6,6005) NPL2,(PR2(L),L=1,NPL2)
      IF(KBUF(5)-1.NE.ILG .OR. KBUF(6).NE.ILAT)     THEN
        PRINT *, '0ILG,ILAT,KBUF(5),KBUF(6) = ',ILG,ILAT,KBUF(5),KBUF(6)
        CALL                                       XIT('INITOXI',-5)
      ENDIF

C     * READ MODEL SIGMA VALUES FROM THE CONTROL FILE.

      ALLOCATE(SG(MAXLEV), SH(MAXLEV)) 
      REWIND 1
      READ(1,END=903) LABL,NSL,(SG(L),L=1,NSL)
     1                        ,(SH(L),L=1,NSL),LAY,ICOORD,PTOIT,MOIST
      IF((NSL.LT.1).OR.(NSL.GT.MAXLEV)) CALL       XIT('INITOXI',-6)
      CALL WRITLEV(SG,NSL,' SG ')
      CALL WRITLEV(SH,NSL,' SH ')
      WRITE(6,6016) LAY,ICOORD,PTOIT
      READ(1,END=904) LABL,MLG,MLAT,ILGM,ILATM,LRLMT,IDAY,GMT
C
      ALLOCATE(AG(NSL), BG(NSL), 
     &         AH(NSL), BH(NSL))
      CALL SIGLAB2 (LG,LH, SG,SH, NSL)
      CALL COORDAB (AG,BG, NSL,SG, ICOORD,PTOIT)
      CALL COORDAB (AH,BH, NSL,SH, ICOORD,PTOIT)

C     * STOP IF THERE IS NOT ENOUGH SPACE.

      ILG1=ILG+1
      LEN = ILAT*ILG1
      NPL = MAX(NPL1,NPL2)
      IF(    LEN.GT.NLOMAX) CALL                   XIT('INITOXI',-7)
      IF(NPL*LEN.GT.NLDAT)  CALL                   XIT('INITOXI',-8)
      IF(NSL*LEN.GT.NLDAT)  CALL                   XIT('INITOXI',-9)
      ALLOCATE(PR(NPL), SIG(NPL), PRLOG(NPL)) 
      ALLOCATE(FPCOL(NPL), DFDLNP(NPL+1), DLNP(NPL))
      ALLOCATE(GZS(ILG1*ILAT), SP(ILG1*ILAT))
      ALLOCATE(X(ILG1*ILAT*NPL), Y(ILG1*ILAT*NPL))
C
C     * READ MOUNTAIN GEOPOTENTIAL (M/SEC)**2 ON ANALYSIS GAUSSIAN GRID.
C
      REWIND 1 
      CALL GETFLD2(1,GZS,NC4TO8("GRID"),-1,NC4TO8("PHIS"),1,
     +                                         IBUF,MAXX,OK)
      IF(.NOT.OK) CALL                             XIT('INITOXI',-10)
      IF(IBUF(5)-1.NE.ILG .OR. IBUF(6).NE.ILAT)     THEN
        PRINT *, '0ILG,ILAT,IBUF(5),IBUF(6) = ',ILG,ILAT,IBUF(5),IBUF(6)
        CALL                                       XIT('INITOXI',-11)
      ENDIF
      WRITE(6,6025) (IBUF(IND),IND=1,8) 
C 
C     * READ SURFACE PRESSURE ON ANALYSIS GAUSSIAN GRID.
C 
      REWIND 2
      CALL GETFLD2(2,SP,NC4TO8("GRID"),-1,NC4TO8("  PS"),1,IBUF,MAXX,OK)
      IF(.NOT.OK) CALL                             XIT('INITOXI',-12)
      IF(IBUF(5)-1.NE.ILG .OR. IBUF(6).NE.ILAT)     THEN
        PRINT *, '0ILG,ILAT,IBUF(5),IBUF(6) = ',ILG,ILAT,IBUF(5),IBUF(6)
        CALL                                       XIT('INITOXI',-13)
      ENDIF
      WRITE(6,6025) (IBUF(IND),IND=1,8) 
C
C     * CONVERT TO LNSP.
C
      DO 120 I=1,LEN
         SP(I)=LOG(SP(I))
  120 CONTINUE 
      MAXP8=MAXX+8
C
C     * LOOP OVER ALL OXIDANTS AND MONTHS.
C
      DO 850 M=1,NMO
        IMO=MMD(M)
        REWIND 3
        DO 800 N=1,NOXI
          KOX=KOXI(N)
          IF(N.LE.NOX1) THEN
            NPL=NPL1
            DO 200 L=1,NPL
              PR(L)=PR1(L)
              LP(L)=LP1(L)
              PRLOG(L) = LOG(PR1(L)) 
  200       CONTINUE
          ELSE
            NPL=NPL2
            DO 250 L=1,NPL
              PR(L)=PR2(L)
              LP(L)=LP2(L)
              PRLOG(L) = LOG(PR2(L)) 
  250       CONTINUE
          ENDIF   
C
C         * PERFORM THE VERTICAL AND HORIZONTAL INTERPOLATIONS.
C

          CALL PTOX(3,4,KOX,IMO,X,Y,GZS,SP,LEN,NPL,NSL,
     1             AG,BG,AH,BH,NPL+1,FPCOL,DFDLNP,DLNP,
     2             LP,LG,LH,SIG,PRLOG,IBUF,MAXP8)
  800   CONTINUE
  850 CONTINUE

      CALL                                         XIT('INITOXI',0)

C     * E.O.F. ON FILE ICTL.

  903 CALL                                         XIT('INITOXI',-14)
  904 CALL                                         XIT('INITOXI',-15)
C-----------------------------------------------------------------------
 6005 FORMAT('0  PRES LEVELS',I5,20F5.0/('0',18X,20F5.0))
 6010 FORMAT(I6,A4,/,(10X,10F6.3))
 6016 FORMAT(' LAY=',I5,', COORD=',A4,', P.LID=',F10.3,' (PA)')
 6025 FORMAT(' ',A4,I10,1X,A4,5I6)
      END
