      PROGRAM ICNTRL7 
C     PROGRAM ICNTRL7 (GPINIT,       LLMTNS,        ICTL,       INPUT,          I2
C    1                                                          OUTPUT, )       I2
C    2           TAPE1=GPINIT, TAPE2=LLMTNS, TAPE99=ICTL, TAPE5=INPUT,
C    3                                                    TAPE6=OUTPUT) 
C     -----------------------------------------------------------------         I2
C                                                                               I2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       I2
C     APR 30/03 - F.MAJAESS (DUE TO NAME CONFLICT, REPLACE "IDATE" BY "IDATEC") I2
C     JUN 13/97 - F.MAJAESS (ABORT IF ILEV > MAXLEV)                            
C     FEB 20/95 - F.MAJAESS (CHANGE "PHIS" PACKING DENSITY FROM 4:1 to 1:1)     
C     FEB 15/94 - F.MAJAESS (REVISE FOR TRUNCATIONS > 99)                       
C     JAN 18/93 - E. CHAN   (CHANGE FORMATTED READ OF ETA LEVELS TO             
C                            FORMATTED READ OF CODED PRESSURE LEVELS)          
C     JUL 22/92 - E. CHAN   (REPLACE UNFORMATTED I/O WITH I/O ROUTINES)       
C     JUL 13/92 - E. CHAN   (DIMENSION SELECTED VARIABLES AS REAL*8)         
C     JAN 29/92 - E. CHAN   (CONVERT HOLLERITH LITERALS TO ASCII)           
C     JAN 31/91 - F.MAJAESS (ABORT IF " SIG" IS SPECIFIED FOR "ICOORD")        
C     DEC 22/89 - D.VERSEGHY. MODIFY TO READ/WRITE IHYD.
C     MAY 24/88 - M.LAZARE. (DERIVED FROM ICNTRL5 AND ICNTRLH). 
C                                                                               I2
CICNTRL7 - CREATES THE INITIALIZATION CONTROL DATASET FOR GCM7          2  1 C  I1
C                                                                               I3
CAUTHOR  - M.LAZARE                                                             I3
C                                                                               I3
CPURPOSE - CREATES THE INITIALIZATION CONTROL DATASET FOR USE IN                I3
C          THE GCM INITIALIZATION SEQUENCE, HYBRID MODEL. IT MUST               I3
C          BE THE FIRST PROGRAM RUN IN ANY INITIALIZATION SEQUENCE              I3
C          SINCE ALL OF THE INITIALIZATION PROGRAMS REQUIRE THAT                I3
C          DATASET.                                                             I3
C          IT CONTAINS CONTROL PARAMETERS READ FROM CARDS AND ALSO              I3
C          A MOUNTAIN FIELD INTERPOLATED FROM A LAT-LONG GRID TO THE            I3
C          ANALYSIS GAUSSIAN GRID AND SPECTRALLY SMOOTHED TO THE                I3
C          SAME RESOLUTION AS THE MODEL. THIS IS THE ONLY PROGRAM               I3
C          IN THE INITIALIZATION THAT READS CARDS.                              I3
C                                                                               I3
CINPUT FILES...                                                                 I3
C                                                                               I3
C      GPINIT = PRESSURE LEVEL GAUSSIAN GRIDS OF FIELDS THAT WILL BE            I3
C               USED TO START THE MODEL. THIS PROGRAM READS ONLY ONE            I3
C               TEMPERATURE FIELD TO GET THE ANALYSIS GRID SIZE.                I3
C      LLMTNS = LAT-LONG PHYSICS DATASET. THIS PROGRAM READS ONLY THE           I3
C               MOUNTAIN FIELD FROM THIS DATASET.                               I3
C                                                                               I3
COUTPUT FILE...                                                                 I3
C                                                                               I3
C      ICTL   = INITIALIZATION CONTROL DATASET. IT CONTAINS ALL THE             I3
C               INFORMATION READ FROM CARDS (SEE BELOW) AND THE                 I3
C               SPECTRALLY SMOOTHED MOUNTAINS ON THE ANALYSIS                   I3
C               GAUSSIAN GRID.                                                  I3
C                                                                               I3
CINPUT PARAMETERS...                                                            I5
C                                                                               I5
C      ILEV       = NUMBER OF MODEL SIGMA LEVELS (<= 50).                       I5
C      ILGM,ILATM = NUMBER OF LONGITUDE AND LATITUDE POINTS TO BE USED IN       I5
C                   THE G.C.M.                                                  I5
C      LRT,LMT    = MODEL SPECTRAL TRUNCATION WAVE NUMBERS.                     I5
C                   A RHOMBOIDAL FIELD WILL HAVE SIZE (LRT+1,LMT+1)             I5
C      KTR        = TRUNCATION TYPE (0=RHOMBOIDAL, 2=TRIANGULAR).               I5
C      IDAY       = JULIAN DAY OF THE YEAR THE MODEL STARTS FROM.               I5
C      LAY        = LAYERING CONVENTION FOR LEVELS VS LAYERS                    I5
C                   (SEE SUBROUTINE BASCAL).                                    I5
C      GMT        = GREENWICH MEAN TIME ON IDAY FROM WHICH MODEL STARTS.        I5
C      ICOORD     = 4HET10/4HET15/4H ETA FOR HYBRID VERTICAL COORDINATE.        I5
C      PTOIT      = PRESSURE (PA) WHERE UPPER BOUNDARY CONDITION IS APPLIED.    I5
C      MOIST      = CHOICE OF WATER VAPOUR MOISTURE VARIABLE,                   I5
C                   4HT-TD FOR DEW POINT DEPRESSION                             I5
C                   4H  TD FOR DEW POINT                                        I5
C                   4H   Q FOR SPECIFIC HUMIDITY,                               I5
C                   4HRLNQ FOR -1./LN(Q),                                       I5
C                   4HSQRT FOR Q**0.5.                                          I5
C      IHYD       = CODE INDICATING DESIRED LAND SURFACE SCHEME                 I5 
C                   0  FOR "OLD" HYDROLOGY SCHEME                               I5 
C                   1  FOR "NEW" HYDROLOGY SCHEME                               I5 
C                   2  FOR CLASS ("CANADIAN LAND SURFACE SCHEME")               I5
C                                                                               I5
C      LG         = VALUE OF MID MOMENTUM LAYER (UP TO 50 VALUES) IN CODED      I5
C                   PRESSURE LEVELS.                                            I5
C                                                                               I5
C      LH         = VALUE OF MID THERMODYNAMIC LAYER (UP TO 50 VALUES) IN       I5
C                   CODED PRESSURE LEVELS.                                      I5
C                                                                               I5
CEXAMPLE OF INPUT CARDS...                                                      I5
C                                                                               I5
C* ICNTRL7   10   64   32   20   20    2    1                                   I5
C*  0 0.00  ETA      500. RLNQ    1                                             I5
C* 10   32   80  150  235  360  550  750  880  970                              I5
C*  0    0    0    0    0    0    0    0    0    0                              I5
C*  0    0    0    0    0    0    0    0    0    0                              I5
C*  0    0    0    0    0    0    0    0    0    0                              I5
C*  0    0    0    0    0    0    0    0    0    0                              I5
C* 18   51  110  188  291  445  642  812  924  985                              I5
C*  0    0    0    0    0    0    0    0    0    0                              I5
C*  0    0    0    0    0    0    0    0    0    0                              I5
C*  0    0    0    0    0    0    0    0    0    0                              I5
C*  0    0    0    0    0    0    0    0    0    0                              I5
C------------------------------------------------------------------------------ 
C 
      use diag_sizes, only : SIZES_BLAT,
     &                       SIZES_BLONP1,
     &                       SIZES_BLONP1xBLAT,
     &                       SIZES_BLONP1xBLATxNWORDIO,
     &                       SIZES_LA,
     &                       SIZES_LMTP1,
     &                       SIZES_LONP1xLAT,
     &                       SIZES_MAXLEV

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)


      LOGICAL OK
      CHARACTER*4 STRING
  
      REAL*8 SL(SIZES_BLAT),CL(SIZES_BLAT),WL(SIZES_BLAT),
     & WOSSL(SIZES_BLAT)
      REAL SG(50),SH(50),LLGC(SIZES_BLONP1xBLAT)
      INTEGER LSR(2,SIZES_LMTP1+1), LG(50), LH(50)
  
      COMPLEX SP
      COMMON /BLANCK/ SP  (SIZES_LA*SIZES_MAXLEV)
      COMMON /BLANCK/ GG  (SIZES_LONP1xLAT),GLL (SIZES_BLONP1xBLAT),
     & WRKL(SIZES_BLONP1+5) 
      COMMON /BLANCK/ GC  (SIZES_LONP1xLAT),LLGC
      COMMON /SCM   / WRKS(3*INT((SIZES_BLONP1+4)/2.E0),2)
      COMMON /ALPCOM/ ALP   (SIZES_LA+SIZES_LMTP1),
     & DALP(SIZES_LA+SIZES_LMTP1) 
      COMMON /ALPCOM/ DELALP(SIZES_LA+SIZES_LMTP1),
     & EPSI(SIZES_LA+SIZES_LMTP1) 
      COMMON /GAUS  / SL,CL,WL,WOSSL
      COMMON /GAUS  / RAD(SIZES_BLAT),ANG(SIZES_BLAT),
     & DLON(SIZES_BLONP1)
      COMMON /ICOM  / IBUF(8),IDAT(SIZES_BLONP1xBLATxNWORDIO) 
  
      DATA MAXX/SIZES_BLONP1xBLATxNWORDIO/, MAXLEV/50/ 
C---------------------------------------------------------------------- 
      NFIL=5
      CALL JCLPNT (NFIL,1,2,99,5,6) 
  
C     * GET THE ANALYSIS GRID SIZE FROM GPINIT. 
  
      REWIND 1
      CALL FIND (1,NC4TO8("GRID"),-1,NC4TO8("TEMP"),-1,OK)
      IF(.NOT.OK) CALL                             XIT('ICNTRL7',-1)
      CALL FBUFFIN(1,IBUF,-8,K,LEN)
      IF (K.GE.0) GOTO 903
      CALL PRTLAB (IBUF)
      ILG1 =IBUF(5) 
      ILAT =IBUF(6) 
      ILG  =ILG1-1
      ILATH=ILAT/2
  
C     * READ MODEL PARAMETERS FROM CARDS. WRITE CONTROL FILE RECORDS. 
  
      READ(5,5010,END=904) ILEV,ILGM,ILATM,LRT,LMT,KTR,IDAY,LAY,                I4
     1                     GMT,ICOORD,PTOIT,MOIST,IHYD                          I4
      IF (ILEV.GT.MAXLEV) THEN
        WRITE(6,6018) ILEV,MAXLEV
        CALL                                       XIT('ICNTRL7',-2)
      ENDIF
      READ(5,5012,END=905) (LG(L),L=1,MAXLEV)                                   I4
      READ(5,5012,END=905) (LH(L),L=1,MAXLEV)                                   I4
  
C     * CONVERT CODED PRESSURE LEVELS TO ETA/SIGMA LEVELS.

      CALL LVDCODE(SG,LG,MAXLEV)
      CALL LVDCODE(SH,LH,MAXLEV)
      DO 100 L=1,MAXLEV
        SG(L) = SG(L) * 0.001E0
        SH(L) = SH(L) * 0.001E0
  100 CONTINUE 
C
C     LRLMT=1000*(LRT+1)+10*(LMT+1)+KTR 
      CALL FXLRLMT (LRLMT,LRT+1,LMT+1,KTR)
      LABL=NC4TO8("LABL")
      REWIND 99 
      WRITE(99) LABL,ILEV,(SG(L),L=1,ILEV), 
     1                    (SH(L),L=1,ILEV),LAY,ICOORD,PTOIT,MOIST 
      WRITE(99) LABL,ILG,ILAT,ILGM,ILATM,LRLMT,IDAY,GMT,IHYD
C 
C     * PRINT OUT PARAMETERS.
C
      CALL WRITLEV(SG,ILEV,' SG ')
      CALL WRITLEV(SH,ILEV,' SH ')
C
      WRITE(6,6017) ILG,ILAT,ILGM,ILATM 
      CALL IDATEC  (MONTH,MDAY,IDAY)
      WRITE(6,6019) IDAY,MONTH,MDAY 
      WRITE(6,6021) LAY,ICOORD,PTOIT,GMT,MOIST
      WRITE(6,6022) IHYD
      IF((ICOORD.EQ.NC4TO8("    ")).OR.(ICOORD.EQ.NC4TO8(" SIG")))
     A  CALL                                       XIT('ICNTRL7',-3)
  
C     * COMPUTE CONSTANTS.
  
      CALL DIMGT  (LSR,LA,LR,LM,KTR,LRLMT)
      CALL PRLRLMT (LA,LR,LM,KTR,LRLMT)
      CALL EPSCAL (EPSI,LSR,LM) 
      CALL GAUSSG (ILATH,SL,WL,CL,RAD,WOSSL)
      CALL  TRIGL (ILATH,SL,WL,CL,RAD,WOSSL)
  
      DO 110 I=1,ILAT 
  110 ANG(I)=RAD(I)*180.E0/3.14159E0
  
      DGX=360.E0/(FLOAT(ILG)) 
      DO 120 I=1,ILG1 
        DLON(I)=DGX*FLOAT(I-1)
  120 CONTINUE
C 
C     * GET LAND-WATER MASK GROUND COVER FIELDS TO BE USED IN HRALR.
C     * IF THIS ROUTINE ABORTS BECAUSE GAUSSIAN GRID IS OF SAME SIZE OR LARGER
C     * THAN INITIAL GRID, REDO USING EXTRACTION. 
C 
      CALL GETFLD2(-2,LLGC,NC4TO8("GRID"),1,NC4TO8("  GC"),1,
     +                                          IBUF,MAXX,OK)
      IF(.NOT.OK) CALL                             XIT('ICNTRL7',-4)
      LLL=IBUF(5)*IBUF(6) 
      DO 160 I=1,LLL
        LLGC(I)=MERGE(0.E0,LLGC(I),LLGC(I).EQ.1.E0) 
  160 CONTINUE
      CALL HRTOLR(GC,ILG1,ILAT,DLON,ANG,LLGC,IBUF(5),IBUF(6),2,OK,
     1            LONBAD,LATBAD)
      IF(.NOT.OK)THEN 
        WRITE(6,6030) LONBAD,LATBAD 
        CALL LLEGG2(GC,ILG1,ILAT,ANG,LLGC,IBUF(5),IBUF(6))
      ENDIF 
C 
C     * GET THE LAT-LONG MOUNTAINS AND CONVERT TO GAUSSIAN GRID.
C     * USE ROUTINE HRALR WITH THE GROUND COVER FIELDS OBTAINED ABOVE.
C     * IF THIS ROUTINE ABORTS BECAUSE GAUSSIAN GRID IS OF SAME SIZE OR LARGER
C     * THAN INITIAL GRID, REDO USING INTERPOLATION.
C 
      CALL GETFLD2(-2,GLL,NC4TO8("GRID"),0,NC4TO8("  ZS"),1,
     +                                         IBUF,MAXX,OK)
      IF(.NOT.OK) CALL                             XIT('ICNTRL7',-5)
      CALL PRTLAB (IBUF)
      NLG1=IBUF(5)
      NLAT=IBUF(6)
      INTERP=1
      CALL HRALR(GG,ILG1,ILAT,DLON,ANG,GLL,NLG1,NLAT,GC,LLGC,1,OK,
     1           LONBAD,LATBAD) 
      IF(.NOT.OK)THEN 
        WRITE(6,6030) LONBAD,LATBAD 
        CALL LLIGG2(GG,ILG1,ILAT,ANG,GLL,NLG1,NLAT,INTERP)
      ENDIF 
C 
C     * CONVERT FROM HEIGHT (METERS) TO GEOPOTENTIAL (GZ).
C 
      LGG  =ILG1*ILAT 
      DO 210 I=1,LGG
  210 GG(I)=GG(I)*9.80616E0 
  
C     * DO A SPECTRAL ANALYSIS TO THE SAME RESOLUTION AS THE MODEL. 
C     * THEN RESTORE IT TO THE GAUSSIAN GRID. 
C     * THIS IS DONE SO THAT THE INTERPOLATION FROM PRESSURE TO ETA/SIG 
C     * WILL USE THE SAME MOUNTAINS AS THE MODEL. 
  
      CALL GGAST2 (SP,LSR,LM,LA, GG,ILG1,ILAT,1,SL,WL,
     1             ALP,EPSI,WRKS,WRKL)
      CALL STAGG2 (GG,ILG1,ILAT,1,SL, SP,LSR,LM,LA, 
     1             ALP,EPSI,WRKS,WRKL)
  
C     * PUT THE MOUNTAINS ON THE CONTROL FILE.
  
      CALL SETLAB  (IBUF,NC4TO8("GRID"),0,NC4TO8("PHIS"),1,
     +                                       ILG1,ILAT,0,1)
      CALL PUTFLD2 (99,GG,IBUF,MAXX) 
      CALL PRTLAB (IBUF)
      CALL                                         XIT('ICNTRL7',0) 
  
C     * E.O.F. ON FILE GPINIT.
  
  903 CALL                                         XIT('ICNTRL7',-6)
  
C     * E.O.F. ON INPUT.
  
  904 CALL                                         XIT('ICNTRL7',-7)
  905 CALL                                         XIT('ICNTRL7',-8)
C-----------------------------------------------------------------------
 5010 FORMAT (10X,7I5,/,I5,E5.0,1X,A4,E10.0,1X,A4,I5)                           I4
 5012 FORMAT (10I5)                                                             I4
 6000 FORMAT (I5,A4,'LEVELS',/,(5X,10(1PE9.2)))
 6005 FORMAT (5X,10F9.5)
 6010 FORMAT (5X,10F9.3)
 6017 FORMAT ('0 ANALYSIS GRID=',2I5,5X,'MODEL GRID =',2I5)
 6018 FORMAT ('0 CAN NOT HANDLE ',I3,' LEVELS; MAXIMUM IS ',I3)
 6019 FORMAT ('0 IDAY =',I6,'  (',A3,I3,')')
 6021 FORMAT (' LAY=',I5,', COORD=',A4,', P.LID=',F10.3,' (PA)',
     1        /' GMT=',F10.3,',  MOIST=',A4)
 6022 FORMAT (10X,'  IHYD=',I4)
 6030 FORMAT('0NO POINTS FOUND WITHIN GRID SQUARE CENTRED AT (',I3,',',
     1       I3,')')
      END
