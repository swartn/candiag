      PROGRAM ZONDEV
C     PROGRAM ZONDEV (GGIN,       ZMIN,       GGDEV,       OUTPUT,      )       D2
C    1          TAPE1=GGIN, TAPE2=ZMIN, TAPE3=GGDEV, TAPE6=OUTPUT)
C     ------------------------------------------------------------              D2
C                                                                               D2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       D2
C     NOV 06/95 - F.MAJAESS (REVISE DEFAULT PACKING DENSITY VALUE)              
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                
C     MAY 14/84 - R.LAPRISE.                                                    
C     NOV 05/80 - J.D.HENDERSON 
C                                                                               D2
CZONDEV  - COMPUTES ZONAL DEVIATIONS OF A GRID SET                      2  1    D1
C                                                                               D3
CAUTHOR  - J.D.HENDERSON                                                        D3
C                                                                               D3
CPURPOSE - COMPUTES A SET OF GAUSSIAN GRIDS CONTAINING DEVIATIONS FROM          D3
C          THE PRECOMPUTED ZONAL MEANS AND SAVES THEM ON FILE GGDEV.            D3
C                                                                               D3
CINPUT FILES...                                                                 D3
C                                                                               D3
C      GGIN  = MULTI-LEVEL SET OF GAUSSIAN GRIDS.                               D3
C      ZMIN  = CORRESPONDING SERIES OF ZONAL MEAN CROSS-SECTIONS.               D3
C                                                                               D3
COUTPUT FILE...                                                                 D3
C                                                                               D3
C      GGDEV = GAUSSIAN GRIDS OF DEVIATIONS FROM THE ZONAL MEANS.               D3
C------------------------------------------------------------------------------ 
C 
      use diag_sizes, only : SIZES_LAT,
     &                       SIZES_LONP1xLAT,
     &                       SIZES_LONP1xLATxNWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      COMMON/BLANCK/ZM(SIZES_LAT),F(SIZES_LONP1xLAT) 
C 
      LOGICAL OK
      COMMON/ICOM/ IBUF(8),IDAT(SIZES_LONP1xLATxNWORDIO) 
      DATA MAXX/SIZES_LONP1xLATxNWORDIO/
C-------------------------------------------------------------------- 
      NFF=4 
      CALL JCLPNT(NFF,1,2,3,6)
      REWIND 1
      REWIND 2
      REWIND 3
C 
C     * READ THE NEXT GRID AND ZONAL MEAN.
C 
      NR=0
  110 CALL GETFLD2(1,F,NC4TO8("GRID"),-1,-1,-1,IBUF,MAXX,OK)
      IF(.NOT.OK)THEN 
        WRITE(6,6030) NR
        IF(NR.EQ.0) CALL                           XIT('ZONDEV',-1) 
        CALL                                       XIT('ZONDEV',0)
      ENDIF 
      IF(NR.EQ.0) THEN
        WRITE(6,6025) IBUF
        NPACK=MIN(2,IBUF(8))
      ENDIF
      NAME=IBUF(3)
      LEVEL=IBUF(4) 
      NLG=IBUF(5) 
      NLAT=IBUF(6)
C 
      CALL GETFLD2(2,ZM,NC4TO8("ZONL"), -1, -1 ,LEVEL,IBUF,MAXX,OK)
      IF(.NOT.OK)THEN 
        WRITE(6,6030) NR
        CALL                                       XIT('ZONDEV',-2) 
      ENDIF 
      IF(IBUF(5).NE.NLAT) CALL                     XIT('ZONDEV',-3) 
      IF(NR.EQ.0) THEN
        WRITE(6,6025) IBUF
        NPACK=MIN(NPACK,IBUF(8))
      ENDIF
C 
C     * COMPUTE THE ZONAL DEVIATIONS FOR THIS GRID. 
C 
      DO 150 J=1,NLAT 
      N=(J-1)*NLG 
      ZMEAN=ZM(J) 
      DO 140 I=1,NLG
  140 F(N+I)=F(N+I)-ZMEAN 
  150 CONTINUE
C 
C     * WRITE THE GRID OF ZONAL DEVIATIONS ONTO FILE 3. 
C 
      CALL SETLAB(IBUF,NC4TO8("GRID"),-1,NAME,-1,NLG,NLAT,-1,NPACK)
      CALL PUTFLD2(3,F,IBUF,MAXX)
      IF(NR.EQ.0) WRITE(6,6025) IBUF
      NR=NR+1 
      GO TO 110 
C-------------------------------------------------------------------- 
 6025 FORMAT(' ',A4,I10,2X,A4,I10,4I6)
 6030 FORMAT('   ZONDEV READ',I5,'  PAIRS OF RECORDS')
      END
