      PROGRAM GGLOB 
C     PROGRAM GGLOB (GGH,       GGG,       INPUT,       OUTPUT,         )       D2
C    1         TAPE1=GGH, TAPE2=GGG, TAPE5=INPUT, TAPE6=OUTPUT) 
C     ---------------------------------------------------------                 D2
C                                                                               D2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       D2
C     NOV 06/95 - F.MAJAESS (REVISE TO ALLOW NHEM=3)                           
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                
C     NOV 21/84 - B.DUGAS.                                                     
C     DEC 17/80 - J.D.HENDERSON 
C                                                                               D2
CGGLOB   - CONVERTS HEMI GRID OR CROSS-SECTION FILE TO GLOBAL           1  1 C  D1
C                                                                               D3
CAUTHOR  - J.D.HENDERSON                                                        D3
C                                                                               D3
CPURPOSE - GLOBALIZES A FILE (GGH) OF  NORTHERN OR SOUTHERN HEMISPHERIC         D3
C          GAUSSIAN GRIDS OR CROSS-SECTIONS AND PUTS THE RESULTS IN GGG         D3
C          FILE.                                                                D3
C          NOTE - IF GGH FILE IS ALREADY GLOBAL IT IS JUST COPIED.              D3
C                 THE  GENERATED  HEMISPHERE IS FILLED BY SYMMETRY  OR          D3
C                 ANTI-SYMMETRY DEPENDING ON THE VALUE OF KSM FOUND ON          D3
C                 THE INPUT CARD.                                               D3
C                                                                               D3
CINPUT FILE...                                                                  D3
C                                                                               D3
C      GGH = NORTHERN OR SOUTHERN HEMISPHERE GRIDS OR CROSS-SECTIONS            D3
C                                                                               D3
COUTPUT FILE...                                                                 D3
C                                                                               D3
C      GGG = CORRESPONDING GLOBAL GRIDS OR CROSS-SECTIONS                       D3
C 
CINPUT PARAMETERS...
C                                                                               D5
C      KSM  = 1, FOR      SYMMETRIC SOUTHERN HEMISPHERE                         D5
C            -1, FOR ANTI-SYMMETRIC SOUTHERN HEMISPHERE                         D5
C                                                                               D5
CEXAMPLE OF INPUT CARD...                                                       D5
C                                                                               D5
C*GGLOB       1                                                                 D5
C-------------------------------------------------------------------------
C 
      use diag_sizes, only : SIZES_LONP1xLAT,
     &                       SIZES_LONP1xLATxNWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      LOGICAL OK
      LOGICAL GRID,ZONL 
      COMMON/BLANCK/GH(SIZES_LONP1xLAT),GG(SIZES_LONP1xLAT) 
C 
      COMMON/ICOM/IBUF(8),IDAT(SIZES_LONP1xLATxNWORDIO) 
      DATA MAXX/SIZES_LONP1xLATxNWORDIO/
C---------------------------------------------------------------------
      NFF=4 
      CALL JCLPNT(NFF,1,2,5,6)
      REWIND 1
      REWIND 2
C 
C     * READ KSM FROM INPUT CARD. 
C     * IF KSM = 1, THE FILE CONTAINS SYMMETRIC DATA
C     * IF KSM =-1, THE FILE CONTAINS ANTI-SYMMETRIC DATA 
C     * ANYTHING ELSE DEFAULTS TO -1. 
C 
      READ(5,5010,END=905) KSM                                                  D4
      IF (KSM.NE.1.AND.KSM.NE.-1) KSM=-1
C 
C     * READ THE NEXT RECORD FROM FILE GGH. 
C 
      NR=0
  150 CALL GETFLD2(1,GH,-1,-1,-1,-1,IBUF,MAXX,OK)
      IF(.NOT.OK)THEN 
        IF(NR.EQ.0)THEN 
          CALL                                     XIT('GGLOB',-1)
        ELSE
          WRITE(6,6010) NR
          CALL                                     XIT('GGLOB',0) 
        ENDIF 
      ENDIF 
      IF(NR.EQ.0) WRITE(6,6025) IBUF
C 
C     * PROCESS ONLY GRIDS AND CROSS SECTIONS.
C 
      NTYPE=IBUF(1) 
      GRID=(NTYPE.EQ.NC4TO8("GRID"))
      ZONL=(NTYPE.EQ.NC4TO8("ZONL"))
      IF(.NOT.(GRID.OR.ZONL)) GO TO 150 
C 
      IF(GRID)THEN
        NLG=IBUF(5) 
        NLATH=IBUF(6) 
      ENDIF 
      IF(ZONL)THEN
        NLG=1 
        NLATH=IBUF(5) 
      ENDIF 
      NWDH=NLATH*NLG
C 
C     * IF NHEM=0 OR 3, THE FIELD IS ALREADY GLOBAL.
C     * IN THIS CASE JUST TRANSFER THE WHOLE GRID TO GG.
C     * OTHERWISE NHEM = 1,2 FOR N,S HEMISPHERE.
C 
      NHEM=IBUF(7)
      IF(NHEM.NE.0 .AND. NHEM.NE.3 ) GO TO 250 
      DO 210 I=1,NWDH 
  210 GG(I)=GH(I) 
      GO TO 350 
C 
C     * TRANSFER INPUT GRID TO NORTHERN HALF OF GLOBAL GRID.
C     * THEN FILL IN THE SOUTHERN HALF. 
C 
  250 CALL HAGG2(GG,GH,NLG,NLATH,NHEM)
      CALL GGSYM2(GG,NLG,NLATH,0.E0,NHEM,KSM) 
      IF(GRID) IBUF(6)=NLATH*2
      IF(ZONL) IBUF(5)=NLATH*2
C 
C     * SAVE THE GLOBAL FIELD ON FILE GGG.
C 
  350 IF( IBUF(7).NE.3 .AND. IBUF(7).NE.0 ) IBUF(7)=0 
      CALL PUTFLD2(2,GG,IBUF,MAXX) 
      IF(NR.EQ.0) WRITE(6,6025) IBUF
      NR=NR+1 
      GO TO 150 
C 
C     * END OF FILE ON UNIT 5.
C 
  905 CALL                                         XIT('GGLOB',-2)
C---------------------------------------------------------------------
 5010 FORMAT(10X,I5)                                                            D4
 6010 FORMAT( '0',I6,'  RECORDS READ')
 6025 FORMAT(' ',A4,I10,2X,A4,I10,4I6)
      END
