      PROGRAM ADDCYC
C     PROGRAM ADDCYC (X,       Y,       OUTPUT,                         )       D2
C    1          TAPE1=X, TAPE2=Y, TAPE6=OUTPUT)
C     ----------------------------------                                        D2
C                                                                               D2
C     FEB 13/20 - S.KHARIN                                                      D2
C                                                                               D2
CADDCYC  - ADD A CYCLIC LONGITUDE.                                      1  1   GD1
C                                                                               D3
CAUTHOR  - S.KHARIN                                                             D3
C                                                                               D3
CPURPOSE - ADD A CYCLIC LONGITUDE.                                              D3
C                                                                               D3
C          NOTE - CYCLIC LONGITUDE IS ADDED ONLY IF NUMBER OF LONGITUDES        D3
C                 IS EVEN, OTHERWISE NO CHANGES ARE MADE.                       D3
C                                                                               D3
CINPUT FILE...                                                                  D3
C                                                                               D3
C      X = INPUT CCRN FILE.                                                     D3
C                                                                               D3
COUTPUT FILE...                                                                 D3
C                                                                               D3
C      Y = OUTPUT CCRN FILE WITH A CYCLIC LONGITUDE.                            D3
C                                                                               D3
C--------------------------------------------------------------------------
C
      use diag_sizes, only : SIZES_BLONP1xBLAT,
     &                       SIZES_BLONP1xBLATxNWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      PARAMETER(
     & NWDSX=SIZES_BLONP1xBLAT,
     & NWDSX2=SIZES_BLONP1xBLATxNWORDIO)
      LOGICAL OK
C
      REAL A(NWDSX),B(NWDSX)
      COMMON/ICOM/IBUF(8),IDAT(NWDSX2)
C
      DATA MAXX/NWDSX2/
C---------------------------------------------------------------------
      NFF=3
      CALL JCLPNT(NFF,1,2,6)
      NFF=NFF-1
      IF (NFF.LT.2)  CALL                          XIT('ADDCYC',-1)

      REWIND 1
      REWIND 2

C     * READ THE NEXT FIELD.

      NR=0
  100 CALL GETFLD2(1,A,-1,-1,-1,-1,IBUF,MAXX,OK)
      IF (.NOT.OK) THEN
        IF(NR.EQ.0) CALL                           XIT('ADDCYC',-2)
        WRITE(6,6010) NR
        CALL                                       XIT('ADDCYC',0)
      ENDIF
      NR=NR+1
      IF (NR.EQ.1) CALL PRTLAB(IBUF)
      IF((IBUF(1).EQ.NC4TO8("LABL")).OR.
     +      (IBUF(1).EQ.NC4TO8("CHAR")))     THEN 
       CALL PUTFLD2(2,A,IBUF,MAXX)
       GO TO 100  
      ENDIF

      IF (IBUF(1).NE.4HGRID.AND. IBUF(1).NE.4HSUBA) 
     1     CALL                                    XIT('ADDCYC',-3)

      NLON=IBUF(5)
      NLAT=IBUF(6)
C
C     * DO NOT ADD CYCLIC IF NLON IS ODD
C
      IF (MOD(NLON,2).NE.0) THEN
        IF (NR.EQ.1) CALL PRTLAB(IBUF)
        CALL PUTFLD2(2,A,IBUF,MAXX)
      ELSE
        IJ=0
        IJ1=0
        DO J=1,NLAT
          DO I=1,NLON
            IJ=IJ+1
            IJ1=IJ1+1
            B(IJ1)=A(IJ)
          ENDDO
          IJ1=IJ1+1
          B(IJ1)=A((J-1)*IBUF(5)+1)
        ENDDO
        IBUF(5)=NLON+1
        IF (NR.EQ.1) CALL PRTLAB(IBUF)
        CALL PUTFLD2(2,B,IBUF,MAXX)
      ENDIF
      GO TO 100
C---------------------------------------------------------------------
 6010 FORMAT(' ',I10,' RECORDS PROCESSED')
      END
