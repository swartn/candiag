      PROGRAM EXTFIL
C     PROGRAM EXTFIL (INFIL,       OUTFIL,       INPUT,       OUTPUT,   )       D2
C    1          TAPE1=INFIL, TAPE2=OUTFIL, TAPE5=INPUT, TAPE6=OUTPUT)
C     ---------------------------------------------------------------           D2
C                                                                               D2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       D2
C     FEB 10/94 - D. RAMSDEN (ALLOW FOR SHIFTED GRIDS <---> GG)                 D2
C     JUL 13/92 - E. CHAN  (DIMENSION SELECTED VARIABLES AS REAL*8)             
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)
C     JULY 11/85 - M.LAZARE.
C                                                                               D2
CEXTFIL  - CONVERTS FROM ONE TYPE OF GRID TO ANOTHER BY EXTRACTION      1  1 C  D1
C                                                                               D3
CAUTHOR  - M. LAZARE                                                            D3
C                                                                               D3
CPURPOSE - CONVERTS A FILE FROM ONE SIZE GRID TO ANOTHER BY EXTRACTION.         D3
C          EITHER OF THE  GRIDS MAY BE GAUSSIAN  OR LAT-LONG. THE INPUT         D3
C          CARDS READ DETERMINE WHICH IS THE CONVERSION TAKING PLACE.           D3
C                                                                               D3
CINPUT FILE...                                                                  D3
C                                                                               D3
C      INFIL  = INPUT ARRAY, TYPE DETERMINED BY IEXT.                           D3
C                                                                               D3
COUTPUT FILE...                                                                 D3
C                                                                               D3
C      OUTFIL = OUTPUT ARRAY, TYPE DETERMINED BY IEXT.                          D3
C
CINPUT PARAMETERS...
C                                                                               D5
C      ILG  = NUMBER OF EQUALLY-SPACED LONGITUDES IN RESULTING FIELD:           D5
C             IBUF(5) = ILG+1 (UNSHIFTED GRIDS).                                D5
C      ILAT = NUMBER OF LATITUDES IN RESULTING FIELD: IBUF(6)=ILAT              D5
C      IEXT = TYPE OF CONVERSION TAKING PLACE, I.E.:                            D5
C             IEXT=0     :     GG TO LL                                         D5
C             IEXT=1     :     LL TO GG                                         D5
C             IEXT=2     :     LL TO LL                                         D5
C             IEXT=3     :     GG TO GG                                         D5
C             IEXT=4     :     GG TO SHIFTED LL                                 D5
C             IEXT=5     :     SHIFTED LL TO GG                                 D5
C             IEXT>5     :     PROGRAM ABORTS                                   D5
C   SHFTLT =  LATITUDE  SHIFT OF GRID IN DEGREES (INPUT OR OUTPUT)              D5
C   SHFTLG =  LONGITUDE SHIFT OF GRID IN DEGREES (INPUT OR OUTPUT)              D5
C                                                                               D5
CEXAMPLE OF INPUT CARD WITHOUT GRID SHIFT...                                    D5
C                                                                               D5
C*EXTFIL    128   64    1                                                       D5
C                                                                               D5
CEXAMPLE OF INPUT CARD WITH A GRID SHIFT...                                     D5
C                                                                               D5
C*EXTFIL    360  180    4       0.5       0.5                                   D5
C-----------------------------------------------------------------------
C
      use diag_sizes, only : SIZES_BLAT,
     &                       SIZES_BLONP1xBLAT,
     &                       SIZES_BLONP1xBLATxNWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      REAL OLDG(SIZES_BLONP1xBLAT),NEWG(SIZES_BLONP1xBLAT)

      LOGICAL OK,SHIFTIN,SHIFTOUT
      REAL NEWANG(SIZES_BLAT),OLDANG(SIZES_BLAT)
      REAL*8 SL(SIZES_BLAT),CL(SIZES_BLAT),WL(SIZES_BLAT),
     & WOSSL(SIZES_BLAT),RAD(SIZES_BLAT) 
      COMMON/ICOM/IBUF(8),IDAT(SIZES_BLONP1xBLATxNWORDIO)

      DATA MAXX/SIZES_BLONP1xBLATxNWORDIO/
C---------------------------------------------------------------------
      NFF=4
      CALL JCLPNT(NFF,1,2,5,6)
      REWIND 1

C     * READ SIZE OF DESIRED GRID, ALONG WITH TYPE OF CONVERSION REQUIRED.
C     * ABORT IF CONVERSION DESIRED IS NOT ONE OF ABOVE CHOICES.

      READ(5,5010,END=910) ILG,ILAT,IEXT,SHFTLT,SHFTLG                          D4
      WRITE(6,6010) ILG,ILAT,IEXT
C
      SHIFTIN=.FALSE.
      SHIFTOUT=.FALSE.
C
      IF(IEXT.GT.3)THEN
         IF(IEXT.EQ.4)THEN
           SHIFTOUT=.TRUE.
           WRITE(6,6050)SHFTLT,SHFTLG
C
         ELSE
           SHIFTIN=.TRUE.
           WRITE(6,6051)SHFTLT,SHFTLG
         ENDIF
      ELSE
         SHFTLT=0.E0
         SHFTLG=0.E0
      ENDIF
C
      IF(IEXT.LT.0.OR.IEXT.GT.5) CALL              XIT('EXTFIL',-1)
C
C     DETERMINE THE NUMBER OF OUTPUT GRID POINTS
C
      IF(.NOT.SHIFTOUT)THEN
         ILG1=ILG+1
         ILATM1=ILAT-1
      ELSE
         ILG1=ILG
         ILATM1=ILAT
      ENDIF

C     * IF THE RESULTING GRID TYPE IS A GAUSSIAN GRID, CALCULATE THE GAUSSIAN
C     * LATITUDES BASED ON ILAT.

      IF(IEXT.EQ.1.OR.IEXT.EQ.3.OR.IEXT.EQ.5) THEN
         ILATH=ILAT/2

C     * GAUSSG COMPUTES THE VALUE OF THE GAUSSIAN LATITUDES AND THEIR
C     * SINES AND COSINES. TRIGL MAKES THEM GLOBAL (S TO N).

        CALL GAUSSG(ILATH,SL,WL,CL,RAD,WOSSL)
        CALL  TRIGL(ILATH,SL,WL,CL,RAD,WOSSL)
        DO 110 I=1,ILAT
  110   NEWANG(I)=RAD(I)*180.E0/3.14159E0
      ELSE

C     * THE REQUIRED GRID TYPE IS A LAT-LONG GRID, SO CALCULATE THE
C     * EQUALLY-SPACED LATITUDES.

        NEWANG(1)=-90.E0
        DEGY=180.E0/(REAL(ILATM1))
        DEGY=1.E-10*ANINT(1.E10*DEGY)
        IF(.NOT.SHIFTOUT)THEN
            DO 120 I=1,ILATM1
  120       NEWANG(I+1)=NEWANG(I)+DEGY
        ELSE
            DO 220 I=1,ILAT
  220       NEWANG(I)=DEGY*REAL(I-1)+SHFTLT-90.E0
        ENDIF
 
      ENDIF

C     * GET THE INPUT FIELD INTO OLDG AND DETERMINE THE GRID DIMENSIONS.

      NR=0
  150 CALL GETFLD2(1,OLDG,NC4TO8("GRID"),-1,-1,-1,IBUF,MAXX,OK)
      IF(.NOT.OK.AND.NR.EQ.0) CALL                 XIT('EXTFIL',-2)
      IF(.NOT.OK) THEN
        CALL PRTLAB(IBUF)
        WRITE(6,6030) NR
        GO TO 900
      ENDIF
      IF(NR.EQ.0) CALL PRTLAB(IBUF)
C
      NLG1=IBUF(5)
      NLAT=IBUF(6)
C
      IF(.NOT.SHIFTIN)THEN
          NLG=NLG1-1
          NLATM1=NLAT-1
      ELSE
          NLG=NLG1
          NLATM1=NLAT
      ENDIF
C
C     * IF THE INPUT GRID TYPE IS A GAUSSIAN GRID, CALCULATE THE GAUSSIAN
C     * LATITUDES BASED ON NLAT.

      IF(IEXT.EQ.0.OR.IEXT.EQ.3.OR.IEXT.EQ.4) THEN
         NLATH=NLAT/2

C     * GAUSSG COMPUTES THE VALUE OF THE GAUSSIAN LATITUDES AND THEIR
C     * SINES AND COSINES. TRIGL MAKES THEM GLOBAL (S TO N).

        CALL GAUSSG(NLATH,SL,WL,CL,RAD,WOSSL)
        CALL  TRIGL(NLATH,SL,WL,CL,RAD,WOSSL)
        DO 130 I=1,NLAT
  130   OLDANG(I)=RAD(I)*180.E0/3.14159E0
      ELSE

C     * THE REQUIRED GRID TYPE IS A LAT-LONG GRID, SO CALCULATE THE
C     * EQUALLY-SPACED LATITUDES.

        OLDANG(1)=-90.E0
        DEGY=180.E0/(REAL(NLATM1))
        DEGY=1.E-10*ANINT(1.E10*DEGY)
        IF(.NOT.SHIFTIN)THEN
C
            DO 140 I=1,NLATM1
  140       OLDANG(I+1)=OLDANG(I)+DEGY
        ELSE
C
            DO 240 I=1,NLAT
  240       OLDANG(I)=DEGY*REAL(I-1)+SHFTLT-90.E0
        ENDIF
      ENDIF

C     * PERFORM THE GRID CONVERSION BY EXTRACTION.

      CALL EXTRAC2(NEWG,ILG1,ILAT,NEWANG,OLDG,NLG1,NLAT,OLDANG
     1                  ,SHFTLG,SHIFTIN,SHIFTOUT)

C     * SAVE THE RESULTING FIELD IN NEWG.

      IBUF(5)=ILG1
      IBUF(6)=ILAT
      CALL PUTFLD2(2,NEWG,IBUF,MAXX)
      NR=NR+1
      GO TO 150

C     * E.O.F. ON FILE OLDG.

  900 CALL                                         XIT('EXTFIL',0)

C     * E.O.F. ENCOUNTERED WHILE READING INPUT.

  910 CALL                                         XIT('EXTFIL',-3)
C---------------------------------------------------------------------
 5010 FORMAT(10X,3I5,2F10.2)                                                    D4
 6010 FORMAT('0ILG,ILAT,IEXT =',3I5)
 6030 FORMAT('0',I5,'  RECORDS READ')
 6050 FORMAT('0','OUTPUT GRID IS SHIFTED IN LAT/LONG ',2F5.2,' DEGREES')
 6051 FORMAT('0',' INPUT GRID IS SHIFTED IN LAT/LONG ',2F5.2,' DEGREES')
      END
