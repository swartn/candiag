      PROGRAM ZONAVG
C     PROGRAM ZONAVG (X,       ZX,       XS,       ZXS2,      OUTPUT,   )       D2
C    1         TAPE11=X,TAPE12=ZX,TAPE13=XS,TAPE14=ZXS2,TAPE6=OUTPUT)           D2
C     ---------------------------------------------------------------           D2
C                                                                               D2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       D2
C     APR 13/00 - S.KHARIN (OPTIONALLY CALCULATE DEVIATIONS AND ZONAL           D2
C                           AVERAGES OF SQUARED DEVIATIONS)                     D2
C     NOV 06/95 - F.MAJAESS (REVISE DEFAULT PACKING DENSITY VALUE)
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)
C     MAY 14/83 - R.LAPRISE.
C     FEB 18/80 - J.D.HENDERSON
C                                                                               D2
CZONAVG  - COMPUTES ZONAL AVERAGE OF A  MULTI-LEVEL FIELD               1  3    D1
C                                                                               D3
CAUTHOR  - J.D.HENDERSON                                                        D3
C                                                                               D3
CPURPOSE - COMPUTES A VECTOR OF ZONAL MEANS FOR EACH GRID AND SAVES             D3
C          THEM ON FILE ZX. OPTIONALLY, ZONAL DEVIATIONS AND THE ZONAL          D3
C          AVERAGE OF SQUARED DEVIATIONS ARE COMPUTED.                          D3
C          NOTE - EACH ZX RECORD IS A VECTOR OF LATITUDINAL AVERAGES            D3
C                 RUNNING SOUTH TO NORTH.                                       D3
C                                                                               D3
CINPUT FILE...                                                                  D3
C                                                                               D3
C      X     = GRIDS TO BE ZONALLY AVERAGED.                                    D3
C                                                                               D3
COUTPUT FILE...                                                                 D3
C                                                                               D3
C      ZX    = ZONAL AVERAGES OF EACH GRID IN X.                                D3
C      XS    = (OPTIONAL) GRIDS OF DEVIATIONS FROM THE ZONAL MEANS.             D3
C      ZXS2  = (OPTIONAL) ZONAL AVERAGES OF SQUARED DEVIATIONS                  D3
C              FROM THE ZONAL MEANS. IF ONLY 'ZXS2' IS REQUIRED AND 'XS' IS NOT D3
C              NEEDED, USE THE UNDERSCORE '_' INSTEAD OF 'XS'.                  D3
CEXAMPLES:                                                                      D3
C     zonavg x zx         # CALCULATE ZONAL AVERAGE.                            D3
C     zonavg x zx xs      # CALCULATE ZONAL AVERAGE AND DEVIATIONS.             D3
C     zonavg x zx xs zxs2 # CALCULATE ZONAL AVERAGE, DEVIATIONS AND             D3
C                         # VARIANCE OF STATIONARY EDDIES.                      D3
C     zonavg x zx _  zxs2 # CALCULATE ZONAL AVERAGE AND STAT. EDDIES.           D3
C-------------------------------------------------------------------------
C
      use diag_sizes, only : SIZES_LAT,
     &                       SIZES_LONP1xLAT,
     &                       SIZES_LONP1xLATxNWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      COMMON/BLANCK/X(SIZES_LONP1xLAT),ZX(SIZES_LAT),ZXS2(SIZES_LAT)
C
      LOGICAL OK,ZONDEV,ZONVAR
C
      COMMON/ICOM/ IBUF(8),IDAT(SIZES_LONP1xLATxNWORDIO)
C
C     * IUA/IUST  - ARRAY KEEPING TRACK OF UNITS ASSIGNED/STATUS.
C
      INTEGER IUA(100),IUST(100)
      COMMON /JCLPNTU/ IUA,IUST
C
      DATA MAXX/SIZES_LONP1xLATxNWORDIO/
C---------------------------------------------------------------------
      NF=5
      CALL JCLPNT(NF,11,12,13,14,6)
      IF (NF.LT.3) CALL                            XIT('ZONAVG',-1)
      REWIND 11
      REWIND 12
C
C     * CHECK IF ZONAL DEVIATIONS ARE REQUESTED.
C
      ZONDEV=.FALSE.
      IF (IUST(13).EQ.1) THEN
         WRITE(6,6005)
         ZONDEV=.TRUE.
         REWIND 13
      ENDIF
C
C     * CHECK IF VARIANCE OF STATIONARY EDDIES IS REQUESTED.
C
      ZONVAR=.FALSE.
      IF (IUST(14).EQ.1) THEN
         WRITE(6,6006)
         ZONVAR=.TRUE.
         REWIND 14
      ENDIF
C
C     * READ THE NEXT GRID FROM THE FILE. STOP AT EOF.
C
      NR=0
  100 CALL GETFLD2(11,X,NC4TO8("GRID"),-1,-1,-1,IBUF,MAXX,OK)
      IF(.NOT.OK)THEN
        IF(NR.EQ.0) CALL                           XIT('ZONAVG',-2)
        WRITE(6,6010) NR
        CALL                                       XIT('ZONAVG',0)
      ENDIF
      IF(NR.EQ.0) THEN
        CALL PRTLAB (IBUF)
        NPACK=MIN(2,IBUF(8))
      ENDIF
C
C     * COMPUTE THE ZONAL AVERAGE FOR THIS GRID.
C     * REMEMBER THAT THE LAST VALUE IN EACH ROW IS A COPY OF THE FIRST.
C
      NLG=IBUF(5)
      NLAT=IBUF(6)
      NLGM=NLG-1
C
      DO 300 J=1,NLAT
      ZXJ=0.E0
      N=(J-1)*NLG
      DO 200 I=1,NLGM
  200 ZXJ=ZXJ+X(N+I)
      ZX(J)=ZXJ/FLOAT(NLGM)
C
C     * COMPUTE THE ZONAL DEVIATIONS FOR X.
C
      IF (ZONDEV.OR.ZONVAR) THEN
        DO 240 I=1,NLG
  240   X(N+I)=X(N+I)-ZX(J)
      ENDIF
C
C     * COMPUTE THE ZONAL AVERAGE OF SQUARED DEVIATIONS.
C
      IF (ZONVAR) THEN
         ZXJ=0.E0
         DO 250 I=1,NLGM
 250     ZXJ=ZXJ+X(N+I)*X(N+I)
         ZXS2(J)=ZXJ/FLOAT(NLGM)
      ENDIF
  300 CONTINUE
C
C     * WRITE OUT THE ZONAL AVERAGE VECTOR.
C
      CALL SETLAB(IBUF,NC4TO8("ZONL"),-1,-1,-1,NLAT,1,-1,NPACK)
      CALL PUTFLD2(12,ZX,IBUF,MAXX)
      IF(NR.EQ.0) CALL PRTLAB (IBUF)
C
C     * WRITE OUT THE GRID OF ZONAL DEVIATIONS.
C
      IF (ZONDEV) THEN
        CALL SETLAB(IBUF,NC4TO8("GRID"),-1,-1,-1,NLG,NLAT,-1,NPACK)
        CALL PUTFLD2(13,X,IBUF,MAXX)
        IF(NR.EQ.0) CALL PRTLAB (IBUF)
      ENDIF
C
C     * WRITE OUT THE ZONAL AVERAGES OF SQUARED DEVIATIONS.
C
      IF (ZONVAR) THEN
        CALL SETLAB(IBUF,NC4TO8("ZONL"),-1,-1,-1,NLAT,1,-1,NPACK)
        CALL PUTFLD2(14,ZXS2,IBUF,MAXX)
        IF(NR.EQ.0) CALL PRTLAB (IBUF)
      ENDIF
      NR=NR+1
      GO TO 100
C---------------------------------------------------------------------
 6005 FORMAT('0 CALCULATE ALSO ZONAL DEVIATIONS.')
 6006 FORMAT('0 CALCULATE ALSO ZONAL AVERAGES OF SQUARED DEVIATIONS.')
 6010 FORMAT('0',I6,'  RECORDS PROCESSED')
      END
