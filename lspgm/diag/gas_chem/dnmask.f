      PROGRAM DNMASK
C     PROGRAM DNMASK (XIN,       XOUT,           ,       OUTPUT,         )
C    1        TAPE1=XIN, TAPE2=XOUT, TAPE6=OUTPUT)
C     ---------------------------------------------------------
C
C     Jan 09/23 - D. Plummer - use diag_sizes and addition of a check to make
C                               sure at least one mask file was written
C     JUL 03/14 - D. PLUMMER (CONVERT HOLLERITH LITERALS TO ASCII AND REVISIONS
C                               FOR F90 AND LINUX PLATFORMS)
C     JAN 11/97 - J.D.(calculate day/night mask)
C
      use diag_sizes, only : SIZES_LONP1xLAT,
     &                       SIZES_LONP1xLATxNWORDIO,
     &                       SIZES_LAT,
     &                       SIZES_MAXLEV

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
C
      PARAMETER (JPSIZ=65341, JPSIZB=130682, MXLEV=100)
C
      REAL GG(JPSIZ)
C
      LOGICAL OK
C
      INTEGER LH(SIZES_MAXLEV)
      COMMON/ICOM/IBUF(8),IDAT(SIZES_LONP1xLATxNWORDIO)
      DATA MAXX /SIZES_LONP1xLATxNWORDIO/
C
C---------------------------------------------------------------------
C
      NFF=4
      CALL JCLPNT(NFF,1,2,5,6)
      REWIND 1

      CALL FILEV(LH,NSL,IBUF,1)
      
      READ(5,5010) IMLEV
      WRITE(6,6007) IMLEV
C
      NR=0
      NW=0
 150  CALL GETFLD2(1,GG,-1,0,0,0,IBUF,MAXX,OK)
      IF(.NOT.OK)THEN
        IF(NR.EQ.0) CALL                           XIT('DNMASK',-1)
        IF(NW.EQ.0) CALL                           XIT('DNMASK',-2)
C        WRITE(6,6025) IBUF
C        WRITE(6,6010) NR
        CALL                                       XIT('DNMASK',0)
      ENDIF
      IF(NR.EQ.0) WRITE(6,6025) IBUF
      NR=NR+1
C
      IF(IBUF(4).EQ.IMLEV) go to 200

      GO TO 150
C
C     * SET THE MASK
C
 200  CONTINUE
C
      WRITE(6,*)'Calculate new mask at level ', IBUF(4)
      NWDS=IBUF(5)*IBUF(6)
      DO 210 I=1,NWDS
         IF (GG(I).GE.1.E-16) THEN
            GG(I)=1.
         ELSE
            GG(I)=-1.
         ENDIF
 210  CONTINUE
C
C     * SAVE ON FILE XOUT.
C
      DO 220 L=1,NSL
         IBUF(3)=NC4TO8("DMSK")
         IBUF(4)=LH(L)
         CALL PUTFLD2(2,GG,IBUF,MAXX)
 220  CONTINUE
      NW=NW+1

      GO TO 150

C---------------------------------------------------------------------
 5010 FORMAT(10X,I5)
 6007 FORMAT('0 DNMASK TARGET LEVEL =',I5)
 6010 FORMAT('0 DNMASK READ',I6,' RECORDS')
 6025 FORMAT(' ',A4,I10,2X,A4,I10,4I6)
      END
