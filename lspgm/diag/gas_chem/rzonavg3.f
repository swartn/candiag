      PROGRAM RZONAVG3
C     PROGRAM RZONAVG3 (X,     D,     ZX,     RZDX,     RZNX,     OUTPUT)       D2
C    1        TAPE1=X, TAPE2=D, TAPE3=ZX, TAPE4=RZDX, TAPE5=RZNX, TAPE6=OUTPUT)
C     ------------------------------------------------------                    D2
C                                                                               D2
C     MAR 29/07 - A. JONSSON  (INCORPORATE LATEST UPDATES FROM POLLUX VERSION   D2
C                              OF RZONAVG)                                      D2
C     JUN 15/02 - A. JONSSON  (DAY AND NIGHT TIME AVERAGES)                     D2
C     APR 13/00 - S.KHARIN (OPTIONALLY CALCULATE DEVIATIONS AND ZONAL           D2
C                           AVERAGES OF SQUARED DEVIATIONS.                     D2
C                           USE MAX PACKING DENSITY = 2)                        D2
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                D2
C     MAY 13/83 - R.LAPRISE.
C                                                                               D2
CRZONAVG - COMPUTES THE REPRESENTATIVE ZONAL AVERAGE OF A FIELD         2  3    D1
C                                                                               D3
CAUTHOR  - R.LAPRISE                                                            D3
C                                                                               D3
CPURPOSE - COMPUTE THE REPRESENTATIVE ZONAL AVERAGE (RZDX) OF A SET             D3
C          OF GRIDS (X) USING (D) AS A MASK.                                    D3
C          NOTE - THERE MUST BE AS MANY TIME STEPS OF (D) AS OF (X),            D3
C                 AND THEIR VERTICAL LEVELS MUST MATCH.                         D3
C                                                                               D3
C          THIS MODIFIED VERSION OF RZONAVG CALCULATES THE ZONAL AVERAGE        D3
C          (RZDX) OF DATA POINTS THAT ARE INSIDE THE MASK (D), E.G. DAY         D3
C          TIME DATA POINTS, AS WELL AS THE ZONAL AVERAGE (RZNX) OF DATA        D3
C          POINTS THAT ARE OUTSIDE THE MASK, E.G. NIGHT TIME DATA POINTS,       D3
C          AND THE COMPLETE (DIURNAL) ZONAL AVERAGE (ZX).                       D3
C                                                                               D3
C          FOR ANY LATITUDE THAT LACKS DAY TIME OR NIGHT TIME DATA POINTS,      D3
C          THE CORRESPONDING ZONAL AVERAGE IS SET TO A NO-DATA-VALUE (SPVAL).   D3
C                                                                               D3
C          IF TIME AVERAGING IS REQUIRED, THE NO-DATA-VALUE OF THE MODIFIED     D3
C          VERSIONS OF RZONAVG (RZONAVGN) AND TIMAVG (TIMAVGN) MUST BE          D3
C          THE SAME! RZONAVGN MUST ALLWAYS BE RUN BEFORE TIMAVGN WHEN USED      D3
C          IN COMBINATION.                                                      D3
C                                                                               D3
CINPUT FILES...                                                                 D3
C                                                                               D3
C      X    = GRIDS TO BE ZONALLY AVERAGED.                                     D3
C      D    = GRIDS OF MASK (ZERO OR NEGATIVE VALUES OUTSIDE MASK AND           D3
C             POSITIVE VALUES INSIDE MASK)                                      D3
C                                                                               D3
COUTPUT FILE...                                                                 D3
C                                                                               D3
C      RZDX = DAY TIME ZONAL MEANS                                              D3
C      RZNX = NIGHT TIME ZONAL MEANS                                            D3
C      ZX = ORDINARY ZONAL MEANS                                                D3
C-------------------------------------------------------------------------
C
      use diag_sizes, only : SIZES_LONP1xLAT,
     &                       SIZES_LONP1xLATxNWORDIO,
     &                       SIZES_LAT,
     &                       SIZES_MAXLEV,
     &                       SIZES_MAXLEVxBLONP1xBLAT

      IMPLICIT REAL (A-H,O-Z),
     +         INTEGER (I-N)

      COMMON/BLANCK/ X(SIZES_LONP1xLAT), D(SIZES_LONP1xLAT), 
     1               RZDX(SIZES_LAT), RZNX(SIZES_LAT), 
     1               ZX(SIZES_LAT), XNOPAK(SIZES_LAT)
C     
      LOGICAL OK, LCKPK
C
      COMMON /ICOM/ IBUF(8),IDAT(SIZES_LONP1xLATxNWORDIO)
      COMMON /JCOM/ JBUF(8),JDAT(SIZES_LONP1xLATxNWORDIO)
C
      DATA MAXX/SIZES_LONP1xLATxNWORDIO/, LCKPK/.TRUE./,
     1     SPVAL/-9.99E33/, FRC/0.001/
C
C     SPVAL - No data value
C     LCKPK - Turns on/off packing error check
C     FRC   - Maximum acceptable relative packing error
C
C-----------------------------------------------------------------------
      NF=6
      CALL JCLPNT(NF,11,12,13,14,15,6)
      IF (NF.LT.5) CALL                            XIT('RZONAVG',-1)                                                                
      REWIND 11
      REWIND 12
c      REWIND 13
C
C     * READ THE NEXT PAIR OF X AND D FIELDS. STOP AT EOF.
C
      NR=0
  100 CALL GETFLD2(11,X,NC4TO8("GRID"),-1,-1,-1,IBUF,MAXX,OK)
      IF(.NOT.OK)THEN
        IF(NR.EQ.0) CALL                           XIT('RZONAVG',-2)
        WRITE(6,6010) NR
        CALL                                       XIT('RZONAVG',0)
      ENDIF
      IF(NR.EQ.0) THEN                                                                                                              
        CALL PRTLAB (IBUF)                                                                                                          
        NPACK=MIN(2,IBUF(8))                                                                                                        
      ENDIF                                                                                                                         
C
      CALL GETFLD2(12,D,NC4TO8("GRID"),-1,-1,-1,JBUF,MAXX,OK)
      IF(.NOT.OK) CALL                             XIT('RZONAVG',-3)
      IF(NR.EQ.0) CALL PRTLAB (JBUF)
C
C     * CHECK SIZES.
C
      CALL CMPLBL (0,IBUF,0,JBUF,OK)
      IF(.NOT.OK .OR. IBUF(4).NE.JBUF(4)) THEN                                                                                      
        CALL PRTLAB (IBUF)                                                                                                          
        CALL PRTLAB (JBUF)                                                                                                          
        CALL                                       XIT('RZONAVG',-4)                                                                
      ENDIF                                                                                                                         
C
C     * COMPUTE ZONAL AVERAGES.
C     * REMEMBER THAT THE LAST VALUE IN EACH ROW IS A COPY OF THE FIRST.                                                            
C
      NLG=IBUF(5)                                                                                                                   
      NLAT=IBUF(6)                                                                                                                  
      NLGM=NLG-1                                                                                                                    
C
      DO 300 J=1,NLAT
        ZDJ  =0.0
        ZDXJ =0.0
        ZNJ  =0.0
        ZNXJ =0.0
        ZXJ  =0.0
        N=(J-1)*NLG

        DO 200 I=1,NLGM
          IF(D(N+I) .GT. 0) THEN
C          DMSK=1.0
            ZDJ = ZDJ + 1.0
            ZDXJ = ZDXJ + X(N+I)
          ELSE
C          DMSK=-1.0
            ZNJ = ZNJ + 1.0
            ZNXJ = ZNXJ + X(N+I)
          ENDIF
          ZXJ = ZXJ + X(N+I)
  200   CONTINUE
C
        ZX(J) = ZXJ / FLOAT(NLGM)
C
C     * COMPUTE THE REPRESENTATIVE ZONAL AVERAGE OF X.
C
        IF(ZDJ.NE.0.)THEN
          RZDX(J)=ZDXJ/ZDJ
        ELSE
          RZDX(J)=SPVAL
        ENDIF
C
C     * COMPUTE THE COMPLMENTARY REPRESENTATIVE ZONAL AVERAGE OF X.
C
        IF(ZNJ.NE.0.)THEN
          RZNX(J)=ZNXJ/ZNJ
        ELSE
          RZNX(J)=SPVAL
        ENDIF
C
C     * STORE ORIGINAL FIELD FOR LATER COMPARISON WITH PACKED FIELD.
C
        XNOPAK(J)=ZX(J)
C
  300 CONTINUE
C
C     * WRITE OUT THE ZONAL AVERAGE VECTOR.
C
c      NPACK=MIN(2,IBUF(8))
      CALL SETLAB(IBUF,NC4TO8("ZONL"),-1,-1,-1,NLAT,1,-1,NPACK)
      CALL PUTFLD2(13,ZX,IBUF,MAXX)
      IF(NR.EQ.0) CALL PRTLAB (IBUF)
C
      IF (LCKPK) THEN     
        CALL CHKPAK(XNOPAK,ZX,NLAT,NR,FRC,OK)
      ENDIF 
      IF(.NOT.OK)THEN
        CALL PRTLAB (IBUF)
        CALL                                       XIT('RZONAVG',-9)
      ENDIF
C
C     * WRITE OUT THE REPRESENTATIVE ZONAL AVERAGE VECTOR.
C
      NPACK=MIN(1,IBUF(8)) ! NO PACKING ALLOWED FOR FIELDS 
                           ! CONTAINING "NO DATA" VALUES.
      CALL SETLAB(IBUF,NC4TO8("ZONL"),-1,-1,-1,NLAT,1,-1,NPACK)
      CALL PUTFLD2(14,RZDX,IBUF,MAXX)
      IF(NR.EQ.0) CALL PRTLAB (IBUF)
C
C     * WRITE OUT THE COMPLEMENTARY REPRESENTATIVE ZONAL AVERAGE VECTOR.
C
      NPACK=MIN(1,IBUF(8)) ! NO PACKING ALLOWED FOR FIELDS 
                           ! CONTAINING "NO DATA" VALUES.
      CALL SETLAB(IBUF,NC4TO8("ZONL"),-1,-1,-1,NLAT,1,-1,NPACK)
      CALL PUTFLD2(15,RZNX,IBUF,MAXX)
      IF(NR.EQ.0) CALL PRTLAB (IBUF)
C
      NR=NR+1
      GO TO 100
C-----------------------------------------------------------------------
 6010 FORMAT('0',I6,'  PAIRS OF RECORDS PROCESSED')
      END


      SUBROUTINE CHKPAK(XNOPAK,XPAK,NWDS,NR,FRC,OK)

C----------------------------------------------------------------------
C
      REAL*8 XPAK(65341),XNOPAK(65341)
      REAL RELDIF
      LOGICAL OK

C---------------------------------------------------------------------
      OK = .TRUE.
      DO 100 I=1,NWDS
      RELDIF=(XPAK(I)-XNOPAK(I))/XNOPAK(I)
      IF( ABS(RELDIF) .GE. FRC) THEN
C      IF(XPAK(I)-XNOPAK(I) .NE. 0) THEN
        OK = .FALSE.
C        OK = .TRUE.
        WRITE(6,*) 'Packing error at ( NR=',NR,', I=',I,')'
     1             ,'  Value',XNOPAK(I),'packed as',XPAK(I)
     2             ,'  Rel diff(%):',100*RELDIF
C        CALL                                       XIT(' CHKPAK',-9)
      ENDIF
  100 CONTINUE
      RETURN
C---------------------------------------------------------------------
 6010 FORMAT('0',I6,'  PAIRS OF RECORDS PROCESSED')
 6025 FORMAT(' ',A4,I10,2X,A4,I10,4I6)
      END
