      PROGRAM LLAFRB
C     PROGRAM LLAFRB (LL,       FC,       INPUT,       OUTPUT,           )
C    1         TAPE1=LL, TAPE2=FC, TAPE5=INPUT, TAPE6=OUTPUT)
C     -------------------------------------------------------------------
C
C     OCT 23/06 - C.MCLANDRESS
C
CLLAFRB   - FOURIER ANALYSIS OF GRID FILE; ZEROING OUT FOURIER COEFFICIENTS
C           OUTSIDE OF A SPECIFIED WAVENUMBER BAND. BASED ON LLAFR
C
CAUTHOR  - C.MCLANDRESS 
C
CPURPOSE - CONVERTS A FILE OF GAUSSIAN OR LAT-LONG GRIDS TO FOURIER
C          COEFFICIENTS AT EACH LATITUDE AND ZEROES OUT FOURIER
C          COEFFICIENTS OUTSIDE OF A SPECIFIED WAVENUMBER BAND.
C          NOTE - GRID ROW LENGTH SHOULD BE (2**N) OR 3*(2**N).
C                 ANALYSIS IS LATITUDE-BY-LATITUDE TO A RESOLUTION
C                 SPECIFIED ON A CARD.
C                 MAXIMUM GRID OR FOURIER ARRAY SIZE IS "IJ" WORDS.
C
CINPUT FILE...
C
C      LL = GAUSSIAN OR LAT-LONG GRIDS.
C
COUTPUT FILE...
C
C      FC = LATITUDINAL FOURIER ANALYSIS OF THE GRIDS IN LL.
C
CINPUT PARAMETERS...
C
C      MAXF = MAXIMUM WAVE NUMBER USED IN THE FOURIER ANALYSIS.
C             EACH LATITUDE IN THE OUTPUT FILE WILL HAVE MAXF+1
C             COMPLEX NUMBERS.
C      MFIRST = FIRST ZONAL WAVENUMBER TO RETAIN
C      MLAST  = LAST ZONAL WAVENUMBER TO RETAIN
C      (E.G. MFIRST=10 AND MLAST=20 MEANS THAT FOURIER COEFFS
C      FOR M=0-9 AND 21-MAXF ARE SET TO ZERO.)
C
CEXAMPLE OF INPUT CARD...
C
C*   LLAFRB   20   10   20
C--------------------------------------------------------------------------
C
      use diag_sizes, only : SIZES_BLONP1,
     &                       SIZES_BLONP1xBLAT,
     &                       SIZES_BLONP1xBLATxNWORDIO,
     &                       SIZES_LAT,
     &                       SIZES_LMTP1

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      COMMON /BLANCK/ GG(SIZES_BLONP1xBLAT),
     &                FC(2*(SIZES_LMTP1+1)*SIZES_LAT)
C
      LOGICAL OK
      REAL WRKS(3*(SIZES_BLONP1+3))
      COMMON/ICOM/IBUF(8),IDAT(SIZES_BLONP1xBLATxNWORDIO)
C
      DATA MAXX,MAXG/SIZES_BLONP1xBLATxNWORDIO,SIZES_BLONP1xBLAT/
C--------------------------------------------------------------------------
      NFF=4
      CALL JCLPNT(NFF,1,2,5,6)
      REWIND 1
      REWIND 2
C
C     * READ THE WAVENUMBERS
C
      READ(5,5010,END=905) MAXF, MFIRST, MLAST
      WRITE(6,6005) MAXF
      WRITE(6,6015) MFIRST,MLAST
C
C     * GET THE NEXT GRID FROM FILE 1.
C
      NR=0
  140 CALL GETFLD2(1,GG,NC4TO8("GRID"),-1,-1,-1,IBUF,MAXX,OK)
      IF(.NOT.OK) THEN
        IF(NR.EQ.0)THEN
          CALL                                     XIT('LLAFRB',-1)
        ELSE
          WRITE(6,6010) NR
          CALL                                     XIT('LLAFRB',0)
        ENDIF
      ENDIF
      IF(NR.EQ.0) WRITE(6,6025) IBUF
C
      NLG1=IBUF(5)
      NLAT=IBUF(6)
      NLG=NLG1-1
C
      IF(MAXF.GT.NLG/2) CALL                       XIT('LLAFRB',-2)
      IF(NLG1*NLAT.GT.MAXG) CALL                   XIT('LLAFRB',-3)
      IF(2*MAXF*NLAT.GT.MAXG) CALL                 XIT('LLAFRB',-4)
C
C     * CALCULATE FOURIER COEFF FOR EACH LATITUDE IN THE GRID.
C
      CALL FFWFG2(FC,MAXF+1,GG,NLG1,MAXF,NLG,WRKS,NLAT)
      LFR=(MAXF+1)

CCC      IF (NR.EQ.0) THEN 
CCC        WRITE(6,*) 'LFR=',LFR
CCC        DO J=1,NLAT
CCC          WRITE(6,*) ' J=',J, ' before'
CCC          DO I=1,LFR*2,2
CCC            M=(I-1)/2    ! zonal wavenumber ( 0 is zonal mean)
CCC            N=(J-1)*LFR*2+I
CCC            WRITE(6,6120) M,FC(N),FC(N+1)
CCC          ENDDO
CCC        ENDDO
CCC 6120   FORMAT('     M,FC=',I4,2(1X,F10.4))
CCC      ENDIF

C
C     * ZERO OUT COEFFICIENTS OUTSIDE OF SPECIFIED WAVENUMBER RANGE
C
      DO J=1,NLAT
        DO I=1,LFR*2,2
          MS=(I-1)/2     ! zonal wavenumber (0 is zonal mean)
          IF (MS.LT.MFIRST.OR.MS.GT.MLAST) THEN 
            N=(J-1)*LFR*2+I
            FC(N)=0.    ! real part
            FC(N+1)=0.  ! imag part
          ENDIF
        ENDDO
      ENDDO

CCC      IF (NR.EQ.0) THEN 
CCC        WRITE(6,*)
CCC        DO J=1,NLAT
CCC          WRITE(6,*) ' J=',J,' after'
CCC          DO I=1,LFR*2,2
CCC            M=(I-1)/2    ! zonal wavenumber ( 0 is zonal mean)
CCC            N=(J-1)*LFR*2+I
CCC            WRITE(6,6120) M,FC(N),FC(N+1)
CCC          ENDDO
CCC        ENDDO
CCC      ENDIF

C
C     * SAVE ARRAY OF FOURIER COEFF ON FILE 2.
C
      CALL SETLAB(IBUF,NC4TO8("FOUR"),-1,-1,-1,LFR,NLAT,-1,1)
      CALL PUTFLD2(2,FC,IBUF,MAXX)
      IF(NR.EQ.0) WRITE(6,6025) IBUF
      NR=NR+1
      GO TO 140
C
C     * E.O.F. ON INPUT.
C
  905 CALL                                         XIT('LLAFRB',-5)
C--------------------------------------------------------------------
 5010 FORMAT(10X,3I5)
 6005 FORMAT('0FOURIER ANALYSIS TO WAVENUMBER',I5)
 6015 FORMAT(' ZEROING OUT WAVENUMBERS OUTSIDE OF BAND',I5,' TO',I5)
 6007 FORMAT('0NLG1,NLAT,NWDS=',3I6)
 6010 FORMAT('0',I6,' GRIDS CONVERTED TO FOURIER COEFF')
 6025 FORMAT(' ',A4,I10,2X,A4,I10,4I6)
      END
