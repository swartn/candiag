      PROGRAM SQUARE
C     PROGRAM SQUARE (GGIN,       GGOUT,       INPUT,       OUTPUT,     )       C2
C    1          TAPE1=GGIN, TAPE2=GGOUT, TAPE5=INPUT, TAPE6=OUTPUT) 
C     -------------------------------------------------------------             C2
C                                                                               C2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       C2
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                
C     MAY 14/83 - R.LAPRISE.                                                    
C     JUL 26/82 - S.L.CROZIER 
C                                                                               C2
CSQUARE  - SQUARES GRID FIELDS                                          1  1    C1
C                                                                               C3
CAUTHOR  - S.CROZIER                                                            C3
C                                                                               C3
CPURPOSE - FILE COMPUTATION  GGOUT = GGIN**2                                    C3
C          NOTE - GGIN MUST NOT BE COMPLEX                                      C3
C                 ZERO SQUARED IS LEFT AS ZERO                                  C3
C                                                                               C3
CINPUT FILE...                                                                  C3
C                                                                               C3
C      GGIN = FILE OF FIELDS TO BE RAISED TO THE POWER 2                        C3
C                                                                               C3
COUTPUT FILE...                                                                 C3
C                                                                               C3
C      GGOUT = FILE CONTAINING THE SQUARE OF GGIN                               C3
C---------------------------------------------------------------------------
C 
      use diag_sizes, only : SIZES_BLONP1xBLAT,
     &                       SIZES_BLONP1xBLATxNWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      COMMON/BLANCK/F(SIZES_BLONP1xBLAT)
C 
      LOGICAL OK,SPEC 
      COMMON/ICOM/IBUF(8),IDAT(SIZES_BLONP1xBLATxNWORDIO) 
      DATA MAXX/SIZES_BLONP1xBLATxNWORDIO/ 
C-----------------------------------------------------------------------
      NFF=3 
      CALL JCLPNT(NFF,1,2,6)
      REWIND 1
      REWIND 2
C 
C     * READ THE NEXT GRID FROM THE FILE GGIN.
C 
      NR=0
  150 CALL GETFLD2(1,F,-1,0,0,0,IBUF,MAXX,OK)
      IF(.NOT.OK)THEN 
        IF(NR.EQ.0) CALL                           XIT('SQUARE',-1) 
        WRITE(6,6025) IBUF
        WRITE(6,6010) NR
        CALL                                       XIT('SQUARE',0)
      ENDIF 
      IF(NR.EQ.0) WRITE(6,6025) IBUF
C 
C     * DETERMINE SIZE OF THE FIELD.
C 
      KIND=IBUF(1)
      SPEC=(KIND.EQ.NC4TO8("SPEC") .OR. KIND.EQ.NC4TO8("FOUR"))
      IF(SPEC) CALL                                XIT('SQUARE',-2) 
C 
C     * PERFORM THE SQUARING. 
C 
      NWDS=IBUF(5)*IBUF(6)
      DO 210 I=1,NWDS 
        F(I)=F(I)**2
  210 CONTINUE
C 
C     * SAVE ON FILE GGOUT. 
C 
      CALL PUTFLD2(2,F,IBUF,MAXX)
      NR=NR+1 
      GO TO 150 
C-----------------------------------------------------------------------
 6010 FORMAT('0 SQUARE READ',I6,' RECORDS')
 6025 FORMAT(' ',A4,I10,2X,A4,I10,4I6)
      END
