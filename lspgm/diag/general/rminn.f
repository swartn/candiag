      PROGRAM RMINN
C     PROGRAM RMINN (XIN,       MIN,       INPUT,       OUTPUT,         )       C2
C    1         TAPE1=XIN, TAPE2=MIN, TAPE5=INPUT, TAPE6=OUTPUT)                 C2
C     ---------------------------------------------------------                 C2
C                                                                               C2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       C2
C     JUL 05/99 - GJB                                                           C2
C                                                                               C2
CRMINN   - EXTRACTS MINIMUM VALUE IN SETS OF N DATA RECORDS IN A FILE   1  1 C  C1
C                                                                               C3
CAUTHOR  - GJB                                                                  C3
C                                                                               C3
CPURPOSE - EXTRACTS THE MINIMUM VALUE FROM A SET OF N RECORDS IN A FILE.        C3
C          * THERE IS ONE OUTPUT RECORD FOR EACH SET OF N INPUT RECORDS.        C3
C          * THE INPUT RECORDS ARE ASSUMED TO HAVE THE SAME FORM                C3
C          * THE OUTPUT RECORD HAS THIS FORM WITH ALL VALUES EQUAL TO           C3
C            THE MINIMUM VALUE                                                  C3
C          * ITS LABEL IS THAT OF THE FIRST INPUT RECORD IN THE SET             C3
C                                                                               C3
C          FOR COMPLEX FIELDS, THE REAL AND IMAGINARY PARTS OF THE OUTPUT       C3
C          RECORD ARE SET TO THE CORRESPONDING PARTS OF THE COMPLEX NUMBER      C3
C          IN THE N INPUT RECORDS THAT HAS THE SMALLEST AMPLITUDE.              C3
C                                                                               C3
CINPUT FILE...                                                                  C3
C                                                                               C3
C      XIN = INPUT FILE.                                                        C3
C                                                                               C3
COUTPUT FILE...                                                                 C3
C                                                                               C3
C      MIN = OUTPUT FILE OF MINIMUM VALUES.                                     C3
C
CINPUT PARAMETERS...
C                                                                               C5
C      N   = NUMBER OF RECORDS IN A "SET"                                       C5
C                                                                               C5
C                                                                               C5
CEXAMPLE OF INPUT CARD...                                                       C5
C                                                                               C5
C*  RMINN         31                                                            C5
C   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
C-------------------------------------------------------------------------------
C
      use diag_sizes, only : SIZES_BLONP1xBLAT,
     &                       SIZES_BLONP1xBLATxNWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      LOGICAL OK,CPLXFLD
      REAL AMINVAL,CMINVAL(2)
      INTEGER JBUF(8)
C
      COMMON/BLANCK/A(SIZES_BLONP1xBLAT)
      COMMON/ICOM/IBUF(8),IDAT(SIZES_BLONP1xBLATxNWORDIO)
C
      DATA MAXX/SIZES_BLONP1xBLATxNWORDIO/
C---------------------------------------------------------------------
      NFF=4
      CALL JCLPNT(NFF,1,2,5,6)
      REWIND 1
      REWIND 2
C
C     * READ NUMBER OF RECORDS IN A SET FROM CARD.
C
      READ(5,5000,END=900) N                                                    C4
      WRITE(6,6000)        N
      IF(N.LE.0) CALL                              XIT('RMINN',-1)
C
      CPLXFLD=.FALSE.

      NR=0
      NB=0

C     * GET THE FIRST RECORD IN THE SET
C
  140 CALL GETFLD2(1,A, -1 ,0,0,0,IBUF,MAXX,OK)
      IF(.NOT.OK) THEN
         IF(NR.EQ.0) CALL                          XIT('RMINN',-2)
         WRITE(6,6010) NR,NB,NS
         IF (CPLXFLD) WRITE(6,6020)
         CALL                                      XIT('RMINN',0)
      ENDIF
      IF(NR.EQ.0) CALL PRTLAB(IBUF)

C
C     * DETERMINE THE "KIND" OF FIELD AND SETUP ACCORDINGLY.
C
      KIND=IBUF(1)
      LA=IBUF(5)*IBUF(6)

      IF(KIND.EQ.NC4TO8("GRID").OR.KIND.EQ.NC4TO8("ZONL").OR.
     1     KIND.EQ.NC4TO8("SUBA").OR.KIND.EQ.NC4TO8("TIME")) THEN
         INC=1
      ELSE IF (KIND.EQ.NC4TO8("FOUR").OR.KIND.EQ.NC4TO8("SPEC")) THEN
         LA=2*LA
         INC=2
         CPLXFLD=.TRUE.
      ELSE
         CALL PRTLAB(IBUF)
         CALL                                      XIT('RMINN',-3)
      ENDIF

C     * PROCESS THE FIELD VALUES.

      IF ( INC.EQ.2) THEN

C       * COMPLEX FIELDS.

        CMINVAL(1)=A(1)
        CMINVAL(2)=A(2)
        AMINVAL=ABS(CMPLX(A(1),A(2)))
        DO I=3,LA,2
           IF(ABS(CMPLX(A(I),A(I+1))).LT.AMINVAL) THEN
             AMINVAL=ABS(CMPLX(A(I),A(I+1)))
             CMINVAL(1)=A(I)
             CMINVAL(2)=A(I+1)
           ENDIF
        ENDDO

      ELSE

C       * REAL FIELDS.

        AMINVAL=A(1)
        DO I=2,LA
           IF(A(I).LT.AMINVAL) AMINVAL=A(I)
        ENDDO

      ENDIF

C
C     * ADJUST COUNTERS...
C
      NR=NR+1
      NB=NB+1
      NS=1

C     * SKIP TO OUTPUT THE RESULT IF EACH SET CONSISTS 
C     * OF JUST ONE RECORD.

      IF (N.EQ.1) GO TO 215
C
C     * PRESERVE IN "JBUF" THE LABEL SETTING OF THE FIRST 
C     * RECORD IN THE SET.

      DO K=1,8
        JBUF(K)=IBUF(K)
      ENDDO

C     * READ SUBSEQUENT RECORDS IN SET AND FIND MIN

  150 CALL GETFLD2(1,A, -1 ,0,0,0,IBUF,MAXX,OK)
      IF(.NOT.OK) GO TO 215
      NR=NR+1
C
C     * MAKE SURE THAT THE FIELDS ARE THE SAME KIND AND SIZE AND FIND MIN
C
      CALL CMPLBL(0,JBUF,0,IBUF,OK)
      IF(.NOT.OK)THEN
        CALL PRTLAB(JBUF)
        CALL PRTLAB(IBUF)
        CALL                                       XIT('RMINN',-4)
      ENDIF
     
C     * PROCESS THE FIELD VALUES.

      IF ( INC.EQ.2) THEN

C       * COMPLEX FIELDS.

        DO I=1,LA,2
           IF(ABS(CMPLX(A(I),A(I+1))).LT.AMINVAL) THEN
             AMINVAL=ABS(CMPLX(A(I),A(I+1)))
             CMINVAL(1)=A(I)
             CMINVAL(2)=A(I+1)
           ENDIF
        ENDDO

      ELSE

C       * REAL FIELDS.

        DO I=1,LA
           IF(A(I).LT.AMINVAL) AMINVAL=A(I)
        ENDDO

      ENDIF

      NS=NS+1
      IF(NS.LT.N) GO TO 150

C     * ABORT IF THE SET PROCESSED IS SHORT.

  215 IF (NS.NE.N) THEN
        CALL PRTLAB(IBUF)
        WRITE(6,6030) N,NS
        CALL                                       XIT('RMINN',-5)
      ENDIF

C     * SAVE THE RESULT IN ARRAY "A". 

      IF ( INC.EQ.2) THEN

C       * COMPLEX FIELDS.

        DO I=1,LA,2
           A(I)  =CMINVAL(1)
           A(I+1)=CMINVAL(2)
        ENDDO

      ELSE

C       * REAL FIELDS.

        DO I=1,LA
           A(I)=AMINVAL
        ENDDO

      ENDIF

C     * WRITE THE RESULTING OUTPUT RECORD WITH ITS LABEL
C     * SET TO THAT OF THE FIRST RECORD IN THE SET.

      IF (N.GT.1) THEN
        DO K=1,8
          IBUF(K)=JBUF(K)
        ENDDO
      ENDIF

      CALL PUTFLD2(2,A,IBUF,MAXX)

C     * BRANCH TO PROCESS THE NEXT SET.

      GO TO 140

C     * FAILED TO READ INPUT PARAMETER.

  900 CALL                                         XIT('RMINN',-6)

C---------------------------------------------------------------------
 5000 FORMAT(10X,I5)                                                            C4
 6000 FORMAT(' NUMBER OF RECORDS IN SET =', I5)
 6010 FORMAT(' RECORDS=',I5,', SETS=',I5,', NO. IN LAST SET=',I5)
 6020 FORMAT(/,' FOR COMPLEX FIELDS, THE REAL AND IMAGINARY ',
     1     'PARTS OF THE OUTPUT RECORD',/,' ARE SET TO THE ',
     2     'CORRESPONDING PARTS OF THE COMPLEX NUMBER IN THE ',/,
     3     ' N INPUT RECORDS THAT HAS THE SMALLEST AMPLITUDE.')
 6030 FORMAT('0 RMINN: INCOMPLETE SET, EXPECTED=',I5,', ACTUAL=',
     1       I5,' RECORDS.')
      END
