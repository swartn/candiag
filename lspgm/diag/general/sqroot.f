      PROGRAM SQROOT
C     PROGRAM SQROOT (XIN,       XOUT,       INPUT,       OUTPUT,       )       C2
C    1          TAPE1=XIN, TAPE2=XOUT, TAPE5=INPUT, TAPE6=OUTPUT) 
C     -----------------------------------------------------------               C2
C                                                                               C2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       C2
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                
C     APR 02/85 - B.DUGAS. (VECTORIZE LOOP 210...)                              
C     JUL 26/82 - S.L.CROZIER 
C                                                                               C2
CSQROOT  - TAKES THE SQUARE ROOT OF AN ARRAY (NO TYPE CHECK)            1  1    C1
C                                                                               C3
CAUTHOR  - S.CROZIER                                                            C3
C                                                                               C3
CPURPOSE - CALCULATE THE SQUARE ROOT OF ANY KIND OF NUMERICAL                   C3
C          INPUT FIELD, COMPLEX OR REAL. IF KIND IS SPECTRAL                    C3
C          THE OUTPUT IS (SQRT(REAL),SQRT(IMAG)) AND A WARNING                  C3
C          MESSAGE IS PRINTED.                                                  C3
C          NOTE - THE SQUARE ROOT OF ZERO IS LEFT AS ZERO                       C3
C                                                                               C3
CINPUT FILE...                                                                  C3
C                                                                               C3
C      XIN  = FILE OF FIELDS TO BE RAISED TO THE POWER 1/2                      C3
C                                                                               C3
COUTPUT FILE...                                                                 C3
C                                                                               C3
C      XOUT = FILE CONTAINING THE SQUARE ROOT OF XIN                            C3
C---------------------------------------------------------------------------
C 
      use diag_sizes, only : SIZES_BLONP1xBLAT,
     &                       SIZES_BLONP1xBLATxNWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      COMMON/BLANCK/F(SIZES_BLONP1xBLAT)
  
      LOGICAL OK,SPEC 
      COMMON/ICOM/IBUF(8),IDAT(SIZES_BLONP1xBLATxNWORDIO) 
      DATA MAXX/SIZES_BLONP1xBLATxNWORDIO/ 
C-----------------------------------------------------------------------
      NFF=3 
      CALL JCLPNT(NFF,1,2,6)
      REWIND 1
      REWIND 2
  
C     * READ THE NEXT GRID FROM THE FILE XIN. 
  
      NR=0
  150 CALL GETFLD2(1,F,-1,0,0,0,IBUF,MAXX,OK)
      IF(.NOT.OK)THEN 
        IF(NR.EQ.0) CALL                           XIT('SQROOT',-1) 
        WRITE(6,6025) IBUF
        WRITE(6,6010) NR
        CALL                                       XIT('SQROOT',0)
      ENDIF 
      IF(NR.EQ.0) WRITE(6,6025) IBUF
  
C     * DETERMINE SIZE OF THE FIELD.
  
      KIND=IBUF(1)
      SPEC=(KIND.EQ.NC4TO8("SPEC") .OR. KIND.EQ.NC4TO8("FOUR"))
      NWDS=IBUF(5)*IBUF(6)
      IF (SPEC)    THEN 
          NWDS=NWDS*2 
          IF (NR.EQ.0) WRITE(6,6100)
      ENDIF 
  
C     * PERFORM THE SQUARE ROOT 
  
      IF (F(ISMIN(NWDS,F,1)).LT.0.E0)   THEN
          CALL                                     XIT('SQROOT',-2) 
      ELSE
          DO 210 I=1,NWDS 
             F(I)=SQRT(F(I))
  210     CONTINUE
      ENDIF 
  
C     * SAVE ON FILE XOUT.
  
      CALL PUTFLD2(2,F,IBUF,MAXX)
      NR=NR+1 
      GO TO 150 
C-----------------------------------------------------------------------
 6010 FORMAT('0  SQROOT  READ',I6,' RECORDS')
 6025 FORMAT(' ',A4,I10,2X,A4,I10,4I6)
 6100 FORMAT(' ***** INPUT DATA IS SPECTRAL, RESULT CORRECT ONLY *****'/
     1       ' *****     IF IMAG(DATA) IS IDENTICALLY ZERO       *****')
      END
