      PROGRAM MAG2D
C     PROGRAM MAG2D (U,       V,       S,       A,       OUTPUT,        )       C2
C    1         TAPE1=U, TAPE2=V, TAPE3=S, TAPE4=A, TAPE6=OUTPUT)
C     ----------------------------------------------------------                C2
C                                                                               C2
C     NOV 02/04 - S.KHARIN (OPTIONALLY SAVE THE ANGLE OF THE (U,V) VECTOR FROM  C2
C                           THE X-AXIS)                                         C2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)
C     OCT 23/00 - S.KHARIN.
C                                                                               C2
CMAG2D   - COMPUTE SQRT(U**2+V**2) FOR TWO FIELDS U AND V.              2  1    C1
C                                                                               C3
CAUTHOR  - S. KHARIN                                                            C3
C                                                                               C3
CPURPOSE - COMPUTE SQRT(U**2+V**2) FOR TWO FIELDS U AND V, AND OPTIONALLY,      C3
C          THE ANGLE ATAN2(V,U).                                                C3
C                                                                               C3
CINPUT FILES...                                                                 C3
C                                                                               C3
C      U = CCRN DATA FILE.                                                      C3
C      V = CCRN DATA FILE.                                                      C3
C                                                                               C3
COUTPUT FILE...                                                                 C3
C                                                                               C3
C      S = OUTPUT FILE WHOSE CONTENT IS: SQRT(U**2+V**2).                       C3
C      A = (OPTIONAL) OUTPUT FILE WHOSE CONTENT IS: ATAN2(V,U).                 C3
C----------------------------------------------------------------------------
C
      use diag_sizes, only : SIZES_BLONP1xBLAT,
     &                       SIZES_BLONP1xBLATxNWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      LOGICAL OK
      COMMON/BLANCK/ U(SIZES_BLONP1xBLAT),V(SIZES_BLONP1xBLAT)
C
      COMMON /ICOM/ IBUF(8),IDAT(SIZES_BLONP1xBLATxNWORDIO)
      COMMON /JCOM/ JBUF(8),JDAT(SIZES_BLONP1xBLATxNWORDIO)
      DATA MAXX/SIZES_BLONP1xBLATxNWORDIO/

C---------------------------------------------------------------------
      NFF=5
      CALL JCLPNT(NFF,1,2,3,4,6)
      NFF=NFF-1
      REWIND 1
      REWIND 2
      REWIND 3
      IF (NFF.EQ.4) REWIND 4
C
C     * READ THE NEXT PAIR OF FIELDS.
C
      NR=0
  150 CALL GETFLD2(1,U,-1,-1,-1,-1,IBUF,MAXX,OK)

      IF (.NOT.OK) THEN
         IF(NR.EQ.0) CALL                          XIT('MAG2D',-1)
         CALL PRTLAB(IBUF)
         WRITE(6,6010) NR
         CALL                                      XIT('MAG2D',0)
      ENDIF
C
      CALL GETFLD2(2,V,-1,-1,-1,-1,JBUF,MAXX,OK)
      IF(.NOT.OK) CALL                             XIT('MAG2D',-2)
C
C     * MAKE SURE THAT THE FIELDS ARE THE SAME KIND AND SIZE.
C
      CALL CMPLBL(0,IBUF,0,JBUF,OK)
      IF(.NOT.OK)THEN
        CALL PRTLAB(IBUF)
        CALL PRTLAB(JBUF)
        CALL                                       XIT('MAG2D',-3)
      ENDIF
C
C     * DETERMINE SIZE OF THE FIELD.
C
      NWDS=IBUF(5)*IBUF(6)
      IF (IBUF(1).EQ.NC4TO8("SPEC").OR.
     +    IBUF(1).EQ.NC4TO8("FOUR")    ) NWDS=NWDS*2
C
C     * COMPUTE THE MAGNITUDE OF THE VECTOR (U,V).
C
      DO I=1,NWDS
         IF(NFF.EQ.4) A=ATAN2(V(I),U(I))
         U(I)=SQRT(U(I)*U(I)+V(I)*V(I))
         V(I)=A
      ENDDO
C
C     * SAVE THE RESULT.
C
      IBUF(3)=NC4TO8("MAGN")
      CALL PUTFLD2(3,U,IBUF,MAXX)
      IF (NFF.EQ.4) THEN
         IBUF(3)=NC4TO8("ANGL")
         CALL PUTFLD2(4,V,IBUF,MAXX)
      ENDIF
      NR = NR+1
      GOTO 150
C---------------------------------------------------------------------
 6010 FORMAT('0',I6,' PAIRS OF RECORDS PROCESSED.')
      END
