      PROGRAM MLT 
C     PROGRAM MLT (X,       Y,       Z,       OUTPUT,                   )       C2
C    1       TAPE1=X, TAPE2=Y, TAPE3=Z, TAPE6=OUTPUT) 
C     -----------------------------------------------                           C2
C                                                                               C2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       C2
C     JAN 29/92 - E. CHAN    (CONVERT HOLLERITH LITERALS TO ASCII)              
C     DEC 30/87 - F. MAJAESS (DON'T CHANGE NAME TO ' MLT' IN FILE Z IF          
C                             THE NAMES IN FILES X AND Y ARE IDENTICAL).        
C     MAY 12/83 - R.LAPRISE.
C     JAN 01/81 - J.D.HENDERSON 
C                                                                               C2
CMLT     - MULTIPLIES TWO REAL FILES                                    2  1    C1
C                                                                               C3
CAUTHOR  - J.D.HENDERSON                                                        C3
C                                                                               C3
CPURPOSE - FILE ARITHMETIC PROGRAM  Z = X * Y                                   C3
C          NOTE - X AND Y MUST NOT BE COMPLEX.                                  C3
C                                                                               C3
CINPUT FILES...                                                                 C3
C                                                                               C3
C      X = FIRST  INPUT FILE                                                    C3
C      Y = SECOND INPUT FILE                                                    C3
C                                                                               C3
COUTPUT FILE...                                                                 C3
C                                                                               C3
C      Z = PRODUCT X*Y                                                          C3
C-----------------------------------------------------------------------------
C 
      use diag_sizes, only : SIZES_BLONP1xBLAT,
     &                       SIZES_BLONP1xBLATxNWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      COMMON/BLANCK/A(SIZES_BLONP1xBLAT),B(SIZES_BLONP1xBLAT) 
C 
      LOGICAL OK,SPEC 
      COMMON/ICOM/IBUF(8),IDAT(SIZES_BLONP1xBLATxNWORDIO) 
      COMMON/JCOM/JBUF(8),JDAT(SIZES_BLONP1xBLATxNWORDIO) 
      DATA MAXX/SIZES_BLONP1xBLATxNWORDIO/ 
C---------------------------------------------------------------------
      NFF=4 
      CALL JCLPNT(NFF,1,2,3,6)
      REWIND 1
      REWIND 2
      REWIND 3
C 
C     * READ THE NEXT PAIR OF FIELDS. 
C 
      NR=0
  140 CALL GETFLD2(1,A, -1 ,0,0,0,IBUF,MAXX,OK)
      IF(.NOT.OK)THEN 
        IF(NR.EQ.0)THEN 
          CALL                                     XIT(' MLT',-1) 
        ELSE
          WRITE(6,6010) NR
          CALL                                     XIT(' MLT',0)
        ENDIF 
      ENDIF 
      IF(NR.EQ.0) WRITE(6,6025) IBUF
C 
      CALL GETFLD2(2,B, -1 ,0,0,0,JBUF,MAXX,OK)
      IF(.NOT.OK) CALL                             XIT(' MLT',-2) 
      IF(NR.EQ.0) WRITE(6,6025) JBUF
C 
C     * MAKE SURE THAT THE FIELDS ARE REAL AND THE SAME SIZE. 
C 
      CALL CMPLBL(0,IBUF,0,JBUF,OK) 
      IF(.NOT.OK)THEN 
        WRITE(6,6025) IBUF,JBUF 
        CALL                                       XIT(' MLT',-3) 
      ENDIF 
      KIND=IBUF(1)
      SPEC=(KIND.EQ.NC4TO8("SPEC") .OR. KIND.EQ.NC4TO8("FOUR"))
      IF(SPEC) CALL                                XIT(' MLT',-4) 
C 
C     * MULTIPLY THE FIELDS.
C 
      NWDS=IBUF(5)*IBUF(6)
      DO 210 I=1,NWDS 
  210 A(I)=A(I)*B(I)
C 
C     * SAVE THE RESULT ON FILE C.
C 
      IF (IBUF(3).NE.JBUF(3)) IBUF(3)=NC4TO8(" MLT")
      CALL PUTFLD2(3,A,IBUF,MAXX)
      IF(NR.EQ.0) WRITE(6,6025) IBUF
      NR=NR+1 
      GO TO 140 
C---------------------------------------------------------------------
 6010 FORMAT('0',I6,'  PAIRS OF RECORDS PROCESSED')
 6025 FORMAT(' ',A4,I10,2X,A4,I10,4I6)
      END
