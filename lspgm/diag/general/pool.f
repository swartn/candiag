      PROGRAM POOL
C     PROGRAM POOL(TAVG,       STDV,       COVD,       COVM,                    C2
C    1             INP1,       INP2,        ...,       INP50,                   C2
C    2                                    INPUT,       OUTPUT,         )        C2
C    3      TAPE11=TAVG,TAPE12=STDV,TAPE13=COVD,TAPE14=COVM,                    C2
C    4      TAPE31=INP1,TAPE52=INP2,        ...,TAPE80=INP50,                   C2
C    5                              TAPE5=INPUT,TAPE6 =OUTPUT)                  C2
C     --------------------------------------------------------                  C2
C                                                                               C2
C     DEC 23/2010 - S.KHARIN (FORGO LEVEL CHECK IN READING DATA RECORDS)        C2
C     MAY 20/2009 - F.MAJAESS (REWIND DATA OUTPUT FILES)
C     JAN 26/2009 - S.KHARIN (COMPUTE TIME AVERAGE OF LPA, LPAC, AND SALB
C                             FOR POSTIVE VALUES ONLY.)
C     MAR 15/2005 - S.KHARIN (USE MAXIMUM FOR 'TIME MAXIMUM X' AND MINIMUM FOR
C                            'TIME MINIMUM X' IN SEASONAL POOLING)
C     FEB 14/2005 - S.KHARIN (USE 'X"Y"' INSTEAD OF 'A X"Y"' FOR SEASONAL
C                             POOLING)
C     JUL 19/2004 - F.MAJAESS (ENSURE FREEING UNIT/FILE BEFORE REASSIGNMENT)
C     JUL 03/2003 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)
C     APR 03/2003 - F.MAJAESS (EMPLOY OPEN/INQUIRE STATEMENTS TO ACCOMPLISH
C                              PROPER FILE OPENING AND POINTER POSITIONING)
C     FEB 28/2003 - F.MAJAESS (REVISED TO SKIP PROCESSING "CHAR" KIND RECORDS)
C     OCT 04/2002 - S.KHARIN (CORRECT REFERENCE OF 'poolstrict' TO 'poolabort')
C     JUL 15/2002 - S.KHARIN (INCREASE "LENMAX" TO 10000)
C     JUL 03/2001 - S.KHARIN (PROCESS 'A X"Y"' IN DIAGNOSTIC FILES)
C     APR 18/2000 - S.KHARIN
C                                                                               C2
CPOOL    - POOL UP TO 50 DIAGNOSTIC AND/OR POOLED FILES.               50  4 C  C1
C                                                                               C3
CAUTHOR  - S.KHARIN                                                             C3
C                                                                               C3
CPURPOSE - POOL UP TO 50 DIAGNOSTIC AND/OR POOLED FILES.     .                  C3
C                                                                               C3
CINPUT FILES...                                                                 C3
C                                                                               C3
C      INP1,...INP50  INPUT MONTHLY DIAGNOSTIC OR POOLED FILES.                 C3
C                     ALL POSSIBLE VARIABLES ARE POOLED.                        C3
C                                                                               C3
COUTPUT FILES...                                                                C3
C                                                                               C3
C      TAVG   =  TIME AVERAGES 'X' AND 'A X"Y"'                                 C3
C      STDV   =  STANDARD DEVIATIONS 'S(X)'                                     C3
C      COVD   =  DAILY (CO)VARIANCES 'X"Y"'                                     C3
C      COVM   =  MONTHLY (CO)VARIANCES 'X-Y-'                                   C3
C
CINPUT PARAMETERS...
C                                                                               C5
C      IABORT = 0 DO NOT ABORT WHEN SOME VARIABLES IN SOME INPUT FILES ARE      C5
C                 MISSING. TRY TO POOL ALL COMMON VARIABLES.                    C5
C             = 1 ABORT WHEN SOME VARIABLES ARE MISSING ("STRICT" POOLING).     C5
C                                                                               C5
C      ITYPE    POOLING TYPE:                                                   C5
C             = 0 MULTI-YEAR POOLING OF ALL VARIABLES I.E. FULL POOLING OF      C5
C                 'X', 'S(X)', 'X"Y"', 'A X"Y"' AND 'X-Y-' FOR THE SAME MONTH,  C5
C                 OR SEASON.                                                    C5
C             = 1 MULTI-YEAR POOLING OF TIME MEANS AND STANDARD DEVIATIONS      C5
C                 ONLY ('X', 'S(X)' AND 'A X"Y"'). DAILY COVARIANCES ARE NOT    C5
C                 POOLED, 'X"Y"' AND 'X-Y-' ARE NOT CALCULATED.                 C5
C             = 2 SEASONAL POOLING OF 'X' AND 'X"Y"' FOR SEVERAL CONSECUTIVE    C5
C                 DIAGNOSTIC FILES.                                             C5
C             = 3 SEASONAL POOLING OF TIME MEANS ONLY ('X').                    C5
C                                                                               C5
C  NP1,NP2,...,NP50:                                                            C5
C          IF ITYPE=0 OR 1:                                                     C5
C             = 0 THE CORRESPONDING INPUT FILE IS A DIAGNOSTIC FILE.            C5
C             >=1 THE NUMBER OF YEARS POOLED IN THE INPUT FILES.                C5
C          IF ITYPE=2 OR 3:                                                     C5
C             >=1 THE MONTH LENGTH OF THE CORRESPONDING INPUT FILE.             C5
C                                                                               C5
C  *** NOTE1: OPTIMAL I/O PERFORMANCE IS ACHIEVED WHEN ALL INPUT FILES HAVE     C5
C             THE SAME SET OF VARIABLES IN THE SAME ORDER.                      C5
C             THE PERFORMANCE IS SUB-OPTIMAL WHEN POOLING POOLED AND            C5
C             DIAGNOSTIC FILES.                                                 C5
C                                                                               C5
C  *** NOTE2: WHEN POOLING POOLED AND DIAGNOSTIC FILES, THE FIRST INPUT FILE    C5
C             MUST BE A POOLED FILE. THE ORDER OF THE REST INPUT                C5
C             FILES MAY BE ARBITRARY.                                           C5
C                                                                               C5
CEXAMPLES OF INPUT CARDS...                                                     C5
C*POOL.       0                                                                 C5
C*POOL.      20   10    5                                                       C5
C(POOL 3 FILES CONTAINING 20-, 10- AND 5-YEAR POOLED STATISTICS, RESPECTIVELY)  C5
C                                                                               C5
C*POOL.     1 0                                                                 C5
C*POOL.       0    0    0    0    0    0    0    0    0    0                    C5
C(POOL 10 DIAGNOSTIC FILES, STRICT VARIABLE CHECKING)                           C5
C                                                                               C5
C*POOL.     1 1                                                                 C5
C*POOL.       0    0    0    0    0    0    0    0    0    0                    C5
C(POOL 10 DIAGNOSTIC FILES, STRICT VARIABLE CHECKING, 'X"Y"' AND 'X-Y-' ARE NOT C5
C CALCULATED. USEFUL FOR POOLING ZONALLY AVERAGED STATISTICS.)                  C5
C                                                                               C5
C*POOL.       0                                                                 C5
C*POOL.      10   10    0    0    0    0    0                                   C5
C(POOL TWO 10-YEAR POOLED FILES AND 5 DIAGNOSTIC FILES)                         C5
C                                                                               C5
C*POOL.     1 2                                                                 C5
C*POOL.      31   31   28                                                       C5
C(POOL DIAGNOSTIC FILES FOR DECEMBER, JANUARY AND FEBRUARY. STRICT VARIABLE     C5
C CHECKING. USEFUL FOR SEASONAL POOLING OF DIAGNOSTIC FILES)                    C5
C                                                                               C5
C-----------------------------------------------------------------------------
C
      use diag_sizes, only : SIZES_BLONP1xBLAT,
     &                       SIZES_BLONP1xBLATxNWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      LOGICAL OK,CALC,LABX,LABMAX,LABMIN,LABXY,LABXY1,LABAXY,LABAXY1,
     1     LABMXY,LABSX,LABANO,SCAN,NEWANO,POOLED,LABORT
      LOGICAL STAT
C
C     * LISTS FOR SUPERLABELS:
C
C     * LSTPOOL = LIST OF ALL SUPERLABELS IN INPUT POOLED FILES
C     * LSTDIAG = LIST OF ALL SUPERLABELS IN INPUT DIAGNOSTIC FILES
C     * LSTX    = LIST OF TIME AVERAGES 'X'
C     * LSTMAX  = LIST OF TIME MAXIMUM 'TIME MAXIMUM X'
C     * LSTMIN  = LIST OF TIME MINIMUM 'TIME MINIMUM X'
C     * LSTXY   = LIST OF DAILY (CO)VARIANCES 'X"Y"' THAT CAN BE FULLY POOLED
C     * LSTXY1  = LIST OF (CO)VARIANCES 'X"Y"' THAT CANNOT BE FULLY POOLED
C     * LSTAXY  = LIST OF MEAN INTRA-MONTHLY DAILY (CO)VARIANCES 'A X"Y"'
C     *           THAT CAN BE FULLY POOLED
C     * LSTAXY1 = LIST OF MEAN INTRA-MONTHLY DAILY (CO)VARIANCES 'A X"Y"'
C     *           THAT CANNOT BE FULLY POOLED
C     * LSTMXY  = LIST OF (CO)VARIANCES OF MONTHLY MEANS 'X-Y-'
C     * LSTSX   = LIST OF STANDARD DEVIATIONS OF MONTHLY MEANS 'S(X)'
C     * LSTANO  = LIST OF ANOMALIES 'X-' TO BE CALCULATED
C
C     * LENMAX  = THE MAXIMAL NUMBER OF VARIABLES IN THE INPUT FILES
C     * NINPMAX = THE MAXIMAL NUMBER OF INPUT FILES
C
      PARAMETER (LENMAX=10000,NINPMAX=50)
      CHARACTER*80 LSTPOOL(LENMAX),LSTDIAG(LENMAX),
     &     LSTX(LENMAX),LSTXY(LENMAX),LSTXY1(LENMAX),LSTMXY(LENMAX),
     &     LSTAXY(LENMAX),LSTAXY1(LENMAX),LSTSX(LENMAX),LSTANO(LENMAX),
     &     LSTMAX(LENMAX),LSTMIN(LENMAX)
      CHARACTER*80 NEWLAB,OLDLAB,WRKLAB,VAR1,VAR2,VAR3
      CHARACTER*512 FILEANO
      CHARACTER*512 FNAME

      REAL X(SIZES_BLONP1xBLAT),Y(SIZES_BLONP1xBLAT),
     & XAVG(SIZES_BLONP1xBLAT),
     & XWNP(SIZES_BLONP1xBLAT)

      INTEGER NP(NINPMAX)
      REAL    WNP(NINPMAX),WNP1(NINPMAX)

      COMMON/ICOM/ IBUF(8),IDAT(SIZES_BLONP1xBLATxNWORDIO)
      EQUIVALENCE (IDAT,NEWLAB)

      INTEGER IABORT
      INTEGER IFD

      DATA MAXX/SIZES_BLONP1xBLATxNWORDIO/
C--------------------------------------------------------------------
      NFF=56
      CALL JCLPNT(NFF,11,12,13,14,
     1     31,32,33,34,35,36,37,38,39,40,
     2     41,42,43,44,45,46,47,48,49,50,
     3     51,52,53,54,55,56,57,58,59,60,
     4     61,62,63,64,65,66,67,68,69,70,
     5     71,72,73,74,75,76,77,78,79,80,5,6)
C
C     * REWIND DATA OUTPUT FILES.
C
      REWIND 11
      REWIND 12
      REWIND 13
      REWIND 14

C     * THE NUMBER OF FILES TO POOL IS DEFINED FROM THE NUMBER OF
C     * COMMAND LINE ARGUMENTS
C
      NFF=NFF-6
      MAXLEN=MAXX+8
C
C     * READ NUMBERS OF YEARS POOLED IN INPUT FILES
C
      READ(5,5010,END=900) IABORT,ITYPE,(NP(NF),NF=1,NFF)                       C4
      LABORT=.FALSE.
      IF (IABORT.EQ.1) THEN
         LABORT=.TRUE.
      ENDIF
      WRITE(6,6010) NFF,IABORT,ITYPE,(NP(NF),NF=1,NFF)
      IF (LABORT) THEN
         WRITE(6,'(A)')
     1 ' THE PROGRAM WILL ABORT IF THERE ARE ANY MISSING VARIABLES.'
      ELSE
         WRITE(6,'(A)')
     1 ' THE PROGRAM WILL POOL EVEN IF THERE ARE MISSING VARIABLES.'
      ENDIF
C
C     * CHECK INPUT PARAMETERS AND CALCULATE TIME AVERAGING WEIGHTS
C
      NPT=0
      MINNP=99999
      MAXNP=0
      DO NF=1,NFF
         IF (NP(NF).LT.0 .OR. (ITYPE.EQ.2.AND.NP(NF).EQ.0)) THEN
            WRITE(6,'(A)')
     1 ' *** ERROR: INVALID INPUT PARAMETERS.'
            CALL                                   XIT('POOL',-1)
         ENDIF
         IF (NP(NF).LT.MINNP) MINNP=NP(NF)
         IF (NP(NF).GT.MAXNP) MAXNP=NP(NF)
         NPT=NPT+MAX(NP(NF),1)
      ENDDO

C
C     * THE FIRST FILE MUST BE A POOLED FILE, IF ANY PRESENT
C
      IF (MAXNP.GT.0.AND.NP(1).EQ.0) THEN
         WRITE(6,'(A)')
     1 ' *** ERROR: THE FIRST INPUT FILE MUST BE A POOLED FILE.'
         CALL                                      XIT('POOL',-2)
      ENDIF

      IF (ITYPE.EQ.0 .OR. ITYPE.EQ.1) THEN
         IF (MINNP.GT.0) THEN
            WRITE(6,'(A)')
     1 ' MULTI-YEAR POOLING OF POOLED FILES.'
            POOLED=.TRUE.
         ELSE IF (MAXNP.EQ.0) THEN
            WRITE(6,'(A)')
     1 ' MULTI-YEAR POOLING OF DIAGN. FILES.'
            POOLED=.FALSE.
         ELSE
            WRITE(6,'(A)')
     1 ' MULTI-YEAR POOLING OF POOLED AND DIAGN. FILES.'
            POOLED=.TRUE.
         ENDIF

         WRITE(6,'(A,I5)')
     1 ' THE TOTAL NUMBER OF YEARS IN THE POOLED OUTPUT FILES =',NPT
         IF (ITYPE.EQ.1) WRITE(6,'(A)')
     1 ' DAILY COVARIANCES ''X"Y"'' WON''T BE FULLY POOLED.'

      ELSE IF (ITYPE.EQ.2 .OR. ITYPE.EQ.3) THEN

         WRITE(6,'(A)') ' SEASONAL POOLING OF DIAGNOSTIC FILES.'
         POOLED=.FALSE.
         WRITE(6,'(A,I5)')
     1 ' THE TOTAL SEASON LENGTH IN THE POOLED OUTPUT FILES =',NPT
         IF (ITYPE.EQ.3) WRITE(6,'(A)')
     1 ' STANDARD DEVIATIONS ''X"Y"'' WON''T BE FULLY POOLED.'

      ELSE
         WRITE(6,'(A,I5)') ' UNKNOWN POOLING TYPE =',ITYPE
         CALL                                      XIT('POOL',-3)
      ENDIF
C
C     * TIME AVERAGING WEIGHTS
C
      SWNP=0.E0
      DO NF=1,NFF
         WNP(NF)=FLOAT(MAX(NP(NF),1))/FLOAT(NPT)
         WNP1(NF)=FLOAT(MAX(NP(NF),1)-1)/FLOAT(NPT)
         SWNP=SWNP+WNP(NF)
      ENDDO
C
C     * 'CORRECTION' FACTOR FOR UNBIASED STANDARD DEVIATIONS
C
      IF (NPT.GT.1) THEN
         FSTD=FLOAT(NPT)/FLOAT(NPT-1)
      ELSE
         FSTD=0.E0
      ENDIF
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C     * STEP 1.
C
C     * READ INPUT FILES TO FIND THE COMMON SET OF SUPERLABELS.
C     * DO THIS SEPARATELY FOR POOLED AND DIAGNOSTIC FILES.
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
      WRITE(6,'(/A)')
     1 ' STEP 1. FINDING COMMON SET OF SUPERLABELS...'
      DO NF=1,NFF
         ITNF=30+NF
         INQUIRE(ITNF,OPENED=STAT)
         IF (STAT) THEN
          INQUIRE(ITNF,NAME=FNAME)
          CLOSE (ITNF)
          OPEN(ITNF,FILE=FNAME,FORM='UNFORMATTED')
          REWIND ITNF
         ENDIF
      ENDDO
C
C     * FIND ALL SUPERLABELS IN THE FIRST INPUT FILE AND
C     * STORE THEM IN THE LIST 'LSTPOOL' OR 'LSTDIAG'
C
      NF=1
      NLBPOOL=0
      NLBDIAG=0
 110  CONTINUE
      CALL FBUFFIN(30+NF,IBUF,MAXLEN,K,LEN)
      IF (K.EQ.0) THEN
         IF (NLBPOOL.EQ.0.AND.NLBDIAG.EQ.0) CALL   XIT('POOL',-4)
         GOTO 115
      ENDIF
      IF (IBUF(1).NE.NC4TO8("LABL")) GOTO 110
      IF (ITYPE.EQ.2 .OR. ITYPE.EQ.3 .OR. NP(NF).EQ.0) THEN
         NLBDIAG=NLBDIAG+1
         IF (NLBDIAG.GT.LENMAX) THEN
           WRITE(6,'(A)')' ***ERROR: TOO MANY VARIABLES IN INPUT FILES.'
           CALL                                    XIT('POOL',-5)
         ENDIF
         LSTDIAG(NLBDIAG)=NEWLAB
      ELSE
         NLBPOOL=NLBPOOL+1
         IF (NLBPOOL.GT.LENMAX) THEN
           WRITE(6,'(A)')' ***ERROR: TOO MANY VARIABLES IN INPUT FILES.'
           CALL                                    XIT('POOL',-6)
         ENDIF
         LSTPOOL(NLBPOOL)=NEWLAB
      ENDIF
      GOTO 110

 115  CONTINUE

      IF (ITYPE.EQ.2 .OR. ITYPE.EQ.3 .OR. NP(NF).EQ.0) THEN
         WRITE(6,6020) NLBDIAG,NF
      ELSE
         WRITE(6,6025) NLBPOOL,NF
      ENDIF
C
C     * CHECK IF THE SAME SET OF SUPERLABELS EXIST IN ALL OTHER INPUT FILES
C
      DO NF=2,NFF
C
C     * READ ALL SUPERLABELS FROM THE NEXT INPUT FILE
C
         NLAB=0
 120     CONTINUE
         CALL FBUFFIN(30+NF,IBUF,MAXLEN,K,LEN)
         IF (K.EQ.0) THEN
            IF (NLAB.EQ.0) CALL                    XIT('POOL',-7)
            GOTO 125
         ENDIF
         IF (IBUF(1).NE.NC4TO8("LABL")) GOTO 120
         NLAB=NLAB+1
         LSTX(NLAB)=NEWLAB
         GOTO 120

 125     CONTINUE

         IF (ITYPE.EQ.2.OR.ITYPE.EQ.3.OR.NP(NF).EQ.0) THEN
C
C     * DIAGNOSTIC FILE...
C
            WRITE(6,6020) NLAB,NF
C
C     *  IF THIS IS THE FIRST DIAGNOSTIC FILE, PUT ALL LABELS INTO LSTDIAG
C
            IF (NLBDIAG.EQ.0) THEN
               NLBDIAG=NLAB
               DO N=1,NLBDIAG
                  LSTDIAG(N)=LSTX(N)
               ENDDO
               GOTO 155
            ENDIF
C
C     * CHECK IF THE NUMBER OF SUPERLABELS IN DIAGNOSTIC FILES DIFFERS
C
            IF (NLBDIAG.NE.NLAB) THEN
               WRITE(6,6030) NF
               IF (LABORT) THEN
                  WRITE(6,6090)
                  CALL                             XIT('POOL',-8)
               ENDIF
            ENDIF
C
C     * COMPARE TWO SUPERLABEL LISTS
C
            N=1
 130        CONTINUE
            IF (N.GT.NLBDIAG) GOTO 135
            DO I=1,NLAB
               IF (LSTDIAG(N).EQ.LSTX(I)) THEN
                  N=N+1
                  GOTO 130
               ENDIF
            ENDDO
C
C     * THE SUPERLABEL IS NOT FOUND...
C
            WRITE(6,6050) LSTDIAG(N),NF
            IF (LABORT) THEN
               WRITE(6,6090)
               CALL                                XIT('POOL',-9)
            ENDIF
C
C     * REMOVE THE SUPERLABEL FROM LSTDIAG
C
            NLBDIAG=NLBDIAG-1
            DO I=N,NLBDIAG
               LSTDIAG(I)=LSTDIAG(I+1)
            ENDDO
            GOTO 130
 135        CONTINUE

         ELSE
C
C     * POOLED FILE...
C
            WRITE(6,6025) NLAB,NF
C
C     * CHECK IF THE NUMBER OF SUPERLABELS IN POOLED FILES DIFFERS
C
            IF (NLBPOOL.NE.NLAB) THEN
               WRITE(6,6035) NF
               IF (LABORT) THEN
                  WRITE(6,6090)
                  CALL                             XIT('POOL',-10)
               ENDIF
            ENDIF
C
C     * COMPARE TWO SUPERLABEL LISTS LSTPOOL AND LSTX
C
            N=1
 150        CONTINUE
            IF (N.GT.NLBPOOL) GOTO 155
            DO I=1,NLAB
               IF (LSTPOOL(N).EQ.LSTX(I)) THEN
                  N=N+1
                  GOTO 150
               ENDIF
            ENDDO
C
C     * THE SUPERLABEL IS NOT FOUND...
C
            WRITE(6,6055) LSTPOOL(N),NF
            IF (LABORT) THEN
               WRITE(6,6090)
               CALL                                XIT('POOL',-11)
            ENDIF
C
C     * REMOVE THE SUPERLABEL FROM LSTPOOL
C
            NLBPOOL=NLBPOOL-1
            DO I=N,NLBPOOL
               LSTPOOL(I)=LSTPOOL(I+1)
            ENDDO
            GOTO 150
         ENDIF
 155     CONTINUE
      ENDDO
C
C     * CHECK IF 'LSTDIAG' IS A SUBSET OF 'LSTPOOL'...
C
      IF (NLBPOOL.GT.0.AND.NLBDIAG.GT.0) THEN
         N=1
 160     CONTINUE
            IF (N.GT.NLBDIAG) GOTO 165
            DO I=1,NLBPOOL
               IF (LSTDIAG(N).EQ.LSTPOOL(I)) THEN
                  N=N+1
                  GOTO 160
               ENDIF
            ENDDO
            WRKLAB(1:6)='    A '
            WRKLAB(7:64)=LSTDIAG(N)(5:62)
            DO I=1,NLBPOOL
               IF (WRKLAB.EQ.LSTPOOL(I)) THEN
                  N=N+1
                  GOTO 160
               ENDIF
            ENDDO
C
C     * THE SUPERLABEL IS NOT FOUND...
C
            WRITE(6,6055) LSTDIAG(N)
            IF (LABORT) THEN
               WRITE(6,6090)
               CALL                                XIT('POOL',-12)
            ENDIF
C
C     * REMOVE THE SUPERLABEL FROM LSTPOOL
C
            NLBPOOL=NLBPOOL-1
            DO I=N,NLBPOOL
               LSTPOOL(I)=LSTPOOL(I+1)
            ENDDO
            GOTO 160
 165     CONTINUE
      ENDIF
C
C     * MOVE ALL LABELS FROM LSTDIAG TO LSTPOOL IF ALL INPUT FILES ARE
C     * DIAGNOSTIC FILES
C
      IF (.NOT.POOLED) THEN
         NLBPOOL=NLBDIAG
         DO N=1,NLBPOOL
            LSTPOOL(N)=LSTDIAG(N)
         ENDDO
      ENDIF
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C     * STEP 2.
C
C     * TO THIS POINT, LSTPOOL CONTAINS THE LIST OF SUPERLABELS
C     * COMMON IN ALL INPUT FILES.
C
C     * CLASSIFY THE SUPERLABELS INTO 'X', 'X"Y"', 'A X"Y"',
C     * 'X-Y-' AND 'S(X)'
C
C     * CAUTION: WE RELY HEAVILY ON THE SPECIFIC FORM OF VARIABLE NAMES!
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
      WRITE(6,'(/A)')
     1 ' STEP 2. CLASSIFYING SUPERLABELS...'
      NX=0
      NMAX=0
      NMIN=0
      NSX=0
      NXY=0
      NXY1=0
      NAXY=0
      NAXY1=0
      NMXY=0
      DO N=1,NLBPOOL
         NEWLAB=LSTPOOL(N)

         IF (NEWLAB(1:16).EQ.'    TIME MAXIMUM'.AND.ITYPE.GE.2) THEN
C
C     * 'TIME MAXIMUM X' (FOR SEASONAL POOLING ONLY)
C
            NMAX=NMAX+1
            LSTMAX(NMAX)=NEWLAB

         ELSE IF (NEWLAB(1:16).EQ.'    TIME MINIMUM'.AND.ITYPE.GE.2)THEN
C
C     * 'TIME MINIMUM X' (FOR SEASONAL POOLING ONLY)
C
            NMIN=NMIN+1
            LSTMIN(NMIN)=NEWLAB

         ELSE IF (ITYPE.EQ.3) THEN
C
C     * IF ITYPE=3, ALL VARIABLES ARE ASSUMED TO BE OF THE FORM 'X'
C     * ('LIGHT' VERSION OF SEASONAL POOLING)
C
            NX=NX+1
            LSTX(NX)=NEWLAB

         ELSE IF (NEWLAB(1:6).EQ.'    S(') THEN
C
C     * 'S(X)'
C
            NSX=NSX+1
            LSTSX(NSX)=NEWLAB

         ELSE IF (ITYPE.EQ.1) THEN
C
C     * IF ITYPE=1, ALL VARIABLES OTHER THAN 'S(X)' ARE ASSUMED TO BE 'X'
C     * ('LIGHT' VERSION OF MULTI-YEAR POOLING)
C
            NX=NX+1
            LSTX(NX)=NEWLAB

         ELSE
C
C     * COUNT PRIMES, DASHES AND STARS IN THE SUPERLABEL
C
            NPRIM=0
            NDASH=0
            NSTAR=0
            DO I=5,64
C     * X"Y" OR X"2
               IF(NEWLAB(I:I).EQ.'"') THEN
                  NPRIM=NPRIM+1
                  I1=MIN(I+1,64)
                  IF (NEWLAB(I1:I1).EQ.'2') THEN
                     NPRIM=NPRIM+1
                  ENDIF
               ENDIF
C     * X-Y- OR X-2
               IF(NEWLAB(I:I).EQ.'-') THEN
                  NDASH=NDASH+1
                  I1=MIN(I+1,64)
                  IF (NEWLAB(I1:I1).EQ.'2') THEN
                     NDASH=NDASH+1
                  ENDIF
               ENDIF
C     * X*Y* OR X*2
               IF(NEWLAB(I:I).EQ.'"') THEN
                  NSTAR=NSTAR+1
                  I1=MIN(I+1,64)
                  IF (NEWLAB(I1:I1).EQ.'2') THEN
                     NSTAR=NSTAR+1
                  ENDIF
               ENDIF
            ENDDO
            IF (NPRIM.EQ.2) THEN
               IF (NEWLAB(1:6).EQ.'    A '.AND.NEWLAB(7:7).NE.' ') THEN
C
C     * 'A X"Y"'
C
                  NAXY=NAXY+1
                  LSTAXY(NAXY)=NEWLAB
               ELSE
C
C     * 'X"Y"' OR 'X"2'
C
                  NXY=NXY+1
                  LSTXY(NXY)=NEWLAB
               ENDIF
            ELSE IF (NSTAR.EQ.2) THEN
               IF (NEWLAB(1:6).EQ.'    A '.AND.NEWLAB(7:7).NE.' ') THEN
C
C     * 'A (X*Y*)R'
C
                  NAXY1=NAXY1+1
                  LSTAXY1(NAXY1)=NEWLAB
               ELSE
C
C     * '(X*Y*)R' OR '(X*2)R'
C
                  NXY1=NXY1+1
                  LSTXY1(NXY1)=NEWLAB
               ENDIF
            ELSE IF (NDASH.EQ.2) THEN
C
C     * 'X-Y-' OR 'X-2'
C
               NMXY=NMXY+1
               LSTMXY(NMXY)=NEWLAB
            ELSE
C
C     * ALL OTHER VARIABLES ARE ASSUMED TO BE 'X'
C
               NX=NX+1
               LSTX(NX)=NEWLAB
            ENDIF
         ENDIF
      ENDDO

C
C     * EXTRACT VARIABLE NAMES 'X' AND 'Y' FROM SUPERLABELS 'X"Y"'.
C     * WE NEED THESE TO COMPLETE CALCULATIONS OF POOLED 'X"Y"'
C
      NANO=0
      N=1
 210  CONTINUE
         IF (N.GT.NXY) GOTO 245
         NEWLAB=LSTXY(N)
         VAR1='    '
         LVAR1=0
         DO I=5,64
            IF (NEWLAB(I:I).EQ.'"') GOTO 215
            VAR1(I:I)=NEWLAB(I:I)
            LVAR1=LVAR1+1
         ENDDO
 215     CONTINUE
         VAR2='    '
         LVAR2=0
         DO I=5,64
            IF (NEWLAB(I+LVAR1+1:I+LVAR1+1).EQ.'"'.OR.
     1          NEWLAB(I+LVAR1+1:I+LVAR1+1).EQ.' ') GOTO 220
            VAR2(I:I)=NEWLAB(I+LVAR1+1:I+LVAR1+1)
            LVAR2=LVAR2+1
         ENDDO
 220     CONTINUE
C
C     * CHECK THE SUPERLABEL REMAINDER. FOR EXAMPLE, SUCH SUPERLABELS
C     * AS 'X"Y" (200MB)' WILL NOT BE POOLED.
C
         VAR3=NEWLAB(LVAR1+LVAR2+7:64)
         IF (VAR3.NE.' ') GOTO 240
C
C     * 'X"2'
C
         IF (VAR2.EQ.'    2') THEN
            VAR2=VAR1
            LVAR2=LVAR1
         ENDIF
C
C     * CHECK IF THE EXTRACTED NAMES ARE IN LSTX
C
         DO I=1,NX
            IF(VAR1.EQ.LSTX(I)) GOTO 225
         ENDDO
         GOTO 240
 225     CONTINUE
         DO I=1,NX
            IF(VAR2.EQ.LSTX(I)) GOTO 228
         ENDDO
         GOTO 240
 228     CONTINUE
C
C     * ADD 'X' AND 'Y' TO LSTANO
C
         DO I=1,NANO
            IF (VAR1.EQ.LSTANO(I)) GOTO 230
         ENDDO
         NANO=NANO+1
         LSTANO(NANO)=VAR1
 230     CONTINUE
         DO I=1,NANO
            IF (VAR2.EQ.LSTANO(I)) GOTO 235
         ENDDO
         NANO=NANO+1
         LSTANO(NANO)=VAR2
 235     CONTINUE
         N=N+1
         GOTO 210
C
C     * MOVE 'X"Y"' FROM LIST LSTXY TO LIST LSTXY1.
C     * (COVARIANCES THAT CANNOT BE FULLY POOLED)
C
 240     CONTINUE
         WRITE(6,6065) LSTXY(N)
         NXY1=NXY1+1
         LSTXY1(NXY1)=NEWLAB
         NXY=NXY-1
         IF (N.GT.NXY) GOTO 245
         DO I=N,NXY
            LSTXY(I)=LSTXY(I+1)
         ENDDO
         GOTO 210
 245  CONTINUE

C
C     * CHECK FOR 'X"Y"' IN ALL 'A X"Y"'
C
      N=1
 250  CONTINUE
         IF (N.GT.NAXY) GOTO 255
         NEWLAB(1:4)='    '
         NEWLAB(5:62)=LSTAXY(N)(7:64)
         DO I=1,NXY
            IF(NEWLAB.EQ.LSTXY(I)) THEN
               N=N+1
               GOTO 250
            ENDIF
         ENDDO
C
C     * MOVE 'A X"Y"' FROM LIST LSTAXY TO LIST LSTAXY1.
C     * (COVARIANCES THAT CANNOT BE FULLY POOLED)
C
         WRITE(6,6065) NEWLAB
         NAXY1=NAXY1+1
         LSTAXY1(NAXY1)=LSTAXY(N)
         NAXY=NAXY-1
         IF (N.GT.NAXY) GOTO 255
         DO I=N,NAXY
            LSTAXY(I)=LSTAXY(I+1)
         ENDDO
         GOTO 250
 255  CONTINUE

C
C     * PRINT SUPERLABELS OF TIME MEANS 'X'
C
      WRITE(6,6100) NX
      DO I=1,NX
         WRITE(6,6200) LSTX(I)
      ENDDO
C
C     * PRINT SUPERLABELS OF TIME MAXIMUM 'X'
C
      WRITE(6,6101) NMAX
      DO I=1,NMAX
         WRITE(6,6200) LSTMAX(I)
      ENDDO
C
C     * PRINT SUPERLABELS OF TIME MINIMUM 'X'
C
      WRITE(6,6102) NMIN
      DO I=1,NMIN
         WRITE(6,6200) LSTMIN(I)
      ENDDO
C
C     * PRINT SUPERLABELS OF MEAN DAILY (CO)VARIANCES 'A X"Y"'
C     * THAT CAN BE FULLY POOLED
C
      WRITE(6,6110) NAXY
      DO I=1,NAXY
         WRITE(6,6200) LSTAXY(I)
      ENDDO
C
C     * PRINT SUPERLABELS OF MEAN DAILY (CO)VARIANCES 'A X"Y"'
C     * THAT CANNOT BE FULLY POOLED
C
      WRITE(6,6115) NAXY1
      DO I=1,NAXY1
         WRITE(6,6200) LSTAXY1(I)
      ENDDO
C
C     * PRINT SUPERLABELS OF DAILY (CO)VARIANCES 'X"Y"'
C     * THAT CAN BE FULLY POOLED
C
      WRITE(6,6120) NXY
      DO I=1,NXY
         WRITE(6,6200) LSTXY(I)
      ENDDO
C
C     * PRINT SUPERLABELS OF DAILY (CO)VARIANCES 'X"Y"' THAT
C     * CANNOT BE FULLY POOLED
C
      WRITE(6,6125) NXY1
      DO I=1,NXY1
         WRITE(6,6200) LSTXY1(I)
      ENDDO
C
C     * PRINT SUPERLABELS OF MONTHLY (CO)VARIANCES 'X-Y-'
C
      WRITE(6,6130) NMXY
      DO I=1,NMXY
         WRITE(6,6200) LSTMXY(I)
      ENDDO
C
C     * PRINT SUPERLABELS OF STANDARD DEVIATIONS 'S(X)'
C
      WRITE(6,6140) NSX
      DO I=1,NSX
         WRITE(6,6200) LSTSX(I)
      ENDDO

C
C     * PRINT SUPERLABELS OF VARS TO CALCULATE ANOMALIES 'X-'
C     * (THESE ARE NEEDED FOR CALCULATING LONG TERM DAILY COVARIANCE 'X"Y"')
C
      WRITE(6,6150) NANO
      DO I=1,NANO
         WRITE(6,6200) LSTANO(I)
      ENDDO

C
C     * CHECK FOR AVAILABILITY OF SOME STATISTICS, IF POOLING POOLED DATA.
C
      IF (POOLED) THEN
C
C     * WE NEED 'X-Y-' FOR CALCULATING 'X"Y"'
C
         N=1
 260     CONTINUE
            IF (N.GT.NXY) GOTO 265
            WRKLAB=LSTXY(N)
            DO I=5,64
               IF(WRKLAB(I:I).EQ.'"')WRKLAB(I:I)='-'
            ENDDO
            DO I=1,NMXY
               IF (WRKLAB.EQ.LSTMXY(I)) THEN
                  N=N+1
                  GOTO 260
               ENDIF
            ENDDO
C
C     * THE SUPERLABEL IS NOT FOUND...
C
            WRITE(6,6070) WRKLAB
            IF (LABORT) THEN
               WRITE(6,6090)
               CALL                                XIT('POOL',-13)
            ENDIF
C
C     * REMOVE IT FROM LSTXY AND CONTINUE
C

            NXY=NXY-1
            IF (N.GT.NXY) GOTO 265
            DO I=N,NXY
               LSTXY(I)=LSTXY(I+1)
            ENDDO
            GOTO 260
 265     CONTINUE

C
C     * WE ALSO NEED 'A X"Y"' FOR CALCULATING 'X"Y"'
C
         N=1
 270     CONTINUE
            IF (N.GT.NXY) GOTO 275
            WRKLAB(1:6)='    A '
            WRKLAB(7:64)=LSTXY(N)(5:62)
            DO I=1,NAXY
               IF (WRKLAB.EQ.LSTAXY(I)) THEN
                  N=N+1
                  GOTO 270
               ENDIF
            ENDDO
C
C     * THE SUPERLABEL IS NOT FOUND...
C
            WRITE(6,6075) WRKLAB
            IF (LABORT) THEN
               WRITE(6,6090)
               CALL                                XIT('POOL',-14)
            ENDIF
C
C     * REMOVE IT FROM LSTXY AND CONTINUE
C
            NXY=NXY-1
            IF (N.GT.NXY) GOTO 275
            DO I=N,NXY
               LSTXY(I)=LSTXY(I+1)
            ENDDO
            GOTO 270
 275     CONTINUE

C
C     * WE NEED 'X' FOR CALCULATING 'S(X)' AND 'A X"Y"' FOR 'S(X"Y")'
C
         N=1
 280     CONTINUE
            IF (N.GT.NSX) GOTO 290
            NEWLAB(1:4)='    '
            NEWLAB(5:62)=LSTSX(N)(7:64)
            DO I=62,1,-1
               IF (NEWLAB(I:I).EQ.')')GOTO 285
            ENDDO
 285        CONTINUE
            NEWLAB(I:I)=' '
C
C     * CHECK FOR 'X' IN LIST 'LSTX'
C
            DO I=1,NX
               IF (NEWLAB.EQ.LSTX(I)) THEN
                  N=N+1
                  GOTO 280
               ENDIF
            ENDDO
C
C     * CHECK FOR 'A X"Y"' IN LIST 'LSTAXY'
C
            WRKLAB(7:64)=NEWLAB(5:62)
            WRKLAB(5:6)='A '
            DO I=1,NAXY
               IF (WRKLAB.EQ.LSTAXY(I)) THEN
                  N=N+1
                  GOTO 280
               ENDIF
            ENDDO
C
C     * CHECK FOR 'A X"Y"' IN LIST 'LSTAXY1'
C
            DO I=1,NAXY1
               IF (WRKLAB.EQ.LSTAXY1(I)) THEN
                  N=N+1
                  GOTO 280
               ENDIF
            ENDDO
C
C     * THE SUPERLABEL IS NOT FOUND...
C
            WRITE(6,6075) WRKLAB
            WRITE(6,6080) NEWLAB
            IF (LABORT) THEN
               WRITE(6,6090)
               CALL                                XIT('POOL',-15)
            ENDIF
C
C     * REMOVE IT FROM LSTSX AND CONTINUE
C
            NSX=NSX-1
            IF (N.GT.NSX) GOTO 290
            DO I=N,NSX
               LSTSX(I)=LSTSX(I+1)
            ENDDO
            GOTO 280
 290     CONTINUE
      ENDIF
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C     * STEP 3.
C
C     * READ INPUT FILES AND CALCULATE AVG 'X' AND 'A X"Y"'
C
C     * UNIT 11 IS OUTPUT FILE FOR TIME AVERAGES 'X', 'A X"Y"'
C     * UNIT 95 IS FOR 'A X"Y"' NEEDED FOR CALCULATING 'X"Y"' LATER ON.
C     * UNIT 96 IS FOR '(NP-1)*S(X)^2' NEEDED FOR CALCULATING S(X).
C     * UNIT 97 IS FOR 'X-Y-' NEEDED FOR CALCULATING 'X"Y"',
C     *         WHEN POOLING POOLED FILES.
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
      WRITE(6,'(/A)')
     1 ' STEP 3. CALCULATING ''X'' AND ''A X"Y"'', IF NECESSARY...'
      DO NF=1,NFF
         ITNF=30+NF
         INQUIRE(ITNF,OPENED=STAT)
         IF (STAT) THEN
          INQUIRE(ITNF,NAME=FNAME)
          CLOSE (ITNF)
          OPEN(ITNF,FILE=FNAME,FORM='UNFORMATTED')
          REWIND ITNF
         ENDIF
      ENDDO
      INQUIRE(11,OPENED=STAT)
      IF (STAT) THEN
       INQUIRE(11,NAME=FNAME)
       CLOSE (11)
       OPEN(11,FILE=FNAME,FORM='UNFORMATTED')
       REWIND 11
      ENDIF

      INQUIRE(95,OPENED=STAT)
      IF (STAT) CLOSE (95)
      OPEN(95,FILE='fort.95',FORM='UNFORMATTED')
      REWIND 95
      INQUIRE(96,OPENED=STAT)
      IF (STAT) CLOSE (96)
      OPEN(96,FILE='fort.96',FORM='UNFORMATTED')
      REWIND 96
      IF (POOLED) THEN
         INQUIRE(97,OPENED=STAT)
         IF (STAT) CLOSE (97)
         OPEN(97,FILE='fort.97',FORM='UNFORMATTED')
         REWIND 97
      ENDIF

      NREC=0
      CALC=.FALSE.
C
C     * READ NEXT RECORD FROM THE FIRST INPUT FILE
C
 300  CONTINUE
      IF (CALC) THEN
         CALL GETFLD2(31,X,-1,-1,-1,-1,IBUF,MAXX,OK)
         IF (.NOT.OK) THEN
            IF (NREC.EQ.0) CALL                    XIT('POOL',-16)
            GOTO 390
         ENDIF
      ELSE
         CALL FBUFFIN(31,IBUF,MAXLEN,K,LEN)
         IF (K.EQ.0) THEN
            IF (NREC.EQ.0) CALL                    XIT('POOL',-17)
            GOTO 390
         ENDIF
      ENDIF
      IF (IBUF(1).EQ.NC4TO8("CHAR")) GO TO 300
      NREC=NREC+1
C
C     * IF THE RECORD IS A SUPERLABEL...
C
      IF (IBUF(1).EQ.NC4TO8("LABL")) THEN
         OLDLAB=NEWLAB
C
C     * CHECK IF THE SUPERLABEL IS IN ONE OF THE REQUIRED LISTS
C
         LABX=.FALSE.
         LABMAX=.FALSE.
         LABMIN=.FALSE.
         LABXY=.FALSE.
         LABXY1=.FALSE.
         LABAXY=.FALSE.
         LABAXY1=.FALSE.
         LABMXY=.FALSE.
         LABSX=.FALSE.
C
C     * 'X'
C
         DO I=1,NX
            IF (NEWLAB.EQ.LSTX(I)) THEN
               LABX=.TRUE.
               GOTO 310
            ENDIF
         ENDDO
C
C     * 'TIME MAXIMUM'
C
         DO I=1,NMAX
            IF (NEWLAB.EQ.LSTMAX(I)) THEN
               LABMAX=.TRUE.
               GOTO 310
            ENDIF
         ENDDO
C
C     * 'TIME MINIMUM'
C
         DO I=1,NMIN
            IF (NEWLAB.EQ.LSTMIN(I)) THEN
               LABMIN=.TRUE.
               GOTO 310
            ENDIF
         ENDDO
C
C     * 'A X"Y"'
C
         DO I=1,NAXY
            IF (NEWLAB.EQ.LSTAXY(I)) THEN
               LABAXY=.TRUE.
               GOTO 310
            ENDIF
         ENDDO
         DO I=1,NAXY1
            IF (NEWLAB.EQ.LSTAXY1(I)) THEN
               LABAXY1=.TRUE.
               GOTO 310
            ENDIF
         ENDDO
C
C     * 'X"Y"', IF THE INPUT FILE IS DIAGNOSTIC
C
         IF (.NOT.POOLED) THEN
            DO I=1,NXY
               IF (NEWLAB.EQ.LSTXY(I)) THEN
                  LABXY=.TRUE.
                  GOTO 310
               ENDIF
            ENDDO
            DO I=1,NXY1
               IF (NEWLAB.EQ.LSTXY1(I)) THEN
                  LABXY1=.TRUE.
                  GOTO 310
               ENDIF
            ENDDO
         ENDIF
C
C     * 'X-Y-'
C
         DO I=1,NMXY
            IF (NEWLAB.EQ.LSTMXY(I)) THEN
               LABMXY=.TRUE.
               GOTO 310
            ENDIF
         ENDDO
C
C     * 'S(X)'
C
         DO I=1,NSX
            IF (NEWLAB.EQ.LSTSX(I)) THEN
               LABSX=.TRUE.
               GOTO 310
            ENDIF
         ENDDO
 310     CONTINUE
         CALC=LABX.OR.LABXY.OR.LABXY1.OR.LABAXY.OR.LABAXY1.OR.
     1        LABMXY.OR.LABSX.OR.LABMAX.OR.LABMIN
         IF (.NOT.CALC) GOTO 300

         IF (LABX.OR.LABMAX.OR.LABMIN.OR.LABXY1.OR.LABAXY1) THEN
            IF (LABXY1.AND.(ITYPE.EQ.0.OR.ITYPE.EQ.1)) THEN
C
C     * MODIFY SUPERLABEL 'X"Y"' TO 'A X"Y"'
C     * DO IT ONLY FOR MULTI-YEAR POOLING.
C
               NEWLAB(1:6)='    A '
               NEWLAB(7:64)=OLDLAB(5:62)
            ENDIF
C
C     * SAVE SUPERLABEL 'X' AND 'A X"Y" THAT CANNOT BE FULLY POOLED TO OUTPUT UNIT11
C
            CALL FBUFOUT(11,IBUF,LEN,K)
            WRITE(6,6200) NEWLAB

         ELSE IF (LABXY) THEN
C
C     * MODIFY SUPERLABEL 'X"Y"' TO 'A X"Y"'
C
            NEWLAB(1:6)='    A '
            NEWLAB(7:64)=OLDLAB(5:62)
C
C     * SAVE SUPERLABEL 'A X"Y"' TO OUTPUT UNIT 11 AND UNIT 95 (NEEDED LATER)
C
            IF (ITYPE.EQ.0) THEN
               CALL FBUFOUT(11,IBUF,LEN,K)
               WRITE(6,6200) NEWLAB
            ENDIF
            CALL FBUFOUT(95,IBUF,LEN,K)

         ELSE IF (LABAXY) THEN
C
C     * SAVE SUPERLABEL 'A X"Y"' TO OUTPUT UNIT 11 AND UNIT 95 (NEEDED LATER)
C
            CALL FBUFOUT(11,IBUF,LEN,K)
            WRITE(6,6200) NEWLAB
            CALL FBUFOUT(95,IBUF,LEN,K)

         ELSE IF (LABSX) THEN
C
C     * SAVE SUPERLABEL 'S(X)' TO UNIT 96 (NEEDED LATER)
C
            CALL FBUFOUT(96,IBUF,LEN,K)

         ELSE IF (LABMXY) THEN
C
C     * SAVE SUPERLABEL 'X-Y-' TO UNIT 97 (NEEDED LATER)
C
            CALL FBUFOUT(97,IBUF,LEN,K)
         ENDIF

C
C     * FIND THE SUPERLABEL IN ALL OTHER INPUT FILES
C
         DO NF=2,NFF
            WRKLAB=OLDLAB
            IF (POOLED.AND.(NP(NF).EQ.0)) THEN
C
C     * SKIP SUPERLABELS 'S(X)' AND 'X-Y-'
C
               IF (LABSX.OR.LABMXY) GOTO 330
               IF (LABAXY.OR.LABAXY1) THEN
C
C     * MODIFY SUPERLABEL 'A X"Y"' TO 'X"Y"'
C
                  WRKLAB(5:62)=OLDLAB(7:64)
               ENDIF
            ENDIF
C
C     * FIRST TRY TO FIND THE REQUIRED LABEL FROM THE CURRENT POSITION.
C
            SCAN=.TRUE.
 320        CALL FBUFFIN(30+NF,IBUF,MAXLEN,K,LEN)
            IF (K.EQ.0) THEN
               IF (.NOT.SCAN) THEN
                  CALL PRTLAB(IBUF)
                  CALL                             XIT('POOL',-18)
               ENDIF
C
C     * IF NOT FOUND, REWIND AND TRY AGAIN FROM THE BEGINNING OF THE FILE.
C
               SCAN=.FALSE.
               ITNF=30+NF
               INQUIRE(ITNF,OPENED=STAT)
               IF (STAT) THEN
                INQUIRE(ITNF,NAME=FNAME)
                CLOSE (ITNF)
                OPEN(ITNF,FILE=FNAME,FORM='UNFORMATTED')
                REWIND ITNF
               ENDIF
               GOTO 320
            ENDIF
            IF ((IBUF(1).NE.NC4TO8("LABL")).OR.
     +          (NEWLAB.NE.WRKLAB)             ) GOTO 320
 330        CONTINUE
         ENDDO

      ELSE
C
C     * THE RECORD IS NOT A SUPERLABEL. DO CALCULATIONS, IF NECESSARY.
C
         IF (.NOT.CALC) GOTO 300
         KIND=IBUF(1)
         NTIME=IBUF(2)
         NAME=IBUF(3)
         LEVEL=IBUF(4)
         NLON=IBUF(5)
         NLAT=IBUF(6)
         NWDS=NLON*NLAT
         IF(KIND.EQ.NC4TO8("SPEC").OR.KIND.EQ.NC4TO8("FOUR"))NWDS=2*NWDS

         IF (LABMAX.OR.LABMIN) THEN
C
C     * START CALCULATING 'TIME MAXIMUM'/'TIME MINIMUM'
C
            DO IJ=1,NWDS
               XAVG(IJ)=X(IJ)
            ENDDO
         ELSE IF (LABSX) THEN
C
C     * START CALCULATING '(NP-1)*S(X)^2'
C
            DO IJ=1,NWDS
               XAVG(IJ)=X(IJ)*X(IJ)*WNP1(1)
            ENDDO
         ELSE
C
C     * START CALCULATING 'X', 'A X"Y"' AND 'X-Y-'
C
            DO IJ=1,NWDS
               XAVG(IJ)=X(IJ)*WNP(1)
            ENDDO
C
C           * TREAT ZERO VALUES IN LPA,LPAC, SALB AS MISSING:
C           * ACCUMULATE POSITIVE VALUES ONLY.
C
            IF(OLDLAB.EQ.'    LPA'.OR.
     +         OLDLAB.EQ.'    LPAC'.OR.
     +         OLDLAB.EQ.'    SALB')THEN
              DO IJ=1,NWDS
                IF(X(IJ).GT.0.E0)THEN
                  XWNP(IJ)=WNP(1)
                ELSE
                  XWNP(IJ)=0.E0
                ENDIF
              ENDDO
            ENDIF
         ENDIF
C
C     * DO FOR ALL OTHER INPUT FILES
C
         DO NF=2,NFF
C
C     * SKIP 'S(X)' OR 'X-Y-' IF THE FILE IS DIAGNOSTIC
C
            IF (POOLED.AND.(NP(NF).EQ.0).AND.(LABMXY.OR.LABSX)) GOTO 340
            CALL GETFLD2(30+NF,X,KIND,-1,NAME,-1,IBUF,MAXX,OK)
            IF (.NOT.OK) THEN
               CALL PRTLAB(IBUF)
               CALL                                XIT('POOL',-19)
            ENDIF
            IF(IBUF(5).NE.NLON.OR.IBUF(6).NE.NLAT) THEN
               CALL PRTLAB(IBUF)
               CALL                                XIT('POOL',-20)
            ENDIF
            IF (LABMAX) THEN
C
C     * TIME MAXIMUM
C
               DO IJ=1,NWDS
                  XAVG(IJ)=MAX(XAVG(IJ),X(IJ))
               ENDDO
            ELSE IF (LABMIN) THEN
C
C     * TIME MINIMUM
C
               DO IJ=1,NWDS
                  XAVG(IJ)=MIN(XAVG(IJ),X(IJ))
               ENDDO
            ELSE IF (LABSX) THEN
C
C     * ACCUMULATE '(NP-1)*S(X)^2'
C
               DO IJ=1,NWDS
                  XAVG(IJ)=XAVG(IJ)+X(IJ)*X(IJ)*WNP1(NF)
               ENDDO
            ELSE
C
C     * ACCUMULATE 'X', 'A X"Y"' AND 'X-Y-'
C
               DO IJ=1,NWDS
                  XAVG(IJ)=XAVG(IJ)+X(IJ)*WNP(NF)
               ENDDO
C
C           * ACCUMULATE POSITIVE VALUES ONLY.
C
               IF(OLDLAB.EQ.'    LPA'.OR.
     +            OLDLAB.EQ.'    LPAC'.OR.
     +            OLDLAB.EQ.'    SALB')THEN
                 DO IJ=1,NWDS
                   IF(X(IJ).GT.0.E0)THEN
                     XWNP(IJ)=XWNP(IJ)+WNP(NF)
                   ENDIF
                 ENDDO
               ENDIF
            ENDIF
            NTIME=NTIME+IBUF(2)
 340        CONTINUE
         ENDDO
C
C        * COMPLETE CALCULATIONS OF LPA, LPAC, SALB
C
         IF(OLDLAB.EQ.'    LPA'.OR.
     +      OLDLAB.EQ.'    LPAC'.OR.
     +      OLDLAB.EQ.'    SALB')THEN
           DO IJ=1,NWDS
             IF(XWNP(IJ).GT.0.E0)THEN
               XAVG(IJ)=XAVG(IJ)*SWNP/XWNP(IJ)
             ENDIF
           ENDDO
         ENDIF
C
C     * SET TIME STAMP
C
         IF(ITYPE.EQ.2.OR.ITYPE.EQ.3) THEN
            IBUF(2)=NTIME
         ELSE
            IBUF(2)=NPT
         ENDIF

         IF (LABX.OR.LABMAX.OR.LABMIN.OR.LABXY1.OR.LABAXY1) THEN
C
C     * SAVE 'X' AND 'A X"Y"' THAT CANNOT BE FULLY POOLED TO OUTPUT UNIT 11
C
            CALL PUTFLD2(11,XAVG,IBUF,MAXX)

         ELSE IF (LABXY) THEN
C
C     * SAVE 'A X"Y"' TO OUTPUT UNIT 11 AND UNIT 95 (NEEDED LATER)
C
            IF (ITYPE.EQ.0) THEN
               CALL PUTFLD2(11,XAVG,IBUF,MAXX)
            ENDIF
            CALL PUTFLD2(95,XAVG,IBUF,MAXX)

         ELSE IF (LABAXY) THEN
C
C     * SAVE 'A X"Y"' TO OUTPUT UNIT 11 AND UNIT 95 (NEEDED LATER)
C
            CALL PUTFLD2(11,XAVG,IBUF,MAXX)
            CALL PUTFLD2(95,XAVG,IBUF,MAXX)

         ELSE IF (LABSX) THEN
C
C     * SAVE '(NP-1)*S(X)^2' TO UNIT 96 (NEEDED LATER)
C
            CALL PUTFLD2(96,XAVG,IBUF,MAXX)

         ELSE IF (LABMXY)  THEN
C
C     * SAVE 'X-Y-' TO UNIT 97 (NEEDED LATER)
C
            CALL PUTFLD2(97,XAVG,IBUF,MAXX)
         ENDIF
      ENDIF
      GOTO 300
 390  CONTINUE
C
C     * SKIP THE REST IF ITYPE=3 ('LIGHT' SEASONAL POOLING)
C
      IF (ITYPE.EQ.3) GOTO 590
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C     * STEP 4.
C
C     * READ INPUT FILES ONCE AGAIN AND CALCULATE 'S(X)' AND 'XANO'
C
C     * UNIT 11 IS THE OUTPUT FILE WITH PREVIOUSLY CALCULATED TIME
C     *         AVERAGES 'X' AND 'A X"Y"'
C     * UNIT 12 IS THE OUTPUT FILE FOR 'S(X)'
C     * UNIT 96 CONTAINS TERMS '(NP-1)*S(X)^2' CALCULATED PREVIOUSLY
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
      WRITE(6,'(/A)')
     1 ' STEP 4. CALCULATING ''S(X)'', IF NECESSARY, AND ''XANO''...'
      DO NF=1,NFF
         ITNF=30+NF
         INQUIRE(ITNF,OPENED=STAT)
         IF (STAT) THEN
          INQUIRE(ITNF,NAME=FNAME)
          CLOSE (ITNF)
          OPEN(ITNF,FILE=FNAME,FORM='UNFORMATTED')
          REWIND ITNF
         ENDIF
      ENDDO

      INQUIRE(11,OPENED=STAT)
      IF (STAT) THEN
       INQUIRE(11,NAME=FNAME)
       CLOSE (11)
       OPEN(11,FILE=FNAME,FORM='UNFORMATTED')
       REWIND 11
      ENDIF

      INQUIRE(12,OPENED=STAT)
      IF (STAT) THEN
       INQUIRE(12,NAME=FNAME)
       CLOSE (12)
       OPEN(12,FILE=FNAME,FORM='UNFORMATTED')
       REWIND 12
      ENDIF

      INQUIRE(96,OPENED=STAT)
      IF (STAT) CLOSE (96)
      OPEN(96,FILE='fort.96',FORM='UNFORMATTED')
      REWIND 96

      CALC=.FALSE.
 400  CONTINUE
      IF (CALC) THEN
         CALL GETFLD2(31,X,-1,-1,-1,-1,IBUF,MAXX,OK)
         IF (.NOT.OK) GOTO 490
      ELSE
         CALL FBUFFIN(31,IBUF,MAXLEN,K,LEN)
         IF (K.EQ.0) GOTO 490
      ENDIF
      IF (IBUF(1).EQ.NC4TO8("CHAR")) GO TO 400
C
C     * IF THE RECORD IS A SUPERLABEL...
C
      IF (IBUF(1).EQ.NC4TO8("LABL")) THEN
         NEWANO=.TRUE.
         OLDLAB=NEWLAB
C
C     * CHECK IF THE SUPERLABEL IS IN ONE OF THE OF THE REQUIRED LISTS
C
         LABX=.FALSE.
         LABXY=.FALSE.
         LABXY1=.FALSE.
         LABAXY=.FALSE.
         LABAXY1=.FALSE.
         DO I=1,NX
            IF (NEWLAB.EQ.LSTX(I)) THEN
               LABX=.TRUE.
               GOTO 410
            ENDIF
         ENDDO
C
C     * FIND 'A X"Y"'...
C
         DO I=1,NAXY
            IF (NEWLAB.EQ.LSTAXY(I)) THEN
               LABAXY=.TRUE.
               GOTO 410
            ENDIF
         ENDDO
         DO I=1,NAXY1
            IF (NEWLAB.EQ.LSTAXY1(I)) THEN
               LABAXY1=.TRUE.
               GOTO 410
            ENDIF
         ENDDO
C
C     * OR 'X"Y"', IF THE FILE IS DIAGNOSTIC
C
         IF (.NOT.POOLED) THEN
            DO I=1,NXY
               IF (NEWLAB.EQ.LSTXY(I)) THEN
                  LABXY=.TRUE.
                  GOTO 410
               ENDIF
            ENDDO
            DO I=1,NXY1
               IF (NEWLAB.EQ.LSTXY1(I)) THEN
                  LABXY1=.TRUE.
                  GOTO 410
               ENDIF
            ENDDO
         ENDIF
 410     CONTINUE
         CALC=LABX.OR.LABXY.OR.LABXY1.OR.LABAXY.OR.LABAXY1
         IF (.NOT.CALC) GOTO 400
C
C     * CHECK IF THE SUPERLABEL IS IN LSTANO
C
         LABANO=.FALSE.
         DO I=1,NANO
            IF (NEWLAB.EQ.LSTANO(I)) THEN
               LABANO=.TRUE.
               GOTO 415
            ENDIF
         ENDDO
 415     CONTINUE
C
C     * IF POOLING IS SEASONAL, WE NEED ONLY ANOMALIES
C
         IF (ITYPE.EQ.2) THEN
            CALC=LABANO
            IF (.NOT.CALC) GOTO 400
            GOTO 425
         ENDIF
C
C     * MODIFY SUPERLABEL 'X' TO 'S(X)'
C
         NEWLAB(1:6)='    S('
         IF (OLDLAB(1:6).EQ.'    A ') THEN
            NEWLAB(7:64)=OLDLAB(7:64)
         ELSE
            NEWLAB(7:64)=OLDLAB(5:62)
         ENDIF
         DO I=64,1,-1
            IF (NEWLAB(I:I).NE.' ') THEN
               NEWLAB(I+1:I+1)=')'
               GOTO 420
            ENDIF
         ENDDO
 420     CONTINUE
C
C     * SAVE SUPERLABELS 'S(X)' TO UNIT 12
C
         IBUF(2)=NPT
         CALL FBUFOUT(12,IBUF,LEN,K)
         WRITE(6,6200) NEWLAB

 425     CONTINUE
C
C     * FIND THE SUPERLABEL IN ALL OTHER INPUT FILES
C
         DO NF=2,NFF
            WRKLAB=OLDLAB
            IF ((LABAXY.OR.LABAXY1).AND.POOLED.AND.(NP(NF).EQ.0)) THEN
C
C     * MODIFY SUPERLABEL 'A X"Y"' TO 'X"Y"'
C
               WRKLAB(5:62)=OLDLAB(7:64)
            ENDIF
C
C     * FIRST TRY TO FIND THE REQUIRED LABEL FROM THE CURRENT RECORD.
C
            SCAN=.TRUE.
 430        CALL FBUFFIN(30+NF,IBUF,MAXLEN,K,LEN)
            IF (K.EQ.0) THEN
               IF (.NOT.SCAN) THEN
                  CALL PRTLAB(IBUF)
                  CALL                             XIT('POOL',-21)
               ENDIF
C
C     * IF NOT FOUND, REWIND AND TRY AGAIN FROM THE BEGINNING OF THE FILE.
C
               SCAN=.FALSE.
               ITNF=30+NF
               INQUIRE(ITNF,OPENED=STAT)
               IF (STAT) THEN
                INQUIRE(ITNF,NAME=FNAME)
                CLOSE (ITNF)
                OPEN(ITNF,FILE=FNAME,FORM='UNFORMATTED')
                REWIND ITNF
               ENDIF
               GOTO 430
            ENDIF
            IF ((IBUF(1).NE.NC4TO8("LABL")).OR.
     +          (NEWLAB.NE.WRKLAB)             ) GOTO 430
         ENDDO
C
C     * FIND THE CORRESPONDING SUPERLABEL 'X' OR 'A X"Y"' FROM UNIT 11
C
         IF (LABXY.OR.LABXY1) THEN
            WRKLAB(1:6)='    A '
            WRKLAB(7:64)=OLDLAB(5:62)
         ELSE
            WRKLAB=OLDLAB
         ENDIF
 435     CONTINUE
         CALL FBUFFIN(11,IBUF,MAXLEN,K,LEN)
         IF (K.EQ.0) THEN
            CALL PRTLAB(IBUF)
            CALL                                   XIT('POOL',-22)
         ENDIF
         IF ((IBUF(1).NE.NC4TO8("LABL")).OR.(NEWLAB.NE.WRKLAB)) GOTO 435
C
C     * FIND THE CORRESPONDING SUPERLABEL 'S(X)' FROM UNIT 96,
C     * IF POOLING POOLED DATA
C
         IF (POOLED) THEN
            WRKLAB(1:6)='    S('
            IF (LABX) THEN
               WRKLAB(7:64)=OLDLAB(5:62)
            ELSE
               WRKLAB(7:64)=OLDLAB(7:64)
            ENDIF
            DO I=64,1,-1
               IF (WRKLAB(I:I).NE.' ') THEN
                  WRKLAB(I+1:I+1)=')'
                  GOTO 440
               ENDIF
            ENDDO
 440        CONTINUE
            SCAN=.TRUE.
 450        CALL FBUFFIN(96,IBUF,MAXLEN,K,LEN)
            IF (K.EQ.0) THEN
               IF (.NOT.SCAN) THEN
                  CALL PRTLAB(IBUF)
                  CALL                             XIT('POOL',-23)
               ENDIF
C
C     * IF NOT FOUND, REWIND AND TRY AGAIN FROM THE BEGINNING OF THE FILE.
C
               SCAN=.FALSE.
               INQUIRE(96,OPENED=STAT)
               IF (STAT) CLOSE (96)
               OPEN(96,FILE='fort.96',FORM='UNFORMATTED')
               REWIND 96
               GOTO 450
            ENDIF
            IF ((IBUF(1).NE.NC4TO8("LABL")).OR.
     +          (NEWLAB.NE.WRKLAB)             ) GOTO 450
         ENDIF

      ELSE
C
C     * THE RECORD IS NOT A SUPERLABEL. DO CALCULATIONS, IF NECESSARY.
C
         IF (.NOT.CALC) GOTO 400
         KIND=IBUF(1)
         NTIME=IBUF(2)
         NAME=IBUF(3)
         LEVEL=IBUF(4)
         NLON=IBUF(5)
         NLAT=IBUF(6)
         NWDS=NLON*NLAT
         IF(KIND.EQ.NC4TO8("SPEC").OR.
     +      KIND.EQ.NC4TO8("FOUR")    ) NWDS=2*NWDS
C
C     * READ AVG FROM UNITS 11
C
         CALL GETFLD2(11,XAVG,KIND,-1,NAME,-1,IBUF,MAXX,OK)
         IF (.NOT.OK) THEN
            CALL PRTLAB(IBUF)
            CALL                                   XIT('POOL',-24)
         ENDIF
         DO IJ=1,NWDS
            X(IJ)=X(IJ)-XAVG(IJ)
         ENDDO

         IF (LABANO) THEN
C
C     * CONSTRUCT FILE NAME FOR ANOMALIES
C
            IF (NEWANO) THEN
               WRITE(6,'(A64,A)') OLDLAB,' ANO'
               VAR1=OLDLAB(5:64)
               FILEANO=' '
               DO I=64,1,-1
                  IF(VAR1(I:I).NE.' ') GOTO 480
               ENDDO
 480           CONTINUE
               LVAR1=I
               FILEANO(1:I)=VAR1(1:I)
C
C     * REPLACE IN FILE NAMES ALL '/' AND ' ' WITH '_'
C
               DO I=1,LVAR1
                  IF ( FILEANO(I:I).EQ.'/' .OR.
     1                 FILEANO(I:I).EQ.' ') FILEANO(I:I)='_'
               ENDDO
C
C     * APPEND 'ANO_POOL' FOR UNIQUENESS
C
               FILEANO(LVAR1+1:LVAR1+8)='ANO_POOL'
C
C     * OPEN FILE
C
               INQUIRE(1,OPENED=STAT)
               IF (STAT) CLOSE (1)
               OPEN(1,FILE=FILEANO,FORM='UNFORMATTED')
               REWIND 1
               NEWANO=.FALSE.
            ENDIF
C
C     * SAVE ANOMALIES
C
            CALL PUTFLD2(1,X,IBUF,MAXX)
         ENDIF
C
C     * START CALCULATING VARIANCE
C
         IF (ITYPE.EQ.0 .OR. ITYPE.EQ.1) THEN
            DO IJ=1,NWDS
               Y(IJ)=X(IJ)*X(IJ)*WNP(1)
            ENDDO
         ENDIF
C
C     * DO FOR ALL OTHER INPUT FILES
C
         DO NF=2,NFF
            CALL GETFLD2(30+NF,X,KIND,-1,NAME,-1,IBUF,MAXX,OK)
            IF (.NOT.OK) THEN
               CALL PRTLAB(IBUF)
               CALL                                XIT('POOL',-25)
            ENDIF
            IF(IBUF(5).NE.NLON.OR.IBUF(6).NE.NLAT) THEN
               CALL PRTLAB(IBUF)
               CALL                                XIT('POOL',-26)
            ENDIF
C
C     * CALCULATE ANOMALIES
C
            DO IJ=1,NWDS
               X(IJ)=X(IJ)-XAVG(IJ)
            ENDDO
            IF (LABANO) THEN
C
C     * SAVE ANOMALIES
C
               CALL PUTFLD2(1,X,IBUF,MAXX)
            ENDIF
C
C     * ACCUMULATE VARIANCE
C
            IF (ITYPE.EQ.0 .OR. ITYPE.EQ.1) THEN
               DO IJ=1,NWDS
                  Y(IJ)=Y(IJ)+X(IJ)*X(IJ)*WNP(NF)
               ENDDO
            ENDIF
         ENDDO
C
C     * SKIP THE REST, IF POOLING IS SEASONAL
C
         IF (ITYPE.EQ.2) GOTO 400
C
C     * READ AND ADD MEAN VARIANCE FROM UNIT 96, IF POOLING POOLED DATA
C
         IF (POOLED) THEN
            CALL GETFLD2(96,X,KIND,-1,-1,-1,IBUF,MAXX,OK)
            IF (.NOT.OK) THEN
               CALL PRTLAB(IBUF)
               CALL                                XIT('POOL',-27)
            ENDIF
            IF(IBUF(5).NE.NLON.OR.IBUF(6).NE.NLAT) THEN
               CALL PRTLAB(IBUF)
               CALL                                XIT('POOL',-28)
            ENDIF
            DO IJ=1,NWDS
               Y(IJ)=Y(IJ)+X(IJ)
            ENDDO
         ENDIF
C
C     * COMPLETE CALCULATION OF 'S(X)'
C
         DO IJ=1,NWDS
            Y(IJ)=SQRT(Y(IJ)*FSTD)
         ENDDO
C
C     * SAVE STD TO UNIT 12
C
         IBUF(2)=NPT
         CALL PUTFLD2(12,Y,IBUF,MAXX)
      ENDIF
      GOTO 400
 490  CONTINUE
C
C     * SKIP THE REST IF ITYPE=1 ('LIGHT' MULTI-YEAR POOLING)
C
      IF (ITYPE.EQ.1) GOTO 590
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C     * STEP 5.
C
C     * CALCULATE 'X"Y"' AND 'X-Y-'
C
C     * UNIT 13 IS THE OUTPUT FILE FOR 'X"Y"'
C     * UNIT 14 IS THE OUTPUT FILE FOR 'X-Y-'
C     * UNIT 95 CONTAIN 'A X"Y"' CALCULATED PREVIOUSLY
C     * UNIT 97 CONTAIN 'X-Y-' CALCULATED PREVIOUSLY
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
      WRITE(6,'(/A)')
     1 ' STEP 5. CALCULATING ''X"Y"'' AND ''X-Y-'', IF NECESSARY,...'

      INQUIRE(13,OPENED=STAT)
      IF (STAT) THEN
       INQUIRE(13,NAME=FNAME)
       CLOSE (13)
       OPEN(13,FILE=FNAME,FORM='UNFORMATTED')
       REWIND 13
      ENDIF

      INQUIRE(14,OPENED=STAT)
      IF (STAT) THEN
       INQUIRE(14,NAME=FNAME)
       CLOSE (14)
       OPEN(14,FILE=FNAME,FORM='UNFORMATTED')
       REWIND 14
      ENDIF

      INQUIRE(95,OPENED=STAT)
      IF (STAT) CLOSE (95)
      OPEN(95,FILE='fort.95',FORM='UNFORMATTED')
      REWIND 95

      INQUIRE(97,OPENED=STAT)
      IF (STAT) CLOSE (97)
      OPEN(97,FILE='fort.97',FORM='UNFORMATTED')
      REWIND 97

      NREC=0
 500  CONTINUE
      CALL FBUFFIN(95,IBUF,MAXLEN,K,LEN)
      IF (K.EQ.0) THEN
         IF (NREC.NE.NXY) CALL                     XIT('POOL',-29)
         GOTO 590
      ENDIF
      IF (IBUF(1).EQ.NC4TO8("CHAR")) GO TO 500
      NREC=NREC+1
      IF(IBUF(1).NE.NC4TO8("LABL")) GOTO 500
C
C     * EXTRACT NAMES X AND Y FROM THE SUPERLABEL 'A X"Y"'
C
      NEWLAB(5:62)=NEWLAB(7:64)
      VAR1=' '
      LVAR1=0
      DO I=1,60
         IF (NEWLAB(4+I:4+I).EQ.'"') GOTO 510
         VAR1(I:I)=NEWLAB(4+I:4+I)
         LVAR1=LVAR1+1
      ENDDO
 510  CONTINUE

      VAR2=' '
      LVAR2=0
      DO I=1,60
         IF (NEWLAB(4+I+LVAR1+1:4+I+LVAR1+1).EQ.'"'.OR.
     1       NEWLAB(4+I+LVAR1+1:4+I+LVAR1+1).EQ.' ') GOTO 520
         VAR2(I:I)=NEWLAB(4+I+LVAR1+1:4+I+LVAR1+1)
         LVAR2=LVAR2+1
      ENDDO
 520  CONTINUE
C
C     * 'X"2
C
      IF (VAR2.EQ.'2') THEN
         VAR2=VAR1
         LVAR2=LVAR1
      ENDIF
C
C     * SAVE SUPERLABELS 'X"Y"' TO UNIT 13
C
      CALL FBUFOUT(13,IBUF,LEN,K)
      WRITE(6,6200) NEWLAB

      IF (ITYPE.EQ.0) THEN
C
C     * SAVE SUPERLABELS 'X-Y-' TO UNIT 14
C
         DO I=5,64
            IF(NEWLAB(I:I).EQ.'"')NEWLAB(I:I)='-'
         ENDDO
         CALL FBUFOUT(14,IBUF,LEN,K)
      ENDIF
C
C     * OPEN FILES WITH ANOMALIES
C
      INQUIRE(1,OPENED=STAT)
      IF (STAT) CLOSE (1)
      INQUIRE(2,OPENED=STAT)
      IF (STAT) CLOSE (2)

      FILEANO=VAR1
      DO I=1,LVAR1
         IF ( FILEANO(I:I).EQ.'/' .OR.
     1        FILEANO(I:I).EQ.' ') FILEANO(I:I)='_'
      ENDDO
      FILEANO(LVAR1+1:LVAR1+8)='ANO_POOL'
      OPEN(1,FILE=FILEANO,FORM='UNFORMATTED')
      REWIND 1

      IF (VAR1.NE.VAR2) THEN
         FILEANO=VAR2
         DO I=1,LVAR2
            IF ( FILEANO(I:I).EQ.'/' .OR.
     1           FILEANO(I:I).EQ.' ') FILEANO(I:I)='_'
         ENDDO
         FILEANO(LVAR2+1:LVAR2+8)='ANO_POOL'
         OPEN(2,FILE=FILEANO,FORM='UNFORMATTED')
         REWIND 2
      ENDIF
C
C     * CALCULATE X-Y-
C
 530  CONTINUE
      DO NF=1,NFF
 540     CALL GETFLD2(1,X,-1,-1,-1,-1,IBUF,MAXX,OK)
         IF (.NOT.OK) THEN
            IF(NF.NE.1) THEN
               CALL PRTLAB(IBUF)
               CALL                                XIT('POOL',-30)
            ENDIF
            GOTO 500
         ENDIF
         IF (IBUF(1).EQ.NC4TO8("CHAR")) GO TO 540
         IF (NF.EQ.1) THEN
            KIND=IBUF(1)
            LEVEL=IBUF(4)
            NLON=IBUF(5)
            NLAT=IBUF(6)
            NWDS=NLON*NLAT
            IF(KIND.EQ.NC4TO8("SPEC").OR.
     +         KIND.EQ.NC4TO8("FOUR")    ) NWDS=2*NWDS
            DO IJ=1,NWDS
               XAVG(IJ)=0.E0
            ENDDO
         ENDIF
         IF (VAR1.NE.VAR2) THEN
            CALL GETFLD2(2,Y,KIND,-1,-1,-1,IBUF,MAXX,OK)
            IF (.NOT.OK) THEN
               CALL PRTLAB(IBUF)
               CALL                                XIT('POOL',-31)
            ENDIF
            DO IJ=1,NWDS
               XAVG(IJ)=XAVG(IJ)+X(IJ)*Y(IJ)*WNP(NF)
            ENDDO
         ELSE
            DO IJ=1,NWDS
               XAVG(IJ)=XAVG(IJ)+X(IJ)*X(IJ)*WNP(NF)
            ENDDO
         ENDIF
      ENDDO
C
C     * READ AND ADD 'X-Y-' FROM UNIT 97
C
      IF (ITYPE.EQ.0) THEN
         IF (POOLED) THEN
            CALL GETFLD2(97,X,KIND,-1,-1,-1,IBUF,MAXX,OK)
            IF (.NOT.OK) THEN
               CALL PRTLAB(IBUF)
               CALL                                XIT('POOL',-32)
            ENDIF
            DO IJ=1,NWDS
               XAVG(IJ)=XAVG(IJ)+X(IJ)
            ENDDO
         ENDIF
C
C     * SAVE 'X-Y-' TO UNIT 14
C
         IBUF(2)=NPT
         CALL PUTFLD2(14,XAVG,IBUF,MAXX)
      ENDIF
C
C     * READ AND ADD 'A X"Y"'
C
      CALL GETFLD2(95,X,KIND,-1,-1,-1,IBUF,MAXX,OK)
      IF (.NOT.OK) THEN
         CALL PRTLAB(IBUF)
         CALL                                      XIT('POOL',-33)
      ENDIF
      DO IJ=1,NWDS
         XAVG(IJ)=XAVG(IJ)+X(IJ)
      ENDDO
C
C     * SAVE 'X"Y"' TO UNIT 13
C
      CALL PUTFLD2(13,XAVG,IBUF,MAXX)

      GOTO 530

 590  CONTINUE
C
C     * NORMAL EXIT
C
      CALL                                         XIT('POOL',0)
C
C     * E.O.F. ON INPUT.
C
  900 CALL                                         XIT('POOL',-99)
C--------------------------------------------------------------------
 5010 FORMAT(10X,I3,I2/4(10X,14I5/))                                            C4
 6010 FORMAT(' NUMBER OF INPUT FILES =',I5/
     1     ' INPUT CARD PARAMETERS:'/
     2     ' IABORT = ',I2/' POOLING TYPE = ',I2/
     3     ' NP1,NP2,... = ',50I5/)
 6020 FORMAT(I5,' SUPERLABELS ARE FOUND IN DIAGN. FILE ',I5)
 6025 FORMAT(I5,' SUPERLABELS ARE FOUND IN POOLED FILE ',I5)
 6030 FORMAT(' THE NUMBER OF SUPERLABELS IN DIAGN. FILE',
     1     I5,' DIFFERS FROM THAT IN THE FIRST DIAGN. FILE.')
 6035 FORMAT(' THE NUMBER OF SUPERLABELS IN POOLED FILE',
     1     I5,' DIFFERS FROM THAT IN THE FIRST POOLED FILE.')
 6050 FORMAT(' SUPERLABEL ',A64,' IS NOT FOUND IN DIAGN. FILE ',I5)
 6055 FORMAT(' SUPERLABEL ',A64,' IS NOT FOUND IN POOLED FILE ',I5)
 6060 FORMAT(' SUPERLABEL ',A64,
     1     ' FROM DIAGN. FILES IS NOT FOUND IN POOLED FILES.')

 6065 FORMAT(' COVARIANCE ',A64,' CANNOT BE FULLY POOLED.')

 6070 FORMAT(' SUPERLABEL ',A64,' IS NOT FOUND IN LIST OF ''X-Y-''')
 6075 FORMAT(' SUPERLABEL ',A64,' IS NOT FOUND IN LIST OF ''A X"Y"''')
 6080 FORMAT(' SUPERLABEL ',A64,' IS NOT FOUND IN LIST OF ''X''')
 6090 FORMAT(' ***ERROR. TRY TO SET ''poolabort=off'' AND RUN AGAIN.')

 6100 FORMAT(/' NUMBER OF SUPERLABELS OF TYPE ''X''     =',I5,':')
 6101 FORMAT(/' NUMBER OF SUPERLABELS OF TYPE ''TIMMAX''=',I5,':')
 6102 FORMAT(/' NUMBER OF SUPERLABELS OF TYPE ''TIMMIN''=',I5,':')
 6110 FORMAT(/' NUMBER OF SUPERLABELS OF TYPE ''A X"Y"'''/
     1          ' THAT CAN BE FULLY POOLED             =',I5,':')
 6115 FORMAT(/' NUMBER OF SUPERLABELS OF TYPE ''A X"Y"'''/
     1          ' THAT CANNOT BE FULLY POOLED          =',I5,':')
 6120 FORMAT(/' NUMBER OF SUPERLABELS OF TYPE ''X"Y"'''/
     1          ' THAT CAN BE FULLY POOLED              =',I5,':')
 6125 FORMAT(/' NUMBER OF SUPERLABELS OF TYPE ''X"Y"'''/
     1          ' THAT CANNOT BE FULLY POOLED           =',I5,':')
 6130 FORMAT(/' NUMBER OF SUPERLABELS OF TYPE ''X-Y-''  =',I5,':')
 6140 FORMAT(/' NUMBER OF SUPERLABELS OF TYPE ''S(X)''  =',I5,':')
 6150 FORMAT(/' NUMBER OF SUPERLABELS TO CALCULATE ANOMALIES =',I5,':')
 6200 FORMAT(A64)
      END
