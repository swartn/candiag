      PROGRAM ACYCLE
C     PROGRAM ACYCLE(X,      AC,      ANO,      VAR,      INPUT,                C2
C    1                                                   OUTPUT,        )       C2
C    2         TAPE1=X,TAPE2=AC,TAPE3=ANO,TAPE4=VAR,TAPE5=INPUT,
C    3                                              TAPE6=OUTPUT)
C     -----------------------------------------------------------               C2
C                                                                               C2
C     SEP 30/04 - S.KHARIN                                                      C2
C                                                                               C2
CACYCLE  - COMPUTES AND SUBTRACTS THE MEAN ANNUAL CYCLE.                1  3 C  C1
C                                                                               C3
CAUTHOR  - S.KHARIN                                                             C3
C                                                                               C3
CPURPOSE - COMPUTES AND SUBTRACTS THE MEAN ANNUAL CYCLE.                        C3
C          OPERATES BOTH ON SERIES OF GRIDS AND TIME SERIES.                    C3
C                                                                               C3
CINPUT FILES...                                                                 C3
C                                                                               C3
C       X = INPUT (MULTI-LEVEL) FIELDS OR TIME SERIES.                          C3
C                                                                               C3
COUTPUT FILES...                                                                C3
C                                                                               C3
C      AC = (OPTIONAL) CONTAINS THE ANNUAL CYCLE.                               C3
C     ANO = (OPTIONAL) CONTAINS ANOMALIES FROM THE ANNUAL CYCLE.                C3
C     VAR = (OPTIONAL) CONTAINS VARIANCE.                                       C3
C                                                                               C3
C
CINPUT PARAMETERS...
C                                                                               C5
C     LENAC  = LENGTH OF ANNUAL CYCLE (12 FOR MONTHLY DATA, 365 FOR DAILY, ETC.)C5
C              IF LENAC<0, THEN READ THE ANNUAL CYCLE FROM THE INPUT FILE AC.   C5
C     NYEARS = NUMBER OF YEARS FROM WHICH THE ANNUAL CYCLE IS COMPUTED.         C5
C            = 0, USE ALL RECORDS IN THE INPUT FILE.                            C5
C     NSKIP  = NUMBER OF YEARS TO SKIP FROM THE BEGINNING.                      C5
C     LABORT = 0 ABORT IF THE LAST YEAR IS INCOMPLETE,                          C5
C              OTHERWISE PRINT A WARNING AND CONTINUE.                          C5
C                                                                               C5
CEXAMPLE OF INPUT CARD...                                                       C5
C                                                                               C5
C*ACYCLE          12        10                                                  C5
C---------------------------------------------------------------------------
C
      use diag_sizes, only : SIZES_BLONP1xBLAT,
     &                       SIZES_BLONP1xBLATxNWORDIO,
     &                       SIZES_LONP1xLAT,
     &                       SIZES_MAXLEV

      IMPLICIT NONE


      INTEGER MAXG

      PARAMETER (MAXG=SIZES_LONP1xLAT*6*SIZES_MAXLEV)

      INTEGER, ALLOCATABLE, DIMENSION(:) :: NY
      REAL, ALLOCATABLE, DIMENSION(:) :: X, AC, VAR

      LOGICAL OK,SPEC,LAC,LANO,LVAR,LACREAD
      REAL ACAVG,VARAVG
      REAL SPVAL,SPVALT
      INTEGER LEV(SIZES_MAXLEV),JBUF(8)
      INTEGER NF,LENAC,NYEARS,NSKIP,LABORT,NC4TO8,
     +     I,IW,K,L,LEN,NWDS,NREC,N,NR,NYR,KIND,NAME,NTIME,NTIME1,
     +     NLEV,NRECMAX
      INTEGER MAXX,MAXL,MAXAC,MAXRSZ
C
C     * IUA/IUST  - ARRAY KEEPING TRACK OF UNITS ASSIGNED/STATUS.
C
      INTEGER IUA(100),IUST(100)
      COMMON /JCLPNTU/ IUA,IUST

      INTEGER IBUF(8),IDAT(SIZES_BLONP1xBLATxNWORDIO)
      COMMON/ICOM/IBUF,IDAT


      DATA MAXX,MAXL,MAXAC,MAXRSZ/
     & SIZES_BLONP1xBLATxNWORDIO,SIZES_MAXLEV,
     & 366,SIZES_BLONP1xBLAT/
      DATA SPVAL/1.E+38/,SPVALT/1.E+32/
C---------------------------------------------------------------------------
      NF=6
      CALL JCLPNT(NF,1,2,3,4,5,6)
      IF (NF.LT.4) CALL                            XIT('ACYCLE',-1)
      REWIND 1
C
C     * READ INPUT PARAMETERS
C
      READ(5,5010,END=900) LENAC,NYEARS,NSKIP,LABORT                            C5
      WRITE(6,6010) LENAC,NYEARS,NSKIP,LABORT
      IF (LENAC.EQ.0) LENAC=1
      IF (LENAC.GT.MAXAC) CALL                     XIT('ACYCLE',-2)
      LACREAD=.FALSE.
      IF (LENAC.LT.0) THEN
        LENAC=-LENAC
        LACREAD=.TRUE.
      ENDIF
C
C     * ANALYSE COMMAND LINE ARGUMENTS
C
      LAC=.FALSE.
      IF (IUST(2).EQ.1) THEN
        IF (.NOT.LACREAD) THEN
          LAC=.TRUE.
        ENDIF
        REWIND 2
      ENDIF
      LANO=.FALSE.
      IF (IUST(3).EQ.1) THEN
        LANO=.TRUE.
        REWIND 3
      ENDIF
      LVAR=.FALSE.
      IF (IUST(4).EQ.1) THEN
        LVAR=.TRUE.
        REWIND 4
      ENDIF
C
C     * FIND THE NUMBER OF LEVELS, TYPE OF DATA, ETC...
C
      CALL FILEV(LEV,NLEV,IBUF,1)
      IF (NLEV.EQ.0) THEN
        WRITE(6,6020)
        CALL                                       XIT('ACYCLE',-3)
      ENDIF
      IF (NLEV.GT.MAXL) CALL                       XIT('ACYCLE',-4)
      KIND=IBUF(1)
      NAME=IBUF(3)
      NWDS=IBUF(5)*IBUF(6)
      SPEC=(KIND.EQ.NC4TO8("FOUR").OR.KIND.EQ.NC4TO8("SPEC"))
      IF (SPEC) NWDS=NWDS*2
      IF (NWDS.GT.MAXRSZ.OR.
     1    NWDS*NLEV*LENAC.GT.MAXG) CALL            XIT('ACYCLE',-5)
      WRITE(6,6030) NAME,NLEV,(LEV(L),L=1,NLEV)
      DO I=1,8
        JBUF(I)=IBUF(I)
      ENDDO
      CALL PRTLAB(IBUF)

      ALLOCATE(NY(NWDS*NLEV*LENAC), AC(NWDS*NLEV*LENAC), 
     &         VAR(NWDS*NLEV*LENAC))
      ALLOCATE(X(MAXX))
      IF(KIND.EQ.NC4TO8("TIME"))GO TO 200
C
C     * INPUT FILE IS TIME SERIES OF GRIDS, SPECTRAL COEFFICIENTS ETC.
C
      IF(.NOT.LACREAD) THEN
        WRITE(6,'(A)')' SAVE ANNUAL CYCLE...'
C
C       * SKIP THE FIRST NSKIP YEARS
C
        DO N=1,NSKIP
          DO K=1,LENAC
            DO L=1,NLEV
              CALL FBUFFIN(1,IBUF,-8,I,LEN)
              IF (I.GE.0) CALL                     XIT('ACYCLE',-6)
            ENDDO
          ENDDO
        ENDDO
C
C       * START ACCUMULATING THE ANNUAL CYCLE
C
        DO IW=1,LENAC*NLEV*NWDS
          AC(IW)=0.E0
          NY(IW)=0
        ENDDO
        NREC=0
        NYR=0
 100    CONTINUE
        DO K=1,LENAC
          DO L=1,NLEV
            IW=((K-1)*NLEV+L-1)*NWDS
            CALL GETFLD2(1,X,-1,-1,-1,LEV(L),IBUF,MAXX,OK)
            IF(.NOT.OK) THEN
              IF (K.NE.1) THEN
                WRITE(6,6050)
                IF (LABORT.EQ.0)THEN
                  WRITE(6,6040)NREC
                  WRITE(6,6042)NYR
                  WRITE(6,6055)
                  CALL                             XIT('ACYCLE',-7)
                ENDIF
              ENDIF
              GO TO 110
            ENDIF
C
C           * MAKE SURE THAT ALL RECORDS HAVE THE SAME KIND AND DIMENSIONS
C
            CALL CMPLBL(0,IBUF,0,JBUF,OK)
            IF (.NOT.OK) THEN
              CALL PRTLAB (IBUF)
              CALL PRTLAB (JBUF)
              CALL                                 XIT('ACYCLE',-8)
            ENDIF
C
C           * ACCUMULATE THE ANNUAL CYCLE
C
            DO I=1,NWDS
              IF(ABS(X(I)-SPVAL).GT.SPVALT)THEN
                AC(IW+I)=AC(IW+I)+X(I)
                NY(IW+I)=NY(IW+I)+1
              ENDIF
            ENDDO
          ENDDO
          NREC=NREC+1
        ENDDO
        NYR=NYR+1
        IF (NYEARS.EQ.0.OR.NYR.LT.NYEARS) GO TO 100
 110    CONTINUE
C
C       * COMPLETE CALCULATIONS OF THE ANNUAL CYCLE
C
        DO K=1,LENAC
          DO L=1,NLEV
            IW=((K-1)*NLEV+L-1)*NWDS
            DO I=1,NWDS
              IF(NY(IW+I).GT.0)THEN
                AC(IW+I)=AC(IW+I)/FLOAT(NY(IW+I))
              ELSE
                AC(IW+I)=SPVAL
              ENDIF
            ENDDO
C
C           * SAVE AC
C
            IF (LAC) THEN
              IBUF(2)=K
              IBUF(4)=LEV(L)
              IBUF(8)=MIN(2,IBUF(8))
              IF(K.EQ.1.AND.L.EQ.1)CALL PRTLAB(IBUF)
              CALL PUTFLD2(2,AC(IW+1),IBUF,MAXX)
            ENDIF
          ENDDO
        ENDDO
        WRITE(6,6040)NREC
        WRITE(6,6042)NYR
      ELSE
C
C       * READ ANNUAL CYCLE
C
        WRITE(6,'(A)')' READ ANNUAL CYCLE...'
        DO K=1,LENAC
          DO L=1,NLEV
            IW=((K-1)*NLEV+L-1)*NWDS
            CALL GETFLD2(2,AC(IW+1),-1,-1,-1,LEV(L),IBUF,MAXX,OK)
            IF(.NOT.OK) CALL                       XIT('ACYCLE',-9)
C
C           * MAKE SURE THAT ALL RECORDS HAVE THE SAME NAME, KIND AND DIMENSIONS
C
            CALL CMPLBL(0,IBUF,0,JBUF,OK)
            IF (.NOT.OK .OR. IBUF(3).NE.JBUF(3)) THEN
              CALL PRTLAB (IBUF)
              CALL PRTLAB (JBUF)
              CALL                                 XIT('ACYCLE',-10)
            ENDIF
            IF(K.EQ.1.AND.L.EQ.1) CALL PRTLAB(IBUF)
          ENDDO
        ENDDO
      ENDIF
C
C     * CALCULATE ANOMALIES
C
      IF (LANO.OR.LVAR) THEN
        IF(LANO)WRITE(6,'(A)')' SAVE ANOMALIES...'
        IF(LVAR)THEN
          DO IW=1,LENAC*NLEV*NWDS
            VAR(IW)=0.E0
            NY(IW)=0
          ENDDO
        ENDIF
        REWIND 1
        NREC=0
 120    CONTINUE
        K=MOD(NREC,LENAC)+1
        DO L=1,NLEV
          IW=((K-1)*NLEV+L-1)*NWDS
          CALL GETFLD2(1,X,-1,-1,-1,LEV(L),IBUF,MAXX,OK)
          IF(.NOT.OK) GO TO 130
          DO I=1,NWDS
            IF(ABS(X(I)-SPVAL).GT.SPVALT.AND.
     1         ABS(AC(IW+I)-SPVAL).GT.SPVALT)THEN
              X(I)=X(I)-AC(IW+I)
              IF(LVAR)THEN
                VAR(IW+I)=VAR(IW+I)+X(I)**2
                NY(IW+I)=NY(IW+I)+1
              ENDIF
            ELSE
              X(I)=SPVAL
            ENDIF
          ENDDO
          IF(NREC.EQ.0.AND.L.EQ.1) CALL PRTLAB(IBUF)
          IF(LANO)CALL PUTFLD2(3,X,IBUF,MAXX)
        ENDDO
        NREC=NREC+1
        GO TO 120
 130    CONTINUE
C
C       * CALCULATE VARIANCE
C
        IF(LVAR)THEN
          WRITE(6,'(A)')' SAVE VARIANCE...'
          DO K=1,LENAC
            DO L=1,NLEV
              IW=((K-1)*NLEV+L-1)*NWDS
              DO I=1,NWDS
                IF(NY(IW+I).GT.1)THEN
                  VAR(IW+I)=VAR(IW+I)/FLOAT(NY(IW+I))
                ELSE
                  VAR(IW+I)=SPVAL
                ENDIF
              ENDDO
C
C             * SAVE VAR
C
              IBUF(2)=K
              IBUF(4)=LEV(L)
              IBUF(8)=MIN(2,IBUF(8))
              IF(K.EQ.1.AND.L.EQ.1)CALL PRTLAB(IBUF)
              CALL PUTFLD2(4,VAR(IW+1),IBUF,MAXX)
            ENDDO
          ENDDO
        ENDIF
        WRITE(6,6045) NREC
      ENDIF
      CALL                                         XIT('ACYCLE',0)
C
C-------------------------------------------------------------------------------
C
 200  CONTINUE
      WRITE(6,'(A)')' INPUT FILE CONTAIN TIME SERIES.'
C
C     * INPUT FILE CONTAIN TIME SERIES (KIND=4HTIME)
C
      NTIME1=0
      NR=0
 210  CONTINUE
      CALL GETFLD2(1,X,NC4TO8("TIME"),-1,-1,-1,IBUF,MAXX,OK)
      IF (.NOT.OK) THEN
        WRITE(6,6060) NR
        CALL                                       XIT('ACYCLE',0)
      END IF
      NR=NR+1
      NTIME=IBUF(5)
      IF(NTIME.NE.NTIME1) THEN
C
C       * LENGTH TIME SERIES HAS CHANGED
C
        CALL PRTLAB(IBUF)
        IF(MOD(NTIME,LENAC).NE.0)THEN
          WRITE(6,6050)
          IF (LABORT.EQ.0)THEN
            WRITE(6,6055)
            CALL                                   XIT('ACYCLE',-11)
          ENDIF
        ENDIF
        IF (NTIME.LE.NSKIP*LENAC)CALL              XIT('ACYCLE',-12)
      ENDIF
C
C     * COMPUTE THE ANNUAL CYCLE
C
      IF(.NOT.LACREAD) THEN
        DO K=1,LENAC
          AC(K)=0.E0
          NY(K)=0
        ENDDO
        NREC=0
        IF(NYEARS.EQ.0)THEN
          NRECMAX=NTIME
        ELSE
          NRECMAX=MIN(NTIME,(NSKIP+NYEARS)*LENAC)
        ENDIF
        DO N=NSKIP*LENAC+1,NRECMAX
          K=MOD(N-1,LENAC)+1
          IF(ABS(X(N)-SPVAL).GT.SPVALT)THEN
            AC(K)=AC(K)+X(N)
            NY(K)=NY(K)+1
          ENDIF
          NREC=NREC+1
        ENDDO
        IF(NR.EQ.1.OR.NTIME.NE.NTIME1)WRITE(6,6040)NREC
C
C       * COMPLETE CALCULATIONS OF THE ANNUAL CYCLE
C
        DO K=1,LENAC
          IF(NY(K).GT.0)THEN
            AC(K)=AC(K)/FLOAT(NY(K))
          ELSE
            AC(K)=SPVAL
          ENDIF
        ENDDO
C
C       * SAVE AC
C
        IF (LAC) THEN
          IBUF(5)=LENAC
          IBUF(8)=MIN(2,IBUF(8))
          CALL PUTFLD2(2,AC,IBUF,MAXX)
        ENDIF
      ELSE
C
C       * READ ANNUAL CYCLE
C
        CALL GETFLD2(2,AC,NC4TO8("TIME"),-1,-1,-1,IBUF,MAXX,OK)
        IF(.NOT.OK) CALL                           XIT('ACYCLE',-13)
        IF(IBUF(5).NE.LENAC)CALL                   XIT('ACYCLE',-14)
      ENDIF
C
C     * CALCULATE ANOMALIES, MEAN AND VARIANCE
C
      IF (LANO.OR.LVAR) THEN
        
        DO K=1,LENAC
          VAR(K)=0.E0
          NY(K)=0
        ENDDO
        DO N=1,NTIME
          K=MOD(N-1,LENAC)+1
          IF(ABS(X(N)-SPVAL).GT.SPVALT.AND.
     1       ABS(AC(K)-SPVAL).GT.SPVALT)THEN
            X(N)=X(N)-AC(K)
            VAR(K)=VAR(K)+X(N)**2
            NY(K)=NY(K)+1
          ENDIF
        ENDDO
        DO K=1,LENAC
          IF(NY(K).GT.0)THEN
            VAR(K)=VAR(K)/FLOAT(NY(K))
          ELSE
            VAR(K)=SPVAL
          ENDIF
        ENDDO
C
C       * SAVE ANOMALIES
C
        IBUF(5)=NTIME
        CALL PUTFLD2(3,X,IBUF,MAXX)
        IF(NR.EQ.1.OR.NTIME.NE.NTIME1)WRITE(6,6045)NTIME
      ENDIF
C
C     * SAVE VARIANCE
C
      IF(LVAR)THEN
        IBUF(5)=LENAC
        IBUF(8)=MIN(2,IBUF(8))
        CALL PUTFLD2(4,VAR,IBUF,MAXX)
      ENDIF
C
C     * PRINT OVERALL STATISTICS
C
      ACAVG=0.E0
      VARAVG=0.E0
      DO K=1,LENAC
        ACAVG=ACAVG+AC(K)
        VARAVG=VARAVG+VAR(K)
      ENDDO
      ACAVG=ACAVG/FLOAT(LENAC)
      VARAVG=VARAVG/FLOAT(LENAC)
      IF(NR.EQ.1)WRITE(6,6070) NR,ACAVG,VARAVG,SQRT(VARAVG)

      NTIME1=NTIME
      GO TO 210
C
C     * E.O.F. ON INPUT.
C
  900 CALL                                         XIT('ACYCLE',-15)
C-----------------------------------------------------------------------------
 5010 FORMAT(10X,3I10,I5)                                                       C5
 6010 FORMAT(' LENAC=',I10,' NYEARS=',I10,' NSKIP=',I10,' LABORT=',I5)
 6020 FORMAT('0..ACYCLE *** ERROR: INPUT FILE IS EMPTY.')
 6030 FORMAT(' NAME =',A4/' NLEVS =',I5/
     1       ' LEVELS = ',15I6/100(10X,15I6/))
 6040 FORMAT(' NUMBER OF STEPS IN INPUT FILE:',I10)
 6042 FORMAT(' NUMBER OF YEARS IN INPUT FILE:',I10)
 6045 FORMAT(' NUMBER OF STEPS IN ANOM. FILE:',I10)
 6050 FORMAT('0..ACYCLE *** WARNING: LAST YEAR IS INCOMPLETE.')
 6055 FORMAT('0..ACYCLE *** ABORT. USE LABORT=1 TO CONTINUE.')
 6060 FORMAT(' READ ',I10,' TIME SERIES.')
 6070 FORMAT(' NR =',I10,' AVG =',1P1E14.6,
     1                   ' VAR =',1P1E14.6,' SD =',1P1E14.6)
      END
