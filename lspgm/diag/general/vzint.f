      PROGRAM VZINT
C     PROGRAM VZINT(FIELD,       DZ,       INTDZ,       OUTPUT,         )       C2
C    1        TAPE1=FIELD, TAPE2=DZ, TAPE3=INTDZ, TAPE6=OUTPUT)
C     ---------------------------------------------------------                 C2
C                                                                               C2
C     SEP 21/2009 - D.YANG (BYPASS SUPERLABEL - ONLY READ LEV(L))               C2
C     MAY 12/2009 - D.YANG (USE INPUT OCEAN FIELD TIME STAMP IN THE OUTPUT FILE)
C     JUL 03/2003 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)
C     MAY 06/2002 - B.MIVILLE (REVISED FOR NEW DATA DESCRIPTION FORMAT)
C     MAR 05/2002 - F.MAJAESS (REVISED FOR "CHAR" KIND RECORDS)
C     JUN 06/2001 - B.MIVILLE - ORIGINAL VERSION
C                                                                               C2
CVZINT   - VERTICAL INTEGRAL OF OCEAN FIELD                             2  1    C1
C                                                                               C3
CAUTHOR  - B. MIVILLE                                                           C3
C                                                                               C3
CPURPOSE - VERTICAL INTEGRAL OF OCEAN FIELD                                     C3
C                                                                               C3
CINPUT FILES...                                                                 C3
C                                                                               C3
C   FIELD = INPUT OCEAN FIELD (PRE-MASKED BY BETA)                              C3
C   DZ    = VERTICAL GRID CELL HEIGHT (3-D)                                     C3
C                                                                               C3
COUTPUT FILE...                                                                 C3
C                                                                               C3
C   INTDZ = VERTICAL INTEGRATION RESULTS                                        C3
C                                                                               C3
C-------------------------------------------------------------------------------
C
C     * MAXIMUM 2-D GRID SIZE (I+2)*(J+2)=IM*JM --> IJM
C     * FOR MAX GRID SIZE OF I*J PLUS ONE MORE ON EACH SIDE.
C
C     * MAXIMUM NUMBER OF LEVELS IS SET IN "KM"
C
      use diag_sizes, only : SIZES_NWORDIO,
     &                       SIZES_OLAT,
     &                       SIZES_OLEV,
     &                       SIZES_OLON

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)


      PARAMETER (
     & IM=SIZES_OLON+2,
     & JM=SIZES_OLAT+2,
     & IJM=IM*JM,
     & IJMV=IJM*SIZES_NWORDIO)
      PARAMETER (KM=SIZES_OLEV)
C
      REAL FIELD(IJM),DZ(IJM),INTDZ(IJM)
      INTEGER LEV(KM)
      INTEGER NWORDS,I,L,NF,NR,NLON,NLAT,FLABEL,LEVEL,LEVELP,NLEV
C
      LOGICAL OK
      COMMON/ICOM/IBUF(8),IDAT(IJMV)
      DATA MAXX/IJMV/
C---------------------------------------------------------------------
C
      NF=4
      CALL JCLPNT(NF,1,2,3,6)
      REWIND 1
      REWIND 2
      REWIND 3
C     * INPUT FILE NUMBER FOR DZ
      NDZ=2
C
C     * FIND NUMBER OF DZ LEVELS
C
      CALL FILEV(LEV,NLEV,IBUF,NDZ)
      IF(NLEV.EQ.0.OR.NLEV.GT.KM) CALL             XIT('VZINT',-1)
C
      NR=0
      LEVELP=0
      LEVELPF=999999
C
C     * READ OCEAN FIELD LEVEL BY LEVEL
C
 200  DO 300 L=1,NLEV
C
         CALL GETFLD2 (1,FIELD,NC4TO8("GRID"),-1,-1,LEV(L),
     +                                        IBUF,MAXX,OK)
         IF (.NOT.OK)THEN
            IF (NR.EQ.0) CALL                      XIT('VZINT',-2)
            CALL                                   XIT('VZINT',0)
         ENDIF
C
C        * SAVE FIELD TIME STAMP & LEVEL
C
         ITIME=IBUF(2)
         LEVEL=IBUF(4)
C
         IF(NR.EQ.0)THEN
C
C           * SAVE ORIGINAL FIELD LABEL
C
            FLABEL=IBUF(3)
            NLON=IBUF(5)
            NLAT=IBUF(6)
            NWORDS=NLON*NLAT
C
         ELSE
            IF(LEVEL.LT.LEVELP)THEN
               WRITE(6,6000)' * PROBLEMS WITH LEVELS IN FIELD FILE *'
               CALL                                XIT('VZINT',-3)
            ENDIF
C
            IF(LEVEL.GT.LEVELPF)THEN
               WRITE(6,6000)' * PROBLEMS WITH LEVELS IN FIELD FILE *'
               CALL                                XIT('VZINT',-4)
            ENDIF
         ENDIF
C
C        * CHECK IF FIELD CHANGED LABEL
C
         IF(IBUF(3).NE.FLABEL) THEN
            WRITE(6,6000)' FIELD NAME HAS CHANGED '
            WRITE(6,6005) 'RECORD NUMBER: ',NR+1
            CALL                                   XIT('VZINT',-5)
         ENDIF
C
C        * CHECK IF FIELD HAS SAME DIMENSION THROUGHT THE INPUT FILE
C
         IF((IBUF(5).NE.NLON).OR.(IBUF(6).NE.NLAT))THEN
            WRITE(6,6000) ' * DIMENSION OF FIELD HAS CHANGED *'
            WRITE(6,6005) 'RECORD NUMBER: ',NR+1
            CALL                                   XIT('VZINT',-6)
         ENDIF
C
C        * READ DZ
C
C        * REWIND IF NEEDED
         IF(L.EQ.1.AND.NR.NE.0) THEN
            NF=-NDZ
         ELSE
            NF=NDZ
         ENDIF
C
         CALL GETFLD2(NF,DZ,NC4TO8("GRID"),-1,-1,LEV(L),
     +                                     IBUF,MAXX,OK)
         IF(.NOT.OK) CALL                          XIT('VZINT',-7)
         LEVDZ=IBUF(4)
         IF(LEVDZ.NE.LEVEL) THEN
            WRITE(6,6010) LEVEL,LEVDZ
            CALL                                   XIT('VZINT',-8)
         ENDIF
C
C        * CHECK IF DZ HAS SAME DIMENSION AS INPUT FIELD
C
         IF((IBUF(5).NE.NLON).OR.(IBUF(6).NE.NLAT))THEN
            WRITE(6,6000) ' * DIMENSION OF DZ DO NOT MATCH FIELD *'
            WRITE(6,6005) 'RECORD NUMBER: ',NR+1
            CALL                                   XIT('VZINT',-9)
         ENDIF
C
C        * INITIALIZE AND INTEGRATE
C
         IF(L.EQ.1)THEN
            DO 240 IJ=1,NWORDS
               INTDZ(IJ) = 0.0E0
               INTDZ(IJ) = FIELD(IJ) * DZ(IJ)
 240        CONTINUE
         ELSE
            DO 250 IJ=1,NWORDS
               INTDZ(IJ) = INTDZ(IJ) + FIELD(IJ) * DZ(IJ)
 250        CONTINUE
         ENDIF
C
         NR=NR+1
C
         LEVELP=LEVEL
C
 300  CONTINUE
C
      LEVELP=0
      LEVELPF=LEVEL
C
C     * WRITE RESULTS TO FILE
C
      IBUF(2)=ITIME
      IBUF(3)=FLABEL
      IBUF(4)=1
      IBUF(5)=NLON
      IBUF(6)=NLAT
      IBUF(8)=1
C
      CALL PUTFLD2(3,INTDZ,IBUF,MAXX)
C
      GOTO 200
C
C---------------------------------------------------------------------
 6000 FORMAT(A)
 6005 FORMAT(A,I8)
 6010 FORMAT('LEVELS DO NOT MATCH. FIELD: ',I6,'  DZ: ',I6)
      END
