      PROGRAM SPZXMLT 
C     PROGRAM SPZXMLT (SPIN,       ZXIN,       SPOUT,       OUTPUT,     )       E2
C    1           TAPE1=SPIN, TAPE2=ZXIN, TAPE3=SPOUT, TAPE6=OUTPUT) 
C     -------------------------------------------------------------             E2
C                                                                               E2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       E2
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                
C     MAY 14/83 - R.LAPRISE.                                                    
C     DEC 04/80 - J.D.HENDERSON 
C     JUL 18/79 - TED SHEPHERD. 
C                                                                               E2
CSPZXMLT - MULTIPLY SPECTRAL FILE BY CROSS-SECTION FILE                 2  1    E1
C                                                                               E3
CAUTHORS - T.SHEPHERD, J.D.HENDERSON                                            E3
C                                                                               E3
CPURPOSE - MULTIPLIES A COMPLEX-VALUED SPECTRAL COEFFICIENT FILE (SPIN)         E3
C          BY A GLOBALLY-AVERAGED ZONAL CROSS-SECTION FILE (ZXIN) WITH          E3
C          THE SAME VERTICAL LEVEL STRUCTURE. ZXIN FILE MUST BE CONSTANT        E3
C          ON EACH LEVEL.                                                       E3
C          NOTE - USUALLY THE FILES ARE ON PRESSURE LEVELS, AND THE             E3
C                 ZONAL FILE (ZXIN) IS THE GLOBAL AVERAGE OF DEL-HAT.           E3
C                                                                               E3
CINPUT FILES...                                                                 E3
C                                                                               E3
C      SPIN  = SPECTRAL MULTI-LEVEL FILE.                                       E3
C      ZXIN  = CROSS-SECTION FILE CORRESPONDING TO SPIN.                        E3
C                                                                               E3
COUTPUT FILE...                                                                 E3
C                                                                               E3
C      SPOUT = SPECTRAL FILE OF SPIN MULTIPLIED BY ZXIN.                        E3
C-------------------------------------------------------------------------- 
C 
      use diag_sizes, only : SIZES_LA,
     &                       SIZES_LAT,
     &                       SIZES_LMTP1,
     &                       SIZES_LONP1,
     &                       SIZES_MAXLONP1LAT,
     &                       SIZES_NWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      integer, parameter ::
     & MAXZ = max(SIZES_LONP1,SIZES_LAT)*SIZES_NWORDIO 
      integer, parameter :: 
     & MAXX = 2*(SIZES_LA+SIZES_LMTP1)*SIZES_NWORDIO 

      COMMON/BLANCK/F(2*(SIZES_LA+SIZES_LMTP1)),
     & G(SIZES_MAXLONP1LAT)
C 
      LOGICAL OK
      COMMON/ICOM/IBUF(8),IDAT(MAXX)
      COMMON/JCOM/JBUF(8),JDAT(MAXZ) 
C-----------------------------------------------------------------------
      NFF=4 
      CALL JCLPNT(NFF,1,2,3,6)
      REWIND 1
      REWIND 2
      REWIND 3
      NR=0
C 
C     * READ NEXT RECORD FROM INPUT.
C 
  100 CALL GETFLD2(1,F,NC4TO8("SPEC"),-1,-1,-1,IBUF,MAXX,OK)
      IF(.NOT.OK)THEN 
        IF(NR.EQ.0) CALL                           XIT('SPZXMLT',-1)
        WRITE(6,6020) NR
        CALL                                       XIT('SPZXMLT',0) 
      ENDIF 
      IF(NR.EQ.0) WRITE(6,6030) IBUF
C 
      CALL GETFLD2(2,G,NC4TO8("ZONL"),-1,-1,IBUF(4),JBUF,MAXZ,OK)
      IF(.NOT.OK) CALL                             XIT('SPZXMLT',-2)
      IF(NR.EQ.0) WRITE(6,6030) JBUF
C 
C     * MULTIPLY SPECTRAL COEFFICIENTS BY CONSTANT ZX FILE. 
C 
      NWDS=2*IBUF(5)*IBUF(6)
      DO 200 I=1,NWDS 
  200 F(I)=F(I)*G(1)
C 
C     * SAVE ON FILE OUT. 
C 
      CALL PUTFLD2(3,F,IBUF,MAXX)
      IF(NR.EQ.0) WRITE(6,6030) IBUF
      NR=NR+1 
      GO TO 100 
C-----------------------------------------------------------------------
 6020 FORMAT('0SPZXMLT READ',I6,' RECORDS')
 6030 FORMAT(' ',A4,I10,2X,A4,I10,4I6)
      END
