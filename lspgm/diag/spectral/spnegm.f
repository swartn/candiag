      PROGRAM SPNEGM
C     PROGRAM SPNEGM (SPIN,       SPOUT,       INPUT,       OUTPUT,     )       E2
C    1          TAPE1=SPIN, TAPE2=SPOUT, TAPE5=INPUT, TAPE6=OUTPUT) 
C     -------------------------------------------------------------             E2
C                                                                               E2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       E2
C     FEB 15/94 - F.MAJAESS (REVISE FOR TRUNCATIONS > 99)                       
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                
C     MAR   /85  - G.J.BOER                                                     
C                                                                               E2
CSPNEGM  - PRODUCES SPECTRAL HARMONIC "-M CONJUGATE"                    1  1    E1
C                                                                               E3
CAUTHOR  - G.J. BOER                                                            E3
C                                                                               E3
CPURPOSE - PRODUCES THE SPECTRAL HARMONIC COEFFICIENTS WHICH ARISE              E3
C          WHEN THE INDEX IS "CONJUGATED", IE WHEN M IS NEGATIVE.               E3
C          THESE ARE (-1)**M TIMES THE COMPLEX CONJUGATE OF THE INPUT           E3
C          SPHERICAL HARMONIC COEFFICIENT.                                      E3
C                                                                               E3
CINPUT FILE...                                                                  E3
C                                                                               E3
C      SPIN   = ANY COMPLEX ARRAY (MAX 2000 COMPLEX WORDS)                      E3
C                                                                               E3
COUTPUT FILE...                                                                 E3
C                                                                               E3
C      SPOUT = SPHERICAL COEFFICIENT CONJUGATE OF SPIN                          E3
C---------------------------------------------------------------------------
C 
      use diag_sizes, only : SIZES_LA,
     &                       SIZES_LMTP1,
     &                       SIZES_NWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      integer, parameter :: 
     & MAXX = 2*(SIZES_LA+SIZES_LMTP1)*SIZES_NWORDIO

      LOGICAL OK
      DIMENSION LSR(2,SIZES_LMTP1+1)
      COMPLEX F(SIZES_LA+SIZES_LMTP1)
      COMMON/LCM/F
      COMMON/BUFCOM/IBUF(8),IDAT(MAXX)
C-----------------------------------------------------------------------
      NFF=3 
      CALL JCLPNT(NFF,1,2,6)
      NRECS=0 
      REWIND 1
      REWIND 2
  
  100 CALL GETFLD2(1,F,NC4TO8("SPEC"),0,0,0,IBUF,MAXX,OK)
      IF(.NOT.OK)THEN 
        IF(NRECS.EQ.0) CALL                        XIT('SPNEGM',-1) 
        WRITE(6,6020) NRECS 
        CALL                                       XIT('SPNEGM',0)
      ENDIF 
      IF(NRECS.EQ.0) CALL PRTLAB (IBUF)
      CALL DIMGT(LSR,LA,LR,LM,KTR,IBUF(7))
  
      A=-1.E0 
      DO 200 M=1,LM 
         NN=LSR(1,M+1)-LSR(1,M) 
         A=-1.E0*A
         DO 200 J=1,NN
            N=M+J-2 
            F(LSR(1,M)-1+J)=A*CONJG(F(LSR(1,M)-1+J))
  200 CONTINUE
  
      CALL PUTFLD2(2,F,IBUF,MAXX)
      IF(NRECS.EQ.0) CALL PRTLAB (IBUF)
      NRECS=NRECS+1 
      GO TO 100 
C-----------------------------------------------------------------------
6020  FORMAT('0SPNEGM COMPUTED ',I5,' RECORDS')
      END
