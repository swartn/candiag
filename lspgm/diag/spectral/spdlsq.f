      PROGRAM SPDLSQ
C     PROGRAM SPDLSQ (SPIN,       SPOUT,       INPUT,       OUTPUT,     )       E2
C    1          TAPE1=SPIN, TAPE2=SPOUT, TAPE5=INPUT, TAPE6=OUTPUT) 
C     -------------------------------------------------------------             E2
C                                                                               E2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       E2
C     FEB 15/94 - F.MAJAESS (REVISE FOR TRUNCATIONS > 99)                       
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                
C     MAY 13/83 - R.LAPRISE.                                                    
C     DEC 01/80 - J.D.HENDERSON 
C     JAN   /80 - S. LAMBERT
C                                                                               E2
CSPDLSQ  - COMPUTES SPECTRAL LAPLACIAN                                  1  1   GE1
C                                                                               E3
CAUTHOR  - S.LAMBERT,J.D.HENDERSON                                              E3
C                                                                               E3
CPURPOSE - COMPUTES THE SPHERICAL HARMONIC COEFFICIENTS OF THE LAPLACIAN        E3
C          OF A GLOBAL SPECTRAL FILE.                                           E3
C                                                                               E3
CINPUT FILE...                                                                  E3
C                                                                               E3
C      SPIN  = GLOBAL SPECTRAL COEFF.                                           E3
C                                                                               E3
COUTPUT FILE...                                                                 E3
C                                                                               E3
C      SPOUT = SPECTRAL LAPLACIAN OF SPIN.                                      E3
C---------------------------------------------------------------------------- 
C 
      use diag_sizes, only : SIZES_LA,
     &                       SIZES_LMTP1,
     &                       SIZES_NWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      integer, parameter :: 
     & MAXX = 2*(SIZES_LA+SIZES_LMTP1)*SIZES_NWORDIO 

      COMPLEX F 
      COMMON/BLANCK/F(SIZES_LA+SIZES_LMTP1)
C 
      LOGICAL OK
      DIMENSION LSR(2,SIZES_LMTP1+1)
      COMMON/ICOM/IBUF(8),IDAT(MAXX)
C-----------------------------------------------------------------------
      NFF=3 
      CALL JCLPNT(NFF,1,2,6)
      NRECS=0 
      REWIND 1
      REWIND 2
      A=6370000.E0
      ASQ=A*A 
  100 CALL GETFLD2(1,F,NC4TO8("SPEC"),-1,-1,-1,IBUF,MAXX,OK)
      IF(.NOT.OK)THEN 
        IF(NRECS.EQ.0) CALL                        XIT('SPDLSQ',-1) 
        WRITE(6,6020) NRECS 
        CALL                                       XIT('SPDLSQ',0)
      ENDIF 
      IF(NRECS.EQ.0) CALL PRTLAB (IBUF)
      CALL DIMGT(LSR,LA,LR,LM,KTR,IBUF(7))
      DO 200 M=1,LM 
      NN=LSR(1,M+1)-LSR(1,M)
      DO 200 J=1,NN 
      N=M+J-2 
200   F(LSR(1,M)-1+J)=-N*(N+1)*F(LSR(1,M)-1+J)/ASQ
      IBUF(3)=NC4TO8("DLSQ")
      CALL PUTFLD2(2,F,IBUF,MAXX)
      IF(NRECS.EQ.0) CALL PRTLAB (IBUF)
      NRECS=NRECS+1 
      GO TO 100 
C-----------------------------------------------------------------------
6020  FORMAT('0SPDLSQ COMPUTED ',I5,' RECORDS')
      END
