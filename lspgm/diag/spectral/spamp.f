      PROGRAM SPAMP 
C     PROGRAM SPAMP (X,       Y,       OUTPUT,                          )       E2
C    1         TAPE1=X, TAPE2=Y, TAPE6=OUTPUT)
C     ----------------------------------------                                  E2
C                                                                               E2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       E2
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                
C     MAR   /85 - G.J.BOER                                                      
C                                                                               E2
CSPAMP   - PRODUCES AMPLITUDE OF COMPLEX ARRAY                          1  1    E1
C                                                                               E3
CAUTHOR  - G.J. BOER                                                            E3
C                                                                               E3
CPURPOSE - FILE ARITHMETIC PROGRAM Y = SQROOT( X * CONJG(X) ),                  E3
C          SO THAT Y IS THE COMPLEX AMPLITUDE OF X.                             E3
C          NOTE - X HAS TO BE COMPLEX.                                          E3
C                                                                               E3
CINPUT FILE...                                                                  E3
C                                                                               E3
C      X = ANY COMPLEX ARRAY (MAX "R" COMPLEX WORDS)                            E3
C                                                                               E3
COUTPUT FILE...                                                                 E3
C                                                                               E3
C      Y = COMPLEX AMPLITUDE OF FILE X                                          E3
C---------------------------------------------------------------------------- 
C 
      use diag_sizes, only : SIZES_LA,
     &                       SIZES_LAT,
     &                       SIZES_LMTP1,
     &                       SIZES_NWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      integer, parameter :: 
     & MAXC=MAX((SIZES_LMTP1+1)*SIZES_LAT,(SIZES_LA+SIZES_LMTP1))
      integer, parameter ::
     & MAXX = 2*MAXC*SIZES_NWORDIO ! defined as a max buffer size for I/O 

      COMPLEX A 
      COMMON/BLANCK/A(MAXC) 
  
      LOGICAL OK,SPEC 
      COMMON/ICOM/IBUF(8),IDAT(MAXX) 
C-----------------------------------------------------------------------
      NFF=3 
      CALL JCLPNT(NFF,1,2,6)
      REWIND 1
      REWIND 2
  
C     * READ THE NEXT FIELD.
  
      NR=0
  140 CALL GETFLD2(1,A, -1 ,0,0,0,IBUF,MAXX,OK)
      IF(.NOT.OK)THEN 
        IF(NR.EQ.0) CALL                           XIT(' SPAMP',-1) 
        WRITE(6,6020) NR
        CALL                                       XIT(' SPAMP',0)
      ENDIF 
      IF(NR.EQ.0) WRITE(6,6030) IBUF
  
C     * MAKE SURE THAT THE FIELD IS COMPLEX.
  
      KIND=IBUF(1)
      SPEC=(KIND.EQ.NC4TO8("SPEC") .OR. KIND.EQ.NC4TO8("FOUR"))
      IF(.NOT.SPEC) CALL                           XIT(' SPAMP',-2) 
  
C     * COMPUTE THE AMPLITUDE.
  
      NWDS=IBUF(5)*IBUF(6)
      DO 200 I=1,NWDS 
  200 A(I)=SQRT( A(I)*CONJG(A(I)) ) 
  
C     * SAVE THE RESULT.
  
      CALL PUTFLD2(2,A,IBUF,MAXX)
      IF(NR.EQ.0) WRITE(6,6030) IBUF
      NR=NR+1 
      GO TO 140 
C-----------------------------------------------------------------------
 6020 FORMAT('0 SPAMP READ',I6,' RECORDS')
 6030 FORMAT(' ',A4,I10,2X,A4,I10,4I6)
      END
