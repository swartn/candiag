      PROGRAM ZXTRIG
C     PROGRAM ZXTRIG (ZXIN,       ZXOUT,       INPUT,       OUTPUT,     )       F2
C    1          TAPE1=ZXIN, TAPE2=ZXOUT, TAPE5=INPUT, TAPE6=OUTPUT) 
C     -------------------------------------------------------------             F2
C                                                                               F2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       F2
C     JUL 13/92 - E. CHAN   (DIMENSION SELECTED VARIABLES AS REAL*8)            
C     JAN 29/92 - E. CHAN   (CONVERT HOLLERITH LITERALS TO ASCII)               
C     JUN 09/89 - F.MAJAESS (RESTRICT POWER VALUE TO AN INTEGER)                
C     AUG 05/88 - F.MAJAESS (ALLOW READING REAL POWER VALUE)
C     MAY 14/83 - R.LAPRISE.
C     MAY 25/81 - J.D.HENDERSON 
C                                                                               F2
CZXTRIG  - CROSS-SECTION MULTIPLIED BY  CONST*TRIG(LAT)**N              1  1 C GF1
C                                                                               F3
CAUTHOR  - J.D.HENDERSON                                                        F3
C                                                                               F3
CPURPOSE - MULTIPLIES CROSS-SECTIONS IN ZXIN FILE BY CONST*TRIG(LAT)**N         F3
C          AND PUTS THE RESULT ON FILE ZXOUT.                                   F3
C          TRIG ABOVE CAN BE SIN,COS OR TAN.                                    F3
C                                                                               F3
CINPUT FILE...                                                                  F3
C                                                                               F3
C      ZXIN = CROSS-SECTIONS                                                    F3
C             (MAXIMUM LATITUDES IS $BJ$).                                      F3
C                                                                               F3
COUTPUT FILE...                                                                 F3
C                                                                               F3
C      ZXOUT = CROSS-SECTIONS MULTIPLIED AS REQUESTED                           F3
C 
CINPUT PARAMETERS...
C                                                                               F5
C      CONST = MULTIPLYING CONSTANT                                             F5
C      KTR   = 4H SIN, 4H COS, 4H TAN                                           F5
C      N     = POWER TO WHICH TRIG FUNCTION IS RAISED                           F5
C                                                                               F5
CEXAMPLE OF INPUT CARD...                                                       F5
C                                                                               F5
C*  ZXTRIG      1.E0  COS    2                                                  F5
C-------------------------------------------------------------------------------
C 
      use diag_sizes, only : SIZES_BLAT,
     &                       SIZES_BLATxNWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      integer, parameter :: MAXX = SIZES_BLATxNWORDIO
      COMMON/BLANCK/XR(SIZES_BLAT)
C 
      LOGICAL OK
      REAL TRIG(SIZES_BLAT) 
      REAL*8 SL(SIZES_BLAT),CL(SIZES_BLAT),WL(SIZES_BLAT),
     & WOSSL(SIZES_BLAT),RAD(SIZES_BLAT)
      CHARACTER*3 KTR
      COMMON/ICOM/IBUF(8),IDAT(MAXX)
C---------------------------------------------------------------------
      NFF=4 
      CALL JCLPNT(NFF,1,2,5,6)
      REWIND 1
      REWIND 2
C 
C     * READ CONST, KTR, N FROM A CARD. 
C     * KTR IS THE KIND OF TRIG FUNCTION ('SIN','COS','TAN'). 
C 
      READ(5,5010,END=902) CONST,KTR,N                                          F4
      IF(KTR.EQ.'SIN') GO TO 120
      IF(KTR.EQ.'COS') GO TO 120
      IF(KTR.EQ.'TAN') GO TO 120
      WRITE(6,6901) KTR 
      CALL                                         XIT('ZXTRIG',-1) 
  120 CONTINUE
      WRITE(6,6005)CONST,KTR,N
C 
      NR=0
  150 CALL GETFLD2(1,XR,NC4TO8("ZONL"),-1,-1,-1,IBUF,MAXX,OK)
      IF(.NOT.OK)THEN 
        IF(NR.EQ.0)THEN 
          WRITE(6,6902) 
          CALL                                     XIT('ZXTRIG',-2) 
        ELSE
          WRITE(6,6010) NR
          CALL                                     XIT('ZXTRIG',0)
        ENDIF 
      ENDIF 
C - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
C     * FIRST TIME ONLY...
C     * GAUSSG COMPUTES THE VALUE OF THE GAUSSIAN LATITUDES AND THEIR 
C     * SINES AND COSINES. TRIGL MAKES THEM GLOBAL (S TO N).
C 
      IF(NR.GT.0) GO TO 350 
      NLAT=IBUF(5)
      ILATH=NLAT/2
      CALL GAUSSG(ILATH,SL,WL,CL,RAD,WOSSL) 
      CALL  TRIGL(ILATH,SL,WL,CL,RAD,WOSSL) 
C 
C     * SET TRIG TO THE REQUESTED TRIG FUNCTION.
C 
      DO 320 J=1,NLAT 
      IF(KTR.EQ.'SIN') TRIG(J)= SL(J)**N
      IF(KTR.EQ.'COS') TRIG(J)= CL(J)**N
      IF(KTR.EQ.'TAN') TRIG(J)=( SL(J)/ CL(J))**N 
  320 CONTINUE
C - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
C     * MULTIPLY THIS LEVEL AND SAVE ON FILE 2. 
C 
  350 IF(IBUF(5).NE.NLAT) CALL                     XIT('ZXTRIG',-3) 
      DO 410 J=1,NLAT 
  410 XR(J)=CONST*TRIG(J)*XR(J) 
C 
      CALL PUTFLD2(2,XR,IBUF,MAXX) 
      IF(NR.EQ.0) WRITE(6,6025) IBUF
      NR=NR+1 
      GO TO 150 
C 
C     * E.O.F. ON INPUT.
C 
  902 CALL                                         XIT('ZXTRIG',-4) 
C---------------------------------------------------------------------
 5010 FORMAT(10X,E10.0,2X,A3,I5)                                                F4
 6005 FORMAT('0ZXTRIG CONST,KTR,N = ',1PE12.4,4X,A3,I5)
 6010 FORMAT(I6,' RECORDS READ')
 6025 FORMAT(' ',A4,I10,1X,A4,I10,4I6)
 6901 FORMAT('0KTR MUST BE SIN,COS OR TAN.  KTR=',A3)
 6902 FORMAT('0 **** EMPTY INPUT FILE ****')
      END
