      PROGRAM ZLATINT
C     PROGRAM ZLATINT (ZXIN,          ZXOUT,       OUTPUT,              )       F2
C    1           TAPE1=ZXIN,    TAPE2=ZXOUT, TAPE6=OUTPUT)
C     ----------------------------------------------------                      F2
C                                                                               F2
C     MAR 27/13 - S.KHARIN (ADD "GAUSSIAN WEIGHTS" OPTION)                      F2
C     FEB/1995  - B.DENIS                                                       
C                                                                               F2
CZLATINT - COMPUTES LATITUDINAL INTEGRATION  OF CROSS-SECTIONS          1  1 C GF1
C                                                                               F3
CAUTHOR  - B.DENIS                                                              F3
C                                                                               F3
CPURPOSE - READS CROSS-SECTIONS FROM FILE ZXIN, INTEGRATES IN LATITUDE          F3
C          AND PUTS THE RESULTS ON FILE ZXOUT. EACH LATITUDINAL VALUE           F3
C          IN ZXOUT IS THE RESULT OF A SOUTHWARD OR NORTHWARD                   F3
C          INTEGRATION FOLLOWING:                                               F3
C          Y(I)=INTEGRAL[X(LAT).FCT(LAT).DLAT)] FROM I= NP/SP TO LAT.           F3
C          FCT(LAT) IS A MULTIPLYING FUNCTION: COS(LAT), SIN(LAT), ONE, OR      F3
C          GAUSSIAN WEIGHTS.                                                    F3
C                                                                               F3
CINPUT FILE...                                                                  F3
C                                                                               F3
C      ZXIN  = ZONAL AVERAGES.                                                  F3
C                                                                               F3
COUTPUT FILE...                                                                 F3
C                                                                               F3
C      ZXOUT = LATITUDINALLY INTEGRATED ZONAL AVERAGES.                         F3
C
CINPUT PARAMETERS...                                                            F5
C                                                                               F5
C      OPTFCT= OPTION TO SET THE MULTIPLYING FUNCTION:'COS','SIN','ONE' OR 'GAU'F5
C      OPTPOL= OPTION TO SET FROM WHICH POLE TO START:'NP' OR 'SP'.             F5
C                                                                               F5
C      N.B:    THE DEFAULT (BLANK CHARACTERS IN THE INPUT CARD) IS:             F5
C              OPTFCT='COS', OPTPOL='NP'                                        F5
C                                                                               F5
CEXAMPLE OF INPUT CARD...                                                       F5
C                                                                               F5
C*ZLATINT   COS   NP                                                            F5
C------------------------------------------------------------------------------
C
C
      use diag_sizes, only : SIZES_BLAT,
     &                       SIZES_BLATxNWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)


      LOGICAL OK
      REAL ZX(SIZES_BLAT),FINTL(SIZES_BLAT)
      REAL*8 SL(SIZES_BLAT),CL(SIZES_BLAT),WL(SIZES_BLAT),
     & WOSSL(SIZES_BLAT),RAD(SIZES_BLAT),FNCT(SIZES_BLAT) 
      CHARACTER OPTFCT*3,OPTPOL*2
      COMMON /ICOM/ IBUF(8),IDAT(SIZES_BLATxNWORDIO)
      DATA MAXX/SIZES_BLATxNWORDIO/
C---------------------------------------------------------------------
      NFF=4
      CALL JCLPNT(NFF,1,2,5,6)
      REWIND 1
      REWIND 2
C
C     * READ  OPTFCT AND OPTPOL FROM INPUT CARD.
C
      READ(5,5010,END=902) OPTFCT, OPTPOL                                       F4
      WRITE(6,6000) OPTFCT
      WRITE(6,6010) OPTPOL
C
C     * SET THE DEFAULT OPTIONS IF REQUIRED
C
      IF(OPTFCT.EQ.'   ') OPTFCT='COS'
      IF(OPTPOL.EQ.'  ' ) OPTPOL='NP'
C
C     * CHECK FOR ERRORS/BAD OPTIONS
C
      IF(OPTFCT.NE.'COS'.AND.OPTFCT.NE.'SIN'.AND.
     +   OPTFCT.NE.'ONE'.AND.OPTFCT.NE.'GAU') CALL XIT('ZLATINT',-1)
      IF(OPTPOL.NE.'NP'.AND.OPTPOL.NE.'SP')   CALL XIT('ZLATINT',-2)
C
C     * GET THE NEXT CROSS-SECTION. STOP IF THE FILE IS EMPTY.
C
      NR=0
 110  CALL GETFLD2(1,ZX,NC4TO8("ZONL"),-1,-1,-1,IBUF,MAXX,OK)
      IF(.NOT.OK)THEN
        WRITE(6,6030) NR
        IF(NR.EQ.0) CALL                           XIT('ZLATINT',-3)
        CALL                                       XIT('ZLATINT', 0)
      ENDIF
      NLAT=IBUF(5)
C
C     * GAUSSG COMPUTES THE VALUE OF THE GAUSSIAN LATITUDES AND THEIR
C     * SINES AND COSINES. TRIGL MAKES THEM GLOBAL (S TO N).
C
      ILATH=NLAT/2
      CALL GAUSSG(ILATH,SL,WL,CL,RAD,WOSSL)
      CALL  TRIGL(ILATH,SL,WL,CL,RAD,WOSSL)
C
C     * CHOOSE MULTIPLYING FUNCTION
C
      IF(OPTFCT.EQ.'COS') THEN
        DO I=1,NLAT
          FNCT(I)=CL(I)
        ENDDO
      ELSE IF(OPTFCT.EQ.'SIN') THEN
        DO I=1,NLAT
          FNCT(I)=SL(I)
        ENDDO
      ELSE
        DO I=1,NLAT
          FNCT(I)=1.0
        ENDDO
      ENDIF
C
C     * INTEGRATE FROM THE MOST SOUTHERN OR NORTHERN * LATITUDE TO THE
C     * CURRENT ONE.
C
      IF(OPTPOL.EQ.'SP')THEN
C
C       * SOUTH POLE TO NORTH POLE
C
        IF(OPTFCT.EQ.'GAU') THEN
C
C         * USE GAUSSIAN WEIGHTS
C
          FINTL(1)=WL(1)*ZX(1)
          DO J=2,NLAT
            FINTL(J)=FINTL(J-1)+WL(J)*ZX(J)
          ENDDO
        ELSE
C
C         * USE SPECIFIED FUNCTION (COS, SIN OR ONE)
C
          FINTL(1)=0.0
          DO J=2,NLAT
            FINTL(J)=(ZX(J)*FNCT(J)+ZX(J-1)*FNCT(J-1))
     1           *(RAD(J)-RAD(J-1))/2.+ FINTL(J-1)
          ENDDO
        ENDIF
      ELSE
C
C       * NORTH POLE TO SOUTH POLE (INTEGRAL SIGN IS REVERSED)
C
        IF(OPTFCT.EQ.'GAU') THEN
C
C         * USE GAUSSIAN WEIGHTS
C
          FINTL(NLAT)=-WL(NLAT)*ZX(NLAT)
          DO J=NLAT-1,1,-1
            FINTL(J)=FINTL(J+1)-WL(J)*ZX(J)
          ENDDO
        ELSE
C
C         * USE SPECIFIED FUNCTION (COS, SIN OR ONE)
C
          FINTL(NLAT)=0.0
          DO J=NLAT-1,1,-1
            FINTL(J)=(ZX(J)*FNCT(J)+ZX(J+1)*FNCT(J+1))
     1           *(RAD(J)-RAD(J+1))/2.+ FINTL(J+1)
          ENDDO
        ENDIF
      ENDIF
C
      CALL PUTFLD2(2,FINTL,IBUF,MAXX)
      CALL PRTLAB(IBUF)
      NR=NR+1
      GO TO 110
C
  902 CALL                                         XIT('ZLATINT',-4)
C----------------------------------------------------------------------
 5010 FORMAT(10X,2X,A3,3X,A2)                                                   F4
 6000 FORMAT('0ZLATINT -MULTIPLYING FNCT SET TO  :',A3)
 6010 FORMAT('0ZLATINT -INTEGRATION STARTING FROM:',A2)
 6030 FORMAT('   ZLATINT READ',I5, '  RECORDS')
      END
