      PROGRAM ZXMAV 
C     PROGRAM ZXMAV (ZXIN,       ZXOUT,       OUTPUT,                   )       F2
C    1         TAPE1=ZXIN, TAPE2=ZXOUT, TAPE6=OUTPUT) 
C     -----------------------------------------------                           F2
C                                                                               F2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       F2
C     JUL 13/92 - E. CHAN  (DIMENSION SELECTED VARIABLES AS REAL*8)             
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                
C     MAY 14/83 - R.LAPRISE.                                                    
C     NOV 07/80 - J.D.HENDERSON 
C                                                                               F2
CZXMAV   - COMPUTES MERIDIONAL AVERAGE OF ONE CROSS-SECTION             1  1   GF1
C                                                                               F3
CAUTHOR  - J.D.HENDERSON                                                        F3
C                                                                               F3
CPURPOSE - READS ONE CROSS-SECTION FROM FILE ZXIN, INTEGRATES IN LATITUDE       F3
C          AND PUTS THE RESULTS ON FILE ZXOUT.                                  F3
C          NOTE - AT INPUT THE CROSS-SECTION IS ORDERED FROM S TO N AND         F3
C                 TOP TO BOTTOM.                                                F3
C                 MAXIMUM LATITUDES IS $BJ$, MAXIMUM LEVELS IS $L$.             F3
C                                                                               F3
CINPUT FILE...                                                                  F3
C                                                                               F3
C      ZXIN  = ONE CROSS-SECTION ON PRESSURE LEVELS                             F3
C                                                                               F3
COUTPUT FILE...                                                                 F3
C                                                                               F3
C      ZXOUT = ONE CROSS-SECTION WHERE EACH LEVEL IS SET TO THE MERIDIONAL      F3
C              AVERAGE OF THE CORRESPONDING LEVEL OF ZXIN.                      F3
C------------------------------------------------------------------------------ 
C 
      use diag_sizes, only : SIZES_BLAT,
     &                       SIZES_BLATxNWORDIO,
     &                       SIZES_MAXLEV

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      COMMON/XS/XA(SIZES_BLAT,SIZES_MAXLEV),F(SIZES_BLAT)
C 
      LOGICAL OK
      REAL FL(SIZES_MAXLEV)
      REAL*8 SL(SIZES_BLAT),CL(SIZES_BLAT),
     & WL(SIZES_BLAT),WOSSL(SIZES_BLAT),
     & RAD(SIZES_BLAT)
      INTEGER LEV(SIZES_MAXLEV)
      COMMON/ICOM/IBUF(8),IDAT(SIZES_BLATxNWORDIO)
      DATA MAXX/SIZES_BLATxNWORDIO/, 
     & MAXL/SIZES_MAXLEV/, 
     & MAXJ/SIZES_BLAT/ 
C---------------------------------------------------------------------
      NFF=3 
      CALL JCLPNT(NFF,1,2,6)
      PIH=3.14159265E0/2.E0 
      REWIND 1
      REWIND 2
C 
C     * GET THE CROSS-SECTION. STOP IF THE FILE IS EMPTY. 
C     * LEV WILL CONTAIN THE PRESSURE LEVEL VALUES IN MILLIBARS.
C 
      CALL GETZX2(1,XA,MAXJ,LEV,NLEV,IBUF,MAXX,OK) 
      IF(.NOT.OK) CALL                             XIT('ZXMAV',-1)
      IF((NLEV.LT.1).OR.(NLEV.GT.MAXL)) CALL       XIT('ZXMAV',-2)
      NLAT=IBUF(5)
C 
C     * GAUSSG COMPUTES THE VALUE OF THE GAUSSIAN LATITUDES AND THEIR 
C     * SINES AND COSINES. TRIGL MAKES THEM GLOBAL (S TO N).
C 
      ILATH=NLAT/2
      CALL GAUSSG(ILATH,SL,WL,CL,RAD,WOSSL) 
      CALL  TRIGL(ILATH,SL,WL,CL,RAD,WOSSL) 
C - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
C     * INTEGRATE EACH LEVEL IN LATITUDE. PLOT ON A GRAPH.
C 
      DO 280 L=1,NLEV 
      DO 240 J=1,NLAT 
  240 F(J)=XA(J,L)* CL(J) 
      VAL=F(1)*(RAD(1)-(-PIH))
      VAL2= CL(1)*(RAD(1)-(-PIH)) 
      DO 250 J=2,NLAT 
      VAL2=VAL2+0.5E0*( CL(J)+ CL(J-1))*(RAD(J)-RAD(J-1))
  250 VAL=VAL+0.5E0*(F(J)+F(J-1))*(RAD(J)-RAD(J-1))
      VAL2=VAL2+ CL(NLAT)*(PIH-RAD(NLAT)) 
      VAL=VAL+F(NLAT)*(PIH-RAD(NLAT)) 
      FL(L)=VAL/VAL2
      WRITE(6,6028) LEV(L),FL(L)
  280 CONTINUE
C 
C     * PUT THE LATITUDE INTEGRALS ON FILE ZXOUT. 
C 
      DO 480 L=1,NLEV 
      DO 470 J=1,NLAT 
  470 F(J)=FL(L)
      IBUF(4)=LEV(L)
      CALL PUTFLD2(2,F,IBUF,MAXX)
      WRITE(6,6025) IBUF
  480 CONTINUE
C 
      CALL                                         XIT('ZXMAV',0) 
C---------------------------------------------------------------------
 5010 FORMAT(10X,3E10.0)
 6025 FORMAT(' ',A4,I10,1X,A4,I10,4I6)
 6028 FORMAT(' LATITUDE INTEGRAL AT LEVEL',I5,1PE15.6)
      END
