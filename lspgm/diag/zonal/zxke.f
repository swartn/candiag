      PROGRAM ZXKE
C     PROGRAM ZXKE (ZXU,       ZXV,       ZXKE,       OUTPUT,           )       F2
C    1        TAPE1=ZXU, TAPE2=ZXV, TAPE3=ZXKE, TAPE6=OUTPUT) 
C     -------------------------------------------------------                   F2
C                                                                               F2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       F2
C     JAN 29/92 - E. CHAN   (CONVERT HOLLERITH LITERALS TO ASCII)               
C     AUG 19/88 - F.MAJAESS (REPLACE GETDATA, PUTDATA BY GETFLD2, PUTFLD2)    
C     JUL 31/78 - J.D.HENDERSON 
C                                                                               F2
CZXKE    - COMPUTES KINETIC ENERGY FROM U AND V ZONAL CROSS-SECTIONS    2  1    F1
C                                                                               F3
CAUTHOR  - J.D.HENDERSON                                                        F3
C                                                                               F3
CPURPOSE - COMPUTES KINETIC ENERGY FROM U AND V ZONAL CROSS-SECTIONS            F3
C          THE COMPUTATION IS DONE ONE LEVEL AT A TIME SO THERE IS NO           F3
C          MAXIMUM NUMBER OF LEVELS.                                            F3
C                                                                               F3
CINPUT FILES...                                                                 F3
C                                                                               F3
C      ZXU  = U ZONAL CROSS-SECTION FILE                                        F3
C      ZXV  = V ZONAL CROSS-SECTION FILE                                        F3
C                                                                               F3
COUTPUT FILE...                                                                 F3
C                                                                               F3
C      ZXKE = THE RESULTANT KINETIC ENERGY                                      F3
C-----------------------------------------------------------------------------
C 
      use diag_sizes, only : SIZES_BLAT,
     &                       SIZES_BLATxNWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      LOGICAL OK
      INTEGER JBUF(8) 
      REAL U(SIZES_BLAT),V(SIZES_BLAT)
      COMMON/ICOM/IBUF(8),IDAT(SIZES_BLATxNWORDIO)
      DATA MAXX/SIZES_BLATxNWORDIO/
C---------------------------------------------------------------------
      NFF=4 
      CALL JCLPNT(NFF,1,2,3,6)
      REWIND 1
      REWIND 2
      REWIND 3
C 
C     * GET ONE LEVEL AT A TIME AND PERFORM THE COMPUTATION.
C 
      N=0 
  140 CALL GETFLD2(1,U,NC4TO8("ZONL"),-1,-1,-1,IBUF,MAXX,OK)
      IF(.NOT.OK)THEN 
        WRITE(6,6010) N 
        IF(N.EQ.0) CALL                            XIT('ZXKE',-1) 
        CALL                                       XIT('ZXKE',0)
      ENDIF 
      WRITE(6,6025) IBUF
      DO 150 J=1,8
        JBUF(J)=IBUF(J) 
  150 CONTINUE
      CALL GETFLD2(2,V,NC4TO8("ZONL"),-1,-1,-1,IBUF,MAXX,OK)
      IF(.NOT.OK)THEN 
        WRITE(6,6015) 
        CALL                                       XIT('ZXKE',-2) 
      ENDIF 
      WRITE(6,6025) IBUF
      CALL CMPLBL(0,IBUF,0,JBUF,OK) 
      IF(.NOT.OK) CALL                             XIT('ZXKE',-3) 
C 
      NWDS=IBUF(5)
      DO 210 I=1,NWDS 
        U(I)=0.5E0*(U(I)**2+V(I)**2) 
  210 CONTINUE
C 
C     * PUT THE RESULT ON FILE 3. 
C 
      IBUF(3)=NC4TO8("  KE")
      CALL PUTFLD2(3,U,IBUF,MAXX)
      WRITE(6,6025) IBUF
      N=N+1 
      GO TO 140 
C 
C---------------------------------------------------------------------
 6010 FORMAT('0ZXKE  ON ',I4,' LEVELS')
 6015 FORMAT('0...FILES DO NOT ALIGN')
 6025 FORMAT(' ',A4,I10,1X,A4,I10,4I6)
      END
