      PROGRAM TXT2BIN
C     PROGRAM TXT2BIN(CHAR,        BIN,       OUTPUT,                   )       B2
C    1         TAPE11=CHAR, TAPE12=BIN, TAPE6=OUTPUT)
C     -----------------------------------------------                           B2
C                                                                               B2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       B2
C     DEC 18/02 - F.MAJAESS (REVISE TO SUPPORT UP TO 128-CHARACTERS PER LINE)   B2
C     MAR 05/02 - F.MAJAESS                                                   
C                                                                               B2
CTXT2BIN - CONVERTS TEXT FILE INTO STANDARD "CHAR" CCRN BINARY RECORDS  1  1    B1
C                                                                               B3
CAUTHOR  - F.MAJAESS                                                            B3
C                                                                               B3
CPURPOSE - CONVERTS THE INPUT TEXT DATA IN "CHAR" FILE INTO STANDARD "CHAR"     B3
C          KIND CCRN BINARY RECORD(S) FORMAT. ONE BINARY RECORD PER SET WHERE   B3
C          THE SETS ARE SEPARATED BY LINES HAVING " +" IN THE FIRST 2 COLUMNS.  B3
C          NOTE: ONLY UP TO THE FIRST 128 CHARACTERS PER LINE ARE RECOGNIZED IN B3
C                "CHAR" TEXT FILE.                                              B3
C                THE FIELD NAME TO USE IN THE 8-WORD LABEL (4 CHARACTERS LONG;  B3
C                COLUMNS 3-6),IS READ FROM THE "SEPARATOR LINE" STARTING WITH   B3
C                " +" IN THE FIRST 2 COLUMNS, ANYTHING ELSE ON SUCH LINE IS     B3
C                IGNORED. "NAME" SPECIFIED IS CONVERTED INTO UPPER CASE.        B3
C                BLANK/EMPTY NAME FIELD DEFAULTS TO "TEXT". DUPLICATE FIELD     B3
C                NAME IS NOT ALLOWED.                                           B3
C                THE MAXIMUM NUMBER OF LINES THAT CAN BE HANDLED PER SET IS     B3
C                1000 128-CHARACTER LINES EXCLUDING THE FIELD NAME LINE.        B3
C                LINES READ WHICH ARE SHORTER THAN 128 CHARACTERS LONG ARE      B3
C                PADDED WITH BLANKS TO MAKE THEM 128 CHARACTERS LONG BEFORE     B3
C                WRITING THE CCRN BINARY RECORD. THE ONLY EXCEPTION TO THIS     B3
C                RULE IS THE CASE WHERE, THE SET CONSISTS OF A SINGLE LINE WITH B3
C                LESS THAN 128 CHARACTERS LONG, IN WHICH CASE A PARTIAL LINE    B3
C                WRITING IS ALLOWED WITH STRING PADDED IF NECESSARY TO MAKE IT  B3
C                A MULTIPLE OF 8-BYTE WORD(S) IN LENGTH.                        B3
C                                                                               B3
CINPUT FILE...                                                                  B3
C                                                                               B3
C      CHAR = TEXT FILE.                                                        B3
C             UP TO 128 CHARACTERS/LINE AND MAXIMUM 1000 LINES PER SET.         B3
C                                                                               B3
COUTPUT FILE...                                                                 B3
C                                                                               B3
C      BIN = BINARY STANDARD CCRN FILE.                                         B3
C--------------------------------------------------------------------------
C
C     * MXNMLST = DIMENSION OF ARRAY USED TO CHECK FOR DUPLICATES
C     *           IN FIELD NAME USED.
C     * NCBUF   = MAXIMUM NUMBER OF 128-CHARACTER LINES PER SET.
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      PARAMETER (MXNMLST=300,NCBUF=1000,NDAT=1000*16)
      CHARACTER*128 ALINE,BLINE,CBUF(NCBUF)
      CHARACTER*8 BCLINE(16),BLANK,FLINE(16)
      CHARACTER*1 ACHAR
      CHARACTER*4 CNAME
      INTEGER     NAME,MACHINE,INTSIZE,NAMEA(MXNMLST)
      EQUIVALENCE (BLINE,BCLINE)
      EQUIVALENCE (IDAT,CBUF),(CBUF,FLINE)
      EQUIVALENCE (NAME,CNAME)
      COMMON /ICOM/    IBUF(8),IDAT(NDAT)
      COMMON /MACHTYP/ MACHINE,INTSIZE

C     * INITIALIZE THE BLANK LINE.

      DATA BCLINE/16*'        '/,BLANK/'        '/
C-----------------------------------------------------------------------

     
      NFF=3
      CALL JCLPNT(NFF,-11,12,6)
     
      REWIND 11
      REWIND 12

C
C     * SET "MAXX" VALUE BASED ON "NDAT" AND THE "INTSIZE" VALUE 
C     * COMPUTED IN "MACHTYP" COMMON BLOCK VIA THE CALL TO "JCLPNT".
C
      MAXX=NDAT*INTSIZE
C
C     * INITIALIZE COUNTER USED FOR DUPLICATE FIELD NAME CHECK.
C
      NMC=0
C
C     * READ THE FIELD NAME TO USE IN THE 8-WORD LABEL.
C
      NSET=0
  10  READ(11,1010,END=199) ACHAR,NAME
      IF(ACHAR.NE.'+') THEN
       WRITE(6,6020)
       CALL                                        XIT('TXT2BIN',-1)
      ENDIF
      IF(NAME.EQ.NC4TO8("    ")) THEN
        NAME=NC4TO8("TEXT")
      ELSE
        CALL LWRTUPR(CNAME,4)
      ENDIF
      WRITE(6,6030) ACHAR,NAME
      ACHAR=' '
C     
C     * ENSURE NO DUPLICATES IN FIELD NAME...
C
      IF (NMC.EQ.0) THEN
       NMC=1
       NAMEA(NMC)=NAME
      ELSE
       DO I=1,NMC
         IF(NAME.EQ.NAMEA(I)) THEN
           WRITE(6,6015) NAME
           CALL                                    XIT('TXT2BIN',-2)
         ENDIF
       ENDDO
       NMC=NMC+1
       NAMEA(NMC)=NAME
      ENDIF
C
C     * INITIALIZE "CBUF" TO BLANK ...
C
      DO NLINE=1,NCBUF
       CBUF(NLINE)=BLINE
      ENDDO
C
C     * ACCUMULATE THE REST OF THE TEXT LINES IN "CBUF".
C     * ABORT IF THE MAXIMUM NUMBER OF LINES EXCEED WHAT
C     * CAN BE ACCUMULATED IN "CBUF".
C     * USE "ACHAR" VALUE TO DIFFERENTIATE BETWEEN EOF AND
C     * BREAKING FROM THE LOOP TO PROCESS A SET.
C
      NLINE=0
 100  READ(11,1000,END=199) CBUF(NLINE+1) 
      IF(CBUF(NLINE+1)(2:2).EQ.'+') THEN
        ACHAR='+'
        BACKSPACE 11
        GO TO 199
      ENDIF
      WRITE(6,1000) CBUF(NLINE+1)
      NLINE=NLINE+1
      IF ( NLINE+1.GT.NCBUF) THEN
       READ(11,1000,END=199) BLINE
       IF(BLINE(2:2).EQ.'+') THEN
         ACHAR='+'
         BACKSPACE 11 
         GO TO 199
       ELSE
        WRITE(6,6010) NCBUF
        CALL                                       XIT('TXT2BIN',-3)
       ENDIF
      ELSE
       GO TO 100
      ENDIF

C
C     * E.O.F. ON FILE CHAR OR END OF A SET.
C
 199  IF(NLINE.EQ.0) THEN

       IF( ACHAR.EQ.'+') THEN

C       * "CHAR" FILE IS EMPTY OR JUST HAS THE FIELD "NAME" LINE
C       * FOR A SET.
      
        CALL                                       XIT('TXT2BIN',-4)

       ELSE

C       * EOF REACHED AND NO MORE SETS TO PROCESS.
      
        CALL                                       XIT('TXT2BIN',0)

       ENDIF

      ELSE
       
       IF (NLINE.EQ.1) THEN

C       * ALLOW FOR PARTIAL LINE TO BE WRITTEN OUT IN THE 
C       * SINGLE LINE CASE.

        NWDPLN=16
        DO I=16,1,-1
         NWDPLN=I
         IF(FLINE(I).NE.BLANK) GO TO 200
        ENDDO

       ELSE

C       * ENFORCE FULL 128 CHARACTERS PER LINE IN THE MULTIPLE
C       * LINES CASE.

        NWDPLN=16

       ENDIF

C      * SETUP THE 8-WORD LABEL AND WRITE OUT THE SET LINES 
C      * IN CCRN BINARY RECORD FORMAT.

  200  CALL SETLAB(IBUF,NC4TO8("CHAR"),0,NAME,0,NWDPLN,NLINE,0,1)
       CALL PUTFLD2(12,CBUF,IBUF,MAXX)
       CALL PRTLAB (IBUF)

       NSET=NSET+1
       IF (NLINE.EQ.1) THEN
        WRITE(6,6000) NWDPLN,NSET
       ELSE
        WRITE(6,6005) NLINE,NSET
       ENDIF
C
C      * CHECK IF MORE SETS TO PROCESS OR REACHED THE EOF.
C
       IF (ACHAR.EQ.'+' ) THEN

C       * GO BACK IF THERE ARE MORE SETS TO PROCESS ...

        GO TO 10

       ELSE

C       * OTHERWISE EXIT.

        CALL                                       XIT('TXT2BIN',0)

       ENDIF

      ENDIF
C
C-----------------------------------------------------------------------
 1000 FORMAT(A128)
 1010 FORMAT(1X,A1,A4)
 6000 FORMAT(/,'0 TXT2BIN PROCESSED ',I5,' 8-BYTE WORDS FOR ',
     +                         '"CHAR" RECORD # ',I5,'.',/)
 6005 FORMAT(/,'0 TXT2BIN PROCESSED ',I5,' LINES FOR ',
     +                         '"CHAR" RECORD # ',I5,'.',/)
 6010 FORMAT('0 MAXIMUM NUMBER OF LINES (',I5,') PER SET EXCEEDED.')
 6015 FORMAT(' DUPLICATE FIELD NAME (',A4,') IS NOT ALLOWED!')
 6020 FORMAT(/,'0 YOU NEED TO SPECIFY A SEPARATOR CARD WITH +',
     +                                 ' IN THE 2ND COLUMN.')
 6030 FORMAT(/,1X,A1,A4)
      END
