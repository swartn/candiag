      PROGRAM RMERGE
C     PROGRAM RMERGE (OUT,       IN1,...,       IN88,      OUTPUT,      )       B2
C    1          TAPE1=OUT,TAPE11=IN1,...,TAPE98=IN88,TAPE6=OUTPUT)
C     ------------------------------------------------------------              B2
C                                                                               B2
C     JAN 21/09 - S. KHARIN (FIX A BUG IN SKIPPING SUPERLABELS.                 B2
C                            INCREASE NUMBER OF FILES FROM 60 TO 88).           B2
C                                                                               B2
CRMERGE  - MERGES UP TO 88 INPUT FILES INTO ONE FILE,RECORD BY RECORD. 88  1    B1
C                                                                               B3
CAUTHOR  - S. KHARIN                                                            B3
C                                                                               B3
CPURPOSE - MERGES UP TO 88 INPUT FILES INTO ONE FILE, RECORD BY RECORD.         B3
C          SUPERLABELS/CHAR ARE IGNORED.                                        B3
C                                                                               B3
CINPUT FILES...                                                                 B3
C                                                                               B3
C      IN1,.. = INPUT FILES.                                                    B3
C                                                                               B3
COUTPUT FILE...                                                                 B3
C                                                                               B3
C      OUT    = OUTPUT FILE WITH ALL INPUT FILES MERGED TOGETHER:               B3
C               IN1(1),IN2(1),...,INN(1),IN1(2),...                             B3
C
C--------------------------------------------------------------------------
C
      use diag_sizes, only : SIZES_BLONP1xBLATxNWORDIO

      LOGICAL OK
C
      COMMON/ICOM/IBUF(8),IDAT(SIZES_BLONP1xBLATxNWORDIO)
C
      DATA LA/SIZES_BLONP1xBLATxNWORDIO/
C---------------------------------------------------------------------
      NF=90
      CALL JCLPNT(NF,1,11,12,13,14,15,16,17,18,19,20,
     1                 21,22,23,24,25,26,27,28,29,30,
     2                 31,32,33,34,35,36,37,38,39,40,
     3                 41,42,43,44,45,46,47,48,49,50,
     4                 51,52,53,54,55,56,57,58,59,60,
     5                 61,62,63,64,65,66,67,68,69,70,
     6                 71,72,73,74,75,76,77,78,79,80,
     7                 81,82,83,84,85,86,87,88,89,90,
     8                 91,92,93,94,95,96,97,98,6)
      NF=NF-2
      WRITE(6,6010) NF
      IF(NF.LT.1)CALL                              XIT('RMERGE',-1)
      REWIND 1
      DO N=1,NF
        REWIND 10+N
      ENDDO
C
      NREC=0
      NSET=0
 100  CONTINUE
      DO N=1,NF
 110    CALL RECGET(10+N,-1,-1,-1,-1, IBUF,LA,OK)
        IF (.NOT.OK) THEN
          WRITE(6,6020) NSET,NREC
C
C         * ABORT IF INPUT FILE IS SHORTER THAN THE FIRST INPUT FILE
C
          IF(NSET.EQ.0.OR.N.NE.1) CALL             XIT('RMERGE',-2)
          CALL PRTLAB(IBUF)
          CALL                                     XIT('RMERGE',0)
        ENDIF
C
C       * SKIP SUPERLABELS/CHAR
C
        IF(IBUF(1).EQ.NC4TO8("LABL").OR.
     +     IBUF(1).EQ.NC4TO8("CHAR"))GOTO 110
        NREC=NREC+1
        IF(NREC.EQ.1) CALL PRTLAB(IBUF)
C
C       * SAVE RECORD
C
        CALL RECPUT(1,IBUF)
      ENDDO
      NSET=NSET+1
      GO TO 100
C-----------------------------------------------------------------------------
 6010 FORMAT(' MERGE ',I5,' FILES INTO ONE FILE, RECORD BY RECORD.')
 6020 FORMAT(' PROCESSED RECORDS FROM INPUT FILES :',I10/
     1       '     SAVED RECORDS IN  OUTPUT FILE  :',I10)
      END
