      PROGRAM SELRANG
C     PROGRAM SELRANG (IN,      OUT1,      OUT2,      INPUT,     OUTPUT,)       B2
C    1           TAPE1=IN,TAPE2=OUT1,TAPE2=OUT2,TAPE5=INPUT,TAPE6=OUTPUT)
C     ------------------------------------------------------------------        B2
C                                                                               B2
C     DEC 11/08 - S.KHARIN (OPTIONAL OUTPUT FILE FOR COMPLEMENTARY SET)         B2
C     SEP 24/08 - M.FYFE (CORRECT A BUG WHEN FULL SEASONS ARE REQUESTED)
C     NOV 07/06 - S.KHARIN
C                                                                               B2
CSELRANG  - SELECT A SPECIFIED RANGE FROM EACH YEAR.                    1  2 C  B1
C                                                                               B3
CAUTHOR   - S.KHARIN.                                                           B3
C                                                                               B3
CPURPOSE  - SELECT A SPECIFIED RANGE FROM EACH YEAR.                            B3
C                                                                               B3
CINPUT FILE...                                                                  B3
C                                                                               B3
C      IN = FILE CONTAINING INPUT FIELDS IN CCRN FORMAT.                        B3
C                                                                               B3
COUTPUT FILE...                                                                 B3
C                                                                               B3
C     OUT1 = (OPTIONAL) SELECTED RECORDS FOR THE SPECIFIED TIME RANGE.          B3
C     OUT2 = (OPTIONAL) SELECTED RECORDS OUTSIDE THE SPECIFIED TIME RANGE.      B3
C                                                                               B3
CINPUT PARAMETERS...
C
C10X                                                                            B5
CI10  NT1 = THE FIRST TIME STEP OF THE RANGE TO BE SELECTED.                    B5
C         > 0, START WITH THE FIRST COMPLETE TIME RANGE.                        B5
C           WARNING: THE LAST YEAR RANGE MAY STILL BE INCOMPLETE.               B5
C         < 0, SELECT *ALL* TIME STEPS IN THE INTERVAL |NT1|...NT2.             B5
CI10  NT2 = THE LAST TIME STEP OF THE RANGE TO BE SELECTED.                     B5
C           IF NT2 < |NT1|, THE RANGE OVERLAPS THE YEAR BOUNDARY.               B5
CA10 DFMT = 10-CHARACTER STRING ALIGNED TO THE RIGHT TO SPECIFY TIMESTEP FORMAT.B5
C           POSSIBLE COMBINATIONS ARE:                                          B5
C           'YYYYMMDDHH', OR '  YYMMDDHH', OR '    MMDDHH' - HOURLY DATA,       B5
C           '  YYYYMMDD', OR '    YYMMDD', OR '      MMDD' - DAILY DATA,        B5
C           '    YYYYMM', OR '      YYMM', OR '        MM' - MONTHLY DATA,      B5
C           '       MON'  INFORMATION IN TIME STEP IS IGNORED AND INPUT RECORDS B5
C                         ARE ASSUMED TO BE MONTHLY MEANS STARTING FROM JAN.    B5
CI5  LSPL = 0 SKIP SUPERLABELS (DEFAULT)                                        B5
C         = 1 KEEP SUPERLABELS.                                                 B5
C                                                                               B5
CEXAMPLES OF INPUT CARD...                                                      B5
C                                                                               B5
C*SELRANG          6         8    YYYYMM                                        B5
C (SELECT JUNE-JULY-AUGUST FROM MONTHLY DATA FILE)                              B5
C                                                                               B5
C*SELRANG         12         2    YYYYMM                                        B5
C (SELECT WINTER MONTHS DECEMBER, JANUARY, FEBRUARY FROM EACH YEAR.             B5
C  START WITH A FULL SEASON, I.E., SKIP JANUARY AND FEBRUARY OF THE 1ST YEAR.   B5
C  BE WARNED THAT THE LAST SEASON MAY STILL BE INCOMPLETE!)                     B5
C                                                                               B5
C*SELRANG        -12         2    YYYYMM                                        B5
C (SELECT ALL WINTER MONTHS DECEMBER, JANUARY, FEBRUARY, INCLUDING JANUARY AND  B5
C  FEBRUARY OF THE FIRST YEAR).                                                 B5
C                                                                               B5
C*SELRANG       1201      0228  YYYYMMDD                                        B5
C (SELECT ALL DAYS BETWEEN DECEMBER 1ST AND FEBRUARY 28TH FROM EACH YEAR)       B5
C                                                                               B5
C*SELRANG     010100    010100YYYYMMDDHH                                        B5
C (SELECT JANUARY 1ST, 00Z FROM EACH YEAR)                                      B5
C                                                                               B5
C*SELRANG          6         8       MON                                        B5
C (ASSUME THAT INPUT RECORDS ARE MONTHLY MEANS STARTING FROM JANUARY.           B5
C  SELECT JUNE-JULY-AUGUST.)                                                    B5
C                                                                               B5
C*SELRANG       0229      0229  YYYYMMDD                                        B5
C (SELECT OR EXCLUDE FEBRUARY 29)                                               B5
C
C----------------------------------------------------------------------------
C
      use diag_sizes, only : SIZES_BLONP1xBLATxNWORDIO

      LOGICAL OK,LCMPL,LOUT1,LOUT2
C
      COMMON/ICOM/IBUF(8),IDAT(SIZES_BLONP1xBLATxNWORDIO)
      CHARACTER*10 DFMT
C
C     * IUA/IUST  - ARRAY KEEPING TRACK OF UNITS ASSIGNED/STATUS.
C
      INTEGER IUA(100),IUST(100)
      COMMON /JCLPNTU/ IUA,IUST
C
      DATA MAXX/SIZES_BLONP1xBLATxNWORDIO/
C---------------------------------------------------------------------
      NF=5
      CALL JCLPNT(NF,1,2,3,5,6)
      REWIND 1
      LOUT1=.FALSE.
      IF (IUST(2).EQ.1) THEN
        LOUT1=.TRUE.
        REWIND 2
      ENDIF
      LOUT2=.FALSE.
      IF (IUST(3).EQ.1) THEN
        LOUT2=.TRUE.
        REWIND 3
      ENDIF
      IF(.NOT.(LOUT1.OR.LOUT2))CALL                XIT('SELRANG',0)
C
C     * READ INPUT PARAMETERS FROM CARD
C
      READ(5,5010,END=900) NT1,NT2,DFMT,LSPL                                    B4
C
C     * WRITE OUT THE INPUT PARAMETERS TO STANDARD OUTPUT
C
      WRITE(6,6010) NT1,NT2,DFMT,LSPL

      LCMPL=.TRUE.
      IF (NT1.LT.0) THEN
        LCMPL=.FALSE.
        NT1=-NT1
      ENDIF
C
C     * CHECK THE TIME STEP FORMAT
C
      LMON=0
      IF (DFMT.EQ.'YYYYMMDDHH'.OR.
     1    DFMT.EQ.'  YYMMDDHH'.OR.
     2    DFMT.EQ.'    MMDDHH') THEN
        IDIV=1000000
        NTYB=010100
        NTYE=123124
      ELSEIF(DFMT.EQ.'  YYYYMMDD'.OR.
     1       DFMT.EQ.'    YYMMDD'.OR.
     2       DFMT.EQ.'      MMDD')THEN
        IDIV=10000
        NTYB=0101
        NTYE=1231
      ELSEIF(DFMT.EQ.'    YYYYMM'.OR.
     1       DFMT.EQ.'      YYMM'.OR.
     2       DFMT.EQ.'        MM')THEN
        IDIV=100
        NTYB=1
        NTYE=12
      ELSEIF(DFMT.EQ.'       MON')THEN
        NTYB=1
        NTYE=12
        LMON=1
      ELSE
        WRITE(6,'(A,A10)')
     1       ' *** ERROR: ILLEGAL TIME STEP FORMAT', DFMT
        CALL                                       XIT('SELRANG',-1)
      ENDIF
C
C     * PRINT THE RANGE
C
      IF (NT2.GE.NT1) THEN
        WRITE(6,'(2(A,I10))')' SELECT TIME RANGE:',NT1,' ...',NT2
      ELSE
        WRITE(6,'(4(A,I10))')' SELECT TIME RANGE:',NT1,' ...',NTYE,
     1       ' &',NTYB,' ...',NT2
        IF(LCMPL) THEN
          WRITE(6,'(A)')' START WITH THE FIRST COMPLETE TIME RANGE.'
          WRITE(6,'(A)')' ***WARNING: THE LAST RANGE MAY BE INCOMPLETE.'
        ELSE
          WRITE(6,'(A)')' SELECT ALL RECORDS IN THE SPECIFIED RANGE.'
        ENDIF
      ENDIF
C
C     * READ RECORDS
C
      NRIN=0
      NROUT1=0
      NROUT2=0
 100  CONTINUE
      CALL RECGET(1,-1,-1,-1,-1,IBUF,MAXX,OK)
      IF (.NOT.OK) THEN
         IF(NRIN.EQ.0) CALL                        XIT('SELRANG',-2)
         CALL PRTLAB(IBUF)
         WRITE(6,6020) NRIN
         IF(LOUT1)WRITE(6,6025) NROUT1
         IF(LOUT2)WRITE(6,6025) NROUT2
         CALL                                      XIT('SELRANG',0)
      ENDIF
C
C     * WRITE OUT SUPERLABELS/CHARS
C
      IF(IBUF(1).EQ.NC4TO8("LABL").OR.IBUF(1).EQ.NC4TO8("CHAR"))THEN
        IF(LSPL.NE.0)THEN
          IF(LOUT1)CALL RECPUT(2,IBUF)
          IF(LOUT2)CALL RECPUT(3,IBUF)
        ENDIF
        GOTO 100
      ENDIF
C
C     * COUNT INPUT RECORDS OTHER THAN LABL/CHAR
C
      NRIN=NRIN+1
      IF(NRIN.EQ.1) CALL PRTLAB(IBUF)
C
C     * SKIP RECORD IF TIME IS OUTSIDE OF THE SPECIFIED RANGE
C
      IF(LMON.EQ.1)THEN
        NT=MOD(NRIN-1,12)+1
      ELSE
        NT=MOD(IBUF(2),IDIV)
      ENDIF
      IF ( NT1.LE.NT2.AND.(NT.LT.NT1.OR. NT.GT.NT2) .OR.
     1     NT1.GT.NT2.AND.(NT.LT.NT1.AND.NT.GT.NT2)) THEN
        IF(LOUT2) CALL RECPUT(3,IBUF)
        NROUT2=NROUT2+1
        GOTO 100
      ENDIF
C
C     * ENSURE THAT THE FIRST SEASON IS FULL, IF REQUESTED
C
      IF(LCMPL.AND.NT.NE.NT1) THEN
         LCMPL=.TRUE.
      ELSEIF(LCMPL.AND.NT.EQ.NT1) THEN
         LCMPL=.FALSE.
      ENDIF
      IF(LCMPL) THEN
        IF(LOUT2) CALL RECPUT(3,IBUF)
        NROUT2=NROUT2+1
        GOTO 100
      ENDIF
C
C     * WRITE OUT THE RECORD
C
      IF(LOUT1) CALL RECPUT(2,IBUF)
      NROUT1=NROUT1+1
      GO TO 100

 900  CALL                                         XIT('SELRANG',-3)
C---------------------------------------------------------------------
 5010 FORMAT (10X,2I10,A10,1I5)                                                 B4
 6010 FORMAT (' INPUT PARAMETERS:'/' NT1=',I10,' NT2=',I10,
     1        ' DFMT=',A10,' LSPL=',I5)
 6020 FORMAT (1X,I10,' RECORDS READ IN.')
 6025 FORMAT (1X,I10,' RECORDS WRITTEN OUT.')
      END
