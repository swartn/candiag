      PROGRAM XLREP 
C     PROGRAM XLREP (XIN,        XOUT,       INPUT,       OUTPUT,       )       B2
C    1        TAPE11=XIN, TAPE12=XOUT, TAPE5=INPUT, TAPE6=OUTPUT) 
C     -----------------------------------------------------------               B2
C                                                                               B2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       B2
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                
C     MAR 06/89 - F.MAJAESS (INCREASE FIELD DIMENSION TO 66000)                 
C     APR 25/88 - F.MAJAESS (ALLOW SPECIFYING A VALUE OF '000000' FOR 
C                            OLDTAPE) 
C     JUL 07/86 - M.LAZARE
C                                                                               B2
CXLREP   - CHANGES THE HYPERLABEL TAPE NUMBER VALUE                     1  1 C  B1
C                                                                               B3
CAUTHOR  - M.LAZARE                                                             B3
C                                                                               B3
CPURPOSE - FILE XIN IS SEARCHED FOR HYPERLABEL RECORD TAPE NUMBER VALUE         B3
C          MATCHING THE OLDTAPE VALUE, THEN THE TAPE NUMBER ON THE HYPERLABEL   B3
C          IS CHANGED TO THE NEW VALUE.                                         B3
C          ALL OTHER RECORDS ARE MERELY COPIED TO FILE XOUT.                    B3
C                                                                               B3
CINPUT FILE...                                                                  B3
C                                                                               B3
C      XIN  = EXISTING HYPERLABELLED FILE                                       B3
C                                                                               B3
COUTPUT FILE...                                                                 B3
C                                                                               B3
C      XOUT = RESULTING FILE                                                    B3
C 
CINPUT PARAMETERS...
C                                                                               B5
C      OLDTAPE = TAPE VALUE TO BE SEARCHED FOR AND REPLACED.                    B5
C                IF THE VALUE SPECIFIED FOR OLDTAPE IS EQUAL TO '000000'        B5
C                THEN THE VALUE CONSIDERED FOR OLDTAPE IS TAKEN TO BE           B5
C                THE TAPE NUMBER VALUE FOUND ON THE FIRST RECORD HYPERLABEL     B5
C                IN "XIN" FILE.                                                 B5
C      NEWTAPE = TAPE VALUE REPLACING "OLDTAPE"                                 B5
C                                                                               B5
CEXAMPLE OF INPUT CARD...                                                       B5
C                                                                               B5
C000000 123456                                                                  B5
C  NOTE THE ABOVE EXAMPLE MUST BE SHIFTED ONE CHARACTER TO THE LEFT 
C---------------------------------------------------------------------------- 
C 
      use diag_sizes, only : SIZES_BLONP1xBLATxNWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      LOGICAL OK
      INTEGER*8 OLDTAPE,NEWTAPE,IFAKE,IDAT
      COMMON /ICOM/ IBUF(8),IDAT(SIZES_BLONP1xBLATxNWORDIO) 
C 
      DATA MAXX/SIZES_BLONP1xBLATxNWORDIO/, IFAKE/6H000000/
C------------------------------------------------------------------------ 
      NFF=4 
      CALL JCLPNT(NFF,11,12,5,6)
      REWIND 11 
      REWIND 12 
C 
C     * READ-IN TAPE INFORMATION CARD.
C 
      READ(5,5010,END=901) OLDTAPE,NEWTAPE                                      B4
C 
C     * SEARCH XIN FOR MATCHING OLDTAPE IN HYPERLABEL RECORD. 
C 
      NC=0
      NS=0
  100 CALL RECGET(11,NC4TO8("LABL"),-1,NC4TO8("FILE"),-1,IBUF,MAXX,OK)
      IF(.NOT.OK.AND.NS.EQ.0) CALL                 XIT('XLREP',-1)
      NS=NS+1 
      IF (OLDTAPE.EQ.IFAKE) THEN
        OLDTAPE=IDAT(1) 
        GO TO 150 
      ENDIF 
      IF(IDAT(1).NE.OLDTAPE) CALL                  XIT('XLREP',-2)
C 
C     * MATCHING HYPERLABEL FOUND.  REPLACE TAPE NUMBER AND COPY FOLLOWING
C     * SET TO XOUT TILL NEXT HYPERLABEL FOUND. 
C 
  150 IF (NS.EQ.1) WRITE(6,6010) OLDTAPE,NEWTAPE
      IDAT(1)=NEWTAPE 
      CALL RECPUT(12,IBUF)
  200 CALL RECGET(11,-1,-1,-1,-1,IBUF,MAXX,OK) 
      IF(.NOT.OK .AND. NC.EQ.0) CALL               XIT('XLREP',-3)
      IF(IBUF(1).EQ.NC4TO8("LABL").AND.IBUF(3).EQ.NC4TO8("FILE")) THEN
        BACKSPACE 11
        BACKSPACE 11
        GO TO 100 
      ENDIF 
      IF(.NOT.OK)THEN 
        WRITE(6,6020) NS,NC 
        CALL                                       XIT('XLREP',0) 
      ENDIF 
      CALL RECPUT(12,IBUF)
      NC=NC+1 
      GO TO 200 
C 
C     * E.O.F. ON INPUT.
C 
  901 CALL                                         XIT('XLREP',-4)
C---------------------------------------------------------------------- 
 5010 FORMAT(A6,1X,A6)                                                          B4
 6010 FORMAT('0 CONVERTING FROM OLDTAPE= ',A6,' TO NEWTAPE= ',A6)
 6020 FORMAT(' XLREP CHANGED ',I5,' HYPERLABELS AND COPIED ',I5,
     1       ' DATA RECORDS')
      END
