      PROGRAM LEVREV
C     PROGRAM LEVREV (SERA,       AVGA,       OUTPUT,                   )       B2
C    1          TAPE1=SERA, TAPE2=AVGA, TAPE6=OUTPUT) 
C     -----------------------------------------------                           B2
C                                                                               B2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       B2
C     JUL 12/93 - F.MAJAESS (MODIFY TO HANDLE SPECTRAL FIELDS... )              
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                
C     MAY 12/83 - R.LAPRISE.                                                    
C     NOV 17/80 - J.D.HENDERSON 
C                                                                               B2
CLEVREV  - REVERSES THE ORDER OF LEVELS OF EACH SET IN A FILE           1  1    B1
C                                                                               B3
CAUTHOR  - J.D.HENDERSON                                                        B3
C                                                                               B3
CPURPOSE - REVERSES THE ORDER OF THE LEVELS IN EACH SET IN FILE SERA.           B3
C                                                                               B3
CINPUT FILE...                                                                  B3
C                                                                               B3
C      SERA  = FILE OF (MULTI-LEVEL) SETS                                       B3
C                                                                               B3
COUTPUT FILE...                                                                 B3
C                                                                               B3
C      AVGA = COPY OF FILE SERA WITH LEVELS REVERSED.                           B3
C-------------------------------------------------------------------------- 
C 
      use diag_sizes, only : SIZES_BLONP1xBLATxNWORDIO,
     &                       SIZES_MAXLEV,
     &                       SIZES_MAXLEVxBLONP1xBLAT

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      COMMON/BLANCK/F(SIZES_MAXLEVxBLONP1xBLAT)
C 
      LOGICAL OK,SPEC
      INTEGER LEV(SIZES_MAXLEV)
      COMMON/ICOM/ IBUF(8),IDAT(SIZES_BLONP1xBLATxNWORDIO) 
      DATA MAXX/SIZES_BLONP1xBLATxNWORDIO/, MAXL/SIZES_MAXLEV/ 
C-----------------------------------------------------------------------
      NFF=3 
      CALL JCLPNT(NFF,1,2,6)
      REWIND 1
      REWIND 2
C 
C     * GET THE NEXT GRID SET FROM FILE 1 
C 
      NSETS=0 
  140 CALL GETSET2(1,F,LEV,NLEV,IBUF,MAXX,OK)
      IF(.NOT.OK)THEN 
        IF(NSETS.EQ.0) CALL                        XIT('LEVREV',-1) 
        WRITE(6,6030) NSETS 
        CALL                                       XIT('LEVREV',0)
      ENDIF 
      IF((NLEV.LT.1).OR.(NLEV.GT.MAXL)) CALL       XIT('LEVREV',-2) 
      IF(NSETS.EQ.0) CALL PRTLAB (IBUF)
C 
C     * WRITE THE FIELDS IN REVERSE ORDER TO FILE 2 
C 
      SPEC=(IBUF(1).EQ.NC4TO8("SPEC").OR.IBUF(1).EQ.NC4TO8("FOUR"))
      NWDS=IBUF(5)*IBUF(6)
      IF(SPEC) NWDS=NWDS*2
      DO 400 L=1,NLEV 
      LL=NLEV-L+1 
      N=(LL-1)*NWDS+1 
      IBUF(4)=LEV(LL) 
      CALL PUTFLD2(2,F(N),IBUF,MAXX) 
      IF(NSETS.EQ.0) CALL PRTLAB (IBUF)
400   CONTINUE
      NSETS=NSETS+1 
      GO TO 140 
C-----------------------------------------------------------------------
 6030 FORMAT(' ',I6,' SETS READ')
      END
