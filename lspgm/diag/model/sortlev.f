      PROGRAM SORTLEV
C-----------------------------------------------------------------------
CSORTLEV - SORT THE ORDER OF VERTICAL LEVELS IN A FILE
C
CAUTHOR  - Y.JIAO :  AUG 10, 2016
C
CINPUT FILE...
C
C      FIN  = FILE OF (MULTI-LEVEL) SETS
C
COUTPUT FILE...
C
C      FOUT = COPY OF FIN WITH LEVELS SORTED MONOTONICALLY.
C-----------------------------------------------------------------------
C

      use diag_sizes, only : SIZES_BLONP1xBLATxNWORDIO,
     &                       SIZES_MAXLEVxBLONP1xBLAT, 
     &                       SIZES_MAXLEV

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      PARAMETER(MAXLEV=2*SIZES_MAXLEV)
      PARAMETER(MAXLL=SIZES_MAXLEVxBLONP1xBLAT)
      REAL, ALLOCATABLE, DIMENSION(:) :: F
C
      LOGICAL OK,SPEC
      INTEGER LEV(SIZES_MAXLEV),LES(SIZES_MAXLEV)
      REAL RLEV(SIZES_MAXLEV),TMP
      COMMON/ICOM/ IBUF(8),IDAT(SIZES_BLONP1xBLATxNWORDIO)
C-----------------------------------------------------------------------
      NFF=3
      CALL JCLPNT(NFF,1,2,6)
      REWIND 1
      REWIND 2
C
C     * GET THE NEXT GRID SET FROM FILE 1
C
      ALLOCATE(F(MAXLL))
      NSETS=0
  140 CALL GETSET2(1,F,LEV,NLEV,IBUF,MAXLL,OK)
      IF(.NOT.OK)THEN
        IF(NSETS.EQ.0) CALL                        XIT('SORTLEV',-1)
        WRITE(6,6030) NSETS
        CALL                                       XIT('SORTLEV', 0)
      ENDIF
      IF((NLEV.LT.1).OR.(NLEV.GT.MAXLEV)) CALL     XIT('SORTLEV',-2)
      IF(NSETS.EQ.0) CALL PRTLAB (IBUF)
C
C     * WRITE THE FIELDS IN SORTED ORDER TO FILE 2
C
      SPEC=(IBUF(1).EQ.NC4TO8("SPEC").OR.IBUF(1).EQ.NC4TO8("FOUR"))
      NWDS=IBUF(5)*IBUF(6)
      IF(SPEC) NWDS=NWDS*2

C     PRINT*,'INPUT ORDER',LEV(1:NLEV)
      IF (ANY(LEV(1:NLEV).LE.0)) THEN
        CALL LVDCODE(RLEV,LEV,NLEV)
      ELSE
        RLEV=REAL(LEV)
      ENDIF

      DO I = 1, NLEV-1
      DO J = I+1, NLEV
        IF (RLEV(I) .GT. RLEV(J)) THEN
          TMP=RLEV(J)
          RLEV(J)=RLEV(I)
          RLEV(I)=TMP
        ENDIF
      ENDDO
      ENDDO

      IF (ANY(LEV(1:NLEV).LE.0)) THEN
        RLEV=RLEV/1000.0
        CALL LVCODE(LES,RLEV,NLEV)
      ELSE
        LES=INT(RLEV)
      ENDIF
c     PRINT*,'OUTPUT ORDER',NC4TO8(IBUF(3)),LES(1:NLEV)

      DO J=1,NLEV
        DO I=1,NLEV
          IF ( LES(J) .EQ. LEV(I) ) THEN
            N=(I-1)*NWDS+1
            IBUF(4)=LEV(I)
            CALL PUTFLD2(2,F(N),IBUF,MAXLL)
          ENDIF
        END DO
      END DO

      NSETS=NSETS+1
      GO TO 140
C-----------------------------------------------------------------------
 6030 FORMAT(' ',I6,' SETS READ')
      END

