      PROGRAM SORTIME
C
CSORTIME  - SPLIT INPUT FILE BASED ON THE VALUE OF IBUF(2), i.e. TIMESTEP
C           THIS IS TO SPLIT RANDOM ORDERED DAILY GEM4/5 OUTPUT FILE INTO HOURLY FILES
C
C AUTHOR  - Y. JIAO - JULY 2018
C-----------------------------------------------------------------------
C

      use diag_sizes, only : SIZES_BLONP1xBLATxNWORDIO,
     &                       SIZES_BLONP1,
     &                       SIZES_BLAT,
     &                       SIZES_BLONP1xBLAT

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      LOGICAL OK
C
      DATA MAXLON, MAXLAT, MAXLL /SIZES_BLONP1, SIZES_BLAT,
     &  SIZES_BLONP1xBLAT/
      COMMON/ICOM/IBUF(8),IDAT(SIZES_BLONP1xBLATxNWORDIO)
      INTEGER IBF2(999)
      CHARACTER*3 NUM
C-----------------------------------------------------------------------
      NF=2
      CALL JCLPNT(NF,1,6)
      REWIND 1

      NF=999
      IBF2=-999

      NU=0
      NREC=0
 100  CONTINUE
      CALL RECGET(1,-1,-1,-1,-1,IBUF,MAXLL,OK)
      IF (.NOT.OK) THEN
        WRITE(6,6020) NREC
        CALL PRTLAB(IBUF)
        CALL                                     XIT('SORTIME',0)
      ENDIF
C
C     * SKIP SUPERLABELS/CHAR
C
      IF(IBUF(1).EQ.NC4TO8("LABL").OR.
     +   IBUF(1).EQ.NC4TO8("CHAR")) GOTO 100
      NREC=NREC+1

      IF(NREC.EQ.1) CALL PRTLAB(IBUF)
C
      IF(ALL(IBF2(1:NF).NE.IBUF(2))) THEN
        NU=NU+1
        IBF2(NU)=IBUF(2)
        WRITE(NUM,'(I3.3)') NU
        OPEN(NU+10,FILE='sortout'//TRIM(NUM), FORM='UNFORMATTED')
      ENDIF

C     * SAVE RECORD TO DIFFERENT FILE
      DO N=1,NF
        IF(IBUF(2).EQ.IBF2(N)) NUO=10+N
      ENDDO

      CALL RECPUT(NUO,IBUF)
      GOTO 100
C-----------------------------------------------------------------------------
 6010 FORMAT(' SPLIT INPUT FILE INTO ',I5,' FILES.')
 6020 FORMAT(' PROCESSED RECORDS FROM INPUT FILE :',I10)
      END
