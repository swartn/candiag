      PROGRAM GSTRACX
C     PROGRAM GSTRACX (TRAC,       LNSP,       RHO,       XL,       XP,         J2
C                      XML,        XMP,              INPUT,      OUTPUT)        J2
C    1          TAPE11=TRAC,TAPE12=LNSP,TAPE13=RHO,TAPE14=XL,TAPE15=XP,         J2
C    2          TAPE16=XML, TAPE17=XMP,        TAPE5=INPUT,TAPE6=OUTPUT)        J2
C     --------------------------------------------------------------            J2
C                                                                               J2
C     JUN 26/20 - S.KHARIN (SAVE MMR AND MC BOTH ON MODEL AND PRESSURE LEVELS)  J2
C     JUN 26/12 - S.KHARIN (FIX BUG WHICH OVERWROTE MODEL WITH PRESSURE LEVELS) J2
C     DEC 11/09 - S.KHARIN (REVISED TMIN=MAX(XREF*1.E-16,1.E-36)
C     NOV 16/09 - S.KHARIN. - REVISE FOR "ITRVAR=[4H   Q/4HSL3D" INPUT
C                             PARAMETER.
C     NOV 03/09 - S.KHARIN. - ADD OPTIONAL "XM" OUTPUT GRID FILE AND MORE
C                             OPTIONS FOR "LPRESS" INPUT PARAMETER.
C     OCT 26/09 - S.KHARIN. - ADD OPTIONAL CONVERTSION TO MASS CONCENTRATIONS.
C     NOV 17/08 - S.KHARIN. - TRACER CAN BE ON GRID OR SPECTRAL.
C                             OUTPUT CAN BE ON MODEL OR PRESSURE LEVELS.
C     MAY 01/08 - M.LAZARE. - GENERALIZED TRACER CONVERSION PROGRAM BASED
C                             ON GSHUMH.
C                                                                               J2
CGSTRACX - CONVERTS ETA (SIGMA/HYBRID) LEVEL TRACER VARIABLE TO                 J1
C          MIXING RATIO OR MASS CONCENTRATION.                          3  4 C  J1
C                                                                               J3
CAUTHOR - M. LAZARE                                                             J3
C                                                                               J3
CPURPOSE - CONVERTS ETA (SIGMA/HYBRID) LEVEL GRID FILE OF MODEL                 J3
C          TRACER VARIABLE TO MIXING RATIO OR MASS CONCENTRATION.               J3
C                                                                               J3
CINPUT FILES...                                                                 J3
C                                                                               J3
C        TRAC = MODEL TRACER ON ETA (SIGMA/HYBRID) LEVELS.                      J3
C               IT CAN BE ON GRID OR SPECTRAL.                                  J3
C        LNSP = LOG OF SURFACE PRESSURE.                                        J3
C         RHO = AIR DENSITY.                                                    J3
C                                                                               J3
COUTPUT FILES...                                                                J3
C                                                                               J3
C        XL   = OUTPUT GRID FILE FOR THE TRACER MIXING RATIOS ON MODEL LEVELS.  J3
C        XP   = OUTPUT GRID FILE FOR THE TRACER MIXING RATIOS ON PRES. LEVELS.  J3
C        XML  = OUTPUT GRID FILE FOR THE TRACER MASS CONC. ON MODEL LEVELS.     J3
C        XMP  = OUTPUT GRID FILE FOR THE TRACER MASS CONC. ON PRES. LEVELS.     J3
C                                                                               J3
CINPUT PARAMETERS...
C                                                                               J5
C CARD 1: PARAMETERS FOR CONVERSION TO MIXING RATIO.                            J5
C                                                                               J5
C      ICOORD = 4H SIG/4H ETA FOR SIGMA/ETA VERTICAL COORDINATE.                J5
C      ITRVAR = 4H   Q  FOR  VARIABLE Q                                         J5
C               4HSL3D                SL3D (SEMI-LAGRANGIAN OPTION)             J5
C               4HQHYB                HYBRID ( =Q                      ; Q<=Q0  J5
C                                              =Q0/((1+P*LN(Q0/Q))**P  ; Q>Q0   J5
C                                              WHERE Q0 IS THE REFERENCE VALUE  J5
C                                              "XREF" BELOW AND "P" IS THE      J5
C                                              REFERENCE POWER ("XPOW" BELOW)   J5
C               4HSLQB                QHYB AS ABOVE EXCEPT SEMI-LAGRANGIAN ADV. J5
C        XPOW = REFERENCE POWER FOR THE HYBRID CASE (DEFAULT; 1.)               J5
C        XREF = REFERENCE VALUE FOR THE HYBRID CASE (DEFAULT; 10.E-3 KG/KG)     J5
C                                                                               J5
C CARD 2: PARAMETERS FOR OUTPUT GAUSSIAN GRID IF INPUT TRACER IS SPECTRAL.      J5
C         (BASED ON COFAGG)                                                     J5
C      ILG   = NUMBER OF GRID POINTS IN GAUSSIAN LATITUDE CIRCLE                J5
C              (MUST BE A POWER OF TWO OR THREE TIMES A POWER OF TWO)           J5
C      ILAT  = NUMBER OF GAUSSIAN LATITUDES                                     J5
C      KUV   = 0 FOR NORMAL ANALYSIS. THIS IS THE ONLY POSSIBLE CHOICE HERE>    J5
C      NPKGG = GRID PACKING DENSITY (0 DEFAULTS TO 2)                           J5
C                                                                               J5
C CARD 3: PARAMETERS FOR CONVERSION TO PRESSURE LEVELS (BASED ON GSAPL)         J5
C                                                                               J5
C      NPL    = NUMBER OF REQUESTED PRESSURE LEVELS, (MAX $L$).                 J5
C      RLUP   = LAPSE RATE USED TO EXTRAPOLATE UPWARDS.                         J5
C      RLDN   = LAPSE RATE USED TO EXTRAPOLATE DOWNWARDS.                       J5
C      ICOORD = 4H SIG/ 4H ETA FOR SIGMA/ETA VERTICAL COORDINATES.              J5
C      PTOIT  = PRESSURE (PA) AT THE LID OF MODEL.                              J5
C      PR     = PRESSURE LEVELS (MB)                                            J5
C                                                                               J5
CEXAMPLE OF INPUT CARD(S)...                                                    J5
C                                                                               J5
C*GSTRACX.  SIG QHYB    1.5E-8        2.                                        J5
C*COFAGG.   128   64    0    2                                                  J5
C*GSAPL.     17       0.0       0.0 ET15      50.0                              J5
C  10   20   30   50   70  100  150  200  250  300  400  500  600  700  850  925J5
C1000                                                                           J5
C (CONVERT SPECTRAL TRACER ON MODEL LEVELS TO MMR AND MC ON MODEL/PRES. LEVELS) J5
C------------------------------------------------------------------------

C     Load diag size values
      use diag_sizes, only : SIZES_LA,
     &                       SIZES_LAT,
     &                       SIZES_LMTP1,
     &                       SIZES_LONP1,
     &                       SIZES_LONP1xLAT,
     &                       SIZES_LONP1xLATxNWORDIO,
     &                       SIZES_LONP2,
     &                       SIZES_MAXLEV,
     &                       SIZES_MAXLEVxLONP1xLAT,
     &                       SIZES_MAXLONP1LAT,
     &                       SIZES_PTMIN
      integer, parameter :: 
     & MAXW = SIZES_MAXLEV*
     &  (2*(SIZES_LA+SIZES_LMTP1)+(SIZES_LONP1+1)*SIZES_LAT)

      DATA MAXX,MAXLG,MAXLEV/SIZES_LONP1xLATxNWORDIO,
     &  SIZES_MAXLONP1LAT,SIZES_MAXLEV/ 

      LOGICAL OK
      INTEGER LEVS(SIZES_MAXLEV),LEVP(SIZES_MAXLEV)

      INTEGER LSR(2,SIZES_LMTP1+1),IFAX(10)
      REAL, ALLOCATABLE, DIMENSION(:) :: WRKS, WRKL, TRIGS
      REAL*8, ALLOCATABLE, DIMENSION(:) :: ALP, EPSI
      REAL*8, ALLOCATABLE, DIMENSION(:) :: SL, CL, WL, WOSSL, RAD

      REAL, ALLOCATABLE, DIMENSION(:) :: ETA,A,B,
     &                                   SIG,FSIG,
     &                                   DFLNSIG,DLNSIG
      REAL, ALLOCATABLE, DIMENSION(:) :: PR,PRLOG

C     * WORKING ARRAY (F) DIMENSION: NLEV*(NLG*NLAT+2*LA)
C     * WORKING ARRAY (FP) DIMENSION: NLEV*NLG*NLAT

      REAL, ALLOCATABLE, DIMENSION(:) :: F, FP, GSLNSP, RHO
C
C     * IUA/IUST  - ARRAY KEEPING TRACK OF UNITS ASSIGNED/STATUS.
C
      INTEGER IUA(100),IUST(100)
      COMMON /JCLPNTU/ IUA,IUST

      COMMON/ICOM/IBUF(8),IDAT(SIZES_LONP1xLATxNWORDIO)
C---------------------------------------------------------------------
      NFIL = 9
      CALL JCLPNT (NFIL,11,12,13,14,15,16,17,5,6)
      REWIND 11 ! INPUT GRIDDED OR SPECRAL TRACER
      REWIND 12 ! INPUT GRIDDED LOG OF SURF. PRESSURE
      REWIND 13 ! INPUT GRIDDED AIR DENSITY
      LMRML=0
      IF (IUST(14).EQ.1) THEN
        LMRML=1
        REWIND 14 ! OUTPUT MASS MIXING RATIO ON MODEL LEVELS
      ENDIF
      LMRPL=0
      IF (IUST(15).EQ.1) THEN
        LMRPL=1
        REWIND 15 ! OUTPUT MASS MIXING RATIO ON PRESSURE LEVELS
      ENDIF
      LMCML=0
      IF (IUST(16).EQ.1) THEN
        LMCML=1
       REWIND 16 ! OUTPUT MASS CONCENTRATION ON MODEL LEVELS
      ENDIF
      LMCPL=0
      IF (IUST(17).EQ.1) THEN
        LMCPL=1
        REWIND 17 ! OUTPUT MASS CONCENTRATION ON PRESSURE LEVELS
      ENDIF

C     * READ-IN DIRECTIVE CARD.

      READ(5,5000,END=911) ICOORD,ITRVAR,XREF,XPOW                              J4
      WRITE(6,6000) ICOORD,ITRVAR,XREF,XPOW
      IF( ICOORD.EQ.NC4TO8("    ")) CALL           XIT('GSTRACX',-1)
      IF( ITRVAR.EQ.NC4TO8("    ")) CALL           XIT('GSTRACX',-2)
      IF( ITRVAR.NE.NC4TO8("   Q") .AND.
     1    ITRVAR.NE.NC4TO8("SL3D") .AND.
     2    ITRVAR.NE.NC4TO8("QHYB") .AND.
     3    ITRVAR.NE.NC4TO8("SLQB")) CALL           XIT('GSTRACX',-3)

C     * DETERMINE NUMBER OF LEVELS IN THE MODEL, IE NSL.

      CALL FILEV (LEVS,NSL,IBUF,11)
      IF(NSL.EQ.0 .OR. NSL.GT.MAXLEV) CALL         XIT('GSTRACX',-4)
      NAME=IBUF(3)
      WRITE(6,6015) NAME,NSL,(LEVS(L),L=1,NSL)
C
C     * DETERMINE RECORD TYPE
C
      IF(IBUF(1).EQ.NC4TO8("GRID")) THEN
C
C       * INPUT FILE CONTAINS GRIDS
C
        LSPEC=0
        ILG1=IBUF(5)
        ILAT=IBUF(6)
        NWDS=ILG1*ILAT
        ILLGG=IBUF(7)
        NPKGG=IBUF(8)
      ELSE IF(IBUF(1).EQ.NC4TO8("SPEC")) THEN
C
C       * INPUT FILE CONTAINS SPECTRAL COEFFICIENTS
C
        LSPEC=1
        WRITE(6,'(A)') ' READ-IN COFAGG CARD...'
        READ(5,5010,END=912) ILG,ILAT,KUV,NPKGG                                 J4
        IF (ILG+2.GT.MAXLG)   THEN
          WRITE(6,6005) ILG,MAXLG-2
          CALL                                     XIT('GSTRACX',-5)
        ENDIF
        MAXLG=ILG+2
C
C       * DO NOT ALLOW MODEL WINDS
C
        IF(KUV.NE.0) CALL                          XIT('GSTRACX',-6)
        ILATH=ILAT/2
        ILG1=ILG+1
        NWDS=ILG1*ILAT
        ILLGG=0
        LRLMT=IBUF(7)
        IF(LRLMT.LT.100) CALL FXLRLMT (LRLMT,IBUF(5),IBUF(6),0)
        CALL DIMGT(LSR,LA,LR,LM,KTR,LRLMT)
        CALL PRLRLMT (LA,LR,LM,KTR,LRLMT)

        ALLOCATE(WRKS(64*MAXLG),
     &           WRKL(SIZES_MAXLEV*MAX(ILG1,ILAT)),
     &           TRIGS(NWDS))
        ALLOCATE(ALP(SIZES_LA+(2*SIZES_LMTP1)),
     &           EPSI(SIZES_LA+(2*SIZES_LMTP1)))
        ALLOCATE(SL(ILAT),CL(ILAT),WL(ILAT),WOSSL(ILAT),RAD(ILAT))
     
        CALL EPSCAL(EPSI,LSR,LM)
        CALL GAUSSG(ILATH,SL,WL,CL,RAD,WOSSL)
        CALL  TRIGL(ILATH,SL,WL,CL,RAD,WOSSL)
        CALL FTSETUP(TRIGS,IFAX,ILG)

        WRITE(6,6020) LR,LM,ILG1,ILAT,NSL
      ENDIF

      ALLOCATE(ETA(NSL),A(NSL),B(NSL),
     &         SIG(NSL),FSIG(NSL),
     &         DFLNSIG(NSL+1),DLNSIG(NSL))
      ALLOCATE(F(MAXW),FP(NWDS*NSL),
     &         GSLNSP(NWDS),RHO(NWDS))
C     * READ IN INPUT CARD FOR PRESSURE-LEVEL INTERPOLATION.

      WRITE(6,'(A)') ' READ-IN GSAPL CARD...'
      READ(5,5015,END=913) NPL,RLUP,RLDN,ICOORD,PTOIT                           J4
      IF(ICOORD.EQ.NC4TO8("    ")) ICOORD=NC4TO8(" SIG")
      IF(ICOORD.EQ.NC4TO8(" SIG")) THEN
        PTOIT=MAX(PTOIT,0.00E0)
      ELSE
        PTOIT=MAX(PTOIT,SIZES_PTMIN)
      ENDIF
      IF(NPL.GT.MAXLEV) CALL                       XIT('GSTRACX',-7)
      ALLOCATE(PR(NPL),PRLOG(NPL))
      READ(5,5020,END=914) (LEVP(I),I=1,NPL)                                    J4

C     * DECODE LEVELS.

      CALL LVDCODE(PR,LEVP,NPL)

      WRITE(6,6025) RLUP,RLDN,ICOORD,PTOIT
      CALL WRITLEV(PR,NPL,' PR ')

      DO L=2,NPL
        IF(PR(L).LE.PR(L-1)) CALL                  XIT('GSTRACX',-8)
      ENDDO
      DO L=1,NPL
        PRLOG(L)=LOG(PR(L))
      ENDDO

C     * GET ETA VALUES FROM THE GSFLD FILE.

      CALL LVDCODE(ETA,LEVS,NSL)
      DO L=1,NSL
        ETA(L)=ETA(L)*0.001E0
      ENDDO

C     * EVALUATE THE PARAMETERS OF THE ETA VERTICAL DISCRETIZATION.

      CALL COORDAB (A,B, NSL,ETA, ICOORD,PTOIT)

C---------------------------------------------------------------------

      NRECS=0
      NSETS=0
C
  200 CONTINUE

C     * LEVEL LOOP OVER ALL LEVELS.
C       ---------------------------

      DO L=1,NSL

C       * GET THE MODEL TRACER VARIABLE INTO ARRAY F

        IF (LSPEC.EQ.0)THEN
C
C         * GRIDS
C
          IJ=(L-1)*NWDS+1
          CALL GETFLD2(11,F(IJ),NC4TO8("GRID"),-1,-1,LEVS(L),
     1         IBUF,MAXX,OK)
        ELSE
C
C         * SPECTRAL COEFFICIENTS
C
          IJ=(L-1)*2*LA+1
          CALL GETFLD2(11,F(IJ),NC4TO8("SPEC"),-1,-1,LEVS(L),
     1         IBUF,MAXX,OK)
        ENDIF
        IF(NSETS.EQ.0) CALL PRTLAB(IBUF)
        IF(.NOT.OK)THEN
          WRITE(6,6030) NRECS,NSETS
          IF(NRECS.EQ.0 .OR. L.NE.1)THEN
            CALL                                   XIT('GSTRACX',-9)
          ELSE
            CALL                                   XIT('GSTRACX',0)
          ENDIF
        ENDIF
        NRECS=NRECS+1
      ENDDO ! L=1,NSL
C
C       * CONVERT SPECTRAL TO GRIDS (GRIDS ARE IN F STARTING AT LEN+1)
C
      IF (LSPEC.EQ.1)THEN
        LEN=(2*LA+NWDS)*NSL
        CALL STAGG (F(LEN+1),ILG1,ILAT,NSL,SL,F(1),LSR,LM,LA,
     1       ALP,EPSI,WRKS,WRKL,TRIGS,IFAX,MAXLG)
        DO I=1,ILG1*ILAT*NSL
          F(I)=F(LEN+I)
        ENDDO
      ENDIF
C
C     * CALCULATE MIXING RATIO
C
      IF(XREF.GT.0.E0) THEN
C
C       * FOR XREF=0., FIELD IS ALREADY IN MIXING RATIO AND
C       *              NO CONVERSION REQUIRED.
C
        DO L=1,NSL
          IJ=(L-1)*NWDS
          PINV=1./XPOW
          TMIN=MAX(XREF*1.E-16,1.E-36)
          XMIN=XREF/((1.E0+XPOW*LOG(XREF/TMIN))**PINV)
          DO I=1,NWDS
            TRAC = MAX(F(IJ+I),XMIN)
            IF(TRAC.GE.XREF) THEN
              F(IJ+I) = TRAC
            ELSE
              F(IJ+I) = XREF*(EXP(PINV*(1.-(XREF/TRAC)**XPOW)))
            ENDIF
          ENDDO
        ENDDO
      ENDIF
      NST=IBUF(2)
      CALL SETLAB(IBUF,NC4TO8("GRID"),-1,-1,-1,ILG1,ILAT,ILLGG,NPKGG)
C
C     * SAVE MIXING RATIOS ON MODEL LEVELS
C
      IF(LMRML.EQ.1) THEN
        IBUF(3)=NAME
        IBUF(8)=NPKGG
        CALL PUTSET2(14,F,LEVS,NSL,IBUF,MAXX)
        IF(NSETS.EQ.0) CALL PRTLAB(IBUF)
      ENDIF
C
C     * READ LOG OF SURFACE PRESSURE FOR THE SAME TIME STEP
C
      IF(LMRPL.EQ.1.OR.LMCPL.EQ.1) THEN
        CALL GETFLD2(12,GSLNSP,NC4TO8("GRID"),NST,NC4TO8("LNSP"),1,
     +       IBUF,MAXX,OK)
        IF(NSETS.EQ.0) CALL PRTLAB(IBUF)
        IF(.NOT.OK) CALL                           XIT('GSTRACX',-10)
        IF(IBUF(5).NE.ILG1.OR.IBUF(6).NE.ILAT)CALL XIT('GSTRACX',-11)
      ENDIF
C
C     * INTERPOLATE MIXING RATIOS TO PRESSURE LEVELS
C
      IF(LMRPL.EQ.1) THEN
        IF(NSETS.EQ.0)
     1       WRITE(6,'(A)')' INTERPOLATE MIX.RATIOS TO PRESSURE LEVELS.'

C     * INTERPOLATE FROM ETA TO PRESSURE.

        CALL EAPL (FP,NWDS,PRLOG,NPL,F,SIG,NSL,GSLNSP,RLUP,RLDN,
     1       A,B, NSL+1,FSIG,DFLNSIG,DLNSIG)
C
C     * SAVE MIXING RATIOS ON PRESSURE LEVELS
C
        IBUF(3)=NAME
        IBUF(8)=NPKGG
        CALL PUTSET2(15,FP,LEVP,NPL,IBUF,MAXX)
        IF(NSETS.EQ.0) CALL PRTLAB(IBUF)
      ENDIF
C
C     * CONVERT MIXING RATIO TO MASS CONCENTRATION
C
      IF(LMCML.EQ.1.OR.LMCPL.EQ.1) THEN
        DO L=1,NSL
          IJ=(L-1)*NWDS
C
C     * READ AIR DENSITY
C
          CALL GETFLD2(13,RHO,NC4TO8("GRID"),NST,-1,LEVS(L),
     +         IBUF,MAXX,OK)
          IF(.NOT.OK) THEN
            CALL PRTLAB(IBUF)
            CALL                                   XIT('GSTRACX',-12)
          ENDIF
          DO I=1,NWDS
            F(IJ+I)=F(IJ+I)*RHO(I)
          ENDDO
        ENDDO ! L=1,NSL
      ENDIF
C
C     * SAVE MASS CONCENTREATIONS ON MODEL LEVELS
C
      IF(LMCML.EQ.1) THEN
        IBUF(3)=NAME
        IBUF(8)=NPKGG
        CALL PUTSET2(16,F,LEVS,NSL,IBUF,MAXX)
        IF(NSETS.EQ.0) CALL PRTLAB(IBUF)
      ENDIF
C
C     * INTERPOLATE MASS CONCENTRATIONS TO PRESSURE LEVELS
C
      IF(LMCPL.EQ.1) THEN
        IF(NSETS.EQ.0)
     1       WRITE(6,'(A)')' INTERPOLATE MASS CONC. TO PRESSURE LEVELS.'
C
C     * INTERPOLATE FROM ETA TO PRESSURE.
C
        CALL EAPL (FP,NWDS,PRLOG,NPL,F,SIG,NSL,GSLNSP,RLUP,RLDN,
     1       A,B, NSL+1,FSIG,DFLNSIG,DLNSIG)
C
C     * SAVE TRACER OR MASS CONCENTR. ON PRESSURE LEVELS
C
        IBUF(3)=NAME
        IBUF(8)=NPKGG
        CALL PUTSET2(17,FP,LEVP,NPL,IBUF,MAXX)
        IF(NSETS.EQ.0) CALL PRTLAB(IBUF)
      ENDIF

      NSETS=NSETS+1
      GO TO 200

C     * E.O.F. ON INPUT.

  911 CALL                                         XIT('GSTRACX',-13)
  912 CALL                                         XIT('GSTRACX',-14)
  913 CALL                                         XIT('GSTRACX',-15)
  914 CALL                                         XIT('GSTRACX',-16)
C-----------------------------------------------------------------------
 5000 FORMAT(10X,2(1X,A4),2E10.0)                                               J4
 5010 FORMAT(10X,4I5)                                                           J4
 5015 FORMAT(10X,I5,2E10.0,1X,A4,E10.0)                                         J4
 5020 FORMAT(16I5)                                                              J4
 6000 FORMAT(' COORD=',A4,' MOISTURE VARIABLE=',A4,
     1       ' XREF=',E10.3,' XPOW=',E10.3)
 6005 FORMAT(' ILG=',I4,' IS TO BIG, MAXIMUM PERMISSIBLE IS ',I4)
 6015 FORMAT(' NAME=',A4,',NSL=',I5,/,' ETA LEVELS=',10I6/(12X,10I6))
 6020 FORMAT(' SPEC=',2I5,'  GRID=',2I5,'  NSL=',I5)
 6025 FORMAT(' RLUP,RLDN=',2F6.2,' ICOORD=',1X,A4,
     1       ' P.LID (PA)=',E10.3)
 6030 FORMAT(' GSTRAC TRANSFORMED',I6,'  RECORDS AND',I6, ' SETS')
      END
